# Ned README.md

test April 14, 2022

The New Editor (ned) open source programming editor is designed to work well over a terminal
(local or remote, like SSH), and so is text only. The full-screen editor uses ANSI terminal
commands for display, which is supported by most shells. It has been tested on Windows, MAC and
Linux.

Ned was originally created in the 1980s by a couple of college kids (Kevin and Drew) over a few
weekends in a cabin by the lake.

Some notable features of Ned are:

* Full-featured editor (cut, copy, paste, syntax highlighting, undo, redo)
* Can work with multiple workspaces (list of files, project folders, settings, etc...)
* Supports color themes, including dark mode, monochrome, greenterm, classic
* Next/previous error and function commands
* Remappable hot-keys for user preference

## Downloading Ned

For some systems, ned comes pre-built (no need to build from source).

Go to: https://gitlab.com/drew.gislason/neweditor

Download the appropriate binary from the downloads folder.

For a full list of commands and features download NedManual.pdf from the docs folder.

## Ned Folder Structure

folder      | description
----------- | -----------
/docs       | documentation
/downloads  | downloads
/src        | source files
/test       | unit tests
/tools      | extra tools to build source

## Using SSH

On MacOS:

```shell

# see: https://itprohelper.com/how-to-change-the-default-ssh-port-in-macos-using-terminal/
# check if SSH is up and running
sudo systemsetup -getremotelogin

# enable SSH server on this macOS machine
sudo launchctl load -w /System/Library/LaunchDaemons/ssh.plist

# disable SSH server on this macOS machine
sudo launchctl unload /System/Library/LaunchDaemons/ssh.plist
```

# MacOS Setup

Some keys like `Ctrl-Up` or `Alt-Up` aren't available to applications in the MacOS terminal by default.

To set these up, go to Terminal/Preferences/Prilfes/Keyboard and add key Esc codes. See docs/keyboard.png

## Building Ned

Ned is designed to be very simple to build and test. It requires GCC (or a compatible C compiler) and make.

On MAC OS X / Linux, Windows WSL. Open a terminal, choose a folder, then type:

```bash
$ git clone git@gitlab.com:drew.gislason/neweditor.git
$ cd neweditor/tools
$ make
$ cd ../src
$ make
$ cd ../test
$ make
```

## Release History

04/14/2022 - Ned 2.0    Initial release of this project
