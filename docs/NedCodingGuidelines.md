# C Coding Guidelines

Unfortunately there are no specific coding guidelines for C... or there too are many depending on
how you look at it. Here are the C coding guidelines used throughout the Ned editor project.

## 1. Overview

Code is read much more often than it is written.

General C coding guidelines:

* Code and Comment for Clarity
* Iterate Requirements, design, code and test
* Make as much code "single purpose" as possible
* Turn up compiler warnings as high as possible
* Have a bug-free attitude. Don't write code that "mostly works". Test limits
* Consistently comment, indent and bracket throughout the code
* Consistently name variables, functions, types and CONSTANTS
* Use consistent tabs or spaces for indent, spaces always for alignment
* Use consistent folder structure across projects
* Use common c variables where possible (i,j,k for loops, n for length)
* Use the "Rule of .h" for #includes
* TODO: and BUGBUG: are searchable markers in code
* Use as narrow of scope has possible for types, variables and functions
* Use const, restrict and static where appropriate
* Treat C structures and modules as objects when appropriate
* Think about, discuss, plan and implement testing while coding
* Make as many tests as possible automated
* Once an object or module API is complete, be slow to modify the API, augment instead
* Refactor to clean up poor code or as understanding is increased
* Use Assert() and AssertDbg() liberally throughout the code
* Read books like "The Practice of Programming", "Writing Solid Code"
* Use Model/View/Controller where possible (separate concerns of processing, output, input)
* Use Git, not editors, to fiddle with line endings (\r vs \r\n)
* Limit use of any features after C99
* If coding for embedded (memory constrained) systems, RAM (stack & heap) are precious
* Only optimize where needed, based on  (which may have been at design phase)

Below is a Ned .h coding example excerpt:

```c
#ifndef NED_BUFFER_H
#define NED_BUFFER_H

#define NEDBUF_SANCHK      777U   // sanity check
#define NEDBUF_SIZE        ((8 * 1024) - 2 * sizeof(void *) - 3 * sizeof(unsigned))

typedef struct sNedBuffer_t
{
  struct sNedBuffer_t *pPrev;     // NULL if at start of chain
  struct sNedBuffer_t *pNext;     // NULL if at end of chain
  unsigned             sanchk;    // sanity check
  unsigned             id;        // id for entire chain
  unsigned             len;       // length of data in this buffer (0 - (NEDBUF_SIZE-1))
  uint8_t              aData[NEDBUF_SIZE];
} sNedBuffer_t;

// buffer API
sNedBuffer_t   *NedBufNew         (unsigned id);
sNedBuffer_t   *NedBufAppend      (sNedBuffer_t *pBuf);
sNedBuffer_t   *NedBufFree        (sNedBuffer_t *pBuf);
bool_t          NedBufIsBuf       (const sNedBuffer_t *pBuf);
// ...

#endif // NED_BUFFER_H
```

Below is a .c coding example excerpt:

```c
/*!-----------------------------------------------------------------------------------------------
  Create a new buffer chain. The id is used to identify the entire chain.

  @ingroup  ned_buf
  @param    id    managed by caller, should be unique among all buffer chains in the program.

  @return   Pointer to buffer or NULL if it couldn't be allocated.
*///-----------------------------------------------------------------------------------------------
sNedBuffer_t *NedBufNew(unsigned id)
{
  sNedBuffer_t *pBuf;

  pBuf = malloc(sizeof(sNedBuffer_t));
  if(pBuf)
  {
    pBuf->sanchk  = NEDBUF_SANCHK;  // sanity check: make sure this is a buffer
    pBuf->id      = id;             // ID of buffer chain
    pBuf->len     = 0;
    pBuf->pNext   = NULL;
    pBuf->pPrev   = NULL;
    memset(&pBuf->aData[0], NEDDBG_ALLOC, NedBufMaxLen());
  }
  return pBuf;
}
```

### 1.1 - Code and Comment for Clarity

Code is read much more often than it is written, both on teams and even solo. Look that up.

This is a different philosophy than many programmers actually use. Most programmers optimize for
brevity (fewest lines of code), speed, code-size or cleverness (to show they know advanced
language features). Most programmers comment the code sometime later, if at all, rather than
commenting for clarity as they go. Many programmers don't update the comments when they update the
code, leaving a confusing mismatch. Hint: bad directions are worse than no directions.

What does it really mean to code and comment for clarity? Just read the "Up goer 5" on
[xkcd](https://xkcd.com/1133/).

Simply put, comments, and the way the code is separated into blocks, functions and modules, should
make the code obvious what it does. Assume the reader can read the programming language.

Try your code out on collegues. Have them read some of your code and then tell you what it does. Is
it obvious to them after a few minutes, or does it require intense study?

Coding for Clarity is hard. Practice it and you'll get better. 

### 1.2 - Design, Code, Test, Iterate Frequently

The Waterfull method is Define-Requirements, Design, Code, Itegrate-and-Test. The waterfall method
is flawed. No one, and no team is smart enough to get all the requirements right up front, all the
design right before coding, all the coding right before testing, and have all those separately
built components integrate. The Waterfall method is like World Peace: great in concept, not
practical.

Instead, write-down and agree on your initial requirements (what must it do), come up with initial
designs for some parts of the system, code them, test them, integrate them, demonstrate them to
all concerned and go back to the beginning from what you just learned. Do this for each
sub-system, and integrate with the whole system early and often.

Some say to use a prototyping language to do this, but you really want to get great at using your
language(s) of choice to make the actual thing. Just make sure you understand alot of your inital
code and testing will be thrown away. Don't expect to get it right the first time. Don't expect the
requirements to remain unchanged throughout the project.

Often times the people who need to make the requirements aren't technical enough to do so precisely
enough to code. And the people coding don't understand the use cases and customers enough to make
the proper design choices. It takes iterations between these two competing thought processes to get
it right for something that works for the market and can be done in a timely manner.

### 1.3 - Make as much code "general purpose" and "single purpose" as possible

Why write a specialized function or subsystem? Reuse what you already have built, or what someone
else has made.

There is lots of great open source. Use it if your project (and legal) requirements allow. Or
glean ideas from it and use those, coding it in your own way. Why start from scratch when so many
others have done things well.

### 1.4 - Turn up Compiler Warnings as High as Possible

Why not get free debugging? Modern compilers can catch many commonplace problems before you even
run the code, why not use them? Turn warnings into error in the compile process if possible.

For example, use `-std=c11 -pedantic-errors -Wall -Werror` with gcc.

You may initially find many more warnings/errors than you expected. You'll eventually change your
coding style to reflect safer practices.

Also, use linters, such as flake8 for Python.

### 1.5 -  Have a Bug-Free Attitude

As you write code, think: what would be likely bugs from this code?

One issue is limits. Every function that copies into memory MUST have a limit. Even if it allocates
memory from the heap, it must have a reasonable limit.

Functions like gets() and sprintf() weren't written with a bug-free attitude. Both of them can
cause buffer overruns.

Functions like snprintf() and strncpy() weren't written with a bug-free attitude. Both of them
SOMETIMES terminate the string with a '\0'.

Spend time reading books like "The Practice of Programming" and "Writing Solid Code" to help think
about this topic more.

### 1.6 Use the "Rule of .h" for #includes

All .h files have a define to prevent it from being included more than once.

```c
#ifndef NED_BUFFER_H
#define NED_BUFFER_H
...
#endif
```

In addition, if a C module will be used with C++, then use the following:

```c
#ifdef __cplusplus
extern "C" {
#endif
...
#ifdef __cplusplus
}
#endif
```

H files should never be order dependent. If they depend on other .h files, then include those in
the .h, as they will not be included twice.

### 1.7 Style Guides for Projects

Publish a style guide for each project. Ideally, it's the same style guide for all code that is
created by your company.

The Ned Style Guide is in section 2 of this document.

The most important thing is to be consistent, so readers don't get confused.

### 1.8 API Contracts

When developing a C API, liberally use `const` where you can. For example, the prototype below
indicates that the contents of the memory pointed to by either `pThis` or `pThat` won't be changed
by NedMemICmp(). And why is memicmp() included in the standard C library?

```c
int NedMemICmp(const void *pThis, const void *pThat, size_t len);
```

Liberally use static for functions, to indicate they are local.

```c
static int _MyLocalFunction(myType_t t);
```

### 1.9 A Comment on Comments

Ned uses a function header comments that can be read by Doxygen and other tools (like NedManPage).

```c
/*!----------------------------------------------------------------------------------------------
  Compare bytes in memory, but ignore case for ASCII 'a'-'z' and 'A'-'Z'.
  @param    pThis   pointer to some memory
  @param    pThat   pointer to some other memory
  @param    len     length to compare
  @return   -1 if less than, 0 if equal, 1 if greater than
*///---------------------------------------------------------------------------------------------
int NedMemICmp(const void *pThis, const void *pThat, size_t len);
```

Make sure comments augment what is shown in the code. Assume the reader can read code. The
following code block has a line comment to indicate that a return of -1 from `NedMsgLineEdit` means
the user pressed ESC. Otherwise, the reader would have to look up NedMsgLineEdit(). Of course, make
sure to update this comment if changing the return codes of `NedMsgLineEdit`.

```c
  // do nothing if user pressed ESC
  len = NedMsgLineEdit(szNewPath, sizeof(szNewPath) - 1, szSaveAs, NULL, NedFilePath(pFile));
  if(len <= 0)
    break;
````

Keep comments pithy. A lengthy comment is hard to read and rarely adds clarity.

## 2 - Ned Style Guidelines

Style doesn't affect compiling, or debugging, but does make the code consistent when a team of
programmers work on the same code. It also makes the code easier to read. Don't believe me? Look
up "obfuscated C" in your browser.

Symbol styles can be camelCase, MixedCase, snake_case and CONST_CASE. Ned doesn't use snake_case,
as studies have found camelCase to be faster to read. Don't believe me? Look on Wikipedia for
"camelCase".

Ned C Style choices:

* Indent 2 spaces (not hard-tabs)
* CONSTANTS are CONST_CASE
* All types not native C end in _t, for example nedSize_t, or nedPoint_t, but `int` is native
* typedefs and variables are camelCase (myType_t, myVar, i)
* typedef all structs and enums
* functions are MixedCase()
* Use an underscore in front of local (static) functions, so they are obvious
* Comment each file/function with Doxygen Subset, that also works with NedManPage
* Braces are never a good idea
* Braces are always at the same indent level. Same goes for case/break.
* Always use `default:` in switch statements
* Declare one variable per line
* Line things up for neatness. Examples are function returns vs names vs parameters
* Comments should add value. Don't state the obvious. Don't forget to comment the non-obvious
* Keep comments up to date as you code
* Generally use 1 return per function. It's OK to error out and return at the top of the function
* If using the same code in 2 or more places, make a function and call that
* Create functions to reduce the complexity of higher-level function
* Functions generally should be viewable in a single page in the editor or less
* Use a private .h file for internal APIs to an object spread over multiple modules
* All files can count on NedAssert(), NumElements(), and bool_t
* All higher-level objects and APIs uses the NedLog() system
* All objects include a way to print the object to the log

### 2.1 Ned Doxygen Comments

Ned uses a subset of the Doxygen commands that work with the ned `manpage` tool for making quick
HTML or markdown documentation.

Here are the allowed fields: `@file`, `@copyright`, `@defgroup`, `@ingroup`, `@brief`,`@param`, `@return`. 

Each .c file contains an `@file`, `@copyright` and `@brief` describe the module, followed by an
`@ingroup` to which indicate which group the functions belongs to. If it's the "main" module for an
API, then it also will include and `@defgroup` in the header.

Each function is documented with the `/*!` and `*///` block comment so that this function will be
autodocumented. This is shorthand for include `@fn` and `@brief` to describe the function. Any
parameters and return values are documented with the `@param` and `@return`.

```c
/*!------------------------------------------------------------------------------------------------
  Is this character a brace? One of ({<[]>}). Returns index into "({<[".

  @param    pPoint       pointer to preallocated point
  @return   >0 if open brace, <0 if ending brace, 0 if not brace.
*///-----------------------------------------------------------------------------------------------
int NedPointIsBrace(const sNedPoint_t *pPoint)
```
