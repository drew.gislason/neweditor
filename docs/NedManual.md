Ned 2.0 Manual

April 14, 2022

Why yet another editor? Sometimes it's nice to have a modern editor in a text only terminal.

If you are familiar with editors such as Sublime Text, Atom or Visual Studio Code, you'll be
instantly familiar with Ned.

Ned works cross platform on macOs X, Linux, and Windows (WSL).

<img src="NedMainScreen.png" alt="ned" width="512"/>

## 1 - Ned Overview

Ned uses ANSI color and positioning codes to display a full screen editor in a terminal such as
Bash, zsh or SSH. This section describes Ned in brief.

### 1.1 - Ned License

Ned is open source. You are welcome to the Ned editor free of charge and use some or all parts of
Ned source code in your own projects. Simply include the following [MIT License]
(https://opensource.org/licenses/MIT) in documentation and leave the MIT license in the Ned source
code.

Copyright 2021 Drew Gislason

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

### 1.2 - Ned Features

Ned contains a reasonable list of features expected in modern editors, most of which it had back in
the 1980s. It was originally created by two college kids, Kevin Smith and Drew Gislason, in a few
weekends by the lake as an alternative to the expensive editors of the day. This version of ned
has been rewritten from scratch.

* Full screen editor similar to Sublime Text, Visual Studio Code, or Atom, but for a text terminal
* Multiple projects (lists of files/folders, settings per project, color themes, etc...)
* Restore session (same files open in same positions, saved in project file)
* Color themes (classic, darkmode, natural, 1980s, custom, etc...)
* Search and replace within file or across project folder(s)
* Syntax highlighting and tab settings for many programming languages
* Bookmarks
* Undo/redo
* Record/replay macros for repetitious editing
* Match brace, next/previous function, goto definition and other advanced navigation
* Simple install: `cp ned` to somewhere in the $PATH
* If building from source, no external dependencies other than a C compiler
* Remap any key to any command

Some Ned unique features:

* Smart case toggle: `lower`, `UPPER`, `camelCase`, `MixedCase`, `snake_case`, `CONST_CASE`
* Goto next/previous method/function/struct
* Goto next/previous error for edit/compile/debug cycle
* Smart tabs: tabs or spaces for indentation, but always spaces for alignment in comments
* Built-in help documentation (this file)
* Read-only mode to prevent accidental editing
* Quickly save all unsaved and eXit `Alt-X`, or Quit `Alt-Q` to ask save each unsaved file
* Max file size and line limit based on C "long" type, usually 63-bits

What Ned doesn't have:

* No Mouse or graphics support (text only)
* No Unicode or UTF-8, understands ASCII only
* No Split windows or multiple views into a file. Instead, open multiple terminal windows
* No Multiple cursors
* No Compare / merge files
* No Regular expression search/replace
* Not and IDE. Just an editor

### 1.3 - Getting Started

This manual assumes you are familar with the `bash` or `zsh` command-line and with `git`.

The `ned` executable program can be found precompiled in the `ned` git repository in the `/bin`
folder with `macOs`, `linux` and `windows` sub-folders. Just copy the `ned` file your $HOME folder
or to someplace in your path.

```bash
# Copy precompiled ned on macOs, Linux, or Windows WSL anywhere in your $HOME folder
$ sudo cp ned ~
$ ~/ned
```

If you don't already have a place for terminal programs, you can add `ned` to your $HOME folder
and also add that to your $PATH.
 
The following line when added to the last line of your terminal startup file adds your $HOME folder
to your $PATH. In zsh, the terminal startup file is `~/.zshrc`, in bash, it is `~/.bashrc`.

```bash
# in ~/.zshrc
export PATH=$PATH:~

# in ~/.bashrc
PATH=$PATH:~
````

If you'd like to build `ned` from source code, you'll need a C compiler, make and git installed.

```bash
# Or Build ned from source code
$ git clone url/to/ned
$ cd ned/src
$ make -B
$ sudo cp ned ~
$ ~/ned
```

Below are a few examples of command-line arguments to Ned.

```bash
$ ned --version
$ ned --help
$ ned myfile.c
$ ned *.c *.py "~/Documents/Folder With Spaces/filename with spaces.txt"
$ ned -p . test.c
$ ned
$ gcc -c -Wall myfile.c 2>err.txt
$ ned -e err.txt
$ ned *.rs -l,ef
```

Project files save things like the file list and location, recently opened files and current color
theme. The project file is optional and is NOT created by default. You must specifyf the `-p`
option.

The following example will create a project file, then after quitting ned, running ned will restore
the session.

```bash
$ ned -p . *.c
$ ned 
```

Running Ned with a `-e errfile` will load up all the files listed with warnings/errors from the
`gcc` compiler or programs like `flake8` for Python, and allow you to navigate to next/previous
error, autoloading files as needed.

```bash
  $ gcc -c myfile.c 2>err.txt
  $ ned -e err.txt
```

To quit Ned, press `Alt-Q`. It will ask you to save any unsaved files before exiting. Or you can
save all unsaved files quietly and exit with `Alt-X`. The lesser used `Alt-K` will "kill" Ned,
saving nothing (not even the project file).

On the off-chance that Ned crashes, all unsaved files will be saved.

### 1.4 - A Caveat About Keys

Ned has no mouse support.

The typical keys like `Ctrl-X`, `Ctrl-C`, `Ctrl-V` are cut, copy, paste as you'd expect. Arrows do
what you'd expect. Sorry macOs fans, Command-X, Command-C and Command-V aren't available until I
make a better keyboard driver for the editor. 

Selecting in most editors is done with Shift-cursor keys. Again, due to C's limited termios input,
shift-keys aren't available. Instead use `Ctrl-D` to select a word or `Ctrl-L` to select a line,
then use cursor keys. You can then use cut, copy, paste as normal. Press `Esc` to exit select
mode.

Keys like `Ctrl-Left` (word left) and `Ctrl-Right` (word right) work fine. The keys `Home` and
`End` work on some systems (Linux), but not on others (macOs), so you can the Alt-ernatives. For
example, Alt-Left to mimic Home and go to the beginning of a line.

Keys like `PgUp` and `PgDn` may or may not be available on any given terminal. So, instead, Ned
uses a variety of alternate Alt-Keys as described in the "Ned Key Map Reference" later on.

In macOS, by default, Ctrl-Up, Alt-Up do nothing. I my Terminal configuration, I add some keys in
the Terminal/Preferences/Profiles/Keyboard menu. Set `Ctrl-Up` as `Esc[1;5A`, `Ctrl-Down` as `Esc
[1;5B`, `Alt-Up` (PgUp) as `Esc[5~` and `Alt-Down` as `Esc[6~`. 

I'm working on portable ways to get more keys in a C program. If you have knowledge of such with C
code examples, let me know. If you downloaded the source, you can play around with the raw
keyboard input in `NedKey.c`, function `NedKeyRawGetKey()`. If you built the test suite, try
`test_key -raw`.

### 1.5 - The Ned Screen

Ned supports color by default. You can turn it off to make Ned monochrome using Theme Color on the
Options menu.

When Ned is invoked the whole screen fills with the color theme and is split into four distinct
parts, as shown below in text-only.

```
File  Edit  Search  Goto  Option  Window  Help
#include <stdio.h>

int main(void)
{
  printf("Hello World\n");
  return 0;
}

/users/drewg/Work/git/ned/src/hello.c               [*]  5:11  Tab: 2 Lf
Press Alt-Q to quit, or Alt-F to enter File Menu
```

1. The top line is the `Menu Bar`
2. The bulk of the screen, called the `Editor`, displays the current file
3. The `Status Bar` displays filename, row:col and other useful information
4. The last line is the `Message Line` and used for status messages and line-input

Access the `Menu Bar` with `Alt-F` (file menu), `Alt-D` (edit menu), `Alt-S` (search menu) etc...
Arrow keys work in the menu bar. Press `Enter` to execute the item on the menu.

Press `<Esc>` to exit the menu without doing anything. Esc gets out of anything.

As you move around in the file the current position will update in the Status Bar. Use `Ctrl-G` to
goto a line.

## 2 - Ned Key Bindings

In Ned, (nearly) any key can be mapped (or bound) to any command.

To see the entire list of commands, see menu `Help/View Key Bindings`.

To remap one or more keys, use menu `Option/Map Keys...`. Any number of keys can be mapped to any
given command. For example, some like `Ctrl-A` mapped to `CursorHome` and `Ctrl-E` to be mapped to
`CursorEnd`, as it is in a terminal prompt.

When inside of Ned menus, or when entering text in the Message Line, keys do what you expect and
are not remapped (arrows, Enter, Esc, etc...), but if you've remapped Ctrl-A to be MoveHome, then
this key will also work in the MessageLine.

What's listed below are the default key mappings. If you've played around with key mappings and got
stuck you can use the `Option/Factory Settings` command.

### 2.1 - Default Menu Keys

To quickly access a menu, press `Alt` and the highlighed letter in the Menu Bar. The `Esc` key will
get you out of the menu system without doing anything. Pressing Enter on a menu item does that
command or brings up another menu, dialog or allows you to type a field into the Message Bar. The
arrows `Left` and `Right` move to the neighboring menus. The arrows `Up` and `Down` work as
expected.

Menu Hotkey  | Description
------------ | -----------
Alt-F        | File Menu
Alt-D        | Edit Menu
Alt-S        | Search Menu
Alt-G        | Goto Menu
Alt-O        | Option Menu
Alt-W        | Window Menu
Alt-H        | Help Menu

### 2.2 - Default Exit Keys

Key      | Description
-------- | -----------
Alt-K    | Kill Ned without saving anything, not even project settings
Alt-X    | Silently save all unsaved files and Exit
Alt-Q    | Quit Ask Save (y=yes, n=no, a=save-all, o=save none)

### 2.3 - Default Cursor Keys

Ned uses a variety of keys to move the cursor around, just like most editors.

 Key       | Command          | Description
---------- | ---------------- | -----------
Up         | CursorUp         | move cursor up one line
Down       | CursorDn         | move cursor down one line
Left       | CursorLeft       | move cursor left one character
Right      | CursorRight      | move cursor right one character
Ctrl-Left  | CursorWordLeft   | move cursor a word to the left
Ctrl-Right | CursorWordRight  | move cursor a word to the right
Alt-Left   | CursorHome       | move cursor to start of line
Alt-Right  | CursorEnd        | move cursor to end of line
*Home      | CursorHome       | move cursor to start of line
*End       | CursorEnd        | move cursor to end of line
*PgUp      | CursorPgUp       | move cursor up one page
*PgDn      | CursorPgDn       | move cursor down one page
*Ctrl-Up   | MoveFunctionUp   | move cursor up one function
*Ctrl-Down | MoveFunctionDown | move cursor down one function

* Not available on macOS, but is available on Linux

And Ned has cursor alternatives due to a limited keyboard driver (especially on macOS).

 Alt-Key   | Command          | Description
---------- | ---------------- | -----------
Alt-,      | CursorScrollUp   | Scroll up a 5 lines
Alt-.      | CursorScrollDown | Scroll down 5 lines
Alt-;      | CursorPgUp       | Move cursor up one page
Alt-'      | CursorPgDp       | move cursor down one page
Alt-[      | CursorFnUp       | Move cursor up one function
Alt-]      | CursorFnDn       | Move cursor down one function
Ctrl-Space | CursorCenter     | Center screen on cursor
Alt-Space  | RedrawScreen     | If screen looks messed up, redraw it
Alt--      | CursorTop        | Move cursor to top of file
Alt-=      | CursorBottom     | Move cursor to bottom of file

Think of `Ctrl` as more, and `Alt` as a lot more (e.g. `Left Arrow` is left one character,
`Ctrl-Left` is left one word, `Alt-left` is beginning of line).

Start selecting text with `Ctrl-D` (word), `Ctrl-L` (line), or use `Ctrl-\` to toggle select. Press ESC
to get out of select mode, all of the above cursor keys will extend the selection.

### 2.4 - Default Control Key Bindings

Control characters are ASCII characters 0x00 - 0x1f (decimal 0 - 31). Below are the default hotkey
bindings for control characters.

Key        | Command            | Description
---------- | -----------------  | -----------
Ctrl-Space | MoveCenter         | Center the screen around the cursor
Ctrl-A     | EditSelectAll      | Select all text in file
Ctrl-B     | SearchBackward     | Search backward through file
Ctrl-C     | EditCopy           | Copy selected text to clipboard. Does nothing if no text selected
Ctrl-D     | SelectWord         | Select the current word 
Ctrl-E     | SearchAgain        | Repeat last search/replace
Ctrl-F     | SearchForward      | Search forward through file
Ctrl-G     | MoveGotoLine       | Go to a line number in file
Ctrl-H     | BookmarkPrev       | Go to previous bookmark
Ctrl-I     | EditIndent         | Same as Tab. Indent the 
Ctrl-J     | BookmarkNext       | Go (jump) to next bookmark
Ctrl-K     | EditCutEol         | cut to end of line
Ctrl-L     | SelectLine         | select line under cursor. If repeated, extends the selection to the next line
Enter      | Enter              | Enter new line matching indent
Ctrl-N     | FileNew            | create a new file
Ctrl-O     | FileOpen           | open a file
Ctrl-P     | MacroPlayback      | play back a macro. Also ends recording
Ctrl-Q     | MacroRecord        | Record macro. Also ends recording
Ctrl-R     | Replace            | Replace text
Ctrl-S     | FileSave           | save the current file
Ctrl-T     | EditToggleCase     | toggle between camelCase, MixedCase, snake_case, CONSTANT_CASE, lowercase and UPPERCASE
Ctrl-U     | EditCutLine        | cut line and add to clipboard
Ctrl-V     | EditPaste          | Paste from the clipboard to current cursor location
Ctrl-W     | FileClose          | Close the current file
Ctrl-X     | EditCut            | Cut currently selected text to clipboard. Does nothing if no text selected
Ctrl-Y     | EditRedo           | Redo last undo
Ctrl-Z     | EditUndo           | Undo last edit
Ctrl-]     | MoveMatchBrace     | Move to a matching brace
Ctrl-[     | Esc                | Same as Esc. Exit menu, dialog box, input line or select
Ctrl-\     | SelectToggle       | Toggle select mode
Ctrl-^     | BookmarkToggle     | Toggle bookmark
Ctrl--     | MovePosPrev        | Go back to previous position

### 2.5 Default Miscellaneous Key Bindings

Key        | Command            | Description
---------- | ------------------ | -----------
BackTab    | EditUndent         | Undent the selected lines of text
Backspace  | EditBackspace      | Delete selection or one character behind cursor
Delete     | EditDelete         | Delete selection or one character under the cursor
Alt-T      | HelpDateTime       | Display current date/time; also in clipboard for pasting
Alt-\      | MoveToDefinition   | Move to the definition of the current symbol
Alt-Space  | RedrawScreen       | Redraw everything on the screen

### 2.6 Default Function Key Bindings

Key  | Command           | Description
---- | ----------------- | -----------
Fn1  | Help              | Load this manual read-only into a buffer
Fn2  | FileOpen          | Bring up Open/File pick menu
Fn3  | SearchAgain       | Repeat last search/replace
Fn4  | SearchProject     | Find string in all project files
Fn5  | ReplaceProject    | Replace in all project files
Fn6  | SettingsTheme     | Choose a new color theme for editor
Fn7  | Sort Lines        | Sort the selected lines
Fn8  | Wrap Text         | Wrap the current paragraph or selected paragraphs
Fn9  | GotoErrPrevious   | Move to previous error (only works if loaded an error file)
Fn10 | GotoErrNext       | Move to next error (only works if loaded an error file)
Fn11 | FilePrevOpen      | Go to previous file in Window menu
Fn12 | FileNextOpen      | Go to next file in Window menu

### 2.7 Default Key Binding Quick Reference

Key        | Command          | Key         | Command           | Key        | Command          
:--------- | :--------------- | :---------- | :---------------- | :--------- | :----------------
Ctrl-Space | MoveCenter       | Ctrl-A      | SelectAll         | Ctrl-B     | SearchBackward   
Ctrl-C     | EditCopy         | Ctrl-D      | SelectWord        | Ctrl-E     | SearchAgain      
Ctrl-F     | SearchForward    | Ctrl-G      | MoveGotoLine      | Ctrl-H     | BookmarkPrev     
Tab        | EditIndent       | Ctrl-J      | BookmarkNext      | Ctrl-K     | EditCutEol       
Ctrl-L     | SelectLine       | Enter       | EditEnter         | Ctrl-N     | FileNew          
Ctrl-O     | FileOpenWildcard | Ctrl-P      | MacroPlay         | Ctrl-Q     | MacroRecord      
Ctrl-R     | Replace          | Ctrl-S      | FileSave          | Ctrl-T     | EditToggleCase   
Ctrl-U     | EditCutLine      | Ctrl-V      | EditPaste         | Ctrl-W     | FileClose        
Ctrl-X     | EditCut          | Ctrl-Y      | EditRedo          | Ctrl-Z     | EditUndo         
Esc        | SelectOff        | Ctrl-\      | SelectToggle      | Ctrl-]     | MoveMatchBrace   
Ctrl-^     | BookmarkToggle   | Ctrl--      | MovePosPrev       | Backspace  | EditBackspace    
Up         | CursorUp         | Down        | CursorDown        | Left       | CursorLeft       
Right      | CursorRight      | Home        | CursorHome        | End        | CursorEnd        
PgUp       | CursorPgUp       | PgDn        | CursorPgDn        | Ctrl-Up    | MoveFunctionUp   
Ctrl-Down  | MoveFunctionDown | Ctrl-Left   | CursorWordLeft    | Ctrl-Right | CursorWordRight  
Ctrl-Home  | CursorHome       | Ctrl-End    | CursorEnd         | Ctrl-PgUp  | CursorTop        
Ctrl-PgDn  | CursorBottom     | Alt-Up      | CursorUp          | Alt-Down   | CursorDown       
Alt-Left   | CursorHome       | Alt-Right   | CursorEnd         | Alt-Home   | CursorHome       
Alt-End    | CursorEnd        | Alt-PgUp    | CursorTop         | Alt-PgDn   | CursorBottom     
Shift-Left | CursorLeftSelect | Shift-Right | CursorRightSelect | Fn1        | Help             
Fn2        | FileOpen         | Fn3         | SearchAgain       | Fn4        | SearchProject    
Fn5        | ReplaceProject   | Fn6         | SettingsTheme     | Fn7        | EditSortLines    
Fn8        | EditWrapText     | Fn9         | MoveErrPrev       | Fn10       | MoveErrNext      
Fn11       | FilePrevOpen     | Fn12        | FileNextOpen      | Alt-A      | FileSaveAs       
Alt-B      | Debug            | Alt-C       | EditPbCopy        | Alt-D      | MenuEdit         
Alt-F      | MenuFile         | Alt-G       | MenuGoto          | Alt-H      | MenuHelp         
Alt-K      | ExitSaveNone     | Alt-O       | MenuOption        | Alt-Q      | ExitSaveAsk      
Alt-S      | MenuSearch       | Alt-T       | HelpDateTime      | Alt-V      | EditPbPaste      
Alt-W      | MenuWindow       | Alt-X       | ExitSaveAll       | Backtab    | EditUndent       
Delete     | EditDelete       | Alt--       | CursorTop         | Alt-=      | CursorBottom     
Alt-[      | MoveFunctionUp   | Alt-]       | MoveFunctionDown  | Alt-\      | MoveToDefinition 
Alt-Space  | RedrawScreen     | Alt-;       | CursorPgUp        | Alt-'      | CursorPgDn       
Alt-,      | MoveScrollUp     | Alt-.       | MoveScrollDown    | Alt-/      | EditToggleComment

99 Mapped Keys

## 3 - Ned Menu Reference

One way to access features within ned is through the menuing system. This manual assumes you're
familiar with the concept of menus.

When in the Editor, pressing Alt and the highlighted menu letter (e.g. `Alt-F` for the File Menu,
`Alt-D` for the Edit menu, or `Alt-O` for the Option Menu) will open that menu directly. Use
left/right and up/down arrows to navigate the menus.

Pressing `Enter` on an item will select that item. Press `Esc` will abort.

If you remap keys to commands, then the new hotkey for that command may be visible in the menu
system. If multiple hotkeys refer to the same command, only the 1st one will show in the menu.

If a menu item has and ellipsis `...` after it, then that means it will require some other input,
such a filename on `File/Open Wildcard...`. If the menu item has an arrow `->` after it, it means
the menu item will bring up a pick list. An example of tile is `File/Open Recent->`.

The Ned Menus are:

Menu    | Description
------- | -----------
File    | `Alt-F` Open, save files, exit, etc...
Edit    | `Alt-D` Cut, copy, paste, reformat, etc...
Search  | `Alt-S` Search and replace, etc..
Goto    | `Alt-G` Bookmarks, other movement etc...
Options | `Alt-O` Project settings, preferences
Window  | `Alt-W` List of open files 
Help    | `Alt-H` About Ned, Ned manual

### 3.1 - File Menu

The File Menu provides access to commands that deal with files: loading new files, saving files,
closing open files, and exiting Ned. Access the File Menu by pressing `Alt-F` from within the
Editor. The three "..." elipses indicate some more input is required (from a dialog box or in the
Message Area). The menu items can be select with arrow keys or the hotkey. If a command has
multiple hotkeys bound to it, only the 1st one shows up in the menu.

```
+--------------------------+
| New               Ctrl-N |
| Open->               Fn2 |
| Open Recent->      Alt-R |
| Open Wildcard...  Ctrl-O |
| Save              Ctrl-S |
| Save As            Alt-A |
| Save All                 |
| Toggle Read Only         |
| Revert                   |
| Close             Ctrl-W |
| Close All Files          |
| Quit               Alt-Q |
| Exit Save All      Alt-X |
+--------------------------+
```

`New` creates a new untitled file. The next Save will do a Save As.

`Open` Pick which file to open from a list of files. Navigates folders.

`Open Wildcard` opens one or more files using wildcards like `*.c`

`Open Recent` opens a list of recently edited/viewed files.

`Save` saves a file immediately to the same name/folder.

`Save As` allows the file to be saved under a different name/folder.

`Save All` saves all unsaved files.

`Toggle Read Only` prevents accidentally changing a file you just want to view.

`Revert` reverts the file to what's on disk.

`Close` closes the file and asks to save if changes.

`Close All Files` closes all files and asks to save if changes.

`Quit Ned` asks to save each unsaved file, then exits Ned.

`Exit Save All` saves any unsaved files silently, then exits Ned.

### 3.2 - Edit Menu

The Edit Menu contains various common editing operations. Marked text can be copied to the
clipboard. Lines of text can be indented from this menu. Access the Edit Menu by pressing Alt-D.

```
+------------------------------+
| Cut                   Ctrl-X |
| Copy                  Ctrl-C |
| Paste                 Ctrl-V |
| Undo                  Ctrl-Z |
| Redo                  Ctrl-Y |
| Select Word           Ctrl-D |
| Select Line           Ctrl-L |
| Toggle Select         Ctrl-\ |
| Sort Lines               Fn7 |
| Wrap Text                Fn8 |
| Comment Lines          Alt-/ |
| Toggle Case           Ctrl-T |
+------------------------------+
```

The `Cut`, `Copy`, `Paste` are the same everywhere. Select some text, and cut-copy-paste.

`Undo` will undo the last editing operation. `Redo` undoes the undo.

Ned doesn't have access to the shift key. So shift arrows, which normally select, aren't possible.
A quick way to select the word with `Ctrl-D` or the line with  `Ctrl-L`. Use `Toggle Select` to go
into select mode, then use any cursor or movement command, including `MoveFnNext`. Press `Esc` at
any time to exit select mode. You can tell you're selecting because the message line indicates how
many characters are selected.

`Sort Lines` allows you to sort a selected set of lines. Does nothing if no selection. Blank lines
indicate more than 1 paragraph.

`Comment Lines` toggles comments on and off for one or more lines. This is a quick, easy way to
comment out a block of code with line comments.

`Toggle Case` toggles between the common ways to name variables/functions. No need to select the
symbol, just cursor onto it. If there is a selection, will toggle all symbols to be the same as
the first symbol. Toggles between:

* lower and UPPER  -or-
* camelCase, MixedCase, snake_case, CONSTANT_CASE

`Wrap Text` allows wrapping text to the number of columns specified in the language settings.
Useful for block or line comments.

### 3.3 - Search Menu

Enter the search menu by pressing Alt-S from the Editor.

```
+-------------------------------+
| Search Again              Fn3 |
| Search Backward...     Ctrl-B |
| Search Forward...      Ctrl-F |
| Replace...             Ctrl-R |
| Project Search...         Fn4 |
| Project Replace...        Fn5 |
| Goto Definition     Alt-Space |
| Search Options...             |
+-------------------------------+
```

`Search Again` repeats the last search or replace.

`Search Forward` searches forward in the file for the text

`Search Backward` will search backward in the file(s) for the text.

`Replace` searches for and replaces text. If you accidentally replace all in a file, you can undo.

`Project Search` searches all files in the project.

`Project Replace` is the same as Replace, but for all files in the project. Be careful with this
one. It has no undo.

`Search Options` lets you set whether the next find/replace uses Case Sensitive, wrAP, or Whole
Word. Note, these can options also be set when editing the Find/Replace string. The `[CAW]` search
options are displayed on the status bar.

### 3.4 - Goto Menu

Press Alt-G from the Editor to get to the Goto Menu.

```
+-------------------------------+
| Goto Line...           Ctrl-G |
| Go Back                Ctrl-- |
| Toggle Bookmark        Ctrl-^ |
| Jump To Next Bookmark  Ctrl-J |
| Jump To Prev Bookmark  Ctrl-H |
| Clear All Bookmarks           |
| Previous Error            Fn9 |
| Next Error               Fn10 |
| Previous Open File       Fn11 |
| Next Open File           Fn12 |
| Match Brace            Ctrl-] |
+-------------------------------+
```

`Goto Line` allows you to enter a line number. If the line is greater than the number of lines in
the file, it goes to the end of the file.

`Go Back` keep a history of large cursor movements. For example if you accidentally go to the top
of the file, you can use `Go Back` to get back to where you were editing. Positions are cross file
(that is, can be in different files). Ned keeps up to 12 items in the position history and will
round robin between them.

Bookmarks are same as the position history, but chosen by the user with `Bookmark Toggle`. You can
jump forward and backward through bookmarks with `Bookmark Jump Next` and `Bookmark Jump Prev`.
across a set of files or within a large file. Bookmarks are remembered across exit of Ned, so that
you can continue to code where you left off. Ned keeps up to 12 bookmarks.

`Bookmark Clear All` does just what is says. Lets you start with a clean slate rather than
toggling them off one by one.

`Next Error` and `Previous Error` are useful when compiling a programs with gcc or using linters
like Python's flake8. Just pipe the output of your compiler to a file and then run Ned with the -e
option. The key Fn10 will then go down your error list one by one. All the files with errors or
warnings are automatically loaded.

```
$ gcc -Wall file1.c file2.c -o test 2>err.txt
$ ned -e err.txt

$ flake8 turtle*.py >err.txt
$ ned -e err.txt
```

`Next Open File` goes to the next file in the Window menu, round robin style.

`Match Brace` finds the matching `( )` or `{ }`, `< >` or `[ ]`. The cursor must be on or just
after the brace.

See commands MoveGotoLine, MovePosPrev, BookmarkToggle, BookmarkJumpNext, BookmarkJumpPrev,
BookmarkClearAll, MoveErrPrev, MoveErrNext, MoveNextOpen and MoveMatchBrace for more
information.

### 3.5 - Option Menu

The Option Menu contains a variety of advanced functions and settings. Press Alt-O to reach the
options menu directly from the Editor.

```
+---------------------------+
| Redraw Screen         Fn2 |
| Record Macro       Ctrl-Q |
| Play Macro         Ctrl-P |
| Map Keys->                |
| Color Theme...        Fn6 |
| Edit Settings             |
| Tab Settings...           |
| Line Endings...           |
| Wrap Length...            |
| Project Folders->         |
| Factory Settings          |
+---------------------------+
```

`Redraw` will redraw the screen. Use this if resizing the terminal window or if something is
corrupted.

Macros are simply keystroke playback. Use `Record Macro` or `Ctrl-Q` (think quote) to both
start/stop recording of keystrokes. Then play them back with `Play Macro`, or `Ctrl-P`
(think play). Useful for many repetative editing activities.

What hotkeys do in an editor can be very personal. `Map Keys` allows you to quickly set hotkeys to
your liking. For example, if you want `Ctrl-A` to go to the beginning of a line (CursorHome), and
`Ctrl-E` to go to the end of a line (CursorEnd), like they do on the command-line, you could remap
these from the default SelectAll and SearchAgain respectively.

`Color Theme` adjusts the editor colors to pre-defined color schemes. You can make up new color
themes if you edit settings.

`Edit Settings` allows you to view the project file that would be saved at this moment. This is a
read-only file by default. Don't modify it unless you know what you're doing.

`Project Folders` lets you specify a set of files to search when doing a Project Search and/or
Replace or using MoveToDefiniton, which finds the function definition quickly. By default, the
search will happen wherever the `ned.json` project file is stored.

`Factory Settings` resets all the settings back to factory defaults. Doesn't remove bookmarks and
the like, but all the keys, color theme, language settings, etc... are back to Ned original.

### 3.6 Window Menu

Contains a list of open files. If two files have the same name, then their respective folders
(perhaps shortened with ...) will be included in the list. For example:

```
+-----------------------------+
| file.c                      |
| /home/user/git/proj/test.py |
| ...ser/git/proj/src/test.py |
+-----------------------------+
```

### 3.7 Help Menu

Enter the Help Menu with `Alt-H` from the Editor. The Help Menu shows Ned version and provides
access to the Ned Manual (this document).

```
+--------------------------+
| About Ned                |
| Ned Manual           Fn1 |
| View Key Bindings        |
| Date and Time      Alt-T |
+--------------------------+
```

`About Ned` simply brings up a dialog box with info about the Ned Version.

`Ned Manual` brings up the built-in manual for Ned (this manual).

Choose `View Key Bindings` to view all the key bindings in a read-only file. This is built
on-the-fly, so if you remap keys, they will be reflected when you choose this option again.

all key bindings in a table.`Date and Time` is a quick way to check the date/time, but also is added to the Paste Buffer for
quick insertion into text with `Ctrl-V`

## 4 - Ned Status Bar Reference

The Ned status bar provides a variety of information. Left to right, those fields are:

```
path\to\file    [searchOpts][fileStatus]   line:col  Ned v2.0
```

The search options are only visible when searching or replacing. They are, in order: Case
sensitive, wrAp, Whole Word, or `[CAW]`. File status will be `[*]` to indicate the file is
modified, or `[R]` to indicate file is read-only.

For example:

```
/~/Work/git/ned/src/hello.c      [CAW][*]  1234:123  Ned v2.0
Find: a string
```

## 5 - Ned Command Reference

This section describes all of the commands available from within ned, listed alphabetically.

Use Options/Map Keys..." to map hotkeys to commands.

### 5.1 - Ned Command List

Command          | Command           | Command          | Command        | Command          
:--------------- | :---------------- | :--------------- | :------------- | :----------------
BookmarkClearAll | BookmarkNext      | BookmarkPrev     | BookmarkToggle | CursorUp         
CursorDown       | CursorLeft        | CursorLeftSelect | CursorRight    | CursorRightSelect
CursorTop        | CursorBottom      | CursorHome       | CursorEnd      | CursorPgUp       
CursorPgDn       | CursorWordLeft    | CursorWordRight  | Debug          | DoNothing        
EditBackspace    | EditCopy          | EditCut          | EditCutEol     | EditCutLine      
EditCutWord      | EditDelete        | EditEnter        | EditIndent     | EditInsertSelf   
EditPaste        | EditPbCopy        | EditPbPaste      | EditRedo       | EditSortLines    
EditToggleCase   | EditToggleComment | EditUndent       | EditUndo       | EditWrapText     
ExitSaveAll      | ExitSaveAsk       | ExitSaveNone     | FileClose      | FileCloseAll     
FileNew          | FileNextOpen      | FilePrevOpen     | FileOpen       | FileOpenRecent   
FileOpenWildcard | FileRevert        | FileSave         | FileSaveAs     | FileSaveAll      
FileToggleRdOnly | Help              | HelpAbout        | HelpDateTime   | MacroRecord      
MacroPlay        | MenuFile          | MenuEdit         | MenuSearch     | MenuGoto         
MenuOption       | MenuWindow        | MenuHelp         | MoveCenter     | MoveErrNext      
MoveErrPrev      | MoveFunctionDown  | MoveFunctionUp   | MoveGotoLine   | MoveMatchBrace   
MovePosNext      | MovePosPrev       | MoveScrollUp     | MoveScrollDown | MoveToDefinition 
ProjectFolders   | RedrawScreen      | Replace          | ReplaceProject | SearchAgain      
SearchBackward   | SearchForward     | SearchOptions    | SearchProject  | SelectAll        
SelectLine       | SelectOff         | SelectToggle     | SelectWord     | SettingsCrLf     
SettingsEdit     | SettingsFactory   | SettingsMapKeys  | SettingsTab    | SettingsTheme    
SettingsWrap     | ViewKeyBindings   | 
total commands 102

### 5.2 - Ned Command Description

Each command is described in more detail below.

#### 5.2.1 - BookmarkClearAll

This simply clears all the bookmarks. Bookmarks are across all open files. If a file is closed, any
bookmarks in that file are automatically cleared. Bookmarks are preserved in the project file.

#### 5.2.2 - BookmarkNext

Go to the next bookmark in round robin fashion, which may be in a different file.

#### 5.2.3 - BookmarkPrev

Go to the previous bookmark in round robin fashion, which may be in a different file.

#### 5.2.4 - BookmarkToggle

Bookmarks give quick access to various files and locations you're working on. Toggle them on/off.

#### 5.2.5 - MoveCenter

This will center the text vertically on the screen if the file is long enough. Doesn't change
cursor position, just potentially which line is at the top of screen.

#### 5.2.6 - CursorXxx

No need to explain cursor keys. You already know.

#### 5.2.7 - EditBackspace, EditDelete

EditBackspace deletes character prior to cursor, whereas EditDelete deletes the character under the
cursor. Both delete the whole selection if 1 or more characters selected.

#### 5.2.8 - EditCopy, EditCut, EditPaste

No need to explain cut, copy, paste. You already know.

#### 5.2.9 - EditCutEol, EditCutLine, EditCutWord

Sometimes quicker than selecting then delete, simply cut to end of line, the whole line or the
word. Repeating will append to the paste buffer. Do undo cutting, either paste or use undo.

#### 5.2.10 - EditEnter

Enter is smart, and that it preserves the indicate of the line you're on.

#### 5.2.11 - EditIndent, EditUndent

The tab and back-tab keys indent or undent the current line or selected lines of text. The tab key
also just inserts a tab's worth of spaces or a single tab, depending on settings for this file extension.

#### 5.2.12 - EditInsertSelf

This is used for every ASCII key from space through tilde. It just inserts itself. If you have a
selection, that selection will be replaced by the one key. All very standard editor stuff.

#### 5.2.13 - EditPbCopy, EditPbPaste

In order to cut-and-paste with other programs, or between two copies of ned running at the same
time, use `Alt-C` and `Alt-V` rather than the `Ctrl-C` and `Ctrl-V` use for normal copy/paste.
This requires that your terminal support pbcopy and pbpaste tools.

#### 5.2.14 - EditUndo, EditRedo

Ned keeps track of all edits as a series of inserts and deletes. It simply replays these in reverse
for undo. It plays them in the forward direction again with redo. An Undo is defined by a series
of edits, followed by a non-edit command, such as a cursor key.

#### 5.2.15 - EditSortLines

Sorts selected lines, case sensitive.

#### 5.2.16 - EditToggleCase

Toggles case of current symbol between

* lower, UPPER -or-
* camelCase, MixedCase, snake_case, CONSTANT_CASE

#### 5.2.17 - EditToggleComment

Toggles line comments on/off for the selected lines. Preserved indent.

#### 5.2.18 - EditWrapText

Wraps text at the # of columns set by the language file extension. That is, .py files could wrap at
column 79, and .c files at 100.

Useful to re-wrap block or line coments. Preserves indent.

#### 5.2.19 - ExitSaveAll, ExitSaveAsk, ExitSaveNone

The only commands that exit Ned. Save all will quietly save any unsaved files before exit. If a
file can't be saved (due to permissions, folder is now gone, etc...), then it will ask to SaveAS.

ExitSaveAsk ask to save each unsaved file.

ExitSaveNone does just that. To prevent data loss, it asks (are you sure) if anything is unsaved.

#### 5.2.20 - FileClose

Closes the currently open file. Will ask to save if unsaved.

#### 5.2.21 - FileCloseAll

Closes all other files except the currently open file. Will ask to save if any unsaved files.

#### 5.2.22 - FileNew

Creates an empty file with the same file extension and location as the current file, but with a
unique name.

#### 5.2.23 - FileNextOpen, FilePrevOpen

Quick way to navigate open files. You can also use the Window menu.

#### 5.2.24 - FileOpen, FileOpenRecent, FileOpenWildcard

FileOpen picks from a picklist of files, an easy way to find and files in folders.

FileOpenRecent brings up a picklist of files you opened recently.

FileOpenWildcard lets you type in a file as you would from the command-line. Tab completion also
works. For example, if the home folder contains "Dewsktop/", "Documents/", "Dodo.txt",
"Downloads/", you can toggle between them by typing "~/D" then tab.

#### 5.2.25 - FileRevert

Revert the file to whatever is saved on disk.

#### 5.2.26 - FileSave, FileSaveAs, FileSaveAll

FileSave saves the current file. If the folder was removed where the file was loaded from, or has
read-only permissions, gives the opportunity to SaveAs.

FileSaveAs lets you change the name, location of the file. Doesn't affect the original.

FileSaveAll quietly saves all unsaved files. Again SaveAs used if problems saving.

#### 5.2.27 - FileToggleRdOnly

Sometimes it's useful to view a file and not accidently alter it. The StatusBar will indicate `[R]`
if read-only mode.

#### 5.2.28 - Help, HelpAbout

Help brings up this manual. You can alter and save it where your project file resides if you want.

#### 5.2.29 - HelpDateTime

Just displays current date/time in ISO format: 2021-11-05T13:22:07. 

It also puts this string in the the paste buffer. Why is this in ned? I don't know.

#### 5.2.30 - MacroRecord, MacroPlay

Press `Ctrl-Q` to quote (or start/stop recording). Press `Ctrl-P` to playback the keystrokes just
recorded. Really useful for repetative editing.

#### 5.2.31 - MenuFile, MenuEdit, MenuSearch, MenuGoto, MenuOption, MenuWindow, MenuHelp

By default, these commands are bound to `Alt-F`, `Alt-D`, `Alt-S`, `Alt-G`, `Alt-O`, `Alt-W`,
`Alt-H`. Used as quick-hotkeys to get the menu system.

#### 5.2.32 - MoveCenter

Centers the cursor vertically on the screen if possible. If the line is very long, then will center
left-to-right as well. Doesn't move the cursor, just what's visible.

#### 5.2.33 - MoveErrNext, MoveErrPrev

Use during edit/compile/debug cycle. Simply pipe the compiler output to a file, then edit with
the -e option. You can go to next/previous error across any set of files. Example:

```
$ gcc -Wall file1.c file2.c -o my_program 2>err.txt
$ ned -e err.txt
```

If any errors or warnings are encounted, then err.txt will contain them, and they will be loaded
into ned. Navigate next/previous errors with `Fn9` and `Fn10`.

Error format is standarsized in the industry to `filename:line:column:description`.

```
file1.c:6:8: warning: using the result of an assignment as a condition without parentheses
file2.c:19:27: error: use of undeclared identifier 'x'
```

This even works with linters like flake8 for Python.

```
makemaze.py:3:1: W293 blank line contains whitespace
makemaze.py:4:1: E302 expected 2 blank lines, found 1
```

#### 5.2.34 - MoveFunctionDown, MoveFunctionUp

The definition of a function line depends on the the language. These are built-in to ned for all
the common languages. Quick way to navigate a file by class or function.

#### 5.2.35 - MoveGotoLine

Goto a line number. The top line in a file is 1.

#### 5.2.36 - MoveMatchBrace

Moves the cursor to the matching brace `({[< >]})`. Is counting, so handles nested braces.

Doesn't handle quotes. Not smart about comments, this is a purely smart text search.

#### 5.2.37 - MovePosNext, MovePosPrev

Ned keeps track of the file positions you've been to while editing. Go back, or MovePosPrev, is so
useful. Let's say your just opened a new file, just press `Ctrl--` (the minus key) to go back to
the previous file. Let's say you accidently went to the top of the file, press `Ctrl--` to get
back to your previous position.

#### 5.2.38 - MoveScrollUp, MoveScrollDown

When reading a source file, it's nice to scroll through a few lines at a time.

#### 5.2.39 - MoveToDefinition 

While `Ctrl-Up` and `Ctrl-Down` move up or down one function or class, the MoveToDefinition command
searches project folders for that function or class, and moves to it if found. Bound to `Alt-\` by
default. Use this as a quick way to navigate a project.

See the command `ProjectFolders` to set up which folders so search. If no folders are set up, then
Ned will use the `ned.json` folder, where the file project is stored as the root. If not using
project files at all, then Ned will use the current directory `.` as the root folder for searching.

#### 5.2.40 - ProjectFolders

Ned automatically will search all folders and subfolders of from where the project file is stored
when using ProjectSearch or ProjectReplace.

If you've used `ned -p .` this would be the root of your git repository. If you haven't made
separate ned project files, then this could be your $HOME folder, which means Ned would search a
LOT of files.

Sometimes you don't want to search all the folders in a project, or you want to search outside the
project's main tree (for example, if it depends on multiple git repos).

Specify a list of folders to search on ProjectSearch and Replace with this command. Or edit the
`ned.json` project file.

#### 5.2.41 - RedrawScreen

Screen messed up? Redraw it with `Alt-Space`. Forces a redraw of everything.

#### 5.2.42 - Replace

Search and replace one item at a time, or all items in this file. Search or replace strings cannot
span multiple lines.

#### 5.2.43 - ReplaceProject

Search and replace one item at a time or all items across many files. Uses recursive list of
project folders.

CAREFUL! There is no undo for this action!

#### 5.2.44 - SearchForward, SearchBackward, SearchAgain

Search forward, backward or repeat the search. Search options include Case Sensitive, wrAp, and
Whole Word, also known as `[CAW]` in ned. Use `Ctrl-A`, `Ctrl-A`, `Ctrl-W` to toggle options.

Sorry, no regular expressions or multi-line searches.

#### 5.2.45 - SearchOptions

If you forget that you can press `Ctrl-A`, `Ctrl-A`, `Ctrl-W` to change search options, you can get
to those options via a menu command.

#### 5.2.46 - SearchProject

Search for a string across many files. Uses recursive list of project folders.

#### 5.2.47 - SelectAll, SelectLine, SelectOff, SelectToggle, SelectWord

Standard selecting text in file for cut, sorting, wrapping, etc...

Selecting anything stays in select mode until `Esc` is pressed or some action such as cut deletes
the selected text.

You can tell what text is selected by the highlight.

#### 5.2.48 - SettingsBindings

See `Option/View Key Bindings` menu item. Places all the current key/command bindings(hotkeys) into
markdown table format in a file called `nedkeymap.md`. This can then be saved, edited like normal
text.

#### 5.2.49 - SettingsCrLf

Linux and macOs use `\n` for line endings. Windows uses `\r\n` for line endings. Some file types,
like .html, must always have `\r\n` line endings. Ned it set up to do this automatically.

Ned auto-detects line endings when opening a file. When creating a new file, it uses the langauge
settings for that file type.

This command let's you modify the line endings for this file or for all files of this type.

Note: this only affects files that are modified saved by ned. It does not affect other files on
disk.

#### 5.2.50 - SettingsEdit

Brings up the project `ned.json`, or as the project would be if saved with the current open files
and settings.

You can edit this manually, but if you do, make sure it remains valid .json and that you save
`Ctrl-S`, then quit without saving project file via ExitNoSave, or kill, via `Alt-K`.

#### 5.2.51 - SettingsFactory

Resets all settings to factory defaults. You can also do this by just deleting the `ned.json`
project file.

#### 5.2.52 - SettingsMapKeys

Map one or more keys to one or more commands. Uses a pick list of all commands.

#### 5.2.53 - SettingsTab

Ned auto-detects tab settings when loading a file. When creating a new file, the tab settings are
on a per-language basis.

Some files require particular tab settings. For example, makefiles require hard-tabs `\t`. Python
PEP8 specifies tab-indents of 4 spaces. The ned team uses .c tab-indents of 2 spaces.

The argument of tabs vs spaces is a long one that will probably never be resolved.

[See spaces-vs-tabs](https://thenewstack.io/spaces-vs-tabs-a-20-year-debate-and-now-this-what-the-hell-is-wrong-with-go/)

This command let's you modify the tab settings for this file or for all files of this type.

Note: this only affects files that are modified saved by ned. It does not affect other files on
disk.

#### 5.2.54 - SettingsTheme

Ned has a variety of color themes, including, yes, Dark Mode.

Ned can keep track of up to 12 themes. Six (6) are built-in. They are: "Classic" (original Ned
color), "Dark Mode", "Natural" (text color is same as terminal color), "Monochrome"
(all grey-scale), "1980s" (like 1980s green terminal), and "Zero" (no color at all).

To define your own theme(s), see "userThemes" section of the ned.json file in Section 6 - Ned
Project Files.

Run `termcolors` to see what the colors are available in your terminal. Each color number is 0-255.
The "colors" in order are:

  * text - normal text
  * textHighlight - highlighted text
  * comment - language comments both block and line
  * keyword - language keywords
  * constant - constants like 0 or "hello"
  * menuText - color of menu text and frame
  * menuHotkey - color of hot key in menu text
  * menuDropText - color of selected menu item text
  * menuDropHotkey - hot key color of selected menu item
  * message - message line text

#### 5.2.55 - SettingsWrap

Choose at what column the command `EditWrapText` will wrap for the given file extension. For
example, PEP8 for Python says at 79. Modern screens can easily handle 100 - 160.

Ned doesn't autodetect this because source code often has outliers.

#### 5.2.56 - ViewKeyBindigns

Creates a pseudo markdown file with a table that lists which keys are bound to which commands.

## 6 Ned Project Files

The project file is an optional feature of Ned. Project files are the way ned remembers the last
session, including what files were open, bookmarks, currently selected color them

You can enable it by using the `-p <folder>`
option. For example, the following will create the project file in the current folder.

```
$ ned -p . myfile.c
```

The project file created is called `ned.json` in the folder specified. When ned launches without
the `-p` option, it looks in the current folder, then every parent folder up to root for a project
file to use.

The following settings are stored in the project file

1. A list of open files, the position within those files, and whether they are currently read-only
2. A list of recently opened files
3. Bookmark locations
4. The current color theme
5. Any user defined color themes
6. A list of folders to search for Search/Project
7. Any resassigned key/command pairs
8. Any modified tab, line ending and keyword settings for each programming language

Project files are stored in JSON format. See [Introducing JSON](www.json.org). Invalid JSON files
or those without the proper version are ignored.

An example project file:

```json
{
    "nedVersion": 200,
    "curFile": 1,
    "recentFiles": [
        "test.c",
        "~/Work/Git/file.c",
        "file3.h"
    ],
    "projectFolders": [
        "~/Work/Git/ned"
    ],
    "theme": "Natural",
    "userThemes": [
        {
            "name": "Grass",
            "colors": [
                39,
                114,
                47,
                46,
                43,
                63,
                58,
                47,
                43,
                47
            ]
        }
    ],
    "openFiles": [
        {
            "file": "/Users/drewg/Work/Git/ned/test/tcdata_Settings.json",
            "pos": 99,
            "topLine": 1,
            "leftCol": 0,
            "prefCol": 16,
            "rdOnly": false
        },
        {
            "file": "/Users/drewg/Work/Git/ned/test/NedTestSettings.c",
            "pos": 133,
            "topLine": 1,
            "leftCol": 2,
            "prefCol": 24,
            "rdOnly": true
        }
    ],
    "bookmarks": [
        {
            "idx": 1,
            "pos": 99
        },
        {
            "idx": 0,
            "pos": 200
        }
    ],
    "keyMap": [
        {
            "key": "Ctrl-G",
            "cmd": "EditCutWord"
        }
    ],
    "language": [
        {
            "lang": "C",
            "ext": ".c .h",
            "tabSize": 2,
            "hardTabs": false,
            "crLf": false,
            "wrapLen": 120
        },
        {
            "lang": "C++",
            "ext": ".cpp .hpp .cc .hh",
            "tabSize": 2,
            "hardTabs": false,
            "crLf": false,
            "wrapLen": 120
        },
        {
            "lang": "Html",
            "ext": ".html .htm",
            "tabSize": 2,
            "hardTabs": false,
            "crLf": true,
            "wrapLen": 120
        },
        {
            "lang": "Java",
            "ext": ".java",
            "tabSize": 4,
            "hardTabs": false,
            "crLf": false,
            "wrapLen": 72
        },
        {
            "lang": "JavaScript",
            "ext": ".js .css",
            "tabSize": 2,
            "hardTabs": false,
            "crLf": false,
            "wrapLen": 120
        },
        {
            "lang": "JSON",
            "ext": ".json",
            "tabSize": 2,
            "hardTabs": false,
            "crLf": false,
            "wrapLen": 120
        },
        {
            "lang": "Makefile",
            "ext": "makefile Makefile .mak",
            "tabSize": 2,
            "hardTabs": true,
            "crLf": false,
            "wrapLen": 120
        },
        {
            "lang": "Python",
            "ext": ".py",
            "tabSize": 4,
            "hardTabs": false,
            "crLf": false,
            "wrapLen": 79
        },
        {
            "lang": "Ruby",
            "ext": ".rb",
            "tabSize": 3,
            "hardTabs": false,
            "crLf": false,
            "wrapLen": 80,
            "keywords": [
                "BEGIN",
                "END",
                "__LINE__"
            ]
        },
        {
            "lang": "Rust",
            "ext": ".rs",
            "tabSize": 4,
            "hardTabs": false,
            "crLf": false,
            "wrapLen": 100
        },
        {
            "lang": "Shell",
            "ext": ".sh .bash .zsh .bat",
            "tabSize": 2,
            "hardTabs": false,
            "crLf": false,
            "wrapLen": 120
        },
        {
            "lang": "Swift",
            "ext": ".swift",
            "tabSize": 2,
            "hardTabs": false,
            "crLf": false,
            "wrapLen": 120
        },
        {
            "lang": "Text",
            "ext": ".txt .md .markdown",
            "tabSize": 2,
            "hardTabs": false,
            "crLf": false,
            "wrapLen": 100
        },
        {
            "lang": "Foo",
            "ext": ".foo .bar .baz",
            "tabSize": 4,
            "hardTabs": false,
            "crLf": true,
            "wrapLen": 120,
            "lineComments": [
                "~~"
            ],
            "blockComments": [
                "<~~",
                "~~>"
            ],
            "keywords": [
                "foo",
                "bar",
                "baz",
                "return"
            ]
        }
    ]
}
```

For built-in languages, ned only saves the "small" fields like "tabSize" and "ext", but doesn't
save the comments or keywords unless changed from what is hard-coded into ned.

To edit a project file manually in ned simply open it like a normal file, save it, then press
`Alt-K` to kill ned without overwriting the project file.
