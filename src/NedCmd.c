/*!************************************************************************************************
  @file NedCmd.c

  @brief  Ned Command Implementation

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  All commands are prototyped in NedCmd.h.

  To keep it organized (and # of lines per module down), the various command groups have their own
  source files. For example, bookmark type things can be found in NedCmdBookmark.c.

  The few odd-ball commands are including in this file.

*///***********************************************************************************************
#include "NedCmd.h"

typedef struct
{
  const char     *szName;
  pfnNedCmd_t     pfnCmd;
  nedCmdFlags_t   flags;
} sNedCmdList_t;

// list of commands and their functions alphabetically
static const sNedCmdList_t maNedCmdList[] =
{
  { "BookmarkClearAll",     NedCmdBookmarkClearAll },
  { "BookmarkPrev",         NedCmdBookmarkPrev },
  { "BookmarkNext",         NedCmdBookmarkNext },
  { "BookmarkToggle",       NedCmdBookmarkToggle },

  { "CursorUp",             NedCmdCursorUp },
  { "CursorDown",           NedCmdCursorDown },
  { "CursorLeft",           NedCmdCursorLeft },
  { "CursorLeftSelect",     NedCmdCursorLeftSelect },
  { "CursorRight",          NedCmdCursorRight },
  { "CursorRightSelect",    NedCmdCursorRightSelect },
  { "CursorTop",            NedCmdCursorTop },
  { "CursorBottom",         NedCmdCursorBottom },
  { "CursorHome",           NedCmdCursorHome },
  { "CursorEnd",            NedCmdCursorEnd },
  { "CursorPgUp",           NedCmdCursorPgUp },
  { "CursorPgDn",           NedCmdCursorPgDn },
  { "CursorWordLeft",       NedCmdCursorWordLeft },
  { "CursorWordRight",      NedCmdCursorWordRight },

  { "Debug",                NedCmdDebug },
  { "DoNothing",            NedCmdDoNothing },

  { "EditBackspace",        NedCmdEditBackspace,      NEDCMD_FLAG_EDIT | NEDCMD_FLAG_SELECT },
  { "EditCopy",             NedCmdEditCopy,           NEDCMD_FLAG_EDIT | NEDCMD_FLAG_SELECT },
  { "EditCut",              NedCmdEditCut,            NEDCMD_FLAG_EDIT },
  { "EditCutEol",           NedCmdEditCutEol,         NEDCMD_FLAG_EDIT | NEDCMD_FLAG_CUT },
  { "EditCutLine",          NedCmdEditCutLine,        NEDCMD_FLAG_EDIT | NEDCMD_FLAG_CUT },
  { "EditCutWord",          NedCmdEditCutWord,        NEDCMD_FLAG_EDIT | NEDCMD_FLAG_CUT },
  { "EditDelete",           NedCmdEditDelete,         NEDCMD_FLAG_EDIT | NEDCMD_FLAG_SELECT },
  { "EditEnter",            NedCmdEditEnter,          NEDCMD_FLAG_EDIT },
  { "EditIndent",           NedCmdEditIndent,         NEDCMD_FLAG_EDIT | NEDCMD_FLAG_SELECT},
  { "EditInsertSelf",       NedCmdEditInsertSelf,     NEDCMD_FLAG_EDIT | NEDCMD_FLAG_SELECT },
  { "EditPaste",            NedCmdEditPaste,          NEDCMD_FLAG_EDIT | NEDCMD_FLAG_SELECT },
  { "EditPbCopy",           NedCmdEditPbCopy,         NEDCMD_FLAG_EDIT | NEDCMD_FLAG_SELECT },
  { "EditPbPaste",          NedCmdEditPbPaste,        NEDCMD_FLAG_EDIT | NEDCMD_FLAG_SELECT },
  { "EditRedo",             NedCmdEditRedo,           NEDCMD_FLAG_EDIT },
  { "EditSortLines",        NedCmdEditSortLines,      NEDCMD_FLAG_EDIT | NEDCMD_FLAG_SELECT },
  { "EditToggleCase",       NedCmdEditToggleCase,     NEDCMD_FLAG_EDIT | NEDCMD_FLAG_SELECT },
  { "EditToggleComment",    NedCmdEditToggleComment,  NEDCMD_FLAG_EDIT | NEDCMD_FLAG_SELECT },
  { "EditUndent",           NedCmdEditUndent,         NEDCMD_FLAG_EDIT | NEDCMD_FLAG_SELECT },
  { "EditUndo",             NedCmdEditUndo,           NEDCMD_FLAG_EDIT },
  { "EditWrapText",         NedCmdEditWrapText,       NEDCMD_FLAG_EDIT | NEDCMD_FLAG_SELECT },

  { "ExitSaveAll",          NedCmdExitSaveAll },
  { "ExitSaveAsk",          NedCmdExitSaveAsk },
  { "ExitSaveNone",         NedCmdExitSaveNone },

  { "FileClose",            NedCmdFileClose },
  { "FileCloseAll",         NedCmdFileCloseAll },
  { "FileNew",              NedCmdFileNew },
  { "FileNextOpen",         NedCmdFileNextOpen },
  { "FilePrevOpen",         NedCmdFilePrevOpen },
  { "FileOpen",             NedCmdFileOpen },
  { "FileOpenRecent",       NedCmdFileOpenRecent },
  { "FileOpenWildcard",     NedCmdFileOpenWildcard },
  { "FileRevert",           NedCmdFileRevert,         NEDCMD_FLAG_EDIT },
  { "FileSave",             NedCmdFileSave },
  { "FileSaveAs",           NedCmdFileSaveAs },
  { "FileSaveAll",          NedCmdFileSaveAll },
  { "FileToggleRdOnly",     NedCmdFileToggleRdOnly },

  { "Help",                 NedCmdHelp },
  { "HelpAbout",            NedCmdHelpAbout },
  { "HelpDateTime",         NedCmdHelpDateTime },

  { "MacroRecord",          NedCmdMacroRecord },
  { "MacroPlay",            NedCmdMacroPlay,          NEDCMD_FLAG_EDIT | NEDCMD_FLAG_SELECT },

  { "MenuFile",             NedCmdMenuFile,           NEDCMD_FLAG_NO_OP },
  { "MenuEdit",             NedCmdMenuEdit,           NEDCMD_FLAG_NO_OP },
  { "MenuSearch",           NedCmdMenuSearch,         NEDCMD_FLAG_NO_OP },
  { "MenuGoto",             NedCmdMenuGoto,           NEDCMD_FLAG_NO_OP },
  { "MenuOption",           NedCmdMenuOption,         NEDCMD_FLAG_NO_OP },
  { "MenuWindow",           NedCmdMenuWindow,         NEDCMD_FLAG_NO_OP },
  { "MenuHelp",             NedCmdMenuHelp,           NEDCMD_FLAG_NO_OP },

  { "MoveCenter",           NedCmdMoveCenter },
  { "MoveErrNext",          NedCmdMoveErrNext },
  { "MoveErrPrev",          NedCmdMoveErrPrev },
  { "MoveFunctionDown",     NedCmdMoveFunctionDown },
  { "MoveFunctionUp",       NedCmdMoveFunctionUp },
  { "MoveGotoLine",         NedCmdMoveGotoLine },
  { "MoveMatchBrace",       NedCmdMoveMatchBrace },
  { "MovePosNext",          NedCmdMovePosNext },
  { "MovePosPrev",          NedCmdMovePosPrev },
  { "MoveScrollDown",       NedCmdMoveScrollDown },
  { "MoveScrollUp",         NedCmdMoveScrollUp },
  { "MoveToDefinition",     NedCmdMoveToDefinition },

  { "ProjectFolders",       NedCmdProjectFolders },
  { "RedrawScreen",         NedCmdRedrawScreen },

  { "Replace",              NedCmdReplace,            NEDCMD_FLAG_EDIT },
  { "ReplaceProject",       NedCmdReplaceProject,     NEDCMD_FLAG_EDIT },
  { "SearchAgain",          NedCmdSearchAgain },
  { "SearchBackward",       NedCmdSearchBackward },
  { "SearchForward",        NedCmdSearchForward },
  { "SearchOptions",        NedCmdSearchOptions },
  { "SearchProject",        NedCmdSearchProject },

  { "SelectAll",            NedCmdSelectAll },
  { "SelectLine",           NedCmdSelectLine },
  { "SelectOff",            NedCmdSelectOff },
  { "SelectToggle",         NedCmdSelectToggle },
  { "SelectWord",           NedCmdSelectWord },

  { "SettingsCrLf",         NedCmdSettingsCrLf },
  { "SettingsEdit",         NedCmdSettingsEdit },
  { "SettingsFactory",      NedCmdSettingsFactory,    NEDCMD_FLAG_EDIT },
  { "SettingsMapKeys",      NedCmdSettingsMapKeys },
  { "SettingsTab",          NedCmdSettingsTab },
  { "SettingsTheme",        NedCmdSettingsTheme },
  { "SettingsWrap",         NedCmdSettingsWrap },
  { "ViewKeyBindings",      NedCmdViewKeyBindings }
};


/*!------------------------------------------------------------------------------------------------
  Returns a ptr to the command based on match of the name.

  @ingroup  ned_edit
  @param    szName       pointer to a string.

  @return   Ptr to command, or NULL if not found.
*///-----------------------------------------------------------------------------------------------
pfnNedCmd_t NedCmdGetCommand(const char *szName)
{
  pfnNedCmd_t   pfnCmd = NULL;
  unsigned      i;

  if(szName)
  {
    for(i=0; i<NumElements(maNedCmdList); ++i)
    {
      if( strcmp(maNedCmdList[i].szName, szName) == 0 )
      {
        pfnCmd = maNedCmdList[i].pfnCmd;
        break;
      }
    }
  }
  return pfnCmd;
}

/*!------------------------------------------------------------------------------------------------
  Returns the command name based on a ::pfnNedCmd_t

  @ingroup  ned_edit
  @param    pfnCmd       pointer to command.

  @return   ptr to name, or NULL if not found.
*///-----------------------------------------------------------------------------------------------
const char * NedCmdGetName(pfnNedCmd_t pfnCmd)
{
  const char   *szName = NULL;
  unsigned      i;

  if(pfnCmd)
  {
    for(i=0; i<NumElements(maNedCmdList); ++i)
    {
      if(maNedCmdList[i].pfnCmd == pfnCmd)
      {
        szName = maNedCmdList[i].szName;
        break;
      }
    }
  }

  return szName;
}

/*!------------------------------------------------------------------------------------------------
  Get the flags of the command

  @ingroup  ned_edit
  @param    pfnCmd    pointer to a command

  @return   bitmask of flags for the command
*///-----------------------------------------------------------------------------------------------
nedCmdFlags_t NedCmdFlags(pfnNedCmd_t pfnCmd)
{
  unsigned        i;
  nedCmdFlags_t   flags = 0;

  if(pfnCmd)
  {
    for(i=0; i<NumElements(maNedCmdList); ++i)
    {
      if(maNedCmdList[i].pfnCmd == pfnCmd)
      {
        flags = maNedCmdList[i].flags;
        break;
      }
    }
  }

  return flags;
}

/*!------------------------------------------------------------------------------------------------
  Is one or more of these flags set?

  @ingroup  ned_edit
  @param    pfnCmd    pointer to a command
  @param    isFlags   bitmask of flags

  @return   TRUE if one or more flags match, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedCmdIs(pfnNedCmd_t pfnCmd, unsigned isFlags)
{
  return (NedCmdFlags(pfnCmd) & isFlags) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Return a list of pointers to all commands.

  @ingroup  ned_edit
  @param    pNuMCmds    return parameter, the # of commands

  @return   array of pointers to strings
*///-----------------------------------------------------------------------------------------------
const char **NedCmdGetAllCommands(unsigned *pNumCmds)
{
  static const char **ppCmds = NULL;  // static, never changes after first time called
  unsigned            i;
  unsigned            numCmds = NumElements(maNedCmdList);

  if(ppCmds == NULL)
  {
    ppCmds = malloc(numCmds * sizeof(char *));
    for(i=0; i<numCmds; ++i)
      ppCmds[i] = maNedCmdList[i].szName;
  }

  if(pNumCmds)
    *pNumCmds = numCmds;
  return ppCmds;
}
