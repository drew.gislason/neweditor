/*!************************************************************************************************
  @file NedUtils.h

  @brief  General Ned Utilities for string manipulation and other things.

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_utils Ned Utils - An API for various utilities that aren't Ned specific.

*///***********************************************************************************************
#ifndef NED_UTIL_FILE_H
#define NED_UTIL_FILE_H

#include <stdio.h>
#include <limits.h>
#include <time.h>
#include "Ned.h"

#define FILEINFO_SANCHK       22922

// used for navigating files and folders
typedef struct
{
  unsigned              sanchk;
  bool_t                fIsDir;
  bool_t                fIsModified;    // modified outside editor
  bool_t                fRdOnly;
  time_t                modTime;
  char                  szFullPath[PATH_MAX];
} sNedFileInfo_t;

// for dealing with files (see also getenv() and getcwd()
char *        NedFileRead           (const char *szFilename);
bool_t        NedLibFileFullPath    (char *szFullPath, const char *szPartialPath);
bool_t        NedLibParentPath      (char *szPath);
bool_t        NedFileExists         (const char *szPath);
bool_t        NedFileInfoGet        (sNedFileInfo_t *pInfo, const char *szPath);
bool_t        NedFileFindInPath     (char *szPath, unsigned maxSize, const char *szBaseName, bool_t cwdFirst);
bool_t        NedFileFindInFolder   (char *szPath, unsigned maxSize, const char *szBaseName, const char *szBaseFolder);
void          NedFileGetHomeFolder  (char *szPath, unsigned maxSize);
bool_t        NedFileExpandHome     (char *szPath, unsigned maxSize);
bool_t        NedFileReduceHome     (char *szPath);
void          NedFileGetCwd         (char *szPath, unsigned maxSize);

// for iterating through a file/folder trees/lists
void *        NedFileListNew        (const char *pszWildPath);
bool_t        NedFileIsList         (void *hList);
void          NedFileListFree       (void *hList);
unsigned      NedFileListLen        (void *hList);
const char *  NedFileListGetName    (void *hList, unsigned i);
unsigned      NedFileListLenEx      (void *hList);
const char *  NedFileListGetNameEx  (void *hList, unsigned i);

// for iterating through tabbed complete of a partial file/pathname like "Doc" tab -> "Documents/" or "
void *        NedTabCompleteNew     (unsigned maxSize);
bool_t        NedTabComplete        (char *szPath, void *hTabComplete);
bool_t        NedTabCompleteRewind  (void *hTabComplete);
void          NedTabCompleteFree    (void *hTabComplete);

#endif // NED_UTIL_FILE_H
