/*!************************************************************************************************
  NedKey.c
  Copyright (c) 2017 Drew Gislason

  Ned Key implementation.

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include <termios.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "NedLog.h"
#include "NedKey.h"


/*************************************************************************
  Types and defines
**************************************************************************/

typedef struct
{
  const uint8_t *pKeySeq;
  const char    *szName;
  uint8_t        len;
  nedKey_t       key;
} sNedKeySeq_t;

typedef struct
{
  const char    *szName;
  nedKey_t       key;
} sNedKeyNames_t;


static bool_t   NedKeyInSequence            (nedKey_t *pKey);
static int      NedFindBytesInSeqArray      (int iLen, uint8_t *pKeySeq);
static int      NedKeyIsFoundInSeqArray     (nedKey_t iKey);
static int      NedKeyIsFoundInKeyNameArray (nedKey_t iKey);

// multi-byte key sequences (Mac OS X)
static const uint8_t mpNedKeyBackTab[]        = { 27,91,90 };
static const uint8_t mpNedKeyDelete[]         = { 27,91,51,126 };
static const uint8_t mpNedKeyUp[]             = { 27,91,65 };
static const uint8_t mpNedKeyDown[]           = { 27,91,66 };
static const uint8_t mpNedKeyLeft[]           = { 27,91,68 };
static const uint8_t mpNedKeyRight[]          = { 27,91,67 };
static const uint8_t mpNedKeyHome[]           = { 27,79,72 };
static const uint8_t mpNedKeyEnd[]            = { 27,79,70 };
static const uint8_t mpNedKeyHome2[]          = { 27,91,49,126 };
static const uint8_t mpNedKeyEnd2[]           = { 27,91,52,126 };
static const uint8_t mpNedKeyPgUp[]           = { 27,91,53,126 };       // Esc[5~
static const uint8_t mpNedKeyPgDn[]           = { 27,91,54,126 };       // Esc[6~
static const uint8_t mpNedKeyCtrlUp[]         = { 27,91,49,59,53,65 };  // Esc[1;5A
static const uint8_t mpNedKeyCtrlDown[]       = { 27,91,49,59,53,66 };  // Esc[1;5B
static const uint8_t mpNedKeyCtrlLeft[]       = { 27,91,49,59,53,68 };
static const uint8_t mpNedKeyCtrlRight[]      = { 27,91,49,59,53,67 };
static const uint8_t mpNedKeyAltLeft[]        = { 27,98 };
static const uint8_t mpNedKeyAltRight[]       = { 27,102 };
static const uint8_t mpNedKeyShiftLeft[]      = { 27,91,49,59,50,68 }; 
static const uint8_t mpNedKeyShiftRight[]     = { 27,91,49,59,50,67 };
static const uint8_t mpNedKeyCtrlBackspace[]  = { 27,91,51,59,53,126 };
static const uint8_t mpNedKeyFn1[]            = { 27,79,80 };
static const uint8_t mpNedKeyFn2[]            = { 27,79,81 };
static const uint8_t mpNedKeyFn3[]            = { 27,79,82 };
static const uint8_t mpNedKeyFn4[]            = { 27,79,83 };
static const uint8_t mpNedKeyFn5[]            = { 27,91,49,53,126 };
static const uint8_t mpNedKeyFn1_Linux[]      = { 27,91,91,65 };
static const uint8_t mpNedKeyFn2_Linux[]      = { 27,91,91,66 };
static const uint8_t mpNedKeyFn3_Linux[]      = { 27,91,91,67 };
static const uint8_t mpNedKeyFn4_Linux[]      = { 27,91,91,68 };
static const uint8_t mpNedKeyFn5_Linux[]      = { 27,91,91,69 };
static const uint8_t mpNedKeyFn6[]            = { 27,91,49,55,126 };
static const uint8_t mpNedKeyFn7[]            = { 27,91,49,56,126 };
static const uint8_t mpNedKeyFn8[]            = { 27,91,49,57,126 };
static const uint8_t mpNedKeyFn9[]            = { 27,91,50,48,126 };
static const uint8_t mpNedKeyFn10[]           = { 27,91,50,49,126 };
static const uint8_t mpNedKeyFn11[]           = { 27,91,50,51,126 };
static const uint8_t mpNedKeyFn12[]           = { 27,91,50,52,126 };
static const uint8_t mpNedKeyEsc[]            = { 27,255 };
static const uint8_t mpNedKeyEsc2[]           = { 27,27 };
static const uint8_t mpNedKeyAltA[]           = { 195,165 };
static const uint8_t mpNedKeyAltB[]           = { 226,136,171 };
static const uint8_t mpNedKeyAltC[]           = { 195,167 };
static const uint8_t mpNedKeyAltD[]           = { 226,136,130 };
static const uint8_t mpNedKeyAltF[]           = { 198,146 };
static const uint8_t mpNedKeyAltG[]           = { 194,169 };
static const uint8_t mpNedKeyAltH[]           = { 203,153 };
static const uint8_t mpNedKeyAltJ[]           = { 226,136,134 };
static const uint8_t mpNedKeyAltK[]           = { 203,154 };
static const uint8_t mpNedKeyAltL[]           = { 194,172 };
static const uint8_t mpNedKeyAltM[]           = { 194,181 };
static const uint8_t mpNedKeyAltO[]           = { 195,184 };
static const uint8_t mpNedKeyAltP[]           = { 207,128 };
static const uint8_t mpNedKeyAltQ[]           = { 197,147 };
static const uint8_t mpNedKeyAltR[]           = { 194,174 };
static const uint8_t mpNedKeyAltS[]           = { 195,159 };
static const uint8_t mpNedKeyAltT[]           = { 226,128,160 };
static const uint8_t mpNedKeyAltV[]           = { 226,136,154 };
static const uint8_t mpNedKeyAltW[]           = { 226,136,145 };
static const uint8_t mpNedKeyAltX[]           = { 226,137,136 };
static const uint8_t mpNedKeyAltZ[]           = { 206,169 };
static const uint8_t mpNedKeyAltA_2[]         = { 27,97 };
static const uint8_t mpNedKeyAltB_2[]         = { 27,98 };
static const uint8_t mpNedKeyAltC_2[]         = { 27,99 };
static const uint8_t mpNedKeyAltD_2[]         = { 27,100 };
static const uint8_t mpNedKeyAltE_2[]         = { 27,101 };
static const uint8_t mpNedKeyAltF_2[]         = { 27,102 };
static const uint8_t mpNedKeyAltG_2[]         = { 27,103 };
static const uint8_t mpNedKeyAltH_2[]         = { 27,104 };
static const uint8_t mpNedKeyAltI_2[]         = { 27,105 };
static const uint8_t mpNedKeyAltJ_2[]         = { 27,106 };
static const uint8_t mpNedKeyAltK_2[]         = { 27,107 };
static const uint8_t mpNedKeyAltL_2[]         = { 27,108 };
static const uint8_t mpNedKeyAltM_2[]         = { 27,109 };
static const uint8_t mpNedKeyAltN_2[]         = { 27,110 };
static const uint8_t mpNedKeyAltO_2[]         = { 27,111 };
static const uint8_t mpNedKeyAltP_2[]         = { 27,112 };
static const uint8_t mpNedKeyAltQ_2[]         = { 27,113 };
static const uint8_t mpNedKeyAltR_2[]         = { 27,114 };
static const uint8_t mpNedKeyAltS_2[]         = { 27,115 };
static const uint8_t mpNedKeyAltT_2[]         = { 27,116 };
static const uint8_t mpNedKeyAltU_2[]         = { 27,117 };
static const uint8_t mpNedKeyAltV_2[]         = { 27,118 };
static const uint8_t mpNedKeyAltW_2[]         = { 27,119 };
static const uint8_t mpNedKeyAltX_2[]         = { 27,120 };
static const uint8_t mpNedKeyAltY_2[]         = { 27,121 };
static const uint8_t mpNedKeyAltZ_2[]         = { 27,122 };
static const uint8_t mpNedKeyAltDash[]        = { 226,128,147 };
static const uint8_t mpNedKeyAltEqual[]       = { 226,137,160 };
static const uint8_t mpNedKeyAltLeftBrace[]   = { 226,128,156 };
static const uint8_t mpNedKeyAltRightBrace[]  = { 226,128,152 };
static const uint8_t mpNedKeyAltBackslash[]   = { 194,171 };
static const uint8_t mpNedKeyAltSpace[]       = { 194, 160 };
static const uint8_t mpNedKeyAltColon[]       = { 226,128,166 };
static const uint8_t mpNedKeyAltQuote[]       = { 195,166 };
static const uint8_t mpNedKeyAltComma[]       = { 226,137,164 };
static const uint8_t mpNedKeyAltPeriod[]      = { 226,137,165 };
static const uint8_t mpNedKeyAltSlash[]       = { 195,183 };


static const sNedKeySeq_t maNedKeySequences[] =
{
  { mpNedKeyUp, (const char *)"Up", sizeof(mpNedKeyUp), NEDKEY_UP },
  { mpNedKeyDown, (const char *)"Down", sizeof(mpNedKeyDown), NEDKEY_DOWN },
  { mpNedKeyLeft, (const char *)"Left", sizeof(mpNedKeyDown), NEDKEY_LEFT },
  { mpNedKeyRight, (const char *)"Right", sizeof(mpNedKeyDown), NEDKEY_RIGHT },
  { mpNedKeyHome, (const char *)"Home", sizeof(mpNedKeyHome), NEDKEY_HOME },
  { mpNedKeyEnd, (const char *)"End", sizeof(mpNedKeyEnd), NEDKEY_END },
  { mpNedKeyHome2, (const char *)"Home", sizeof(mpNedKeyHome2), NEDKEY_HOME },
  { mpNedKeyEnd2, (const char *)"End", sizeof(mpNedKeyEnd2), NEDKEY_END },
  { mpNedKeyPgUp, (const char *)"PgUp", sizeof(mpNedKeyPgUp), NEDKEY_PGUP },
  { mpNedKeyPgDn, (const char *)"PgDn", sizeof(mpNedKeyPgDn), NEDKEY_PGDN },
  { mpNedKeyCtrlUp,(const char *)"Ctrl-Up", sizeof(mpNedKeyCtrlUp), NEDKEY_CTRL_UP },
  { mpNedKeyCtrlDown,(const char *)"Ctrl-Down", sizeof(mpNedKeyCtrlDown), NEDKEY_CTRL_DOWN },
  { mpNedKeyCtrlLeft,(const char *)"Ctrl-Left", sizeof(mpNedKeyCtrlLeft), NEDKEY_CTRL_LEFT },
  { mpNedKeyCtrlRight,(const char *)"Ctrl-Right", sizeof(mpNedKeyCtrlRight), NEDKEY_CTRL_RIGHT },
  { mpNedKeyAltLeft,(const char *)"Alt-Left", sizeof(mpNedKeyAltLeft), NEDKEY_ALT_LEFT },
  { mpNedKeyAltRight,(const char *)"Alt-Right", sizeof(mpNedKeyAltRight), NEDKEY_ALT_RIGHT },
  { mpNedKeyShiftLeft,(const char *)"Shift-Left", sizeof(mpNedKeyShiftLeft), NEDKEY_SHIFT_LEFT },
  { mpNedKeyShiftRight,(const char *)"Shift-Right", sizeof(mpNedKeyShiftRight), NEDKEY_SHIFT_RIGHT },
  { mpNedKeyCtrlBackspace,(const char *)"Ctrl-Backspace", sizeof(mpNedKeyCtrlBackspace), NEDKEY_CTRL_BACKSPACE },
  { mpNedKeyEsc,  (const char *)"Esc",   sizeof(mpNedKeyEsc), NEDKEY_ESC },
  { mpNedKeyEsc2, (const char *)"Esc",   sizeof(mpNedKeyEsc2), NEDKEY_ESC },
  { mpNedKeyBackTab, (const char *)"Backtab", sizeof(mpNedKeyBackTab), NEDKEY_BACK_TAB },
  { mpNedKeyDelete, (const char *)"Delete", sizeof(mpNedKeyDelete), NEDKEY_DELETE },
  { mpNedKeyAltDash, (const char *)"Alt--", sizeof(mpNedKeyAltDash), NEDKEY_ALT_DASH },
  { mpNedKeyAltEqual, (const char *)"Alt-=", sizeof(mpNedKeyAltEqual), NEDKEY_ALT_EQUAL },
  { mpNedKeyAltLeftBrace, (const char *)"Alt-[", sizeof(mpNedKeyAltLeftBrace), NEDKEY_ALT_LEFT_BRACE },
  { mpNedKeyAltRightBrace, (const char *)"Alt-]", sizeof(mpNedKeyAltLeftBrace), NEDKEY_ALT_RIGHT_BRACE },
  { mpNedKeyAltBackslash, (const char *)"Alt-\\", sizeof(mpNedKeyAltBackslash), NEDKEY_ALT_BACKSLASH },
  { mpNedKeyAltSpace, (const char *)"Alt-Space", sizeof(mpNedKeyAltSpace), NEDKEY_ALT_SPACE },
  { mpNedKeyAltColon, (const char *)"Alt-;", sizeof(mpNedKeyAltColon), NEDKEY_ALT_COLON },
  { mpNedKeyAltQuote, (const char *)"Alt-'", sizeof(mpNedKeyAltQuote), NEDKEY_ALT_QUOTE },
  { mpNedKeyAltComma, (const char *)"Alt-,", sizeof(mpNedKeyAltComma), NEDKEY_ALT_COMMA },
  { mpNedKeyAltPeriod, (const char *)"Alt-.", sizeof(mpNedKeyAltPeriod), NEDKEY_ALT_PERIOD },
  { mpNedKeyAltSlash, (const char *)"Alt-/", sizeof(mpNedKeyAltSlash), NEDKEY_ALT_SLASH },
  { mpNedKeyFn1, (const char *)"Fn1", sizeof(mpNedKeyFn1), NEDKEY_FN1 },
  { mpNedKeyFn2, (const char *)"Fn2", sizeof(mpNedKeyFn2), NEDKEY_FN2 },
  { mpNedKeyFn3, (const char *)"Fn3", sizeof(mpNedKeyFn3), NEDKEY_FN3 },
  { mpNedKeyFn4, (const char *)"Fn4", sizeof(mpNedKeyFn4), NEDKEY_FN4 },
  { mpNedKeyFn5, (const char *)"Fn5", sizeof(mpNedKeyFn5), NEDKEY_FN5 },
  { mpNedKeyFn1_Linux, (const char *)"Fn1", sizeof(mpNedKeyFn1_Linux), NEDKEY_FN1 },
  { mpNedKeyFn2_Linux, (const char *)"Fn2", sizeof(mpNedKeyFn2_Linux), NEDKEY_FN2 },
  { mpNedKeyFn3_Linux, (const char *)"Fn3", sizeof(mpNedKeyFn3_Linux), NEDKEY_FN3 },
  { mpNedKeyFn4_Linux, (const char *)"Fn4", sizeof(mpNedKeyFn4_Linux), NEDKEY_FN4 },
  { mpNedKeyFn5_Linux, (const char *)"Fn5", sizeof(mpNedKeyFn5_Linux), NEDKEY_FN5 },
  { mpNedKeyFn6, (const char *)"Fn6", sizeof(mpNedKeyFn6), NEDKEY_FN6 },
  { mpNedKeyFn7, (const char *)"Fn7", sizeof(mpNedKeyFn7), NEDKEY_FN7 },
  { mpNedKeyFn8, (const char *)"Fn8", sizeof(mpNedKeyFn8), NEDKEY_FN8 },
  { mpNedKeyFn9, (const char *)"Fn9", sizeof(mpNedKeyFn9), NEDKEY_FN9 },
  { mpNedKeyFn10, (const char *)"Fn10", sizeof(mpNedKeyFn10), NEDKEY_FN10 },
  { mpNedKeyFn11, (const char *)"Fn11", sizeof(mpNedKeyFn11), NEDKEY_FN11 },
  { mpNedKeyFn12, (const char *)"Fn12", sizeof(mpNedKeyFn12), NEDKEY_FN12 },
  { mpNedKeyAltA, (const char *)"Alt-A", sizeof(mpNedKeyAltA), NEDKEY_ALT_A },
  { mpNedKeyAltB, (const char *)"Alt-B", sizeof(mpNedKeyAltB), NEDKEY_ALT_B },
  { mpNedKeyAltC, (const char *)"Alt-C", sizeof(mpNedKeyAltC), NEDKEY_ALT_C },
  { mpNedKeyAltD, (const char *)"Alt-D", sizeof(mpNedKeyAltD), NEDKEY_ALT_D },
  { mpNedKeyAltF, (const char *)"Alt-F", sizeof(mpNedKeyAltF), NEDKEY_ALT_F },
  { mpNedKeyAltG, (const char *)"Alt-G", sizeof(mpNedKeyAltG), NEDKEY_ALT_G },
  { mpNedKeyAltH, (const char *)"Alt-H", sizeof(mpNedKeyAltH), NEDKEY_ALT_H },
  { mpNedKeyAltJ, (const char *)"Alt-J", sizeof(mpNedKeyAltJ), NEDKEY_ALT_J },
  { mpNedKeyAltK, (const char *)"Alt-K", sizeof(mpNedKeyAltK), NEDKEY_ALT_K },
  { mpNedKeyAltL, (const char *)"Alt-L", sizeof(mpNedKeyAltL), NEDKEY_ALT_L },
  { mpNedKeyAltM, (const char *)"Alt-M", sizeof(mpNedKeyAltM), NEDKEY_ALT_M },
  { mpNedKeyAltO, (const char *)"Alt-O", sizeof(mpNedKeyAltO), NEDKEY_ALT_O },
  { mpNedKeyAltP, (const char *)"Alt-P", sizeof(mpNedKeyAltP), NEDKEY_ALT_P },
  { mpNedKeyAltQ, (const char *)"Alt-Q", sizeof(mpNedKeyAltQ), NEDKEY_ALT_Q },
  { mpNedKeyAltR, (const char *)"Alt-R", sizeof(mpNedKeyAltR), NEDKEY_ALT_R },
  { mpNedKeyAltS, (const char *)"Alt-S", sizeof(mpNedKeyAltS), NEDKEY_ALT_S },
  { mpNedKeyAltT, (const char *)"Alt-T", sizeof(mpNedKeyAltT), NEDKEY_ALT_T },
  { mpNedKeyAltV, (const char *)"Alt-V", sizeof(mpNedKeyAltV), NEDKEY_ALT_V },
  { mpNedKeyAltW, (const char *)"Alt-W", sizeof(mpNedKeyAltW), NEDKEY_ALT_W },
  { mpNedKeyAltX, (const char *)"Alt-X", sizeof(mpNedKeyAltX), NEDKEY_ALT_X },
  { mpNedKeyAltZ, (const char *)"Alt-Z", sizeof(mpNedKeyAltZ), NEDKEY_ALT_Z },
  { mpNedKeyAltA_2, (const char *)"Alt-A", sizeof(mpNedKeyAltA_2), NEDKEY_ALT_A },
  { mpNedKeyAltB_2, (const char *)"Alt-B", sizeof(mpNedKeyAltB_2), NEDKEY_ALT_B },
  { mpNedKeyAltC_2, (const char *)"Alt-C", sizeof(mpNedKeyAltC_2), NEDKEY_ALT_C },
  { mpNedKeyAltD_2, (const char *)"Alt-D", sizeof(mpNedKeyAltD_2), NEDKEY_ALT_D },
  { mpNedKeyAltE_2, (const char *)"Alt-E", sizeof(mpNedKeyAltE_2), NEDKEY_ALT_E },
  { mpNedKeyAltF_2, (const char *)"Alt-F", sizeof(mpNedKeyAltF_2), NEDKEY_ALT_F },
  { mpNedKeyAltG_2, (const char *)"Alt-G", sizeof(mpNedKeyAltG_2), NEDKEY_ALT_G },
  { mpNedKeyAltH_2, (const char *)"Alt-H", sizeof(mpNedKeyAltH_2), NEDKEY_ALT_H },
  { mpNedKeyAltI_2, (const char *)"Alt-I", sizeof(mpNedKeyAltI_2), NEDKEY_ALT_I },
  { mpNedKeyAltJ_2, (const char *)"Alt-J", sizeof(mpNedKeyAltJ_2), NEDKEY_ALT_J },
  { mpNedKeyAltK_2, (const char *)"Alt-K", sizeof(mpNedKeyAltK_2), NEDKEY_ALT_K },
  { mpNedKeyAltL_2, (const char *)"Alt-L", sizeof(mpNedKeyAltL_2), NEDKEY_ALT_L },
  { mpNedKeyAltM_2, (const char *)"Alt-M", sizeof(mpNedKeyAltM_2), NEDKEY_ALT_M },
  { mpNedKeyAltN_2, (const char *)"Alt-N", sizeof(mpNedKeyAltN_2), NEDKEY_ALT_N },
  { mpNedKeyAltO_2, (const char *)"Alt-O", sizeof(mpNedKeyAltO_2), NEDKEY_ALT_O },
  { mpNedKeyAltP_2, (const char *)"Alt-P", sizeof(mpNedKeyAltP_2), NEDKEY_ALT_P },
  { mpNedKeyAltQ_2, (const char *)"Alt-Q", sizeof(mpNedKeyAltQ_2), NEDKEY_ALT_Q },
  { mpNedKeyAltR_2, (const char *)"Alt-R", sizeof(mpNedKeyAltR_2), NEDKEY_ALT_R },
  { mpNedKeyAltS_2, (const char *)"Alt-S", sizeof(mpNedKeyAltS_2), NEDKEY_ALT_S },
  { mpNedKeyAltT_2, (const char *)"Alt-T", sizeof(mpNedKeyAltT_2), NEDKEY_ALT_T },
  { mpNedKeyAltU_2, (const char *)"Alt-U", sizeof(mpNedKeyAltU_2), NEDKEY_ALT_U },
  { mpNedKeyAltV_2, (const char *)"Alt-V", sizeof(mpNedKeyAltV_2), NEDKEY_ALT_V },
  { mpNedKeyAltW_2, (const char *)"Alt-W", sizeof(mpNedKeyAltW_2), NEDKEY_ALT_W },
  { mpNedKeyAltX_2, (const char *)"Alt-X", sizeof(mpNedKeyAltX_2), NEDKEY_ALT_X },
  { mpNedKeyAltY_2, (const char *)"Alt-Y", sizeof(mpNedKeyAltY_2), NEDKEY_ALT_Y },
  { mpNedKeyAltZ_2, (const char *)"Alt-Z", sizeof(mpNedKeyAltZ_2), NEDKEY_ALT_Z }
};

// special keynames checked first (e.g. "Enter" instead of "Ctrl-M")
static const sNedKeyNames_t maNedKeyNames[] =
{
  { (const char *)"Ctrl-Space", NEDKEY_CTRL_SPACE },
  { (const char *)"Esc",        NEDKEY_ESC },
  { (const char *)"Tab",        NEDKEY_TAB },
  { (const char *)"Enter",      NEDKEY_ENTER },
  { (const char *)"Ctrl-\\",    NEDKEY_CTRL_BACKSLASH },
  { (const char *)"Ctrl-]",     NEDKEY_CTRL_RIGHT_BRACE },
  { (const char *)"Ctrl-^",     NEDKEY_CTRL_CAROT },
  { (const char *)"Ctrl--",     NEDKEY_CTRL_MINUS },
  { (const char *)"Backspace",  NEDKEY_BACKSPACE },
  { (const char *)"Space",      (nedKey_t)' ' },
  { (const char *)"Ctrl-Home",  NEDKEY_CTRL_HOME },
  { (const char *)"Ctrl-End",   NEDKEY_CTRL_END },
  { (const char *)"Ctrl-PgUp",  NEDKEY_CTRL_PGUP },
  { (const char *)"Ctrl-PgDn",  NEDKEY_CTRL_PGDN },
  { (const char *)"Alt-Up",     NEDKEY_ALT_UP },
  { (const char *)"Alt-Down",   NEDKEY_ALT_DOWN },
  { (const char *)"Alt-Home",   NEDKEY_ALT_HOME },
  { (const char *)"Alt-End",    NEDKEY_ALT_END },
  { (const char *)"Alt-PgUp",   NEDKEY_ALT_PGUP },
  { (const char *)"Alt-PgDn",   NEDKEY_ALT_PGDN },
  { (const char *)"Alt-E",      NEDKEY_ALT_E },
  { (const char *)"Alt-I",      NEDKEY_ALT_I },
  { (const char *)"Alt-N",      NEDKEY_ALT_N },
  { (const char *)"Alt-U",      NEDKEY_ALT_U },
  { (const char *)"Alt-Y",      NEDKEY_ALT_Y },
  { (const char *)"None",       NEDKEY_NONE }
};

static int                miNedInKeySeq;
static uint8_t            maNedKeySeqFound[6];      // maximum of 6 sequences in key
static pfnNedKeyIdle_t    m_pfnIdle       = NULL;

bool_t                    m_fMacroRecording;
unsigned                  m_MacroIndex;
unsigned                  m_MacroNumKeys;
nedKey_t                  m_aMacroKeys[NEDKEY_MACRO_MAX];

// use termios to get raw ANSI keys
// static struct termios old, new;

/*!------------------------------------------------------------------------------------------------
  Sets the idle function for when keyboard is idle

  @ingroup  ned_key
  @param    pfnIdle   idle function, or NULL to turn off

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedKeySetIdle(pfnNedKeyIdle_t pfnIdle)
{
  m_pfnIdle = pfnIdle;
}

/*!------------------------------------------------------------------------------------------------
  Return TRUE if playing back the macro

  @ingroup  ned_key

  @return   TRUE if playing back the macro
*///-----------------------------------------------------------------------------------------------
bool_t NedKeyInMacro(void)
{
  return (m_MacroIndex < m_MacroNumKeys) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Currently recording the macro

  @ingroup  ned_key

  @return   TRUE if recording
*///-----------------------------------------------------------------------------------------------
bool_t NedKeyMacroRecording(void)
{
  return m_fMacroRecording;
}

/*!------------------------------------------------------------------------------------------------
  Play the macro (if any)

  @ingroup  ned_key

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedKeyMacroPlay(void)
{
  if(NedKeyMacroRecording())
    NedKeyMacroEndRecord();
  m_MacroIndex = 0;
}

/*!------------------------------------------------------------------------------------------------
  Brings the macro state machine back to factory condition (nothing recorded, everything stopped

  @ingroup  ned_key

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedKeyMacroClear(void)
{
  m_MacroIndex = m_MacroNumKeys = 0;
  m_fMacroRecording = FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Record the macro

  @ingroup  ned_key

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedKeyMacroRecord(void)
{
  NedKeyMacroClear();
  m_fMacroRecording = TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Record the macro

  @ingroup  ned_key

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedKeyMacroEndRecord(void)
{
  // whatever ended the record, don't include in the list of keys
  if(m_MacroNumKeys)
    --m_MacroNumKeys;
  m_MacroIndex = m_MacroNumKeys;
  m_fMacroRecording = FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Add a key to the macro sequence. Playback remains stopped.

  @ingroup  ned_key

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedKeyMacroAddKey(nedKey_t key)
{
  if(m_MacroNumKeys < NEDKEY_MACRO_MAX)
  {
    m_aMacroKeys[m_MacroNumKeys] = key;
    ++m_MacroNumKeys;
    m_MacroIndex = m_MacroNumKeys;
  }
}

/*!------------------------------------------------------------------------------------------------
  Get the current macro key sequence

  @ingroup  ned_key
  @param    pNumKeys    pointer to returned # of keys

  @return   a pointer to the key sequence and the number of keys
*///-----------------------------------------------------------------------------------------------
const nedKey_t *NedKeyMacroGet(unsigned *pNumKeys)
{
  *pNumKeys = m_MacroNumKeys;
  return m_aMacroKeys;
}

/*!------------------------------------------------------------------------------------------------
  Gets a key from either the macro system (if playing back), or from the raw terminal.

  @ingroup  ned_key

  @return   key code (0-255). See ::nedKey_t
*///-----------------------------------------------------------------------------------------------
nedKey_t NedKeyGetKey(void)
{
  nedKey_t  key;
  bool_t    fInSeq = FALSE;

  // play the macro
  if(NedKeyInMacro())
  {
    key = m_aMacroKeys[m_MacroIndex];
    ++m_MacroIndex;
  }

  else
  {
    // keep waiting until an actual key
    do
    {
      miNedInKeySeq = 0;

      // pull the whole sequence out of the keyboard buffer
      do
      {
        // key may be part of a sequence (e.g. up arrow key is 27,91,65)
        // if no key, returns NEDKEY_NONE
        key = NedKeyRawGetKey();

        // none cannot be the 1st part of sequence, but can be last part
        if((key != NEDKEY_NONE) || miNedInKeySeq)
        {
          // printf(" (key = %d) ", key);
          // fflush(stdout);
          fInSeq = NedKeyInSequence(&key);
        }

      } while( fInSeq );

      // call on the idle function if doing nothing
      if(m_pfnIdle && key == NEDKEY_NONE)
        (*m_pfnIdle)();

    } while(key == NEDKEY_NONE);

    // if recording, add the key
    if(NedKeyMacroRecording())
      NedKeyMacroAddKey(key);
  }

  return key;
}

/*!----------------------------------------------------------------------------------
  Gets name of the Ned Key in string form (e.g "Ctrl-A"). Note: this is built on the
  fly, so copy if it must be remembered after next call to NedKeyName().

  @ingroup  ned_key
  @param    key     The key code. See ::nedKey_t

  @return   string name of key code
*///---------------------------------------------------------------------------------
const char * NedKeyName(nedKey_t key)
{
  static char   szKeyName[12];
  const  char  *pszKeyName = "None";
  int           i;

  // assume we must build key name
  pszKeyName    = (const char *)szKeyName;
  szKeyName[0]  = 0;

  // is keyname in the sequence array? use it
  i = NedKeyIsFoundInSeqArray(key);
  if( i != -1 )
  {
    pszKeyName = maNedKeySequences[i].szName;
  }
  else
  {
    // is keyname in the keyname array? use it
    i = NedKeyIsFoundInKeyNameArray(key);
    if( i != -1 )
    {
      pszKeyName = maNedKeyNames[i].szName;
    }

    // keyname not in either array, build it
    else
    {
      if( (key >= NEDKEY_CTRL_A) && (key <= NEDKEY_CTRL_Z) )
      {
        sprintf(szKeyName, "Ctrl-%c", 'A' + (key - NEDKEY_CTRL_A));
      }
      else if( (key >= ' ') && (key <= '~') )
      {
        sprintf(szKeyName, "%c", key);
      }
      else
      {
        sprintf(szKeyName, "None");
      }
    }
  }

  return pszKeyName;
}

/*!----------------------------------------------------------------------------------
  Return the nedKey code that matches the name, or NEDKEY_NONE if no match.

  @ingroup      ned_key
  @param[in]    szName     The key code.

  @return       The nedKey that matches the name. See ::nedKey_t, or NEDKEY_NONE
*///---------------------------------------------------------------------------------
nedKey_t NedKeyFromName(const char *szName)
{
  static char   szKeyName[8];
  nedKey_t      key     = NEDKEY_NONE;
  bool_t        fFound  = FALSE;
  unsigned      i;

  if(szName != NULL)
  {
    // first check the key sequences
    for(i=0; i<NumElements(maNedKeySequences); ++i)
    {
      if( strcmp(maNedKeySequences[i].szName, szName) == 0 )
      {
        key    = maNedKeySequences[i].key;
        fFound = TRUE;
        break;
      }
    }

    // next check the keyname array
    if(!fFound)
    {
      for(i=0; i<NumElements(maNedKeyNames); ++i)
      {
        if( strcmp(maNedKeyNames[i].szName, szName) == 0 )
        {
          key    = maNedKeyNames[i].key;
          fFound = TRUE;
          break;
        }
      }
    }

    // ctrl keys
    if(!fFound)
    {
      for(i=NEDKEY_CTRL_A; i<= NEDKEY_CTRL_Z; ++i)
      {
        sprintf(szKeyName, "Ctrl-%c", (char)('A'+(i-1)));
        if( strcmp(szKeyName, szName) == 0 )
        {
          key    = (nedKey_t)i;
          fFound = TRUE;
          break;
        }
      }
    }

    // printable ascii keys
    if(!fFound)
    {
      for(i=' '; i<= '~'; ++i)
      {
        sprintf(szKeyName, "%c", (char)i);
        if( strcmp(szKeyName, szName) == 0 )
        {
          key    = (nedKey_t)i;
          fFound = TRUE;
          break;
        }
      }
    }
  }

  return key;
}

/*-------------------------------------------------------------------------------------------------
  Is this key found in the sequence array?
-------------------------------------------------------------------------------------------------*/
static bool_t NedKeyInSequence(nedKey_t *pKey)
{
  int i;

  // look for this key sequence in the array
  maNedKeySeqFound[miNedInKeySeq++] = *pKey;
  i = NedFindBytesInSeqArray(miNedInKeySeq, maNedKeySeqFound);
  if(i != -1 )
  {
    if(maNedKeySequences[i].len == miNedInKeySeq)
    {
      *pKey = maNedKeySequences[i].key;
      return FALSE;
    }
    return TRUE;
  }
  return FALSE;
}

/*-------------------------------------------------------------------------
  Is this key found in the sequence array?
-------------------------------------------------------------------------*/
static int NedFindBytesInSeqArray(int len, uint8_t *pKeySeq)
{
  int i;

  // printf("NedFindBytesInSeqArray(len %d) ", len);
  for(i=0; i<NumElements(maNedKeySequences); ++i)
  {
    if( (maNedKeySequences[i].len >= len) && 
        (memcmp(pKeySeq, maNedKeySequences[i].pKeySeq, len) == 0) )
    {
      // printf("  found %d\r\n", i);
      // fflush(stdout);
      return i; // found a match, return the index
    }
  }
  // printf("  not found\r\n");
  // fflush(stdout);
  return -1;  // no match
}

/*-------------------------------------------------------------------------
  Is this key found in the sequence array?
-------------------------------------------------------------------------*/
static int NedKeyIsFoundInSeqArray(nedKey_t key)
{
  int i;
  for(i=0; i<NumElements(maNedKeySequences); ++i)
  {
    if(maNedKeySequences[i].key == key)
      return i;
  }
  return -1;
}

/*-------------------------------------------------------------------------
  Is this key found in the keyname array?
-------------------------------------------------------------------------*/
static int NedKeyIsFoundInKeyNameArray(nedKey_t key)
{
  int i;
  for(i=0; i<NumElements(maNedKeyNames); ++i)
  {
    if(maNedKeyNames[i].key == key)
      return i;
  }
  return -1;
}

static unsigned         fRawEnabled;
static struct termios   old;
static struct termios   new;

/*!------------------------------------------------------------------------------------------------
  enable raw mode if not already enabled.

  @ingroup  ned_key

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedKeyRawEnable(void)
{
  if(!fRawEnabled)
  {
    fRawEnabled = TRUE;

    tcgetattr(STDIN_FILENO, &old);
    new = old;
    cfmakeraw(&new);
    // new.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP
    //                         | INLCR  | IGNCR  | ICRNL | IXON);
    // new.c_oflag &= ~OPOST;
    // new.c_lflag &= ~(ECHO   | ECHONL | ICANON | ISIG  | IEXTEN);
    // new.c_cflag &= ~(CSIZE  | PARENB);
    // new.c_cflag |= CS8;
    new.c_cc[VMIN] = 0;
    new.c_cc[VTIME] = 1;
    tcsetattr(0, TCSADRAIN, &new);

  }
}

/*!------------------------------------------------------------------------------------------------
  Go back to original terminal settings. Do this if using NedKeyRawGetKey() or NedKeyRawEnable()
  before exiting the programing.

  As the keyboard is places in raw mode, make sure to call NedKeyRawDisable() before exiting your

  @ingroup  ned_key

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedKeyRawDisable(void)
{
  if(fRawEnabled)
  {
    fRawEnabled = FALSE;
    tcsetattr(STDIN_FILENO, TCSANOW, &old);
  }
}

/*!------------------------------------------------------------------------------------------------
  Returns the raw key code from the read() function, one byte at a time. Waits only 1/10th of a
  second and, if no key is available, returns psudo key NEDKEY_NONE.

  This places the console in raw mode, make sure to call NedKeyRawDisable() before exiting your
  program. When raw mode is enabled, you need to use \r\n with printf().

  Some keys are multi-byte, for examples arrows. Since some of the multi-byte sequences begin with
  Esc (27), the only way to tell if it's an Esc or an arrow is if a NEDKEY_NONE occurs after the
  Esc key. See also ::nedKey_t.

  @ingroup  ned_key

  @return   NEDKEY_NONE or key value
*///-----------------------------------------------------------------------------------------------
nedKey_t NedKeyRawGetKey(void)
{
  static uint8_t    aBuf[10];
  static unsigned   i     = 0;
  static unsigned   len   = 0;
  uint8_t           key   = NEDKEY_NONE;

  // will only enabled not already enabled (set to wait 1/10th second)

  if(i >= len)
  {
    i   = 0;
    fflush(stdout);
    NedKeyRawEnable();
    len = read(STDIN_FILENO, aBuf, sizeof(aBuf));
    NedKeyRawDisable();
  }

  // len may be 0 after read, or this may be from a previous read
  if(i < len)
  {
    key = aBuf[i];
    ++i;
  }

  return key;
}
