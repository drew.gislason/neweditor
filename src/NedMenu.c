/*!************************************************************************************************
  @file NedMenu.c

  @brief  Ned Menu Implementation

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  NedWinIsWin()
  winPos_t
  NedWinGetBackdrop();
  NedWinGetPicked();
  NedWinPick(pNmData->pWinParent);
  NedWinGetSize(&rows, &cols);
  NedWinGetPos(&top, &left);
  NedWinResize(1, cols);
  NedWinNew(top, left, 1, cols);
  NedWinClose(pNmData->pWinMenu);
  NedWinFlush();
  NedWinFlushCursor();
  NedWinSetAttr(pTheme->menuText);
  NedWinClearAll();
  NedWinGoto(0,0);
  NedWinPrintf("  ");

  @ingroup  ned_menu

*///***********************************************************************************************
#include <ctype.h>
#include "NedWin.h"
#include "NedMenu.h"

#define MENU_SANCHK      32404U
#define PICK_SANCHK      32405U
#define MENU_SPACE       2
// #define MENU_MIN_ROWS    NEDMIN_ROWS

typedef struct
{
  unsigned              sanchk;
  sNedMenuBar_t        *pMenuBar;       // pointer to menu bar
  void                 *pWinParent;     // parent of menu bar/windowing system
  void                 *pWinMenuBar;    // main menu bar
  void                 *pWinMenu;       // menu window
  unsigned              themeIndex;     // current color theme
  bool_t                fInMenu;        // user has not chosen a menu item or ESCaped
  pfnSubMenu_t          pfnSubMenu;     // current submenu handler
  unsigned              pickedMenu;     // 0-n, menu bar item picked (File) or NEDMENU_PICK_NONE
  unsigned              pickedItem;     // 0-n, item within that menu (File/Open) or NEDMENU_PICK_NONE
  unsigned              pickedSubItem;  //  0-n, item within sub (File/Open/files...) menu or NEDMENU_PICK_NONE
} sNmData_t;

typedef struct
{
  unsigned              sanchk;
  void                 *pWin;           // see NedWin.h
  winPos_t              top;
  winPos_t              desiredLeft;
  winPos_t              left;
  winPos_t              rows;
  winPos_t              cols;
  nedAttr_t             text;
  nedAttr_t             highlight;
  unsigned              topItem;        // if scrolled
  unsigned              curItem;
  unsigned              numItems;
  const char          **ppszItems;
} sNmPick_t;

static void         NmDrawMenuBar     (sNmData_t *pNmData);
static void         NmDrawSubMenu     (sNmData_t *pNmData);
static void         NmMenuExit        (sNmData_t *pNmData);
static void         NmCloseSubmenu    (sNmData_t *pNmData);
static void         NmCalcMenuPos     (sNmData_t *pNmData, unsigned nMenu, winPos_t *pTop, winPos_t *pLeft);
static void         NmCalcMenuSize    (sNmData_t *pNmData, unsigned nMenu, winPos_t *pRows, winPos_t *pCols);
static bool_t       NmPickMenuBarMenu (sNmData_t *pNmData, nedKey_t key);
static bool_t       NmPickMenuItem    (sNmData_t *pNmData, nedKey_t key);
static unsigned     NmPuts            (nedAttr_t attrText, nedAttr_t attrHotkey, const char *sz, bool_t fNoHotKey);
static unsigned     NmStrLen          (const char *sz);
static unsigned     NmMaxLenStrings   (unsigned numItems, const char **ppItems);
static bool_t       NmSetPicked       (sNmData_t *pNmData, sMenuPicked_t *pPicked, bool_t fPicked);
static pfnSubMenu_t NmGetSubMenu      (sNmData_t *pNmData);

/*!------------------------------------------------------------------------------------------------
  Create menu system that points to the backdrop. Immediately call NedMenuSetParent(), which will
  resize, if using a parent window other than the backdrop.

  The menubar and all of its submenus must be instantiated and filled out before calling this
  function.

  @ingroup  ned_menu
  @param    pMenuBar       pointer to preallocated menu bar structure tree
  @param    pWinParent     pointer to preallocated window, used as parent (could be backdrop)
  @param    themeIndex     index into color theme for menu

  @return   menu handle, or NULL if failed
*///-----------------------------------------------------------------------------------------------
void * NedMenuNew(sNedMenuBar_t *pMenuBar, nedThemeIndex_t themeIndex)
{
  sNmData_t  *pNmData = NULL;

  NedAssertDbg(pMenuBar && pMenuBar->numMenus);
  if(pMenuBar && pMenuBar->numMenus)
  {
    pNmData = malloc(sizeof(sNmData_t));
    if(pNmData)
    {
      memset(pNmData, 0, sizeof(sNmData_t));
      NedLogPrintfEx(NEDLOG_MENU, "NedMenuNew(numMenus=%u)\n", pMenuBar->numMenus);

      pNmData->sanchk       = MENU_SANCHK;
      pNmData->pMenuBar     = pMenuBar;
      pNmData->pWinParent   = NedWinGetBackdrop();
      pNmData->pWinMenuBar  = NULL;
      pNmData->pWinMenu     = NULL;
      NedMenuSetTheme(pNmData, themeIndex);
      pNmData->themeIndex   = themeIndex;
      pNmData->fInMenu      = FALSE;
      pNmData->pickedMenu   = NEDMENU_PICK_NONE;
      pNmData->pickedItem   = NEDMENU_PICK_NONE;  
    }
  }

  return pNmData;
}

/*!------------------------------------------------------------------------------------------------
  Is this actually a menu or not?

  @ingroup  ned_menu
  @param    hMenu       pointer to valid menu handle

  @return   TRUE if yes, FALSE if not a menu
*///-----------------------------------------------------------------------------------------------
bool_t NedMenuIsMenu(void *hMenu)
{
  sNmData_t *pNmData = hMenu;
  return (pNmData && (pNmData->sanchk == MENU_SANCHK)) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Parent window has resized, resize the menu bar and move menu window (if open)

  @ingroup  ned_menu
  @param    hMenu       pointer to valid menu handle

  @return   TRUE if worked, otherwise unchanged
*///-----------------------------------------------------------------------------------------------
bool_t NedMenuResize(void *hMenu)
{
  void        *pOldWin;
  sNmData_t   *pNmData  = hMenu;
  bool_t       fWorked  = FALSE;
  winPos_t     top;
  winPos_t     left;
  winPos_t     rows;
  winPos_t     cols;

  // chose parent window so we can resize
  NedAssert(NedMenuIsMenu(hMenu) && NedWinIsWin(pNmData->pWinParent));

  pOldWin = NedWinPick(pNmData->pWinParent);
  NedWinGetSize(&rows, &cols);
  NedWinGetPos(&top, &left);

  // create or resize the menu bar
  if(!pNmData->pWinMenuBar)
  {
    pNmData->pWinMenuBar = NedWinNew(top, left, 1, cols);
  }
  else
  {
    NedWinPick(pNmData->pWinMenuBar);
    NedWinMove(top, left);
    NedWinResize(1, cols);
  }
  if(pNmData->pWinMenuBar)
    fWorked = TRUE;

  // possibly move submenu if open
  if(pNmData->pickedMenu != NEDMENU_PICK_NONE)
  {
    NedAssert(pNmData->pWinMenu);
    NmCalcMenuPos(pNmData, pNmData->pickedMenu, &top, &left);
    NedWinPick(pNmData->pWinMenu);
    NedWinMove(top, left);
  }

  // point to window prior to this call
  NedWinPick(pOldWin);
  return fWorked;
}

/*!------------------------------------------------------------------------------------------------
  Set the parent window.

  @ingroup  ned_menu
  @param    hMenu       pointer to valid menu handle

  @return   TRUE if worked, otherwise unchanged
*///-----------------------------------------------------------------------------------------------
bool_t NedMenuSetParent(void *hMenu, void *hWinParent)
{
  sNmData_t   *pNmData  = hMenu;
  bool_t       fWorked  = FALSE;

  if(NedMenuIsMenu(hMenu) && NedWinIsWin(hWinParent))
  {
    pNmData->pWinParent = hWinParent;
    fWorked = NedMenuResize(hMenu);
  }
  return fWorked;
}

/*!------------------------------------------------------------------------------------------------
  Set the menu color theme for the menuing system

  @ingroup  ned_menu
  @param    theme       theme 

  @return   TRUE if worked, otherwise unchanged
*///-----------------------------------------------------------------------------------------------
void NedMenuSetTheme(void *hMenu, nedThemeIndex_t themeIndex)
{
  sNmData_t *pNmData = hMenu;
  NedAssertDbg(NedMenuIsMenu(hMenu));
  if(themeIndex < NEDCFG_THEME_MAX)
    pNmData->themeIndex = themeIndex;
}

/*!------------------------------------------------------------------------------------------------
  Get column where to draw a submenu, like File/Open->Files...

  @ingroup  ned_menu
  @param    hMenu       menu handle
  @param    nMenu       0-n, index into menu bar (which submenu)
  @param    nItem       0-n, index into items (which item in submenu)

  @return   column for submenu, or 0 if not valid menu item
*///-----------------------------------------------------------------------------------------------
unsigned NedMenuSubMenuCol(void *hMenu, unsigned nMenu)
{
  sNmData_t            *pNmData   = hMenu;
  winPos_t              top;
  winPos_t              left;
  winPos_t              rows;
  winPos_t              cols = 0;

  NedAssertDbg(NedMenuIsMenu(pNmData) && (nMenu < pNmData->pMenuBar->numMenus));
  if(NedMenuIsMenu(pNmData) && (nMenu < pNmData->pMenuBar->numMenus))
  {
    NmCalcMenuPos(hMenu, nMenu, &top, &left);
    NmCalcMenuSize(hMenu, nMenu, &rows, &cols);
    NedLogPrintfEx(NEDLOG_MENU, "NedMenuSubMenuCol(nMenu %u, left %d, cols %d)\n", nMenu, left, cols);
    cols = left + cols + 2;
  }
  return (unsigned)cols;
}

/*!------------------------------------------------------------------------------------------------
  Get the menu item name string. nMen

  @ingroup  ned_menu
  @param    hMenu       menu handle
  @param    nMenu       0-n, index into menu bar (which submenu)
  @param    nItem       0-n, index into items (which item in submenu)

  @return   pointer to name if menu item exists, NULL if not
*///-----------------------------------------------------------------------------------------------
const char * NedMenuItemName(void *hMenu, unsigned nMenu, unsigned nItem)
{
  const sNedMenuItem_t *pMenuItem = NULL;
  const char           *pszName   = NULL;

  pMenuItem = NedMenuItemPtr(hMenu, nMenu, nItem);
  if(pMenuItem)
    pszName = pMenuItem->szName;

  return pszName;
}

/*!------------------------------------------------------------------------------------------------
  Get a pointer to the item based on menu/item, e.g. File/Open

  @ingroup  ned_menu
  @param    hMenu       menu handle
  @param    nMenu       0-n, index into menu bar (which submenu)
  @param    nItem       0-n, index into items (which item in submenu)

  @return   pointer to name if menu item exists, NULL if not
*///-----------------------------------------------------------------------------------------------
const sNedMenuItem_t *NedMenuItemPtr(void *hMenu, unsigned nMenu, unsigned nItem)
{
  sNmData_t            *pNmData   = hMenu;
  const sNedMenuItem_t *pMenuItem = NULL;
  const sNedMenu_t     *pMenu;

  if(NedMenuIsMenu(pNmData) && (nMenu < pNmData->pMenuBar->numMenus))
  {
    pMenu = &pNmData->pMenuBar->aMenus[nMenu];
    if(nItem < pMenu->numItems)
      pMenuItem = &pMenu->aItems[nItem];
  }
  return pMenuItem;
}

/*!------------------------------------------------------------------------------------------------
  Draw the menu bar and current menu (if any).

  Does NOT affect state, other than "window" things, like width of menubar and position of open
  menu.

  Use when:
  1. Screen size has changed
  2. menu or menu bar items have changed (e.g. new hot key, name change, etc...)
  3. Color theme has changed

  Duties:
  1. Redraw menu bar
  2. Redraw menu (if open)

  @ingroup  ned_menu
  @param    hMenu       menu handle

  @return   TRUE if worked
*///-----------------------------------------------------------------------------------------------
bool_t NedMenuDraw(void *hMenu)
{
  void            *pOldWin;
  sNmData_t       *pNmData    = hMenu;
  bool_t           fWorked    = FALSE;

  NedAssert(NedMenuIsMenu(pNmData));

  pOldWin = NedWinGetPicked();
  fWorked = NedMenuResize(hMenu);
  NmDrawMenuBar(hMenu);
  if(pNmData->pickedMenu != NEDMENU_PICK_NONE)
  {
    NedAssert(pNmData->pWinMenu);
    NmDrawSubMenu(pNmData);
  }
  NedWinPick(pOldWin);

  return fWorked;
}

/*!------------------------------------------------------------------------------------------------
  Free the menu bar system. Closes down any open windows. Opposite of NedMenuNew().

  @ingroup  ned_menu
  @param    pMenu       menu handle

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedMenuFree(void *hMenu)
{
  sNmData_t       *pNmData    = hMenu;
  if(NedMenuIsMenu(pNmData))
  {
    if(pNmData->pWinMenu)
      NedWinClose(pNmData->pWinMenu);
    if(pNmData->pWinMenuBar)
      NedWinClose(pNmData->pWinMenuBar);
    free(pNmData);
  }
}

/*!------------------------------------------------------------------------------------------------
  Enter the menu bar or directly to a sub menu. If pickMenu is NEDMENU_PICK_NONE, then the menu bar
  alone is highlighed. If pickMenu is 0-n, then that submenu is chosen.

  @ingroup  ned_menu
  @param    pMenu       menu handle
  @param    pickMenu    NEDMENU_PICK_NONE, or 0-n

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedMenuEnter(void *hMenu, unsigned pickMenu)
{
  sNmData_t    *pNmData = hMenu;
  void         *pWinOld;

  pWinOld = NedWinGetPicked();

  if(NedMenuIsMenu(pNmData))
  {
    // NedLogPrintfEx(NEDLOG_MENU, "NedMenuEnter(pickMenu=%u)\n",pickMenu);
    pNmData->fInMenu        = TRUE;
    pNmData->pfnSubMenu     = NULL;
    pNmData->pickedSubItem  = NEDMENU_PICK_NONE;

    // covers both NEDMENU_PICK_NONE, and out of range pickMenu
    if(pickMenu >= pNmData->pMenuBar->numMenus)
    {
      pNmData->pickedMenu = 0;
      pNmData->pickedItem = NEDMENU_PICK_NONE;
    }

    // in a submenu at the start
    else
    {
      pNmData->pickedMenu = pickMenu;
      pNmData->pickedItem = 0;
    }
    // NedLogPrintfEx(NEDLOG_MENU, "  pickMenu=%u, pickItem=%u\n", pNmData->pickedMenu, pNmData->pickedItem);

    // draw the menu bar, and optionally go directly into submenu
    NmDrawMenuBar(pNmData);
    if(pNmData->pWinMenu)
      NedWinClose(pNmData->pWinMenu);
    if(pickMenu < pNmData->pMenuBar->numMenus)
      NmDrawSubMenu(pNmData); // will create the pWinMenu window
  }

  NedWinPick(pWinOld);
}

/*!------------------------------------------------------------------------------------------------
  Feed a key into the menu system. The state is determined by:

      pickedMenu != NEDMENU_PICK_NONE if picked a menu on menubar  
      pickedItem != NEDMENU_PICK_NONE if in a submenu  

      key        | menu bar      | submenu
      ---------- | ------------- | -------
      esc        | exits         | exits
      a-z, A-Z   | picks a menu  | picks menu item by letter and exits
      down arrow | picks submenu | picks next item
      up arrow   | none          | picks prev item
      left/right | next menu bar | next / prev menu item (doesn't wrap)
      hotkey     | picks item    | picks item

  @ingroup      ned_menu
  @param[in]    pMenu       menu handle
  @param[in]    key         key to feed into menu systm
  @param[out]   pPicked     which menu/submenu was picked (or fPicked=FALSE if not)

  @return   TRUE if picked something, FALSE if not (still in menu).
*///-----------------------------------------------------------------------------------------------
bool_t NedMenuFeedKey(void *hMenu, nedKey_t key, sMenuPicked_t *pPicked)
{
  sNmData_t            *pNmData   = hMenu;
  const sNedMenu_t     *pMenu;
  void                 *pWinOld;
  bool_t                fPicked   = FALSE;

  pWinOld = NedWinGetPicked();
  // NedLogPrintfEx(NEDLOG_MENU, "NedMenuFeedKey(key %s, pfnSubMenu %p, pickedMenu %u, pickedItem %u\n", 
  //   NedKeyName(key), pNmData->pfnSubMenu, pNmData->pickedMenu, pNmData->pickedItem);

  // can only enter into menu system through NedMenuEnter()
  if(NedMenuIsMenu(pNmData) && pNmData->fInMenu)
  {
    NedAssertDbg(pNmData->pickedMenu < pNmData->pMenuBar->numMenus);

    // if in a pick window like File/Open
    if(pNmData->pfnSubMenu)
    {
      if(!(pNmData->pfnSubMenu)(key, &pPicked->pickedSubItem))
      {
        fPicked = (pPicked->pickedSubItem == NEDMENU_PICK_NONE) ? FALSE : TRUE;
        fPicked = NmSetPicked(pNmData, pPicked, fPicked);
        NmMenuExit(pNmData);
      }
    }

    // get out of menu system
    else if(key == NEDKEY_ESC)
    {
      fPicked = NmSetPicked(pNmData, pPicked, FALSE);
      NmMenuExit(pNmData);
    }

    // chosing an item in the menu system
    else if(key == NEDKEY_ENTER)
    {
      // enter picks an item if in submenu
      if(pNmData->pickedItem != NEDMENU_PICK_NONE)
      {
        pNmData->pfnSubMenu = NmGetSubMenu(pNmData);
        if(pNmData->pfnSubMenu)
        {
          // let submenu intialize/draw itself
          (pNmData->pfnSubMenu)(NEDKEY_MENU_ENTER, &pNmData->pickedSubItem);
        }
        else
        {
          fPicked = NmSetPicked(pNmData, pPicked, TRUE);
          NmMenuExit(pNmData);
        }
      }

      // enter opens a submenu if still on menu bar
      else
      {
        pPicked->pickedItem   = 0;
        NmDrawSubMenu(pNmData);
      }
    }

    else if(key == NEDKEY_RIGHT)
    {
      if(pNmData->pickedMenu + 1 < pNmData->pMenuBar->numMenus)
      {
        NmCloseSubmenu(pNmData);
        ++pNmData->pickedMenu;
        NmDrawMenuBar(pNmData);
        if(pNmData->pickedItem != NEDMENU_PICK_NONE)
        {
          pNmData->pickedItem = 0;
          NmDrawSubMenu(pNmData);
        }
      }
    }
    else if(key == NEDKEY_LEFT)
    {
      if(pNmData->pickedMenu > 0)
      {
        NmCloseSubmenu(pNmData);
        --pNmData->pickedMenu;
        NmDrawMenuBar(pNmData);
        if(pNmData->pickedItem != NEDMENU_PICK_NONE)
        {
          pNmData->pickedItem = 0;
          NmDrawSubMenu(pNmData);
        }
      }
    }
    else if(key == NEDKEY_UP)
    {
      // only does something if in submenu
      if( (pNmData->pickedItem != NEDMENU_PICK_NONE) && (pNmData->pickedItem > 0) )
      {
        --pNmData->pickedItem;
        NmDrawSubMenu(pNmData);
      }
    }

    else if(key == NEDKEY_DOWN)
    {
      // must be on a menu
      NedAssertDbg(pNmData->pickedMenu != NEDMENU_PICK_NONE);
      if(pNmData->pickedItem == NEDMENU_PICK_NONE)
      {
        pNmData->pickedItem = 0;
        NmDrawSubMenu(pNmData);
      }
      else
      {
        pMenu = &pNmData->pMenuBar->aMenus[pNmData->pickedMenu];
        if(pNmData->pickedItem + 1 < pMenu->numItems)
        {
          ++pNmData->pickedItem;
          NmDrawSubMenu(pNmData);
        }
      }
    }

    // check hotkey: if chosing a menu
    else if(NmPickMenuBarMenu(hMenu, key))
    {
      NmDrawMenuBar(pNmData);
      NmDrawSubMenu(pNmData);
    }

    // check hotkey: if chosing an item in a menu
    else if(NmPickMenuItem(hMenu, key))
    {
      NmDrawSubMenu(pNmData);
      pNmData->pfnSubMenu = NmGetSubMenu(pNmData);
      if(pNmData->pfnSubMenu)
      {
        // let submenu intialize/draw itself
        (pNmData->pfnSubMenu)(NEDKEY_NONE, &pNmData->pickedSubItem);
      }
      else
      {
        fPicked = NmSetPicked(pNmData, pPicked, TRUE);
        NmMenuExit(pNmData);
      }
    }
  }

  // update screen if not in submenu (because submenu updates its own screen)
  if(pNmData->pfnSubMenu == NULL)
  {
    NedWinFlush();
    NedWinFlushCursor();
  }
  NedWinPick(pWinOld);

  return fPicked;
}

/*!------------------------------------------------------------------------------------------------
  Lets a higher layer know whether to keep feeding the menu system keys.

  @ingroup  ned_menu
  @param    pMenu       menu handle

  @return   TRUE if in menu
*///-----------------------------------------------------------------------------------------------
bool_t NedMenuInMenu(void *pMenu)
{
  sNmData_t    *pNmData   = pMenu;
  bool_t        fInMenu   = FALSE;

  NedAssert(NedMenuIsMenu(pNmData));
  if(NedMenuIsMenu(pNmData))
    fInMenu = pNmData->fInMenu;
  return fInMenu;
}

/*-------------------------------------------------------------------------------------------------
  Internal function draws the main menu bar to the screen.
-------------------------------------------------------------------------------------------------*/
static void NmDrawMenuBar(sNmData_t *pNmData)
{
  const sNedMenuBar_t     *pMenuBar;
  nedTheme_t              *pTheme;
  unsigned                i;
  nedAttr_t               attrText;
  nedAttr_t               attrHotkey;
  winPos_t                cols;

  // validate parameters
  NedAssertDbg(pNmData);
  NedAssertDbg(pNmData->pMenuBar);
  NedAssertDbg(pNmData->pWinMenuBar);
  NedAssertDbg(pNmData->pWinParent);

  // get colors
  pMenuBar = pNmData->pMenuBar;
  pTheme = NedThemeGet(pNmData->themeIndex);

  // adjust size if needed
  NedWinPick(pNmData->pWinParent);
  NedWinGetSize(NULL, &cols);
  NedWinPick(pNmData->pWinMenuBar);
  NedWinSetAttr(pTheme->menuText);
  NedWinResize(1, cols);

  // draw menu bar
  NedWinClearAll();
  NedWinGoto(0,0);
  for(i=0; i<pMenuBar->numMenus; ++i)
  {
    if(i == pNmData->pickedMenu)
    {
      attrText   = pTheme->menuDropText;
      attrHotkey = pTheme->menuDropHotkey;
    }
    else
    {
      attrText   = pTheme->menuText;
      attrHotkey = pTheme->menuHotkey;
    }
    NmPuts(attrText, attrHotkey, pMenuBar->aMenus[i].szName, FALSE);
    NedWinSetAttr(pTheme->menuText);
    NedWinPrintf("  ");
  }

  NedWinFlush();
}

/*-------------------------------------------------------------------------------------------------
  Draw the submenu, including highlighting the picked item
-------------------------------------------------------------------------------------------------*/
static void NmDrawSubMenu(sNmData_t *pNmData)
{
  const sNedMenu_t  *pMenu;
  nedTheme_t        *pTheme;
  unsigned           i;
  winPos_t           top;
  winPos_t           left;
  winPos_t           rows;
  winPos_t           cols;
  unsigned           len;
  nedAttr_t          text;
  nedAttr_t          hotkey;

  NedLogPrintfEx(NEDLOG_MENU, "NmDrawSubMenu(fInMenu=%u,pickedMenu=%u,pickedItem=%u)\n",
    pNmData->fInMenu, pNmData->pickedMenu, pNmData->pickedItem);

  // only draw submenu if the menu and an item is picked, otherwise do nothing
  if((pNmData->pickedMenu != NEDMENU_PICK_NONE) && (pNmData->pickedItem != NEDMENU_PICK_NONE))
  {
    pMenu   = &pNmData->pMenuBar->aMenus[pNmData->pickedMenu];
    pTheme = NedThemeGet(pNmData->themeIndex);

    // no window menu yet? create it
    if(pNmData->pWinMenu == NULL)
    {
      NmCalcMenuPos(pNmData, pNmData->pickedMenu, &top, &left);
      NmCalcMenuSize(pNmData, pNmData->pickedMenu, &rows, &cols);
      NedLogPrintfEx(NEDLOG_MENU, "  top=%u, left=%u, rows=%u, cols=%u\n", top, left, rows, cols);
      pNmData->pWinMenu = NedWinNew(top, left, rows, cols);
      if(pNmData->pWinMenu)
      {
        NedWinFrameSetAttr(pTheme->menuText);
      }
    }

    // draw the window with picked item
    if(pNmData->pWinMenu)
    {
      NedWinPick(pNmData->pWinMenu);
      NedWinGetSize(&rows, &cols);
      NedLogPrintfEx(NEDLOG_MENU, "  rows=%u, cols=%u\n",rows,cols);

      for(i=0; i<rows; ++i)
      {
        NedWinGoto(i,0);
        text      = pTheme->menuText;
        hotkey    = pTheme->menuHotkey;
        if(i == pNmData->pickedItem)
        {
          NedWinFrameLinePick(i);
          text    = pTheme->menuDropText;
          hotkey  = pTheme->menuDropHotkey;
        }
        len = cols - NmPuts(text, hotkey, pMenu->aItems[i].szName, pMenu->fNoHotKey);
        NedWinSetAttr(text);
        if(pMenu->aItems[i].hotkey == NEDKEY_NONE)
          NedWinPrintf("%*s",len, "");
        else
          NedWinPrintf("%*s",len, NedKeyName(pMenu->aItems[i].hotkey));
      }

      // put cursor on lower part of frame
      NedWinGetPos(&top, &left);
      NedWinPick(NedWinGetBackdrop());
      NedWinGoto(top + rows, left + cols);
      NedWinFlushCursor();
    }
    NedWinFlush();
  }

  return;
}

/*-------------------------------------------------------------------------------------------------
  Close the submenu if needed
-------------------------------------------------------------------------------------------------*/
static void NmCloseSubmenu(sNmData_t *pNmData)
{
  if(pNmData->pWinMenu)
  {
    NedWinClose(pNmData->pWinMenu);
    pNmData->pWinMenu = NULL;
  }
  return;
}

/*-------------------------------------------------------------------------------------------------
  No longer in the menu
-------------------------------------------------------------------------------------------------*/
static void NmMenuExit(sNmData_t *pNmData)
{
  pNmData->fInMenu        = FALSE;
  pNmData->pickedMenu     = NEDMENU_PICK_NONE;
  pNmData->pickedItem     = NEDMENU_PICK_NONE;
  pNmData->pickedSubItem  = NEDMENU_PICK_NONE;
  pNmData->pfnSubMenu     = NULL;
  NmCloseSubmenu(pNmData);
  NmDrawMenuBar(pNmData);
}

/*-------------------------------------------------------------------------------------------------
  Returns TRUE if in menu bar and user picked one of the menus. Sets pickedMenu if TRUE.
-------------------------------------------------------------------------------------------------*/
static bool_t NmPickMenuBarMenu(sNmData_t *pNmData, nedKey_t key)
{
  const sNedMenuBar_t  *pMenuBar  = pNmData->pMenuBar;
  const char           *psz;
  unsigned              i;
  bool_t                fPicked   = FALSE;

  // will only look at menu names if in menu bar
  if(pNmData->pickedItem == NEDMENU_PICK_NONE)
  {
    for(i=0; i<pMenuBar->numMenus; ++i)
    {
      psz = strchr(pMenuBar->aMenus[i].szName, MENU_HOTCHAR);
      if( psz && (toupper(key) == toupper(psz[1])) )
      {
        pNmData->pickedMenu = i;
        fPicked = TRUE;
        break;
      }
    }
  }
  return fPicked;
}

/*-------------------------------------------------------------------------------------------------
  Returns TRUE if in submenu and user picked one of the items. Sets pickedItems if TRUE.
-------------------------------------------------------------------------------------------------*/
static bool_t NmPickMenuItem(sNmData_t *pNmData, nedKey_t key)
{
  const sNedMenu_t   *pMenu;
  const char         *psz;
  unsigned            i;
  bool_t              fPicked   = FALSE;

  if( (pNmData->pickedMenu != NEDMENU_PICK_NONE) && (pNmData->pickedItem != NEDMENU_PICK_NONE) &&
      (key != NEDKEY_NONE) )
  {
    pMenu = &pNmData->pMenuBar->aMenus[pNmData->pickedMenu];
    for(i=0; i<pMenu->numItems; ++i)
    {
      psz = strchr(pMenu->aItems[i].szName, MENU_HOTCHAR);
      if( (key == pMenu->aItems[i].hotkey) || 
          (psz && (toupper(key) == toupper(psz[1]))) )
      {
        pNmData->pickedItem = i;
        fPicked             = TRUE;
      }
    }
  }
  return fPicked;
}


/*-------------------------------------------------------------------------------------------------
  Calculate the top, left of menu
-------------------------------------------------------------------------------------------------*/
static void NmCalcMenuPos(sNmData_t *pNmData, unsigned nMenu, winPos_t *pTop, winPos_t *pLeft)
{
  void       *pOldWin;
  winPos_t    top;
  winPos_t    left;
  winPos_t    menuRows;
  winPos_t    menuCols;
  winPos_t    parentCols;
  unsigned    i;

  pOldWin = NedWinPick(pNmData->pWinParent);
  NedWinGetPos(&top, &left);
  NedWinGetSize(NULL, &parentCols);

  // lines up with menu bar entry, with room for frame
  if(nMenu < pNmData->pMenuBar->numMenus)
  {
    for(i=0; i<nMenu; ++i)
      left += NmStrLen(pNmData->pMenuBar->aMenus[i].szName) + MENU_SPACE;
  }
  top  += 2;
  if(left <= 0)
    left = 1;

  // keep all within boundaries of parent window if possible
  NmCalcMenuSize(pNmData, nMenu, &menuRows, &menuCols);
  if(left + menuCols + 1 > parentCols)
    left = parentCols - (menuCols + 1);
  if(left <= 0)
    left = 1;

  NedWinPick(pOldWin);

  // report top/left
  *pTop   = top;
  *pLeft  = left;
}

/*-------------------------------------------------------------------------------------------------
  Calculate the width of the picked menu.
-------------------------------------------------------------------------------------------------*/
static void NmCalcMenuSize(sNmData_t *pNmData, unsigned nMenu, winPos_t *pRows, winPos_t *pCols)
{
  const sNedMenu_t      *pMenu;
  const sNedMenuItem_t  *aItems;
  unsigned               rows        = 0;
  unsigned               cols        = 0;
  unsigned               lenMaxTitle = 0;
  unsigned               lenMaxKey   = 0;
  unsigned               len;
  unsigned               i;

  if(nMenu != NEDMENU_PICK_NONE)
  {
    pMenu   = &pNmData->pMenuBar->aMenus[nMenu];
    aItems  = pMenu->aItems;
    rows    = pMenu->numItems;

    NedLogPrintfEx(NEDLOG_MENU, "NmCalcMenuSize(pMenu %p aItems %p, numItems %u, nMenu %u\n",
        pMenu, aItems, rows, nMenu);

    for(i=0; i<rows; ++i)
    {
      if(pMenu->fNoHotKey)
        len = strlen(aItems[i].szName);
      else
        len = NmStrLen(aItems[i].szName);   // don't include hotkey in strlen
      if(len > lenMaxTitle)
        lenMaxTitle = len;
      if(aItems[i].hotkey != NEDKEY_NONE)
      {
        len = strlen(NedKeyName(aItems[i].hotkey));
        if(len > lenMaxKey)
          lenMaxKey = len;
      }
    }
  }

  // columns allow room for longest title and longest key, plus some white space
  // if no hotkeys, then just room for longest title
  cols = lenMaxTitle + lenMaxKey;
  if(lenMaxKey)
    cols += MENU_SPACE;

  *pRows = rows;
  *pCols = cols;
}

/*-------------------------------------------------------------------------------------------------
  Internal function, draws a string. Assumes menu bar or menu window is already
  picked. An underscore indicates a highlighted character (e.g. _File highlights the
  'F', whereas E_dit highlights the 'd').

  Returns Length of string without the underscore (e.g. "_File" returns length of 4)
-------------------------------------------------------------------------------------------------*/
static unsigned NmPuts(nedAttr_t attrText, nedAttr_t attrHotkey, const char *sz, bool_t fNoHotKey)
{
  unsigned        len = 0;

  // NedLogPrintfEx(NEDLOG_MENU, "NmPuts(attrText=%02x, attrHotkey=%02x, sz=%s\n",attrText,attrHotkey,sz);

  // print highlighted or normal items
  NedWinSetAttr(attrText);
  for(len=0; *sz && len < NEDWIN_MAX_COLS; ++sz)
  {
    // underscores be
    if(!fNoHotKey && *sz == MENU_HOTCHAR)
    {
      NedWinSetAttr(attrHotkey);
      continue;
    }
    else
    {
      ++len;
    }
    NedWinPutc(*sz);
    NedWinSetAttr(attrText);
  }

  return len;
}

/*-------------------------------------------------------------------------------------------------
  Determine length of string, excluding underscores.  So "_File" is length 4, so is "E_dit".
-------------------------------------------------------------------------------------------------*/
static unsigned NmStrLen(const char *sz)
{
  unsigned  len = 0;

  for(len=0; *sz; ++sz)
  {
    if(*sz == MENU_HOTCHAR)
      continue;
    ++len;
  }

  return len;
}

/*-------------------------------------------------------------------------------------------------
  Determine max len in an array of strings
-------------------------------------------------------------------------------------------------*/
static unsigned NmMaxLenStrings(unsigned numItems, const char **ppItems)
{
  unsigned    i;
  unsigned    thisLen;
  unsigned    len = 0;

  for(i=0; i<numItems; ++i)
  {
    thisLen = strlen(ppItems[i]);
    if(thisLen > len)
      len = thisLen;
  }
  return len;
}

/*-------------------------------------------------------------------------------------------------
  Seets the pPickedItem appropriately. Always returns TRUE.
-------------------------------------------------------------------------------------------------*/
static bool_t NmSetPicked(sNmData_t *pNmData, sMenuPicked_t *pPicked, bool_t fPicked)
{
  pPicked->fPicked       = fPicked;
  if(fPicked)
  {  
    pPicked->pickedMenu    = pNmData->pickedMenu;
    pPicked->pickedItem    = pNmData->pickedItem;
    pPicked->pickedItem    = pNmData->pickedItem;
  }
  else
  {
    pPicked->pickedMenu    = NEDMENU_PICK_NONE;
    pPicked->pickedItem    = NEDMENU_PICK_NONE;
    pPicked->pickedItem    = NEDMENU_PICK_NONE;
  }
  return TRUE;
}

/*-------------------------------------------------------------------------------------------------
  Return the submenu for this item or NULL
-------------------------------------------------------------------------------------------------*/
static pfnSubMenu_t NmGetSubMenu(sNmData_t *pNmData)
{
  sNedMenuItem_t    *pMenuItem;
  pfnSubMenu_t      pfnSubMenu = NULL;

  pMenuItem = (sNedMenuItem_t *)NedMenuItemPtr(pNmData, pNmData->pickedMenu, pNmData->pickedItem);
  if(pMenuItem)
    pfnSubMenu = pMenuItem->pfnSubMenu;

  return pfnSubMenu;
}

/*!------------------------------------------------------------------------------------------------
  Create a new pick window. Not displayed until NedPickDraw()

  @param    top         window top
  @param    left        window left
  @param    numItems    1-n, number of items in the array of string pointers
  @param    ppszItems   array of numItems string pointers
  @param    curItem     0-n, index into items

  @return   handle to pick window or NULL
*///-----------------------------------------------------------------------------------------------
hPick_t NedPickNew(unsigned top, unsigned left, unsigned numItems, const char **ppszItems, unsigned curItem)
{
  sNmPick_t    *pPick;

  NedLogPrintfEx(NEDLOG_MENU, "NedPickNew(top=%u, left=%u, numItems=%u, ppszItems %p, curItem=%u\n",
    top, left, numItems, ppszItems, curItem);

  if(curItem >= numItems)
    curItem = 0;

  pPick = malloc(sizeof(*pPick));
  if(pPick)
  {
    memset(pPick, 0, sizeof(sNmPick_t));
    pPick->sanchk         = PICK_SANCHK;
    pPick->top            = top;
    pPick->desiredLeft    = left;
    pPick->numItems       = numItems;
    pPick->ppszItems      = ppszItems;
    pPick->curItem        = curItem;
    NedPickAdjust(pPick); // adjusts rows, cols, left
    NedLogPrintfEx(NEDLOG_MENU, "NedPickNew: rows %d\n", pPick->rows);
  }

  return pPick;
}

/*!------------------------------------------------------------------------------------------------
  Free a pick window

  @ingroup  ned_menu
  @param    hPick       handle to pick window

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedPickFree(hPick_t hPick)
{
  sNmPick_t    *pPick = hPick;
  if(NedPickIsPick(hPick))
  {
    if(pPick->pWin)
      NedWinClose(pPick->pWin);
    free(pPick);
  }
}

/*!------------------------------------------------------------------------------------------------
  Is this a pick window?

  @ingroup  ned_menu
  @param    hPick       handle to pick window

  @return   TRUE if this is a pick window, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedPickIsPick(hPick_t hPick)
{
  sNmPick_t    *pPick = hPick;
  return (pPick && (pPick->sanchk == PICK_SANCHK)) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Dump the pick window structure to the log file.

  @ingroup  ned_menu
  @param    hPick       handle to pick window

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedPickLogShow(hPick_t hPick)
{
  sNmPick_t    *pPick = hPick;
  NedLogPrintfEx(NEDLOG_MENU, "Pick Window: curItem %u, numItems %u, topItem %u, top %d, left %d, rows %d, cols %d\n",
    pPick->curItem, pPick->numItems, pPick->topItem, pPick->top, pPick->left, pPick->rows, pPick->cols);
}

/*!------------------------------------------------------------------------------------------------
  Dump the pick window structure to the log file.

  @ingroup  ned_menu
  @param    hPick       handle to pick window

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedPickDraw(hPick_t hPick)
{
  sNmPick_t    *pPick = hPick;
  unsigned      i;
  void         *pOldWin;

  pOldWin = NedWinGetPicked();

  // nothing to do, no items in pick list
  NedAssert(NedPickIsPick(hPick));
  if(!pPick->numItems)
    return;

  if(!pPick->pWin)
  {
    NedPickAdjust(pPick);
    pPick->pWin = NedWinNewEx(pPick->top, pPick->left, pPick->rows, pPick->cols,
                              pPick->text, TRUE, pPick->text);
  }
  else
    NedWinPick(pPick->pWin);
  NedAssert(NedWinIsWin(pPick->pWin));

  // adjust if window size has changed last draw
  if(pPick->top != NedWinGetTop() || pPick->left != NedWinGetLeft() || 
    pPick->rows != NedWinGetRows() || pPick->cols != NedWinGetCols())
  {
    NedPickAdjust(pPick);
    NedWinMove(pPick->top, pPick->left);
    NedWinResize(pPick->rows, pPick->cols);
  }

  // draw all the pick items
  for(i=0; i<pPick->rows; ++i)
  {
    NedWinGoto((winPos_t)i, 0);
    NedWinSetAttr((i == (pPick->curItem - pPick->topItem)) ? pPick->highlight : pPick->text);
    NedWinPrintf("%s", pPick->ppszItems[pPick->topItem + i]);
    NedWinClearEol();
  }

  // set cursor to the picked line
  NedWinGoto((winPos_t)(pPick->curItem - pPick->topItem), pPick->cols-1);
  NedWinFlush();
  NedWinFlushCursor();

  // pick the previous window
  NedWinPick(pOldWin);
}

/*!------------------------------------------------------------------------------------------------
  Set the items for the pick window. The number of items may be more than the # of rows set by
  NedPickAdjust().

  @ingroup  ned_menu
  @param    hPick       handle to pick window
  @param    numItems    number of items in the array of string pointers
  @param    ppszItems   array of string pointers

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedPickSetItems(hPick_t hPick, unsigned numItems, const char **ppszItems, unsigned curItem)
{
  sNmPick_t    *pPick = hPick;
  if(curItem >= numItems)
    curItem = 0;
  if(NedPickIsPick(hPick))
  {
    pPick->numItems   = numItems;
    pPick->curItem    = curItem;
    pPick->ppszItems  = ppszItems;
    pPick->curItem    = pPick->topItem = 0;
    NedPickAdjust(hPick);
  }  
}

/*!------------------------------------------------------------------------------------------------
  Get the item at the index nItem. Use NedPickSetItems() to change the items.

  @param    hPick       handle to pick window
  @param    nItems      item in the pick list, ASCIIZ string
  @return   ptr to item string
*///-----------------------------------------------------------------------------------------------
const char * NedPickGetItem(hPick_t hPick, unsigned nItem)
{
  sNmPick_t    *pPick = hPick;
  const char   *psz   = NULL;

  if(NedPickIsPick(hPick) && nItem < pPick->numItems)
      psz = pPick->ppszItems[nItem];
  return psz;
}

/*!------------------------------------------------------------------------------------------------
  Adjust rows, left, cols based on data

  @param    hPick       handle to pick window
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedPickAdjust(hPick_t hPick)
{
  sNmPick_t    *pPick = hPick;
  winPos_t      screenRows = NEDMIN_ROWS;
  winPos_t      screenCols = NEDMIN_COLS;
  winPos_t      rows;
  winPos_t      left;

  NedAssert(NedPickIsPick(hPick));
  NedWinGetScreenSize(&screenRows, &screenCols);

  // adjust columns/left for window as contents may have changed
  pPick->cols = NmMaxLenStrings(pPick->numItems, pPick->ppszItems);
  left = pPick->desiredLeft;
  if(left + pPick->cols + 1 > screenCols)
    left = screenCols - (pPick->cols + 1);  // make sure right frame is visisble
  if(left < 0)  // make sure window if smaller than string items isn't off left side
    left = 0;
  pPick->left = left;

  // adjust rows for screen
  rows = pPick->numItems;
  if(rows > screenRows - (NEDMIN_ROWS - 1))
    rows = screenRows - (NEDMIN_ROWS - 1);
  if(rows > NEDMENU_PICK_MAX)
    rows = NEDMENU_PICK_MAX;
  pPick->rows = rows;

  NedLogPrintfEx(NEDLOG_MENU, "NedPickAdjust(newRows %d, newCols %d, newLeft %d, screenCols %d)\n",
    pPick->rows, pPick->cols, pPick->left, screenCols);
}

/*!------------------------------------------------------------------------------------------------
  Set pick window colors. Won't change display until next NedPickDraw().

  @ingroup  ned_menu
  @param    hPick       handle to pick window
  @param    nItems      item in the pick list, ASCIIZ string

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedPickSetColors(hPick_t hPick, nedAttr_t text, nedAttr_t highlight)
{
  sNmPick_t    *pPick = hPick;
  
  if(NedPickIsPick(hPick))
  {
    pPick->text      = text;
    pPick->highlight = highlight;
  }
}

/*!------------------------------------------------------------------------------------------------
  Feed a key into the pick window. Keep feeding keys until this function returns TRUE. Use
  NedPickDraw() after each call to this function.

  @ingroup  ned_menu
  @param    pMenu         menu handle
  @param    key           key to feed into menu systm
  @param    pPickedItem   return value 0-n, or NEDMENU_PICK_NONE

  @return   TRUE if still in pick window, FALSE out of menu (ENTER or ESC)
*///-----------------------------------------------------------------------------------------------
bool_t NedPickFeedKey(hPick_t hPick, nedKey_t key, unsigned *pPickedItem)
{
  sNmPick_t  *pPick         = hPick;
  bool_t      fInPickWindow = TRUE;
  unsigned    thisAmount;

  if(!NedPickIsPick(hPick))
    return FALSE;

  switch(key)
  {
    case NEDKEY_ESC:
      if(pPick->pWin)
      {
        NedWinClose(pPick->pWin);
        pPick->pWin = NULL;
      }
      fInPickWindow = FALSE;
      *pPickedItem  = NEDMENU_PICK_NONE;
    break;

    case NEDKEY_ENTER:
      if(pPick->pWin)
      {
        NedWinClose(pPick->pWin);
        pPick->pWin = NULL;
      }
      fInPickWindow = FALSE;
      *pPickedItem  = pPick->curItem;
    break;

    case NEDKEY_UP:
      if(pPick->curItem > 0)
      {
        --pPick->curItem;
        if(pPick->curItem < pPick->topItem)
          pPick->topItem = pPick->curItem;
      }
    break;

    case NEDKEY_DOWN:
      if(pPick->curItem + 1 < pPick->numItems)
      {
        ++pPick->curItem;
        if(pPick->curItem >= pPick->topItem + pPick->rows)
          pPick->topItem = pPick->curItem - pPick->rows + 1;
      }
    break;

    case NEDKEY_PGUP:
      if(pPick->topItem > 0)
      {
        if(pPick->topItem < pPick->rows)
          thisAmount = pPick->topItem;
        else
          thisAmount = pPick->rows;
        pPick->topItem -= thisAmount;
        pPick->curItem -= thisAmount;
      }
      else
        pPick->curItem = 0;
    break;

    case NEDKEY_PGDN:
      pPick->curItem += pPick->rows;
      if(pPick->curItem >= pPick->numItems)
        pPick->curItem = pPick->numItems - 1;

      pPick->topItem += pPick->rows;
      if(pPick->topItem + pPick->rows >= pPick->numItems)
        pPick->topItem = pPick->numItems - pPick->rows;
    break;

    case NEDKEY_HOME:
    case NEDKEY_CTRL_PGUP:
      pPick->curItem = 0;
      pPick->topItem = 0;
    break;

    case NEDKEY_END:
    case NEDKEY_CTRL_PGDN:
      pPick->curItem = pPick->numItems - 1;
      if(pPick->numItems > pPick->rows)
        pPick->topItem = pPick->numItems - pPick->rows;
      else
        pPick->topItem = 0;
    break;
  }

  return fInPickWindow; // TRUE to keep feeding keys
}
