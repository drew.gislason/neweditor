/*!************************************************************************************************
  @file NedPoint.c

  @brief  Ned Point Implementation

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include <ctype.h>
#include <string.h>
#include "NedLog.h"
#include "NedPoint.h"


/*!------------------------------------------------------------------------------------------------
  The point is now part of this buffer chain, at offset 0 within the buffer.

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point
  @param    pBufHead     pointer to preallocated buffer head

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedPointInit(sNedPoint_t *pPoint, sNedBuffer_t *pBuf)
{
  NedAssertDbg(pPoint);
  NedAssertDbg(NedBufIsBuf(pBuf));

  pPoint->sanchk  = NEDPOINT_SANCHK;
  pPoint->offset  = 0;
  pPoint->pBuf    = pBuf;
}

/*!------------------------------------------------------------------------------------------------
  Is this a valid point?

  @ingroup  ned_point
  @param    pPoint       pointer to point

  @return   TRUE if valid point, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedPointIsPoint(const sNedPoint_t *pPoint)
{
  // NedLogPrintfEx(NEDLOG_POINT, "NedPointIsPoint(%p), buf=%p, id=%u, offset=%u\n", pPoint, pPoint->pBuf
  //                                                                pPoint->id,pPoint->offset);
  return ( (pPoint != NULL) && (pPoint->sanchk == NEDPOINT_SANCHK) &&
           NedBufIsBuf(pPoint->pBuf) ) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  The point is now part of this buffer chain, at offset 0 within the buffer.

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point
  @param    pBufHead     pointer to preallocated buffer head

  @return   TRUE if points are the same
*///-----------------------------------------------------------------------------------------------
bool_t NedPointIsSame(const sNedPoint_t *pThis, const sNedPoint_t *pThat)
{
  NedAssertDbg(NedPointIsPoint(pThis) && NedPointIsPoint(pThat));
  return ((pThis->pBuf == pThat->pBuf) && (pThis->offset == pThat->offset)) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Determine "indent" of file. Search from point forward

  @ingroup  ned_point
  @param    pPoint       pointer to valid point

  @return   indent 0 if not found, 2-n if found
*///-----------------------------------------------------------------------------------------------
unsigned NedPointFileIndent(const sNedPoint_t *pPoint)
{
  long        count   = 0L;
  sNedPoint_t sPoint  = *pPoint;

  while(1)
  {
    if(!NedPointLineNext(&sPoint))
      break;
    count = NedPointCharCount(&sPoint, ' ');
    if(count >= 2)
      break;
  }

  return count;
}

/*!------------------------------------------------------------------------------------------------
  Determine size of file. Does not move point

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point

  @return   none
*///-----------------------------------------------------------------------------------------------
long NedPointFileSize(const sNedPoint_t *pPoint)
{
  long  fileSize = 0L;
  sNedPoint_t sPoint = *pPoint;
  NedPointFileTop(&sPoint);
  NedBufChainSize(sPoint.pBuf, &fileSize);
  return fileSize;
}

/*!------------------------------------------------------------------------------------------------
  Go to the top of the file.

  @ingroup  ned_point
  @param    pPoint       pointer to valid point

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedPointFileTop(sNedPoint_t *pPoint)
{
  NedAssertDbg(NedPointIsPoint(pPoint));

  pPoint->pBuf    = NedBufHead(pPoint->pBuf);
  pPoint->offset  = 0;
}

/*!------------------------------------------------------------------------------------------------
  Go to a position in the file. See NedPointPos().

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedPointFileGotoPos(sNedPoint_t *pPoint, long pos)
{
  NedPointFileTop(pPoint);
  while(pos)
  {
    // position in this buffer
    if(pos < NedBufLen(pPoint->pBuf))
    {
      pPoint->offset = (unsigned)pos;
      return;
    }

    // at end of file
    if(!NedBufNext(pPoint->pBuf))
    {
      pPoint->offset = NedBufLen(pPoint->pBuf);
      return;
    }
    pos          -= NedBufLen(pPoint->pBuf);
    pPoint->pBuf  = NedBufNext(pPoint->pBuf);
  }
}

/*!------------------------------------------------------------------------------------------------
  Go to the bottom of the file.

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedPointFileBottom(sNedPoint_t *pPoint)
{
  NedAssertDbg(NedPointIsPoint(pPoint));

  pPoint->pBuf    = NedBufTail(pPoint->pBuf);
  pPoint->offset  = NedBufLen(pPoint->pBuf);
}

/*!------------------------------------------------------------------------------------------------
  Count the # of characters at this point that are c.

  @ingroup  ned_point
  @param    pPoint       pointer to valid point

  @return   indent size 2-8
*///-----------------------------------------------------------------------------------------------
long NedPointCharCount(const sNedPoint_t *pPoint, nedChar_t c)
{
  long        count   = 0L;
  sNedPoint_t sPoint  = *pPoint;

  while(1)
  {
    if(NedPointCharGet(&sPoint) != c)
      break;

    ++count;
    NedPointCharNext(&sPoint);
  }

  return count;
}

/*!------------------------------------------------------------------------------------------------
  Get the character at the point. May be NEDCHAR_EOF if passed end of file.

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point

  @return   0-0xff, or NEDCHAR_EOF
*///-----------------------------------------------------------------------------------------------
nedChar_t NedPointCharGet(const sNedPoint_t *pPoint)
{
  unsigned  c;

  NedAssertDbg(NedPointIsPoint(pPoint));

  if(NedPointIsAtBottom(pPoint))
    c = NEDCHAR_EOF;
  else
    c = NedBufData(pPoint->pBuf)[pPoint->offset];
  return c;
}

/*!------------------------------------------------------------------------------------------------
  Advance the point, then get the character. If NEDCHAR_EOF then already at bottom of file.

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point

  @return   0-0xff, or NEDCHAR_EOF
*///-----------------------------------------------------------------------------------------------
nedChar_t NedPointCharGetNext(sNedPoint_t *pPoint)
{
  NedAssertDbg(NedPointIsPoint(pPoint));

  NedPointCharNext(pPoint);
  return NedPointCharGet(pPoint);
}

/*!------------------------------------------------------------------------------------------------
  Decrease the point, then get the character. If NEDCHAR_EOF then already at top of file.

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point

  @return   0-0xff, or NEDCHAR_EOF
*///-----------------------------------------------------------------------------------------------
nedChar_t NedPointCharGetPrev(sNedPoint_t *pPoint)
{
  NedAssertDbg(NedPointIsPoint(pPoint));

  if(!NedPointCharPrev(pPoint))
    return NEDCHAR_EOF;

  return NedPointCharGet(pPoint);
}

/*!------------------------------------------------------------------------------------------------
  Move point to the next character. Skips the CR in CR/LF pairs if Windows-style line endings, that
  is "abc\r\n", at "c" next char is "\n".

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point

  @return   TRUE if pointer moved, FALSE if at EOF
*///-----------------------------------------------------------------------------------------------
bool_t NedPointCharNext(sNedPoint_t *pPoint)
{
  bool_t    fMoved  = FALSE;
  unsigned  i;

  NedAssertDbg(NedPointIsPoint(pPoint));

  // skip at most one /r
  for(i=0; i<2; ++i)
  {
    // already at end of file
    if( NedPointIsAtBottom(pPoint) )
      break;

    // move on to next char. Last buf only allows being on BufLen.
    ++pPoint->offset;
    if( (pPoint->offset >= NedBufLen(pPoint->pBuf)) && NedBufNext(pPoint->pBuf) )
    {
      pPoint->pBuf    = NedBufNext(pPoint->pBuf);
      pPoint->offset  = 0;
    }

    // skips the CR in CR/LF pair. That is "abc\r\n", at "c" next char is "\n".
    if( (i == 0) && (NedPointCharGet(pPoint) == '\r') )
      continue;
    else
    {
      fMoved = TRUE;
      break;
    }
  }

  return fMoved;
}

/*!------------------------------------------------------------------------------------------------
  Go to the previous character. Skips the CR in CR/LF pairs if Windows-style line endings, that is
  "abc\r\n", at "\n" prev char is "c".

  @ingroup  ned_point
  @param    pPoint       pointer to valid point

  @return   TRUE if moved.
*///-----------------------------------------------------------------------------------------------
bool_t NedPointCharPrev(sNedPoint_t *pPoint)
{
  bool_t    fMoved  = FALSE;
  unsigned  i;

  NedAssertDbg(NedPointIsPoint(pPoint));

  for(i=0; i<2; ++i)
  {
    // already at top of file
    if( NedPointIsAtTop(pPoint) )
      break;

    // move to prev char
    if(pPoint->offset == 0)
    {
      pPoint->pBuf    = NedBufPrev(pPoint->pBuf);
      NedAssertDbg(NedBufIsBuf(pPoint->pBuf));
      pPoint->offset  = NedBufLen(pPoint->pBuf) - 1;
    }
    else
    {
      --pPoint->offset;
    }
    fMoved = TRUE;

    // skip CR
    if( (i == 0) && NedPointCharGet(pPoint) == '\r' )
      continue;
    else
      break;
  }

  return fMoved;
}

/*!------------------------------------------------------------------------------------------------
  Advance the point by an amount (forward or backward), up to the top of the file or the bottom
  of the file. Does NOT pay attention to CR/LF. This is strictly bytes in buffers.

  @ingroup  ned_point
  @param    pPoint      pointer to valid point
  @param    amount      amount to advance (or retreat). May be <0 to go backward.

  @return   TRUE if moved, FALSE if already at top or bottom.
*///-----------------------------------------------------------------------------------------------
bool_t NedPointCharAdvance(sNedPoint_t *pPoint, long amount)
{
  bool_t        fMoved  = FALSE;
  unsigned      thisLen;
  sNedBuffer_t  *pBuf;

  NedAssert(pPoint);

  // advance backward toward top of file
  if(amount < 0)
  {
    amount = -1L * amount;
    while(amount)
    {
      if(pPoint->offset == 0)
      {
        // on to next buffer
        pBuf = NedBufPrev(pPoint->pBuf);
        if(!pBuf)
          break;
        pPoint->pBuf    = pBuf;
        pPoint->offset  = pBuf->len;
      }

      thisLen = pPoint->offset;
      if(thisLen > amount)
        thisLen = (unsigned)amount;

      pPoint->offset -= thisLen;
      amount         -= thisLen;
      fMoved = TRUE;
    }
  }

  // advance forward toward bottom of file
  else
  {
    while(amount)
    {
      thisLen = NedBufLen(pPoint->pBuf) - pPoint->offset;
      if(thisLen > amount)
        thisLen = (unsigned)amount;

      pPoint->offset += thisLen;
      amount         -= thisLen;
      fMoved = TRUE;

      if(pPoint->offset >= NedBufLen(pPoint->pBuf))
      {
        // on to next buffer (unless at bottom of file, in which case, we're done)
        pBuf = NedBufNext(pPoint->pBuf);
        if(!pBuf)
          break;
        pPoint->pBuf    = pBuf;
        pPoint->offset  = 0;
      }
    }
  }
  return fMoved;
}

/*!------------------------------------------------------------------------------------------------
  Go to the end of word, then skip whitespace. For example "abc( 5 );" anywhere on "abc(" would
  point to "5".

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point

  @return   FALSE if at EOF, otherwise TRUE
*///-----------------------------------------------------------------------------------------------
bool_t NedPointWordNext(sNedPoint_t *pPoint)
{
  nedChar_t   c;

  NedAssertDbg(NedPointIsPoint(pPoint));

  // if at end of line, just go to start of next line
  c = NedPointCharGet(pPoint);
  if(c == '\n')
    NedPointCharNext(pPoint);

  // otherwise, skip word and trailing whitespace
  else
  {
    if(!NedPointWordEnd(pPoint))
      return FALSE;

    // skip all following whitespace following the word
    c = NedPointCharGet(pPoint);
    while(NedCharType(c) == IS_SPACE)
    {
      c = NedPointCharGetNext(pPoint);
    }
  }

  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Go to the end of the word (same type of character). For example "abc( 5 );" anywhere on "abc
  (" would end up on the '('. 

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point

  @return   FALSE if at EOF, otherwise TRUE
*///-----------------------------------------------------------------------------------------------
bool_t NedPointWordEnd(sNedPoint_t *pPoint)
{
  nedCharType_t   type;
  nedChar_t       c;

  NedAssertDbg(NedPointIsPoint(pPoint));

  // check if we're at end of file
  c = NedPointCharGet(pPoint);
  if(c == NEDCHAR_EOF)
    return FALSE;

  type = NedCharType(c);
  // if(type == IS_SPACE)
  //   return TRUE;

  // skip all things of similar type
  while( NedCharType(c) == type )
  {
    c = NedPointCharGetNext(pPoint);
  }
  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Go to previous word. For example "   abc( 5 );" anywhere on "5" would end up on "(". Another
  previous word would end on "abc". Another previous word would end on start of line. Another
  previous word on end up on "\n" of previous line.

  @ingroup  ned_point
  @param    pPoint       pointer to valid point

  @return   TRUE if moved, FALSE if already at top of file
*///-----------------------------------------------------------------------------------------------
bool_t NedPointWordPrev(sNedPoint_t *pPoint)
{
  nedChar_t       c;

  NedAssertDbg(NedPointIsPoint(pPoint));

  // done if already at top of file
  if( !NedPointCharPrev(pPoint) )
    return FALSE;

  // skip all preceding whitespace
  c = NedPointCharGet(pPoint);
  while( NedCharType(c) == IS_SPACE )
  {
    // skip white space
    if( !NedPointCharPrev(pPoint) )
      return TRUE;
    c = NedPointCharGet(pPoint);

    // stay on 1st whitespace if at start of line
    if(c == '\n')
      NedPointCharNext(pPoint);
  }

  // at end of line, done
  if(c == '\n')
    return TRUE;

  // go to beginning of word
  NedPointWordBeg(pPoint);
  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Go to the beginning of word.

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point

  @return   FALSE if already at top of file, TRUE otherwise
*///-----------------------------------------------------------------------------------------------
bool_t NedPointWordBeg(sNedPoint_t *pPoint)
{
  nedCharType_t   type;
  nedChar_t       c;
  sNedPoint_t     sTestPoint;

  NedAssertDbg(NedPointIsPoint(pPoint));

  // already at top of file, nothing to do
  if(NedPointIsAtTop(pPoint))
    return FALSE;

  // skip all things of similar type
  c = NedPointCharGet(pPoint);
  type = NedCharType(c);
  while( !NedPointIsAtTop(pPoint) )
  {
    // get previous character without decrementing point
    sTestPoint = *pPoint;
    NedPointCharPrev(&sTestPoint);
    c = NedPointCharGet(&sTestPoint);

    // only decrement point if still on same type
    if(NedCharType(c) == type)
      *pPoint = sTestPoint;

    // otherwise, on something else, done
    else
      break;
  }

  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Go to the next line from anywhere in a line. If already at bottom line, do nothing

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point

  @return   TRUE if moved, FALSE if not (at EOF)
*///-----------------------------------------------------------------------------------------------
bool_t NedPointLineNext(sNedPoint_t *pPoint)
{
  sNedPoint_t  sPoint;

  NedAssertDbg(NedPointIsPoint(pPoint));

  sPoint = *pPoint;
  NedPointLineEnd(&sPoint);
  if(NedPointIsAtBottom(&sPoint))
    return FALSE;
  *pPoint = sPoint;
  return NedPointCharNext(pPoint);
}

/*!------------------------------------------------------------------------------------------------
  Go to previous line, from anywhere in a line. If already at top line, do nothing

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point

  @return   TRUE if moved, FALSE if not (at start of file).
*///-----------------------------------------------------------------------------------------------
bool_t NedPointLinePrev(sNedPoint_t *pPoint)
{
  sNedPoint_t   sPoint;
  NedAssertDbg(NedPointIsPoint(pPoint));

  sPoint = *pPoint;
  NedPointLineBeg(&sPoint);
  if(!NedPointCharPrev(&sPoint))
    return FALSE;
  NedPointLineBeg(&sPoint);
  *pPoint = sPoint;
  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Go to start of line. If already at the start of the line, do nothing.

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point

  @return   TRUE if moved, FALSE if not.
*///-----------------------------------------------------------------------------------------------
bool_t NedPointLineBeg(sNedPoint_t *pPoint)
{
  bool_t  fMoved = FALSE;

  NedAssertDbg(NedPointIsPoint(pPoint));

  while(NedPointCharPrev(pPoint))
  {
    if(NedPointCharGet(pPoint) == '\n')
    {
      NedPointCharNext(pPoint);
      break;
    }
    fMoved = TRUE;
  }
  return fMoved;
}

/*!------------------------------------------------------------------------------------------------
  Go to end of line. If already at end of line, do nothing. If at end of file, may be no '\n', so
  just go to the end of the file.

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point

  @return   TRUE if moved, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedPointLineEnd(sNedPoint_t *pPoint)
{
  NedAssertDbg(NedPointIsPoint(pPoint));

  // already at bottom or end of line.
  if(NedPointIsAtBottom(pPoint) || NedPointCharGet(pPoint) == '\n')
    return FALSE;

  if(!NedPointMemChr(pPoint, '\n', LONG_MAX))
    NedPointFileBottom(pPoint);
  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Report current location in terms of lines/columns

  @ingroup  ned_point
  @param    pPoint    pointer to valid point
  @param    pCol      ptr to returned column (1-n), or NULL (don't get col)

  @return   line number (1-n) and optionally column (1-n)
*///-----------------------------------------------------------------------------------------------
long NedPointLineGet(const sNedPoint_t *pPoint, long *pCol)
{
  sNedPoint_t     sPoint;
  long            line        = 1;
  long            col         = 1;
  long            posPoint    = 0;
  long            posThisLine = 0;
  long            posNextLine = 0;

  NedAssertDbg(NedPointIsPoint(pPoint));

  NedLogPrintfEx(NEDLOG_POINT, "NedPointLineGet(%s ", NedPointLogStr(pPoint));

  // get position of current point (to find that line)
  posPoint = NedPointPos(pPoint);

  // look for a line that contains posPoint
  NedPointInit(&sPoint, pPoint->pBuf);
  NedPointFileTop(&sPoint);
  while(line<LONG_MAX)
  {
    // get line boundaries (break if at EOF)
    posThisLine = posNextLine;
    if(!NedPointLineNext(&sPoint))
      break;
    // NedLogPrintfEx(NEDLOG_POINT, "  line %ld, posThisLine %ld, sPoint %s\n", line, posThisLine, NedLogPointStr(&sPoint));
    posNextLine = NedPointPos(&sPoint);

    // found the line!
    if((posThisLine <= posPoint) && (posPoint < posNextLine))
      break;

    ++line;
  }

  col = 1 + (posPoint - posThisLine);
  if(pCol)
    *pCol = col;

  NedLogPrintfEx(NEDLOG_POINT, "line %ld, col %ld, posPoint %ld, posThisLine %ld)\n", line, col, posPoint,
    posThisLine);

  return line;
}

/*!------------------------------------------------------------------------------------------------
  Go to a line number (1-n). If line # is greater than file length, then stops at
  last line in file.

  @ingroup  ned_point
  @param    pPoint    pointer to preallocated point
  @param    line      line 1-n

  @return   actual line number (1-n)
*///-----------------------------------------------------------------------------------------------
long NedPointLineGoto(sNedPoint_t *pPoint, long line)
{
  long    actualLine;

  NedAssertDbg(NedPointIsPoint(pPoint));
  NedAssertDbg(line > 0);

  NedPointFileTop(pPoint);
  for(actualLine=1; actualLine<line; ++actualLine)
  {
    if(!NedPointLineNext(pPoint))
      break;
  }
  return actualLine;
}


/*!------------------------------------------------------------------------------------------------
  Goto a line/col. If file is shorter, then ends up on last line.

  @ingroup  ned_point
  @param    pPoint    pointer to preallocated point
  @param    line      line 1-n
  @param    col       column 1-n

  @return   nothing
*///-----------------------------------------------------------------------------------------------
void NedPointGotoLineCol(sNedPoint_t *pPoint, long line, long col)
{
  NedPointLineGoto(pPoint, line);
  if(col >= 1)
    NedPointCharAdvance(pPoint, col-1);
}


/*!------------------------------------------------------------------------------------------------
  Returns TRUE if at bottom

  @ingroup  ned_point

  @param    pPoint       pointer to preallocated point

  @return   TRUE if at bottom
*///-----------------------------------------------------------------------------------------------
bool_t NedPointIsAtBottom(const sNedPoint_t *pPoint)
{
  // validate parameters
  NedAssertDbg(NedPointIsPoint(pPoint));

  return (NedBufIsLast(pPoint->pBuf) && (pPoint->offset == NedBufLen(pPoint->pBuf))) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Returns TRUE if at top

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point

  @return   TRUE if at top
*///-----------------------------------------------------------------------------------------------
bool_t NedPointIsAtTop(const sNedPoint_t *pPoint)
{
  // validate parameters
  NedAssertDbg(NedPointIsPoint(pPoint));

  return (NedBufIsFirst(pPoint->pBuf) && (pPoint->offset == 0)) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Returns TRUE if at beginning of a line.

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point

  @return   TRUE if at top
*///-----------------------------------------------------------------------------------------------
bool_t NedPointIsAtBegLine(const sNedPoint_t *pPoint)
{
  NedAssertDbg(NedPointIsPoint(pPoint));
  sNedPoint_t   sPoint;

  sPoint = *pPoint;
  return (NedPointCharPrev(&sPoint) && (NedPointCharGet(&sPoint) == '\n')) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Is this point between the 2 others (inclusive), that is pThis <= pTest <= pThat.

  @ingroup  ned_point
  @param    pTest       pointer to valid point
  @param    pTest       pointer to valid point
  @param    pTest       pointer to valid point

  @return   TRUE if valid point, FALSE if not (perhaps anything not on same chain)
*///-----------------------------------------------------------------------------------------------
bool_t NedPointIsBetween(const sNedPoint_t *pThis, const sNedPoint_t *pThat, const sNedPoint_t *pTest)
{
  long    posTest;
  long    posThis;
  long    posThat;

  // validate parameters
  NedAssertDbg(NedPointIsSameChain(pThis, pThat));

  // get positions of points
  posThis = NedPointPos(pThis);
  posThat = NedPointPos(pThat);
  posTest = NedPointPos(pTest);
  // printf("posThis %ld, posThat %ld, posTest %ld\n", posThis, posThat, posTest);

  // this and that not sorted. SeeNedPointSort2Points.
  if(posThis >= posThat)
    return FALSE;

  return ((posThis <= posTest) && (posTest <= posThat)) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Are these 2 points part of the same chain?

  @ingroup  ned_point
  @param    pThis     pointer to point
  @param    pThat     pointer to point

  @return   TRUE if both points are valid and part of the same chain, FALSE if not.
*///-----------------------------------------------------------------------------------------------
bool_t NedPointIsSameChain(const sNedPoint_t *pThis, const sNedPoint_t *pThat)
{
  return ( NedPointIsPoint(pThis) && NedPointIsPoint(pThat) && 
          (pThis->pBuf->id == pThat->pBuf->id) ) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Difference between 2 points (that - this). If this <= that number will be positive, otherwise,
  number will be negative. Must be in same buffer chain.

  @ingroup  ned_point
  @param    pThis     pointer to first valid point
  @param    pThat     pointer to last valid point on same buffer chain as pThis

  @return   LONG_MIN through LONG_MAX
*///-----------------------------------------------------------------------------------------------
long NedPointDifference(const sNedPoint_t *pThis, const sNedPoint_t *pThat)
{
  long    posThis;
  long    posThat;

  // validate parameters
  NedAssertDbg(NedPointIsSameChain(pThis, pThat));

  posThis = NedPointPos(pThis);
  posThat = NedPointPos(pThat);

  return (posThat - posThis);
}

/*!------------------------------------------------------------------------------------------------
  Get position of this point in buffer chain.

  @ingroup  ned_point
  @param    pThis     pointer to point
  @param    pThat     pointer to point

  @return   LONG_MIN through LONG_MAX
*///-----------------------------------------------------------------------------------------------
long NedPointPos(const sNedPoint_t *pPoint)
{
  long          pos = 0;
  sNedBuffer_t *pBuf;

  // validate parameters
  NedAssertDbg(NedPointIsPoint(pPoint));

  pBuf = NedBufHead(pPoint->pBuf);
  while(pBuf)
  {
    // if point is inside this buffer, we're done
    if(pBuf == pPoint->pBuf)
    {
      pos += pPoint->offset;
      break;
    }

    // on to next buffer
    pos += pBuf->len;
    pBuf = NedBufNext(pBuf);
  }

  return pos;
}

/*!------------------------------------------------------------------------------------------------
  Sort 2 points in the same buffer chain. pThis will be first, pThat will be last.

  @ingroup  ned_point
  @param    pThis      pointer to valid point
  @param    pThat      pointer to valid point in same buffer chain

  @return   TRUE if sorted, FALSE if points are in different buffer chains (can't sort)
*///-----------------------------------------------------------------------------------------------
bool_t NedPointSort2Points(sNedPoint_t *pThis, sNedPoint_t *pThat)
{
  long          posThis;
  long          posThat;
  sNedPoint_t   sTempPoint;

  // validate parameters
  if(!NedPointIsSameChain(pThis, pThat))
    return FALSE;

  posThis = NedPointPos(pThis);
  posThat = NedPointPos(pThat);

  // only need to swap if
  if(posThis > posThat)
  {
    sTempPoint  = *pThat;
    *pThat      = *pThis;
    *pThis      = sTempPoint;
  }

  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Search forward in buffer chain for the character. Moves point if found. Does not move point if
  not found.

  @ingroup  ned_point
  @param    pPoint      pointer to preallocated point
  @param    c           character to search for
  @param    maxLen      

  @return   TRUE if character is found. FALSE if not.
*///-----------------------------------------------------------------------------------------------
bool_t NedPointMemChr(sNedPoint_t *pPoint, nedChar_t c, long maxLen)
{
  uint8_t        *psz    = NULL;
  sNedBuffer_t   *pBuf;
  unsigned        offset;
  bool_t          fFound = FALSE;
  unsigned        thisLen;

  // validate parameters
  NedAssertDbg(NedPointIsPoint(pPoint));

  // can't search past end of file or for invalid characters
  if(NedPointIsAtBottom(pPoint))
    return FALSE;

  pBuf    = pPoint->pBuf;
  offset  = pPoint->offset;
  // NedLogPrintfEx(NEDLOG_POINT, "NedPointStrChr(pPoint=%p,c=x%02x), pBuf=%p, offset=%u",pPoint,c,pBuf,offset);
  while(!fFound && (maxLen > 0))
  {
    // go on to next buffer
    if(offset >= NedBufLen(pBuf))
    {
      // at last buffer, we're done, not found
      if(NedBufIsLast(pBuf))
      {
        // NedLogPrintfEx(NEDLOG_POINT, "  last buf %p\n",pBuf);
        break;
      }

      pBuf    = NedBufNext(pBuf);
      offset  = 0;
      // NedLogPrintfEx(NEDLOG_POINT, "  next buf %p\n",pBuf);
    }

    thisLen = NedBufLen(pBuf) - offset;
    if(thisLen > maxLen)
      thisLen = (unsigned)maxLen;

    // if not found this buffer, try next
    psz = memchr(NedBufData(pBuf) + offset, (int)c, thisLen);
    if(psz == NULL)
    {
      //NedLogPrintfEx(NEDLOG_POINT, "  memchr NULL, not found, trying next buffer\n");
      // will force to go on to next buffer at top of while loop
      offset = NedBufLen(pBuf);
    }

    // found it! move point
    else
    {
      pPoint->pBuf    = pBuf;
      pPoint->offset  = (psz - NedBufData(pBuf));
      fFound          = TRUE;
      // NedLogPrintfEx(NEDLOG_POINT, "  found %p, offset %u\n", pPoint->pBuf, pPoint->offset);
      break;
    }
    maxLen -= thisLen;
  }

  return fFound;
}

/*!------------------------------------------------------------------------------------------------
  Search backward for the character from the given point. if found, pPoint

  @ingroup  ned_point
  @param    pPoint      pointer to valid point
  @param    c           character to search for

  @return   TRUE if character is found. FALSE if not.
*///-----------------------------------------------------------------------------------------------
bool_t NedPointStrChrRev(sNedPoint_t *pPoint, nedChar_t c)
{
  uint8_t       *psz    = NULL;
  sNedBuffer_t  *pBuf;
  unsigned       offset;
  bool_t         fFound = FALSE;

  // validate parameters
  NedAssertDbg(NedPointIsPoint(pPoint));

  // Not found if already at top. Searching backward for EOF makes no sense.
  if(c == NEDCHAR_EOF || NedPointIsAtTop(pPoint))
    return FALSE;

  // check special at offset 0
  pBuf    = pPoint->pBuf;
  offset  = pPoint->offset;
  // NedLogPrintfEx(NEDLOG_POINT, "NedPointStrChr(pPoint=%p,c=x%02x), pBuf=%p, offset=%u",pPoint,c,pBuf,offset);
  if(NedBufLen(pBuf) == 0)
  {
    NedAssertDbg(NedPointIsAtBottom(pPoint));
    pBuf  = NedBufPrev(pBuf);
    if(pBuf)
    {
      offset = NedBufLen(pBuf);
      NedAssertDbg(offset);
    }
  }

  // look far character backwards through buffer chain
  while((pBuf!=NULL) && !fFound)
  {
    // NedLogPrintfEx(NEDLOG_POINT, "  searchinbg %p, len %u\n", NedBufData(pBuf), offset);
    // if not found this buffer, try next
    psz = NedMemRChr(NedBufData(pBuf) + offset, (int)c, offset+1);
    if(psz == NULL)
    {
      //NedLogPrintfEx(NEDLOG_POINT, "  memrchr NULL, not found, trying prev buffer\n");
      pBuf = NedBufPrev(pBuf);
      if(pBuf == NULL)
        break;
      offset = NedBufLen(pBuf);
      NedAssertDbg(offset);
    }

    // found it! move point
    else
    {
      pPoint->pBuf    = pBuf;
      pPoint->offset  = (psz - NedBufData(pBuf));
      fFound          = TRUE;
      // NedLogPrintfEx(NEDLOG_POINT, "  found %p,offset %u\n", pPoint->pBuf, pPoint->offset);
      break;
    }
  }

  return fFound;
}

/*!------------------------------------------------------------------------------------------------
  Gets up to the length specified by by maxLen. Higher layer must guarantee buffer to receive the
  data is at least maxLen in size. Will only be short if at end of file.

  @ingroup  ned_point
  @param    pTo       byte array to receive data. May be NULL to determine length only.
  @param    pFrom     pointer to valid point
  @param    maxLen    max number of bytes to get

  @return   returns up to maxLen bytes (0-n). May be less if end of bufchain reached.
*///-----------------------------------------------------------------------------------------------
long NedPointMemGet(uint8_t *pTo, const sNedPoint_t *pFrom, long maxLen)
{
  unsigned        thisLen;
  unsigned        offset;
  long            foundLen = 0;
  sNedBuffer_t   *pBuf;

  NedAssertDbg(pTo);
  NedAssertDbg(maxLen);
  NedAssertDbg(NedPointIsPoint(pFrom));

  // leave room for '\0'
  offset = pFrom->offset;
  pBuf   = pFrom->pBuf;
  while((maxLen > 0) && pBuf)
  {
    // determine length to copy
    thisLen = NedBufLen(pBuf) - offset;
    if(thisLen > maxLen)
      thisLen = (unsigned)maxLen;

    // copy the data
    if(pTo)
      memcpy(pTo, NedBufData(pBuf)+offset, thisLen);

    // on to next buffer (if needed)
    pBuf = NedBufNext(pBuf);
    offset = 0;
    maxLen   -= thisLen;
    foundLen += thisLen;
    if(pTo)
      pTo    += thisLen;
  }

  return foundLen;
}

/*!------------------------------------------------------------------------------------------------
  StrGet gets up to the length specified by by maxLen and NUL terminates the string. Make sure the
  buffer to receive the data is at least (maxLen+1) in size. Will only be short if at end of file.

  @ingroup  ned_point
  @param    pTo       byte array to receive data. May be NULL to determine length only.
  @param    pFrom     pointer to valid point
  @param    maxLen    max number of bytes to get

  @return   returns up to maxLen bytes (0-n). May be less if end of bufchain reached.
*///-----------------------------------------------------------------------------------------------
long NedPointStrGet(uint8_t *pTo, const sNedPoint_t *pFrom, long maxLen)
{
  long            foundLen;

  foundLen = NedPointMemGet(pTo, pFrom, maxLen);
  if(pTo)
    pTo[foundLen] = '\0';
  return foundLen;
}

/*!------------------------------------------------------------------------------------------------
  Gets a symbol from the buffer. Returns 0 if point not on a symbol. A symbol is the name of a 
  function, variable, etc... (any combo of a-z, A-Z, _, 0-9)

  Returns asciiz symbol string.

  @ingroup  ned_point
  @param    pTo       byte array to receive data. May be NULL to determine length only.
  @param    pFrom     pointer to valid point
  @param    maxLen    max number of bytes to get (1-n)

  @return   returns up to maxLen bytes (0-n). May be less if end of bufchain reached.
*///-----------------------------------------------------------------------------------------------
long NedPointSymbolGet(uint8_t *pTo, const sNedPoint_t *pFrom, long maxLen)
{
  sNedPoint_t   sPoint;
  sNedPoint_t   sPtEnd;
  long          symLen = 0;

  sPoint = *pFrom;
  if(NedCharType(NedPointCharGet(&sPoint)) == IS_ALNUM && maxLen > 0)
  {
    NedPointWordBeg(&sPoint);
    sPtEnd = sPoint;
    NedPointWordEnd(&sPtEnd);
    symLen = NedPointDifference(&sPoint, &sPtEnd);
    if(symLen > maxLen)
      symLen = maxLen;
    if(pTo)
    {
      NedPointMemGet(pTo, &sPoint, symLen);
      pTo[symLen] = '\0';
    }
  }

  return symLen;
}


/*!------------------------------------------------------------------------------------------------
  Standard strncmp(), but with a point instead of a string for 1st parameter. Does not affect the
  point.

  @ingroup  ned_point
  @param    pPoint      valid point
  @param    sz          valid string
  @param    len         length to check

  @return   -1, 0, 1, as per strcmp()
*///-----------------------------------------------------------------------------------------------
int NedPointStrNCmp(const sNedPoint_t *pPoint, const char *sz, long len)
{
  sNedPoint_t   sPt = *pPoint;
  int           match = 0;  // -1 is <, 0 is =, 1 is >
  nedChar_t     c;

  while(len > 0)
  {
    c = NedPointCharGet(&sPt);
    if(c < (nedChar_t)(*sz) || (c == NEDCHAR_EOF))
    {
      match = -1;
      break;
    }
    else if(c > (nedChar_t)(*sz))
    {
      match = 1;
      break;
    }
    ++sz;
    NedPointCharNext(&sPt);
    --len;
  }

  return match;
}

/*!------------------------------------------------------------------------------------------------
  Standard strcmp(), but with a point instead of a string for 1st parameter. Does not affect the
  point.

  @ingroup  ned_point
  @param    pPoint      valid point
  @param    sz          valid ASCIIZ string

  @return   -1, 0, 1, as per strcmp()
*///-----------------------------------------------------------------------------------------------
int NedPointStrCmp(const sNedPoint_t *pPoint, const char *sz)
{
  return NedPointStrNCmp(pPoint, sz, strlen(sz));
}

/*!------------------------------------------------------------------------------------------------
  Show the point to the log

  @ingroup  ned_point
  @param    pPoint    pointer to valid point
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedPointLogShow(const sNedPoint_t *pPoint)
{
  NedLogPrintf("Point: %p, off %u\n", pPoint->pBuf, pPoint->offset);
}

/*!------------------------------------------------------------------------------------------------
  Returns a static string that shows the point "%p, offset %u". Not reentrant (string static).

  @ingroup  ned_point
  @param    pPoint    pointer to valid point
  @return   character string.
*///-----------------------------------------------------------------------------------------------
const char * NedPointLogStr(const sNedPoint_t *pPoint)
{
  static char szStr[32];   // max 1000123410001234, offset 4096

  NedAssertDbg(NedPointIsPoint(pPoint));
  snprintf(szStr, sizeof(szStr)-1, "%p, offset %u", (void *)pPoint->pBuf, pPoint->offset);
  return (const char *)szStr;
}
