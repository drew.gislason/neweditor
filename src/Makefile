#  Ned Makefile
#  Copyright (c) 2021 Drew Gislason, All Rights Reserved.
#

UNAME=$(shell uname -s)

# -g for Linux, -g (was -fno-pie for macOS)
ifdef WINDIR
	HOSTENV=Cygwin
	HOSTFLAGS=-g
	HOST_LFLAGS=-limagehlp
else
	ifeq ($(UNAME),Darwin)
		HOSTENV=Mac
		HOSTFLAGS=-g
		HOST_LFLAGS=-g
	else
		HOSTENV=Linux
		HOSTFLAGS=-g
		HOST_LFLAGS=-g
	endif
endif

# common dependancies
DEPS=../src/Ned.h ../src/NedEditor.h

# output folder
OUT=out

# -std=c11 -pedantic-errors 
CC=gcc
INCLUDE=-I. -I ../src
DEFINES=-DNEDDBG_LEVEL=1
CCFLAGS =-Wall -Werror $(HOSTFLAGS) $(INCLUDE)
CFLAGS=$(CCFLAGS) $(DEFINES) -c -o
LFLAGS=$(HOST_LFLAGS) -o

# For Unit Testing the BSP layer
NED_OBJS = \
	$(OUT)/Ned.o \
	$(OUT)/NedAnsi.o \
	$(OUT)/NedAssert.o \
	$(OUT)/NedBuffer.o \
	$(OUT)/NedCmd.o \
	$(OUT)/NedCmdBookmarks.o \
	$(OUT)/NedCmdEdit.o \
	$(OUT)/NedCmdFile.o \
	$(OUT)/NedCmdHelp.o \
	$(OUT)/NedCmdMenu.o \
	$(OUT)/NedCmdMisc.o \
	$(OUT)/NedCmdMove.o \
	$(OUT)/NedCmdSearch.o \
	$(OUT)/NedEditSubmenu.o \
	$(OUT)/NedFile.o \
	$(OUT)/NedJson2.o \
	$(OUT)/NedKey.o \
	$(OUT)/NedKeyMap.o \
	$(OUT)/NedLanguage.o \
	$(OUT)/NedList.o \
	$(OUT)/NedLog.o \
	$(OUT)/NedMenu.o \
	$(OUT)/NedMsg.o \
	$(OUT)/NedPoint.o \
	$(OUT)/NedPointClip.o \
	$(OUT)/NedPointEdit.o \
	$(OUT)/NedPointMisc.o \
	$(OUT)/NedPosList.o \
	$(OUT)/NedSettings.o \
	$(OUT)/NedSignal.o \
	$(OUT)/NedTheme.o \
	$(OUT)/NedUtilStr.o \
	$(OUT)/NedUtilFile.o \
	$(OUT)/NedUndo.o \
	$(OUT)/NedWin.o \
	$(OUT)/NedView.o \
	$(OUT)/NedController.o \
	$(OUT)/NedEditor.o

$(OUT)/%.o: ../src/%.c $(DEPS)
	$(CC) $< $(CFLAGS) $@

.PHONY: clean mkout SayNed SayDone

ned: SayNed mkout ../tools/file2c NedManual.h NedSha.h $(NED_OBJS) SayDone
	$(CC) $(LFLAGS) $@ $(NED_OBJS)
	@echo Linked ned...

SayNed:
	@echo ------------------------------
	@echo Making ned...
	@echo ------------------------------

SayDone:
	@echo ------------------------------
	@echo Created ned
	@echo ------------------------------

all: ned

# clean up the out folder
clean:
	rm -rf $(OUT)
	rm -f ned
	rm -f *.log
	rm -f NedManual.h
	rm -f NedSha.h
	rm -f NedSha.txt
	rm -f DELETE.ME

# make the out folder
mkout:
	-if test ! -d $(OUT); then mkdir $(OUT); fi

../tools/file2c : ../tools/file2c.c
	gcc ../tools/file2c.c -o ../tools/file2c

NedManual.h: ../docs/NedManual.md
	../tools/file2c ../docs/NedManual.md NedManual.h gszNedManual

NedSha.h: nedsha.sh
	./nedsha.sh
	../tools/file2c NedSha.txt NedSha.h gszNedSha

$(OUT)/NedCmdHelp.o : NedCmdHelp.c NedSha.h
	$(CC) $< $(CFLAGS) $@

$(OUT)/NedSettings.o : NedSettings.c NedManual.h NedSettings.h
	$(CC) $< $(CFLAGS) $@
