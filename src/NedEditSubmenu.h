/*!************************************************************************************************
  @file NedEditor.h

  @brief  Private file for those objects which need access to Editor state.

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  This is a support file for the editor commands. Includes many common functions, getters and
  setters.

*///***********************************************************************************************
#ifndef NED_EDITOR_SUBMENU_H
#define NED_EDITOR_SUBMENU_H

typedef struct
{
  unsigned      nMenu;
  unsigned      nItem;
  bool_t        fExitOnDel;       // TRUE if DEL and BACKSPACE also exit
  nedKey_t      key;              // exit key (e.g. ESC, ENTER, NEDKEY_DELETE, etc...)
  bool_t        fEnteredViaMenu;
  void         *hList;            // see hList NedFileListNew(), NedUtilFile.h
  hPick_t      *hPick;            // see hPick NedPickNew(), NedMenu.h
  void         *pData;            // other data, if needed
  unsigned      pickedSubItem;
  unsigned      numItems;
  const char  **ppStrings;
} sNtmCsm_t;

extern sNtmCsm_t    m_FileOpen;
extern sNtmCsm_t    m_FileOpenRecent;
extern sNtmCsm_t    m_OptionDoCommand;
extern sNtmCsm_t    m_OptionMapKeys;
extern sNtmCsm_t    m_OptionProjectFolders;

// CleanUp Submenu logic
bool_t  NtmCommonSubMenu                (sNtmCsm_t *pCsm, nedKey_t key, unsigned *pSubItem);
void    NtmCommonSubMenuFeedKeys        (pfnSubMenu_t pfnSubMenu, sNtmCsm_t *pCsm);
bool_t  NtmFileOpenSubMenu              (nedKey_t key, unsigned *pSubItem);
bool_t  NtmFileOpenRecentSubMenu        (nedKey_t key, unsigned *pSubItem);
bool_t  NtmOptionDoCommandSubMenu       (nedKey_t key, unsigned *pSubItem);
bool_t  NtmOptionMapKeysSubMenu         (nedKey_t key, unsigned *pSubItem);
bool_t  NtmOptionProjectFoldersSubMenu  (nedKey_t key, unsigned *pSubItem);

#endif // NED_EDITOR_SUBMENU_H
