/*!************************************************************************************************
  @file NedBuffer.c

  Ned Buffer Implementation

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_buf
*///***********************************************************************************************
#include <ctype.h>
#include <limits.h>

#include "NedBuffer.h"
#include "NedMem.h"
#include "NedLog.h"

unsigned  m_maxBufLen = NEDBUF_SIZE;

/*!-----------------------------------------------------------------------------------------------
  Create a new buffer chain. The id is used to identify the entire chain as buffers are added to
  it.

  @param    id    managed by caller, should be unique among all buffer chains in the program.
  @return   Pointer to buffer or NULL if it couldn't be allocated.
*///-----------------------------------------------------------------------------------------------
sNedBuffer_t *NedBufNew(unsigned id)
{
  sNedBuffer_t *pBuf;

  pBuf = malloc(sizeof(sNedBuffer_t));
  if(pBuf)
  {
    pBuf->id      = id;             // buf chain ID
    pBuf->len     = 0;
    pBuf->sanchk  = NEDBUF_SANCHK;  // sanity check
    pBuf->pNext   = NULL;
    pBuf->pPrev   = NULL;
    memset(&pBuf->aData[0], NEDDBG_ALLOC, NedBufMaxLen());
  }
  return pBuf;
}

/*!------------------------------------------------------------------------------------------------
  Appends a new empty buffer to the given buffer in a buffer chain.

  @param    pBuf    pointer to a valid buffer in a buffer chain.
  @return   Pointer to new buffer or NULL if it couldn't be allocated.
*///-----------------------------------------------------------------------------------------------
sNedBuffer_t *NedBufAppend(sNedBuffer_t *pBuf)
{
  sNedBuffer_t *pBufNew = NULL;

  if(!NedBufIsBuf(pBuf))
  {
    NedAssertFail();
    return NULL;
  }

  pBufNew = NedBufNew(pBuf->id);
// printf("NedBufAppend(pBuf %p), pBufNew %p\n", pBuf, pBufNew);
  if(pBufNew)
  {
    // hook new buffer into double linked list chain
    pBufNew->pNext  = pBuf->pNext;
    pBufNew->pPrev  = pBuf;
    pBuf->pNext     = pBufNew;
    if(pBufNew->pNext)
      pBufNew->pNext->pPrev = pBufNew;
// printf("pBuf     %p, prev %p, next %p, len %u\n",pBuf,pBuf->pPrev,pBuf->pNext,pBuf->len);
// printf("pBufNew: %p, prev %p, next %p, len %u\n",pBufNew,pBufNew->pPrev,pBufNew->pNext,pBufNew->len);
  }
  return pBufNew;
}

/*!------------------------------------------------------------------------------------------------
  Free the buffer and remove from linked list.

  @param    pBuf      pointer to valid buffer
  @return   ptr to next buffer after pBuf
*///-----------------------------------------------------------------------------------------------
sNedBuffer_t *NedBufFree(sNedBuffer_t *pBuf)
{
  sNedBuffer_t *pBufNext;

  // validate paraemters
  NedAssertDbg(NedBufIsBuf(pBuf));

  // remove from buffer chain
  pBufNext = pBuf->pNext;
  if(pBuf->pPrev)
    pBuf->pPrev->pNext = pBufNext;
  if(pBufNext)
    pBufNext->pPrev = pBuf->pPrev;

  // free it
  pBuf->pNext   = pBuf->pPrev = NULL;
  pBuf->id      = 0;
  pBuf->len     = 0;
  pBuf->sanchk  = 0;
  memset(&pBuf->aData[0], NEDDBG_FREE, NedBufMaxLen());
  free(pBuf);

  return pBufNext;
}

/*!------------------------------------------------------------------------------------------------
  Clear the buffer chain to a single empty buffer.

  @param    pBufHead      Head of buffer.
  @return   Pointer to buffer or NULL if it couldn't be allocated.
*///-----------------------------------------------------------------------------------------------
void NedBufChainClear(sNedBuffer_t *pBufHead)
{
  sNedBuffer_t *pBuf;
  sNedBuffer_t *pBufNext;

  if(!NedBufIsBuf(pBufHead))
  {
    NedAssertFail();
    return;
  }

  // start at head (maybe caller passed last buffer instead of head)
  pBufHead = NedBufHead(pBufHead);
  memset(&pBufHead->aData[0], NEDDBG_FREE, NedBufMaxLen());
  pBufHead->len = 0;

  pBufNext = pBufHead->pNext;
  while(pBufNext)
  {
    pBuf = pBufNext;
    pBufNext = pBuf->pNext;
    NedBufFree(pBuf);
  }
}

/*!------------------------------------------------------------------------------------------------
  Free up the entire buffer chain (all buffers).

  @param    pBufHead    Should point to the head, but could be any buffer in the chain.
  @return   upon exit, pBufHead is not pointing to valid memory
*///-----------------------------------------------------------------------------------------------
void NedBufChainFree(sNedBuffer_t *pBufHead)
{
  NedAssertDbg(NedBufIsBuf(pBufHead));
  pBufHead = NedBufHead(pBufHead);
  NedBufChainClear(pBufHead);
  NedBufFree(pBufHead);
}

/*!------------------------------------------------------------------------------------------------
  Insert data at the given offset. Will create new buffers as needed.

  @param    pBuf      pointer to valid buffer
  @param    offset    offset <= pBuf->len
  @param    pData     pointer to array of data
  @param    len       length of data <= LONG_MAX
  @return   length added, which if worked is len. If 0, then invalid parameter.
*///-----------------------------------------------------------------------------------------------
long NedBufInsert(sNedBuffer_t *pBuf, unsigned offset, const uint8_t *pData, long len)
{
  long      lenInserted = 0;
  unsigned  thisLen;
  bool_t    fWorked = TRUE;

  // check for invalid parameters
  // NedLogPrintfEx(NEDLOG_BUF, "NedBufInsert(pBuf=%p, pBuf->len %u, offset=%u, pData=%p, len=%ld)\n",
  //   pBuf, pBuf->len, offset, pData, len);
  NedAssertDbg(NedBufIsBuf(pBuf));
  NedAssertDbg(offset < NedBufMaxLen());
  NedAssertDbg(len > 0);

  // split the buffer as we're adding too much data to fit
  if(pBuf->len + len > NedBufMaxLen())
  {
    fWorked = NedBufSplit(pBuf, offset);
    // NedLogPrintfEx(NEDLOG_BUF, "  split\n");
  }

  if(fWorked)
  {
    while(len)
    {
      // add data into buffer
      thisLen = NedBufMaxLen() - offset;
      if(thisLen > len)
        thisLen = len;
      // NedLogPrintfEx(NEDLOG_BUF, "  b4: thisLen %u, offset %u, buflen %u, c=x%02x\n", thisLen, offset, pBuf->len, *pData);
      if(offset < pBuf->len)
      {
        NedLogPrintfEx(NEDLOG_BUF, "  moved %u bytes\n", pBuf->len - offset);
        memmove(&pBuf->aData[offset + thisLen], &pBuf->aData[offset], pBuf->len - offset);
      }
      memcpy(&pBuf->aData[offset], pData, thisLen);
      len         -= thisLen;
      pBuf->len   += thisLen;
      pData       += thisLen;
      lenInserted += thisLen;
      // NedLogPrintfEx(NEDLOG_BUF, "  af: len %ld, lenInserted %ld, bufLen %u, c=x%02x\n", len, lenInserted, pBuf->len, *pData);

      // get another buffer if needed
      if(len > 0)
      {
        offset = 0;
        pBuf  = NedBufAppend(pBuf);
        if(!pBuf)
        {
          // NedLogPrintfEx(NEDLOG_BUF, "  append failed\n");
          break;
        }
        // NedLogPrintfEx(NEDLOG_BUF, "  appended buf %p\n", pBuf);
      }
    }
  }

  // NedLogPrintfEx(NEDLOG_BUF, "  lenInserted %ld\n", lenInserted);
  // if(NedLogMaskGet() & NEDLOG_BUF)
  //   NedBufLogDumpChain(pBuf, 16);
  return lenInserted;
}

/*!------------------------------------------------------------------------------------------------
  Insert a copy of a part (or all) of one buffer chain to a point in another buffer chain. Tries to
  pack if possible, otherwise creates a new buffer.

  Examples:

      BufTo/offset      BufFrom         Results in BufTo
      [abcdef]/2        [xyz]           [abxyzcdef]

      [abcxyzdef]/0     [A...A]         [A...A]
                        [B...B]         [abcxyzdef]
                        [C...C]         [B...B]
                                        [C...C]

      [abc]/3           [def]           [abcdefghi]
                        [ghi]           [A...]
                        []
                        [A...A]

      [abc]/2           [def]           [abdefc]
                        [A...A]         [A...A]
                        [g]             [ghi]
                        [h]
                        [i]

      [A...A]/0         [abc]           [abc]
      [xyz]                             [A...A]
                                        [xyz]

  @param    pBufTo      buffer chain to copy to
  @param    toOffset    offset <= pBuf->len
  @param    pBufFrom    buffer chain to copy from 
  @param    fromOffset  offset in from chain
  @param    len         length to copy
  @return   Length inserted into pBuf (which may be 0)
*///-----------------------------------------------------------------------------------------------
long NedBufChainCopy(sNedBuffer_t *pBufTo, unsigned toOffset,
                    const sNedBuffer_t *pBufFrom, unsigned fromOffset, long len)
{
  long            lenInserted = 0;
  unsigned        thisLen;
  bool_t          fSplit = FALSE;

  // debug logging
  NedLogPrintfEx(NEDLOG_BUF, "NedBufChainCopy(pBufTo=%p, toOffset=%u, pBufFrom %p, fromOffset=%u, len=%ld)\n",
    pBufTo, toOffset, pBufFrom, fromOffset, len);

  // should never happen
  NedAssertDbg(NedBufIsBuf(pBufTo));
  if(!(toOffset <= pBufTo->len))
    NedBufLogDumpChain(NedBufHead(pBufTo), 16);
  NedAssertDbg(toOffset <= pBufTo->len);
  NedAssertDbg(NedBufIsBuf(pBufFrom));
  NedAssertDbg(fromOffset <= pBufFrom->len);
  NedAssertDbg(len > 0);

  // pack data if possible, otherwise, new buffer
  while(len && pBufFrom)
  {
    // pack while we can
    thisLen = pBufFrom->len - fromOffset;
    if(thisLen > len)
      thisLen = len;

    // can the data fit this buffer or next?
    if(pBufTo->len + thisLen <= NedBufMaxLen())
    {
      NedLogPrintfEx(NEDLOG_BUF, "  fit\n");
      // open up if needed (move data after offset to make room)
      if(toOffset < pBufTo->len)
        memmove(&pBufTo->aData[toOffset + thisLen], &pBufTo->aData[toOffset], pBufTo->len - toOffset);
    }

    // no room this buffer, move on to next (or split this one if needed)
    else
    {
      // first time we can't fit in 1st pBufTo, may need to split
      if(!fSplit)
      {
        // no need to split if already at end of buffer
        fSplit = TRUE;
        if(toOffset < pBufTo->len)
        {
          NedLogPrintfEx(NEDLOG_BUF, "  split\n");
          // if we can't split, out of memory, stop
          if(!NedBufSplit(pBufTo, toOffset))
            return lenInserted;
          continue;
        }
      }

      // if we can't append, out of memory, stop
      NedLogPrintfEx(NEDLOG_BUF, "  append\n");
      pBufTo = NedBufAppend(pBufTo);
      toOffset = 0;
      if(!pBufTo)
        return lenInserted;
    }

    NedLogPrintfEx(NEDLOG_BUF, "  inserting thisLen %u, pBufTo %p, toOffset %u, pFrom %p, fromOffset %u\n",
      thisLen, pBufTo, toOffset, pBufFrom, fromOffset);

    // copy in data (if any, as may be empty from buffer)
    if(thisLen)
      memcpy(&pBufTo->aData[toOffset], &pBufFrom->aData[fromOffset], thisLen);

    lenInserted += thisLen;
    len         -= thisLen;
    toOffset    += thisLen;
    pBufTo->len += thisLen;
    fromOffset   = 0;
    pBufFrom     = pBufFrom->pNext;
  }

  NedLogPrintfEx(NEDLOG_BUF, "  lenInserted %ld\n", lenInserted);
  return lenInserted;  
}

/*!------------------------------------------------------------------------------------------------
  Split a single buffer at the given offset. The new buffer is always after the existing buffer.

  pBuf must point to a valid buffer, offset must be <= pBuf->len.

  Examples:

      pBuf/offset       Result
      [abcdef]/3        [abc]
                        [def]
      
      [abcdef]/0        []
                        [abcdef]

      [abcdef]/6        [abcdef]
                        []

  @param    pBuf[in,out]    splits the buffer at the offset
  @param    offset[in]     offset within the buffer to grow
  @return   TRUE if worked, FALSE if failed.
*///-----------------------------------------------------------------------------------------------
bool_t NedBufSplit(sNedBuffer_t *pBuf, unsigned offset)
{
  sNedBuffer_t *pNewBuf;

  // NedLogPrintfEx(NEDLOG_BUF, "NedBufSplit(pBuf=%p, offset=%u)\n", pBuf, offset);
  NedAssertDbg(NedBufIsBuf(pBuf) && (offset <= pBuf->len));

  // add and link in a new buffer
  pNewBuf = NedBufAppend(pBuf);
  if(!pNewBuf)
    return FALSE;

  // copy split data to new buffer
  if(offset > pBuf->len)
    offset = pBuf->len;
  pNewBuf->len = (pBuf->len - offset);
  if( pNewBuf->len )
    memcpy(&pNewBuf->aData[0], &pBuf->aData[offset], pNewBuf->len);
  pBuf->len = offset;
#if NEDDBG_LEVEL >= 1
  memset(&pBuf->aData[pBuf->len], NEDDBG_FREE, NedBufMaxLen() - pBuf->len);
#endif

  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Create enough space at the given offset to add iAddLen bytes in buffers.
  offset within the buffer. May create many new buffers (if iAddLen is large).
  Data after offset will move to accommadate space, possibly to new buffer(s).

  pBuf must point to a valid buffer, offset must be <= pBuf->len.

  @param    pBuf      pointer to valid buffer
  @param    offset    offset within the buffer to grow
  @param    iAddLen   length of bytes to add
  @return   TRUE if worked, FALSE if not (bad parm or out of memory)    
*///-----------------------------------------------------------------------------------------------
bool_t NedBufGrow(sNedBuffer_t *pBuf, unsigned offset, long addLen)
{
  unsigned        thisLen;
  unsigned        splitLen;
  sNedBuffer_t *  pNewBuf;

  // validate parameters
  // NedLogPrintfEx(NEDLOG_BUF, "NedBufGrow(pBuf=%p, offset=%u, addlen=%ld)\n", pBuf, offset, addLen);
  NedAssertDbg(NedBufIsBuf(pBuf) && (offset <= pBuf->len) && (addLen > 0));

  // splitLen is the data in the buffer AFTER the offset (may be 0)
  splitLen = pBuf->len - offset;

  // if it can fit in this buffer, no need to add new buffer(s)
  if(addLen <= (NedBufMaxLen() - pBuf->len))
  {
    /*
      Original buffer length: 3, NEDBUF_SIZE: 8, offset: 2, addlen: 3

      Example 1 (all in same buffer):
      step   org       new       comment
      1      abc.....            NedBufGrow(pBuf,2,3)
      2      abc---..            Space added (no new buffer(s) needed)
      3      ab---c..            Moved split data
    */
    thisLen = (unsigned)addLen;
    if(splitLen)
    {
      memmove(&pBuf->aData[offset+thisLen], &pBuf->aData[offset], splitLen);
#if NEDDBG_LEVEL
      memset(&pBuf->aData[offset], NEDDBG_FREE, thisLen);
#endif
    }
    pBuf->len += thisLen;
    return TRUE;
  }

  // we may have some room in this buffer
  addLen -= (NedBufMaxLen() - pBuf->len);

  // add as many buffers as needed
  pNewBuf = pBuf;
  while(addLen > 0)
  {
    pNewBuf = NedBufAppend(pNewBuf);
    if( !pNewBuf )
      return FALSE;

    // determine how much length this buffer
    thisLen = NedBufMaxLen();
    if(addLen < thisLen)
      thisLen = (unsigned)addLen;
    pNewBuf->len = thisLen;

    addLen -= thisLen;
  }

  /*
    handle "split" portion of the buffer

    Original buffer len: 6, NEDBUF_SIZE: 8

    Example 2, all of the data moves to end of new buffer:
    step   org       new       comment
    1      abcdef..            NedBufGrow(pBuf,2,9)
    2      abcdef--  -------.  Space added (new buffer len=7)
    3      ab------  ---cdef.  Copied part2 of split data after offset (cdef)

    Example 3, some of the data stays in original buffer:
    step   org       new       comment
    1      abcdef..            NedBufGrow(pBuf,2,5)
    2      abcdef--  ---.....  Space added (new buffer len=3)
    3      abcdef--  ---.....  Copied splitPart2 to new buffer
    4      ab-----c  def.....  Copied splitPart1 to end of org buffer

    Example 4, variant on 3
    step   org       new1      new2      comment
    1      abcdef..                      NedBufGrow(pBuf,2,13)
    2      abcdef--  --------  ---.....  Space added (new buffer len=2)
    3      abcdef--  --------  def.....  Copied splitPart2 to new2 buffer
    4      ab------  -------c  def.....  Copied splitPart1 to end of new1

  */
  if(splitLen)
  {
    unsigned s1, s2;

    s2 = pNewBuf->len;
    if( s2 > splitLen)
      s2 = splitLen;
    s1 = 0;
    if( s2 < splitLen )
      s1 = splitLen - s2;

    // copy splitPart2 into new buffer
    memcpy(&pNewBuf->aData[pNewBuf->len - s2],
           &pBuf->aData[pBuf->len - s2], s2);

    if(s1)
    {
      memmove(&pNewBuf->pPrev->aData[NedBufMaxLen() - s1],
              &pBuf->aData[pBuf->len - splitLen], s1);
    }

    // help make deleted space obvious
#if gNedDebugLevel_c
    memset(&pBuf->aData[offset], NEDBUF_FILL_EMPTY, splitLen);
#endif
  }

  pBuf->len = NedBufMaxLen();

  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Removes bytes from the current (and following) buffers. It's OK if removeLen is longer than total
  buffer space from pBuf/offset.
  
  Doesn't remove pBuf (even if empty).

  @param    pBuf          pointer to valid buffer
  @param    offset        offset at which to shrink (<= pBuf->len)
  @param    iRemoveLen    length to remove.
  @return   TRUE if worked, FALSE if not.
*///-----------------------------------------------------------------------------------------------
bool_t NedBufShrink(sNedBuffer_t *pBuf, unsigned offset, long removeLen)
{
  unsigned thisLen;
  unsigned leftOver = 0;

  // NedLogPrintfEx(NEDLOG_BUF, "NedBufShrink(pBuf=%p, offset=%u, removeLen=%ld)\n", pBuf, offset, removeLen);
  NedAssertDbg(NedBufIsBuf(pBuf) && (offset <= pBuf->len) && removeLen);

  /*
    Example 1 (all in same buffer):
    step   org       new       comment
    1      ab---c..            original
    2      abc.....            NedBufShrink(pBuf,2,3)
  */
  thisLen = (pBuf->len - offset);
  if(removeLen < thisLen)
    thisLen = (unsigned)removeLen;
  if( offset + thisLen < pBuf->len )
  {
    leftOver = pBuf->len - (offset + thisLen);
    memmove(&pBuf->aData[offset], &pBuf->aData[offset+thisLen], leftOver);
  }
#if gNedDebugLevel_c
  memset(&pBuf->aData[offset+leftOver], NEDBUF_FILL_EMPTY, thisLen);
#endif
  pBuf->len -= thisLen;
  removeLen -= thisLen;

  if( removeLen == 0 )
    return TRUE;

  // remove whole buffers if needed
  pBuf = pBuf->pNext;
  while(pBuf && (removeLen >= pBuf->len))
  {
    thisLen     = pBuf->len;
    pBuf        = NedBufFree(pBuf);
    removeLen  -= thisLen;
  }

  /*
    Example 2, all of the data moves to end of new buffer:
    step   org       new       comment
    1      ab------  ---cdef.  original
    2      ab......  cdef....  NedBufShrink(pBuf,2,(NEDBUF_SIZE*2)-7)
  */
  if(pBuf && removeLen)
  {
    thisLen = (unsigned)removeLen;
    memmove(&pBuf->aData[0], &pBuf->aData[thisLen], pBuf->len - thisLen);
    pBuf->len -= thisLen;
#if NEDDBG_LEVEL > 1
  memset(&pBuf->aData[pBuf->len], NEDBUF_FILL_EMPTY, thisLen);
#endif
  }

  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Returns TRUE if buffer is full

  @param    pBuf      pointer to valid buffer
  @return   TRUE if buffer is full, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedBufIsFull(const sNedBuffer_t *pBuf)
{
  return (pBuf->len >= NedBufMaxLen()) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Returns how many bytes currently in the buffer

  @param    pBuf      pointer to valid buffer
  @return   number of bytes in this buffer (0-n)
*///-----------------------------------------------------------------------------------------------
unsigned NedBufLen(const sNedBuffer_t *pBuf)
{
  return pBuf->len;
}

/*!------------------------------------------------------------------------------------------------
  Returns how large a buffer could be. Global for system.

  @return  maximum number of bytes in a buffer
*///-----------------------------------------------------------------------------------------------
unsigned NedBufMaxLen(void)
{
  return m_maxBufLen;
}

/*!------------------------------------------------------------------------------------------------
  Return ID for a buffer chain.

  @param    pBuf      pointer to valid buffer
  @return   id for this buffer chain
*///-----------------------------------------------------------------------------------------------
unsigned NedBufId(const sNedBuffer_t *pBuf)
{
  NedAssertDbg(NedBufIsBuf(pBuf));
  if(!pBuf)
    return 0;
  return pBuf->id;
}

/*!------------------------------------------------------------------------------------------------
  Returns buffer chain head.

  @param    pBuf      pointer to valid buffer
  @return   pointer to buffer chain head. NULL if not a buffer chain.
*///-----------------------------------------------------------------------------------------------
sNedBuffer_t * NedBufHead(const sNedBuffer_t *pBuf)
{
  if(!NedBufIsBuf(pBuf))
    return NULL;
  while(pBuf->pPrev)
    pBuf = pBuf->pPrev;
  return (sNedBuffer_t *)pBuf;
}

/*!------------------------------------------------------------------------------------------------
  Returns buffer chain tail.

  @param    pBuf      pointer to valid buffer
  @return   pointer to buffer chain tail. NULL if not a buffer chain.
*///-----------------------------------------------------------------------------------------------
sNedBuffer_t * NedBufTail(const sNedBuffer_t *pBuf)
{
  if(!NedBufIsBuf(pBuf))
    return NULL;
  while(pBuf->pNext)
    pBuf = pBuf->pNext;
  return (sNedBuffer_t *)pBuf;
}

/*!------------------------------------------------------------------------------------------------
  Return next buffer.

  @return  pointer to next buffer or NULL if at tail.
*///-----------------------------------------------------------------------------------------------
uint8_t * NedBufData(const sNedBuffer_t *pBuf)
{
  if(!NedBufIsBuf(pBuf))
    return NULL;
  return (uint8_t *)(&pBuf->aData[0]);
}

/*!------------------------------------------------------------------------------------------------
  Return next buffer.

  @param    pBuf    pointer to valid buffer
  @return   pointer to next buffer or NULL if at tail.
*///-----------------------------------------------------------------------------------------------
sNedBuffer_t * NedBufNext(const sNedBuffer_t *pBuf)
{
  if(!NedBufIsBuf(pBuf))
    return NULL;
  return pBuf->pNext;
}

/*!------------------------------------------------------------------------------------------------
  Return previous buffer.

  @param    pBuf    pointer to valid buffer
  @return   pointer to previous buffer or NULL if at head.
*///-----------------------------------------------------------------------------------------------
sNedBuffer_t * NedBufPrev(const sNedBuffer_t *pBuf)
{
  if(!NedBufIsBuf(pBuf))
    return NULL;
  return pBuf->pPrev;
}

/*!------------------------------------------------------------------------------------------------
  Load a file into buffer chain at the end of the current buffer. File size can be up to LONG_MAX,
  or remaining memory. Even if an error is returned, the buffer may have been modified (partial
  file)

  @param    pBufHead      pointer to valid buffer chain
  @param    szFilePath    file name and/or path          
  @param    piFileSize    Can be NULL if not interested in file size (up to LONG_MAX)
  @return   NEDERR_NONE       if worked. *pBufHead points to head buffer.
            NEDERR_BADPARM    if bad parameters
            NEDERR_READ       if can't open/read the file
            NEDERR_NOMEM      if ran out of memory reading file
            NEDERR_TOOLARGE   file too large to fit in Ned (LONG_MAX)
*///-----------------------------------------------------------------------------------------------
nedErr_t NedBufFileLoad(sNedBuffer_t *pBufHead, const char *szFilePath, long *pFileSize)
{
  FILE          *fp         = NULL;
  sNedBuffer_t  *pBuf       = NULL;
  unsigned       len        = 0;
  unsigned       thisLen    = 0;
  nedErr_t       err        = NEDERR_NONE;
  long           fileSize   = 0;

  NedLogPrintfEx(NEDLOG_BUF, "NedBufFileLoad(%s)\n", szFilePath);

  // check for bad parameters
  if(!szFilePath || !NedBufIsBufChain(pBufHead))
    err = NEDERR_BADPARM;

  // open file
  if(!err)
  {
    fp = fopen(szFilePath, "rb");
    if(!fp)
      err = NEDERR_READ;
  }

  // if the file was opened, read it
  if(!err && fp)
  {
    // read file in buffer sized chunks
    // 1st buffer might be short if pBufHead already has stuff in it.
    pBuf = pBufHead;
    while(pBuf)
    {
      // get as much as we can into the buffer
      len  = NedBufMaxLen() - pBuf->len;

      // don't allow file size to exceed LONG_MAX
      if(fileSize >= LONG_MAX - len)
      {
        err = NEDERR_TOOLARGE;
        break;
      }

      if(len)
      {
        // read into allocated buffer
        thisLen = fread(&pBuf->aData[pBuf->len], 1, len, fp);
        {
          char c = '.';
          if(thisLen && isprint(pBuf->aData[pBuf->len]))
            c = pBuf->aData[pBuf->len];
          NedLogPrintfEx(NEDLOG_BUF, "  len %u, thisLen %u bytes pBuf %p, pBuf->len %u, %c\n", len, thisLen,
              pBuf, pBuf->len, c);
        }
        if(len == 0)
          break;

        pBuf->len += thisLen;
        fileSize  += thisLen;

        if(thisLen != len)
          break;
      }

      // get another buffer for more data (if out of memory, break)
      pBuf = NedBufAppend(pBuf);
      if(!pBuf)
        break;
    }
  }

  // check for errors
  if(!err)
  {
    if(!pBuf)
      err = NEDERR_NOMEM; // ran out of memory reading the file, or bad bufhead
  }

  // if a failure, partial file is better than nothing
  // it's up to caller to release memory
  fclose(fp);
  if(pFileSize)
    *pFileSize = fileSize;

  return err;
}

/*!------------------------------------------------------------------------------------------------
  Save a file into from a buffer chain.

  @param    szFilePath    file name and/or path          
  @param    pBufHead      pointer to valid buffer chain (will clear chain if not already)
  @return   NEDERR_NONE     if worked. *pBufHead points to head buffer.
            NEDERR_BADPARM  if bad parameters
            NEDERR_WRITE     if can't open/read the file
            NEDERR_NOMEM    if ran out of memory reading file
*///-----------------------------------------------------------------------------------------------
nedErr_t NedBufFileSave(const char *szFilePath, const sNedBuffer_t *pBufHead)
{
  FILE                *fp     = NULL;
  const sNedBuffer_t  *pBuf   = NULL;
  size_t               len;
  size_t               len2;
  nedErr_t             err    = NEDERR_NONE;

  // check for bad parameters
  if(!szFilePath || !NedBufIsBufChain(pBufHead))
    err = NEDERR_BADPARM;

  // open file
  if(!err)
  {
    fp = fopen(szFilePath, "wb");
    if(!fp)
      err = NEDERR_WRITE;
  }

  // write all buffers
  if(!err)
  {
    pBuf = pBufHead;
    while(pBuf)
    {
      len = NedBufLen(pBuf);
      if(len > 0)
      {
        len2 = fwrite(&pBuf->aData[0], 1, len, fp);
        if(len2 != len)
        {
          err = NEDERR_WRITE;
          break;
        }
      }
      pBuf = NedBufNext(pBuf);
    }

    fclose(fp);
  }

  return err;
}

/*!------------------------------------------------------------------------------------------------
  Is this a valid buffer

  @param    pBufHead       pointer to buffer
  @return   Returns 0 if failed. Returns # of buffers in list (1-n) otherwise.
*///-----------------------------------------------------------------------------------------------
bool_t NedBufIsBuf(const sNedBuffer_t *pBuf)
{
  return ( pBuf && (pBuf->sanchk == NEDBUF_SANCHK) && (pBuf->len <= NedBufMaxLen()) ) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Check the linked list integrity. Also checks length for integrity.

  @param    pBufHead       pointer to head of linked list
  @return   Returns TRUE if is a valid buffer chain
*///-----------------------------------------------------------------------------------------------
bool_t NedBufIsBufChain(const sNedBuffer_t *pBufHead)
{
  return (NedBufChainSize(pBufHead, NULL) > 0 ? TRUE : FALSE);
}

/*!------------------------------------------------------------------------------------------------
  Are these two buffers of the same chain?

  @param    pBuf1       pointer to valid buffer
  @param    pBuf2       pointer to valid buffer
  @return   Returns TRUE iof same buffer chain
*///-----------------------------------------------------------------------------------------------
bool_t NedBufIsSameChain(const sNedBuffer_t *pBuf1, const sNedBuffer_t *pBuf2)
{
  return (NedBufId(pBuf1) == NedBufId(pBuf2)) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Count both # of bytes (0-LONG_MAX) and # of buffers in buffer chain (1-LONG_MAX). Also checks
  buffer link and structure integrity in both directions.

  @param    pBufHead      pointer to head of linked list
  @param    pNumBytes     number of bytes in chain (if not NULL)
  @returns  0 if bad chain, or # of buffers in chain, and number of bytes in chain (0-n)
*///-----------------------------------------------------------------------------------------------
long NedBufChainSize(const sNedBuffer_t *pBufHead, long *pNumBytes)
{
  const sNedBuffer_t   *pBuf;
  const sNedBuffer_t   *pLast;
  bool_t                fIsBufChain = TRUE;
  unsigned              id;
  long                  numBufs     = 0;
  long                  numBufsBack = 0;
  long                  numBytes    = 0;

  // printf("NedBufChainSize(pBufHead %p, pNumBytes %p)\n",pBufHead,pNumBytes);
  // NedBufLogDumpChain(pBufHead, 16);

  // not a buffer, can't be a chain
  if(NedBufIsBuf(pBufHead))
  {
    pBuf    = NedBufHead(pBufHead);
    id      = pBuf->id;
    while(fIsBufChain && pBuf)
    {
      // make sure entire buffer chain has same ID
      if(!NedBufIsBuf(pBuf) || (pBuf->id != id) || (numBufs == 8))   // LONG_MAX
      {
        fIsBufChain = FALSE;
      }

      // count
      numBytes += pBuf->len;
      ++numBufs;

      // on to next buffer
      pLast     = pBuf;
      pBuf      = pBuf->pNext;
    }

    // check back links
    numBufsBack = 0;
    pBuf        = pLast;
    while(fIsBufChain && pBuf)
    {
      // make sure entire buffer chain has same ID
      if(!NedBufIsBuf(pBuf) || (pBuf->id != id) || (numBufsBack == 8)) // LONG_MAX
      {
        fIsBufChain = FALSE;
      }
      pBuf  = pBuf->pPrev;
      ++numBufsBack;
    }

    if(numBufs != numBufsBack)
    {
      fIsBufChain = FALSE;
    }
  }

  // not a buffer chain, return 0
  if( !fIsBufChain || (numBufs==LONG_MAX) || (numBytes==LONG_MAX) )
  {
    return 0;
  }

  // is a buffer chain, return number of buffers (and numBytes if requested)
  if(pNumBytes)
    *pNumBytes = numBytes;

  return numBufs;
}

/*!------------------------------------------------------------------------------------------------
  Is this the first buffer in the chain?

  @param    pBuf       pointer to valid buffer
  @return   Returns TRUE if is the first buffer in the chain
*///-----------------------------------------------------------------------------------------------
bool_t NedBufIsFirst(const sNedBuffer_t *pBuf)
{
  return (pBuf->pPrev == NULL) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Is this the last buffer in the chain?

  @param    pBuf      pointer to valid buffer
  @return   Returns TRUE if is the last buffer in the chain
*///-----------------------------------------------------------------------------------------------
bool_t NedBufIsLast(const sNedBuffer_t *pBuf)
{
  return (pBuf->pNext == NULL) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Removes all empty space in each buffer to reduce the # of buffers.

  @param    pBufHead      pointer to head of buffer chain
  @return   Returns TRUE if worked, FALSE if not enough room
*///-----------------------------------------------------------------------------------------------
void NedBufCondense(sNedBuffer_t *pBufHead)
{
  sNedBuffer_t    *pBuf;
  unsigned         thisLen;

  while(pBufHead)
  {
    pBuf = NedBufNext(pBufHead);
    if(!pBuf)
      break;

    // if another buffer and room in this buffer
    if(pBufHead->len < NedBufMaxLen())
    {
      thisLen = NedBufMaxLen() - pBufHead->len;
      if(thisLen > pBuf->len)
        thisLen = pBuf->len;

      if(thisLen)
      {
        memcpy(&pBufHead->aData[pBufHead->len], &pBuf->aData[0], thisLen);
        if(pBuf->len > thisLen)
        {
          memmove(&pBuf->aData[0], &pBuf->aData[thisLen], pBuf->len - thisLen);
          pBuf->len -= thisLen;
        }
      }

      // remove empty buffers, unless last buffer
      if(pBuf->len == 0)
      {
        if((pBufHead->len < NedBufMaxLen()) || pBuf->pNext)
          NedBufFree(pBuf);
      }
    }

    // no need to go on to next buffer until this one is full
    if(pBufHead->len >= NedBufMaxLen())
      pBufHead = pBufHead->pNext;
  }
}

/*!------------------------------------------------------------------------------------------------
  Convert CR/LF (windows style) pair to LF (unix style) on each line.

  @param    pBufHead    pointer to head of buffer chain
  @return   TRUE if worked, FALSE if failed.
*///-----------------------------------------------------------------------------------------------
bool_t NedBufCrLfToLf(sNedBuffer_t *pBufHead)
{
  sNedBuffer_t   *pBuf;
  unsigned        i;

  pBuf = NedBufHead(pBufHead);
  while(pBuf)
  {
    // remove all CRs from buffer
    i = 0;
    while(i < pBuf->len)
    {
      if(pBuf->aData[i] == '\r')
      {
        if(((pBuf->len - i) - 1) > 0)
          memmove(&pBuf->aData[i], &pBuf->aData[i + 1], pBuf->len - i - 1);
        --pBuf->len;
      }
      else
        ++i;
    }
    pBuf = NedBufNext(pBuf);
  }
  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Convert LF (unix style) on each line to CR/LF pair (windows style)

  @param    pBuf       pointer to valid buffer
  @return   Returns 0 if failed. Returns # of buffers in list (1-n) otherwise.
*///-----------------------------------------------------------------------------------------------
bool_t NedBufLfToCrLf(sNedBuffer_t *pBufHead)
{
  sNedBuffer_t   *pBuf;
  unsigned        i;
  bool_t          fWorked = TRUE;

  pBuf = NedBufHead(pBufHead);
  while(pBuf)
  {
    // add CRs to line endings, \n becomes \r\n
    i = 0;
    while(fWorked && pBuf && (i < NedBufLen(pBuf)))
    {
      if(pBuf->aData[i] == '\n')
      {
        // push \n over one character to make room for \r
        fWorked = NedBufGrow(pBuf, i, 1);
        if(fWorked)
        {
          // now we're \r\n
          pBuf->aData[i] = '\r';

          // skip \r (++i outside while will skip the \n)
          ++i;

          // if pushed the \n into the next buffer by growing
          if( i >= NedBufLen(pBuf) )
          {
            pBuf = NedBufNext(pBuf);
            NedAssertDbg(NedBufLen(pBuf) >= 1);
            i = 0;
          }
        }
      }
      ++i;
    }

    // at end of buffer, search next
    pBuf = NedBufNext(pBuf);
  }

  return fWorked;
}

/*!------------------------------------------------------------------------------------------------
  Find the character in the buffer chain. Starts at pBufHead (which may or may not be top of chain)

  @param    pBufHead      buffer to start searching
  @param    c             character to search for
  @param    pOffset       offset to start searching in 1st buf, or NULL if don't care
  @return   Returns pBuf if found, NULL if not
*///-----------------------------------------------------------------------------------------------
const sNedBuffer_t *NedBufSearch(const sNedBuffer_t *pBufHead, uint8_t c, unsigned *pOffset)
{
  const sNedBuffer_t *pBufFound = NULL;
  const sNedBuffer_t *pBuf;
  unsigned      offset = 0;
  unsigned      thisLen;
  uint8_t      *p;

  if(pOffset)
    offset = *pOffset;

  pBuf = pBufHead;
  while(pBuf)
  {
    if(offset >= pBuf->len)
      offset = 0;
    thisLen = pBuf->len - offset;
    p = memchr(&pBuf->aData[offset], c, thisLen);
    if(p)
    {
      pBufFound = pBuf;
      offset = (unsigned)(p - &pBuf->aData[0]);
      break;
    }
    pBuf = pBuf->pNext;
  }

  if(pOffset)
    *pOffset = offset;
  return pBufFound;
}

/*!------------------------------------------------------------------------------------------------
  Set the maximum buffer size. Typically used for unit testing with smaller buffers.

  @param    len       length of buffer 0=default, (2-NEDBUF_SIZE)
  @return   Returns 0 if failed. Returns # of buffers in list (1-n) otherwise.
*///-----------------------------------------------------------------------------------------------
void NedBufSetMaxLen(unsigned len)
{
  if( (len < 2) || (len > NEDBUF_SIZE) )
    len = NEDBUF_SIZE;
  m_maxBufLen = len;
}

/*!------------------------------------------------------------------------------------------------
  Display the buffer chain to the log

  @param    pBufHead    pointer to valid buffer head
  @param    linelen     0=default, or 2-n
  @return   Returns number of buffers in chain, or 0 if failed.
*///-----------------------------------------------------------------------------------------------
long NedBufLogDumpChain(const sNedBuffer_t *pBufHead, unsigned linelen)
{
  const sNedBuffer_t   *pBuf    = pBufHead;
  long                  bufNum  = 1;

  // line length defaults (min 16, NedBufMaxLen())
  if(linelen < 2)
  {
    linelen = NedBufMaxLen();
    if(linelen > 32)
      linelen = 32;
  }

  while( pBuf && (bufNum < LONG_MAX) )
  {
    // bad buffer? we're done
    if(!NedBufIsBuf(pBuf))
    {
      NedLogPrintf("******** NedBufLogDumpChain(ERR: BAD BUFFER! - %p) *********\n", pBuf);
      bufNum = 0;
      break;
    }

    // display the buffer
    NedLogPrintf("%u-%ld: pBuf %p, len %u\n", pBufHead->id, bufNum, pBuf, pBuf->len);
    NedLogDump(NedBufData(pBuf), NedBufLen(pBuf), linelen, 2);

    pBuf = NedBufNext(pBuf);
    ++bufNum;
  }

  return bufNum;
}

