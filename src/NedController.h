/*!************************************************************************************************
  @file NedEditor.h

  Editor interface file

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_editor Ned Editor - An API wrapping all subsystems together for an editor.

*///***********************************************************************************************
#ifndef NED_CONTROLLER_H
#define NED_CONTROLLER_H

#include "Ned.h"
#include "NedKey.h"

extern const char gszNedSha[];

// minimal function set for higher layer (Ned.c)
bool_t      NedControllerStartup    (int argc, const char *argv[]);
void        NedControllerShutdown   (void);
nedKey_t    NedControllerGetKey     (void);
void        NedControllerFeedKey    (nedKey_t key);

#endif // NED_CONTROLLER_H
