/*!************************************************************************************************
  @file NedAnsi.h

  @brief Ned ANSI framework API

  @copyright Copyright (c) 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_ansi Ned ANSI - a framework for ANSI terminal code color and cursor positioning.

  The Ned ANSI framework is useful for making full screen text applications or colorizing debug
  logging. It works over SSH and other terminal connections, as will as in a local BASH or other
  shells that support ANSI terminal codes.

  The framework doesn't include a print, as standard printf() works fine for that purpose.

  The attributes are a subset of the ANSI attributes to fit them into a single byte. To view them
  all, use AnsiShowAllColors().

  While you can used `NedAnsiGoto()` to move the curosr, the framework doesn't keep track of where
  the cursor is, so your program will need to do that if it needs to.

  Example:

      #include "NedAnsi.h"
      int main(void)
      {
        AnsiShowAllColors();
        return 0;
      }

*///***********************************************************************************************

#ifndef NED_ANSI_H
#define NED_ANSI_H
#include "Ned.h"


/*!
  This is a bitmask of one NEDATTR_ with one NEDBACK_ color, and optionally
  NEDATTR_BOLD. Use NEDATTR_RESET by itself (no background or bold).

  Example: AnsiSetAttr(NEDATTR_BOLD | NEDATTR_RED | NEDBACK_PURPLE);
*/
typedef uint8_t nedAttr_t;
#define NEDATTR_NONE                  0     //!< default terminal color
#define NEDATTR_RESET                 0     //!< default terminal color
#define NEDATTR_BLACK                 0x00  //!< black text
#define NEDATTR_RED                   0x01  //!< red text
#define NEDATTR_GREEN                 0x02  //!< green text
#define NEDATTR_YELLOW                0x03  //!< yellow text
#define NEDATTR_BLUE                  0x04  //!< blue text
#define NEDATTR_PURPLE                0x05  //!< purple (magenta) text
#define NEDATTR_CYAN                  0x06  //!< cyan (blue-green) text
#define NEDATTR_WHITE                 0x07  //!< white text
#define NEDATTR_BOLD                  0x08  //!< make text bolder and brighter
#define NEDBACK_TERMINAL              0x00  //!< background color of terminal
#define NEDBACK_BRICK                 0x10  //!< brick colored background
#define NEDBACK_FOREST                0x20  //!< forest (deep green) background
#define NEDBACK_BLACK                 0x30  //!< black background
#define NEDBACK_NIGHT                 0x40  //!< navy blue background
#define NEDBACK_PURPLE                0x50  //!< purple background
#define NEDBACK_AQUA                  0x60  //!< aqua background
#define NEDBACK_GREY                  0x70  //!< grey background
#define NEDBACK_CHARCOAL              0x80  //!< dark grey background
#define NEDBACK_RED                   0x90  //!< red background
#define NEDBACK_GREEN                 0xA0  //!< green background
#define NEDBACK_YELLOW                0xB0  //!< yello background
#define NEDBACK_BLUE                  0xC0  //!< blue background
#define NEDBACK_VIOLET                0xD0  //!< violet background
#define NEDBACK_CYAN                  0xE0  //!< cyan background
#define NEDBACK_WHITE                 0xF0  //!< white background

// some default colors
#define NEDATTR_EDIT_FRAME            (NEDATTR_WHITE  | NEDATTR_BOLD | NEDBACK_NIGHT)
#define NEDATTR_EDIT_TXT              (NEDATTR_WHITE  | NEDATTR_BOLD | NEDBACK_NIGHT)
#define NEDATTR_ALERT_FRAME           (NEDATTR_YELLOW | NEDATTR_BOLD | NEDBACK_RED)
#define NEDATTR_ALERT_TXT             (NEDATTR_WHITE  | NEDATTR_BOLD | NEDBACK_RED)

void        AnsiGoto           (unsigned row, unsigned col);
void        AnsiClearScreen    (void);
void        AnsiClearEol       (void);
void        AnsiSetAttr        (nedAttr_t attr);
const char *AnsiGetAttrStr     (nedAttr_t attr);
void        AnsiGetRowsCols    (unsigned *pRows, unsigned *pCols);
void        AnsiShowAllColors  (void);

size_t      AnsiCharCount      (void);
void        AnsiCharCountReset (void);

#endif // NED_ANSI_H
