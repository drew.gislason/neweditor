/*!************************************************************************************************
  @file NedUtils.h

  @brief  General Ned Utilities for string manipulation and other things.

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_utils Ned Utils - An API for various utilities that aren't Ned specific.

*///***********************************************************************************************
#ifndef NED_UTIL_STR_H
#define NED_UTIL_STR_H

#include <ctype.h>
#include <stdio.h>
#include <limits.h>
#include <time.h>
#include "Ned.h"

typedef unsigned char nedChar_t;
#define NEDCHAR_EOF   0xfe   // EOF not in the file buffer, just a return value

typedef enum
{
  TAB_SMART,    // indentation only
  TAB_HARD,
  TAB_SPACES
} nedTabType_t;

typedef enum
{
  IS_ALNUM,     // includes 'a'-'z','A'-'Z','0'-'9' and '_' (C/C++/Python definition)
  IS_SPACE,     // ' ' and '\t'
  IS_LF,        // '\r' and '\n'
  IS_EOF,       // eof
  IS_PUNCT      // anything else
} nedCharType_t;

nedCharType_t     NedCharType       (nedChar_t c);

typedef enum
{
  IS_LOWER_CASE = 0,    // lower
  IS_UPPER_CASE,        // UPPER
  IS_CAMEL_CASE,        // camelCase
  IS_MIXED_CASE,        // MixedCase
  IS_SNAKE_CASE,        // snake_case
  IS_CONSTANT_CASE,     // CONSTANT_CASE
} nedStrCase_t;

#define NED_CHAR_DUMP_LINE_LEN     80
#define NED_CHAR_DUMP_COLS         16

// camelCase, MixedCase, snake_case, UPPER, lower
nedStrCase_t      NedStrIsCase      (const char *sz);
size_t            NedStrToCase      (char *szNew, const char *szOld, size_t maxLen, nedStrCase_t strCase);

// various string helper functions
char              NedStrLastChar    (const char *szStr);
const char       *NedStrLastCharOf  (const char *szStr, char c);
const char       *NedStrArgCpy      (char *szDst, const char *szSrc, size_t len);
char             *NedStrTmpBuf      (void);
unsigned          NedStrTmpBufMaxLen(void);

// ISO 8601 date/time functions
const char       *NedStrDateTime    (time_t time);
const char       *NedStrDateTimeCur (void);

// various string filename/path functions
const char       *NedStrLastSlash   (const char *szFullPath);
const char       *NedStrPrevSlash   (const char *szFullPath, const char *psz);
bool_t            NedStrIsFolder    (const char *szPath);
const char       *NedStrHomeFolder  (void);
const char       *NedStrHomePath    (const char *szFullPath);
const char       *NedStrNameOnly    (const char *szFullPath);
const char       *NedStrNameDirOk   (const char *szFullPath);
const char       *NedStrExt         (const char *szFilename);
const char       *NedStrDirOnly     (const char *szFullPath);
const char       *NedStrDirOnlyHome (const char *szFullPath);
const char       *NedStrFitName     (const char *szFullPath, unsigned width);
bool_t            NedStrAppendName  (char *szPath, const char *szName);
const char       *NedStrExpandTabs  (char *sz, size_t maxLen, unsigned startCol, unsigned tabSize);
size_t            NedStrWhereDiff   (char *szThis, char *szThat, size_t n);

// mem type things
bool_t            memisfilled       (const void *s, int c, size_t n);
void             *NedMemRChr        (const void *s, int c, size_t n);
int               NedMemICmp        (const void *pThis, const void *pThat, size_t len);
size_t            NedMemRemoveExtraSpaces(nedChar_t *pData, size_t len);
size_t            NedMemFindWrap    (const nedChar_t *pLine, size_t len, size_t wrapWidth);

// miscellaneous
size_t            NedStrZCpy        (char *szDst, const char *szSrc, size_t sizeDst);
size_t            NedStrIns         (char *szDst, size_t offset, size_t sizeDst, const char *szSrc);
char             *NedStrIChr        (const char *sz, int c);

// miscellaneous
const char       *NedWCharToUtf8    (uint16_t c);
unsigned          NedCharDumpLine   (char *szDst, long addr, uint8_t *pData, unsigned len);

// arrays of string pointers
char            **NedUtilStrArrayCopy   (const char **asz);
void              NedUtilStrArrayFree   (const char **asz);
int               NedUtilStrArrayCmp    (const char **aszThis, const char **aszThat);
char            **NedUtilStrArrayMalloc (size_t numElem, size_t sizeOf);
char             *NedUtilStrArrayText   (const char **asz);
size_t            NedUtilStrArrayNumElem(const char **asz);
size_t            NedUtilStrArraySizeOf (const char **asz);
void              NedUtilStrArrayPrint  (const char **asz);

#endif // NED_UTIL_STR_H
