/*!************************************************************************************************
  @file NedFile.c

  @brief  Ned File Implementation

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_file
*///***********************************************************************************************
#include "Ned.h"

#include "NedLog.h"
#include "NedFile.h"


/*!------------------------------------------------------------------------------------------------
  Open or create a file (a new file object). Adds to end of pFileList (if pFileList not NULL).
  Autodetects CrLf

  Examples:

      file.c          if doesn't exist, creates empty buffer, if does exist, loads file
      ~/Work/file.c   if doesn't exist, creates empty buffer, if does exist, loads file
      /folder         fails (returns NULL), can't open a folder
      NULL            creates empty buffer "untitled", "untitled1", etc...

  @param      pFileList   pointer to file list head, or NULL if no list
  @param      szPath      path to file, directory must exist if included, NULL creates "untitled"
  @return     pointer to sNedFile_t, or NULL if failed.
*///-----------------------------------------------------------------------------------------------
sNedFile_t * NedFileNew(sNedFile_t *pFileList, const char *szPath)
{
  sNedFile_t   *pFile;
  bool_t        fFailed = FALSE;
  bool_t        fExists = FALSE;
  bool_t        fSaveAs = FALSE;
  const char   *szDir;

  NedLogPrintfEx(NEDLOG_FILE, "NedFileNew(pFileList %p, szPath %s)\n", pFileList, szPath ? szPath : "NULL");

  pFile = malloc(sizeof(sNedFile_t));
  if(pFile)
  {
    // initialize structure
    memset(pFile, 0, sizeof(sNedFile_t));
    pFile->sanchk = NEDFILE_SANCHK;
    // fSaveAs, rdOnly, fDirty, fInSelect implicitely FALSE due to memset 0
    pFile->topLine  = 1;
    pFile->curLine  = 1;
    pFile->curCol   = 1;
    pFile->leftCol  = 1;
    pFile->prefCol  = 1;

    // NULL path means pick a unique name
    if(szPath == NULL)
    {
      szPath = NedFileUniqueName(pFileList, NULL);
      fSaveAs = TRUE;
    }

    // check if file exists and actually is a file
    szPath = NedStrHomePath(szPath);
    if(!NedFileInfoGet(&pFile->sInfo, szPath))
    {
      // if it doesn't exist, does the directory exist?
      szDir = NedStrDirOnlyHome(szPath);
      if(*szDir == '\0')
        szDir = ".";
      if(!NedFileInfoGet(&pFile->sInfo, szDir))
        fFailed = TRUE;
      else
      {
        if(strlen(pFile->sInfo.szFullPath) + strlen(NedStrNameOnly(szPath)) + 2 > PATH_MAX)
          fFailed = TRUE;
        else
        {
          strcat(pFile->sInfo.szFullPath, "/");
          strcat(pFile->sInfo.szFullPath, NedStrNameOnly(szPath));
        }
      }
    }
    else if(pFile->sInfo.fIsDir)
    {
      fFailed = TRUE;
    }
    else
    {
      fExists = TRUE;
    }
    NedLogPrintfEx(NEDLOG_FILE, "  created name %s, exists %u, failed %u\n", pFile->sInfo.szFullPath, fExists, fFailed);

    // Load the file if exists. Create the buffer in any case.
    if(!fFailed)
    {
      if(!pFile->sInfo.fIsDir)
        pFile->fRdOnly = pFile->sInfo.fRdOnly;
      pFile->sInfo.fIsDir       = FALSE;
      pFile->sInfo.fIsModified  = FALSE;
      pFile->fSaveAs            = fSaveAs;
      pFile->pBufHead = NedBufNew(NedFileUniqueId(pFileList));
      if(!pFile->pBufHead)
        fFailed = TRUE;
      else
      {
        NedPointInit(&pFile->sCursor, pFile->pBufHead);
        NedLogPrintfEx(NEDLOG_FILE, "  pBufHead %p, buf id %u\n", pFile->pBufHead, NedBufId(pFile->pBufHead));
        if(fExists)
        {
          if(!NedFileLoad(pFile))
            fFailed = TRUE;
        }
      }

      // add to end of linked list
      if(!fFailed && pFileList)
      {
        while(pFileList->pNext)
          pFileList = pFileList->pNext;
        pFileList->pNext = pFile;
      }
    }

    if(fFailed)
    {
      if(pFile)
      {
        if(pFile->pBufHead)
          NedBufChainFree(pFile->pBufHead);
        free(pFile);
      }
      pFile = NULL;
    }

    if(!fFailed)
    {
      NedPointInit(&pFile->sSelect, pFile->pBufHead);
      NedPointInit(&pFile->sCursor, pFile->pBufHead);
      NedLogPrintfEx(NEDLOG_FILE, "  worked: fIsDir %u, fIsModified %u, fSaveAs %u, fCrLf %u, tabSize %u, file %s\n",
           pFile->sInfo.fIsDir, pFile->sInfo.fIsModified, pFile->fSaveAs, pFile->fCrLf, pFile->tabSize, pFile->sInfo.szFullPath);
    }
  }

  return pFile;
}

/*!------------------------------------------------------------------------------------------------
  Return name of file, or NULL if not a valid pFile structure.

  Example: turns "/users/drewg/work/git/proj/file.c" to "file.c"

  @param      pFile        pointer to file structure
  @return     filename.ext
*///-----------------------------------------------------------------------------------------------
const char *NedFileNameOnly(const sNedFile_t *pFile)
{
  NedAssert(NedFileIsFile(pFile));
  return (const char *)NedStrNameOnly(pFile->sInfo.szFullPath);
}

/*!------------------------------------------------------------------------------------------------
  Get the filename, but with 0 or more path levels

  @param      pFile       pointer to file structure
  @param      pathLevel   0-n
  @return     filename with 1 or more path levels, e.g. "git/proj/file.c"
*///-----------------------------------------------------------------------------------------------
const char *NedFileGetNameEx(const sNedFile_t *pFile, unsigned pathLevel)
{
  const char *pszFileName = NedFileNameOnly(pFile);

  while(pathLevel > 0)
  {
    pszFileName = NedStrPrevSlash(pFile->sInfo.szFullPath, pszFileName);
    if(pszFileName == pFile->sInfo.szFullPath)
      break;
    --pathLevel;
  }

  return pszFileName;
}

/*!------------------------------------------------------------------------------------------------
  Similar to NedFileNameOnly, but keeps using more characters until no duplicates exist in
  pFileList.

  For example, if the file list contained:
  /Users/drewg/test.c
  /Users/drewg/Work/Git/ned/src/test.c
  /Users/drewg/Work/Git/ned/test/test.c

  These three files would return as:
  "drewg/test.c"
  "src/test.c"
  "test/test.c"

  @param      pFileList    pointer to list of files
  @param      pFile        pointer to file structure
  @return     partial to full file path
*///-----------------------------------------------------------------------------------------------
const char *NedFileNameNoDup(sNedFile_t *pFileList, sNedFile_t *pFile)
{
  unsigned      pathLevel = 0;
  sNedFile_t   *pThisFile;
  bool_t        fDuplicate;
  const char   *pszNameEx;
  const char   *pszThisNameEx;

  NedAssert(NedFileIsFile(pFileList));
  NedAssert(NedFileIsFile(pFile));

  // check fof duplicates, but give up after 5 dir levels (so window is not too wide)
  pathLevel = 0;
  while(pathLevel < 5)
  {
    fDuplicate = FALSE;
    pThisFile = pFileList;
    pszNameEx = NedFileGetNameEx(pFile, pathLevel);
    NedLogPrintfEx(NEDLOG_FILE, "pathLevel %u, pFile %s, NameEx %s\n", pathLevel, NedFilePath(pFile), pszNameEx);
    while(pThisFile)
    {
      if(pThisFile != pFile)
      {
        pszThisNameEx = NedFileGetNameEx(pThisFile, pathLevel);
        if((strcmp(pszNameEx, pszThisNameEx) == 0))
        {
          NedLogPrintfEx(NEDLOG_FILE, "Dup: pathLevel %u, nameEx %s, thisNameEx %s\n", pathLevel, pszNameEx, pszThisNameEx);
          fDuplicate = TRUE;
          break;
        }
      }
      pThisFile = pThisFile->pNext;
    }
    if(!fDuplicate)
      break;

    ++pathLevel;
  }

  return pszNameEx;
}

/*!------------------------------------------------------------------------------------------------
  Count how many files in the list. 

  @param      pFileList       pointer to head of file list (may be NULL if no files)
  @return     0-n
*///-----------------------------------------------------------------------------------------------
unsigned NedFileCount(const sNedFile_t *pFileList)
{
  unsigned  count = 0;
  while(NedFileIsFile(pFileList))
  {
    pFileList = NedFileNext(pFileList);
    ++count;
  }
  return count;
}

/*!------------------------------------------------------------------------------------------------
  Return a filelist index (0-n) of a point.

  @param      pFileList       file list to search
  @param      pPoint          point that should be in fileList
  @return     index of point (0-n), or UINT_MAX if point is not in file list.
*///-----------------------------------------------------------------------------------------------
unsigned NedFileIndex(const sNedFile_t *pFileList, const sNedPoint_t *pPoint)
{
  const sNedFile_t *pFile;
  unsigned          index       = 0;

  pFile = pFileList;
  while(pFile)
  {
    if(NedBufIsSameChain(pFile->pBufHead, pPoint->pBuf))
      return index;
    ++index;
    pFile = pFile->pNext;
  }
  return UINT_MAX;  // not found
}

/*!------------------------------------------------------------------------------------------------
  Is this a valid sNedFile_t?

  @param      pFile       pointer to ::sNedFile_t
  @return     TRUE if it's a valid sNedFile_t, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedFileIsFile(const sNedFile_t *pFile)
{
  return (pFile && (pFile->sanchk == NEDFILE_SANCHK)) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Does this file already exist in the file list?

  @param      pFileList   pointer to list of files
  @param      szPath      path to file (could be partial)
  @return     Pointer to file if exists, NULL if not
*///-----------------------------------------------------------------------------------------------
sNedFile_t *NedFileInList(const sNedFile_t *pFileList, const char *szPath)
{
  const sNedFile_t   *pFile   = pFileList;
  char                szFullPath[PATH_MAX];

  if(!NedLibFileFullPath(szFullPath, szPath))
    pFile = NULL;
  else
  {
    while(pFile)
    {
      if(strcmp(pFile->sInfo.szFullPath, szFullPath) == 0)
        break;
      pFile = pFile->pNext;
    }
  }
  return (sNedFile_t *)pFile;
}

/*!------------------------------------------------------------------------------------------------
  Get the next file in the list

  @param      pFile       pointer to ::sNedFile_t
  @return     next pFile (may be NULL)
*///-----------------------------------------------------------------------------------------------
sNedFile_t * NedFileNext(const sNedFile_t *pFile)
{
  NedAssertDbg(NedFileIsFile(pFile));

  if(pFile)
    return pFile->pNext;
  return NULL;
}

/*!------------------------------------------------------------------------------------------------
  Get the previous file in the list

  @param      pFile       pointer to file list
  @param      pFile       pointer to file
  @return     previous pFile (may be NULL)
*///-----------------------------------------------------------------------------------------------
sNedFile_t * NedFilePrev(const sNedFile_t *pFileList, const sNedFile_t *pFile)
{
  sNedFile_t * pFilePrev = NULL;

  NedAssert(NedFileIsFile(pFileList));
  NedAssert(NedFileIsFile(pFile));

  // if the list is not empty or the pFile is not the head of list, look for it
  if((pFileList != NULL) && (pFileList != pFile))
  {
    while(pFileList)
    {
      if(pFileList->pNext == pFile)
      {
        pFilePrev = (sNedFile_t *)pFileList;
        break;
      }
      pFileList = pFileList->pNext;
    }
  }

  return pFilePrev;
}

/*!------------------------------------------------------------------------------------------------
  Return the full path of the file.

  @param      pFile       pointer valid file
  @return     pointer to full path, e.g. "/Users/dg/file.c"
*///-----------------------------------------------------------------------------------------------
char * NedFilePath(const sNedFile_t *pFile)
{
  if(pFile == NULL)
    NedLogPrintfEx(NEDLOG_FILE, "NedFilePath(%p)\n", pFile);
  NedAssert(NedFileIsFile(pFile));

  return (char *)pFile->sInfo.szFullPath;
}

/*!------------------------------------------------------------------------------------------------
  Rename to a new file path (different folder or name)

  @param      pFile       pointer to  valid file
  @param      szNewPath   new path or file. string copy only (no validation)
  @return     pointer to full path (e.g. /Users/dg/file.c), or NULL if not a pFile pointer.
*///-----------------------------------------------------------------------------------------------
void NedFilePathRename(sNedFile_t *pFile, const char *szNewPath)
{
  NedAssert(NedFileIsFile(pFile));
  NedAssert(szNewPath && *szNewPath);

  strncpy(pFile->sInfo.szFullPath, szNewPath, PATH_MAX-1);
  pFile->sInfo.szFullPath[PATH_MAX-1] = '\0';
}

/*!------------------------------------------------------------------------------------------------
  Return the buffer head for this file.

  @param      pFile       pointer to valid file.
  @return     pointer to buffer head, or NULL if invalid file
*///-----------------------------------------------------------------------------------------------
sNedBuffer_t * NedFileBuf(const sNedFile_t *pFile)
{
  sNedBuffer_t  *pBufHead = NULL;

  if(NedFileIsFile(pFile))
    pBufHead = pFile->pBufHead;
  return pBufHead;
}

/*!------------------------------------------------------------------------------------------------
  Free the file and remove from file list. Returns pointer to new filelist (may have changed if
  freeing the 1st file in the list or the only file in the list).

  Does NOT save any file data. All data will be lost if not saved previously.

  @param      pFileList     pointer to head of list, may be NULL if file is stand-alone
  @param      pFile         pointer to file in list
  @return     NULL if last file is closed, otherwise pointer to new file list
*///-----------------------------------------------------------------------------------------------
sNedFile_t * NedFileFree(sNedFile_t *pFileList, sNedFile_t *pFile)
{
  sNedFile_t *pThisFile;

  NedAssertDbg(NedFileIsFile(pFile));

  // NedLogPrintfEx(NEDLOG_FILE, "NedFileFree(pFileList %p, pFile %p\n", pFileList, pFile);

  // not a file, can't free it, return NULL
  if(!NedFileIsFile(pFile))
    return NULL;

  // remove pFile from linked list
  if(pFileList)
  {
    // file at head, change pFileList
    if(pFile == pFileList)
      pFileList = pFile->pNext;

    // not at head, remove from linked list
    else
    {
      pThisFile = pFileList;
      while(pThisFile->pNext)
      {
        if(pThisFile->pNext == pFile)
        {
          pThisFile->pNext = pFile->pNext;
          break;
        }
        pThisFile = pThisFile->pNext;
      }
    }
  }

  // NedLogPrintfEx(NEDLOG_FILE, "  closed %p, new pFileList %p\n", pFile, pFileList);

  // free the file
  NedBufChainFree(pFile->pBufHead);
  memset(pFile, NEDDBG_FREE, sizeof(*pFile));
  free(pFile);

  // return the (potentially new) file list head
  return pFileList;
}

/*!------------------------------------------------------------------------------------------------
  Free all files in the list.

  Does NOT ask about saving modified files. That's up to a higher layer.

  @ingroup    ned_file
  @param      pFileList     pointer to head of list, may be NULL if file is stand-alone
  @param      pFile         pointer to file in list

  @return     pointer to (potentially new) pFileList head, or NULL if all files closed.
*///-----------------------------------------------------------------------------------------------
void NedFileFreeAll(sNedFile_t *pFileList)
{
  if(pFileList)
  {
    while(pFileList->pNext)
      NedFileFree(pFileList, pFileList->pNext);
    NedFileFree(pFileList, pFileList);
  }
}

/*!------------------------------------------------------------------------------------------------
  Return TRUE if file is binary (non-ascii chars)

  Sets fCrLf depending on content of the file

  @param      pFile         pointer to valid file
  @return     TRUE if file is binary
*///-----------------------------------------------------------------------------------------------
bool_t NedFileIsBinary(const sNedFile_t *pFile)
{
  sNedBuffer_t  *pBuf;
  unsigned       i;

  pBuf = NedFileBuf(pFile);
  while(pBuf)
  {
    for(i = 0; i < pBuf->len; ++i)
    {
      if(pBuf->aData[i] > 0x7f)
        return TRUE;
    }
    pBuf = pBuf->pNext;
  }

  return FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Load the file into memory. Assumes a buffer (perhaps empty) has already been allocated for the
  file. Used to load the initial file, revert or to reload a file changed outside the editor.

  Sets fCrLf, fHardTabs and tabSize depending on content of the file.

  @ingroup    ned_file
  @param      pFileList     pointer to head of list, may be NULL if file is stand-alone
  @param      pFile         pointer to file in list

  @return     TRUE if file was loaded successfully
*///-----------------------------------------------------------------------------------------------
bool_t NedFileLoad(sNedFile_t *pFile)
{
  nedErr_t          err;
  sNedBuffer_t     *pBufHead;
  sNedPoint_t       sPointTo;
  sNedPoint_t       sPointFrom;

  NedAssert(NedFileIsFile(pFile));

  // NedLogPrintfEx(NEDLOG_FILE, "NedFileLoad(pFile %p, pBufHead %p, path %s)\n", pFile,
  //     pFile->pBufHead, pFile->sInfo.szFullPath);

  // verify it's a file that we're loading.
  err = NedBufFileLoad(pFile->pBufHead, NedFilePath(pFile), NULL);
  if(err == NEDERR_NONE)
  {
    // binary file
    if(NedFileIsBinary(pFile))
    {
      NedLogPrintfEx(NEDLOG_FILE, "File %s Is Binary, id %u\n", NedFilePath(pFile), pFile->pBufHead->id);
      pBufHead = NedBufNew(pFile->pBufHead->id);
      if(pBufHead)
      {
        NedPointInit(&sPointTo, pBufHead);
        NedPointInit(&sPointFrom, pFile->pBufHead);
      }
      if(pBufHead && NedPointDumpToPoint(&sPointTo, 0, &sPointFrom, NedPointFileSize(&sPointFrom)))
      {
        NedBufChainFree(pFile->pBufHead);
        pFile->pBufHead = pBufHead;
        NedPointInit(&pFile->sCursor, pBufHead);
        pFile->fRdOnly    = TRUE;
        pFile->fIsBinary  = TRUE;
      }
      else if(pBufHead)
        NedBufFree(pBufHead);
    }

    // source file
    else
    {
      if(NedBufSearch(pFile->pBufHead, '\r', NULL))
      {
        pFile->fCrLf = TRUE;
        NedBufCrLfToLf(pFile->pBufHead);
      }
      else
      {
        pFile->fCrLf = FALSE;
      }

      // determine tab-type and size
      if(NedBufSearch(pFile->pBufHead, '\t', NULL))
      {
        pFile->fHardTabs = TRUE;
      }
      else
      {
        NedPointInit(&sPointFrom, pFile->pBufHead);
        pFile->tabSize   = NedPointFileIndent(&sPointFrom);
        pFile->fHardTabs = FALSE;
      }

      NedLogPrintfEx(NEDLOG_FILE, "  loaded file %s, fCrLf %u, fHardTabs %u, tabSize %u\n",
          pFile->sInfo.szFullPath, pFile->fCrLf, pFile->fHardTabs, pFile->tabSize);
      if(NedLogMaskGet() & NEDLOG_FILE)
        NedBufLogDumpChain(pFile->pBufHead, 16);
    }
  }

  NedLogPrintfEx(NEDLOG_FILE, "  err %u\n", err);
  return (err == NEDERR_NONE) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Save the file with the current filename. Saves with cr/lf or lf as specified in pFile structure
  fCrLf.

  @ingroup    ned_file
  @param      pFile         pointer to file in list

  @return     TRUE if successful
*///-----------------------------------------------------------------------------------------------
bool_t NedFileSave(sNedFile_t *pFile)
{
  sNedBuffer_t   *pNewBufHead;
  long            len;
  long            len2;
  nedErr_t        err = NEDERR_NONE;

  // create  new buffer to save it from, as we need to add CR/LF
  if(pFile->fCrLf)
  {
    pNewBufHead = NedBufNew(UINT_MAX - 5);
    NedAssert(pNewBufHead);
    NedBufChainSize(pFile->pBufHead, &len);
    len2 = NedBufChainCopy(pNewBufHead, 0, pFile->pBufHead, 0, len);
    NedAssert(len2 == len);
    NedBufLfToCrLf(pNewBufHead);
    NedBufChainSize(pNewBufHead, &len2);
    err = NedBufFileSave(NedFilePath(pFile), pNewBufHead);
    NedLogPrintfEx(NEDLOG_FILE, "NedFileSave(%s, added CR/LF old len %ld, newlen %ld, err %u\n",
        NedFilePath(pFile), len, len2, err);
    NedBufFree(pNewBufHead);
  }
  else
    err = NedBufFileSave(NedFilePath(pFile), pFile->pBufHead);

  // convert from error code to TRUE/FALSE
  return (err == NEDERR_NONE) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Clear the file to length 0 (empty).

  @ingroup    ned_file
  @param      pFile         pointer to file

  @return     none
*///-----------------------------------------------------------------------------------------------
void NedFileClear(sNedFile_t *pFile)
{
  NedAssertDbg(NedFileIsFile(pFile));

  if(pFile && pFile->pBufHead)
  {
    NedBufChainClear(pFile->pBufHead);
    NedPointInit(&pFile->sSelect, pFile->pBufHead);
    NedPointInit(&pFile->sCursor, pFile->pBufHead);
    pFile->topLine  = 1;
    pFile->curLine  = 1;
    pFile->curCol   = 1;
    pFile->leftCol  = 1;
    pFile->prefCol  = 1;
  }
}

/*!------------------------------------------------------------------------------------------------
  Return size of file in bytes

  @param    pFile         pointer to file

  @return   size of file (0-n)
*///-----------------------------------------------------------------------------------------------
long NedFileSize(const sNedFile_t *pFile)
{
  long          fileSize = 0L;
  sNedPoint_t   sPoint;

  if(NedFileIsFile(pFile))
  {
    NedPointInit(&sPoint, pFile->pBufHead);
    fileSize = NedPointFileSize(&sPoint);
  }

  return fileSize;
}

/*!------------------------------------------------------------------------------------------------
  Get the column, accounting for tabSize (and perhaps in the future UTF-8).
  Example: 

  @param    pFile       pointer to file
  @param    pPoint      a point in the file

  @return   column 1-n
*///-----------------------------------------------------------------------------------------------
long NedFileColGet(const sNedFile_t *pFile, const sNedPoint_t *pPoint)
{
  sNedPoint_t   pt;
  long          col = 0;
  long          nChars;
  nedChar_t     c;

  // start at beginning of line, even if off screen
  pt = *pPoint;
  NedPointLineBeg(&pt);
  nChars = NedPointDifference(&pt, pPoint);

  while(nChars)
  {
    // tab expands to 1-n columns, depeding on start column
    c = NedPointCharGet(&pt);
    if(c == '\t')
      col += pFile->tabSize - (col % pFile->tabSize);
    else
      ++col;
    NedPointCharNext(&pt);
    --nChars;
  }

  return col + 1; // file columns are 1 based
}

/*!------------------------------------------------------------------------------------------------
  Similar to NedPointLineGet(), but understands tabs. The returned column is 

  @param    pFile       pointer to file
  @param    pPoint      a point in the file
  @param    pCol        returned column (1-n), or NULL if column not needed

  @return   line this point is on (1-n)
*///-----------------------------------------------------------------------------------------------
long NedFileLineGet(const sNedFile_t *pFile, const sNedPoint_t *pPoint, long *pCol)
{
  long    curCol;
  long    curLine;

  curLine = NedPointLineGet(pPoint, NULL);

  // determine and return tabbed column (1-n)
  if(pCol)
  {
    curCol  = NedFileColGet(pFile, pPoint);
    *pCol = curCol;
  }

  return curLine;
}

/*!------------------------------------------------------------------------------------------------
  Set the point on the character closest to the preferred column, accounting for tabs and short
  lines. If the prefCol is past end of line, the column will be on the \n or EOF.

  Adjusts both pFile->sCursor and pFile->curCol. Does NOT adjust anything screen related like
  pFile->leftCol or pFile->topCol.

  @param    pFile       pointer to file
  @param    pPoint      a point in the file
  @param    prefCol     preferred column (1-n)

  @return   Actual column (1-n), accounting for tabs and line ending
*///-----------------------------------------------------------------------------------------------
long NedFileColSet(sNedFile_t *pFile, sNedPoint_t *pPoint, long prefCol)
{
  sNedPoint_t   sLineEnd;
  nedChar_t     c;
  long          col;        // 0 based column
  unsigned      tabChars;
  long          nChars;
  long          i;

  // make column 0 based for easier math
  NedAssert(prefCol >= 1);
  --prefCol;  

  // determine length of line the point is on, and start from the beginning of the line
  sLineEnd = *pPoint;
  NedPointLineBeg(pPoint);
  NedPointLineEnd(&sLineEnd);
  nChars = NedPointDifference(pPoint, &sLineEnd);

  // find prefCol or endOfLine, whichever comes first
  col = 0;
  for(i = 0; col < prefCol && i < nChars; ++i)
  {
    // tab expands to 1-n columns, depeding on start column
    c = NedPointCharGet(pPoint);
    if(c == '\t')
    {
      tabChars = pFile->tabSize - (col % pFile->tabSize);
      if(col + tabChars > prefCol)
        break;
      col += tabChars;
    }
    else
    {
      col += 1;
    }
    NedPointCharNext(pPoint);
  }

  // file columns are 1 based
  ++col;
  return col;
}

/*!------------------------------------------------------------------------------------------------
  Create a unique name (looks through the file list). The name will be "untitled.txt",
  "untitled1.txt" and so on until a unique name is created. An alternate file extension may be
  supplied.

  @ingroup    ned_file
  @param      pFileList     pointer to file list
  @param      szExt         file extension (.c, .py), or NULL for .txt

  @return     none
*///-----------------------------------------------------------------------------------------------
const char * NedFileUniqueName(const sNedFile_t *pFileList, const char *szExt)
{
  #define MAX_EXT        32
  #define UINT_MAX_CHARS 10 // 4294967295
  const char          szNameBase[]  = "untitled";
  static char         szName[sizeof(szNameBase) + UINT_MAX_CHARS + MAX_EXT];
  const sNedFile_t   *pFile;
  bool_t              fUnique;
  int                 len;
  static unsigned     i = 0;

  if(szExt == NULL)
    szExt = ".txt";

  // make sure it's unique in this file list
  while(TRUE)
  {
    if(i)
      len = snprintf(szName, sizeof(szName), "%s%u", szNameBase, i+1);
    else
      len = snprintf(szName, sizeof(szName), "%s", szNameBase);
    snprintf(&szName[len], sizeof(szName) - MAX_EXT, "%.*s", MAX_EXT, szExt);

    pFile   = pFileList;
    fUnique = TRUE;
    while(pFile)
    {
      if(strcmp(NedStrNameOnly(pFile->sInfo.szFullPath), szName) == 0)
      {
        fUnique = FALSE;
        break;
      }
      pFile = pFile->pNext;
    }

    // found a unique name, return it
    if(fUnique)
      break;
    ++i;
  }

  return (const char *)szName;
}

/*!------------------------------------------------------------------------------------------------
  checks the file list to create a unique buffer id.

  @ingroup    ned_file
  @param      pFileList      pointer to file list

  @return     none
*///-----------------------------------------------------------------------------------------------
unsigned NedFileUniqueId(const sNedFile_t *pFileList)
{
  static unsigned   id = 1;
  unsigned          ret_id;
  bool_t            fFound;
  const sNedFile_t *pFile;

  while(id < UINT_MAX)
  {
    pFile  = pFileList;
    fFound = FALSE;
    while(pFile)
    {
      if(pFile->pBufHead && (NedBufId(pFile->pBufHead) == id))
      {
        fFound = TRUE;
        break;
      }
      pFile = pFile->pNext;
    }
    if(!fFound)
      break;
    ++id;
  }

  ret_id = id;
  ++id;

  return ret_id;
}

/*!------------------------------------------------------------------------------------------------
  Show the file to the log

  @ingroup    ned_file
  @param      pFile       valid file
  @param      detail      how much detail to show

  @return     none
*///-----------------------------------------------------------------------------------------------
void NedFileLogShow(sNedFile_t *pFile, logDetail_t detail)
{

  NedLogPrintf("File: %p ", pFile);
  if(!NedFileIsFile(pFile))
  {
    NedLogPrintf("<= BAD!\n");
  }
  else
  {
    NedLogPrintf("%s, fDirty %u\n", NedFilePath(pFile), pFile->fDirty);
    if(detail >= LOG_MORE)
    {
      NedLogPrintf("top %ld, left %ld, line %ld, col %ld, inSelect %u, rdOnly %u, crLf %u, bufId %u\n",
          pFile->topLine, pFile->leftCol, pFile->curLine, pFile->curCol,
          pFile->fInSelect, pFile->fRdOnly, pFile->fCrLf, NedBufId(pFile->pBufHead));
      NedLogPrintf("select: ");
      NedPointLogShow(&pFile->sSelect);
      NedLogPrintf("cursor: ");
      NedPointLogShow(&pFile->sCursor);
    }
    if(detail >= LOG_MAX)
    {
      NedBufLogDumpChain(pFile->pBufHead, 16);
    }
  }
}
