/*!************************************************************************************************
  @file NedCmdMisc.c

  @brief  Ned Command Implementation for Miscellaneous commands

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  This includes any commands that don't fit into other categories.

  @ingroup  ned_cmd

*///***********************************************************************************************
#include "NedEditor.h"
#include "NedEditSubmenu.h"
#include "NedUndo.h"
#include "NedSettings.h"

static const char szRdOnly[]      = "File is read only";
static const char szAddFolder[]   = "Add Folder: ";

/*!------------------------------------------------------------------------------------------------
  Add/remove project folders

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdProjectFolders(void)
{
  hNedList_t        hList = NedEditor()->hProjectFolders;
  char              szPath[PATH_MAX];
  const char       *szPicked;
  long              len;
  nedKey_t          key;

  // get choice from pick window/submenu
  while(1)
  {
    NtmCommonSubMenuFeedKeys(NtmOptionProjectFoldersSubMenu, &m_OptionProjectFolders);

    if(m_OptionProjectFolders.pickedSubItem != NEDMENU_PICK_NONE)
    {
      key       = m_OptionProjectFolders.key;
      szPicked  = m_OptionProjectFolders.ppStrings[m_OptionProjectFolders.pickedSubItem];

      // adding a folder
      if(m_OptionProjectFolders.pickedSubItem == m_OptionProjectFolders.numItems - 1)
      {
        if(key == NEDKEY_ENTER)
        {
          strcpy(szPath, NedStrDirOnly(NedEditorProjPath()));
          len = NedMsgLineEdit(szPath, sizeof(szPath)-1, szAddFolder, NULL, NULL);
          if(len <= 0)
            NedMsgClear();
          else
          {
            NedListAdd(hList, szPath, len+1);
            NedMsgPrintf("Added %s", szPath);
          }
        }
      }

      // deleting folder
      else if(key == NEDKEY_DELETE || key == NEDKEY_BACKSPACE)
      {
        NedMsgPrintf("Removed %s", szPicked);
        NedListRemove(hList, szPicked);
      }

      // force a rebuild of project folder list upon doing this command
      if(m_OptionProjectFolders.ppStrings)
      {
        free(m_OptionProjectFolders.ppStrings);
        m_OptionProjectFolders.ppStrings = NULL;
      }
    }

    if(m_OptionProjectFolders.pickedSubItem == NEDMENU_PICK_NONE)
      break;
  }
}

/*!------------------------------------------------------------------------------------------------
  Helper function to controller to redraw the screen

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorRedrawScreen(void)
{
  void *pOldWin = NedViewWinGetPicked(NedEditorView());
  NedViewCheckScreenSize(NedEditorView());
  NedViewCls(NedEditorView());
  NedEditorRedraw();
  NedViewDraw(NedEditorView(), NedEditorFileCur());
  NedViewWinPick(NedEditorView(), pOldWin);
  NedViewFlushAll(NedEditorView());
}

/*!------------------------------------------------------------------------------------------------
  The command Alt-Space to force redraw the entire screen.

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdRedrawScreen(void)
{
  NedEditorRedrawScreen();
  NedMsgPrintf("Redraw Screen");
}

/*!------------------------------------------------------------------------------------------------
  This command is attached to all the keys that don't do anything

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdDoNothing(void)
{
}

/*!------------------------------------------------------------------------------------------------
  For debugging

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdDebug(void)
{
  // dump bookmarks
  NedLogPrintf("------- NedCmdDebug ---------\n");
  NedLogPrintf("Editor:\n");
  NedEditorLogShow(NedEditor(), NEDEDITOR_DETAIL_FILE_LIST);
  NedLogPrintf("PosHistory:\n");
  NedListLogShow(NedEditor()->hPosHistory, TRUE);
  NedLogPrintf("Bookmarks:\n");
  NedListLogShow(NedEditor()->hBookmarks, TRUE);
  NedLogPrintf("UndoList:\n");
  NedUndoLogShow(NedEditorUndoList(), TRUE);
  NedLogPrintf("Total ANSI Chars: %zu\n", AnsiCharCount());
  NedLogPrintf("------- NedCmdDebug End ---------\n");
  NedMsgPrintf("CmdDebug");
}

/*!------------------------------------------------------------------------------------------------
  Record a macro, or stop recording

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdMacroRecord(void)
{
  nedStatOpts_t  opts;

  NedEditorBeep();
  opts = NedStatusBarGetOpts();
  if(NedKeyMacroRecording())
  {
    opts = opts & (~NEDSTAT_OPT_MACRO);
    NedKeyMacroEndRecord();
    NedMsgPrintf("Stopped recording macro");
  }
  else
  {
    opts = opts | NEDSTAT_OPT_MACRO;
    NedKeyMacroRecord();
    NedMsgPrintf("Recording macro...");
  }
  NedStatusBarSetOpts(opts);
}

/*!------------------------------------------------------------------------------------------------
  Play the macro. Stop recording if in recording mode.

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdMacroPlay(void)
{
  NedKeyMacroPlay();
  NedMsgPrintf("Playback macro...");
}

/*!------------------------------------------------------------------------------------------------
  Select the whole file

  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdSelectAll(void)
{
  NedEditorFileCur()->fInSelect = TRUE;
  NedEditorAddPosHistory();
  *NedEditorSelect()  = *NedEditorCursor();
  NedPointFileTop(NedEditorSelect());
  NedPointFileBottom(NedEditorCursor());
  NedEditorInvalidateSelected();
  NedEditorBuildAroundCursor();
}

/*!------------------------------------------------------------------------------------------------
  Turns off select mode. Bound to Esc in the editor.

  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdSelectOff(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();

  NedLogPrintf("NedCmdSelectOff inSelect %u\n", pFile->fInSelect);

  if(pFile->fInSelect)
  {
    NedEditorInvalidateSelected();
    pFile->fInSelect = FALSE;
    NedMsgPrintf("Select Off");
  }
  else
    NedMsgClear();
}

/*!------------------------------------------------------------------------------------------------
  Select the line. Does 3 things:

  1. Select the current line (if no selection)
  2. Expand current partial line selection to whole line
  3. Extends selection to next line if line(s) already selected

  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdSelectLine(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();
  sNedPoint_t  *pTopPt;
  sNedPoint_t  *pBotPt;
  sNedPoint_t   sPoint;

  if(NedLogMaskGet() & NEDLOG_POINT)
    NedFileLogShow(pFile, LOG_MORE);

  // 1. Select the current line (if no selection)
  if(!pFile->fInSelect)
  {
    NedEditorAddPosHistory();
    *NedEditorSelect() = *NedEditorCursor();
    NedPointLineBeg(NedEditorSelect());
    NedPointLineEnd(NedEditorCursor());
    NedPointCharNext(NedEditorCursor());
    pFile->curCol = NedEditorColGet(NedEditorCursor());
    NedEditorFileCur()->fInSelect = TRUE;
  }

  else 
  {
    // determine top and bottom of selection
    if(NedPointDifference(NedEditorSelect(), NedEditorCursor()) < 0L)
    {
      pTopPt = NedEditorCursor();
      pBotPt = NedEditorSelect();
    }
    else
    {
      pTopPt = NedEditorSelect();
      pBotPt = NedEditorCursor();
    }

    // 2. Expand current partial line selection to whole line
    sPoint = *pTopPt;
    if(NedPointCharGetPrev(&sPoint) != '\n')
      NedPointLineBeg(pTopPt);

    // 3. Extends selection to next line if line(s) already selected
    NedPointLineEnd(pBotPt);
    NedPointCharNext(pBotPt);
  }

  NedEditorAdjustCurFileFields();   // may have scrolled off-screen
  NedEditorInvalidateSelected();
  NedEditorShowSelectedChars();

  if(NedLogMaskGet() & NEDLOG_POINT)
    NedFileLogShow(pFile, LOG_MORE);
}

/*!------------------------------------------------------------------------------------------------
  Select the current line

  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdSelectToggle(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();

  if(pFile->fInSelect)
  {
    NedEditorInvalidateSelected();
    pFile->fInSelect = FALSE;
    NedMsgPrintf("Select Off");
  }
  else
  {
    NedEditorAddPosHistory();
    *NedEditorSelect() = *NedEditorCursor();
    NedEditorShowSelectedChars();
    NedEditorFileCur()->fInSelect = TRUE;
  }
}

/*!------------------------------------------------------------------------------------------------
  Select the current word. Does 2 things:

  1. Select the current word (if no selection). Cursor is at end of word
  2. Expand selection to next word

  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdSelectWord(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();
  sNedPoint_t  *pBotPt;
  bool_t        fCursor = FALSE;
  nedCharType_t typeA;
  nedCharType_t typeB;

  NedEditorLogSelect("NedCmdSelectWord");

  // 1. Select the current word (if no selection). Cursor is at end of word
  if(!NedEditorFileCur()->fInSelect)
  {
    NedEditorAddPosHistory();
    *NedEditorSelect() = *NedEditorCursor();
    NedPointWordBeg(NedEditorSelect());
    NedPointWordEnd(NedEditorCursor());
    pFile->curCol = NedEditorColGet(NedEditorCursor());
    NedEditorFileCur()->fInSelect = TRUE;
  }

  // 2. Expand selection to next word
  else
  {
    // if cursor is BEFORE selection, we must reverse things
    if(NedPointDifference(NedEditorSelect(), NedEditorCursor()) < 0L)
      pBotPt = NedEditorSelect();
    else
    {
      fCursor = TRUE;
      pBotPt = NedEditorCursor();
    }
    typeA = NedCharType(NedPointCharGet(pBotPt));
    NedPointCharNext(pBotPt);
    typeB = NedCharType(NedPointCharGet(pBotPt));
    if(typeA == typeB)
      NedPointWordEnd(pBotPt);
    if(fCursor)
      NedEditorAdjustCurFileFields();  // cursor may have moved onto new line
  }

  NedEditorInvalidateSelected();
  NedEditorShowSelectedChars();
}

/*!------------------------------------------------------------------------------------------------
  Load the file nedproj.json wherever it is located. Saves current settings first, so it reflects
  any changes made since loading

  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdSettingsEdit(void)
{
  sNedFile_t     *pFile;
  const char     *szPath = NedEditorProjPath();

  NedSettingsSave(szPath, NedEditor());
  pFile = NedEditorFileNew(szPath, 0L);
  if(pFile)
  {
    NedEditorAddPosHistory();
    NedEditorSetFileCur(pFile);
    NedEditorBuildAroundCursor();
    pFile->fRdOnly = TRUE;
    NedMsgPrintf("If changing project, SaveAs or Save then ExitSaveNone via Alt-K");
  }
  else
    NedMsgPrintf("Couldn't open %s", szPath);
}

/*!------------------------------------------------------------------------------------------------
  Resets all to factory defaults. Does not affect bookmarks, recent files, or other things like that.

  1. Remaps keys (and keys in menus) back to factory defaults
  2. Sets theme to factory defaults.
  3. Sets language settings back to factor defaults

  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdSettingsFactory(void)
{
  NedKeyMapFactory();
  NedMenuMapAdjustMenuKeys();
  NedThemeInit();
  NedEditorSetTheme(NEDTHEME_FACTORY);
  NedLangInit();
  NedEditorNeedRedraw();
  NedMsgPrintf("Restored Factory Defaults");
}

/*!------------------------------------------------------------------------------------------------
  Map one or more keys to a command.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdSettingsMapKeys(void)
{
  nedKey_t    c;
  const char *szCmdName;

  while(1)
  {
    // get the choice of Command from the user
    NtmCommonSubMenuFeedKeys(NtmOptionMapKeysSubMenu, &m_OptionMapKeys);

    if(m_OptionMapKeys.pickedSubItem == NEDMENU_PICK_NONE)
      break;

    else
    {
      szCmdName = m_OptionMapKeys.ppStrings[m_OptionMapKeys.pickedSubItem];
      NedMsgPrintf("Map %s to which key: ", szCmdName);
      c = NedKeyGetKey();
      if(NedKeyMapSetCommand(NedKeyName(c), szCmdName))
        NedMsgPrintf("Mapped %s to %s", szCmdName, NedKeyName(c));
      else
        NedMsgPrintf("Couldn't map key");
    }
  }
  *(bool_t *)m_OptionMapKeys.pData = FALSE;
}

// key/cmd pairs, helper structure
typedef struct
{
  const char  *szKey;
  const char  *szCmd;
} keyCmd_t; 

/*!------------------------------------------------------------------------------------------------
  Copy a string into an allocated copy. Helper to NedCmdSettingsBindings.
-------------------------------------------------------------------------------------------------*/
keyCmd_t * NcsBindingsAlloc(unsigned *pMax)
{
  keyCmd_t * pKeyCmds = malloc(sizeof(keyCmd_t) * NEDKEY_NONE);
  if(pKeyCmds)
  {
    memset(pKeyCmds, 0, sizeof(keyCmd_t) * NEDKEY_NONE);
    *pMax = NEDKEY_NONE;
  }
  else
  {
    *pMax = 0;
  }
  return pKeyCmds;
}

/*!------------------------------------------------------------------------------------------------
  Get memory for the keyname, as NedKeyName() builds the name on the fly (not persistent)
-------------------------------------------------------------------------------------------------*/
char * NcsBindingsCopyKey(const char *szKey)
{
  unsigned   len = strlen(szKey);
  char      *psz;

  psz = malloc(len + 1);
  if(psz)
  {
    memcpy(psz, szKey, len);
    psz[len] = '\0';
  }

  return psz;
}

/*!------------------------------------------------------------------------------------------------
  Free the memory allocated for this process
-------------------------------------------------------------------------------------------------*/
void NcsBindingsFree(keyCmd_t *pKeyCmds, unsigned numPairs)
{
  unsigned  i;

  for(i=0; i<numPairs; ++i)
  {
    if(pKeyCmds[i].szKey)
      free((void *)pKeyCmds[i].szKey);
  }
  memset(pKeyCmds, 0, numPairs * sizeof(keyCmd_t));
  free(pKeyCmds);
}

/*!------------------------------------------------------------------------------------------------
  Show key bindings. Makes a read-only markdown file in the project folder called "nedkeymap.md"

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdViewKeyBindings(void)
{
  #define       COLS  3
  sNedFile_t   *pFile;
  char          szLine[PATH_MAX + 1];
  char          szSepLine[80];
  keyCmd_t     *pKeyCmds;
  unsigned      cmdWidth[COLS];
  unsigned      keyWidth[COLS];
  unsigned      maxKeys = 0;
  unsigned      numKeys;
  unsigned      len;
  unsigned      i;
  pfnNedCmd_t   pfnCmd;
  nedKey_t      key;
  const char    szSep[] = " | ";
  const char    szKey[] = "Key";
  const char    szCmd[] = "Command";

  // create the file in same folder as the szProjName. Make it empty.
  strcpy(szLine, NedStrDirOnly(NedEditorProjPath()));
  NedStrAppendName(szLine, "nedkeymap.md");
  pFile = NedEditorFileIsOpen(szLine);
  if(pFile)
    NedFileClear(pFile);
  else
    pFile = NedEditorFileNew(szLine, 0);
  NedAssert(pFile);
  pFile->fRdOnly = TRUE;

  // allocate space for key/command pairs
  pKeyCmds = NcsBindingsAlloc(&maxKeys);
  NedAssert(pKeyCmds);

  // create a separator line to use in making table
  memset(szSepLine, '-', sizeof(szSepLine));
  szSepLine[sizeof(szSepLine) - 1] = '\0';

  // calculate column width for all key/command pairs
  for(i = 0; i < COLS; ++i)
  {
    keyWidth[i] = strlen(szKey);
    cmdWidth[i] = strlen(szCmd);
  }

  // build key/command pairs
  numKeys = 0;
  for(i = 0; i < maxKeys; ++i)
  {
    key = (nedKey_t)i;
    pfnCmd = NedKeyMapGetCommand(key);
    if(pfnCmd && pfnCmd != NedCmdDoNothing && pfnCmd != NedCmdEditInsertSelf)
    {
      // keynames must be copied as they are built on the fly by NedKeyName()
      pKeyCmds[numKeys].szKey = NcsBindingsCopyKey(NedKeyName(key));
      pKeyCmds[numKeys].szCmd = NedCmdGetName(pfnCmd);

      // calculate maximm column width for key/commmands
      len = strlen(pKeyCmds[numKeys].szKey);
      if(len > keyWidth[numKeys % COLS])
        keyWidth[numKeys % COLS] = len;
      len = strlen(pKeyCmds[numKeys].szCmd);
      if(len > cmdWidth[numKeys % COLS])
        cmdWidth[numKeys % COLS] = len;

      ++numKeys;
    }
  }

  // print markdown header
  // : Key     | Command 
  // :------ | :--------
  for(i = 0; i < COLS; ++i)
  {
    sprintf(szLine, "%-*s%s%-*s%s",
            keyWidth[i % COLS], "Key", szSep,
            cmdWidth[i % COLS], "Command", (i == COLS - 1) ? "" : szSep);
    NedPointInsStr(&pFile->sCursor, szLine);
  }
  NedPointInsStr(&pFile->sCursor, "\n");
  for(i=0; i < COLS; ++i)
  {
    sprintf(szLine, ":%.*s%s:%.*s%s",
          keyWidth[i % COLS] - 1, szSepLine, szSep,
          cmdWidth[i % COLS] - 1, szSepLine, (i == COLS - 1) ? "" : szSep);
    NedPointInsStr(&pFile->sCursor, szLine);
  }
  NedPointInsStr(&pFile->sCursor, "\n");

  // print column data 
  for(i=0; i<numKeys; ++i)
  {
    sprintf(szLine, "%-*s%s%-*s%s",
            keyWidth[i % COLS], pKeyCmds[i].szKey, szSep,
            cmdWidth[i % COLS], pKeyCmds[i].szCmd, (((i + 1) % COLS) == 0) ? "" : szSep);
    NedPointInsStr(&pFile->sCursor, szLine);

    if(((i+1) % COLS) == 0)
      NedPointInsStr(&pFile->sCursor, "\n");
  }
  if((i % COLS) != 0)
      NedPointInsStr(&pFile->sCursor, "\n");

  // free the memory
  NcsBindingsFree(pKeyCmds, numKeys);

  sprintf(szLine, "\n%u Mapped Keys\n", numKeys);
  NedPointInsStr(&pFile->sCursor, szLine);
  NedPointFileTop(&pFile->sCursor);
  NedEditorGotoPoint(&pFile->sCursor);
}

/*!------------------------------------------------------------------------------------------------
  Toggle line endings between "/n" and "/r/n". Only takes effect for this one file at save

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdSettingsCrLf(void)
{
  sNedFile_t       *pFile = NedEditorFileCur();
  sNedLanguage_t   *pLang;
  bool_t            fCrLf;
  nedKey_t          c;

  if(pFile->fRdOnly)
  {
    NedMsgPrintf(szRdOnly);
    return;
  }
  fCrLf  = pFile->fCrLf ? FALSE : TRUE;

  pLang = (sNedLanguage_t *)NedLangGetNoDef(NedFilePath(pFile));
  if(pLang)
  {
    c = NedMsgChoice("Set for all files of this type? ", NULL, "ny");
    if(c == NEDKEY_ESC)
      return;
    if(c == 'y')
      pLang->fCrLf = fCrLf;
  }

  pFile->fDirty = TRUE;
  pFile->fCrLf  = fCrLf;

  NedMsgPrintf("Line endings will be %s on next save", pFile->fCrLf ? "\"\\r\\n\"" : "\"\\n\"");
  NedFileLogShow(pFile, LOG_MIN);
}

/*!------------------------------------------------------------------------------------------------
  Set the wrap length for text for this language (e.g. 79 for Python, 120 for C)

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdSettingsWrap(void)
{
  sNedLanguage_t   *pLang = (sNedLanguage_t *)NedLangGet(NedFilePath(NedEditorFileCur()));
  char              szWrapLen[32];
  unsigned          wrapLen = pLang->wrapLen;
  long              ret;

  snprintf(szWrapLen, sizeof(szWrapLen)-1, "%u", wrapLen);
  ret = NedMsgLineEdit(szWrapLen, sizeof(szWrapLen)-1, "Wrap Length for all files of this .ext (8-1024): ", NULL, NULL);
  if(ret <= 0)
    return;
  wrapLen = atoi(szWrapLen);
  if(wrapLen < 8)
    wrapLen = 8;
  if(wrapLen > 1024)
    wrapLen = 1024;
  pLang->wrapLen    = wrapLen;
}

/*!------------------------------------------------------------------------------------------------
  Allow user to set tab size for current file. Also allow modifying language settings for this file
  type (if found).

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdSettingsTab(void)
{
  sNedFile_t       *pFile = NedEditorFileCur();
  sNedLanguage_t   *pLang;
  char              szTabSize[8];
  unsigned          tabSize = pFile->tabSize;
  bool_t            fHardTabs;
  long              ret;
  nedKey_t          c;

  snprintf(szTabSize, sizeof(szTabSize)-1, "%u", tabSize);
  ret = NedMsgLineEdit(szTabSize, sizeof(szTabSize)-1, "Tab Size (2-8): ", NULL, NULL);
  if(ret <= 0)
    return;
  tabSize = atoi(szTabSize);
  if(tabSize <= 1)
    tabSize = 2;
  if(tabSize > NEDCFG_MAX_INDENT)
    tabSize = NEDCFG_MAX_INDENT;

  c = NedMsgChoice("Hard Tabs? ", NULL, "ny");
  if(c == NEDKEY_ESC)
    return;
  fHardTabs = (c == 'y') ? TRUE : FALSE;

  pLang = (sNedLanguage_t *)NedLangGetNoDef(NedFilePath(pFile));
  if(pLang)
  {
    c = NedMsgChoice("Set for all files of this type? ", NULL, "ny");
    if(c == NEDKEY_ESC)
      return;
    if(c == 'y')
    {
      pLang->tabSize    = tabSize;
      pLang->fHardTabs  = fHardTabs;
    }
  }

  pFile->tabSize    = tabSize;
  pFile->fHardTabs  = fHardTabs;

  NedStatusBarSetTab(pFile->fHardTabs, pFile->tabSize, pFile->fCrLf);
  NedEditorBuildAroundCursor();
}

/*!------------------------------------------------------------------------------------------------
  Pick a color theme. Uses name

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdSettingsTheme(void)
{
  static const char szPrompt[]  = "Set Theme: ";
  nedKey_t      key;
  unsigned      i;
  char          szPrompt2[NEDCFG_MAX_WIDTH];
  char          szChoices[NEDCFG_THEME_MAX + 1];
  const char   *psz;

  // Discover themes: "Classic, Dark, Natural, Monochrome, 1980s, NoColor"
  i = 0;
  memset(szPrompt2, 0, sizeof(szPrompt2));
  memset(szChoices, 0, sizeof(szChoices));
  for(i = 0; i < NEDCFG_THEME_MAX; ++i)
  {
    // determine name of next theme
    psz = NedThemeGetName(i);
    if(psz == NULL)
      break;

    // create the middle prompt
    szChoices[i] = tolower(*psz);
    if(i > 0)
      strcat(szPrompt2, ", ");
    strcat(szPrompt2, psz);
  }

  key = NedMsgChoice(szPrompt, szPrompt2, szChoices);
  psz = strchr(szChoices, (char)key);
  if(psz)
  {
    NedEditorSetTheme((unsigned)(psz - szChoices));
    NedMsgPrintf("Theme %s", NedThemeGetName(NedEditor()->themeIndex));
    NedCmdRedrawScreen();
  }
}

