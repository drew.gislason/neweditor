/*!************************************************************************************************
  @file NedKeyMap.h

  @brief  Default Key and Menu Map API.

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_keymap Ned Key Map - An API that maps keys and menus to commands.

  The keymap provides mapping between keys, menus and commands. It has a factory default, but can
  be overridden by the user with menu `Option/Map Keys...`

  For examplek, the user could remap `Ctrl-A`, which normally is command `SelectAll`, to the
  command `CursorHome`, like it would be in the bash shell.

  Command names can be found in C code by prepending `NedCmd`. For example command `MoveMatchBrace`
  is the function `NedCmdMoveMatchBrace()`.

  This module also defines all the command to menu item mapping, as well as menu hotkey mapping. It
  understands the expandable window menu.

  Key contexts are used for mappings other than the editor. For example, when getting strings from
  the MessageBar.

*///***********************************************************************************************
#ifndef NED_KEYMAP_H
#define NED_KEYMAP_H

#include "NedKey.h"
#include "NedCmd.h"
#include "NedMenu.h"

typedef struct
{
  nedKey_t        key;      // key id
  pfnNedCmd_t     pfnCmd;   // pointer to command function
} sNedKeyMap_t;

typedef enum
{
  NEDMENUINDEX_FILE   = 0,
  NEDMENUINDEX_EDIT,
  NEDMENUINDEX_SEARCH,
  NEDMENUINDEX_GOTO,
  NEDMENUINDEX_OPTION,
  NEDMENUINDEX_WINDOW,
  NEDMENUINDEX_HELP,
} sNedMenuIndex_t;

typedef struct
{
  unsigned          nMenu;      // which menu, e.g. File or Options
  unsigned          nItem;      // item in menu, e.g. File/Open or File/OpenRecent
  pfnSubMenu_t      pfnSubMenu;
} sNedKeyMapSubMenu_t;

// SubMenu index must match actual index in NedKeyMap.c, for all Commmands-> that bring up a list box
#define NEDFILE_INDEX_FILE_OPEN         1
#define NEDFILE_INDEX_FILE_OPEN_RECENT  2
#define NEDOPTION_INDEX_MAP_KEYS        3
#define NEDOPTION_PROJECT_FOLDERS       9

void            NedKeyMapFactory          (void);
nedKey_t        NedKeyMapGetKeyFromCmd    (pfnNedCmd_t pfnCmd);
nedKey_t        NedKeyMapTranslateKey     (nedKey_t key);
pfnNedCmd_t     NedKeyMapGetCommand       (nedKey_t key);
bool_t          NedKeyMapSetCommand       (const char *szKeyName, const char *szCmdName);
pfnNedCmd_t     NedKeyMapGetMapped        (unsigned i, nedKey_t *pKey);

void *          NedMenuMapInit            (nedThemeIndex_t themeIndex);
pfnNedCmd_t     NedMenuMapGetCommand      (unsigned menu, unsigned item);
void            NedMenuMapSetWindowItems  (unsigned nItems, const char **ppFilenames);
void            NedMenuMapSetSubMenus     (void *hMenu, unsigned nItems, sNedKeyMapSubMenu_t *aSubMenus);
void            NedMenuMapAdjustMenuKeys  (void);

#endif  // NED_KEYMAP_H
