/*!************************************************************************************************
  @file NedController.c

  @brief  Ned Controller Implementation

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_editor Ned Editor - An API wrapping all subsystems together for an editor.

  Connects the editor model to the view.

*///***********************************************************************************************
#include "Ned.h"
#include "NedLog.h"
#include "NedKey.h"
#include "NedBuffer.h"
#include "NedFile.h"
#include "NedKeyMap.h"
#include "NedMenu.h"
#include "NedPoint.h"
#include "NedCmd.h"
#include "NedSettings.h"
#include "NedEditor.h"
#include "NedUtilStr.h"
#include "NedSignal.h"
#include "NedController.h"


static void   ControllerIdle        (void);

const static char   m_szHelp[]    = \
  "ned [-e errfile] [-p project_file] [-l[logfile][,opts]] [file list]\n"
  "\n"
  "Examples:\n"
  "  $ ned myfile.c\n"
  "  $ ned\n"
  "  $ ned *.c *.h *.py \"~/Documents/My Files/Temp/t*.txt\"\n"
  "  $ ned -p .\n"
  "  $ ned -p /path/to/project/\n"
  "  $ gcc -c -Wall myfile.c 2>err.txt\n"
  "  $ ned -e /path/err.txt\n"
  "  $ ned -l\n"
  "  $ ned *.rs -l,sfp\n"
  "  $ ned --version\n"
  "  $ ned --help\n";

#if NEDDBG_LEVEL > 0
static const char m_szNedBanner[] =
{
  "\n\n"
  "=======  |\\   |  .----  +---.  =======\n"
  "_______  | \\  |  |__    |   |  _______\n"
  "         |  \\ |  |      |   |         \n"
  "=======  |   \\|  .____  |___/  =======\n"
  "\n\n"
};
#endif

typedef struct
{
  char      c;
  uint32_t  mask;
} logOpts_t;

static const logOpts_t m_aLogOpts[] =
{
  { 'b', NEDLOG_BUF },
  { 'c', NEDLOG_CMD },
  { 'd', NEDLOG_CMD_EDIT },
  { 'e', NEDLOG_EDITOR },
  { 'f', NEDLOG_FILE },
  { 'j', NEDLOG_JSON },
  { 'k', NEDLOG_KEY },
  { 'l', NEDLOG_LIST },
  { 'm', NEDLOG_MENU },
  { 'g', NEDLOG_MSG },
  { 'o', NEDLOG_CMD_OPTS },
  { 'p', NEDLOG_POINT },
  { 's', NEDLOG_SETTINGS },
  { 'u', NEDLOG_UNDO },
  { 't', NEDLOG_UTILS },
  { 'v', NEDLOG_VIEW },
  { 'w', NEDLOG_WIN },
  { 'x', NEDLOG_ALL },
  { '-', NEDLOG_NONE }
};

/*!------------------------------------------------------------------------------------------------
  Get the log bitmask from single characters. See NedCfg.h and m_aLogOpts above.

  For example, ",bfp" becomes NEDLOG_BUF | NEDLOG_FILE | NEDLOG_POINT 

  Returns NEDLOG_DEFAULT if this is not a comma string
-------------------------------------------------------------------------------------------------*/
static uint32_t NcGetLogMask(const char *pszComma)
{
  nedLogMask_t    logMask = 0;
  unsigned        i;

  if(pszComma && *pszComma == ',')
  {
    ++pszComma;
    while(*pszComma)
    {
      for(i=0; i<NumElements(m_aLogOpts); ++i)
      {
        if(m_aLogOpts[i].c == *pszComma)
          logMask |= m_aLogOpts[i].mask;
      }
      ++pszComma;
    }
  }
  else
    logMask = NEDLOG_DEFAULT;
  return logMask;
}

/*!------------------------------------------------------------------------------------------------
  Open the log file. Also, set up Log Mask. By default, the log file will be in the same folder as
  the project file.

  Examples (note: szLogArg is after the -l). NULL means no -l parameter:

  NULL


  Examples: NULL, "-l", "-lfile.log", "-l/path/file,opts", "-l,opts"
  Example: "-l,bfp" to logMask of NEDLOG_BUF | NEDLOG_FILE | NEDLOG_POINT

  szLogArg    NULL if no "-l", or just after the "-l": "", "file.log", "/path/file,opts", ",opts"
  szProjPath  Full path to  project file, used as default folder for log
  pLogMask    [out] pointer to log mask to be filled in with ",opts"

  Returns TRUE if path OK (or no path). Returns FALSE if bad bath.
-------------------------------------------------------------------------------------------------*/
static bool_t NcLogOpen(nedLogMask_t *pLogMask, const char *szLogArg, const char *szProjPath)
{

  bool_t          fPathOk     = TRUE;
  nedLogMask_t    logMask     = 0;    // no logging
  char           *szLogPath   = (char *)NedEditorLogPath();
  const char     *pszComma;
  const char     *szDir;
  size_t          len         = 0;
  sNedFileInfo_t  sInfo;

  // printf("NcLogOpen(logArg %s, projPath %s)\n", szLogArg ? szLogArg : "(null)", szProjPath ? szProjPath : "(null)"); 

  // no logging if szLogArg is NULL
  *szLogPath = '\0';
  if(szLogArg != NULL)
  {
    // determine log path
    pszComma = strchr(szLogArg, ',');
    if(pszComma == NULL)
      len = strlen(szLogArg);
    else
      len = (size_t)(pszComma - szLogArg);

    // default to path of project
    if(len == 0)
    {
      strcpy(szLogPath, NedStrDirOnly(szProjPath));
      if(!NedStrAppendName(szLogPath, szNedLogDefaultName))
        fPathOk = FALSE;
    }
    else
    {
      if(len > PATH_MAX - 1)
        len = PATH_MAX - 1;
      strncpy(szLogPath, szLogArg, len);
      szLogPath[len] = '\0';
      // printf("szLogPath %s\n", szLogPath);

      // if file doesn't exist, still OK if dir exists
      if(!NedFileInfoGet(&sInfo, szLogPath))
      {
        szDir = NedStrDirOnlyHome(szLogPath);
        if(*szDir == '\0')
          szDir = ".";
        if(!NedFileInfoGet(&sInfo, szDir) || !sInfo.fIsDir)
          fPathOk = FALSE;
        else
        {
          if(!NedStrAppendName(sInfo.szFullPath, NedStrNameOnly(szLogPath)))
            fPathOk = FALSE;
          else
            strcpy(szLogPath, sInfo.szFullPath);
        }
      }
      else
      {
        // printf("sInfo.szFullPath %s\n", sInfo.szFullPath);
        if(sInfo.fIsDir)
        {
          if(!NedStrAppendName(sInfo.szFullPath, szNedLogDefaultName))
            fPathOk = FALSE;
        }
        strcpy(szLogPath, sInfo.szFullPath);
      }
    }

    // if path OK, create/open the log file
    // otherwise, just use default log filename in current directory in case of NedLogPrintf() statements
    if(fPathOk)
      fPathOk = NedLogFileOpen(szLogPath);
    else
      strcpy(szLogPath, szNedLogDefaultName);

    // "-l,opts" will set log mask to user options, "-l" will be NEDLOG_DEFAULT
    if(fPathOk)
    {
      if(pszComma)
        logMask = NcGetLogMask(pszComma);
      else
        logMask = NEDLOG_DEFAULT;
    }
  }

  // logMask will be 0 if fPathOK is FALSE, otherwise, set to default or user selected
  *pLogMask = logMask;

  // printf("worked %u, szLogPath %s, logMask %x\n", fWorked, szLogPath, logMask);
  // exit(1);

  return fPathOk;
}

/*!------------------------------------------------------------------------------------------------
  Starts up the editor, including initializing windowing and all other subsystems, and restoring
  state of opened files.

  Initializes everything it can before the windowing system so that it can just use printf() and
  return.

  @ingroup  ned_edit
  @param    argc      number of strings
  @param    argv      array of strings

  @return   TRUE if successful, prints error message and returns FALSE if not.
*///-----------------------------------------------------------------------------------------------
bool_t NedControllerStartup(int argc, const char **argv)
{
  sNedEditor_t   *pEditor       = NedEditor();
  const char     *szProjName    = NULL;   // -p path
  const char     *szErrFile     = NULL;   // -e file
  const char     *szLogArg      = NULL;   // -lfile,opts
  nedLogMask_t    logMask       = 0;
  bool_t          fLoadFiles    = TRUE;
  bool_t          fSaveProj     = FALSE;
  int             i;
  unsigned        rows;
  unsigned        cols;
  nedErr_t        err;

  // set up signal support
  memset(pEditor, 0, sizeof(*pEditor));
  NedAssertSetExit(NedEditorError);
  NedSigSetExit(argv[0], NedEditorSignal);

  // parse cmdline options
  for(i=1; i<argc; ++i)
  {
    if(strcmp(argv[1], "--help") == 0)
    {
      printf("\n");
      printf(szNedCmd_HelpAbout, szNedMachineName, NED_OS, __STDC_VERSION__, gszNedSha);
      printf("\n\n");
      printf(m_szHelp);
      return FALSE;
    }

    else if(strcmp(argv[1], "--version") == 0)
    {
      // see also NedCmdHelpAbout()
      printf("\n");
      printf(szNedCmd_HelpAbout, szNedMachineName, NED_OS, __STDC_VERSION__, gszNedSha);
      printf("\n\n");
      return FALSE;
    }

    // log file,opts MUST fit in a SINGLE cmdline option
    // Examples: "-l",  "-lfile", "-l/path/file,opts" or "-l,opts".
    // Example: "-l,bfp" to log buffer, file, point to ned.log)
    else if(strncmp(argv[i], "-l", 2) == 0)
    {
      szLogArg = &argv[i][2];
    }

    // user wants to create/load project from a particular folder
    // Examples: "-p .", "-p ~/myproj.json"
    else if(strcmp(argv[i], "-p") == 0)
    {
      ++i;
      if(i < argc)
      {
        szProjName = argv[i];
        fSaveProj = TRUE;
      }
    }

    // user wants to load error file
    else if(strcmp(argv[i], "-e") == 0)
    {
      ++i;
      if(i < argc)
        szErrFile = argv[i];
    }

    // don't load files from project as user specified one or more file(s) to load instead
    else
      fLoadFiles = FALSE;
  }

  // find the project full path, e.g. "/Users/drewg/ned.json"
  // only save the project if found. Otherwise, don't litter folder with project file
  err = NedEditorFindProjectFile((char *)NedEditorProjPath(), szProjName);
  if(err == NEDERR_BADPATH)
  {
    printf("Bad path to project file %s\n", szProjName);
    return FALSE;
  }
  else if(err == NEDERR_NONE)
     fSaveProj = TRUE;
  err = NEDERR_NONE;

  // open log file if requested
  if(!NcLogOpen(&logMask, szLogArg, NedEditorProjPath()))
  {
    printf("Bad path to log file %s\n", szLogArg);
    return FALSE;
  }
  NedLogMaskSet(logMask);


  // show startup banner for logging
  if(NedLogMaskGet())
  {
    NedLogPrintf("%sLog Date: %s, logfile %s, mask: %u\n", m_szNedBanner, NedStrDateTimeCur(), NedEditorLogPath(), logMask);
    NedLogPrintf(szNedCmd_HelpAbout, szNedMachineName, NED_OS, __STDC_VERSION__, gszNedSha);
    NedLogPrintf("\nNedControllerStartup(argc %i, %s", argc, argv[0]);
    for(i=1; i<argc; ++i)
      NedLogPrintf(", %s", argv[i]);
    NedLogPrintf(")\n");
  }

  // initialize the subsystems (except windowing subsystem)
  NedKeyMapFactory();
  NedThemeInit();
  NedLangInit();

  // initialize the editor structure (except szProjPath, which was already filled in)
  NedEditorInit(pEditor);
  pEditor->fSaveProj = fSaveProj;

  // load project (for current theme, adjusted key bindings, file list, etc...)
  err = NedEditorLoadProject(NedEditorProjPath(), fLoadFiles);
  if(err == NEDERR_BADPARM)
  {
    printf("Older or corrupt project file %s. Remove or specify other project path\n", NedEditorProjPath());
    return FALSE;
  }
  else if(err)
  {
    printf("Unknown error with project %s\n", NedEditorProjPath());
    return FALSE;
  }

  // handle error file handle error file
  if(szErrFile)
  {
    pEditor->pFileErr = NedEditorFileNew(szErrFile, 0);
    if(!pEditor->pFileErr)
    {
      printf("Cannot open error file %s\n", szErrFile);
      return FALSE;
    }
  }

  // load file(s) specified on command line)
  for(i=1; i<argc; ++i)
  {
    // skip arguments, e.g. -e errfile -p projname.json -l filename.txt 
    if(strncmp(argv[i], "-l", 2) == 0)
      continue;
    if(argv[i][0] == '-')
      ++i;
    else
      NedEditorFileNew(argv[i], 0);   // add the file to the filelist
  }

  // no files loaded due to permissions or bad paths, so load a temporary file
  if(pEditor->pFileList == NULL)
  {
     pEditor->pFileList = NedFileNew(NULL, NULL);
     NedEditorSetFileCur(pEditor->pFileList);
  }

  // initialize view
  AnsiGetRowsCols(&rows, &cols);
  if((rows < NEDMIN_ROWS) || (cols < NEDMIN_COLS))
  {
    printf("Can't run Ned, screen too small!\nNeed %u rows x %u cols\n", NEDMIN_ROWS, NEDMIN_COLS);
    NedEditorClear(pEditor);
    return FALSE;
  }
  NedViewInit();
  pEditor->hView = NedViewNew(NedThemeGet(pEditor->themeIndex));

  // set up menu
  NedEditorSetWindowItems();
  pEditor->hMenu = NedMenuMapInit(pEditor->themeIndex);
  NedEditorSetSubMenus();

  // set up theme for View, Menu, Msg
  NedEditorSetTheme(pEditor->themeIndex);

  // setup keyboard idle for things like changing screen size, and files changed outside editor
  NedKeySetIdle(ControllerIdle);

  // draw everything screen (including cls)
  NedEditorRedrawScreen();
  if(NedLogMaskGet())
    NedMsgPrintf("Logging to %s", NedEditorLogPath());
  else
    NedMsgPrintf("Press Alt-Q to quit, or Alt-F to enter File Menu");
  NedEditorUpdateSmart();

  if(NedLogMaskGet() & NEDLOG_EDITOR)
    NedEditorLogShow(NedEditor(), 2);

  return TRUE;
}


/*!------------------------------------------------------------------------------------------------
  Shuts down the editor.

  @ingroup  ned_edit

  @return   TRUE if successful
*///-----------------------------------------------------------------------------------------------
void NedControllerShutdown(void)
{
  NedLogPrintfEx(NEDLOG_EDITOR, "Shutting down...\n");
  if(NedLogMaskGet() & NEDLOG_EDITOR)
    NedEditorLogShow(NedEditor(), 2);
  NedViewShutdown();
}


/*!------------------------------------------------------------------------------------------------
  Get the next key (may be from a macro). Macros work everwhere Ned gets a key, including in menus
  message line, etc...

  @ingroup  ned_edit

  @return   TRUE if successful
*///-----------------------------------------------------------------------------------------------
nedKey_t NedControllerGetKey(void)
{
  NedEditor()->c = NedKeyMapTranslateKey(NedKeyGetKey());
  return NedEditor()->c;
}


/*!------------------------------------------------------------------------------------------------
  Feed the key into the editor.

  @ingroup  ned_edit
  @param    key       The key to act upon

  @return   TRUE if successful
*///-----------------------------------------------------------------------------------------------
void NedControllerFeedKey(nedKey_t key)
{
  sMenuPicked_t     sPicked;
  pfnNedCmd_t       pfnCmd = NULL;

  // if in menu subsystem, menu absorbs the key
  NedEditor()->fInMenu = FALSE;
  if(NedMenuInMenu(NedEditor()->hMenu))
  {
    NedEditor()->fInMenu = TRUE;
    NedLogPrintfEx(NEDLOG_MENU, "  feed menu key %s\n", NedKeyName(key));
    if(NedMenuFeedKey(NedEditor()->hMenu, key, &sPicked))
    {
      NedLogPrintfEx(NEDLOG_MENU, "picked=%u,menu=%u,item=%u\n", sPicked.fPicked, sPicked.pickedMenu,
                                                  sPicked.pickedItem);
      if(sPicked.fPicked)
      {
        // essentially NedCmdMenuWindow()
        if(sPicked.pickedMenu == NEDMENUINDEX_WINDOW)
        {
          NedEditorFilePick(sPicked.pickedItem);
        }
        else
        {
          pfnCmd = NedMenuMapGetCommand(sPicked.pickedMenu, sPicked.pickedItem);
          NedLogPrintfEx(NEDLOG_MENU, "Picked (%s) menu=%u,item=%u, cmd=(%s)\n",
            NedMenuItemName(NedEditor()->hMenu, sPicked.pickedMenu, sPicked.pickedItem),
            sPicked.pickedMenu, sPicked.pickedItem, NedCmdGetName(pfnCmd));
        }
      }
      else
      {
        NedLogPrintfEx(NEDLOG_MENU, "picked nothing...\n");
      }
      NedEditor()->fInMenu = FALSE;
      NedEditorFlushCursor();
    }
  }

  // not in editor, get command from mapped key
  else
  {
    pfnCmd = NedKeyMapGetCommand(key);
    if((pfnCmd == NULL) || (pfnCmd == NedCmdDoNothing))
      NedMsgPrintf("key %s not mapped", NedKeyName(key));
  }

  // command could have come from menus or key in editor
  if((pfnCmd != NULL) && (pfnCmd != NedCmdDoNothing))
  {
    NedEditorPreCmdCheck(pfnCmd);
    (*pfnCmd)();
    NedEditorPostCmdCheck(pfnCmd);
  }
}

/*-------------------------------------------------------------------------------------------------
  When keyboard is idle, check for various tasks
-------------------------------------------------------------------------------------------------*/
static void ControllerIdle(void)
{
  if(NedViewCheckScreenSize(NedEditorView()))
    NedEditorRedrawScreen();
}
