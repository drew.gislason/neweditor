/*!************************************************************************************************
  @file NedMem.c

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @brief NedMem is a memory subsystem, usually on top of free(), malloc(), that detects buffer
  overruns, memory leaks, use after free, use before init, etc...()

  if NEDDBG_LEVEL == 0 or is not defined, then 

  @defgroup  ned_utils

*///***********************************************************************************************
#include <stdio.h>
#include <stdlib.h>
#include "NedMem.h"

#ifndef NEDDBG_LEVEL
 #define NEDDBG_LEVEL 0
#endif

#define SANCHK_MEM  30303

typedef struct
{
  unsigned    sanchk;
  unsigned    count;    // allocation count
} sNedMemFooter_t;

// reflects user byte allocation, not underlying memory allocate
static nedMemSize_t   m_TotalBytesAllocated;

/*-------------------------------------------------------------------------------------------------
  May increase size, so footer can remain on alignment.
  Example: size of 9 bytes with alignment 8 will be 16.
  Returns alignment size of size
-------------------------------------------------------------------------------------------------*/
static nedMemSize_t NmAlignSize(nedMemSize_t size)
{
  if((size % NEDMEM_ALIGN) != 0)
    size += NEDMEM_ALIGN - (size % NEDMEM_ALIGN);
  return size;  
}

/*-------------------------------------------------------------------------------------------------
  Get pointer to footer, given pointer to header [header][userMem][footer]

  Header must be filled in.
  p MUST point to header

  Returns pointer to footer, or NULL if p is NULL.
-------------------------------------------------------------------------------------------------*/
static sNedMemFooter_t * NmGetFooterFromHeader(const nedMemSize_t *pHeader)
{
  sNedMemFooter_t  *pFooter = NULL;
  uint8_t          *p;
  if(pHeader && *pHeader > 0)
  {
    p = (uint8_t *)pHeader;
    pFooter = (sNedMemFooter_t *)(p + sizeof(*pHeader) + NmAlignSize(*pHeader));
  }
  return pFooter;
}

/*-------------------------------------------------------------------------------------------------
  Get pointer to footer, given pointer to userMem [header][userMem][footer]

  Header must be filled in.
  p MUST point to userMem

  Returns pointer to footer, or NULL if p is NULL.
-------------------------------------------------------------------------------------------------*/
static sNedMemFooter_t * NmGetFooterFromUserMem(const void *p)
{
  const nedMemSize_t   *pHeader = p;
  if(p)
  {
    --pHeader;
    p = NmGetFooterFromHeader(pHeader);
  }
  return (sNedMemFooter_t *)p;
}

/*-------------------------------------------------------------------------------------------------
  Allocate memory for Alloc() and Calloc(). [header][userMem][footer]

  Returns pointer to userMem
-------------------------------------------------------------------------------------------------*/
static void * NmAlloc(nedMemSize_t size)
{
  void             *p;
#if NEDDBG_LEVEL > 0
  nedMemSize_t     *pHeader;
  sNedMemFooter_t  *pFooter;

  // footer must begin on alignment boundary, so increase userMem size accordingly
  NedAssert(size);
  p = NEDMEM_MALLOC(sizeof(*pHeader) + NmAlignSize(size) + sizeof(*pFooter));

  // set up header and footer
  if(p)
  {
    pHeader   = (nedMemSize_t *)(p);
    *pHeader  = size;
    pFooter   = NmGetFooterFromHeader(pHeader);
    pFooter->sanchk = SANCHK_MEM;
    pFooter->count  = 1;

    // point p to user memory (not header)
    p = (void *)(pHeader + 1);  
  }
#else

  p = NEDMEM_MALLOC(size);
#endif

  if(p)
    m_TotalBytesAllocated += size;

  return p; // points to userMem (not pHeader)
}

/*!------------------------------------------------------------------------------------------------
  Same as malloc(), but with all the NedMem features

  @param    size
  @return   none
*///-----------------------------------------------------------------------------------------------
void * NedMemAlloc(nedMemSize_t size)
{
  void *p = NmAlloc(size);
  if(p)
    memset(p, NEDDBG_ALLOC, size);
  return p;
}

/*!------------------------------------------------------------------------------------------------
  same as calloc(), but with all the NedMem features.

  @ingroup  ned_utils
  @return   none
*///-----------------------------------------------------------------------------------------------
void * NedMemCalloc(nedMemSize_t size)
{
  void *p = NmAlloc(size);
  if(p)
    memset(p, 0, size);
  return p;
}

/*!------------------------------------------------------------------------------------------------
  Same as free(), but with all the NedMem features.

  @ingroup  ned_utils
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedMemFree(void *p)
{
#if NEDDBG_LEVEL
  nedMemSize_t *pHeader = p;

  if(NedMemIsMem(p))
  {
    --pHeader;
    m_TotalBytesAllocated -= *pHeader;
    *pHeader = 0;
    NEDMEM_FREE(pHeader);
  }
#else
  NEDMEM_FREE(p);
#endif
}

/*!------------------------------------------------------------------------------------------------
  Same as realloc(), but with all the NedMem features. Doesn't change allocation count.

  @ingroup  ned_utils
  @param    p       points to userMem
  @param    size    size to reallocate memory to (grow or shrink), or 0 to free
  @return   pointer to (potentially new) user memory
*///-----------------------------------------------------------------------------------------------
void * NedMemRealloc(void *p, nedMemSize_t size)
{
#if NEDDBG_LEVEL
  nedMemSize_t     *pHeader = p;
  sNedMemFooter_t  *pFooter;
  unsigned          count;

  if(NedMemIsMem(p))
  {
    // freeing memory
    if(size == 0)
    {
      NedMemFree(p);
    }
    // growing or shrinking memory
    else
    {
      // preserve allocation count
      --pHeader;
      pFooter = NmGetFooterFromHeader(pHeader);
      count   = pFooter->count;

      // growing or shrinking? Keep track of total memory allocated
      if(size > *pHeader)
        m_TotalBytesAllocated += (size - (*pHeader));
      else if(size < *pHeader)
        m_TotalBytesAllocated -= (*pHeader - size);

      p = NEDMEM_REALLOC((void *)pHeader, sizeof(*pHeader) + NmAlignSize(size) + sizeof(*pFooter));
      if(p)
      {
        pHeader   = p;
        *pHeader  = size;
        pFooter   = NmGetFooterFromHeader(pHeader);
        pFooter->sanchk = SANCHK_MEM;
        pFooter->count  = count;
        p = (void *)(pHeader + 1);
      }
    }
  }

#else
  p = NEDMEM_REALLOC(p, size);

#endif

  return p;
}

/*!------------------------------------------------------------------------------------------------
  Increments the count on the memory (if using pointer in two processes for example)

  @ingroup  ned_utils
  @param    p       points to userMem
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedMemCountInc(void *p)
{
  sNedMemFooter_t  *pFooter;

  if(NedMemIsMem(p))
  {
    pFooter = NmGetFooterFromUserMem(p);
    ++pFooter->count;
    NedAssert(pFooter->count < UINT_MAX);
  }
}

/*!------------------------------------------------------------------------------------------------
  Like free, but only actually frees if count goes to 0

  @ingroup  ned_utils
  @param    p       points to userMem
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedMemCountFree(void *p)
{
  sNedMemFooter_t  *pFooter;

  if(NedMemIsMem(p))
  {
    pFooter = NmGetFooterFromUserMem(p);
    if(pFooter->count == 1)
      NedMemFree(p);
    else
      --pFooter->count;
  }
}

/*!------------------------------------------------------------------------------------------------
  Like free, but only actually frees if count goes to 0

  @ingroup  ned_utils
  @param    p       points to userMem
  @return   none
*///-----------------------------------------------------------------------------------------------
unsigned NedMemCountGet(void *p)
{
  sNedMemFooter_t  *pFooter;
  unsigned          count = 0;

  if(NedMemIsMem(p))
  {
    pFooter = NmGetFooterFromUserMem(p);
    count = pFooter->count;
  }
  return count;
}

/*!------------------------------------------------------------------------------------------------
  If NEDDBG_LEVEL >= 1 then this reflects allocs, free and realloc.
  If NEDDBG_LEVEL == 0 then just allocs.

  @ingroup  ned_utils
  @return   total bytes allocated (and freed if NEDDBG_LEVEL >= 1)
*///-----------------------------------------------------------------------------------------------
nedMemSize_t NedMemAllocTotal(void)
{
  return m_TotalBytesAllocated;
}

/*!------------------------------------------------------------------------------------------------
  Reset total to 0. Perhaps using for subsystem statistics.

  @ingroup  ned_utils
  @return   total bytes allocated (and freed if NEDDBG_LEVEL >= 1)
*///-----------------------------------------------------------------------------------------------
void NedMemTotalReset(void)
{
  m_TotalBytesAllocated = 0;
}

/*!------------------------------------------------------------------------------------------------
  Is this valid memory pointer? Points to userMem in [header][userMem][footer]

  @ingroup  ned_utils
  @param    p       points to userMem
  @return   TRUE if valid memory pointer, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedMemIsMem(const void *p)
{
#if NEDDBG_LEVEL
  const nedMemSize_t     *pHeader = p;
  const sNedMemFooter_t  *pFooter;
#endif

  if(p == NULL)
    return FALSE;

#if NEDDBG_LEVEL
  --pHeader;
  pFooter = NmGetFooterFromHeader(pHeader);
  if(*pHeader == 0)
    return FALSE;
  if(pFooter->sanchk != SANCHK_MEM)
    return FALSE;
  if(pFooter->count == 0)
    return FALSE;
#endif

  return TRUE;
}
