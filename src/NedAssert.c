/*!************************************************************************************************
  @file NedAssert.c

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @brief NedAssert saves all files with .tmp extension so as to not lose any data.

  @defgroup  ned_tools   Ned Tools are a set of support functions for C programs.

*///***********************************************************************************************
#include <stdio.h>
#include <stdlib.h>
#include "NedAssert.h"

static pfnNedAssert_t m_pfnAssert = NULL;

/*!------------------------------------------------------------------------------------------------
  An Assert or Error has occurred. Display it to logfile if open and to screen and put keyboard
  back to normal and exit.

  @ingroup  ned_utils

  @param    rows   
  @param    cols   

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedAssertSetExit(pfnNedAssert_t pfnAssert)
{
  m_pfnAssert = pfnAssert;
}

/*!------------------------------------------------------------------------------------------------
  An Assert has occurred. Exit with error code.

  @ingroup  ned_utils

  @param    rows   
  @param    cols   

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedError(const char *szExpr, const char *szFile, const char *szFunc, unsigned line)
{
  static int  m_fAssert = 0;
  int         err       = 1;

  if(!m_fAssert)
  {
    m_fAssert = 1;
    if(m_pfnAssert)
      err = m_pfnAssert(szExpr, szFile, szFunc, line);
    else
      printf("NedAssert: (%s), file: %s, func: %s(), line: %u\n", szExpr, szFile, szFunc, line);
    exit(err);
  }
}
