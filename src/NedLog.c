/*!************************************************************************************************
  @file NedLog.c

  @brief  Ned Logging features

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include <stdarg.h>
#include <ctype.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include "NedLog.h"

const char szNedLogDefaultName[] = SZ_NED_LOG_NAME;

static FILE          *fpLog;
static nedLogMask_t   m_logMask;
static size_t         m_logSize;

/*!------------------------------------------------------------------------------------------------
  Create a new log file. This doesn't append, creates a new log.

  @ingroup  ned_log
  @param    szFilePath    standard printf format
  @return   length of printed string
*///-----------------------------------------------------------------------------------------------
bool_t NedLogFileOpen(const char *szFilePath)
{
  m_logSize = 0;
  NedLogFileClose();
  fpLog = fopen(szFilePath, "w");
  return fpLog ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Create a new log file. This doesn't append, creates a new log.

  @ingroup  ned_log
  @param    szFilePath    standard printf format
  @return   length of printed string
*///-----------------------------------------------------------------------------------------------
bool_t NedLogFileAppend(const char *szFilePath)
{
  fpLog = fopen(szFilePath, "a");
  return fpLog ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Close the log file if open

  @ingroup  ned_log
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedLogFileClose(void)
{
  if(fpLog)
    fclose(fpLog);
}

/*!------------------------------------------------------------------------------------------------
  print to the log file. Flushed every call.

  @ingroup  ned_log
  @param    szFormat    standard printf format
  @return   length of printed string
*///-----------------------------------------------------------------------------------------------
int NedLogPrintf(const char *szFormat, ...)
{
  va_list        arglist;
  int            len = 0;

  // open log file if not already open
  if(fpLog == NULL)
    fpLog = fopen(szNedLogDefaultName, "a");

  // print to logfile and flush
  if(fpLog)
  {
    va_start(arglist, szFormat);
    len = vfprintf(fpLog, szFormat, arglist);
    va_end(arglist);
    fflush(fpLog);
  }

  m_logSize += len;
  return len;
}

/*!------------------------------------------------------------------------------------------------
  print to the log file, but only if the mask bit is set. Flushed every call.

  @ingroup  ned_log
  @param    mask        mask for this printf
  @param    szFormat    standard printf format
  @return   length of printed string
*///-----------------------------------------------------------------------------------------------
int NedLogPrintfEx(nedLogMask_t mask, const char *szFormat, ...)
{
  va_list        arglist;
  int            len = 0;

  if(mask & m_logMask)
  {
    // open log file if not already open
    if(fpLog == NULL)
      fpLog = fopen("debug.log", "a");

    // print to logfile and flush
    if(fpLog)
    {
      va_start(arglist, szFormat);
      len = vfprintf(fpLog, szFormat, arglist);
      va_end(arglist);
      fflush(fpLog);
    }
  }

  m_logSize += len;
  return len;
}

/*!------------------------------------------------------------------------------------------------
  Dump hex data to the log file.

  41 41 41 41 41 41 41 41  42 42 42 42 42 42 42 42  |AAAAAAAABBBBBBBB|

  @ingroup  ned_log
  @param    pData     data to dump
  @param    len       length of data to dump
  @param    linlen    length of line
  @param    indent    indent
  @return   length of dump text
*///-----------------------------------------------------------------------------------------------
size_t NedLogDump(const void *pDataIn, unsigned len, unsigned linelen, unsigned indent)
{
  const uint8_t  *pData = pDataIn;
  unsigned        i;
  unsigned        offset;
  unsigned        thislen;
  unsigned        spaces;
  uint8_t         c;
  size_t          txtLen = 0;

  // default line length to 16
  if(linelen == 0)
    linelen = 16;

  offset = 0;
  while(offset < len)
  {
    if(indent)
      txtLen += NedLogPrintf("%*s",indent,"");
    thislen = linelen;
    if(len - offset < linelen)
      thislen = len - offset;
    for(i=0; i<thislen; ++i)
    {
      txtLen += NedLogPrintf("%02x ", pData[offset+i]);
    }
    spaces = 3 * (linelen - thislen);
    if(spaces)
      txtLen += NedLogPrintf("%*s",spaces,"");
    txtLen += NedLogPrintf(" |");
    for(i=0; i<thislen; ++i)
    {
      c = pData[offset+i];
      txtLen += NedLogPrintf("%c",isprint(c) ? c : '.');
    }
    spaces = (linelen - thislen);
    if(spaces)
      txtLen += NedLogPrintf("%*s",spaces,"");
    txtLen += NedLogPrintf("|\n");
    offset += thislen;
  }

  return txtLen;
}

/*!------------------------------------------------------------------------------------------------
  Dump hex data to the log file, but only if the mask matches.

  41 41 41 41 41 41 41 41  42 42 42 42 42 42 42 42  |AAAAAAAABBBBBBBB|

  @ingroup  ned_log
  @param    pData     data to dump
  @param    len       length of data to dump
  @param    linlen    length of line
  @param    indent    indent
  @return   length of dump text
*///-----------------------------------------------------------------------------------------------
size_t NedLogDumpEx(nedLogMask_t mask, const void *pDataIn, unsigned len, unsigned linelen, unsigned indent)
{
  size_t txtLen = 0;
  if(NedLogMaskGet() & mask)
    txtLen = NedLogDump(pDataIn, len, linelen, indent);
  return txtLen;
}

/*!------------------------------------------------------------------------------------------------
  Set the log mask bits, affects what will print with NedLogPrintfEx()

  @ingroup  ned_log
  @param    mask      bitmask, affects NedLogPrintfEx()

  @return   old mask
*///-----------------------------------------------------------------------------------------------
nedLogMask_t NedLogMaskSet(nedLogMask_t mask)
{
  nedLogMask_t oldMask = m_logMask;
  m_logMask = mask;
  return oldMask;
}

/*!------------------------------------------------------------------------------------------------
  Get the current mask

  @return   current bitmask, affects NedLogPrintfEx()
*///-----------------------------------------------------------------------------------------------
nedLogMask_t NedLogMaskGet(void)
{
  return m_logMask;
}

/*!------------------------------------------------------------------------------------------------
  Get the total size of the log

  @ingroup  ned_log

  @return   size of the log in bytes
*///-----------------------------------------------------------------------------------------------
size_t NedLogSizeGet(void)
{
  return m_logSize;
}

/*!------------------------------------------------------------------------------------------------
  Reset size of log (does not delete the file. Higher layer must do that)

  @ingroup  ned_log

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedLogSizeReset(void)
{
  m_logSize = 0;
}
