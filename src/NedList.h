/*!************************************************************************************************
  @file NedList.h

  @brief  Ned List API - simple API for a lists of objects

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_list Ned List - An API for keeping track of dynamic strings in an unordered list.

  A list is a circular set of items. It is very generic and has no idea what type of data is placed
  in the list. The list makes a copy of the data, so the data can be in local variables.

*///***********************************************************************************************
#ifndef NED_LIST_H
#define NED_LIST_H

#include "Ned.h"

#define NEDLIST_DEF_SIZE  0   // default size, as given to NedListNew()

typedef const void   *hNedList_t;
typedef bool_t      (*pfnListIsSame_t)  (const void *ptr1, const void *ptr2);
typedef void        (*pfnListShow_t)    (const void *ptr);


// API
hNedList_t    NedListNew      (unsigned maxItems, unsigned size, pfnListIsSame_t pfnIsSame, pfnListShow_t pfnShow);
bool_t        NedListIsList   (hNedList_t hList);
void          NedListLogShow  (hNedList_t hList, bool_t fDetailed);
void          NedListClear    (hNedList_t hList);
void          NedListFree     (hNedList_t hList);
unsigned      NedListMaxItems (hNedList_t hList);
unsigned      NedListNumItems (hNedList_t hList);
bool_t        NedListAdd      (hNedList_t hList, const void *pItem, unsigned size);
bool_t        NedListRemove   (hNedList_t hList, const void *pItem);
void         *NedListFind     (hNedList_t hList, const void *pItem);
void         *NedListPrev     (hNedList_t hList);
void         *NedListNext     (hNedList_t hList);
void          NedListIterStart(hNedList_t hList);

#endif // NED_LIST_H
