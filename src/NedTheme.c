/*!************************************************************************************************
  @file NedTheme.c

  @brief Ned Theme implementation

  Copyright (c) 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  NedTheme provides the default colors for the various built-in ned themes.

  @ingroup  ned_theme

*///***********************************************************************************************
#include <string.h>
#include "NedAnsi.h"
#include "NedTheme.h"

// array must match aszThemeNameDefaults[] and nedThemeIndex_t in NedTheme.h
static nedTheme_t     m_aThemeColorDefaults[] =
{
  // text, textHighlight, comment, keyword, constant
  // menuText, menuHotkey, menuDropText, menuDropHotkey, message
  // tx, ht, cm, kw, cn, mt, mh, dt, dh, ms
  {  71,124, 79, 72, 73,124,121, 79, 73, 75},  // NEDTHEME_CLASSIC  was {  71,127, 79, 72, 73,127,121, 79, 73, 75}
  { 135,120,143,132,129, 63, 60,143,140,135},  // NEDTHEME_DARKMODE was { 135,120,132,128,129, 63, 60,143,140,135}
  {   0,112, 12,  4,  2, 79, 66,127,114, 12},  // NEDTHEME_NATURAL
  {   0,112,  8,  8,  8, 55, 63,135,143,  0},  // NEDTHEME_MONOCHOME
  {  50, 50, 50, 50, 50, 50, 50, 50, 50, 50},  // NEDTHEME_1980s
  {  0,   0,  0,  0,  0,  0,  0,  0,  0,  0}   // NEDTHEME_ZERO
};

// array must match aThemeColorDefaults[] and nedThemeIndex_t in NedTheme.h
static const char    *m_aszThemeNameDefaults[] = 
{
  "Classic",
  "Dark Mode",
  "Natural",
  "Monochrome",
  "1980s",
  "Zero"
};

// the two arrays must match in size
static nedTheme_t     m_aThemes[NEDCFG_THEME_MAX];
static char           m_aszThemeNames[NEDCFG_THEME_MAX][NEDTHEME_MAX_NAMESIZE];

/*!------------------------------------------------------------------------------------------------
  Set all themes back to defaults.

  @return   none
*///-----------------------------------------------------------------------------------------------
void  NedThemeInit(void)
{
  unsigned   i;

  memset(m_aThemes, 0, sizeof(m_aThemes));
  memset(m_aszThemeNames, 0, sizeof(m_aszThemeNames));
  memcpy(m_aThemes, m_aThemeColorDefaults, sizeof(m_aThemeColorDefaults));
  for(i = 0; i < NumElements(m_aszThemeNameDefaults); ++i)
    strcpy(&m_aszThemeNames[i][0], m_aszThemeNameDefaults[i]);
}

/*!------------------------------------------------------------------------------------------------
  Get a pointer to the theme colors for index 0-n

  @param    index       index into master theme array

  @return   return theme pointer or NULL if index out of range
*///-----------------------------------------------------------------------------------------------
nedTheme_t * NedThemeGet(unsigned index)
{
  nedTheme_t *pTheme = NULL;

  if(index < NEDCFG_THEME_MAX && strlen(&m_aszThemeNames[index][0]) > 0)
    pTheme = &m_aThemes[index];

  return pTheme;
}

/*!------------------------------------------------------------------------------------------------
  Set the theme colors for index 0-n. May overwrite an entry if name already in list

  @param    szName      Name of theme
  @param    index       

  @return   TRUE if worked, FALSE if 
*///-----------------------------------------------------------------------------------------------
bool_t NedThemeSet(const char *szName, const nedTheme_t *pTheme)
{
  unsigned  slot = NEDCFG_THEME_MAX;
  unsigned  i;
  bool_t    fSet = FALSE;

  for(i = 0; i < NEDCFG_THEME_MAX; ++i)
  {
    // found an empty slot, use this
    if(strlen(&m_aszThemeNames[i][0]) == 0)
    {
      if(slot >= NEDCFG_THEME_MAX)
        slot = i;
    }

    // found the name, overwrite
    else if(strcmp(szName, &m_aszThemeNames[i][0]) == 0)
    {
      slot = i;
      break;
    }
  }

  // if a slot found, use it, limit length of name to NEDTHEME_MAX_NAMESIZE - 1
  if(slot < NEDCFG_THEME_MAX)
  {
    strncpy(&m_aszThemeNames[slot][0], szName, NEDTHEME_MAX_NAMESIZE);
    m_aszThemeNames[slot][NEDTHEME_MAX_NAMESIZE - 1] = '\0';
    m_aThemes[slot] = *pTheme;
    fSet = TRUE;
  }

  return fSet;
}

/*!------------------------------------------------------------------------------------------------
  Get the theme name

  @param    index       index into theme array

  @return   ptr to theme name or NULL if index out of range
*///-----------------------------------------------------------------------------------------------
const char * NedThemeGetName(unsigned index)
{
  const char * szThemeName = NULL;

  if(index < NEDCFG_THEME_MAX && strlen(&m_aszThemeNames[index][0]) > 0)
    szThemeName = &m_aszThemeNames[index][0];

  return szThemeName;
}

/*!------------------------------------------------------------------------------------------------
  Get the theme index from name

  @param    szThemeName   pointer to desired theme name

  @return   theme index (0-n), or NEDTHEME_NOT_FOUND if not found
*///-----------------------------------------------------------------------------------------------
unsigned NedThemeGetIndex(const char *szThemeName)
{
  unsigned    index = NEDTHEME_NOT_FOUND;
  unsigned    i;

  for(i = 0; i < NEDCFG_THEME_MAX; ++i)
  {
    if(m_aszThemeNames[i][0] && strcmp(szThemeName, &m_aszThemeNames[i][0]) == 0)
    {
      index = i;
      break;
    }
  }

  return index;
}

/*!------------------------------------------------------------------------------------------------
  Given a pointer to a theme, is this one of the default themes?

  @param    pTheme      pointer to theme to store

  @return   none
*///-----------------------------------------------------------------------------------------------
bool_t NedThemeIsDefault(const nedTheme_t *pTheme)
{
  nedTheme_t   *pThisTheme;
  unsigned      i;
  bool_t        fIsDefault = FALSE;

  for(i = 0; i < NumElements(m_aThemeColorDefaults); ++i)
  {
    pThisTheme = &m_aThemeColorDefaults[i];
    if(pTheme->text           == pThisTheme->text &&
       pTheme->textHighlight  == pThisTheme->textHighlight &&
       pTheme->comment        == pThisTheme->comment &&
       pTheme->keyword        == pThisTheme->keyword &&
       pTheme->constant       == pThisTheme->constant &&
       pTheme->menuText       == pThisTheme->menuText &&
       pTheme->menuHotkey     == pThisTheme->menuHotkey &&
       pTheme->menuDropText   == pThisTheme->menuDropText &&
       pTheme->menuDropHotkey == pThisTheme->menuDropHotkey &&
       pTheme->message        == pThisTheme->message)
    {
      fIsDefault = TRUE;
      break;
    }
  }

  return fIsDefault;
}

