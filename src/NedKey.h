/*!************************************************************************************************
  @file NedKey.h

  @brief  Ned Key API

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_key NedKey is an API for getting keys and key names (e.g. "PgUp", "Home", "Ctrl-A")

  API to get application keys. It associates key names and keys. It converts key sequences into a
  single key value. For example, left arrow key sequence is 27,91,68 and is converted to
  NEDKEY_LEFT.

*///***********************************************************************************************
#ifndef NED_KEY_H
#define NED_KEY_H
#include "Ned.h"


typedef unsigned char nedKey_t;

// control keys
#define NEDKEY_CTRL_SPACE        0
#define NEDKEY_CTRL_A            1
#define NEDKEY_CTRL_B            2
#define NEDKEY_CTRL_C            3
#define NEDKEY_CTRL_D            4
#define NEDKEY_CTRL_E            5
#define NEDKEY_CTRL_F            6
#define NEDKEY_CTRL_G            7
#define NEDKEY_CTRL_H            8
#define NEDKEY_CTRL_I            9
#define NEDKEY_TAB               9  // same as Ctrl-I
#define NEDKEY_CTRL_J           10
#define NEDKEY_LF               10
#define NEDKEY_CTRL_K           11
#define NEDKEY_CTRL_L           12
#define NEDKEY_CTRL_M           13
#define NEDKEY_CR               13  // same as Ctrl-M
#define NEDKEY_ENTER            13  // same as Ctrl-M
#define NEDKEY_CTRL_N           14
#define NEDKEY_CTRL_O           15
#define NEDKEY_CTRL_P           16
#define NEDKEY_CTRL_Q           17
#define NEDKEY_CTRL_R           18
#define NEDKEY_CTRL_S           19
#define NEDKEY_CTRL_T           20
#define NEDKEY_CTRL_U           21
#define NEDKEY_CTRL_V           22
#define NEDKEY_CTRL_W           23
#define NEDKEY_CTRL_X           24
#define NEDKEY_CTRL_Y           25
#define NEDKEY_CTRL_Z           26
#define NEDKEY_ESC              27  // Esc, Ctrl-[
#define NEDKEY_MENU             27  // Same as Esc
#define NEDKEY_CTRL_BACKSLASH   28  // Ctrl-backspace
#define NEDKEY_CTRL_RIGHT_BRACE 29  // Ctrl-]
#define NEDKEY_CTRL_CAROT       30  // Ctrl-^
#define NEDKEY_CTRL_MINUS       31  // Ctrl-Minus
#define NEDKEY_CTRL_MIN         NEDKEY_CTRL_SPACE
#define NEDKEY_CTRL_MAX         NEDKEY_CTRL_MINUS

// ascii keys
#define NEDKEY_ASCII_MIN        ((nedKey_t)' ')
#define NEDKEY_ASCII_MAX        ((nedKey_t)'~')

// backspace
#define NEDKEY_BACKSPACE       127  // (Fn-delete on MAC)

// cursor keys
#define NEDKEY_UP              128  // 27,91,65
#define NEDKEY_DOWN            129  // 27,91,66
#define NEDKEY_LEFT            130  // 27,91,68
#define NEDKEY_RIGHT           131  // 27,91,67
#define NEDKEY_HOME            132  // 27,79,72     (pseudo key on Mac OS X)
#define NEDKEY_END             133  // 27,79,70     (pseudo key on Mac OS X)
#define NEDKEY_PGUP            134  // 27,91,53,126 (pseudo key on Mac OS X)
#define NEDKEY_PGDN            135  // 27,91,54,126 (pseudo key on Mac OS X)
#define NEDKEY_CTRL_UP         136  // 27, 91, 49, 59, 53, 65
#define NEDKEY_CTRL_DOWN       137  // 27, 91, 49, 59, 53, 66
#define NEDKEY_CTRL_LEFT       138  // 27, 91, 49, 59, 53, 68
#define NEDKEY_CTRL_RIGHT      139  // 27, 91, 49, 59, 53, 67
#define NEDKEY_CTRL_HOME       140  // psuedo key
#define NEDKEY_CTRL_END        141  // psuedo key
#define NEDKEY_CTRL_PGUP       142  // psuedo key
#define NEDKEY_CTRL_PGDN       143  // psuedo key
#define NEDKEY_ALT_UP          144  // psuedo key
#define NEDKEY_ALT_DOWN        145  // psuedo key
#define NEDKEY_ALT_LEFT        146  // 27, 98
#define NEDKEY_ALT_RIGHT       147  // 27, 102
#define NEDKEY_ALT_HOME        148  // psuedo key
#define NEDKEY_ALT_END         149  // psuedo key
#define NEDKEY_ALT_PGUP        150  // psuedo key
#define NEDKEY_ALT_PGDN        151  // psuedo key
#define NEDKEY_SHIFT_LEFT      152  // 27,91,49,59,50,68
#define NEDKEY_SHIFT_RIGHT     153  // 27,91,49,59,50,67
#define NEDKEY_CTRL_BACKSPACE  154  // 27,91,51,59,53,126

// function keys
#define NEDKEY_FN1             160  // 27, 79, 80
#define NEDKEY_FN2             161  // 27, 79, 81
#define NEDKEY_FN3             162  // 27, 79, 82
#define NEDKEY_FN4             163  // 27, 79, 83
#define NEDKEY_FN5             164  // 27, 91, 49, 53, 126
#define NEDKEY_FN6             165  // 27, 91, 49, 55, 126
#define NEDKEY_FN7             166  // 27, 91, 49, 56, 126
#define NEDKEY_FN8             167  // 27, 91, 49, 57, 126
#define NEDKEY_FN9             168  // 27, 91, 50, 48, 126
#define NEDKEY_FN10            169  // 27, 91, 50, 49, 126
#define NEDKEY_FN11            170  // 27, 91, 50, 51, 126
#define NEDKEY_FN12            171  // 27, 91, 50, 52, 126

// Alt Keys
#define NEDKEY_ALT_A           180  // 195, 165
#define NEDKEY_ALT_B           181  // 226, 136, 171
#define NEDKEY_ALT_C           182  // 195, 167
#define NEDKEY_ALT_D           183  // 226, 136, 130
#define NEDKEY_ALT_E           184  // psuedo key
#define NEDKEY_ALT_F           185  // 198, 146
#define NEDKEY_ALT_G           186  // 194, 169
#define NEDKEY_ALT_H           187  // 203, 153
#define NEDKEY_ALT_I           188  // psuedo key
#define NEDKEY_ALT_J           189  // 226, 136, 134
#define NEDKEY_ALT_K           190  // 203, 154
#define NEDKEY_ALT_L           191  // 194, 172
#define NEDKEY_ALT_M           192  // 194, 181
#define NEDKEY_ALT_N           193  // psuedo key
#define NEDKEY_ALT_O           194  // 195, 184
#define NEDKEY_ALT_P           195  // 207, 128
#define NEDKEY_ALT_Q           196  // 197, 147
#define NEDKEY_ALT_R           197  // 194, 174
#define NEDKEY_ALT_S           198  // 195, 159
#define NEDKEY_ALT_T           199  // 226, 128, 160
#define NEDKEY_ALT_U           200  // psuedo key
#define NEDKEY_ALT_V           201  // 226, 136, 154
#define NEDKEY_ALT_W           202  // 226, 136, 145
#define NEDKEY_ALT_X           203  // 226, 137, 136
#define NEDKEY_ALT_Y           204  // psuedo key
#define NEDKEY_ALT_Z           205  // 206, 169

// specialty keys
#define NEDKEY_BACK_TAB        210  // 27,91,90
#define NEDKEY_DELETE          211  // 27, 91, 51, 126 (fn+delete on MAC)
#define NEDKEY_ALT_DASH        212  // 226, 128, 147 
#define NEDKEY_ALT_EQUAL       213  // 226, 137, 160
#define NEDKEY_ALT_LEFT_BRACE  214  // 226, 128, 156
#define NEDKEY_ALT_RIGHT_BRACE 215  // 226, 128, 152
#define NEDKEY_ALT_BACKSLASH   216  // 194, 171
#define NEDKEY_ALT_SPACE       217  // 194 168
#define NEDKEY_ALT_COLON       218  // 226, 128, 166
#define NEDKEY_ALT_QUOTE       219  // 195, 166
#define NEDKEY_ALT_COMMA       220  // 226, 137, 164
#define NEDKEY_ALT_PERIOD      221  // 226, 137, 165
#define NEDKEY_ALT_SLASH       222  // 226, 137, 166

#define NEDKEY_MENU_ENTER      254  // pseudo key
#define NEDKEY_NONE            255  // causes no action, guaranteed to be last key

// for defining hot-keys
#define NedKeyCtrl(key) (NEDKEY_CTRL_A + ((key)-'A'))
#define NedKeyAlt(key)  (NEDKEY_ALT_A  + ((key)-'A'))
#define NedKeyFn(key)   (NEDKEY_FN1    +  (key))

#define NEDKEY_MACRO_MAX       1024

// callback for idle
typedef void (*pfnNedKeyIdle_t)(void);


/*************************************************************************
  Prototypes
**************************************************************************/

// key functions
nedKey_t          NedKeyGetKey          (void);
void              NedKeySetIdle         (pfnNedKeyIdle_t pfnIdle);
const char *      NedKeyName            (nedKey_t key);
nedKey_t          NedKeyFromName        (const char *szName);

// macro functions
void              NedKeyMacroPlay       (void);
bool_t            NedKeyInMacro         (void);
bool_t            NedKeyMacroRecording  (void);
void              NedKeyMacroRecord     (void);
void              NedKeyMacroEndRecord  (void);
void              NedKeyMacroClear      (void);
void              NedKeyMacroAddKey     (nedKey_t key);
const nedKey_t   *NedKeyMacroGet        (unsigned *pNumKeys);

// raw key functions
void              NedKeyRawEnable   (void);
void              NedKeyRawDisable  (void);
nedKey_t          NedKeyRawGetKey   (void);

#endif // NED_KEY_H
