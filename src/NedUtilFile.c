/*!************************************************************************************************
  @file NedUtilFile.c

  @brief  Generic file utilities for checking if a file or path exists.

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @ingroup  ned_util

*///***********************************************************************************************
#include <glob.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "NedLog.h"
#include "NedUtilStr.h"
#include "NedUtilFile.h"

#define NEDFILELIST_SANCHK        21212
#define NEDTABCOMPLETE_SANCHK     51515

typedef struct
{
  unsigned              sanchk;
  unsigned              len;      // size of following arrays
  const char          **aszPath;  // array of string pointers
} sNedFileList_t;

typedef struct
{
  unsigned    sanchk;             // sanity check
  void       *hFileList;          // list of matching files/folders
  unsigned    index;              // index into list
  bool_t      fReduceHome;        // convert from "/Users/drewg/" to "~/" in user szPath
  unsigned    maxSize;            // max size of szPath
  char       *szPath;             // ptr to path to copy to user string
} sNedTabComplete_t;

/*!------------------------------------------------------------------------------------------------
  Read a text file into memory (does not support binary files). User must free the memory. If file
  couldn't be read, returns NULL.

  @param    szFilename      Filename to read. Can include full or partial path.
  @return   pointer file in memory, or NULL if failed. Caller must free file memory
*///-----------------------------------------------------------------------------------------------
char * NedFileRead(const char *szFilename)
{
  FILE     *fp;
  char     *pszFile = NULL;
  long      len     = 0;

  // NedLogPrintfEx(NEDLOG_UTILS, "NedFileRead %s\n", szFilename);
  fp = fopen(szFilename, "r");
  if(fp)
  {
    // NedLogPrintfEx(NEDLOG_UTILS, "  fp %p\n", fp);
    fseek(fp, 0L, SEEK_END);
    len = ftell(fp);
    fseek(fp, 0L, SEEK_SET);
  }
  // NedLogPrintfEx(NEDLOG_UTILS, "  len %ld\n", len);
  pszFile = malloc(len + 1);

  if(fp && pszFile)
  {
    // NedLogPrintfEx(NEDLOG_UTILS, "  reading\n");
    len = fread(pszFile, 1, len, fp);
  }
  if(fp)
    fclose(fp);
  if(pszFile)
    pszFile[len] = '\0';

  return pszFile;
}


/*!------------------------------------------------------------------------------------------------
  Essentially "which" under Linux/Mac or "where" under Windows. Looks for the file szBaseName
  in the current directory or path.

  /Library/Frameworks/Python.framework/Versions/3.8/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:
  /sbin:/Applications/VMware Fusion.app/Contents/Public

  @param    szPath          destination path string
  @param    maxSize         sizeof(szPath) buffer
  @param    szBaseName      file to look for (could be exec name for example)
  @param    cwdFirst        Look at cwd first, before scanning path for file
  @return   TRUE and path in szPath if found, FALSE if not found.
*///-----------------------------------------------------------------------------------------------
bool_t NedFileFindInPath(char *szPath, unsigned maxSize, const char *szBaseName, bool_t cwdFirst)
{
  char     *psz;
  char     *pszThisPath;
  unsigned  len;
  unsigned  baseLen;
  bool_t    fWorked = FALSE;

  if(!szPath || !maxSize || !szBaseName)
    return FALSE;

  if(cwdFirst && NedFileExists(szBaseName))
  {
    if(strlen(szBaseName) < maxSize)
    {
      strcpy(szPath, szBaseName);
      fWorked = TRUE;
    }
  }
  else
  {
    pszThisPath = getenv("PATH");
    baseLen = strlen(szBaseName);
    while(pszThisPath && *pszThisPath)
    {
      psz = strchr(pszThisPath, ':');
      if(psz)
        len = psz - pszThisPath;
      else
        len = strlen(pszThisPath);
      if(len + 1 + baseLen < maxSize)
      {
        strncpy(szPath, pszThisPath, len);
        szPath[len] = '/';
        strcpy(&szPath[len+1], szBaseName);
        if(NedFileExists(szPath))
        {
          fWorked = TRUE;
          break;
        }
      }
      if(!psz)
        break;
      pszThisPath = psz + 1;   // skip ':' or ';' separator
    }
  }

  return fWorked;
}

/*!------------------------------------------------------------------------------------------------
  Searches for this file, going from szBaseFolder to root, then in user folder. If szBaseFolder is
  NULL, the uses cwd.

  For example, if szBaseFolder is "/Users/drewg/Git/ned/test", and basename is "foo" searches:
  
  "/Users/drewg/Git/ned/test/foo"
  "/Users/drewg/Git/ned/foo"
  "/Users/drewg/Git/foo"
  "/Users/drewg/foo"
  "/Users/foo"
  "/foo"

  @param    szPath          destination path string
  @param    maxSize         sizeof(szPath) buffer
  @param    szBaseName      name of file to look for
  @param    szBaseFolder    NULL to use cwd, or folder to search starting at the folder
  @return   TRUE and path in szPath if found, FALSE if not found.
*///-----------------------------------------------------------------------------------------------
bool_t NedFileFindInFolder(char *szPath, unsigned maxSize, const char *szBaseName, const char *szBaseFolder)
{
  char  *psz;

  if(!szBaseName || !szPath || !maxSize)
    return FALSE;

  // find base path
  if(!szBaseFolder)
  {
    if(getcwd(szPath, maxSize) == NULL)
      return FALSE;
  }
  else
  {
    if(strlen(szBaseFolder) >= maxSize || !NedLibFileFullPath(szPath, szBaseFolder))
      return FALSE;
  }

  // verify entire path with basename is OK in size
  if(strlen(szPath) + 1 + strlen(szBaseName) >= maxSize)
    return FALSE;

  psz = szPath + strlen(szPath);
  *psz = '/';
  while(psz)
  {
    // found the file?
    strcpy(psz+1, szBaseName);
    if(NedFileExists(szPath))
      return TRUE;

    // no, try parent folder
    --psz;
    if(psz <= szPath)
      break;
    psz = NedMemRChr(szPath, '/', (psz - szPath));
  }

  // if not found, look in home folder
  psz = getenv("HOME");
  if(psz && (strlen(psz) + 1 + strlen(szBaseName) < maxSize))
  {
    strcpy(szPath, psz);
    psz = szPath + strlen(szPath);
    *psz = '/';
    strcpy(psz+1, szBaseName);
    if(NedFileExists(szPath))
      return TRUE;
  }

  return FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Get home folder

  @ingroup  ned_util
  @param    szPath      pointer to dst string
  @param    maxSize     usually PATH_MAX

  @return   filled in szPath with home folder.
*///-----------------------------------------------------------------------------------------------
void NedFileGetHomeFolder(char *szPath, unsigned maxSize)
{
  char       *psz;
  unsigned    len;

  // get home folder and copy it, up to maxSize-1
  psz = getenv("HOME");
  if(psz)
  {
    len = strlen(psz);
    if(len >= maxSize)
      len = maxSize - 1;  // allow room for '\0'
    strncpy(szPath, psz, len);
    szPath[len] = '\0';
  }
  else
  {
    strcpy(szPath, ".");
  }
}

/*!------------------------------------------------------------------------------------------------
  Expands home folder in-place in path string. E.g. "~/Doc*" becomes "/Users/drewg/Doc*". Does
  nothing if string doesn't begin with "~/" or length of strings are too long.

  @param    szPath      pointer to dst string
  @param    maxSize     sizeof szPath string
  @return   TRUE if expanded. FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedFileExpandHome(char *szPath, unsigned maxSize)
{
  const char   *pszHome;
  unsigned      homeLen;
  unsigned      len;
  bool_t        fExpanded = FALSE;

  // get home folder and copy it, up to maxSize-1
  pszHome = getenv("HOME");
  if(pszHome && (strncmp(szPath, "~/", 2) == 0))
  {
    homeLen = strlen(pszHome);
    len     = strlen(szPath);
    if(homeLen + len < maxSize)
    {
      // expand, leaving \0' and end of szPath
      memmove(&szPath[homeLen], &szPath[1], len);
      memcpy(szPath, pszHome, homeLen);
      fExpanded = TRUE;
    }
  }

  return fExpanded;
}

/*!------------------------------------------------------------------------------------------------
  If string starts with home folder, reduce string to use tilde. Examples:

  "/Users/drewg" becomes "~"
  "/Users/drewg/Work/X*" becomes "~/Work/X*"
  "myfile.txt" remains unchanged

  @param    szPath      pointer string
  @return   TRUE if reduced
*///-----------------------------------------------------------------------------------------------
bool_t NedFileReduceHome(char *szPath)
{
  const char   *pszHome;
  unsigned      homeLen;
  unsigned      len;
  bool_t        fReduced = FALSE;

  // get home folder and copy it, up to maxSize-1
  pszHome = getenv("HOME");
  if(pszHome)
    homeLen = strlen(pszHome);
  if(szPath && pszHome && (strncmp(szPath, pszHome, homeLen) == 0))
  {
    len = strlen(szPath);
    memmove(&szPath[1], &szPath[homeLen], (len - homeLen) + 1);
    szPath[0] = '~';
    fReduced = TRUE;
  }

  return fReduced;
}

/*!------------------------------------------------------------------------------------------------
  Returns the user home folder name (from environment $HOME). For use with '~' in filenames.

  @ingroup  ned_utils
  @return   Returns name. 
*///-----------------------------------------------------------------------------------------------
void NedFileGetCwd(char *szPath, unsigned maxSize)
{
  getcwd(szPath, maxSize);
}

/*!------------------------------------------------------------------------------------------------
  Returns the full path from a partial name. It's OK if the file doesn't exists as long as the
  folder does.

  @ingroup  ned_util
  @param    szFullPath      pointer to a PATH_MAX sized string, to be filled in if TRUE
  @param    szPartialPath   pointer to a partial path like "file.c" or "../file.h" or "~/file.c"

  @return   TRUE if folder exists. File may or may not exist. Returns FALSE if bad path.
*///-----------------------------------------------------------------------------------------------
bool_t NedLibFileFullPath(char *szFullPath, const char *szPartialPath)
{
  sNedFileInfo_t  sInfo;
  bool_t          fWorked;
  const char     *szFolder;

  fWorked = NedFileInfoGet(&sInfo, szPartialPath);
  if(fWorked)
    strcpy(szFullPath, sInfo.szFullPath);
  else
  {
    szFolder = NedStrDirOnlyHome(szPartialPath);
    if(*szFolder == '\0')
      szFolder = ".";
    fWorked = NedFileInfoGet(&sInfo, szFolder);
    if(fWorked)
    {
      strcpy(szFullPath, sInfo.szFullPath);
      fWorked = NedStrAppendName(szFullPath, NedStrNameOnly(szPartialPath));
    }
  }

  return fWorked;
}

/*!------------------------------------------------------------------------------------------------
  Returns the parent path from a full or partial path. Essentially, goes to the previous '/'. Does
  it in place.

  Example: "/Users/drew/ned/x", becomes "/Users/drew/x"

  @ingroup  ned_util
  @param    szPath    pointer to a PATH_MAX sized string, to be filled in always

  @return   TRUE if a parent folder exists
*///-----------------------------------------------------------------------------------------------
bool_t NedLibParentPath(char *szPath)
{
  bool_t    fFoundParent  = FALSE;
  char     *szPrevSlash   = NULL;
  char     *szSlash       = NULL;
  char     *psz;

  psz = szPath;
  while(psz && *psz)
  {
    if(*psz == '/')
    {
      if(szSlash)
        szPrevSlash = (char *)szSlash;
      szSlash = psz;
    }
    ++psz;
  }

  if(szPrevSlash)
  {
    memmove(szPrevSlash, szSlash, strlen(szSlash) + 1);
    fFoundParent = TRUE;
  }
  return fFoundParent;
}

/*!------------------------------------------------------------------------------------------------
  Get information about this file. The preallocated pInfo should be filled with 0s the first time
  this is called on a file. Subsequent calls (which are used to check if a file was modified after
  loading the file) should used on the same structure.

  @ingroup  ned_util
  @param    szPath     pointer to a file or dir path (relative or absolute).
  @param    pInfo      pointer to preallocated pInfo structure.

  @return   TRUE if file/folder exists and results in sNedFileInfo_t structure.
            Returns FALSE if not a file or folder.
*///-----------------------------------------------------------------------------------------------
bool_t NedFileInfoGet(sNedFileInfo_t *pInfo, const char *szPath)
{
  struct stat   sStat;
  bool_t        fExists = FALSE;
  int           err;

  // NedLogPrintfEx(NEDLOG_UTILS, "NedFileInfoGet(path %s)\n",szPath);

  // if not initialized, do so now
  if(pInfo->sanchk != FILEINFO_SANCHK)
  {
    memset(pInfo, 0, sizeof(sNedFileInfo_t));
    pInfo->sanchk = FILEINFO_SANCHK;
  }

  // if it exists, make sure it's a file or dir (not a link)
  err = stat(szPath, &sStat);
  if(err == 0 && ((sStat.st_mode & S_IFDIR) || (sStat.st_mode & S_IFREG)) )
  {
    fExists = TRUE;
    // NedLogPrintfEx(NEDLOG_UTILS, "  mode ifDir %u, ifReg %u\n", (sStat.st_mode & S_IFDIR), (sStat.st_mode & S_IFREG));

    if(sStat.st_mode & S_IFREG)
    {
      pInfo->fIsDir = FALSE;
      if(access(szPath, W_OK) == 0)
        pInfo->fRdOnly = FALSE;
      else
        pInfo->fRdOnly = TRUE;
    }
    else
    {
      pInfo->fIsDir   = TRUE;
      pInfo->fRdOnly  = TRUE;
    }

    if(pInfo->modTime == 0)
    {
      // NedLogPrintfEx(NEDLOG_UTILS, "  New time %lu\n",sStat.st_mtime);
      pInfo->fIsModified  = FALSE;
      pInfo->modTime      = sStat.st_mtime;
    }
    else if(sStat.st_mtime > pInfo->modTime)
    {
      // NedLogPrintfEx(NEDLOG_UTILS, "  Mod time %lu\n",sStat.st_mtime);
      pInfo->fIsModified  = TRUE;
      pInfo->modTime      = sStat.st_mtime;
    }
    else
    {
      pInfo->fIsModified = FALSE;
    }

    if(realpath(szPath, pInfo->szFullPath) == NULL)
    {
      strcpy(pInfo->szFullPath, szPath);
    }
    // NedLogPrintfEx(NEDLOG_UTILS, "  Full Path %s\n", pInfo->szFullPath);
  }

  return fExists;
}


/*!------------------------------------------------------------------------------------------------
  Return TRUE if file exists. Returns false if path is to a directory or doesn't exist.

  @ingroup    ned_file
  @param      szPath         pointer to path string

  @return     TRUE 
*///-----------------------------------------------------------------------------------------------
bool_t NedFileExists(const char *szPath)
{
  bool_t          fExists = FALSE;
  sNedFileInfo_t  sInfo;

  if(szPath)
  {
    fExists = NedFileInfoGet(&sInfo, szPath);
    if(fExists && sInfo.fIsDir)
      fExists = FALSE;
  }
  return fExists;
}


/*!------------------------------------------------------------------------------------------------
  Is this a pointer to an ::sNedFileList_t structure?

  @ingroup    ned_file
  @param      hList         pointer to file list handle

  @return     none
*///-----------------------------------------------------------------------------------------------
bool_t NedFileIsList(void *hList)
{
  sNedFileList_t  *pFileList = hList;
  return (pFileList && (pFileList->sanchk == NEDFILELIST_SANCHK)) ? TRUE : FALSE;
}


/*!------------------------------------------------------------------------------------------------
  Create a list of files and directories. Uses glob under the hood. Returns filenames only.

  Example Inputs:

      "tcdata_*.txt"
      "*"
      "/Users/drewg"
      "."

  Example Outputs:
    READ.ME
    docs/
    src/
    x file is y.z

  @ingroup        ned_file
  @param          pszWildPath    a wildcard path (e.g. "*.c", or a directory, e.g. "..")

  @return         Returns handle to file list or NULL if bad parameter or empty list
*///-----------------------------------------------------------------------------------------------
void * NedFileListNew(const char *pszWildPath)
{
  glob_t            globbuf;
  sNedFileList_t   *pFileList = NULL;
  char             *sz;
  unsigned          len       = 0;
  unsigned          i;

  NedLogPrintfEx(NEDLOG_UTILS, "NedFileListNew(%s)\n", pszWildPath ? pszWildPath : "NULL");

  // will return NULL if no path
  if((pszWildPath != NULL) && (glob(pszWildPath, GLOB_MARK, NULL, &globbuf) == 0))
  {
    // NedLogPrintfEx(NEDLOG_UTILS, "  glob pathc %zu\n", globbuf.gl_pathc);
    if(globbuf.gl_pathc)
    {
      // determine total size of all strings
      for(i=0; i<globbuf.gl_pathc; ++i)
      {
        // NedLogPrintfEx(NEDLOG_UTILS, "  len %u, glob pathv[%u] = %s\n", len, i, globbuf.gl_pathv[i]);
        len += strlen(globbuf.gl_pathv[i]) + 1;   // room for '\0'
      }

      // all 3 objects together in 1 malloc (structure, table of str ptrs, strings)
      pFileList = malloc(sizeof(sNedFileList_t) + (globbuf.gl_pathc * sizeof(const char *)) + len);
      if(pFileList != NULL)
      {
        // fill in structure
        pFileList->sanchk   = NEDFILELIST_SANCHK;
        pFileList->len      = globbuf.gl_pathc;

        // array of pointers
        pFileList->aszPath  = (void *)(pFileList + 1);

        // point sz to AFTER array of pointers, where we'll start adding strings
        sz                  = (char *)(&pFileList->aszPath[pFileList->len]);

        NedLogPrintfEx(NEDLOG_UTILS, "  pFileList %p, len %u, aszPath %p, sz %p\n", pFileList, pFileList->len,
          &pFileList->aszPath[0], sz);

        // fill in strings and ptrs to strings
        for(i=0; i<pFileList->len; ++i)
        {
          pFileList->aszPath[i] = sz;
          strcpy(sz, globbuf.gl_pathv[i]);
          // NedLogPrintfEx(NEDLOG_UTILS, "  pFileList->aszPath[%u] %p = %s\n", i, pFileList->aszPath[i], sz);
          sz += strlen(sz) + 1;
        }
      }
    }
    globfree(&globbuf);
  }

  return pFileList;
}


/*!------------------------------------------------------------------------------------------------
  Free a list of files that was made using NedFileListNew().

  @ingroup    ned_file
  @param      hList          A list previously allocated with NedFileListNew()

  @return     none
*///-----------------------------------------------------------------------------------------------
void NedFileListFree(void *hList)
{
  NedAssertDbg(NedFileIsList(hList));
  free(hList);
}


/*!------------------------------------------------------------------------------------------------
  Return length of file list (that is, # of items in file list)

  @ingroup    ned_file
  @param      List       A list previously allocated with NedFileListNew()

  @return     length of file list (1-n)
*///-----------------------------------------------------------------------------------------------
unsigned NedFileListLen(void *hList)
{
  sNedFileList_t   *pFileList = hList;
  unsigned          len       = 0;

  if(NedFileIsList(pFileList))
    len = pFileList->len;
  return len;
}


/*!------------------------------------------------------------------------------------------------
  Get the name at the index (does not include . or .. folders)

  @ingroup    ned_file
  @param      List       A list previously allocated with NedFileListNew()

  @return     ptr to filename, or NULL if at end of list
*///-----------------------------------------------------------------------------------------------
const char * NedFileListGetName(void *hList, unsigned i)
{
  const char      *szName     = NULL;
  sNedFileList_t  *pFileList  = hList;

  NedAssertDbg(NedFileIsList(pFileList));

  if(i < NedFileListLen(hList))
    szName = pFileList->aszPath[i];

  return szName;
}

/*!------------------------------------------------------------------------------------------------
  Return length assuming ".." is first entry, unless at /.

  @ingroup    ned_file
  @param      List       A list previously allocated with NedFileListNew()

  @return     length of file list (1-n)
*///-----------------------------------------------------------------------------------------------
unsigned NedFileListLenEx(void *hList)
{
  unsigned    len = NedFileListLen(hList);
  if(len)
    ++len;
  return len;
}

/*!------------------------------------------------------------------------------------------------
  Get the name at the index, where index 0 is always ".."

  @ingroup    ned_file
  @param      List       A list previously allocated with NedFileListNew()

  @return     ptr to filename, or NULL if at end of list
*///-----------------------------------------------------------------------------------------------
const char * NedFileListGetNameEx(void *hList, unsigned i)
{
  static const char szDotDot[] = "..";
  if(i == 0)
    return szDotDot;
  else
    return NedFileListGetName(hList, i - 1);
}

/*-------------------------------------------------------------------------------------------------
  Creates TabComplete state machine

  @param    maxSize     maximum size of path string for this state machine
  @preturn handle to state machine
-------------------------------------------------------------------------------------------------*/
void * NedTabCompleteNew(unsigned maxSize)
{
  sNedTabComplete_t *pTabComplete;

  pTabComplete = malloc(sizeof(sNedTabComplete_t));
  if(pTabComplete)
  {
    memset(pTabComplete, 0, sizeof(sNedTabComplete_t));
    pTabComplete->sanchk  = NEDTABCOMPLETE_SANCHK;
    pTabComplete->maxSize = maxSize;
    pTabComplete->szPath  = malloc(maxSize);
    if(!pTabComplete)
    {
      NedTabCompleteFree(pTabComplete);
      pTabComplete = NULL;
    }
  }

  return pTabComplete;
}

/*-------------------------------------------------------------------------------------------------
  Returns TRUE if this is a TabComplete state machine

  @param    hTabComplete     handle to TabComplete state machine
  @return TRUE if this is a TabComplete state machine
-------------------------------------------------------------------------------------------------*/
bool_t NedTabCompleteIsTabComplete(void *hTabComplete)
{
  sNedTabComplete_t  *pTabComplete = hTabComplete;

  return (pTabComplete && (pTabComplete->sanchk == NEDTABCOMPLETE_SANCHK)) ? TRUE : FALSE;
}

/*-------------------------------------------------------------------------------------------------
  Expands szPath in-place if any files or folders match the final name in string. Doesn't expand
  if wildcards are in input string.

  Examples (assuming current and home folder contains folders "Desktop/", "Documents/",
  "Downloads/", and file "Downtime.txt".

  "~/Doc*" becomes "~/Documents/"
  "/Users/drewg/Doc" becomes "/Users/drewg/Documents/"
  "./Doc" becomes "./Documents/"
  "D" becomes "Desktop/", then "Documents/", then "Downloads/", "Downtime.txt"
  "Nothere" doesn't change. Remains "Nothere"

  @param    szPath          a complete or partial path
  @param    hTabComplete    handle to TabComplete state machine
  @return FALSE if no (more) matches in the file/folder list
-------------------------------------------------------------------------------------------------*/
bool_t NedTabComplete(char *szPath, void *hTabComplete)
{
  sNedTabComplete_t  *pTabComplete  = hTabComplete;
  bool_t              fFound        = FALSE;

  // not valid state machine
  if(NedTabCompleteIsTabComplete(hTabComplete))
  {
    // if we are already in the state machine, get next item
    if((pTabComplete->hFileList != NULL) && strcmp(szPath, pTabComplete->szPath) == 0)
    {
      if(pTabComplete->index < NedFileListLen(pTabComplete->hFileList))
        fFound = TRUE;
    }

    // potentially start a new state machine with this path
    else
    {
      // done with previous file list (about to make new one)
      if(pTabComplete->hFileList)
      {
        NedFileListFree(pTabComplete->hFileList);
        pTabComplete->hFileList = NULL;
      }

      // expand home folder if needed (because glob() doesn't understand "~/*"
      NedStrZCpy(pTabComplete->szPath, szPath, pTabComplete->maxSize);
      pTabComplete->fReduceHome = FALSE;
      if(strncmp(pTabComplete->szPath, "~/", 2) == 0)
        pTabComplete->fReduceHome = TRUE;
      NedFileExpandHome(pTabComplete->szPath, pTabComplete->maxSize);

      // implicit wildcard, converts "Doc" to "Doc*", unless user already put in wildcard(s)
      if((strchr(pTabComplete->szPath, '*') == NULL) && (strchr(pTabComplete->szPath, '?') == NULL))
        strcat(pTabComplete->szPath, "*");
      NedLogPrintfEx(NEDLOG_UTILS, "NedTabComplete expanded path %s\n", pTabComplete->szPath);

      pTabComplete->hFileList = NedFileListNew(pTabComplete->szPath);
      if(NedFileListLen(pTabComplete->hFileList) > 0)
      {
        pTabComplete->index = 0;
        fFound = TRUE;
      }
    }
  }

  if(fFound)
  {
    strcpy(pTabComplete->szPath, NedFileListGetName(pTabComplete->hFileList, pTabComplete->index));
    if(pTabComplete->fReduceHome)
      NedFileReduceHome(pTabComplete->szPath);
    strcpy(szPath, pTabComplete->szPath);
    ++pTabComplete->index;
  }

  return fFound;
}

/*-------------------------------------------------------------------------------------------------
  Returns TRUE if successfully rewound the tab complete

  @param    maxSize     maximum size of path string for this state machine
  @preturn handle to state machine
-------------------------------------------------------------------------------------------------*/
bool_t NedTabCompleteRewind(void *hTabComplete)
{
  sNedTabComplete_t  *pTabComplete = hTabComplete;
  bool_t              fWorked = FALSE;

  if(NedTabCompleteIsTabComplete(hTabComplete) && (pTabComplete->hFileList != NULL))
  {
    pTabComplete->index = 0;
    fWorked = TRUE;
  }

  return fWorked;
}

/*-------------------------------------------------------------------------------------------------
  Frees a TabComplete state machine

  @param    hTabComplete     handle to TabComplete state machine
  @return   none
-------------------------------------------------------------------------------------------------*/
void NedTabCompleteFree(void *hTabComplete)
{
  sNedTabComplete_t *pTabComplete = hTabComplete;

  if(NedTabCompleteIsTabComplete(hTabComplete))
  {
    if(pTabComplete->hFileList)
    {
      NedFileListFree(pTabComplete->hFileList);
      pTabComplete->hFileList = NULL;
    }
    if(pTabComplete->szPath)
      free(pTabComplete->szPath);    
    memset(pTabComplete, 0, sizeof(sNedTabComplete_t));
    free(pTabComplete);
  }
}
