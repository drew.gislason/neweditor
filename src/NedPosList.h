/*!************************************************************************************************
  @file NedPointList.h

  @brief  API for a list of points

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  Uses the NedList module and the NedPoint module.

*///***********************************************************************************************
#ifndef NED_POS_LIST_H
#define NED_POS_LIST_H

#include "Ned.h"
#include "NedFile.h"
#include "NedList.h"
#include "NedPoint.h"

typedef struct
{
  sNedFile_t   *pFile;
  long          pos;
} sNedPos_t;

bool_t  NedPosListIsSame     (const void *pPos1, const void *pPos2);
void    NedPosListLogShow    (const void *pPos);
void    NedPosListAdjustIns  (hNedList_t hList, const sNedPoint_t *pPoint, long len);
void    NedPosListAdjustDel  (hNedList_t hList, const sNedPoint_t *pTop, const sNedPoint_t *pBot);
void    NedPosListRemoveAll  (hNedList_t hList, const sNedFile_t *pFile);

#endif // NED_POS_LIST_H
