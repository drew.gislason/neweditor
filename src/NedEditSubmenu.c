/*!************************************************************************************************
  @file NedEditSubmenu.c

  @brief  Private functions to handle dynamic submenus like File/Open->

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include "Ned.h"
#include "NedEditor.h"
#include "NedEditSubmenu.h"

// menu items that have pick submenus
sNtmCsm_t    m_FileOpen             = { NEDMENUINDEX_FILE,   NEDFILE_INDEX_FILE_OPEN };
sNtmCsm_t    m_FileOpenRecent       = { NEDMENUINDEX_FILE,   NEDFILE_INDEX_FILE_OPEN_RECENT };
sNtmCsm_t    m_OptionMapKeys        = { NEDMENUINDEX_OPTION, NEDOPTION_INDEX_MAP_KEYS };
sNtmCsm_t    m_OptionProjectFolders = { NEDMENUINDEX_OPTION, NEDOPTION_PROJECT_FOLDERS };


/*-------------------------------------------------------------------------------------------------
  Does all the common submenu stuff. Assumes strings already initialized.
  Returns TRUE if still in submenu, FALSE if not.
-------------------------------------------------------------------------------------------------*/
bool_t NtmCommonSubMenu(sNtmCsm_t *pCsm, nedKey_t key, unsigned *pSubItem)
{
  bool_t        fInSubMenu;
  unsigned      left;

  pCsm->fEnteredViaMenu = TRUE;
  if(pCsm->hPick == NULL)
  {
    // File  Edit  Search  Goto  Option 
    left = NedMenuSubMenuCol(NedEditorMenu(), pCsm->nMenu);
    pCsm->hPick = NedPickNew(2, left, pCsm->numItems, pCsm->ppStrings, 0);
    NedAssert(pCsm->hPick);
    NedPickSetColors(pCsm->hPick, NedEditorTheme()->menuText, NedEditorTheme()->menuDropText);
    NedPickLogShow(pCsm->hPick);
  }

  // on screen rows may have changed, update them if needed
  NedPickAdjust(pCsm->hPick);
  if(NedLogMaskGet() & NEDLOG_MENU)
    NedPickLogShow(pCsm->hPick);

  // feed the key into the pick window state machine. Returns FALSE if out of state machine
  pCsm->key = key;
  if(pCsm->fExitOnDel && (key == NEDKEY_DELETE || key == NEDKEY_BACKSPACE))
    key = NEDKEY_ENTER;
  fInSubMenu = NedPickFeedKey(pCsm->hPick, key, pSubItem);

  // still in menu, just draw the pick window
  if(fInSubMenu)
    NedPickDraw(pCsm->hPick);

  // left the pick window, free it and return the picked item or NEDMENU_PICK_NONE
  else
  {
    pCsm->pickedSubItem = *pSubItem;
    NedPickFree(pCsm->hPick);
    pCsm->hPick = NULL;
  }

  return fInSubMenu;
}

/*-------------------------------------------------------------------------------------------------
  If the command happened without the menu, get keys until either ENTER or ESC.
  Pick menu has been exited and freed by this point.
-------------------------------------------------------------------------------------------------*/
void NtmCommonSubMenuFeedKeys(pfnSubMenu_t pfnSubMenu, sNtmCsm_t *pCsm)
{
  nedKey_t    key;

  if(!pCsm->fEnteredViaMenu)
  {
    (pfnSubMenu)(NEDKEY_MENU_ENTER, &pCsm->pickedSubItem);
    while(1)
    {
      key = NedKeyMapTranslateKey(NedKeyGetKey());
      if(!(pfnSubMenu)(key, &pCsm->pickedSubItem))
        break;
    }
  }

  if(pCsm->hPick)
  {
    NedPickFree(pCsm->hPick);
    pCsm->hPick = NULL;
  }
  pCsm->fEnteredViaMenu = FALSE;
}

/*-------------------------------------------------------------------------------------------------
  Helps to open a file. Gets fed keys from menu system
  Returns TRUE if still in submenu, FALSE if not
-------------------------------------------------------------------------------------------------*/
bool_t NtmFileOpenSubMenu(nedKey_t key, unsigned *pSubItem)
{
  static char   szFullPath[PATH_MAX];
  unsigned      i;
  sNedFile_t   *pFileCur = NedEditorFileCur();

  NedLogPrintfEx(NEDLOG_MENU, "NtmFileOpenSubMenu(key %s, hList %p\n", NedKeyName(key), m_FileOpen.hList);

  // intializing picker
  if(m_FileOpen.hList == NULL)
  {
    // start with folder of current file
    if(m_FileOpen.pData == NULL)
    {
      strcpy(szFullPath, NedStrDirOnly(NedFilePath(pFileCur)));
      NedStrAppendName(szFullPath, "*");
    }

    // allocate/set up list and other variables
    m_FileOpen.pData      = szFullPath;
    m_FileOpen.hList      = NedFileListNew(szFullPath);
    NedAssert(m_FileOpen.hList);
    m_FileOpen.numItems   = NedFileListLenEx(m_FileOpen.hList);
    m_FileOpen.ppStrings  = malloc(m_FileOpen.numItems * sizeof(char *));
    NedAssert(m_FileOpen.ppStrings);
    for(i=0; i<m_FileOpen.numItems; ++i)
      m_FileOpen.ppStrings[i] = NedStrNameDirOk(NedFileListGetNameEx(m_FileOpen.hList, i));
  }

  return NtmCommonSubMenu(&m_FileOpen, key, pSubItem);
}

/*-------------------------------------------------------------------------------------------------
  Resets the file list (items) for File/Open-> menu
-------------------------------------------------------------------------------------------------*/
void NtmFileOpenReset(void)
{
  // force a refresh for File/Open to use folder in pFileCur
  m_FileOpen.pData = NULL;
}

/*-------------------------------------------------------------------------------------------------
  Shows recent files
  Returns TRUE if still in submenu, FALSE if not
-------------------------------------------------------------------------------------------------*/
bool_t NtmFileOpenRecentSubMenu(nedKey_t key, unsigned *pSubItem)
{
  // static const char *aszRecentFiles[] = { "Ned.h", "../docs/NedManual.md", "../test/NedTestMenu.c" };

  unsigned      i;

  NedLogPrintfEx(NEDLOG_MENU, "NtmFileOpenRecentSubMenu(key %s, ppStrings %p\n", NedKeyName(key), m_FileOpenRecent.ppStrings);

  if(m_FileOpenRecent.ppStrings == NULL)
  {
    m_FileOpenRecent.numItems = NedListNumItems(NedEditor()->hRecentFiles);
    m_FileOpenRecent.ppStrings  = malloc(m_FileOpenRecent.numItems * sizeof(char *));
    NedAssert(m_FileOpenRecent.ppStrings);
    NedListIterStart(NedEditor()->hRecentFiles);
    for(i=0; i<m_FileOpenRecent.numItems; ++i)
      m_FileOpenRecent.ppStrings[i] = NedListNext(NedEditor()->hRecentFiles);
  }

  return NtmCommonSubMenu(&m_FileOpenRecent, key, pSubItem);
}

/*-------------------------------------------------------------------------------------------------
  Handle the MapKeys Submenu pick window
-------------------------------------------------------------------------------------------------*/
bool_t NtmOptionMapKeysSubMenu(nedKey_t key, unsigned *pSubItem)
{
  static bool_t   fNeedMsg = TRUE;

  NedLogPrintfEx(NEDLOG_MENU, "NtmOptionMapKeysSubMenu(key %s, ppStrings %p\n", NedKeyName(key), m_OptionMapKeys.ppStrings);

  if(m_OptionMapKeys.ppStrings == NULL)
  {
    m_OptionMapKeys.pData     = &fNeedMsg;
    m_OptionMapKeys.ppStrings = NedCmdGetAllCommands(&m_OptionMapKeys.numItems);
  }

  if(fNeedMsg)
  {
    NedMsgPrintf("Choose a command, then choose a Key. Press ESC when done mapping keys");
    fNeedMsg = FALSE;
  }

  return NtmCommonSubMenu(&m_OptionMapKeys, key, pSubItem);
}

/*-------------------------------------------------------------------------------------------------
  Handle the ProjectFolders Submenu pick window
-------------------------------------------------------------------------------------------------*/
bool_t NtmOptionProjectFoldersSubMenu(nedKey_t key, unsigned *pSubItem)
{
  static const char szAddFolder[] = "<Add Folder>";
  hNedList_t        hList = NedEditor()->hProjectFolders;
  unsigned          numItems;
  unsigned          i = 0;

  m_OptionProjectFolders.fExitOnDel = TRUE;
  if(!m_OptionProjectFolders.ppStrings)
  {
    numItems = NedListNumItems(hList);
    m_OptionProjectFolders.numItems   = numItems + 1;
    m_OptionProjectFolders.ppStrings  = malloc((numItems + 1) * sizeof(char *));

    NedListIterStart(hList);
    for(i = 0; i<numItems; ++i)
      m_OptionProjectFolders.ppStrings[i] = NedListNext(hList);
    m_OptionProjectFolders.ppStrings[numItems] = szAddFolder;
  }

  return NtmCommonSubMenu(&m_OptionProjectFolders, key, pSubItem);
}

/*!------------------------------------------------------------------------------------------------
  Set up pointer to window submenu handling routines, like 

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorSetSubMenus(void)
{
  sNedKeyMapSubMenu_t aSubMenus[] =
  {
    { NEDMENUINDEX_FILE,   NEDFILE_INDEX_FILE_OPEN,         NtmFileOpenSubMenu },
    { NEDMENUINDEX_FILE,   NEDFILE_INDEX_FILE_OPEN_RECENT,  NtmFileOpenRecentSubMenu },
    { NEDMENUINDEX_OPTION, NEDOPTION_INDEX_MAP_KEYS,        NtmOptionMapKeysSubMenu },
    { NEDMENUINDEX_OPTION, NEDOPTION_PROJECT_FOLDERS,       NtmOptionProjectFoldersSubMenu }
  };

  NedMenuMapSetSubMenus(NedEditor()->hMenu, NumElements(aSubMenus), aSubMenus);
}

