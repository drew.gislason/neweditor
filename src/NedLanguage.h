/*!************************************************************************************************
  @file NedLanguage.h

  @brief Contains the code language API

  Copyright (c) 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_language  The API for language settings

************************************************************************************/
#ifndef NED_LANGUAGE_H
#define NED_LANGUAGE_H
#include "Ned.h"

// default languages
#define SZ_LANG_C             "C"           // .c .h .cpp .hpp
#define SZ_LANG_CPP           "C++"         // .cpp .hpp
#define SZ_LANG_HTML          "Html"        // .java
#define SZ_LANG_JAVA          "Java"        // .htm .html
#define SZ_LANG_JAVASCRIPT    "JavaScript"  // .js
#define SZ_LANG_JSON          "JSON"        // .json
#define SZ_LANG_MAKEFILE      "Makefile"    // Makefile makefile .mak"
#define SZ_LANG_PYTHON        "Python"      // .py
#define SZ_LANG_RUBY          "Ruby"        // .rb
#define SZ_LANG_RUST          "Rust"        // .rs
#define SZ_LANG_SHELL         "Shell"       // bash (.sh, .bash), zsh (.sh .zsh), DOS (.bat)
#define SZ_LANG_SWIFT         "Swift"       // .swift
#define SZ_LANG_TEXT          "Text"        // (default) text (.txt) markdown (.md .markdown)

typedef struct
{
  char              szLang[NEDCFG_MAX_LANG_CHARS];      // "lang": "C/C++",
  char              szExt[NEDCFG_MAX_LANG_CHARS];       // "ext": ".c .h .cpp .hpp",
  unsigned          tabSize;                            // "tabSize": 2,
  bool_t            fHardTabs;                          // "hardTabs": false,
  bool_t            fCrLf;                              // "crLf": false,
  long              wrapLen;                            // "wrapLen": 120,
  const char      **aszLineComments;                    // "lineComments": ["//"],
  const char      **aszBlockComments;                   // "blockComments": ["/*","*/", "#if 0", "#endif"],
  const char      **aszKeywords;                        // "keywords": ["#include","#define","if","else","for"]
} sNedLanguage_t;

void                    NedLangInit       (void);
const sNedLanguage_t   *NedLangGet        (const char *szFilename);
const sNedLanguage_t   *NedLangGetDef     (const char *szFilename);
const sNedLanguage_t   *NedLangGetNoDef   (const char *szFilename);
const sNedLanguage_t   *NedLangGetIndexed (unsigned i);
const sNedLanguage_t   *NedLangGetByName  (const char *szLang);
bool_t                  NedLangSet        (const sNedLanguage_t *pLang);
bool_t                  NedLangIsTxt      (const sNedLanguage_t *pLang);
bool_t                  NedLangIsSame     (const sNedLanguage_t *pLang1, const sNedLanguage_t *pLang2);

#endif // NED_LANGUAGE_H
