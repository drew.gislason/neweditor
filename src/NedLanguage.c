/*!************************************************************************************************
  @file NedLanguage.c

  @brief Contains code language structures and API

  This includes keywords, comments, hard or soft tabs, Lf vs CrLf, etc...

  The following languages are built-in to Ned

  1.  C
  2.  C++
  3.  HTML
  4.  Java
  5.  JavaScript
  6.  JSON
  7.  Makefile
  8.  Python
  9.  Ruby
  10. Rust
  11. Shell
  12. Swift
  13. Text

  @copyright Copyright (c) 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @ingroup    ned_language

*///***********************************************************************************************
#include "Ned.h"
#include "NedLanguage.h"
#include "NedUtilStr.h"
#include "NedLog.h"

// C Language (default)
static const char *m_aszLineCommentsC[]  = { "//", NULL };
static const char *m_aszBlockCommentsC[] = { "/*", "*/", "#if 0", "#endif", NULL };
static const char *m_aszKeywordsC[]      =
{
  "==", "=", "!=", "<=", ">=", "++", "--", "<", ">", "+=", "-=", "|", "|=", "&=", "&&", "&", "||",
  "+", "-", "%", "*", "/", "^", "#include", "#define", "#if", "#ifdef", "#else", "#elseif",
  "#endif", "typedef", "struct", "union", "enum", "if", "else", "return", "case", "switch",
  "while", "for", "goto", "break", "do", "auto", "register", "const", "volatile", "static",
  "extern", "void", "char", "unsigned", "signed", "int", "short", "long", "uint8_t", "int8_t",
  "uint16_t", "int16_t", "uint32_t", "int32_t", "uint64_t", "int64_t", "float", "double", "size_t",
  "ssize_t", "bool_t", "bool", "TRUE", "FALSE", "true", "false", NULL
};
static sNedLanguage_t m_sLangC =
{
  .szLang           = SZ_LANG_C,
  .szExt            = ".c .h",
  .tabSize          = 2,
  .fHardTabs        = FALSE,
  .fCrLf            = FALSE,
  .wrapLen          = 120,
  .aszLineComments  = m_aszLineCommentsC,
  .aszBlockComments = m_aszBlockCommentsC,
  .aszKeywords      = m_aszKeywordsC,
};

// C++ Language
static const char *m_aszLineCommentsCPP[]  = { "//", NULL };
static const char *m_aszBlockCommentsCPP[] = { "/*", "*/", "#if 0", "#endif", NULL };
static const char *m_aszKeywordsCPP[]      =
{
  "alignas", "alignof", "and", "and_eq", "asm", "atomic_cancel", "atomic_commit",
  "atomic_noexcept", "auto", "bitand", "bitor", "bool", "break", "case", "catch", "char",
  "char8_t", "char16_t", "char32_t", "class", "compl", "concept", "const", "consteval",
  "constexpr", "constinit", "const_cast", "continue", "co_await", "co_return", "co_yield",
  "decltype", "default", "delete", "do", "double", "dynamic_cast", "else", "enum", "explicit",
  "export", "extern", "false", "float", "for", "friend", "goto", "if", "inline", "int", "long",
  "mutable", "namespace", "new", "noexcept", "not", "not_eq", "nullptr", "operator", "or", "or_eq",
  "private", "protected", "public", "reflexpr", "register", "reinterpret_cast", "requires",
  "return", "short", "signed", "sizeof", "static", "static_assert", "static_cast", "struct",
  "switch", "synchronized", "template", "this", "thread_local", "throw", "true", "try", "typedef",
  "typeid", "typename", "union", "unsigned", "using", "virtual", "void", "volatile", "wchar_t",
  "while", "xor", "xor_eq", NULL
};

static sNedLanguage_t m_sLangCPP =
{
  .szLang           = SZ_LANG_CPP,
  .szExt            = ".cpp .hpp .cc .hh",
  .tabSize          = 2,
  .fHardTabs        = FALSE,
  .fCrLf            = FALSE,
  .wrapLen          = 120,
  .aszLineComments  = m_aszLineCommentsCPP,
  .aszBlockComments = m_aszBlockCommentsCPP,
  .aszKeywords      = m_aszKeywordsCPP,
};

// HTML Language
static const char *m_aszLineCommentsHtml[]  = { "<!--", NULL };
static const char *m_aszBlockCommentsHtml[] = { "<!--", "-->", NULL };
static const char *m_aszKeywordsHtml[]      =
{
  "head", "body", "content", "col", "data", "html", "img", "script", "div", "span", "name", "charset",
  "<p>", "</p>", "<h1>", "</h1>", "<h2>", "</h2>", "<h3>", "</h3>", "<a", "</a>", NULL
};
static sNedLanguage_t m_sLangHtml =
{
  .szLang           = SZ_LANG_HTML,
  .szExt            = ".html .htm",
  .tabSize          = 2,
  .fHardTabs        = FALSE,
  .fCrLf            = TRUE,
  .wrapLen          = 120,
  .aszLineComments  = m_aszLineCommentsHtml,
  .aszBlockComments = m_aszBlockCommentsHtml,
  .aszKeywords      = m_aszKeywordsHtml
};

// Java Language
static const char *m_aszLineCommentsJava[]   = { "//", NULL };
static const char *m_aszBlockCommentsJava[]  = { "/*", "*/", NULL };
static const char *m_aszKeywordsJava[]       =
{
  "abstract", "continue", "for", "new", "switch", "assert", "default", "goto", "package",
  "synchronized", "boolean", "do"  "if", "private", "this", "break", "double",  "implements",
  "protected", "throw", "byte", "else", "import",  "public",  "throws", "case",  "enum",
  "instanceof",  "return", "transient", "catch", "extends", "int", "short", "try", "char", "final",
  "interface", "static",  "void", "class", "finally", "long",  "strictfp",  "volatile", "const",
  "float", "native",  "super", "while", NULL
};
static sNedLanguage_t m_sLangJava =
{
  .szLang           = SZ_LANG_JAVA,
  .szExt            = ".java",
  .tabSize          = 4,
  .fHardTabs        = FALSE,
  .fCrLf            = FALSE,
  .wrapLen          = 72,
  .aszLineComments  = m_aszLineCommentsJava,
  .aszBlockComments = m_aszBlockCommentsJava,
  .aszKeywords      = m_aszKeywordsJava
};

// JavaScript Language
static const char *m_aszLineCommentsJavaScript[]   = { "//", NULL };
static const char *m_aszBlockCommentsJavaScript[]  = { "/*", "*/", NULL };
static const char *m_aszKeywordsJavaScript[]       =
{
  "abstract", "arguments", "await", "boolean", "break", "byte", "case", "catch", "char", "class",
  "const", "continue", "debugger", "default", "delete", "do", "double", "else", "enum", "eval",
  "export", "extends", "false", "final", "finally", "float", "for", "function", "goto", "if",
  "implements", "import", "in", "instanceof", "int", "interface", "let", "long", "native", "new",
  "null", "package", "private", "protected", "public ", "return", "short", "static", "super",
  "switch", "synchronized", "this", "throw", "throws", "transient", "true", "try", "typeof", "var",
  "void", "volatile", "while", "with", "yield", NULL
};
static sNedLanguage_t m_sLangJavaScript =
{
  .szLang           = SZ_LANG_JAVASCRIPT,
  .szExt            = ".js .css",
  .tabSize          = 2,
  .fHardTabs        = FALSE,
  .fCrLf            = FALSE,
  .wrapLen          = 120,
  .aszLineComments  = m_aszLineCommentsJavaScript,
  .aszBlockComments = m_aszBlockCommentsJavaScript,
  .aszKeywords      = m_aszKeywordsJavaScript
};

// JSON Language
static const char *m_aszLineCommentsJSON[]   = { "//", NULL };
static const char *m_aszBlockCommentsJSON[]  = { "/*", "*/", NULL };
static const char *m_aszKeywordsJSON[]       = { "true", "false", "null", NULL };
static sNedLanguage_t m_sLangJSON =
{
  .szLang           = SZ_LANG_JSON,
  .szExt            = ".json",
  .tabSize          = 2,
  .fHardTabs        = FALSE,
  .fCrLf            = FALSE,
  .wrapLen          = 120,
  .aszLineComments  = m_aszLineCommentsJSON,
  .aszBlockComments = m_aszBlockCommentsJSON,
  .aszKeywords      = m_aszKeywordsJSON
};

// Makefile Language
static const char *m_aszCommentsMakefile[]      = { "#", NULL };
static const char *m_aszBlockCommentsMakefile[] = { NULL };
static const char *m_aszKeywordsMakefile[]      = { "ifdef", "elif", "else", "endif", ".PHONY", NULL };
static sNedLanguage_t m_sLangMakefile =
{
  .szLang           = SZ_LANG_MAKEFILE,
  .szExt            = "makefile Makefile .mak",
  .tabSize          = 2,
  .fHardTabs        = TRUE,
  .fCrLf            = FALSE,
  .wrapLen          = 120,
  .aszLineComments  = m_aszCommentsMakefile,
  .aszBlockComments = m_aszBlockCommentsMakefile,
  .aszKeywords      = m_aszKeywordsMakefile
};

// Python Language
static const char *m_aszLineCommentsPython[]  = { "#", NULL };
static const char *m_aszBlockCommentsPython[] = { "\"\"\"", "\"\"\"", NULL };
static const char *m_aszKeywordsPython[]      =
{
  "as", "assert", "break", "class", "continue", "def", "del", "elif", "else", "except", "False",
  "finally", "for", "from", "global", "if", "import", "in", "is", "lambda", "None", "nonlocal",
  "not", "or", "pass", "raise", "return", "True", "try", "while", "with", "yield", NULL
};
static sNedLanguage_t m_sLangPython =
{
  .szLang           = SZ_LANG_PYTHON,
  .szExt            = ".py",
  .tabSize          = 4,
  .fHardTabs        = FALSE,
  .fCrLf            = FALSE,
  .wrapLen          = 79,   // PEP 8
  .aszLineComments  = m_aszLineCommentsPython,
  .aszBlockComments = m_aszBlockCommentsPython,
  .aszKeywords      = m_aszKeywordsPython
};

// Ruby Language, see https://rubystyle.guide
static const char *m_aszLineCommentsRuby[]  = { "#", NULL };
static const char *m_aszBlockCommentsRuby[] = { "=begin", "=end", NULL };
static const char *m_aszKeywordsRuby[]      =
{
  "BEGIN", "END", "alias", "and", "begin", "break", "case", "class", "def", "defined?", "do",
  "else", "elsif", "end", "ensure", "false", "for", "if", "in", "module", "next", "nil", "not",
  "or", "redo", "rescue", "retry", "return", "self", "super", "then", "true", "undef", "unless",
  "until", "when", "while", "yield", NULL
};
static sNedLanguage_t m_sLangRuby =
{
  .szLang         = SZ_LANG_RUBY,
  .szExt          = ".rb",
  .tabSize        = 2,
  .fHardTabs      = FALSE,
  .fCrLf          = FALSE,
  .wrapLen        = 80,
  .aszLineComments  = m_aszLineCommentsRuby,
  .aszBlockComments = m_aszBlockCommentsRuby,
  .aszKeywords      = m_aszKeywordsRuby
};

// Rust Language
static const char *m_aszLineCommentsRust[]  = { "//", NULL };
static const char *m_aszBlockCommentsRust[] = { "/*", "*/", NULL };
static const char *m_aszKeywordsRust[]      =
{
  "=", "==", "=>", "<=", ">=", "++", "--", "<", ">", "+=", "-=", "|=", "&", "&=", "&&", "||", "::",
  "u8", "i8", "u16", "i16", "u32", "i32", "u64", "i64", "u128", "i128", "usize", "isize", "f32", "f64",
  "as", "break", "const", "continue", "crate", "else", "enum", "extern", "false", "fn", "for",
  "if", "impl", "in", "let", "loop", "match", "mod", "move", "mut", "pub", "ref", "return", "self",
  "Self", "static", "struct", "super", "trait", "true", "type", "unsafe", "use", "where", "while",
  "async", "await", "dyn", NULL
};
static sNedLanguage_t m_sLangRust =
{
  .szLang         = SZ_LANG_RUST,
  .szExt          = ".rs",
  .tabSize        = 4,
  .fHardTabs      = FALSE,
  .fCrLf          = FALSE,
  .wrapLen        = 100,    // see 
  .aszLineComments  = m_aszLineCommentsRust,
  .aszBlockComments = m_aszBlockCommentsRust,
  .aszKeywords      = m_aszKeywordsRust
};

// Shell (e.g. bash, zsh) Language
static const char *m_aszLineCommentsShell[]  = { "#", NULL };
static const char *m_aszBlockCommentsShell[] = { NULL };
static const char *m_aszKeywordsShell[]      =
{
  "echo", "read", "set", "unset", "readonly", "shift", "export", "if", "fi", "else", "while", "do", "done", 
  "for", "until", "case", "esac", "break", "continue", "exit", "return", "trap", "wait", "eval", "exec", 
  "ulimit", "umask", NULL
};
static sNedLanguage_t m_sLangShell =
{
  .szLang         = SZ_LANG_SHELL,
  .szExt          = ".sh .bash .zsh .bat",
  .tabSize        = 2,
  .fHardTabs      = FALSE,
  .fCrLf          = FALSE,
  .wrapLen        = 120,
  .aszLineComments  = m_aszLineCommentsShell,
  .aszBlockComments = m_aszBlockCommentsShell,
  .aszKeywords      = m_aszKeywordsShell
};

// Swift Programming Language, see https://docs.swift.org/swift-book/ReferenceManual/LexicalStructure.html
static const char *m_aszLineCommentsSwift[]  = { "//", NULL };
static const char *m_aszBlockCommentsSwift[] = { "/*", "*/", NULL };
static const char *m_aszKeywordsSwift[]      =
{
  "associatedtype", "class", "deinit", "enum", "extension", "fileprivate", "func", "import",
  "init", "inout", "internal", "let", "open", "operator", "private", "precedencegroup", "protocol",
  "public", "rethrows", "static", "struct", "subscript", "typealias", "var", "break", "case",
  "catch", "continue", "default", "defer", "do", "else", "fallthrough", "for", "guard", "if", "in",
  "repeat", "return", "throw", "switch", "where", "while", "Any", "as", "catch", "false", "is",
   "nil", "rethrows", "self", "Self", "super", "throw", "throws", "true", "try", "#available",
   "#colorLiteral", "#column", "#dsohandle", "#elseif", "#else", "#endif", "#error", "#fileID",
   "#fileLiteral", "#filePath", "#file", "#function", "#if", "#imageLiteral", "#keyPath", "#line",
   "#selector", "associativity", "convenience", "didSet", "dynamic", "final", "get", "indirect",
   "infix", "lazy", "left", "mutating", "none", "nonmutating", "optional", "override", "postfix",
   "precedence", "prefix", "Protocol", "required", "right", "set", "some", "Type", "unowned",
   "weak", "willSet", NULL
};
static sNedLanguage_t m_sLangSwift =
{
  .szLang         = SZ_LANG_SWIFT,
  .szExt          = ".swift",
  .tabSize        = 2,
  .fHardTabs      = FALSE,
  .fCrLf          = FALSE,
  .wrapLen        = 120,
  .aszLineComments  = m_aszLineCommentsSwift,
  .aszBlockComments = m_aszBlockCommentsSwift,
  .aszKeywords      = m_aszKeywordsSwift
};

// Text and Markdown Language
static const char *m_aszLineCommentsText[]   = { "#", NULL };
static const char *m_aszBlockCommentsText[]  = { "```", "```", "`", "`", NULL };
static const char *m_aszKeywordsText[]       = { NULL };
static sNedLanguage_t m_sLangText =
{
  .szLang           = SZ_LANG_TEXT,
  .szExt            = ".txt .md .markdown",
  .tabSize          = 2,
  .fHardTabs        = FALSE,
  .fCrLf            = FALSE,
  .wrapLen          = 100,
  .aszLineComments  = m_aszLineCommentsText,
  .aszBlockComments = m_aszBlockCommentsText,
  .aszKeywords      = m_aszKeywordsText
};

static const sNedLanguage_t *apDefLangs[] = 
{
  &m_sLangC, &m_sLangCPP, &m_sLangHtml, &m_sLangJava, &m_sLangJavaScript, &m_sLangJSON,
  &m_sLangMakefile, &m_sLangPython, &m_sLangRuby, &m_sLangRust, &m_sLangShell, &m_sLangSwift,
  &m_sLangText
};
static sNedLanguage_t aLangs[NEDCFG_MAX_LANG];

/*-------------------------------------------------------------------------------------------------
  Look for the language structure to used based on file extension.
  szExt may be like ".c" or NULL. szExtList is like ".c .h .cpp .hpp" or "."
-------------------------------------------------------------------------------------------------*/
static bool_t NlIsSameExt(const char *szName, const char *szExtList)
{
  char          szThisExt[NEDCFG_MAX_LANG_CHARS];
  const char   *psz;
  const char   *szExt = NedStrExt(szName);
  bool_t        fIsSame = FALSE;

  psz = szExtList;
  while(psz && *psz)
  {
    psz = NedStrArgCpy(szThisExt, psz, sizeof(szThisExt));
    if(!szExt)
    {
      if(NedMemICmp(szThisExt, szName, strlen(szName) + 1) == 0)
      {
        fIsSame = TRUE;
        break;
      }
    }
    else if(NedMemICmp(szThisExt, szExt, strlen(szExt) + 1) == 0)
    {
      fIsSame = TRUE;
      break;
    }
  }

  return fIsSame;
}

/*-------------------------------------------------------------------------------------------------
  Return TRUE if we've reached a blank entry
-------------------------------------------------------------------------------------------------*/
bool_t NlIsEmpty(const sNedLanguage_t *pLang)
{
  return (pLang->szLang[0] == '\0') ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Initialize (or reinitialize) languages from defaults
  @return     none
*///-----------------------------------------------------------------------------------------------
void NedLangInit(void)
{
  unsigned    i;
  unsigned    nDefLangs = NumElements(apDefLangs);
  unsigned    nMaxLangs = NumElements(aLangs);

  NedAssert(nMaxLangs >= nDefLangs);

  memset(aLangs, 0, sizeof(aLangs));
  for(i = 0; i < nDefLangs; ++i)
    memcpy(&aLangs[i], apDefLangs[i], sizeof(aLangs[0]));
}

/*!------------------------------------------------------------------------------------------------
  Is this a text or programming language? Special rules.
  @param      pLanguage    pointer to language
  @return     none
*///-----------------------------------------------------------------------------------------------
bool_t NedLangIsTxt(const sNedLanguage_t *pLanguage)
{
  return (strcmp(pLanguage->szLang, m_sLangText.szLang) == 0) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Get a pointer to the language info based on filename extensionlike .c or .py.

  @param      szFilename    pointer to filename string (can be full path)
  @return     ptr to language, or default if not found
*///-----------------------------------------------------------------------------------------------
const sNedLanguage_t * NedLangGet(const char *szFilename)
{
  const char              szDefName[] = "x.txt";
  const sNedLanguage_t   *pLang;

  pLang = NedLangGetNoDef(szFilename);
  if(pLang == NULL)
    pLang = NedLangGetNoDef(szDefName);
  NedAssert(pLang);

  return pLang;
}

/*!------------------------------------------------------------------------------------------------
  Get one of the default languages, based on filename. Note: this points to the CONST language
  array, not the one in the RAM array, so it will be a different ptr address.

  @param      szFilename    pointer to filename string (can be full path)
  @return     ptr to language or NULL if not found
*///-----------------------------------------------------------------------------------------------
const sNedLanguage_t * NedLangGetDef(const char *szFilename)
{
  const sNedLanguage_t *pLang = NULL;
  const char           *szName = NedStrNameOnly(szFilename);
  unsigned              i;

  for(i = 0; i < NumElements(apDefLangs); ++i)
  {
    if(NlIsSameExt(szName, apDefLangs[i]->szExt))
    {
      pLang = apDefLangs[i];
      break;
    }
  }

  return pLang;
}

/*!------------------------------------------------------------------------------------------------
  Get a pointer to the language info based on filename extention (like .c or .py). If the extension
  is not recognized, return NULL.

  @param      szFilename    pointer to filename string (can be full path)
  @return     ptr to language or NULL if not found
*///-----------------------------------------------------------------------------------------------
const sNedLanguage_t * NedLangGetNoDef(const char *szFilename)
{
  const sNedLanguage_t *pLang = NULL;
  const char           *szName = NedStrNameOnly(szFilename);
  unsigned              i;

  // first try the loaded languages
  for(i = 0; i < NumElements(aLangs) && *aLangs[i].szExt; ++i)
  {
    if(NlIsEmpty(&aLangs[i]))
      break;
    if(NlIsSameExt(szName, aLangs[i].szExt))
    {
      pLang = &aLangs[i];
      break;
    }
  }

  return pLang;
}

/*!------------------------------------------------------------------------------------------------
  Get a pointer to the language based on index (0-n). Useful to iterate through defined languages.

  @param      n    index into languages
  @return     ptr to language, NULL if no more languages
*///-----------------------------------------------------------------------------------------------
const sNedLanguage_t * NedLangGetIndexed(unsigned index)
{
  const sNedLanguage_t *pLang = NULL;
  unsigned        i;

  // first look through modified languages
  for(i=0; i<NumElements(aLangs); ++i)
  {
    if(NlIsEmpty(&aLangs[i]))
      break;
    else
    {
      if(index == 0)
      {
        pLang = &aLangs[i];
        break;
      }
      --index;
    }
  }

  return pLang;
}

/*!------------------------------------------------------------------------------------------------
  Get a pointer to the language based on name. Returns NULL if language name not found

  @param      n    index into languages
  @return     ptr to language, NULL if no more languages
*///-----------------------------------------------------------------------------------------------
const sNedLanguage_t *NedLangGetByName(const char *szLang)
{
  const sNedLanguage_t *pLang = NULL;
  unsigned        i;

  // first look through modified languages
  for(i=0; i<NumElements(aLangs); ++i)
  {
    if(NlIsEmpty(&aLangs[i]))
      break;
    else
    {
      if(strcmp(szLang, aLangs[i].szLang) == 0)
      {
        pLang = &aLangs[i];
        break;
      }
    }
  }

  return pLang;
}

/*!------------------------------------------------------------------------------------------------
  Sets this language as one of the known languages. Copies the language into internal variables.

  @param    pLang1    ptr to valid language structure
  @param    pLang2    ptr to valid language structure
  @return   TRUE if worked, FALSE if failed
*///-----------------------------------------------------------------------------------------------
bool_t NedLangIsSame(const sNedLanguage_t *pLang1, const sNedLanguage_t *pLang2)
{
  bool_t    fIsSame = TRUE;

  if(!pLang1 || !pLang2)
    fIsSame = FALSE;
  else
  {
    if(strcmp(pLang1->szLang, pLang2->szLang) != 0)
      fIsSame = FALSE;
    if(strcmp(pLang1->szExt, pLang2->szExt) != 0)
      fIsSame = FALSE;
    if(pLang1->tabSize != pLang2->tabSize)
      fIsSame = FALSE;
    if(pLang1->fHardTabs != pLang2->fHardTabs)
      fIsSame = FALSE;
    if(pLang1->fCrLf != pLang2->fCrLf)
      fIsSame = FALSE;
    if(pLang1->wrapLen != pLang2->wrapLen)
      fIsSame = FALSE;
    if(NedUtilStrArrayCmp(pLang1->aszLineComments, pLang2->aszLineComments) != 0)
      fIsSame = FALSE;
    if(NedUtilStrArrayCmp(pLang1->aszBlockComments, pLang2->aszBlockComments) != 0)
      fIsSame = FALSE;
    if(NedUtilStrArrayCmp(pLang1->aszKeywords, pLang2->aszKeywords) != 0)
      fIsSame = FALSE;
  }

  return fIsSame;
}

/*!------------------------------------------------------------------------------------------------
  Sets language variables. This will overwrite a language if the same name is used, otherwise it's
  considered a new language

  Note: pLang->aszLineComments, pLang->aszBlockComments and pLang->aszKeywords must be persistant.
  Only the pointers those arrays of strings are copied. The Arrays of strings must be NULL
  terminated.

  @param    pLang       language structure, filled out
  @return   TRUE if worked, FALSE if failed (bad parm or out of entries)
*///-----------------------------------------------------------------------------------------------
bool_t NedLangSet(const sNedLanguage_t *pLang)
{
  unsigned        i;
  unsigned        slot  = UINT_MAX;

  // validate parameters
  if(!pLang || (*pLang->szLang == '\0') || (*pLang->szExt == '\0'))
    return FALSE;

  // find slot for language, may be none. Prefers to overwrite the same language
  for(i=0; i < NumElements(aLangs); ++i)
  {
    if(strcmp(pLang->szLang, aLangs[i].szLang) == 0)
    {
      slot = i;
      break;
    }
    else if(NlIsEmpty(&aLangs[i]))
    {
      slot = i;
      break;
    }
  }

  // no slot available, hit our limit of languages
  if(slot == UINT_MAX)
    return FALSE;

  // we found a slot in the table to put this language structure
  memcpy(aLangs[slot].szLang, pLang->szLang, NEDCFG_MAX_LANG_CHARS);
  memcpy(aLangs[slot].szExt, pLang->szExt, NEDCFG_MAX_LANG_CHARS);
  aLangs[slot].tabSize    = pLang->tabSize;
  aLangs[slot].fHardTabs  = pLang->fHardTabs;
  aLangs[slot].fCrLf      = pLang->fCrLf;
  aLangs[slot].wrapLen    = pLang->wrapLen;
  aLangs[slot].aszLineComments  = pLang->aszLineComments;
  aLangs[slot].aszBlockComments = pLang->aszBlockComments;
  aLangs[slot].aszKeywords      = pLang->aszKeywords;

  return TRUE;
}
