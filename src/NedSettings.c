/*!************************************************************************************************
  @file NedSettings.c

  @brief  NED SETTINGS - Implementation that maps Ned structurs to settings and vice versa.

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_settings NED_SETTINGS module is where all settings are saved from in-memory
  editor structure to a file, and read from a file into editor memory structure.
*///***********************************************************************************************
#include <limits.h>
#include "Ned.h"
#include "NedTheme.h"
#include "NedUtilFile.h"
#include "NedJson2.h"
#include "NedManual.h"
#include "NedPosList.h"
#include "NedSettings.h"

#include "NedLog.h"

typedef struct
{
  char         *szFile;
  long          pos;            // cursor position in file
  long          topLine;        // 1-n, line at top of screen
  long          leftCol;        // 0-n, if line is long, screen may have scrolled to left
  long          prefCol;        // 0-n, preferred column (e.g. up/down arrows stay on column)
  bool_t        fRdOnly;        // file is read only
} sSetFile_t;

typedef struct
{
  unsigned      idx;
  long          pos;
} sSetPosPair_t;

typedef struct
{
  const char    *szKey;
  const char    *szCmd;
} sSetKeyMap_t;

typedef struct
{
  unsigned          nedVersion;
  unsigned          curFile;
  char             *aszRecentFiles    [NEDCFG_MAX_RECENT];
  char             *aszProjectFolders [NEDCFG_MAX_RECENT];
  char             *szTheme;
  sSetFile_t        aOpenFiles        [NEDCFG_MAX_FILES];
  sSetPosPair_t     aBookmarks        [NEDCFG_MAX_HISTORY];
  sSetKeyMap_t      aKeyMap           [NEDCFG_MAX_KEYMAP];
  sNedLanguage_t   *apLang            [NEDCFG_MAX_LANG];
} sNedSettings_t;

#define SET_IDX_NULL    UINT_MAX
#define SET_POS_NULL    LONG_MAX

// pseudo file
const char      szNedManualPath[]       = "~/NedManual.md";

// JSON keys for ned.json settings file
const char      m_szKeyNedVersion[]     = "nedVersion";
const char      m_szKeyCurFile[]        = "curFile";
const char      m_szKeyRecentFiles[]    = "recentFiles";
const char      m_szKeyProjectFolders[] = "projectFolders";
const char      m_szKeyTheme[]          = "theme";
const char      m_szKeyUserThemes[]     = "userThemes";
const char      m_szKeyOpenFiles[]      = "openFiles";
const char      m_szKeyBookmarks[]      = "bookmarks";
const char      m_szKeyKeyMap[]         = "keyMap";
const char      m_szKeyLanguage[]       = "language";
const char      m_szKeyIdx[]            = "idx";
const char      m_szKeyPos[]            = "pos";
const char      m_szKeyKey[]            = "key";
const char      m_szKeyCmd[]            = "cmd";
const char      m_szKeyName[]           = "name";
const char      m_szKeyColors[]         = "colors";
const char      m_szKeyExt[]            = "ext";
const char      m_szKeyTabSize[]        = "tabSize";
const char      m_szKeyHardTabs[]       = "hardTabs";
const char      m_szKeyCrLf[]           = "crLf";
const char      m_szKeyWrapLen[]        = "wrapLen";
const char      m_szKeyFile[]           = "file";
const char      m_szKeyTopLine[]        = "topLine";
const char      m_szKeyLeftCol[]        = "leftCol";
const char      m_szKeyPrefCol[]        = "prefCol";
const char      m_szKeyRdOnly[]         = "rdOnly";
const char      m_szKeyLang[]           = "lang";
const char      m_szKeyLineComments[]   = "lineComments";
const char      m_szKeyBlockComments[]  = "blockComments";
const char      m_szKeyKeywords[]       = "keywords";

/*-------------------------------------------------------------------------------------------------
  String Compare function for tests.
-------------------------------------------------------------------------------------------------*/
bool_t NedSettingsStrIsSame(const void *sz1, const void *sz2)
{
  return (strcmp(sz1, sz2) == 0) ? TRUE : FALSE;
}

/*-------------------------------------------------------------------------------------------------
  String Show function for tests.
-------------------------------------------------------------------------------------------------*/
void NedSettingsStrShow(const void *ptr)
{
  NedLogPrintf("%s", (const char *)ptr);
}

/*-------------------------------------------------------------------------------------------------
  Get pointer to file by index and fileList. Returns NULL if index too large.
-------------------------------------------------------------------------------------------------*/
static sNedFile_t * NsGetFileByIndex(sNedFile_t *pFileList, unsigned index)
{
  sNedFile_t     *pFile = pFileList;

  while(pFile && index)
  {
    pFile = pFile->pNext;
    --index;
  }

  return pFile;
}

/*-------------------------------------------------------------------------------------------------
  Load a set of (pFile, pos) sSetPosPair_t into this list of sNedPos_t.
------------------------------------------------------------------------------------------------*/
static void NsLoadPos(hNedList_t hList, sNedFile_t *pFileList, sSetPosPair_t *pPosPair)
{
  unsigned        i;
  sNedFile_t     *pFile;
  sNedPos_t       sPos;

  for(i=0; i < NEDCFG_MAX_HISTORY; ++i)
  {
    // stop at first invalid file index
    pFile = NsGetFileByIndex(pFileList, pPosPair[i].idx);
    // NedLogPrintf("NsLoadPos [%u]: idx %u, pFile %p\n", i, pPosPair[i].idx, pFile);
    if(!pFile)
      break;

    sPos.pFile  = pFile;
    sPos.pos    = pPosPair[i].pos;
    NedListAdd(hList, &sPos, 0);
  }
}

/*-------------------------------------------------------------------------------------------------
  Get history items (points) into {idx,pos} sSetPosPair_t pairs.
------------------------------------------------------------------------------------------------*/
static void NsGetHistoryItems(sSetPosPair_t aPosPair[], sNedFile_t *pFileList, hNedList_t hList)
{
  unsigned      i;
  unsigned      idx;
  sNedPos_t    *pPos;

  NedLogPrintfEx(NEDLOG_SETTINGS, "NsGetHistoryItems(hList=%p\n", hList);
  if(NedLogMaskGet() & NEDLOG_SETTINGS)
    NedListLogShow(hList, TRUE);

  NedListIterStart(hList);
  for(i = 0; i < NEDCFG_MAX_HISTORY && i < NedListNumItems(hList); ++i)
  {
    pPos  = NedListNext(hList);
    idx   = NedFileIndex(pFileList, &pPos->pFile->sCursor);
    NedLogPrintfEx(NEDLOG_SETTINGS, "  Got Pos[%u] {%p, %ld} idx %u\n", i, pPos->pFile, pPos->pos, idx);
    NedAssert(idx != SET_IDX_NULL);
    aPosPair[i].idx = idx;
    aPosPair[i].pos = pPos->pos;
  }

  // set the last entry to "NULL"
  if(i < NEDCFG_MAX_HISTORY)
  {
    aPosPair[i].idx = SET_IDX_NULL;
    aPosPair[i].pos = SET_POS_NULL;
  }
}

/*-------------------------------------------------------------------------------------------------
  Given an array of pointers to JSON strings (e.g. points to the quote of "hello"), adds each to
  the list
-------------------------------------------------------------------------------------------------*/
static void NsLoadList(hNedList_t hList, char *aszJsonStr[], unsigned maxItems)
{
  unsigned  i;
  unsigned  len;
  char      sz[PATH_MAX];

  // NedLogPrintfEx(NEDLOG_SETTINGS, "NsLoadList()\n");
  for(i = 0; i < maxItems && (aszJsonStr[i] != NULL); ++i)
  {
    // NedLogPrintfEx(NEDLOG_SETTINGS, "  aszStrings[%u]=%.8s\n", i, aszStrings[i]);
    len = NedJsonStrNCpy(sz, aszJsonStr[i], sizeof(sz) - 1);
    if(len)
    {
      if(!NedListAdd(hList, sz, len+1))
        break;
      NedLogPrintfEx(NEDLOG_SETTINGS, "NsLoadList[%u] %s, len %u\n", i, NedStrTmpBuf(), len);
    }
  }
}

/*-------------------------------------------------------------------------------------------------
  Get list items into an array of string pointers, in most to least recent order
-------------------------------------------------------------------------------------------------*/
static void NsGetListItems(char *aszPtrs[], hNedList_t hList)
{
  unsigned i;

  NedListIterStart(hList);
  for(i=0; i < NedListNumItems(hList) && i < NEDCFG_MAX_RECENT; ++i)
  {
    aszPtrs[i] = (void *)NedListNext(hList);
    NedLogPrintfEx(NEDLOG_SETTINGS, "NsGetListItems[%u] %s\n", i, aszPtrs[i]);
  }
}

/*-------------------------------------------------------------------------------------------------
  Load the keymap from the JSON strings in the pKeyMap structure (e.g. strings point to the quote
  of "Ctrl-A"
  "keyMap": [ {"key": "Fn2", "cmd": "HelpAbout" } ],

-------------------------------------------------------------------------------------------------*/
static void NsLoadKeyMap(sSetKeyMap_t *pKeyMap, unsigned maxEntries)
{
  char    szKeyName[128];
  char    szCmdName[128];

  unsigned  i;

  for(i = 0; i < maxEntries; ++i)
  {
    if(pKeyMap[i].szKey == NULL || pKeyMap[i].szCmd == NULL)
      break;

    NedJsonStrNCpy(szKeyName, pKeyMap[i].szKey, sizeof(szKeyName) - 1);
    NedJsonStrNCpy(szCmdName, pKeyMap[i].szCmd, sizeof(szCmdName) - 1);
    NedLogPrintfEx(NEDLOG_SETTINGS, "NsLoadKeymap[%u] szKeyName %s, szCmdName %s\n", i, szKeyName, szCmdName);
    NedKeyMapSetCommand(szKeyName, szCmdName);
  }
}

/*-------------------------------------------------------------------------------------------------
  put the string array into this key. Example: "key":["a","b","c"]. Returns total length of keyed
  array
-------------------------------------------------------------------------------------------------*/
static size_t NsPutStrArray(hNedJson_t hJson, const char szKey[], const char *aStr[])
{
  size_t    len = 0;
  unsigned  i;

  NedAssert(szKey && aStr);

  len += NedJsonPut(hJson, szKey, NEDJSON_ARRAY, NULL);
  for(i = 0; aStr[i]; ++i)
    len += NedJsonPutScalar(hJson, NEDJSON_STRING, aStr[i]);
  len += NedJsonPutEnd(hJson, NEDJSON_ARRAY);

  return len;
}

/*-------------------------------------------------------------------------------------------------
  Encode from settings to JSON form

  @param    szJson      string to receive encoded JSON, or NULL to get size only
  @param    mazSize     sizeof(szJson)
  @param    pSettings   pointer to settings structure to encode into JSON
  @param    fPretty     make the JSON pretty (with indents)
  @return   size of 
-------------------------------------------------------------------------------------------------*/
size_t NsJsonEncode(char *szJson, size_t maxSize, const sNedSettings_t *pSettings, bool_t fPretty)
{
  hNedJson_t              hJson   = NULL;
  size_t                  len     = 0;
  unsigned                i;
  long                    number;
  char                    szFilename[NEDCFG_MAX_LANG_CHARS];
  const sSetFile_t       *pSetFile;
  const sNedLanguage_t   *pLang;
  const sNedLanguage_t   *pLangDef;
  nedTheme_t             *pTheme;
  const char             *szName;

  hJson = NedJsonNew(szJson, SIZE_MAX, fPretty);
  if(NedJsonIsHandle(hJson))
  {
    len   += NedJsonPutBegin(hJson, NEDJSON_OBJ);
    number = pSettings->nedVersion;
    len   += NedJsonPut(hJson, m_szKeyNedVersion, NEDJSON_NUMBER, &number);
    number = pSettings->curFile;
    len   += NedJsonPut(hJson, m_szKeyCurFile, NEDJSON_NUMBER, &number);

    // recent files
    len += NedJsonPut(hJson, m_szKeyRecentFiles, NEDJSON_ARRAY, NULL);
    for(i = 0; i < NumElements(pSettings->aszRecentFiles) && pSettings->aszRecentFiles[i] != NULL; ++i)
    {
      len += NedJsonPutScalar(hJson, NEDJSON_STRING, pSettings->aszRecentFiles[i]);
    }
    len += NedJsonPutEnd(hJson, NEDJSON_ARRAY);
    
    // project folders 
    len += NedJsonPut(hJson, m_szKeyProjectFolders, NEDJSON_ARRAY, NULL);
    for(i = 0; i < NumElements(pSettings->aszProjectFolders) && pSettings->aszProjectFolders[i] != NULL; ++i)
    {
      len += NedJsonPutScalar(hJson, NEDJSON_STRING, pSettings->aszProjectFolders[i]);
    }
    len += NedJsonPutEnd(hJson, NEDJSON_ARRAY);

    // theme
    len += NedJsonPut(hJson, m_szKeyTheme, NEDJSON_STRING, pSettings->szTheme);

    // userTheme
    len += NedJsonPut(hJson, m_szKeyUserThemes, NEDJSON_ARRAY, NULL);
    for(i = 0; i < NEDCFG_THEME_MAX; ++i)
    {
      pTheme = NedThemeGet(i);
      if(pTheme && !NedThemeIsDefault(pTheme))
      {
        len += NedJsonPutBegin(hJson, NEDJSON_OBJ);
        szName = NedThemeGetName(i);
        // NedLogPrintf("NsJsonEncode theme %s\n", szName);
        len += NedJsonPut(hJson, m_szKeyName, NEDJSON_STRING, szName);
        len += NedJsonPut(hJson, m_szKeyColors, NEDJSON_ARRAY, NULL);
        number = pTheme-> text;
        len += NedJsonPutScalar(hJson, NEDJSON_NUMBER, &number);
        number = pTheme->textHighlight;
        len += NedJsonPutScalar(hJson, NEDJSON_NUMBER, &number);
        number = pTheme->comment;
        len += NedJsonPutScalar(hJson, NEDJSON_NUMBER, &number);
        number = pTheme->keyword;
        len += NedJsonPutScalar(hJson, NEDJSON_NUMBER, &number);
        number = pTheme->constant;
        len += NedJsonPutScalar(hJson, NEDJSON_NUMBER, &number);
        number = pTheme->menuText;
        len += NedJsonPutScalar(hJson, NEDJSON_NUMBER, &number);
        number = pTheme->menuHotkey;
        len += NedJsonPutScalar(hJson, NEDJSON_NUMBER, &number);
        number = pTheme->menuDropText;
        len += NedJsonPutScalar(hJson, NEDJSON_NUMBER, &number);
        number = pTheme->menuDropHotkey;
        len += NedJsonPutScalar(hJson, NEDJSON_NUMBER, &number);
        number = pTheme->message;
        len += NedJsonPutScalar(hJson, NEDJSON_NUMBER, &number);
        len += NedJsonPutEnd(hJson, NEDJSON_ARRAY);
        len += NedJsonPutEnd(hJson, NEDJSON_OBJ);
      }
    }
    len += NedJsonPutEnd(hJson, NEDJSON_ARRAY);

    // open files
    len += NedJsonPut(hJson, m_szKeyOpenFiles, NEDJSON_ARRAY, NULL);
    for(i = 0; i < NumElements(pSettings->aOpenFiles) && pSettings->aOpenFiles[i].szFile; ++i)
    {
      pSetFile = &pSettings->aOpenFiles[i];
      len += NedJsonPutBegin(hJson, NEDJSON_OBJ);
      len += NedJsonPut(hJson, m_szKeyFile,    NEDJSON_STRING, pSetFile->szFile);
      len += NedJsonPut(hJson, m_szKeyPos,     NEDJSON_NUMBER, &pSetFile->pos);
      len += NedJsonPut(hJson, m_szKeyTopLine, NEDJSON_NUMBER, &pSetFile->topLine);
      len += NedJsonPut(hJson, m_szKeyLeftCol, NEDJSON_NUMBER, &pSetFile->leftCol);
      len += NedJsonPut(hJson, m_szKeyPrefCol, NEDJSON_NUMBER, &pSetFile->prefCol);
      len += NedJsonPut(hJson, m_szKeyRdOnly,  NEDJSON_BOOL,   &pSetFile->fRdOnly);
      len += NedJsonPutEnd(hJson, NEDJSON_OBJ);
    }
    len += NedJsonPutEnd(hJson, NEDJSON_ARRAY);

    // bookmarks
    len += NedJsonPut(hJson, m_szKeyBookmarks, NEDJSON_ARRAY, NULL);
    for(i = 0; i < NumElements(pSettings->aBookmarks) && pSettings->aBookmarks[i].idx != SET_IDX_NULL; ++i)
    {
      len += NedJsonPutBegin(hJson, NEDJSON_OBJ);
      len += NedJsonPut(hJson, m_szKeyIdx, NEDJSON_NUMBER, &pSettings->aBookmarks[i].idx);
      len += NedJsonPut(hJson, m_szKeyPos, NEDJSON_NUMBER, &pSettings->aBookmarks[i].pos);
      len += NedJsonPutEnd(hJson, NEDJSON_OBJ);
    }
    len += NedJsonPutEnd(hJson, NEDJSON_ARRAY);

    // keymap
    len += NedJsonPut(hJson, m_szKeyKeyMap, NEDJSON_ARRAY, NULL);
    for(i = 0; i < NumElements(pSettings->aKeyMap) && pSettings->aKeyMap[i].szKey != NULL; ++i)
    {
      len += NedJsonPutBegin(hJson, NEDJSON_OBJ);
      len += NedJsonPut(hJson, m_szKeyKey, NEDJSON_STRING, pSettings->aKeyMap[i].szKey);
      len += NedJsonPut(hJson, m_szKeyCmd, NEDJSON_STRING, pSettings->aKeyMap[i].szCmd);
      len += NedJsonPutEnd(hJson, NEDJSON_OBJ);
    }
    len += NedJsonPutEnd(hJson, NEDJSON_ARRAY);

    // language
    len += NedJsonPut(hJson, m_szKeyLanguage, NEDJSON_ARRAY, NULL);
    for(i = 0; i < NumElements(pSettings->apLang); ++i)
    {
      pLang = pSettings->apLang[i];
      if(!pLang)
        break;

      len += NedJsonPutBegin(hJson, NEDJSON_OBJ);
      len += NedJsonPut(hJson, m_szKeyLang, NEDJSON_STRING, pLang->szLang);
      len += NedJsonPut(hJson, m_szKeyExt,  NEDJSON_STRING, pLang->szExt);
      number = pLang->tabSize;
      len += NedJsonPut(hJson, m_szKeyTabSize,  NEDJSON_NUMBER, &number);
      len += NedJsonPut(hJson, m_szKeyHardTabs, NEDJSON_BOOL,   &pLang->fHardTabs);
      len += NedJsonPut(hJson, m_szKeyCrLf,     NEDJSON_BOOL,   &pLang->fCrLf);
      len += NedJsonPut(hJson, m_szKeyWrapLen,  NEDJSON_NUMBER, &pLang->wrapLen);

      // optionally write the string arrays (only if different from hard-coded defaults)
      NedStrArgCpy(szFilename, pLang->szExt, sizeof(szFilename));
      pLangDef = NedLangGetDef(szFilename);
      if(!pLangDef || (NedUtilStrArrayCmp(pLang->aszLineComments, pLangDef->aszLineComments) != 0))
        len += NsPutStrArray(hJson, m_szKeyLineComments, pLang->aszLineComments);
      if(!pLangDef || (NedUtilStrArrayCmp(pLang->aszBlockComments, pLangDef->aszBlockComments) != 0))
        len += NsPutStrArray(hJson, m_szKeyBlockComments, pLang->aszBlockComments);
      if(!pLangDef || (NedUtilStrArrayCmp(pLang->aszKeywords, pLangDef->aszKeywords) != 0))
        len += NsPutStrArray(hJson, m_szKeyKeywords, pLang->aszKeywords);

      len += NedJsonPutEnd(hJson, NEDJSON_OBJ);
    }

    len += NedJsonPutEnd(hJson, NEDJSON_ARRAY);
    len += NedJsonPutEnd(hJson, NEDJSON_OBJ);

    NedJsonFree(hJson);
  }

  return len;
}

/*-------------------------------------------------------------------------------------------------
  Decode a JSON file object into the settings structure

  Example: {"file":"file1.c","pos":99,"topLine":1,"leftCol":0,"prefCol":16,"rdOnly":false}

  @param    pSetFile    pointer to sSetFile_t to be filled in
  @param    szObj       JSON file object
  @return   TRUE if worked, FALSE if not a file object
-------------------------------------------------------------------------------------------------*/
bool_t NsDecodeObjFile(sSetFile_t *pSetFile, const char *szObj)
{
  const char     *szKey;
  const char     *szValue;
  size_t          i;
  size_t          numKeys;
  nedJsonType_t   type;

  memset(pSetFile, 0, sizeof(*pSetFile));
  numKeys = NedJsonGetCount(szObj);
  for(i = 0; i < numKeys; ++i)
  {
    // get the key/value pair
    szKey = NedJsonGetKey(szObj, i);
    NedAssert(szKey);
    szValue = NedJsonGetValuePtr(szKey, &type);

    if(NedJsonStrCmp(m_szKeyFile, szKey) == 0 && type == NEDJSON_STRING)
      pSetFile->szFile = (void *)szValue;
    else if(NedJsonStrCmp(m_szKeyPos, szKey) == 0 && type == NEDJSON_NUMBER)
      pSetFile->pos = NedJsonGetNumber(szValue);
    else if(NedJsonStrCmp(m_szKeyTopLine, szKey) == 0 && type == NEDJSON_NUMBER)
      pSetFile->topLine = NedJsonGetNumber(szValue);
    else if(NedJsonStrCmp(m_szKeyLeftCol, szKey) == 0 && type == NEDJSON_NUMBER)
      pSetFile->leftCol = NedJsonGetNumber(szValue);
    else if(NedJsonStrCmp(m_szKeyPrefCol, szKey) == 0 && type == NEDJSON_NUMBER)
      pSetFile->prefCol = NedJsonGetNumber(szValue);
    else if(NedJsonStrCmp(m_szKeyRdOnly, szKey) == 0 && type == NEDJSON_BOOL)
      pSetFile->fRdOnly = NedJsonGetBool(szValue);
  }

  if(pSetFile->szFile == NULL)
    NedLogPrintfEx(NEDLOG_SETTINGS, "  NsDecodeObjFile: No Filenname at obj %s\n", szObj);
  else
  {
    NedLogPrintfEx(NEDLOG_SETTINGS, "  NsDecodeObjFile: %.20s\n", pSetFile->szFile);
    NedLogPrintfEx(NEDLOG_SETTINGS, "  pos %ld, topLine %ld, leftCol %ld, prefCol %ld, rdOnly %u\n",
      pSetFile->pos, pSetFile->topLine, pSetFile->leftCol, pSetFile->prefCol, pSetFile->fRdOnly);
  }

  return pSetFile->szFile ? TRUE : FALSE;
}

/*-------------------------------------------------------------------------------------------------
  Decode a JSON pospair object into the settings structure. 

  Example: {"idx":0,"pos":99}

  @param    pSetFile    pointer to sSetFile_t to be filled in
  @param    szJson      JSON file object in string form
  @return   TRUE if worked, FALSE if not a pos pair object
-------------------------------------------------------------------------------------------------*/
bool_t NsDecodeObjPosPair(sSetPosPair_t *pPosPair, const char *szObj)
{
  const char     *szKey;
  const char     *szValue;
  size_t          i;
  size_t          numKeys;
  nedJsonType_t   type;
  bool_t          fWorked = TRUE;

  // both "idx" and "pos" keys must be present in this object
  pPosPair->idx = SET_IDX_NULL;
  pPosPair->pos = SET_POS_NULL;
  numKeys = NedJsonGetCount(szObj);
  for(i = 0; i < numKeys; ++i)
  {
    // get the key/value pair
    szKey = NedJsonGetKey(szObj, i);
    NedAssert(szKey);
    szValue = NedJsonGetValuePtr(szKey, &type);

    // both must be numbers
    if(NedJsonStrCmp(m_szKeyIdx, szKey) == 0 && type == NEDJSON_NUMBER)
      pPosPair->idx = NedJsonGetNumber(szValue);
    else if(NedJsonStrCmp(m_szKeyPos, szKey) == 0 && type == NEDJSON_NUMBER)
      pPosPair->pos = NedJsonGetNumber(szValue);
  }

  // if both "idx" and "pos" keys are not present, then bad object
  if(pPosPair->idx == SET_IDX_NULL || pPosPair->pos == SET_POS_NULL)
  {
    pPosPair->idx = SET_IDX_NULL;
    pPosPair->pos = SET_POS_NULL;
    fWorked       = FALSE;
  }

  return fWorked;
}

/*-------------------------------------------------------------------------------------------------
  Decode a JSON keymap object into the settings structure

  Example: {"key":"Ctrl-G","cmd":"EditCutWord"}

  @param    pSetFile    pointer to sSetFile_t to be filled in
  @param    szJson      JSON file object in string form
  @return   TRUE if worked, FALSE if not a pos pair object
-------------------------------------------------------------------------------------------------*/
bool_t NsDecodeObjKeyMap(sSetKeyMap_t *pKeyMap, const char *szObj)
{
  const char     *szKey;
  const char     *szValue;
  size_t          i;
  size_t          numKeys;
  nedJsonType_t   type;

  // initialize keymap struct. Both "szKey":"key" and "szCmd":"cmd" must be present
  memset(pKeyMap, 0, sizeof(*pKeyMap));
  numKeys = NedJsonGetCount(szObj);
  for(i = 0; i < numKeys; ++i)
  {
    // get the key/value pair
    szKey = NedJsonGetKey(szObj, i);
    NedAssert(szKey);
    szValue = NedJsonGetValuePtr(szKey, &type);

    if(NedJsonStrCmp(m_szKeyKey, szKey) == 0 && type == NEDJSON_STRING)
      pKeyMap->szKey = szValue;
    else if(NedJsonStrCmp(m_szKeyCmd, szKey) == 0 && type == NEDJSON_STRING)
      pKeyMap->szCmd = szValue;
  }

  return (pKeyMap->szKey && pKeyMap->szCmd) ? TRUE : FALSE;
}

/*-------------------------------------------------------------------------------------------------
  Create a NULL terminated array of string pointers from JSON strings on the heap. Returned
  pointer to pointer can be freed with free() when no longer needed. Empty array is valid. Verifies
  array is an array of strings

  Example: ["str1","str2"] becomes C equivalent of { "str1", "str2", NULL };

  @param    szArray    pointer to JSON array of strings
  @return   pointer to allocated array of strings, or NULL if not array of strings
-------------------------------------------------------------------------------------------------*/
static const char **NjCopyStrArray(const char *szArray)
{
  size_t          count;
  size_t          i;
  size_t          thisLen;
  size_t          len = 0;
  nedJsonType_t   type;
  const char     *szValue;
  char           *psz;
  char          **ppsz    = NULL;
  bool_t          fJsonOk = TRUE;

  // determine # and length of strings (and check validity while we're at it)
  count = NedJsonGetCount(szArray);
  for(i = 0; i < count; ++i)
  {
    szValue = NedJsonGetScalar(szArray, i, &type);
    if(szValue == NULL || type != NEDJSON_STRING)
    {
      fJsonOk = FALSE;
      break;
    }
    else
    {
      len += NedJsonStrLen(szValue) + 1;
    }
  }

  // only decode if valid array of strings
  if(fJsonOk)
  {
    ppsz = malloc(((sizeof(char *)) * (count + 1)) + len);
    if(ppsz)
    {
      psz = (char *)(&ppsz[count + 1]);
      for(i = 0; i < count; ++i)
      {
        szValue = NedJsonGetScalar(szArray, i, &type);
        thisLen = NedJsonStrLen(szValue);
        ppsz[i] = psz;
        NedJsonStrNCpy(psz, szValue, thisLen);
        psz[thisLen] = '\0';
        psz += (thisLen + 1);
      }
    }
  }

  return (const char **)ppsz;
}

/*-------------------------------------------------------------------------------------------------
  Decode a JSON language object and set the global language settings

  Example: 
  "language":[{"name":"C/C++","ext":".c .h .cpp .hpp","tabSize":2,"hardTabs":false,"crLf":false,
  "wrapLen":120,"lineComments":["//"],"blockComments":["#if 0","#endif"],"keywords":["int","void"]}]

  @param    szObj    pointer to sSetFile_t to be filled in
  @return   none (objerct decoded into global language settings)
-------------------------------------------------------------------------------------------------*/
static void NsJsonDecodeObjLanguage(const char *szObj)
{
  const char             *szKey;
  const char             *szValue;
  const char            **asz;        // array of string pointers, allocated on heap
  size_t                  i;
  size_t                  numKeys;
  nedJsonType_t           type;
  sNedLanguage_t          sLang;
  bool_t                  fName   = FALSE;
  bool_t                  fSet    = FALSE;
  const sNedLanguage_t   *pLang;

  // assume nothing
  memset(&sLang, 0, sizeof(sLang));

  // processs all keys. name of language "lang": field is mandatory, others are optional
  numKeys = NedJsonGetCount(szObj);
  for(i = 0; i < numKeys; ++i)
  {

    // if there no key or value, bad JSON, we're done
    szKey = NedJsonGetKey(szObj, i);
    szValue = NedJsonGetValuePtr(szKey, &type);
    if(!szKey || !szValue)
      break;

    // NedLogPrintf("szKey %.12s, szValue %.12s\n", szKey, szValue);

    // decode each field we know about, ignore all others
    if(NedJsonStrCmp(m_szKeyLang, szKey) == 0 && type == NEDJSON_STRING)
    {
      NedJsonStrNCpy(sLang.szLang, szValue, sizeof(sLang.szLang) - 1);
      fName = TRUE;
    }
    else if(NedJsonStrCmp(m_szKeyExt, szKey) == 0 && type == NEDJSON_STRING)
    {
      NedJsonStrNCpy(sLang.szExt, szValue, sizeof(sLang.szExt) - 1);
    }
    else if(NedJsonStrCmp(m_szKeyTabSize, szKey) == 0 && type == NEDJSON_NUMBER)
    {
      sLang.tabSize = (unsigned)NedJsonGetNumber(szValue);
    }
    else if(NedJsonStrCmp(m_szKeyHardTabs, szKey) == 0 && type == NEDJSON_BOOL)
    {
      sLang.fHardTabs = NedJsonGetBool(szValue);
    }
    else if(NedJsonStrCmp(m_szKeyCrLf, szKey) == 0 && type == NEDJSON_BOOL)
    {
      sLang.fCrLf = NedJsonGetBool(szValue);
    }
    else if(NedJsonStrCmp(m_szKeyWrapLen, szKey) == 0 && type == NEDJSON_NUMBER)
    {
      sLang.wrapLen = NedJsonGetNumber(szValue);
    }
    else if(NedJsonStrCmp(m_szKeyLineComments, szKey) == 0 && type == NEDJSON_ARRAY)
    {
      asz = NjCopyStrArray(szValue);
      if(asz)
      {
        sLang.aszLineComments = asz;
      }
    }
    else if(NedJsonStrCmp(m_szKeyBlockComments, szKey) == 0 && type == NEDJSON_ARRAY)
    {
      asz = NjCopyStrArray(szValue);
      if(asz)
      {
        sLang.aszBlockComments = asz;
      }
    }
    else if(NedJsonStrCmp(m_szKeyKeywords, szKey) == 0 && type == NEDJSON_ARRAY)
    {
      asz = NjCopyStrArray(szValue);
      if(asz)
      {
        sLang.aszKeywords = asz;
      }
    }
  }

  // only set the language if something was changed from the defaults
  if(fName)
  {
    pLang = NedLangGetByName(sLang.szLang);
    fSet = TRUE;

    // modifying an existing language
    if(pLang)
    {
      if(*sLang.szExt == 0)
        strcpy(sLang.szExt, pLang->szExt);
      if(sLang.tabSize == 0)
        sLang.tabSize = pLang->tabSize;
      if(sLang.wrapLen == 0)
        sLang.wrapLen = pLang->wrapLen;
      if(sLang.aszLineComments == NULL)
        sLang.aszLineComments = pLang->aszLineComments;
      if(sLang.aszBlockComments == NULL)
        sLang.aszBlockComments = pLang->aszBlockComments;
      if(sLang.aszKeywords == NULL)
        sLang.aszKeywords = pLang->aszKeywords;
    }

    // new language, all fields must be present
    {
      if((*sLang.szExt == 0) || (sLang.tabSize == 0) || (sLang.wrapLen == 0) ||
         (sLang.aszLineComments == NULL) || (sLang.aszBlockComments == NULL) ||
         (sLang.aszKeywords == NULL))
      {
        fSet = FALSE;
      }
    }

    // NedLogPrintf("Got lang %s, fSet %u, wrapLen %ld\n", sLang.szLang, fSet, sLang.wrapLen);
    if(fSet)
      NedLangSet(&sLang);
  }
}

/*-------------------------------------------------------------------------------------------------
  Decode a theme

  Example: {"name":"grass","colors":[39,114,47,46,43,63,58,47,43,47]}

  @param    pSetFile    pointer to sSetFile_t to be filled in
  @param    szJson      JSON file object in string form
  @return   none
-------------------------------------------------------------------------------------------------*/
void NsDecodeObjTheme(const char *szObj)
{
  nedTheme_t      sTheme;
  char            szName[NEDTHEME_MAX_NAMESIZE];
  const char     *szKey;
  const char     *szValue;
  const char     *szArray;
  size_t          i;
  size_t          j;
  size_t          numKeys;
  size_t          numValues;
  nedAttr_t       attr;

  nedJsonType_t   type;

  // 
  memset(&sTheme, 0, sizeof(sTheme));
  memset(szName, 0, sizeof(szName));
  numKeys = NedJsonGetCount(szObj);
  // NedLogPrintf("NsDecodeObjTheme(numKeys %ld)\n", numKeys);
  for(i = 0; i < numKeys; ++i)
  {
    // get the key/value pair
    szKey = NedJsonGetKey(szObj, i);
    NedAssert(szKey);
    szValue = NedJsonGetValuePtr(szKey, &type);

    if(NedJsonStrCmp(m_szKeyName, szKey) == 0 && type == NEDJSON_STRING)
    {
      NedJsonStrNCpy(szName, szValue, sizeof(szName));
      // NedLogPrintf("  Got %s: %s\n", m_szKeyName, szName);
    }

    else if(NedJsonStrCmp(m_szKeyColors, szKey) == 0 && type == NEDJSON_ARRAY)
    {
      // NedLogPrintf("  Got %s\n", m_szKeyColors);
      szArray = szValue;
      numValues = NedJsonGetCount(szArray);
      for(j = 0; j < numValues && j < NEDTHEME_NUM_FIELDS; ++j)
      {
        szValue = NedJsonGetScalar(szArray, j, &type);
        if(type == NEDJSON_NUMBER)
        {
          attr = (nedAttr_t)NedJsonGetNumber(szValue);
          // NedLogPrintf("  Got attr[%u]=%u\n", j, attr);
          if(j == 0)
            sTheme.text = attr;
          else if(j == 1)
            sTheme.textHighlight = attr;
          else if(j == 2)
            sTheme.comment = attr;
          else if(j == 3)
            sTheme.keyword = attr;
          else if(j == 4)
            sTheme.constant = attr;
          else if(j == 5)
            sTheme.menuText = attr;
          else if(j == 6)
            sTheme.menuHotkey = attr;
          else if(j == 7)
            sTheme.menuDropText = attr;
          else if(j == 8)
            sTheme.menuDropHotkey = attr;
          else if(j == 9)
            sTheme.message = attr;
        }
      }
    }
  }

  // set the theme if valid name
  if(strlen(szName) > 0)
    NedThemeSet(szName, &sTheme);
}


/*-------------------------------------------------------------------------------------------------
  Decode the JSON into the settings structure. See file ned.json in docs folder

  @param    pSettings   pointer to settings structure to decode from JSON
  @param    szJson      JSON file in string form
  @return   number of main keys
-------------------------------------------------------------------------------------------------*/
size_t NsJsonDecode(sNedSettings_t *pSettings, const char *szJson)
{
  const char       *szObj;
  const char       *szKey;
  const char       *szValue;
  const char       *szArray;
  nedJsonType_t     type;
  size_t            numKeys = 0;
  size_t            count2;
  size_t            i, j;
  size_t            idx;

  szObj = NedJsonGetObj(szJson);
  if(!szObj)
    return 0;

  numKeys = NedJsonGetCount(szObj);
  for(i = 0; i < numKeys; ++i)
  {
    // get the key/value pair
    szKey = NedJsonGetKey(szObj, i);
    NedAssert(szKey);
    szValue = NedJsonGetValuePtr(szKey, &type);
    // NedLogPrintf("  NsJsonDecode key=%.16s value=%.8s, type %u\n", szKey, szValue, type);

    // "nedVersion": 200
    if(NedJsonStrCmp(m_szKeyNedVersion, szKey) == 0 && type == NEDJSON_NUMBER)
      pSettings->nedVersion = (unsigned)NedJsonGetNumber(szValue);

    // "curFile": 1
    else if(NedJsonStrCmp(m_szKeyCurFile, szKey) == 0 && type == NEDJSON_NUMBER)
      pSettings->curFile = (unsigned)NedJsonGetNumber(szValue);

    // "recentFiles":["file1.c", "file2.h"]
    else if(NedJsonStrCmp(m_szKeyRecentFiles, szKey) == 0 && type == NEDJSON_ARRAY)
    {
      szArray = szValue;
      count2  = NedJsonGetCount(szArray);
      j = 0;
      while(count2)
      {
        szValue = NedJsonGetScalar(szArray, j, &type);
        if(szValue != NULL && type == NEDJSON_STRING)
        {
          pSettings->aszRecentFiles[j] = (void *)szValue;
          ++j;
        }
        --count2;
      }
    }

    // "projectFolders":["path/folder1", "path/folder2"]
    else if(NedJsonStrCmp(m_szKeyProjectFolders, szKey) == 0 && type == NEDJSON_ARRAY)
    {
      szArray = szValue;
      count2  = NedJsonGetCount(szArray);
      idx     = 0;
      for(j = 0; j < count2; ++j)
      {
        szValue = NedJsonGetScalar(szArray, j, &type);
        if(type == NEDJSON_STRING)
        {
          pSettings->aszProjectFolders[idx] = (void *)szValue;
          ++idx;
        }
      }
    }

    // "theme": "Natural"
    else if(NedJsonStrCmp(m_szKeyTheme, szKey) == 0 && type == NEDJSON_STRING)
    {
      pSettings->szTheme = (void *)szValue;
    }

    // "userThemes":[{"name":"grass","colors":[39,114,47,46,43,63,58,47,43,47]
    else if(NedJsonStrCmp(m_szKeyUserThemes, szKey) == 0 && type == NEDJSON_ARRAY)
    {
      szArray = szValue;
      count2  = NedJsonGetCount(szArray);
      // NedLogPrintf("count2 %ld\n", count2);
      for(j = 0; j < count2; ++j)
      {
        szValue = NedJsonGetScalar(szArray, j, &type);
        // NedLogPrintf("j %ld, %.6s, type %u\n", j, szValue ? szValue : "(null)", type);
        if(type == NEDJSON_OBJ)
          NsDecodeObjTheme(szValue);
      }
    }

    // "openFiles":[{"file":"file1.c","pos":99,"topLine":1,"leftCol":0,"prefCol":16,"rdOnly":false}]
    else if(NedJsonStrCmp(m_szKeyOpenFiles, szKey) == 0 && type == NEDJSON_ARRAY)
    {
      szArray = szValue;
      count2  = NedJsonGetCount(szArray);
      // NedLogPrintf("  OpenFile HERE 1, count2 %zu\n", count2);
      idx     = 0;
      for(j = 0; j < count2; ++j)
      {
        // NedLogPrintf("  OpenFile HERE 2\n");
        szValue = NedJsonGetScalar(szArray, j, &type);
        if(type == NEDJSON_OBJ && NsDecodeObjFile(&pSettings->aOpenFiles[idx], szValue))
          ++idx;
      }
    }

    // "bookmarks":[{"idx":0,"pos":99}, {"idx":0,"pos":200}]
    else if(NedJsonStrCmp(m_szKeyBookmarks, szKey) == 0 && type == NEDJSON_ARRAY)
    {
      szArray = szValue;
      count2  = NedJsonGetCount(szArray);
      idx     = 0;
      for(j = 0; j < count2; ++j)
      {
        szValue = NedJsonGetScalar(szArray, j, &type);
        if(type == NEDJSON_OBJ && NsDecodeObjPosPair(&pSettings->aBookmarks[idx], szValue))
          ++idx;
      }
    }

    // "keyMap":[{"key":"Ctrl-G","cmd":"EditCutWord"}]
    else if(NedJsonStrCmp(m_szKeyKeyMap, szKey) == 0 && type == NEDJSON_ARRAY)
    {
      szArray = szValue;
      count2  = NedJsonGetCount(szArray);
      idx     = 0;
      for(j = 0; j < count2; ++j)
      {
        szValue = NedJsonGetScalar(szArray, j, &type);
        if(type == NEDJSON_OBJ && NsDecodeObjKeyMap(&pSettings->aKeyMap[idx], szValue))
          ++idx;
      }
    }

    // language is decoded into the global language settings
    // "language":[{"name":"C/C++","ext":".c .h .cpp .hpp","tabSize":2,"hardTabs":false,"crLf":false,"wrapLen":120}]
    else if(NedJsonStrCmp(m_szKeyLanguage, szKey) == 0 && type == NEDJSON_ARRAY)
    {
      szArray = szValue;
      count2  = NedJsonGetCount(szArray);
      for(j = 0; j < count2; ++j)
      {
        szValue = NedJsonGetScalar(szArray, j, &type);
        if(type == NEDJSON_OBJ)
          NsJsonDecodeObjLanguage(szValue);
      }
    }
  }

  return numKeys;
}

/*!------------------------------------------------------------------------------------------------
  Initialize the editor for all things that are loaded via settings (e.g. create an empty bookmark 
  list).

  Note: all other pEditor fields areset to 0 or NULL at this pont.

  @ingroup  ned_settings
  @param    pEditor       editor structure (may be initialized or not).
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedSettingsInitEditor(sNedEditor_t *pEditor)
{
  // editor not initialized, set it up
  if(!NedEditorIsEditor(pEditor))
  {
    pEditor->sanchk           = NEDEDITOR_SANCHK;
    pEditor->hPosHistory      = NedListNew(NEDCFG_MAX_HISTORY, sizeof(sNedPos_t), NedPosListIsSame, NedPosListLogShow);
    pEditor->hBookmarks       = NedListNew(NEDCFG_MAX_HISTORY, sizeof(sNedPos_t), NedPosListIsSame, NedPosListLogShow);
    pEditor->hRecentFiles     = NedListNew(NEDCFG_MAX_RECENT, PATH_MAX, NedSettingsStrIsSame, NedSettingsStrShow);
    pEditor->hProjectFolders  = NedListNew(NEDCFG_MAX_RECENT, PATH_MAX, NedSettingsStrIsSame, NedSettingsStrShow);
    pEditor->themeIndex       = NEDTHEME_CLASSIC;
  }

  // editor already initialized, just clear it
  else
  {
    NedListClear(pEditor->hPosHistory);
    NedListClear(pEditor->hBookmarks);
    NedListClear(pEditor->hRecentFiles);
    NedListClear(pEditor->hProjectFolders);
    NedFileFreeAll(pEditor->pFileList);
    // leave color theme alone
    // leave language alone
  }
  pEditor->pFileList = NULL;
  pEditor->pFileCur  = NULL;
}


/*!------------------------------------------------------------------------------------------------
  Free every Settings field pointed to by the  editor. Asserts if not an editor structure.
  Note: does NOT affect non-settings fields, so hMenu, etc... are unaffected.

  @ingroup  ned_settings
  @param    pEditor
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedSettingsFreeEditor(sNedEditor_t *pEditor)
{
  NedAssert(NedEditorIsEditor(pEditor));

  if(pEditor->hPosHistory)
  {
    NedListFree(pEditor->hPosHistory);
    pEditor->hPosHistory = NULL;
  }
  if(pEditor->hBookmarks)
  {
    NedListFree(pEditor->hBookmarks);
    pEditor->hBookmarks = NULL;
  }
  if(pEditor->hRecentFiles)
  {
    NedListFree(pEditor->hRecentFiles);
    pEditor->hRecentFiles = NULL;
  }
  if(pEditor->hProjectFolders)
  {
    NedListFree(pEditor->hProjectFolders);
    pEditor->hProjectFolders = NULL;
  }
  if(pEditor->pFileList)
    NedFileFreeAll(pEditor->pFileList);
  pEditor->pFileList = NULL;
  pEditor->pFileCur  = NULL;
}


/*!------------------------------------------------------------------------------------------------
  Is this a valid editor structure?

  @ingroup  ned_settings
  @return   NEDSET_ERR_NONE if worked (saved), NEDSET_ERR_FAILED if failed (usually bad filepath
            or uninitialized settings structure. See NedSettingsNew().
*///-----------------------------------------------------------------------------------------------
bool_t NedEditorIsEditor(const sNedEditor_t *pEditor)
{
  if(pEditor && (pEditor->sanchk == NEDEDITOR_SANCHK))
    return TRUE;
  return FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Set file ending by language if no lines in file. If there are lines in file, leave line endings
  alone. Also sets wrapLen

  @ingroup  ned_settings
  @param    pFile       pointer to valid file
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedSettingsSetFileCrLf(sNedFile_t *pFile)
{
  const sNedLanguage_t  *pLang = NedLangGet(NedFilePath(pFile));

  pFile->wrapLen = pLang->wrapLen;

  // if no lines in file, then wasn't set by NedFileLoad(), so set line ending by language
  if(NedBufSearch(pFile->pBufHead, '\n', NULL) == NULL)
  {
    if(pLang->fCrLf)
      pFile->fCrLf = TRUE;
    else
      pFile->fCrLf = FALSE;
  }

  if(!pFile->tabSize)
    pFile->tabSize = pLang->tabSize;

  NedLogPrintfEx(NEDLOG_SETTINGS, "NedSettingsSetFileCrLf(%u, wrapLen=%ld, tabSize %u)\n", pFile->fCrLf, pFile->wrapLen,
    pFile->tabSize);
}

/*!------------------------------------------------------------------------------------------------
  Save settings to a Ned Project (JSON) filename, typically "/some/path/nedproj.json".

  @ingroup  ned_settings
  @return   NEDSET_ERR_NONE if worked (saved), NEDSET_ERR_FAILED if failed (usually bad filepath
            or uninitialized settings structure. See NedSettingsNew().
*///-----------------------------------------------------------------------------------------------
nedSetErr_t NedSettingsSave(const char *szProjName, const sNedEditor_t *pEditor)
{
  char                   *szJson    = NULL;
  FILE                   *fp        = NULL;
  sNedSettings_t         *pSettings = NULL;
  const sNedLanguage_t   *pLang     = NULL; 
  size_t                  len;
  sNedFile_t             *pFile;
  sSetFile_t             *pSetFile;
  pfnNedCmd_t             pfnCmd;
  nedKey_t                key;
  nedSetErr_t             err = NEDSET_ERR_NONE;
  unsigned                i;

  NedLogPrintfEx(NEDLOG_SETTINGS, "NedSettingsSave(proj %s, pEditor %p)\n", szProjName, pEditor);

  // verify parameters
  if(szProjName == NULL || !NedEditorIsEditor(pEditor))
    return NEDSET_ERR_BADPARM;

  pSettings = malloc(sizeof(sNedSettings_t));
  if(pSettings == NULL)
  {
    return NEDSET_ERR_MEM;
  }

  // verify we can create the settings file
  fp = fopen(szProjName, "w");
  if(fp == NULL)
  {
    free(pSettings);
    return NEDSET_ERR_FAILED;
  }

  // convert from editor to settings structure
  memset(pSettings, 0, sizeof(*pSettings));
  pSettings->nedVersion = NEDVER_NUM;
  NsGetListItems(pSettings->aszRecentFiles,    pEditor->hRecentFiles);
  NsGetListItems(pSettings->aszProjectFolders, pEditor->hProjectFolders);
  pSettings->szTheme = (void *)NedThemeGetName(pEditor->themeIndex);
  pFile = pEditor->pFileList;
  pSettings->curFile = 0;
  for(i=0; (pFile != NULL) && (i < NumElements(pSettings->aOpenFiles)); ++i)
  {
    if(pFile == pEditor->pFileCur)
      pSettings->curFile = i;

    pSetFile = &pSettings->aOpenFiles[i];
    pSetFile->szFile    = pFile->sInfo.szFullPath;
    pSetFile->pos       = NedPointPos(&pFile->sCursor);
    pSetFile->topLine   = pFile->topLine;
    pSetFile->leftCol   = pFile->leftCol;
    pSetFile->prefCol   = pFile->prefCol;
    pSetFile->fRdOnly   = pFile->fRdOnly;
    NedLogPrintfEx(NEDLOG_SETTINGS, "  Got file[%u] %s, pos %ld, topLine %ld\n",
                                    i, pSetFile->szFile, pSetFile->pos, pSetFile->topLine);
    pFile = pFile->pNext;
  }
  NsGetHistoryItems(pSettings->aBookmarks, pEditor->pFileList, pEditor->hBookmarks);
  for(i=0; i < NumElements(pSettings->aKeyMap); ++i)
  {
    pfnCmd = NedKeyMapGetMapped(i, &key);
    if(!pfnCmd)
      break;
    pSettings->aKeyMap[i].szKey = NedKeyName(key);
    pSettings->aKeyMap[i].szCmd = NedCmdGetName(pfnCmd);
    NedLogPrintfEx(NEDLOG_SETTINGS, "  Got mapped key[%u] %s %s\n",
                                    i, pSettings->aKeyMap[i].szKey, pSettings->aKeyMap[i].szCmd);
  }
  for(i=0; i<NEDCFG_MAX_LANG; ++i)
  {
    pLang = NedLangGetIndexed(i);
    if(!pLang)
      break;
    NedLogPrintfEx(NEDLOG_SETTINGS, "  Got lang[%u] %s\n", i, pLang->szLang);
    pSettings->apLang[i] = (void *)pLang;
  }

  // determine length and encode into JSON
  len = NsJsonEncode(NULL, SIZE_MAX, pSettings, TRUE);
  if(len)
  {
    szJson = malloc(len + 2);
    if(!szJson)
      err = NEDSET_ERR_MEM;
    else
    {
      NsJsonEncode(szJson, len + 1, pSettings, TRUE);
      szJson[len] = '\n';
      szJson[len + 1] = '\0'; 
    }
  }

  // write out the encoded file
  if((fp != NULL) && (szJson != NULL) && (err == NEDSET_ERR_NONE))
  {
    NedLogPrintfEx(NEDLOG_SETTINGS, "  writing JSON file %s\n%s\n", szProjName, szJson);
    if(fwrite(szJson, 1, strlen(szJson), fp) != strlen(szJson))
      err = NEDSET_ERR_FAILED;
  }

  // clean up
  if(fp != NULL)
    fclose(fp);
  if(szJson != NULL)
    free(szJson);
  if(pSettings != NULL)
    free(pSettings);
  NedLogPrintfEx(NEDLOG_SETTINGS, "err %d\n", err);

  return err;
}

/*!------------------------------------------------------------------------------------------------
  Load settings from a project file (e.g. "ned.json") into an pEditor structure. The structure
  may be initialized or not. If initialized, will only affect fields that are part of Settings.
  All files will be closed, all bookmarks cleared, etc...

  @ingroup  ned_settings
  @param    pEditor       structure to fill in from JSON project file
  @param    szProjName    project filename, usually "ned.json"   
  @return   Returns NEDSET_ERR_NONE if worked, or error code if not
*///-----------------------------------------------------------------------------------------------
nedSetErr_t NedSettingsLoad(sNedEditor_t *pEditor, const char *szProjName, bool_t fLoadFiles)
{
  char             *pszJson   = NULL;
  sNedSettings_t   *pSettings = NULL;  
  sNedFile_t       *pFile;
  size_t            i;
  size_t            thisLen;
  nedSetErr_t       err = NEDSET_ERR_NONE;
  char              szPath[PATH_MAX];

  NedLogPrintfEx(NEDLOG_SETTINGS, "NedSettingsLoad(pEditor %p, szProjName %s)\n", pEditor, szProjName);

  // check parameters
  if(!pEditor || !szProjName)
    return NEDSET_ERR_BADPARM;

  // load the JSON file into a '\0' terminated memory string
  pszJson = NedFileRead(szProjName);
  if(!pszJson)
    return NEDSET_ERR_FAILED;
  NedLogPrintfEx(NEDLOG_SETTINGS, "  Loaded JSON file %s:\n%s, strlen %zu\n", szProjName, pszJson, strlen(pszJson));

  // basic prep for loading
  pSettings = malloc(sizeof(sNedSettings_t));
  if(!pSettings)
  {
    free(pszJson);
    return NEDSET_ERR_FAILED;
  }
  memset(pSettings, 0, sizeof(*pSettings));
  for(i = 0; i < NumElements(pSettings->aBookmarks); ++i)
  {
    pSettings->aBookmarks[i].idx = SET_IDX_NULL;
  }
  NedSettingsInitEditor(pEditor);
  NedLogPrintfEx(NEDLOG_SETTINGS, "  LoadSettings 1...\n");

  // decode into settings. This already processes key "language"
  // All other keys must be processed into editor settings after the decode
  // All strings just pointers to JSON strings and so must copied if needed using NedJsonStrNCpy()
  if(!NsJsonDecode(pSettings, pszJson) || pSettings->nedVersion < NEDVER_NUM)
    err = NEDSET_ERR_FAILED;
  NedLogPrintfEx(NEDLOG_SETTINGS, "  LoadSettings 2, nedVersion %u...\n", pSettings->nedVersion);

  // load open files before other things like bookmarks so we have proper file indexes
  if(!err && fLoadFiles)
  {
    NedLogPrintfEx(NEDLOG_SETTINGS, "  LoadSettings 3...\n");

    for(i = 0; (i < NEDCFG_MAX_FILES) && (pSettings->aOpenFiles[i].szFile != NULL); ++i)
    {
      // create the file structure and hook it into editor file list
      thisLen = NedJsonStrLen(pSettings->aOpenFiles[i].szFile);
      if(thisLen == 0 || thisLen >= PATH_MAX)
        continue;

      NedJsonStrNCpy(szPath, pSettings->aOpenFiles[i].szFile, thisLen + 1);
      // NedLogPrintf("szPath %s, thisLen %zu\n", szPath, thisLen);

      pFile = NedFileNew(pEditor->pFileList, szPath);
      if(!pFile)
        continue;
      if(!pEditor->pFileList)
        pEditor->pFileList = pFile;

      // SPECIAL CASE: load the NedManual.md pseudo file
      NedSettingsLoadIfNedManual(pFile);

      // if the file is empty, set fCrLf based on file extension so it saves correctly
      NedSettingsSetFileCrLf(pFile);

      // setup the file structure
      pFile->topLine = pSettings->aOpenFiles[i].topLine;
      pFile->leftCol = pSettings->aOpenFiles[i].leftCol;
      pFile->prefCol = pSettings->aOpenFiles[i].prefCol;
      pFile->fRdOnly = pSettings->aOpenFiles[i].fRdOnly;
      NedPointInit(&pFile->sCursor, pFile->pBufHead);
      NedPointInit(&pFile->sSelect, pFile->pBufHead);
      NedPointFileGotoPos(&pFile->sCursor, pSettings->aOpenFiles[i].pos);
      pFile->curLine = NedPointLineGet(&pFile->sCursor, &pFile->curCol);

      if(NedLogMaskGet() & NEDLOG_SETTINGS)
        NedFileLogShow(pFile, LOG_MORE);
    }

    // loaded all the files, now set up current file based on index
    pFile = NsGetFileByIndex(pEditor->pFileList, pSettings->curFile);
    pEditor->pFileCur = pFile ? pFile : pEditor->pFileList;

    NedLogPrintfEx(NEDLOG_SETTINGS, "  LoadSettings 4...\n");

    // load bookmarks, must be done AFTER loading files
    NsLoadPos(pEditor->hBookmarks, pEditor->pFileList, pSettings->aBookmarks);
  }

  NedLogPrintfEx(NEDLOG_SETTINGS, "  LoadSettings 5...\n");

  // set theme
  pEditor->themeIndex = NEDTHEME_CLASSIC;
  if(pSettings->szTheme)
  {
    NedJsonStrNCpy(szPath, pSettings->szTheme, NEDTHEME_MAX_NAMESIZE);
    szPath[NEDTHEME_MAX_NAMESIZE - 1] = '\0';
    pEditor->themeIndex = NedThemeGetIndex(szPath);
  }

  NedLogPrintfEx(NEDLOG_SETTINGS, "  LoadSettings 6...\n");

  // load recent files, recent projects, project folders
  NsLoadList(pEditor->hRecentFiles,    pSettings->aszRecentFiles,    NEDCFG_MAX_RECENT);
  NsLoadList(pEditor->hProjectFolders, pSettings->aszProjectFolders, NEDCFG_MAX_RECENT);

  NedLogPrintfEx(NEDLOG_SETTINGS, "  LoadSettings 7...\n");

  NsLoadKeyMap(pSettings->aKeyMap, NEDCFG_MAX_KEYMAP);

  NedLogPrintfEx(NEDLOG_SETTINGS, "  LoadSettings 8...\n");

  // done with in-memory JSON file
  if(pszJson)
    free(pszJson);
  if(pSettings)
    free(pSettings);

  return NEDSET_ERR_NONE;
}

/*!------------------------------------------------------------------------------------------------
  If this is the ~/NedManual.md pseudo file, load it into the buffers.

  parameters:
  pFile         Pointer to a file structure

  @return   Loads NedManual.md contents into buffers if it is the special filename.
*///-----------------------------------------------------------------------------------------------
void NedSettingsLoadIfNedManual(sNedFile_t *pFile)
{
  char    szNedManFullPath[PATH_MAX];
  long    numBytes;

  // check if this is ~/NedManual.md, and if the file is empty, load the hard-coded NedManual.md
  NedLibFileFullPath(szNedManFullPath, szNedManualPath);
  if((strcmp(pFile->sInfo.szFullPath, szNedManFullPath) == 0))
  {
    NedBufChainSize(pFile->pBufHead, &numBytes);
    if(numBytes == 0)
      NedBufInsert(pFile->pBufHead, 0, (void *)gszNedManual, strlen(gszNedManual));
    pFile->fRdOnly = TRUE;
  }
}
