/*!************************************************************************************************
  @file NedWin.c
 
  @brief Ned Window framework implementation

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  High level rules to remember:

  * Canvas is the ONLY thing copied to screen (via ANSI calls)
  * Backdrop and Canvas are always the size of the screen.
  * Only Canvas keeps track of dirty portions of screen. Windows don't need to.
  * Only NedWinFlush(), NedWinFlushAll(), NedWinClearScreen() and NedWinFlushCursor() use ANSI calls

*///***********************************************************************************************
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "NedWin.h"

#define WinLogPrintf(szFmt, ...) NedLogPrintfEx(NEDLOG_WIN, szFmt, ##__VA_ARGS__)

#define WIN_SANCHK      41464U

#define NEDWIN_CORNER    '+'
#define NEDWIN_SIDE      '|'
#define NEDWIN_LINE      '-'
#define NEDWIN_PICKLEFT  '['
#define NEDWIN_PICKRIGHT ']'


// Ned window characters include an 8-bit attribute (high byte) and an 8-bit character (low byte)
typedef uint16_t nedWinChar_t;   // attr/char pair
#define NedWinCharAttr(nc)       ( ((nc) >> 8) & 0xff )
#define NedWinChar(nc)           ( (char)((nc) & 0x7f) )
#define NedWinCharMake(attr,c)   ( (((attr) & 0xff) << 8) | ((c) & 0xff) )

typedef struct
{
  winPos_t            left;         // 0-n
  winPos_t            cols;         // 0 = not dirty, 1-n dirty columns
} sNedWinDirtyRow_t;

typedef struct nedWin
{
  unsigned            sanchk;           // sanity check that this object is a Window (see WIN_SANCHK)
  winPos_t            top;
  winPos_t            left;
  winPos_t            rows;
  winPos_t            cols;
  winPos_t            crow;             // cursor row
  winPos_t            ccol;             // cursor col
  winPos_t            oldTop;           // for hiding/unhiding
  winPos_t            oldLeft;
  bool_t              fFramed;
  nedAttr_t           attr;
  nedAttr_t           frameAttr;
  bool_t              fDirty;
  bool_t              fDirtyFrame;
  winPos_t            frameLinePicked;  // 0-n, NEDWIN_LINE_NOPICK, frame line decorator
  sNedWinDirtyRow_t  *pDirty;           // array of dirty rows (Canvas only)
  nedWinChar_t       *pText;            // nedWinChar_t includes attr/char
  struct nedWin      *pNext;            // linked list of sNedWin_t
} sNedWin_t;

typedef struct
{
  sNedWin_t          *pCurWin;      // current picked window
  sNedWin_t          *pBackdrop;    // always bottom most window Z-order (and head of window list)
  sNedWin_t          *pCanvas;      // always size of screen. Used to draw on before transfering to ANSI terminal
} sNedWinScreen_t;

static sNedWinScreen_t msNedWinScreen;

// helper private function prototypes
static bool_t         NwCheckParms      (winPos_t top, winPos_t left, winPos_t rows, winPos_t cols);
static bool_t         NwIsInitialized   (void);
static sNedWin_t     *NwFindWin         (const sNedWin_t *pWin, sNedWin_t **ppParent);
static nedWinChar_t  *NwText            (const sNedWin_t *pWin, winPos_t row, winPos_t col);
static sNedWin_t     *NwGetBackdrop     (void);
static sNedWin_t     *NwGetCanvas       (void);
static sNedWin_t     *NwGetPicked       (void);
static void           NwAddToTail       (sNedWin_t *pWin);
static void           NwClose           (sNedWin_t *pWin);
static void           NwDirtyAll        (sNedWin_t *pWin);
static void           NwDirtyPart       (sNedWin_t *pWin, winPos_t top,    winPos_t left,
                                                          winPos_t rows,   winPos_t cols);
static void           NwDirtyFrame      (sNedWin_t *pWin);
static void           NwDirtyFrameRow   (sNedWin_t *pWin, winPos_t row);
static void           NwSetClean        (sNedWin_t *pWin);
static bool_t         NwIntersect       (sNedWin_t *pWin, winPos_t top,    winPos_t left,
                                                          winPos_t *pRows, winPos_t *pCols);
static bool_t         NwCanvasIntersect (winPos_t *pTop, winPos_t *pLeft, winPos_t *pRows, winPos_t *pCols);
static void           NwCanvasDirtyUnion(winPos_t row, winPos_t left, winPos_t cols);
static void           NwFlush2Canvas    (sNedWin_t *pWin);
static void           NwFlushFrameTopBot(const sNedWin_t *pWin, bool_t fTop);
static void           NwFlushFrameEdges (const sNedWin_t *pWin, winPos_t row);
static void           NwCanvasDraw      (void);
static void           NwCanvasAllocDirty(void);
static bool_t         NwCanvasIsDirtyRow(winPos_t row, winPos_t *pLeft, winPos_t *pCols);


/*!------------------------------------------------------------------------------------------------
  Initializing the windowing system has no visible effect. Backdrop is filled with default terminal
  color.

  @ingroup  ned_win
  @return   TRUE if initialized successfully, FALSE if not.
*///-----------------------------------------------------------------------------------------------
bool_t NedWinInit(void)
{
  return NedWinInitEx(0, 0);
}

/*!------------------------------------------------------------------------------------------------
  Initializing the windowing system has no visible effect. Backdrop is filled with default color.

  backdrop - this is filled with a color or pattern to use as a backdrop to all other windows. It
  defaults to blank with the current terminal color.

      BackdropBackdropBackdrop
      Backd   No    opBackdrop
      Backd  Frame  opBackdrop
      Backd Window1 opBackdrop
      Backd     +---------+rop
      BackdropBa| Framed  |rop
      BackdropBa| Window2 |rop
      BackdropBa+---------+rop
      BackdropBackdropBackdrop

  canvas - modified upon flush, all modified window areas are transferred to the canvas, which is
  in turn sent to the screen via ANSI codes.

  @ingroup  ned_win
  @param    rows        0 or desired rows
  @param    screenCols  0 or desired cols

  @return   TRUE if initialized successfully
*///-----------------------------------------------------------------------------------------------
bool_t NedWinInitEx(winPos_t rows, winPos_t cols)
{
  bool_t      fWorked = TRUE;
  winPos_t    screenRows = 0;
  winPos_t    screenCols = 0;
  sNedWin_t  *pCanvas;
  sNedWin_t  *pBackdrop;

  NedLogPrintfEx(NEDLOG_WIN, "NedWinInit(screenRows=%u, screenCols=%u)\n", cols, rows);

  // determine size of screen, and desired (smaller) artificial screen size
  NedWinGetScreenSize(&screenRows, &screenCols);
  if((rows == 0) || (rows > screenRows))
    rows = screenRows;
  if((cols == 0) || (cols > screenCols))
    cols = screenCols;

  // already initialized, exit, and reinitialize
  if(NwIsInitialized())
  {
    NedWinExit();
  }

  // allocate canvas first, then backdrop
  pCanvas   = NedWinNewEx(0, 0, rows, cols, NEDATTR_NONE, FALSE, NEDATTR_NONE);
  pBackdrop = NedWinNewEx(0, 0, rows, cols, NEDATTR_NONE, FALSE, NEDATTR_NONE);
  if(pCanvas)
  {
    NwCanvasAllocDirty();
  }

  // not enough memory to do the windowing system
  if((pBackdrop == NULL) || (pCanvas == NULL) || (pCanvas->pDirty == NULL))
  {
    NedLogPrintfEx(NEDLOG_WIN, "  NedWinInit => Alloc failure!\n");
    fWorked = FALSE;
    NedWinExit();
  }
  else
  {
    msNedWinScreen.pCanvas   = pCanvas;
    msNedWinScreen.pBackdrop = pBackdrop;
    msNedWinScreen.pCurWin   = pBackdrop;
  }

  // log when complete
  NedLogPrintfEx(NEDLOG_WIN, "  NedWinInit => fWorked %u, rows=%u, cols=%u, pBackdrop %p, pCanvas %p\n", fWorked,
    pCanvas->rows, pCanvas->cols, NwGetBackdrop(), NwGetCanvas(), NwGetPicked());

  return fWorked;
}

/*!------------------------------------------------------------------------------------------------
  Can be called to periodically (or upon command) to adjust the internal structures to match the
  terminal screen size. Does nothing of screen size hasn't changed. If the screen was adjusted,
  call NedWinFlushAll() when done adjusting user windows as needed.

  Adjusts the size of the canvas and backdrop. Does nothing to the "user" windows. All "user"
  windows should be manually resized if desired. It's OK when windows are partially or completely
  off the screen, so more or less of a window could be exposed by the resize.

  Does nothing to screen until NedWinFLush().
  
  @ingroup  ned_win
  @param    rows   0 (use screen size) or desired rows
  @param    cols   0 (use screen size) or desired cols

  @return   TRUE if worked, FALSE if out of memory or system not initialized.
*///-----------------------------------------------------------------------------------------------
bool_t NedWinAdjustScreen(winPos_t rows, winPos_t cols)
{
  sNedWin_t  *pOldWin;
  sNedWin_t  *pCanvas = NwGetCanvas();
  winPos_t    screenRows;
  winPos_t    screenCols;

  // if system not initialized, can't adjust screen
  if(!NwIsInitialized())
    return FALSE;

  NedWinGetScreenSize(&screenRows, &screenCols);
  if((rows == 0) || (rows > screenRows))
    rows = screenRows;
  if((cols == 0) || (cols > screenCols))
    cols = screenCols;

  // if no change, nothing to do
  if((pCanvas->rows != rows) || (pCanvas->cols != cols))
  {
    NedLogPrintfEx(NEDLOG_WIN, "NedWinAdjustScreen(), new rows=%u, cols=%u, old rows=%u, cols=%u\n",
      rows, cols, pCanvas->rows, pCanvas->cols);

    // allocate memory for new canvas size
    pCanvas->rows = rows;
    pCanvas->cols = cols;
    if(pCanvas->pText)
      free(pCanvas->pText);
    pCanvas->pText = malloc(sizeof(nedWinChar_t) * rows * cols);
    NwCanvasAllocDirty();
    NedAssert(pCanvas->pText);
    NedAssert(pCanvas->pDirty);

    // resize backdrop without changing picked window
    if(NwGetBackdrop())
    {
      pOldWin = NedWinPick(NwGetBackdrop());
      if(NedWinResize(rows, cols))
        NwDirtyAll(NwGetBackdrop());
      NedWinPick(pOldWin);
    }
  }

  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Get the physical of the terminal screen. Note: this may be different than the logical size if the
  terminal has changed size since it was last adjusted with NedWinAdjustScreen().

  @ingroup  ned_win
  @param    pRows   pointer to receive screen rows (may be NULL if not needed)
  @param    pCols   pointer to receive screen columns (may be NULL if not needed)

  @return   Returns TRUE
*///-----------------------------------------------------------------------------------------------
void NedWinGetScreenSize(winPos_t *pRows, winPos_t *pCols)
{
  unsigned    rows, cols;

  AnsiGetRowsCols(&rows, &cols);
  if(pRows)
    *pRows = (winPos_t)rows;
  if(pCols)
    *pCols = (winPos_t)cols;
}

/*!------------------------------------------------------------------------------------------------
  AFFECTS SCREEN

  Free all memory and put windowing system back to start state. Does nothing to the screen.

  @ingroup  ned_win

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinExit(void)
{
  unsigned rows, cols;

  NedLogPrintfEx(NEDLOG_WIN, "NedWinExit(), pBackdrop %p, pCanvas %p\n", msNedWinScreen.pBackdrop, msNedWinScreen.pCanvas);

  // back to terminal color, cursor at bottom of screen, but don't clear screen in case user wants to still see windows
  AnsiSetAttr(NEDATTR_RESET);
  AnsiGetRowsCols(&rows, &cols);
  AnsiGoto(rows - 1, 0);

  // free all windows (even if partially initialized)
  if(NwGetBackdrop())
  {
    while(NwGetBackdrop()->pNext != NULL)
      NwClose(NwGetBackdrop()->pNext);
    NwClose(NwGetBackdrop());
  }
  if(NwGetCanvas() != NULL)
    NwClose(NwGetCanvas());
  memset(&msNedWinScreen, 0, sizeof(msNedWinScreen));
}

/*!------------------------------------------------------------------------------------------------
  AFFECTS SCREEN

  Clear the backdrop to a color. Uses ANSI clear screen under the hood. Windows aren't flushed
  until the next NedWinFlush() to allow for other window adjustments.

  Does not change which window is selected.

  @ingroup  ned_win

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinClearScreen(nedAttr_t attr)
{
  sNedWin_t   *pOldWin;

  // NedLogPrintfEx(NEDLOG_WIN, "NedWinClearScreen(attr=0x%02x)\n", attr);

  // clear physical screen (backdrop and canvas are also cleared)
  AnsiGoto(0, 0);
  AnsiSetAttr(attr);
  AnsiClearScreen();

  // clear backdrop if system is initialized
  if(NwIsInitialized())
  {
    NedAssertDbg((NwGetCanvas()->rows == NwGetBackdrop()->rows) && (NwGetCanvas()->cols == NwGetBackdrop()->cols));

    // clear the backdrop (which will cause all windows to be redrawn at flush)
    pOldWin = NwGetPicked();
    {
      NedWinPickBackdrop();
      NedWinSetAttr(attr);
      NedWinGoto(0,0);
      NedWinClearAll();
    }
    NedWinPick(pOldWin);
  }
}

/*!------------------------------------------------------------------------------------------------
  AFFECTS SCREEN

  Flush all changes to the screen. Also updates physical cursor position to current window cursor
  position. Draws all windows into the canvas before flushing canvas efficiently to screen.

  To redraw everything, use NedWinFlushAll().

  @ingroup  ned_win

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinFlush(void)
{
  sNedWin_t   *pWin;
  sNedWin_t   *pDirtyWin = NULL;

  NedLogPrintfEx(NEDLOG_WIN, "NedWinFlush()\n");
  NedAssert(NwIsInitialized());

  // NedWinLogDumpCanvas();

  // find bottom-most dirty window. Flush to canvas each window in the dirty regions of the canvas
  pWin = NedWinGetBackdrop();
  do
  {
    // can't do anything if backdrop not yet defined.
    NedAssertDbg(NedWinIsWin(pWin));

    // if dirty, this is the bottom-most dirty
    if(pWin->fDirty || pWin->fDirtyFrame)
    {
      pDirtyWin = pWin;
      break;
    }

    // try next window toward top
    pWin = pWin->pNext;
  } while(pWin);

  NedLogPrintfEx(NEDLOG_WIN, "  pDirtyWin=%s\n", pDirtyWin ? NedWinLogName(pDirtyWin) : "No Dirty");

  // if no dirty windows, nothing to do
  if(pDirtyWin != NULL)
  {
    // flush each window and frame that overlap the dirty canvas region
    pWin = pDirtyWin;
    while(pWin != NULL)
    {
      NwFlush2Canvas(pWin);
      pWin = pWin->pNext;
    }
    NwCanvasDraw();
  }
}

/*!------------------------------------------------------------------------------------------------
  AFFECTS SCREEN

  Redraw everything

  @ingroup  ned_win

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinFlushAll(void)
{
  NwDirtyAll(NwGetBackdrop());
  NedWinFlush();
}

/*!------------------------------------------------------------------------------------------------
  AFFECTS SCREEN

  Flushes cursor to current picked window cursor position. If this is off-screen, does nothing.

  @ingroup  ned_win

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinFlushCursor(void)
{
  sNedWin_t    *pWin = NwGetPicked();
  winPos_t      row;
  winPos_t      col;

  NedAssert(NedWinIsWin(pWin));
  row = pWin->top  + pWin->crow;
  col = pWin->left + pWin->ccol;
  NedLogPrintfEx(NEDLOG_WIN, "NedWinFlushCursor(pWin %p, row=%d, col=%d)\n", pWin, row, col);
  if((row >= 0) && (row < NwGetCanvas()->rows) && (col >= 0) && (col < NwGetCanvas()->cols))
    AnsiGoto((unsigned)row, (unsigned)col);
  AnsiSetAttr(pWin->attr);
  fflush(stdout);
}

/*!------------------------------------------------------------------------------------------------
  Creates a new window on top with the same color as the terminal.

  @ingroup  ned_win

  @param    top   top of window (0-n)
  @param    left  left of window (0-n)
  @param    rows  rows in window (1-n)
  @param    cols  column in window (1-n)

  @return   NULL if failed, pointer to the window successful
*///-----------------------------------------------------------------------------------------------
void * NedWinNew(winPos_t top, winPos_t left, winPos_t rows, winPos_t cols)
{
  return NedWinNewEx(top, left, rows, cols, NEDATTR_NONE, FALSE, NEDATTR_NONE);
}

/*!------------------------------------------------------------------------------------------------
  Creates a new window on top. Refuses to create window less than minimum size as defined by
  NEDWIN_MIN_ROWS, NEDWIN_MIN_COLS. Could also fail if out of memory. The window has no frame. Use
  NedWinFrameSet() to add a frame to the window.

  @ingroup  ned_win

  @param    top         top of window (0-n)
  @param    left        left of window (0-n)
  @param    rows        rows in window (1-n)
  @param    cols        column in window (1-n)
  @param    attr        attribute of window text
  @param    fFramed     attribute of window text
  @param    frameAttr   attribute of window text

  @return   NULL if failed, pointer to the window successful
*///-----------------------------------------------------------------------------------------------
void * NedWinNewEx(winPos_t top, winPos_t left, winPos_t rows, winPos_t cols,
                 nedAttr_t attr, bool_t fFramed, nedAttr_t frameAttr)
{
  sNedWin_t  *pWin;

  NedLogPrintfEx(NEDLOG_WIN, "NedWinNew(top=%u, left=%u, rows=%u, cols=%u, attr %02x, framed %u, fattr %02x)\n",
    top, left, rows ,cols, attr, fFramed, frameAttr);

  // invalid valid parameters, return NULL
  if(!NwCheckParms(top, left, rows, cols))
    return NULL;

  // allocate the window
  pWin = malloc(sizeof(sNedWin_t));
  if(pWin)
  {
    // special startup case: canvas is needed for many functions...
    if(NwGetCanvas() == NULL)
      msNedWinScreen.pCanvas = pWin;

    // initialze basic window structure
    memset(pWin, 0, sizeof(sNedWin_t));
    pWin->sanchk = WIN_SANCHK;
    NedAssert(NedWinIsWin(pWin));

    // allocate room for text
    pWin->pText = malloc(sizeof(nedWinChar_t) * rows * cols);
    if(pWin->pText == NULL)
    {
      NedLogPrintfEx(NEDLOG_WIN, "!FAILED! NedWinNew(top=%u, left=%u, rows=%u, cols=%u)\n",
        top, left, rows, cols);
      if(pWin == msNedWinScreen.pCanvas)
        msNedWinScreen.pCanvas = NULL;
      free(pWin);
      pWin = NULL;
    }
  }

  if(pWin)
  {
    NedLogPrintfEx(NEDLOG_WIN, "  pWin=%p, pText=%p, pDirty=%p)\n", pWin, pWin->pText, pWin->pDirty);

    // fill in rest of window structure
    pWin->top             = top;
    pWin->left            = left;
    pWin->rows            = rows;
    pWin->cols            = cols;
    pWin->fFramed         = fFramed;
    pWin->attr            = attr;
    pWin->frameAttr       = frameAttr;
    pWin->frameLinePicked = NEDWIN_LINE_NOPICK;
    pWin->pDirty          = NULL;

    // once system is initialized, add new windows to linked list. The new window is on top (last in list).
    if(NwIsInitialized())
    {
      NwAddToTail(pWin);
    }

    // clear the entire window to selected win and frame attributes
    NedWinPick(pWin);
    NedWinClearAll();
    NwDirtyAll(pWin);
  }

  NedLogPrintfEx(NEDLOG_WIN, "  window created\n");

  return pWin;
}

/*!------------------------------------------------------------------------------------------------
  Is this a window?

  @ingroup  ned_win

  @return   returns TRUE if this is a window
*///-----------------------------------------------------------------------------------------------
bool_t NedWinIsWin(const void *pWin)
{
  const sNedWin_t *pWinThis = pWin;
  return (pWinThis && (pWinThis->sanchk == WIN_SANCHK)) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Close all user windows. Doesn't close backdrop.

  @ingroup  ned_win

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinCloseAll(void)
{
  if(NwIsInitialized())
  {
    while(NwGetBackdrop()->pNext != NULL)
      NedWinClose(NwGetBackdrop()->pNext);
  }
}

/*!------------------------------------------------------------------------------------------------
  Close a window (and free it's memory). Remove the window from the window list. Not allowed to
  close the backdrop. Cannot close the backdrop.

  @ingroup  ned_win
  @param    pWin        user window to close.
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinClose(void *pWin)
{
  NedLogPrintfEx(NEDLOG_WIN, "NedWinClose(%p)\n", pWin);
  if(NedWinIsWin(pWin) && (pWin != NedWinGetBackdrop()))
  {
    // window is going away, pick backdrop instead
    if(NwGetPicked() == pWin)
      NedWinPickBackdrop();

    // when the window goes away, backdrop or any other window may be exposed in the region of this window
    NwGetBackdrop()->fDirty = TRUE;
    NwDirtyAll(pWin);

    NwClose(pWin);
    NedLogPrintfEx(NEDLOG_WIN, "  closed\n");
  }
}

/*!------------------------------------------------------------------------------------------------
  Pick a window for operations like NedWinPuts() or NedWinGoto(). Does not change Z order of
  windows.

  @ingroup  ned_win
  @param    pWin    valid window from NedWinNew()

  @return   pointer to previously picked window
*///-----------------------------------------------------------------------------------------------
void * NedWinPick(void *pWin)
{
  void * pOldWin = NwGetPicked();
  if(NedWinIsWin(pWin))
    msNedWinScreen.pCurWin = pWin;
  return pOldWin;
}

/*!------------------------------------------------------------------------------------------------
  Get handle to the current picked window. Thers is ALWAYS a currently picked window, even if it is
  only the backdrop.

  @ingroup  ned_win

  @return   Returns pointer to current window
*///-----------------------------------------------------------------------------------------------
void * NedWinGetPicked(void)
{
  NedAssert(NedWinIsWin(NwGetPicked()));
  return NwGetPicked();
}

/*!------------------------------------------------------------------------------------------------
  Pick the backdrop window as the current window.

  @ingroup  ned_win

  @return   none
*///-----------------------------------------------------------------------------------------------
void *  NedWinPickBackdrop(void)
{
  return NedWinPick(NedWinGetBackdrop());
}

/*!------------------------------------------------------------------------------------------------
  Get handle to the backdrop window.

  @ingroup  ned_win

  @return   pointer to backdrop window
*///-----------------------------------------------------------------------------------------------
void * NedWinGetBackdrop(void)
{
  return NwGetBackdrop();
}

/*!------------------------------------------------------------------------------------------------
  Get ptr to the backdrop window.

  @ingroup  ned_win

  @return   pointer to canvas window
*///-----------------------------------------------------------------------------------------------
static sNedWin_t * NwGetBackdrop(void)
{
  return msNedWinScreen.pBackdrop;
}

/*!------------------------------------------------------------------------------------------------
  Get ptr to the canvas pseudo window.

  @ingroup  ned_win

  @return   pointer to canvas window
*///-----------------------------------------------------------------------------------------------
static sNedWin_t * NwGetCanvas(void)
{
  return msNedWinScreen.pCanvas;
}

/*!------------------------------------------------------------------------------------------------
  Get ptr to the current picked window.

  @ingroup  ned_win

  @return   pointer to canvas window
*///-----------------------------------------------------------------------------------------------
static sNedWin_t * NwGetPicked(void)
{
  return msNedWinScreen.pCurWin;
}

/*!------------------------------------------------------------------------------------------------
  Hide the current window. Same as moving it off-screen.

  @ingroup  ned_win

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinHide(void)
{
  sNedWin_t * pWin = NwGetPicked();

  // can't hide backdrop
  if(pWin != NwGetBackdrop())
  {
    if(pWin->top != NEDWIN_MAX_POS || pWin->left != NEDWIN_MAX_POS)
    {
      pWin->oldTop  = pWin->top;
      pWin->oldLeft = pWin->left;
      NedWinMove(NEDWIN_MAX_POS, NEDWIN_MAX_POS);
    }
  }
}

/*!------------------------------------------------------------------------------------------------
  Move the current window to the front of of the list (on top). Nothing changes on screen until
  next NedWinFlush().

  @ingroup  ned_win

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinUnhide(void)
{
  sNedWin_t * pWin = NwGetPicked();

  // can't hide backdrop
  if(pWin != NwGetBackdrop())
  {
    if(pWin->top == NEDWIN_MAX_POS && pWin->left == NEDWIN_MAX_POS)
    {
      NedWinMove(pWin->oldTop, pWin->oldLeft);
    }
  }
}

/*!------------------------------------------------------------------------------------------------
  Move the current window to the front of of the list (on top). Nothing changes on screen until
  next NedWinFlush().

  @ingroup  ned_win

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinFront(void)
{
  sNedWin_t   *pWin = NwGetPicked();
  sNedWin_t   *pParent;

  NedLogPrintfEx(NEDLOG_WIN, "NedWinFront(pWin=%p)\n", pWin);

  // can't move background to the top
  if(pWin != NwGetBackdrop())
  {
    pWin = NwFindWin(pWin, &pParent);

    // if not found or already at top, do nothing
    if(pWin && (pWin->pNext != NULL))
    {
      NwDirtyAll(pWin);

      // link window into new position
      if(pParent)
      {
        pParent->pNext = pWin->pNext;
        NwAddToTail(pWin);
      }
    }
  }
}

/*!------------------------------------------------------------------------------------------------
  Add or remove the frame around the window contents.

  @ingroup  ned_win

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinFrameSet(bool_t fFramed)
{
  sNedWin_t * pWin = NwGetPicked();
  if(pWin != NwGetBackdrop())
  {
    pWin->fFramed = fFramed;
    NwDirtyFrame(pWin);
    if(!fFramed)
      NwGetBackdrop()->fDirty = TRUE;
  }
}

/*!------------------------------------------------------------------------------------------------
  Return whether the current window is framed or not.

  @ingroup  ned_win

  @return   TRUE if framed.
*///-----------------------------------------------------------------------------------------------
bool_t NedWinIsFramed(void)
{
  return NwGetPicked()->fFramed;
}

/*!------------------------------------------------------------------------------------------------
  Pick a frame line. The visual effect is a square bracket instead of a vertical bar frame for the
  picked line. Useful in menus if there is no color. Example, row 1 is picked:

      +--------+
      | row  0 |
      [ row  1 ]
      | row  2 |
      +--------+

  @ingroup  ned_win
  @param    row       0-n = picked row, NEDWIN_LINE_NOPICK

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinFrameLinePick(winPos_t row)
{
  sNedWin_t * pWin = NwGetPicked();

  // can only pick a line if window is framed
  if(pWin->fFramed)
  {
    // assume invalid rows are no-pick
    if(row >= pWin->rows)
      row = NEDWIN_LINE_NOPICK;

    // pick the new row, and dirty both old and new
    if((pWin->frameLinePicked != NEDWIN_LINE_NOPICK) && (pWin->frameLinePicked < pWin->rows))
      NwDirtyFrameRow(pWin, pWin->frameLinePicked);
    if(row != NEDWIN_LINE_NOPICK)
      NwDirtyFrameRow(pWin, row);

    pWin->frameLinePicked = row;
  }
}

/*!------------------------------------------------------------------------------------------------
  Resize the current window. Window may be off the visible screen. If reduced, then some text will
  be lost. If increased, then text will be preserved, and new area filled with spaces.

  Windows must at least be 1 x 1 in size.

  @ingroup  ned_win
  @param    rows (1-n)
  @param    cols (1-n)

  @return   TRUE if resize worked. FALSE if not (old size left intact).
*///-----------------------------------------------------------------------------------------------
bool_t NedWinResize(winPos_t rows, winPos_t cols)
{
  (void)rows;
  sNedWin_t          *pWin = NwGetPicked();
  nedWinChar_t       *pNewText;
  winPos_t            i;
  winPos_t            copyRows;
  winPos_t            copyCols;
  bool_t              fWorked = TRUE;

  NedLogPrintfEx(NEDLOG_WIN, "NedWinResize(pWin %p, rows=%u, cols=%u)\n", pWin, rows, cols);

  // invalid parameters
  if(!NedWinIsWin(pWin) || (rows < 1) || (cols < 1) || (rows > INT_MAX/2) || (cols > INT_MAX/2))
  {
    NedLogPrintfEx(NEDLOG_WIN, "  !FAILED! bad parm\n");
    return FALSE;
  }

  // only resize if valid window and size has changed
  if((pWin->rows != rows) || (pWin->cols != cols))
  {
    // determine how much to copy from old to new window
    copyRows = rows;
    if(copyRows > pWin->rows)
      copyRows = pWin->rows;
    copyCols = cols;
    if(copyCols > pWin->cols)
      copyCols = pWin->cols;

    // allocate new window and copy into it, clearing part if window grew
    pNewText = malloc(sizeof(nedWinChar_t) * rows * cols);
    if(pNewText == NULL)
    {
      NedLogPrintfEx(NEDLOG_WIN, "  !FAILED! malloc\n");
      fWorked = FALSE;
    }
    else
    {
      NedLogPrintfEx(NEDLOG_WIN, "  rows=%u, cols=%u, oldRows=%u, oldCols=%u, copyRows=%u, copyCols=%u, attr 0x%02x, pWin=%p\n",
        rows, cols, pWin->rows, pWin->cols, copyRows, copyCols, pWin->attr, pWin);

      // copy in the existing text
      for(i=0; i<copyRows; ++i)
      {
        // copy each row separately as line length is different
        memcpy(&pNewText[i * cols], &pWin->pText[i * pWin->cols], copyCols * sizeof(nedWinChar_t));
      }
      free(pWin->pText);

      // update win structure
      pWin->pText   = pNewText;
      pWin->rows    = rows;
      pWin->cols    = cols;

      // clear the new space and dirty entire window, as it's adjusted
      NwSetClean(pWin);
      if(copyRows < rows)
        NedWinClearPart(copyRows, 0, rows - copyRows, cols);
      if(copyCols < cols)
        NedWinClearPart(0, copyCols, copyRows, cols - copyCols);
      NwDirtyAll(pWin);
    }
  }

  return fWorked;
}

/*!------------------------------------------------------------------------------------------------
  Move the window to a new location, which may be off-screen. Has no screen effect until
  NedWinFlush(). Cannot move backdrop.

  @ingroup  ned_win
  @param    top   new top of current window (0-n)
  @param    left  new left of current window (0-n)

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinMove(int top, int left)
{
  sNedWin_t   *pWin     = NwGetPicked();

  NedLogPrintfEx(NEDLOG_WIN, "NedWinMove(pWin=%s, top=%u,left=%u)\n", NedWinLogName(pWin), top, left);
  NwDirtyAll(pWin);
  NwDirtyFrame(pWin);
  pWin->top   = top;
  pWin->left  = left;
  NwDirtyAll(pWin);
  NedLogPrintfEx(NEDLOG_WIN, "  Canvas After:\n");
  //NedWinLogDumpWin(NwGetCanvas());
}

/*!------------------------------------------------------------------------------------------------
  Get number of rows and columns in the current window.

  @ingroup  ned_win
  @param    pRows   Get number of rows in window (may be NULL if not interested)
  @param    pCols   Get number of cols in window (may be NULL if not interested)
  
  @return   none
*///------------------------------------------------------------------------------------------------
void NedWinGetSize(winPos_t *pRows, winPos_t *pCols)
{
  sNedWin_t   *pWin = NwGetPicked();
  if(pRows)
    *pRows = pWin->rows;
  if(pCols)
    *pCols = pWin->cols;
}

/*!------------------------------------------------------------------------------------------------
  Get current top/left of the current window

  @ingroup  ned_win
  @param    pTop    Get the top of the window (may be NULL if not interested)
  @param    pLeft   Get the left of the window (may be NULL if not interested)
  
  @return   none
*///------------------------------------------------------------------------------------------------
void NedWinGetPos(winPos_t *pTop, winPos_t *pLeft)
{
  sNedWin_t   *pWin = NwGetPicked();
  if(pTop)
    *pTop  = pWin->top;
  if(pLeft)
    *pLeft = pWin->left;
}

/*!------------------------------------------------------------------------------------------------
  Get current top of the current window

  @return   none
*///------------------------------------------------------------------------------------------------
int NedWinGetTop(void)
{
  return NwGetPicked()->top;
}

/*!------------------------------------------------------------------------------------------------
  Get current left of the current window

  @return   none
*///------------------------------------------------------------------------------------------------
int NedWinGetLeft(void)
{
  return NwGetPicked()->left;
}

/*!------------------------------------------------------------------------------------------------
  Get # of rows in the current window

  @return   none
*///------------------------------------------------------------------------------------------------
int NedWinGetRows(void)
{
  return NwGetPicked()->rows;
}

/*!------------------------------------------------------------------------------------------------
  Get # of columns in the current window

  @return   none
*///------------------------------------------------------------------------------------------------
int NedWinGetCols(void)
{
  return NwGetPicked()->cols;
}

/*!------------------------------------------------------------------------------------------------
  Move cursor to a new position in the window (can even be outside window boundaries). Doesn't move
  physical cursor. Use NedFlushCursor() for that.

  @param    row   new cursor row position in window (0-n)
  @param    col   new cursor column position in window (0-n)
  
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinGoto(winPos_t row, winPos_t col)
{
  sNedWin_t   *pWin = NwGetPicked();
  pWin->crow = row;
  pWin->ccol = col;
}

/*!-----------------------------------------------------------------------------------------------
  Get cursor position within current window. That is, where NedWinPutc() would place the char.

  @param    pRow   pointer to receive row (can be NULL if row not needed)
  @param    pCol   pointer to receive column (can be NULL if col not needed)
  
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinGetCursor(winPos_t *pRow, winPos_t *pCol)
{
  sNedWin_t   *pWin = NwGetPicked();
  if(pRow)
    *pRow = pWin->crow;
  if(pCol)
    *pCol = pWin->ccol;
}

/*!------------------------------------------------------------------------------------------------
  Print a character at the current cursor position in the window buffer. Understands '\n' which
  fills from cursor position to end of line with current attribute. All other non-printables are
  ignored, including tab. Nothing is displayed on the screen until NedWinFlush().

  @param    c    character to print
  
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinPutc(char c)
{
  sNedWin_t *pWin = NwGetPicked();

  if(c == '\n')
  {
    // fills the rest of line with spaces
    if((pWin->crow < pWin->rows) && (pWin->ccol < pWin->cols))
      NedWinClearEol();
    pWin->ccol = 0;
    // scrolling terminal window
    if(pWin->crow == (pWin->rows - 1))
      NedWinScrollAll(NEDWIN_DIR_UP);
    else
      ++pWin->crow;
  }
  else if(isprint(c))
  {
    if((pWin->crow < pWin->rows) && (pWin->ccol < pWin->cols))
    {
      NwDirtyPart(pWin, pWin->crow, pWin->ccol, 1, 1);
      *NwText(pWin, pWin->crow, pWin->ccol) = NedWinCharMake(pWin->attr,c);
    }
    if(pWin->ccol < NEDWIN_MAX_COLS)
      ++pWin->ccol;
  }
}

/*!-----------------------------------------------------------------------------------------------
  Print text in the window at the current position. Any text printed outside window boundaries is
  thrown away. Nothing is displayed on the screen until NedWinFlush().

  @ingroup  ned_win
  @param    sz    string to print
  
  @return   length of string
*///-----------------------------------------------------------------------------------------------
unsigned NedWinPuts(const char *sz)
{
  unsigned  len = strlen(sz);
  unsigned  i;

  NedLogPrintfEx(NEDLOG_WIN, "NedWinPuts(s=%s, len %u)\n", sz, len);
  for(i=0; i<len; ++i)
    NedWinPutc(sz[i]);

  return len;
}

/*!------------------------------------------------------------------------------------------------
  Same as vprintf() for NedWindows. Limited to NEDWIN_MAX_COLS (1200) characters.

  @ingroup  ned_win
  @param    szFormat    control string, followed by 0 or more parameters.
  @param    arglist     from va_start()
  
  @return   none
*///-----------------------------------------------------------------------------------------------
unsigned NedWinVPrintf(const char *szFormat, va_list arglist)
{
  static char szTmpLine[NEDWIN_MAX_COLS+1];
  int         intLen;
  unsigned    len = 0;
  unsigned    i;

  intLen = (unsigned)vsnprintf(szTmpLine, sizeof(szTmpLine)-1, szFormat, arglist);
  if(intLen > 0)
  {
    len = (unsigned)intLen;
    szTmpLine[len] = '\0';
    // NedLogPrintfEx(NEDLOG_WIN, "NedWinVPrintf(s=%s)\n", szTmpLine);

    for(i=0; i<len; ++i)
      NedWinPutc(szTmpLine[i]);
  }
  return len;
}

/*!------------------------------------------------------------------------------------------------
  Same as printf() but to NedWindows. Limited to NEDWIN_MAX_COLS (1200) characters.

  @ingroup  ned_win
  @param    szFormat    control string, followed by 0 or more parameters.
  
  @return   none
*///-----------------------------------------------------------------------------------------------
unsigned NedWinPrintf(const char *szFormat, ...)
{
  va_list     arglist;
  unsigned    len;

  va_start(arglist, szFormat);
  len = NedWinVPrintf(szFormat, arglist);
  va_end(arglist);

  return len;
}

/*!------------------------------------------------------------------------------------------------
  Clear the current window with the current attribute

  @ingroup  ned_win

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinClearAll(void)
{
  NedWinFillAll(' ');
}

/*!------------------------------------------------------------------------------------------------
  Clear part of the the current window with the current attribute.

  @ingroup  ned_win
  @param    top     top of part to clear (0-n)
  @param    left    left of part to clear (0-n)
  @param    rows    number of rows to clear (1-n)
  @param    cols    number of columns to clear (1-n)

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinClearPart(winPos_t top, winPos_t left, winPos_t rows, winPos_t cols)
{
  NedWinFillPart(top, left, rows, cols, ' ');
}

/*!------------------------------------------------------------------------------------------------
  Clear to end of line (if cursor position within window). Does not change cursor position.

  @ingroup  ned_win

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinClearEol(void)
{
  sNedWin_t *pWin = NwGetPicked();
  if((pWin->crow < pWin->rows) && (pWin->ccol < pWin->cols))
  {
    NedWinClearPart(pWin->crow, pWin->ccol, 1, pWin->cols - pWin->ccol);
  }
}

/*!------------------------------------------------------------------------------------------------
  Fill entire window with a character with current attribute. Does not affect cursor position.

  @ingroup  ned_win
  @param    c       Character with which to fill the window

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinFillAll(char c)
{
  sNedWin_t *pWin = NwGetPicked();
  NedAssertDbg(NedWinIsWin(pWin));
  NedWinFillPart(0, 0, pWin->rows, pWin->cols, c);
}

/*!------------------------------------------------------------------------------------------------
  Fill entire window with string. String must be NULL terminated with only printable characters.

  @ingroup  ned_win
  @param    sz      string with which to fill window
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinFillStr(const char *sz)
{
  sNedWin_t *pWin = NwGetPicked();
  winPos_t  row;
  winPos_t  col;

  NedAssertDbg(NedWinIsWin(pWin));
  // NedLogPrintfEx(NEDLOG_WIN, "NedWinFillStr(pWin=%p, top=%u, left=%u, sz='%s'\n", pWin, pWin->top, pWin->left, sz);
  for(row=0; row < pWin->rows; ++row)
  {
    col = 0;
    NedWinGoto(row, 0);
    while(col < pWin->cols)
      col += NedWinPuts(sz);
  }
}

/*!------------------------------------------------------------------------------------------------
  Fill part of window with a character with current attribute. Does not affect cursor position. Use
  NedWinFlush() to flush to screen.

  @ingroup  ned_win
  @param    top     top of part to fill (0-n)
  @param    left    left of part to fill (0-n)
  @param    rows    number of rows to fill (1-n)
  @param    cols    number of columns to fill (1-n)
  @param    c       Character with which to fill the part of the window

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinFillPart(winPos_t top, winPos_t left, winPos_t rows, winPos_t cols, char c)
{
  sNedWin_t      *pWin = NwGetPicked();
  nedWinChar_t   *pText;
  nedWinChar_t    nc;
  winPos_t        i;
  winPos_t        j;

  NedAssertDbg(NedWinIsWin(pWin));

  NedLogPrintfEx(NEDLOG_WIN, "NedWinFillPart(top=%u, left=%u, rows=%u, cols=%u, c='%c', attr=0x%02x)\n",
                   top,left,rows,cols,c,pWin->attr);

  // must be printable and within window boundaries
  nc = NedWinCharMake(pWin->attr,c);
  if( isprint(c) && NwIntersect(pWin, top, left, &rows, &cols) )
  {
    // NedLogPrintfEx(NEDLOG_WIN, "  intersect rows=%u, cols=%u\n", rows, cols);
    NwDirtyPart(pWin, top, left, rows, cols);
    for(i=0; i<rows; ++i)
    {
      pText = NwText(pWin, top+i, left);
      for(j=0; j<cols; ++j)
      {
        *pText++ = nc;
      }
    }
  }
}

/*!------------------------------------------------------------------------------------------------
  Get current attribute in window.

  @ingroup  ned_win

  @return   current attribute
*///-----------------------------------------------------------------------------------------------
nedAttr_t NedWinGetAttr(void)
{
  return NwGetPicked()->attr;
}

/*!-------------------------------------------------------------------------------------------------
  Set current attribute in window for future printing in window.

  @ingroup  ned_win
  @param    attr      Attribute for future printing in window

  @return   current attribute
*///-----------------------------------------------------------------------------------------------
void NedWinSetAttr(nedAttr_t attr)
{
  NwGetPicked()->attr = attr;
}

/*!------------------------------------------------------------------------------------------------
  Get attribute of the frame surrounding the window. See ::nedWinFramed_t.

  @ingroup  ned_win

  @return   current attribute
*///-----------------------------------------------------------------------------------------------
nedAttr_t NedWinFrameGetAttr(void)
{
  return NwGetPicked()->frameAttr;
}

/*!------------------------------------------------------------------------------------------------
  Set frame attribute. Has no visible affect if window not framed. Does nothing to the backdrop (as
  backdrop is never framed).

  @ingroup  ned_win
  @param    attr      Attribute for the frame surrounding the window.

  @return   nothing
*///-----------------------------------------------------------------------------------------------
void NedWinFrameSetAttr(nedAttr_t attr)
{
  sNedWin_t   *pWin = NwGetPicked();

  if(pWin != NedWinGetBackdrop())
  {
    pWin->frameAttr   = attr;
    pWin->fFramed     = TRUE;
    NwDirtyFrame(pWin);
  }
}

/*!------------------------------------------------------------------------------------------------
  Immediately change attribute of all characters in the window. Also changes current attribute for
  future printing.

  @ingroup  ned_win

  @return   current attribute
*///-----------------------------------------------------------------------------------------------
void NedWinAttrFillAll(nedAttr_t attr)
{
  sNedWin_t    *pWin = NwGetPicked();
  NedWinSetAttr(attr);
  NedWinAttrFillPart(0, 0, pWin->rows, pWin->cols, attr);
}

/*!------------------------------------------------------------------------------------------------
  Immediately change attribute of all characters the window range. Does NOT change current
  attribute for future printing.

  @ingroup  ned_win
  @param    top     top of part to set attibute (0-n)
  @param    left    left of part to set attibute (0-n)
  @param    rows    number of rows to set attibute (1-n)
  @param    cols    number of columns to set attibute (1-n)
  @param    attr    attribute. See ::nedAttr_t.

  @return   current attribute
*///-----------------------------------------------------------------------------------------------
void NedWinAttrFillPart(winPos_t top, winPos_t left, winPos_t rows, winPos_t cols, nedAttr_t attr)
{
  sNedWin_t      *pWin = NwGetPicked();
  char            c;
  winPos_t        i;
  winPos_t        j;
  nedWinChar_t   *pText;

  // set attribute for this rectangle in the window
  if(NwIntersect(pWin, top, left, &rows, &cols))
  {
    NwDirtyPart(pWin, top, left, rows, cols);
    for(i=0; i<rows; ++i)
    {
      pText = NwText(pWin, top+i, left);
      for(j=0; j<cols; ++j)
      {
        c = NedWinChar(*pText);
        *pText++ = NedWinCharMake(attr,c);
      }
    }
  }
}

/*!------------------------------------------------------------------------------------------------
  Scroll the contents of the window up or down by one line. Leaves the exposed row blank with the
  current attribute.

  @ingroup  ned_win
  @param    dir    direction to scroll, NEDWIN_DIR_UP, NEDWIN_DIR_DOWN. See ::nedWinDir_t.

  @return   current attribute
*///-----------------------------------------------------------------------------------------------
void NedWinScrollAll(nedWinDir_t dir)
{
  sNedWin_t    *pWin = NwGetPicked();
  NedWinScrollPart(pWin->top, pWin->left, pWin->rows, pWin->cols, dir);
}

/*!------------------------------------------------------------------------------------------------
  Scroll a portion of the window up or down by one line. Leaves the exposed row blank with the
  current attribute.

  @ingroup  ned_win
  @param    top    top of part to scroll (0-n)
  @param    left   left of part to scroll (0-n)
  @param    rows   number of rows to scroll (1-n)
  @param    cols   number of columns to scroll (1-n)
  @param    dir    direction to scroll, NEDWIN_DIR_UP, NEDWIN_DIR_DOWN. See ::nedWinDir_t.

  @return   current attribute
*///-----------------------------------------------------------------------------------------------
void NedWinScrollPart(winPos_t top, winPos_t left, winPos_t rows, winPos_t cols, bool_t dir)
{
  sNedWin_t      *pWin = NwGetPicked();
  nedWinChar_t   *pText;
  winPos_t        i;

  NedAssertDbg(NedWinIsWin(pWin));

  // gets the intersection of the window with these coordinates
  if(NwIntersect(pWin, top, left, &rows, &cols))
  {
    // that region is now dirty
    NwDirtyPart(pWin, top, left, rows, cols);

    // scroll up
    if(dir == NEDWIN_DIR_UP)
    {
      if(rows > 1)
      {
        pText = NwText(pWin, top, left);
        for(i=top; i<rows-1; ++i)
        {
          memcpy(pText, &pText[pWin->cols], cols*sizeof(nedWinChar_t));
          pText += pWin->cols;
        }
      }
      NedWinClearPart(top+rows-1, left, 1, cols);
    }

    // scroll down
    else
    {
      if(rows > 1)
      {
        pText = NwText(pWin, (top + rows - 1), left);
        for(i=top; i<rows-1; ++i)
        {
          memcpy(pText, pText - pWin->cols, cols*sizeof(nedWinChar_t));
          pText -= pWin->cols;
        }
      }
      NedWinClearPart(top,left,1,cols);
    }
  }
}

/*-------------------------------------------------------------------------------------------------
  !PRIVATE! FUNCTIONS
-------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------
  Verify system is initialized
-------------------------------------------------------------------------------------------------*/
static bool_t NwIsInitialized(void)
{
  return ((NwGetCanvas() != NULL) && (NwGetBackdrop() != NULL)) ? TRUE : FALSE;
}

/*-------------------------------------------------------------------------------------------------
  Verify parameters of window are OK. Returns TRUE if OK, FALSE if not.
-------------------------------------------------------------------------------------------------*/
static bool_t NwCheckParms(winPos_t top, winPos_t left, winPos_t rows, winPos_t cols)
{
  if(top < NEDWIN_MIN_POS  || top > NEDWIN_MAX_POS)
    return FALSE;
  if(left < NEDWIN_MIN_POS || left > NEDWIN_MAX_POS)
    return FALSE;
  if(rows < 1              || rows > NEDWIN_MAX_ROWS)
    return FALSE;
  if(cols < 1              || cols > NEDWIN_MAX_COLS)
    return FALSE;
  return TRUE;
}

/*-------------------------------------------------------------------------------------------------
  Allocates space for keeping track of the dirty regions in each row of the canvas. All rows
  are set to not dirty.
-------------------------------------------------------------------------------------------------*/
static void NwCanvasAllocDirty(void)
{
  sNedWin_t *pCanvas = NwGetCanvas();
  if(pCanvas)
  {
    if(pCanvas->pDirty != NULL)
      free(pCanvas->pDirty);
    pCanvas->pDirty = malloc(sizeof(sNedWinDirtyRow_t) * pCanvas->rows);
    if(pCanvas->pDirty != NULL)
      memset(pCanvas->pDirty, 0, sizeof(sNedWinDirtyRow_t) * pCanvas->rows);
  }
}

/*-------------------------------------------------------------------------------------------------
  Internal function closes a window. Just frees memory and removes from list if needed. Higher
  functions must dirty backdrop or change picked window if needed.
-------------------------------------------------------------------------------------------------*/
static void NwClose(sNedWin_t *pWin)
{
  sNedWin_t    *pFindWin  = NULL;
  sNedWin_t    *pParent   = NULL;

  NedLogPrintfEx(NEDLOG_WIN, "NwClose(%s)\n", NedWinLogName(pWin));

  // remove user window from list (if in the list)
  if(NedWinIsWin(pWin))
  {
    // if user window, then remove from linked list
    pFindWin = NwFindWin(pWin, &pParent);
    if(pFindWin && pParent)
      pParent->pNext = pWin->pNext;

    // free the memory for the window
    if(pWin->pText)
      free(pWin->pText);
    if(pWin->pDirty)
      free(pWin->pDirty);
    free(pWin);
  }
}

/*-------------------------------------------------------------------------------------------------
  Create diry union on the canvas from this dirty row. Helper to make sure everything drawn on
  the canvas is later flushed to screen. All coordinates in canvas coordinates.

    old:      |____|
    this:                 |_|
    union:    |_____________|

  row         screen row to make dirty union
  thisLeft    screen coordinates left for new
  thisCols    number of dirty columns for new

  returns none
-------------------------------------------------------------------------------------------------*/
static void NwCanvasDirtyUnion(winPos_t row, winPos_t thisLeft, winPos_t thisCols)
{
  sNedWin_t  *pCanvas = NwGetCanvas();
  winPos_t    right;

  // NedLogPrintfEx(NEDLOG_WIN, "NwCanvasDirtyUnion(row=%d, thisLeft=%d, thisCols=%d)\n", row, thisLeft, thisCols);
  NedAssertDbg(row < pCanvas->rows);
  NedAssertDbg(thisLeft + thisCols <= pCanvas->cols);

  pCanvas->fDirty = TRUE;
  if(pCanvas->pDirty)
  {
    // row not dirty yet, 
    if(pCanvas->pDirty[row].cols == 0)        // not dirty
    {
      pCanvas->pDirty[row].left = thisLeft;
      pCanvas->pDirty[row].cols = thisCols;
    }

    // row already dirty, make union
    else
    {
      right = thisLeft + thisCols;
      if((pCanvas->pDirty[row].left + pCanvas->pDirty[row].cols) > right)
        right = pCanvas->pDirty[row].left + pCanvas->pDirty[row].cols;
      if(right > pCanvas->cols)
        right = pCanvas->cols;

      if(pCanvas->pDirty[row].left > thisLeft)
        pCanvas->pDirty[row].left = thisLeft;
      pCanvas->pDirty[row].cols = right - pCanvas->pDirty[row].left;

      if(pCanvas->pDirty[row].left + pCanvas->pDirty[row].cols > pCanvas->cols)
      {
        NedLogPrintfEx(NEDLOG_WIN, "  dirty left %u, dirty right %u, canvas cols %u\n",
          pCanvas->pDirty[row].left, pCanvas->pDirty[row].cols, pCanvas->cols);
        NedAssertDbg(pCanvas->pDirty[row].left + pCanvas->pDirty[row].cols <= pCanvas->cols);
      }
    }
  }
}

/*-------------------------------------------------------------------------------------------------
  Flush a window to the canvas, including frame, but only those portions that overlap dirty
  regions on the canvas.

  By end of this the window no longer is dirty.

  pDirtyWin   pointer to valid window

  returns     none
-------------------------------------------------------------------------------------------------*/
static void NwFlush2Canvas(sNedWin_t *pWin)
{
  sNedWin_t      *pCanvas = NwGetCanvas();
  nedWinChar_t   *pCanvasText = NULL;
  nedWinChar_t   *pText       = NULL;
  winPos_t        top;
  winPos_t        left;
  winPos_t        rows;
  winPos_t        cols;
  winPos_t        row;

  NedLogPrintfEx(NEDLOG_WIN, "NwFlush2Canvas()\n");
  // NedWinLogDumpWinHdr(pWin);

  // include framed area
  top  = pWin->top;
  left = pWin->left;
  rows = pWin->rows;
  cols = pWin->cols;
  if(pWin->fFramed)
  {
    --top;
    --left;
    rows += 2;
    cols += 2;
  }

  // nothing to do if window and frame off screen
  if(NwCanvasIntersect(&top, &left, &rows, &cols))
  {
    NedLogPrintfEx(NEDLOG_WIN, "  intersected canvas win %s top=%u left=%u rows=%u cols=%u\n", 
      NedWinLogName(pWin), top, left, rows, cols);

    // draw top of frame if in dirty region on canvas
    NwFlushFrameTopBot(pWin, TRUE);

    // copy only dirty portions of window to canvas (rows might be 1 larger for frame)
    for(row=0; (row < pWin->rows) && (pWin->top + row < pCanvas->rows); ++row)
    {
      // 
      left = pWin->left;
      cols = pWin->cols;
      if(NwCanvasIsDirtyRow(pWin->top + row, &left, &cols))
      {
        pText       = NwText(pWin, row, left - pWin->left);
        pCanvasText = NwText(pCanvas, pWin->top + row, left);
        memcpy(pCanvasText, pText, sizeof(nedWinChar_t) * cols);
      }

      // draw side frames
      NwFlushFrameEdges(pWin, row);
    }

    // draw top of frame if in dirty region on canvas
    NwFlushFrameTopBot(pWin, FALSE);
  }

  NwSetClean(pWin);
}

/*-------------------------------------------------------------------------------------------------
  Returns TRUE if this row is dirty, and adjusts left and cols to intersection of window and dirty
  region. All coordinates in canvas coordinates.

  Example (where v--v is dirty region on canvas, +-----+ is window:

    a)      v-----v                b)   v---1--2-----v 
       +----1-----2---------+               +--+

    c)      v--1---v               d)   v--------2---v 
               +---2----+             +-1--------+

-------------------------------------------------------------------------------------------------*/
static bool_t NwCanvasIsDirtyRow(winPos_t row, winPos_t *pLeft, winPos_t *pCols)
{
  sNedWin_t  *pCanvas = NwGetCanvas();
  winPos_t    left;
  winPos_t    cols;
  winPos_t    dirtyLeft;
  winPos_t    dirtyCols;

  // nothing to do if row off screen or not dirty
  if(row < 0 || row > pCanvas->rows || pCanvas->pDirty[row].cols == 0)
    return FALSE;

  // nothing to do if window portion doesn't overlap dirty region
  dirtyLeft = pCanvas->pDirty[row].left;
  dirtyCols = pCanvas->pDirty[row].cols;
  left = *pLeft;
  cols = *pCols;
  if(left + cols <= dirtyLeft || left >= dirtyLeft + dirtyCols)
    return FALSE;

  // adjust left and cols as needed
  if(left <= dirtyLeft)   // cases a, d above comments
    *pLeft = dirtyLeft;
  if(left + cols < dirtyLeft + dirtyCols) 
    *pCols = (left + cols) - *pLeft;            // cases b, d above comments
  else
    *pCols = (dirtyLeft + dirtyCols) - *pLeft;  // cases a, c above comments

  return TRUE;
}

/*-------------------------------------------------------------------------------------------------
  Flush window top or bottom of frame to canvas, if overlapping with a dirty region.

    +-------------+
-------------------------------------------------------------------------------------------------*/
static void NwFlushFrameTopBot(const sNedWin_t *pWin, bool_t fTop)
{
  nedWinChar_t   *pCanvasText;
  sNedWin_t      *pCanvas;
  char            cCorner;
  char            cLine;
  nedAttr_t       attr;
  winPos_t        row;
  winPos_t        left;
  winPos_t        cols;
  winPos_t        col;

  if(pWin->fFramed)
  {
    left    = pWin->left - 1;
    cols    = pWin->cols + 2;
    row     = fTop ? pWin->top - 1 : pWin->top + pWin->rows;
    if(NwCanvasIsDirtyRow(row, &left, &cols))
    {
      NedLogPrintfEx(NEDLOG_WIN, "NwFlushFrameTopBot(pWin=%s, fTop=%u, row=%d, left=%d, cols %d)\n",
          NedWinLogName(pWin), fTop, row, left, cols);
      pCanvas = NwGetCanvas();
      cCorner = NEDWIN_CORNER;
      cLine   = NEDWIN_LINE;
      attr    = pWin->frameAttr;

      // copy as much of +--------+ onto canvas as possible for window cols
      pCanvasText = NwText(pCanvas, row, left);
      for(col = 0; col < cols; ++col)
      {
        if(((col == 0) && (left == pWin->left - 1)) || ((col + 1) == cols && (pWin->left + pWin->cols + 1) == (left + cols)))
          *pCanvasText = NedWinCharMake(attr, cCorner);
        else
          *pCanvasText = NedWinCharMake(attr, cLine);
        ++pCanvasText;
      }
    }
  }
}

/*-------------------------------------------------------------------------------------------------
  Flush frame row to canvas (only does left/right). Doesn't know/care about dirty. Row must be on
  screen.

      |           |   or  [           ]
-------------------------------------------------------------------------------------------------*/
static void NwFlushFrameEdges(const sNedWin_t *pWin, winPos_t row)
{
  sNedWin_t      *pCanvas;
  nedWinChar_t   *pCanvasText;
  char            cSide;
  char            cPickl;
  char            cPickr;
  nedAttr_t       attr;
  winPos_t        left;
  winPos_t        cols;

  if(pWin->fFramed)
  {
    left      = pWin->left - 1;
    cols      = pWin->cols + 2;
    if(NwCanvasIsDirtyRow(pWin->top + row, &left, &cols))
    {
      pCanvas = NwGetCanvas();
      attr    = pWin->frameAttr;
      cSide   = NEDWIN_SIDE;
      cPickl  = NEDWIN_PICKLEFT;
      cPickr  = NEDWIN_PICKRIGHT;

      // if left edge of frame is in dirty region, copy it to canvas
      if(left == pWin->left - 1)
      {
        pCanvasText = NwText(pCanvas, pWin->top + row, left);
        if(pWin->frameLinePicked == row)
          *pCanvasText = NedWinCharMake(attr, cPickl);
        else
          *pCanvasText = NedWinCharMake(attr, cSide);
      }

      // if right edge of frame is in dirty region, copy it to canvas
      if(pWin->left + pWin->cols + 1 == left + cols)
      {
        pCanvasText = NwText(pCanvas, pWin->top + row, pWin->left + pWin->cols);
        if(pWin->frameLinePicked == row)
          *pCanvasText = NedWinCharMake(attr, cPickr);
        else
          *pCanvasText = NedWinCharMake(attr, cSide);
      }
    }
  }
}

/*-------------------------------------------------------------------------------------------------
  Internal function. Draws all dirty parts of the canvas to the screen using ANSI calls. Canvas
  is no longer dirty upon completion.

  Assumes window data and frames have already been flushed from windows to canvas via
  NwCanvasFlush().
-------------------------------------------------------------------------------------------------*/
static void NwCanvasDraw(void)
{
  sNedWin_t      *pCanvas = NwGetCanvas();
  nedWinChar_t   *pText;
  int             row;
  winPos_t        left;
  winPos_t        cols;
  nedAttr_t       attr;
  nedAttr_t       oldAttr;
  unsigned        screenRows;
  unsigned        screenCols;
  bool_t          fFirstTime = TRUE;
  bool_t          fNoScroll = FALSE;

  NedLogPrintfEx(NEDLOG_WIN, "NwCanvasDraw(rows %d, cols %d)\n", pCanvas->rows, pCanvas->cols);
  NedAssertDbg(NwIsInitialized() && NedWinIsWin(pCanvas));
  NedAssertDbg((pCanvas->rows >= 1) && (pCanvas->cols >= 1));
  NedAssertDbg(pCanvas->pDirty != NULL);

  AnsiGetRowsCols(&screenRows, &screenCols);
  if(pCanvas->rows == screenRows && pCanvas->cols == screenCols)
    fNoScroll = TRUE;

  // canvas and screen coordinates are the same
  for(row=0; row < pCanvas->rows; ++row)
  {
    left = 0;
    cols = pCanvas->cols;
    if(NwCanvasIsDirtyRow(row, &left, &cols))
    {
      NedLogPrintfEx(NEDLOG_WIN, "  drawing dirty row=%d, left=%d, cols=%d\n", row, left, cols);
      NedAssertDbg(left >= 0 && left + cols <= pCanvas->cols);
      pText = NwText(pCanvas, row, left);
      AnsiGoto(row, left);
      while(cols > 0)
      {
        // don't write to lower/right position or screen will scroll
        if(fNoScroll && row == (pCanvas->rows - 1) && left == (pCanvas->cols - 1))
          break;

        // only set attribute as needed
        attr = NedWinCharAttr(*pText);
        if(fFirstTime || (oldAttr != attr))
        {
          fFirstTime  = FALSE;
          oldAttr     = attr;
          AnsiSetAttr(attr);
        }
        printf("%c", NedWinChar(*pText));
        ++pText;
        ++left;
        --cols;
      }
    }
  }
  fflush(stdout);

  // no longer dirty
  NwSetClean(pCanvas);
}

/*-------------------------------------------------------------------------------------------------
  Add to ned of list. If no backdrop (head of list), then this is the backdrop.
-------------------------------------------------------------------------------------------------*/
static void NwAddToTail(sNedWin_t *pWin)
{
  sNedWin_t *pParent;

  // NedLogPrintfEx(NEDLOG_WIN, "NwAddToTail(pWin %p)\n", pWin);

  pParent = msNedWinScreen.pBackdrop;
  while(pParent && pParent->pNext)
    pParent = pParent->pNext;
  if(pParent)
  {
    pParent->pNext  = pWin;
    pWin->pNext     = NULL;
  }
  // NedWinLogDumpChain();
}

/*-------------------------------------------------------------------------------------------------
  Return pointer to text, or NULL if outside of window.

  Returns NULL if pWin not found or not a valid window.
-------------------------------------------------------------------------------------------------*/
static nedWinChar_t * NwText(const sNedWin_t *pWin, winPos_t row, winPos_t col)
{
  nedWinChar_t  *pText;
  // NedLogPrintfEx(NEDLOG_WIN, "NwText(pWin=%p, row=%u, col=%u, ", pWin, row, col);
  NedAssertDbg(NedWinIsWin(pWin));
  NedAssertDbg(pWin->pText);
  NedAssertDbg(row < pWin->rows);
  NedAssertDbg(col < pWin->cols);
  pText = &pWin->pText[(row * pWin->cols) + col];
  // NedLogPrintfEx(NEDLOG_WIN, "pText %p)\n", pText);
  return pText;
}

/*-------------------------------------------------------------------------------------------------
  Find the window and the previous window in the linked list. Useful for link management.

  The parent is Z-order parent (blow this window in Z-order). If the window to find is the
  backdrop, it is always found and its parent in NULL by definition.

  pWin        window to find
  ppParent    if not NULL, returned parent of pWin

  Returns NULL if pWin not found in list
-------------------------------------------------------------------------------------------------*/
static sNedWin_t * NwFindWin(const sNedWin_t *pWin, sNedWin_t **ppParent)
{
  sNedWin_t   *pFound   = NULL;
  sNedWin_t   *pParent  = NULL;

  if(NedWinIsWin(pWin))
  {
    pFound = NedWinGetBackdrop();
    while(pFound)
    {
      if(pFound == pWin)
      {
        if(ppParent)
          *ppParent = pParent;
        return (sNedWin_t *)pWin;
      }
      pParent = pFound;
      pFound  = pFound->pNext;
    }
  }

  return NULL;
}

/*-------------------------------------------------------------------------------------------------
  Set the whole window, including frame, as dirty.
  pWin      pointer valid window
  returns   none
-------------------------------------------------------------------------------------------------*/
static void NwDirtyAll(sNedWin_t *pWin)
{
  NwDirtyPart(pWin, 0, 0, pWin->rows, pWin->cols);
  NwDirtyFrame(pWin);
  if(NwGetBackdrop())
    NwGetBackdrop()->fDirty = TRUE;
}

/*-------------------------------------------------------------------------------------------------
  Marks the union dirty on the canvas. Coordinates top, left in window coordinates.

  pWin      valid window (background or user)
  top       top in window coordinates (0-n)
  left      left in window coordinates (0-n)
  rows      1-n
  cols      1-n
  returns   none
-------------------------------------------------------------------------------------------------*/
static void NwDirtyPart(sNedWin_t *pWin, winPos_t top, winPos_t left,
                                         winPos_t rows, winPos_t cols)
{
  winPos_t            row;
  winPos_t            right;
  sNedWinDirtyRow_t  *pDirty;
  sNedWin_t          *pCanvas = NwGetCanvas();

  // debugging
  NedAssertDbg(rows > 0);
  NedAssertDbg(cols > 0);
  if(top < 0 || (top + rows > pWin->rows))
    NedLogPrintf("NwDirtyPart(pWin=%s, top=%d, left=%d, rows=%d, cols=%d\n", NedWinLogName(pWin), top, left, rows, cols);
  NedAssertDbg((top  >= 0) && (top + rows <= pWin->rows));
  NedAssertDbg((left >= 0) && (left + cols <= pWin->cols));

  // convert region from window to canvas/screen coordinates
  top  += pWin->top;
  left += pWin->left;

  // nothing to do if window is off screen
  if(NwCanvasIntersect(&top, &left, &rows, &cols))
  {
    NedLogPrintfEx(NEDLOG_WIN, "  canvas region top %d, left %d, rows %d, cols %d\n", top, left, rows, cols);
    pWin->fDirty = TRUE;

    // union of each dirty line on canvas
    if(pCanvas->pDirty != NULL)
    {
      for(row=0; row < rows; ++row)
      {
        pDirty = &pCanvas->pDirty[top+row];

        // if row is dirty, make union
        if(pDirty->cols != 0)
        {
          right = left + cols;
          if((pDirty->left + pDirty->cols) > right)
            right = pDirty->left + pDirty->cols;
          if(pDirty->left < left)
            left = pDirty->left;
          cols = right - left;
        }
        pDirty->left = left;
        pDirty->cols = cols;
        // NedLogPrintfEx(NEDLOG_WIN, "  dirty row=%d, left=%d, cols=%d\n", top+row, left, cols);
      }
    }
  }
  NedLogPrintfEx(NEDLOG_WIN, "  done NwDirtyPart()\n");
}

/*-------------------------------------------------------------------------------------------------
  Mark part or all of the frame dirty on the canvas. Called when frame area changed color or
  appeared or disappeared.
-------------------------------------------------------------------------------------------------*/
static void NwDirtyFrame(sNedWin_t *pWin)
{
  int         top;
  int         left;
  winPos_t    rows;
  winPos_t    cols;
  winPos_t    row;

  pWin->fDirty = TRUE;

  // dirty top frame region
  top  = pWin->top  - 1;
  left = pWin->left - 1;
  rows = 1;
  cols = pWin->cols + 2;
  if(NwCanvasIntersect(&top, &left, &rows, &cols))
    NwCanvasDirtyUnion(top, left, cols);

  // mark rows of frame dirty on canvas if they intersect
  for(row = 0; row < pWin->rows; ++row)
    NwDirtyFrameRow(pWin, row);

  // dirty bottom frame region
  top  = pWin->top  + pWin->rows;
  left = pWin->left - 1;
  rows = 1;
  cols = pWin->cols + 2;
  if(NwCanvasIntersect(&top, &left, &rows, &cols))
    NwCanvasDirtyUnion(top, left, cols);
}

/*-------------------------------------------------------------------------------------------------
  Mark the frame row dirty. Row is in window coordates.
-------------------------------------------------------------------------------------------------*/
static void NwDirtyFrameRow(sNedWin_t *pWin, winPos_t row)
{
  winPos_t    top  = pWin->top  + row;
  winPos_t    left = pWin->left - 1;
  winPos_t    rows = 1;
  winPos_t    cols = pWin->cols + 2;
  if(NwCanvasIntersect(&top, &left, &rows, &cols))
  {
    pWin->fDirty = TRUE;
    NwCanvasDirtyUnion(top, left, cols);
  }
}

/*-------------------------------------------------------------------------------------------------
  Determine if this rectangle intersects the canvas. Reduces (if necessary) the rectangle. All
  coordinates in canvas coordinates.

  For example, if top = -3, left = -2, rows = 10, cols = 12
       returns -> top = 0,  left = 0,  rows = 7,  cols = 10

  Returns TRUE if does intersect, FALSE if not.
-------------------------------------------------------------------------------------------------*/
static bool_t NwCanvasIntersect(winPos_t *pTop, winPos_t *pLeft, winPos_t *pRows, winPos_t *pCols)
{
  sNedWin_t *pCanvas = NwGetCanvas();

  // doesn't intersect canvas at all, return FALSE
  if( (*pRows <= 0)          || (*pCols <= 0) ||
      (*pTop  + *pRows <= 0) || (*pTop  >= pCanvas->rows) ||
      (*pLeft + *pCols <= 0) || (*pLeft >= pCanvas->cols) )
  {
    return FALSE;
  }

  // adjust top, left, rows, cols as needed
  if(*pTop < 0)
  {
    *pRows += *pTop;
    *pTop   = 0;
  }
  if(*pLeft < 0)
  {
    *pCols += *pLeft;
    *pLeft = 0;
  }
  if(*pTop + *pRows > pCanvas->rows)
  {
    *pRows = pCanvas->rows - *pTop;
  }
  if(*pLeft + *pCols > pCanvas->cols)
  {
    *pCols = pCanvas->cols - *pLeft;
  }

  return TRUE;
}

/*-------------------------------------------------------------------------------------------------
  Window no longer dirty
-------------------------------------------------------------------------------------------------*/
static void NwSetClean(sNedWin_t *pWin)
{
  pWin->fDirty      = FALSE;
  if(pWin->pDirty)
  {
    memset(pWin->pDirty, 0, sizeof(sNedWinDirtyRow_t) * pWin->rows);
  }
}

/*-------------------------------------------------------------------------------------------------
  Get intersection of the rectangle within this window.

  pWin    must be valid window
  top     top in window coordinates (0-n)
  left    left in window coordinates (0-n)
  *pRows  must be initialized (1-n). Might be curtailed.
  *pCols  must be initialized (1-n). Might be curtailed.

  Returns TRUE if there is an intersection, FALSE if not.
-------------------------------------------------------------------------------------------------*/
static bool_t NwIntersect(sNedWin_t *pWin, winPos_t top, winPos_t left,
                                           winPos_t *pRows, winPos_t *pCols)
{
  bool_t    fIntersects = FALSE;

  NedAssertDbg(top >= 0 && left >=0);
  NedAssertDbg(pRows && (*pRows >= 1));
  NedAssertDbg(pCols && (*pCols >= 1));

  // is any portion of window on screen? if not, no intersection
  if((top < pWin->rows) && (left < pWin->cols))
  {
    fIntersects = TRUE;
    if(top + *pRows > pWin->rows)
      *pRows = pWin->rows - top;
    if(left + *pCols > pWin->cols)
      *pCols = pWin->cols - left;
  }

  return fIntersects;
}

/*!------------------------------------------------------------------------------------------------
  return name of window "Bk" for background, Win1 for 1st user window.

  @ingroup  ned_win
  @param    hWin      pointer to valid window
  @return   none
*///-----------------------------------------------------------------------------------------------
const char * NedWinLogName(const void *pWin)
{
  unsigned      i;
  sNedWin_t    *pFindWin;
  static char   sz[8];

  if(pWin == msNedWinScreen.pCanvas)
    return "pCanvas";
  else if(pWin == NwGetBackdrop())
    return "pBackground";
  else if(NwGetBackdrop() != NULL)
  {
    pFindWin = NwGetBackdrop()->pNext;
    for(i=1; pFindWin && (i < NEDWIN_MAX_WINDOWS); ++i)
    {
      if(pFindWin == pWin)
      {
        snprintf(sz, sizeof(sz), "Win%u", i);
        return (const char *)sz;
      }
      pFindWin = pFindWin->pNext;
    }
  }
  return "(?)";
}

/*!------------------------------------------------------------------------------------------------
  Dump a window title and basic coordinates to the log file, without contents

  @ingroup  ned_win
  @param    pWin      pointer to valid window
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinLogDumpWinHdr(const void *pWin)
{
  const sNedWin_t *pThisWin = pWin;
  NedLogPrintf("%s %p", NedWinLogName(pWin), pWin);
  if(pThisWin)
  {
    NedLogPrintf(", top=%u, left=%u, rows=%u, cols=%u\n",
      pThisWin->top, pThisWin->left, pThisWin->rows, pThisWin->cols);
  }
  else
  {
    NedLogPrintf("\n");
  }
}

/*!------------------------------------------------------------------------------------------------
  Dump the text of the a window to the log file.

  @ingroup  ned_win
  @param    pWin      pointer to valid window
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinLogDumpText(const sNedWin_t *pWin)
{
  nedWinChar_t   *pText;
  winPos_t        row;
  winPos_t        col;

  for(row = 0; row < pWin->rows; ++row)
  {
    pText = NwText(pWin, row, 0);
    NedLogPrintf("[r%-2u", row);
    if(pWin->pDirty)
    {
      if(pWin->pDirty[row].cols == 0)
        NedLogPrintf(" clean  ] ");
      else
        NedLogPrintf(" l%-2u,c%-2u] ", pWin->pDirty[row].left, pWin->pDirty[row].cols);
    }
    else
    {
      NedLogPrintf("] ");
    }
    for(col = 0; col < pWin->cols; ++col)
    {
      NedLogPrintf("%04x ",*pText);
      ++pText;
    }
    NedLogPrintf("\n");
  }
}

/*!------------------------------------------------------------------------------------------------
  Dump a single window structure (background, canvas, user windows) to log file.

  @ingroup  ned_win
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinLogDumpWin(const void *hWin)
{
  const sNedWin_t *pWin = hWin;

  NedWinLogDumpWinHdr(pWin);
  if(pWin)
  {
    // top, left, rows, cols already dumped with NedWinLogDumpWinShort()
    NedLogPrintf("  fDirty     = %u\n",   pWin->fDirty);
    NedLogPrintf("  fDirtyFrame= %u\n",   pWin->fDirtyFrame);
    NedLogPrintf("  picked     = %p\n",   pWin->frameLinePicked);
    NedLogPrintf("  top        = %d\n",   pWin->top);
    NedLogPrintf("  left       = %d\n",   pWin->left);
    NedLogPrintf("  rows       = %d\n",   pWin->rows);
    NedLogPrintf("  cols       = %d\n",   pWin->cols);
    NedLogPrintf("  crow       = %d\n",   pWin->crow);
    NedLogPrintf("  ccol       = %d\n",   pWin->ccol);
    NedLogPrintf("  fFramed    = %u\n",   pWin->fFramed);
    NedLogPrintf("  attr       = x%02x\n",pWin->attr);
    NedLogPrintf("  frameAttr  = x%02x\n",pWin->frameAttr);
    NedLogPrintf("  pDirty     = %p\n",   pWin->pDirty);
    NedLogPrintf("  pText      = %p\n",   pWin->pText);
    NedLogPrintf("  pNext      = %p\n",   pWin->pNext);
    NedWinLogDumpText(pWin);
    NedLogPrintf("\n");
  }
}

/*!------------------------------------------------------------------------------------------------
  Show the chain of windows (window Z order) from bottom to top to long file.

      Backdrop->UserWin1->UserWin2->NULL

  @ingroup  ned_win
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinLogShowChain(void)
{
  unsigned    i     = 0;
  sNedWin_t  *pWin  = NedWinGetBackdrop();

  NedLogPrintf("WinChain: ");
  while(pWin && i < NEDWIN_MAX_WINDOWS)
  {
    NedLogPrintf("(%s %p)->", NedWinLogName(pWin), pWin);
    pWin = pWin->pNext;
  }
  NedLogPrintf("NULL\n");
  NedAssertDbg(i < NEDWIN_MAX_WINDOWS);
}

/*!------------------------------------------------------------------------------------------------
  Dump entire window system to log, includes screen object and every window object (background,
  canvas, user windows) to log file.

  @ingroup  ned_win
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinLogDumpCanvas (void)
{
  NedWinLogDumpWin(NwGetCanvas());
  NedLogPrintf("\n\n");
}

/*!------------------------------------------------------------------------------------------------
  Dump entire window system to log, includes screen object and every window object (background,
  canvas, user windows) to log file.

  @ingroup  ned_win
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedWinLogDumpAll(void)
{
  sNedWin_t      *pWin;

  NedLogPrintf("\nNedWinLogDumpAll(screen rows=%u cols=%u, pCanvas %p, pBackdrop %p, pCurWin %p)\n\n", 
    NwGetCanvas()->rows, NwGetCanvas()->cols, NwGetCanvas(), NwGetBackdrop(), NwGetPicked());
  
  pWin = NedWinGetBackdrop();
  while(pWin)
  {
    NedWinLogDumpWin(pWin);
    pWin = pWin->pNext;
  }
  NedLogPrintf("\n\n");
}

/*!------------------------------------------------------------------------------------------------
  Get characters from the current canvas (screen). Only reflects Flushed changes. Does not reflect
  colors. ASCII Text only.

  @ingroup  ned_win
  @return   '\0' terminated string of length cols (or less) in pszDst.
*///-----------------------------------------------------------------------------------------------
winPos_t NedWinScreenGets(char *pszDst, winPos_t row, winPos_t left, winPos_t cols)
{
  sNedWin_t    *pCanvas = NwGetCanvas();
  nedWinChar_t *pText; 
  winPos_t      i = 0;

  NedAssert(NedWinIsWin(pCanvas));

  // off screen, return empty string
  if((row >= pCanvas->rows) || (left >= pCanvas->cols))
    *pszDst = '\0';

  // copy string, then null terminate
  else
  {
    if((left + cols) > pCanvas->cols)
      cols = pCanvas->cols - left;

    pText = NwText(pCanvas, row, left);
    for(i=0; i < cols; ++i)
    {
      pszDst[i] = NedWinChar(pText[i]);
    }
    pszDst[cols] = '\0';
  }
  return i;
}
