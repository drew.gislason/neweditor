/*!************************************************************************************************
  @file NedPointEdit.c

  @brief  Ned Point Edit Functions Implementation

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include <ctype.h>
#include "NedLog.h"
#include "NedPoint.h"

/*!------------------------------------------------------------------------------------------------
  Insert a character at the point. Aadvances point.

  @ingroup  ned_point
  @param    pPoint       pointer to valid point
  @param    c            character to insert

  @return   TRUE if worked, FALSE if can't allocate another buffer.
*///-----------------------------------------------------------------------------------------------
bool_t NedPointInsChar(sNedPoint_t *pPoint, nedChar_t c)
{
  uint8_t   byte = (uint8_t)c;
  bool_t    fWorked;

  NedAssertDbg(NedPointIsPoint(pPoint));

  // insert character into the buffer at the point
  fWorked = (NedBufInsert(pPoint->pBuf, pPoint->offset, &byte, 1) == 1) ? TRUE : FALSE;
  if(fWorked)
    NedPointCharAdvance(pPoint, 1);
  return fWorked;
}

/*!------------------------------------------------------------------------------------------------
  Insert a block of data at the point. Advances point.

  If at end of file and filled the last buffer will create new empty buffer of 0 length to place
  the advanced point.

  @ingroup  ned_point
  @param    pPoint      pointer to valid point
  @param    pData       pointer to data to insert
  @param    insertLen   length of data (up to LONG_MAX)

  @return   TRUE if worked, FALSE if problem allocating buffers
*///-----------------------------------------------------------------------------------------------
bool_t NedPointInsBlock(sNedPoint_t *pPoint, const uint8_t *pData, long insertLen)
{
  bool_t          fWorked = TRUE;
  long            len;
  sNedBuffer_t   *pBuf;

  NedAssertDbg(NedPointIsPoint(pPoint));
  NedAssertDbg(pData);
  NedAssertDbg(insertLen > 0);

  len = NedBufInsert(pPoint->pBuf, pPoint->offset, pData, insertLen);
  if(len != insertLen)
    fWorked = FALSE;

  else
  {
    // move to just after the inserted data
    NedPointCharAdvance(pPoint, len);

    // special case: if at bottom of file and last buffer is full, make a 0
    // length buffer and put point there
    if(NedBufLen(pPoint->pBuf) == NedBufMaxLen() && NedPointIsAtBottom(pPoint))
    {
      pBuf = NedBufAppend(pPoint->pBuf);
      if(pBuf)
      {
        pPoint->pBuf    = pBuf;
        pPoint->offset  = 0;
      }
      else
        fWorked = FALSE;
    }
  }

  // insert the data into the buffer at the point
  return fWorked;
}

/*!------------------------------------------------------------------------------------------------
  Insert a character at the point. Advances point.

  @ingroup  ned_point
  @param    pPoint       pointer to valid point
  @param    sz           point to NULL terminated string

  @return   TRUE if worked, FALSE if can't allocate another buffer.
*///-----------------------------------------------------------------------------------------------
bool_t NedPointInsStr(sNedPoint_t *pPoint, const char *sz)
{
  long    insertLen;
  bool_t  fWorked = TRUE;

  NedAssertDbg(NedPointIsPoint(pPoint));
  NedAssertDbg(sz);

  insertLen = (long)strlen(sz);
  if(insertLen)
    fWorked = NedPointInsBlock(pPoint, (const uint8_t *)sz, insertLen);

  return fWorked;
}

/*!------------------------------------------------------------------------------------------------
  Delete character at point. The pointer ends up on the next character. Can handle both \n type
  line endings and \r\n line endings. Examples:

      This     Becomes    That
      abcde               abde
        ^                   ^
      pPoint             pPoint

      xy\r\nz              xyz
           ^                 ^
        pPoint            pPoint

  @ingroup  ned_point
  @param    pPoint       pointer to valid point

  @return   TRUE if worked, FALSE if at EOF
*///-----------------------------------------------------------------------------------------------
bool_t NedPointDelChar(sNedPoint_t *pPoint)
{
  sNedBuffer_t   *pBuf;
  unsigned        offset ;
  unsigned        len = 1;
  sNedBuffer_t   *pBufPrev;

  NedAssertDbg(NedPointIsPoint(pPoint));

  if(NedPointCharGet(pPoint) == NEDCHAR_EOF)
    return FALSE;

  // handle case of \r\n pair
  len     = 1;
  offset  = pPoint->offset;
  pBuf    = pPoint->pBuf;
  if(NedPointCharGet(pPoint) == '\n')
  {
    if(offset == 0)
    {
      // is \r\n split across buffers?
      pBufPrev = pBuf->pPrev;
      if(pBufPrev && pBufPrev->len && (pBufPrev->aData[pBufPrev->len - 1] == '\r'))
      {
        pBuf   = pBufPrev;
        offset = pBufPrev->len - 1;
        ++len;
      }
    }

    // \r\n in this buffer?
    else if(pBuf->aData[offset -1] == '\r')
    {
      --offset;
      ++len;
    }
  }

  // handle if deleting last character in buf (moves point to beginning of next buf)
  if(pBuf->pNext && ((offset + 1) >= pBuf->len))
  {
    pPoint->pBuf   = pBuf->pNext;
    pPoint->offset = 0;
  }

  return NedBufShrink(pBuf, offset, len);
}

/*!------------------------------------------------------------------------------------------------
  Delete character before the point. Point will end up on same character, one position less.

  @ingroup  ned_point
  @param    pPoint       pointer to valid point

  @return   TRUE if worked, FALSE if already at top of file
*///-----------------------------------------------------------------------------------------------
bool_t NedPointDelBackspace(sNedPoint_t *pPoint)
{
  // if already at top of file, return false
  if(!NedPointCharPrev(pPoint))
    return FALSE;
  return NedPointDelChar(pPoint);
}

/*!------------------------------------------------------------------------------------------------
  Delete block of bytes from pThis to pThat. Both pThis and pThat will end up on the same point
  after deltee. For example the following:

      [abcdefghijklmnopqrstuvwxyz]
          ^-----------^
        pThis       pThat

  Becomes:

      [abcpqrstuvwxyz]
          ^
        pThis
        pThat

  @ingroup  ned_point
  @param    pThis       pointer to valid point
  @param    removeLen   length (# of bytes) to remove

  @return   TRUE if worked, FALSE if not.
*///-----------------------------------------------------------------------------------------------
bool_t NedPointDelBlock(sNedPoint_t *pThis, sNedPoint_t *pThat)
{
  bool_t      fWorked = FALSE;
  long        removeLen;

  NedAssertDbg(NedPointIsSameChain(pThis, pThat));

  // in case pThat < pThis
  NedPointSort2Points(pThis, pThat);

  // remove the block
  removeLen = NedPointDifference(pThis, pThat);
  if(removeLen)
    fWorked = NedBufShrink(pThis->pBuf, pThis->offset, removeLen);

  // if not at end of file, may have deleted all the data at end of this buffer
  // move pThis to 1st byte of next buffer.
  if((pThis->offset >= pThis->pBuf->len) && (pThis->pBuf->pNext != NULL))
  {
    pThis->offset = 0;
    pThis->pBuf = pThis->pBuf->pNext;
  }

  // block is deleted, pThis and pThat point to the same place.
  *pThat = *pThis;

  return fWorked;
}

