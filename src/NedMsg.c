/*!************************************************************************************************
  @file NedMsg.c

  @brief  Ned Message implementation - Includes functions for status bar and message bar

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  Assume window system has been initialized. The status bar and message line are painted on the
  backdrop. The status bar is 2nd to bottom. The message line is the bottom line. Example:

*///***********************************************************************************************
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "NedKeyMap.h"
#include "NedWin.h"
#include "NedMsg.h"

typedef struct
{
  unsigned          opts;
  const char       *szFilePath;
  long              row;
  long              col;
  bool_t            fHardTabs;
  unsigned          tabSize;
  bool_t            fCrLf;
  const nedTheme_t *pTheme;
} sNedStatusBar_t;

#define NMSG_MSGLINE      FALSE
#define NMSG_STATUSBAR    TRUE
#define ROW_MENU    0
#define ROW_EDIT    1
#define ROW_STATUS  -2
#define ROW_MSG     -1

static sNedStatusBar_t  m_sStatusBar = { 0, "", 1, 1 };
static char             m_szMsgLine[NEDWIN_MAX_COLS+1];

/*-------------------------------------------------------------------------------------------------
  Helper function calculates digits
-------------------------------------------------------------------------------------------------*/
unsigned NMsgDigits(long value)
{
  unsigned  digits = 1;

  while(1)
  {
    value = value / 10;
    if(value == 0)
      break;
    ++digits;
  }
  return digits + 1;
}

/*-------------------------------------------------------------------------------------------------
  Helper function, picks backdrop and proper row, returns # of cols
-------------------------------------------------------------------------------------------------*/
winPos_t NMsgSetup(bool_t fStatusBar, unsigned col)
{
  winPos_t          rows;
  winPos_t          cols;

  // make sure there is a theme (user should called NedStatusBarSetTheme())
  NedAssert(m_sStatusBar.pTheme);

  // pick the backdrop and proper row/col
  NedWinPickBackdrop();
  NedWinGetSize(&rows, &cols);
  NedWinSetAttr(fStatusBar ? m_sStatusBar.pTheme->menuText : m_sStatusBar.pTheme->message);
  NedWinGoto(fStatusBar ? rows + ROW_STATUS : rows + ROW_MSG, col);

  return cols;
}

/*!------------------------------------------------------------------------------------------------
  The status information on the right takes precedence over the filename on the left. The filename
  gets the leftovers, and may be shortened. Unless there are aren't enough columns for the
  information on the right, then the filename only is displayed.

                                                                                           [MACRO]
  /users/drewg/Documents/Work/git/ned/src/hello.c                  [CAW][*]  12345:1234  Tabs 2 Lf

  @ingroup  ned_msg
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedStatusBarShow(void)
{
  #define MIN_FILENAME_SIZE         8                   // file.ext
  static const char szFileOpts[]    = "[%c] ";          // 3 chars (1 option)
  static const char szFileOpts2[]   = "    ";           // 3 chars
  static const char szSearchOpts[]  = " [%c%c%c]";      // 6 chars (3 options)
  static const char szSearchOpts2[] = "      ";         // 6 chars
  static const char szCrLf[]        = "CrLf";
  static const char szLf[]          = "Lf";
  static const char szMacro[]       = "[MACRO]";        // record macro
  static const char szTabOpts[]     = "Tabs %u %s";    // Tabs: 8 CrLf
  static const char szHTabOpts[]    = "HTab %u %s";    // HTab: 8 CrLf
  static const char szRightFields[] = "%*ld:%-*ld%s";
  char              aSearchOpts[4];
  char              szTabInfo[13];                      // Tab: 16 CrLf
  char              cFileOpt;
  unsigned          rightCols;      // for NED string
  unsigned          rowCols;        // # chars in cursor line, e.g. "1234" is 4
  unsigned          colCols;        // # chars in cursor col, e.g. "567" is 3
  unsigned          tabCols;        // "Tabs 2 Lf" is 9
  unsigned          width;
  unsigned          cols;

  // NedLogPrintfEx(NEDLOG_MSG, "NedStatusBarShow(opts=%u, szFilePath=%p, row=%u, col=%u, pTheme=%p\n",
  //     m_sStatusBar.opts, m_sStatusBar.szFilePath, m_sStatusBar.row, m_sStatusBar.col, m_sStatusBar.pTheme);

  NedAssertDbg(m_sStatusBar.szFilePath);
  NedAssertDbg(m_sStatusBar.row);
  NedAssertDbg(m_sStatusBar.col);

  // prep for displaying message (
  cols = NMsgSetup(NMSG_STATUSBAR, 0);

  // set file options
  if(m_sStatusBar.opts & NEDSTAT_OPT_UNSAVED)
    cFileOpt = '*';
  else if(m_sStatusBar.opts & NEDSTAT_OPT_READONLY)
    cFileOpt = 'R';
  else
    cFileOpt = ' ';

  // set search options
  if(m_sStatusBar.opts & NEDSTAT_OPT_IN_SEARCH)
  {
    memset(aSearchOpts, ' ', sizeof(aSearchOpts));
    if(m_sStatusBar.opts & NEDSTAT_OPT_CASE_SENSITIVE)
      aSearchOpts[0] = 'C';
    if(m_sStatusBar.opts & NEDSTAT_OPT_WRAP)
      aSearchOpts[1] = 'A';
    if(m_sStatusBar.opts & NEDSTAT_OPT_WHOLE_WORD)
      aSearchOpts[2] = 'W';
  }

  // determine # of columns for line/col fields
  rowCols = NMsgDigits(m_sStatusBar.row);
  if(rowCols < 4)
    rowCols = 4;
  colCols = NMsgDigits(m_sStatusBar.col);
  if(colCols < 4)
    colCols = 4;

  // determine # of columns for tab fields
  sprintf(szTabInfo, m_sStatusBar.fHardTabs ? szHTabOpts : szTabOpts, m_sStatusBar.tabSize, m_sStatusBar.fCrLf ? szCrLf : szLf);
  tabCols = strlen(szTabInfo);
  if(m_sStatusBar.opts & NEDSTAT_OPT_MACRO)
    sprintf(szTabInfo, "%*s", tabCols, szMacro);

  // [CAW][*]  12345:1234  Tabs 2 Lf
  rightCols = (sizeof(szSearchOpts2)-1) + (sizeof(szFileOpts2)-1) + (rowCols + 1 + colCols) + tabCols;

  // if columns too few, just display filename
  if(MIN_FILENAME_SIZE + rightCols > cols)
  {
    NedWinPrintf("%s", NedStrFitName(m_sStatusBar.szFilePath, cols));
  }

  // display all info fields
  else
  {
    width = cols - rightCols;
    NedWinPrintf("%-*s", width, NedStrFitName(m_sStatusBar.szFilePath, width));
    if(m_sStatusBar.opts & NEDSTAT_OPT_IN_SEARCH)
      NedWinPrintf(szSearchOpts, aSearchOpts[0], aSearchOpts[1], aSearchOpts[2]);
    else
      NedWinPrintf(szSearchOpts2);
    if(cFileOpt == ' ')
      NedWinPrintf(szFileOpts2);
    else
      NedWinPrintf(szFileOpts, cFileOpt);
    NedWinPrintf(szRightFields, rowCols, m_sStatusBar.row, colCols, m_sStatusBar.col, szTabInfo);
  }
  NedWinFlush();
}

/*!------------------------------------------------------------------------------------------------
  Set message bar option bitmask

  @ingroup  ned_msg
  @param    opts
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedStatusBarSetOpts(nedStatOpts_t opts)
{
  m_sStatusBar.opts = opts;
}

/*!------------------------------------------------------------------------------------------------
  Set message bar option bitmask

  @ingroup  ned_msg
  @param    opts
  @returns  none
*///-----------------------------------------------------------------------------------------------
nedStatOpts_t NedStatusBarGetOpts(void)
{
  return m_sStatusBar.opts;
}

/*!------------------------------------------------------------------------------------------------
  Set message bar filepath name

  @ingroup  ned_msg
  @param    szFilePath
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedStatusBarSetFile(const char *szFilePath)
{
  m_sStatusBar.szFilePath = szFilePath;
}

/*!------------------------------------------------------------------------------------------------
  Set message bar row and column

  @ingroup  ned_msg
  @param    row
  @param    col
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedStatusBarSetRowCol(long row, long col)
{
  NedAssertDbg(row >= 1);
  NedAssertDbg(col >= 1);
  m_sStatusBar.row = row;
  m_sStatusBar.col = col;
}

/*!------------------------------------------------------------------------------------------------
  Set tab and line endings field

  @ingroup  ned_msg
  @param    fHardTabs   TRUE if hard tabs, otherwise spaces for tabs
  @param    tabSize     tabSize, max 8 (see NEDCFG_MAX_INDENT)
  @param    fCrLf       TRUE if CrLf, otherwise Lf
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedStatusBarSetTab(bool_t fHardTabs, unsigned tabSize, bool_t fCrLf)
{
  m_sStatusBar.fHardTabs  = fHardTabs;
  m_sStatusBar.tabSize    = tabSize;
  m_sStatusBar.fCrLf      = fCrLf;
} 

/*!------------------------------------------------------------------------------------------------
  Set message bar color theme

  @ingroup  ned_msg
  @param    pTheme
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedStatusBarSetTheme(const nedTheme_t *pTheme)
{
  m_sStatusBar.pTheme = pTheme;
}

/*!------------------------------------------------------------------------------------------------
  Printf to MessageLine

  @ingroup  ned_msg
  @param    szFormat    constant format string
  @param    ...         optional variable number of parameters

  @returns  length of printed string
*///-----------------------------------------------------------------------------------------------
void NedMsgClear(void)
{
  NMsgSetup(NMSG_MSGLINE, 0);
  NedWinClearEol();
  NedWinFlush();
  m_szMsgLine[0] = '\0';
}

/*!------------------------------------------------------------------------------------------------
  reprint message line

  @ingroup  ned_msg
  @param    szFormat    constant format string
  @param    ...         optional variable number of parameters

  @returns  length of printed string
*///-----------------------------------------------------------------------------------------------
void NedMsgRedraw(void)
{
  winPos_t  cols;
  unsigned  len;

  // make sure it fits on line
  cols = NMsgSetup(NMSG_MSGLINE, 0);
  len = strlen(m_szMsgLine);
  if(len > cols)
    m_szMsgLine[cols] = '\0';

  NedWinClearEol();
  NedWinPrintf("%s", m_szMsgLine);
  NedWinFlush();
}


/*!------------------------------------------------------------------------------------------------
  Printf to MessageLine

  @ingroup  ned_msg
  @param    szFormat    constant format string
  @param    ...         optional variable number of parameters

  @returns  length of printed string
*///-----------------------------------------------------------------------------------------------
unsigned NedMsgPrintf(const char *szFormat, ...)
{
  va_list     arglist;
  unsigned    len;

  // don't allow \n as it will scroll the screen (mess it up)
  if(strchr(szFormat, '\n') != NULL)
    NedLogPrintfEx(NEDLOG_MSG, "NedMsgPrintf: bad szFormat %s\n", szFormat);
  NedAssert(strchr(szFormat, '\n') == NULL);

  // print, and clear to end of line
  va_start(arglist, szFormat);
  len = vsnprintf(m_szMsgLine, sizeof(m_szMsgLine) - 1, szFormat, arglist);
  va_end(arglist);

  // move to the proper place to print
  NedMsgRedraw();

  return len;
}


/*-------------------------------------------------------------------------------------------------
  Helper function, previous word in Message Line editor
-------------------------------------------------------------------------------------------------*/
static unsigned NMsgWordPrev(char *szLine, unsigned cursorCol)
{
  nedChar_t       c = 0;
  nedCharType_t   type;

  // NedLogPrintfEx(NEDLOG_MSG, "NMsgWordPrev(szLine %p, cursorCol %u)\n", szLine, cursorCol);

  // skip preceding whitespace
  if(cursorCol > 0)
  {
    --cursorCol;
    c = szLine[cursorCol];
    // NedLogPrintfEx(NEDLOG_MSG, "  white: c %c, cursorCol %u, type %u\n", c ? c : '~', cursorCol, NedCharType(c));
    while((NedCharType(c) == IS_SPACE) && cursorCol)
    {
      --cursorCol;
      c = szLine[cursorCol];
    }
  }

  // skip all chars of same type
  type = NedCharType(c);
  NedLogPrintfEx(NEDLOG_MSG, "  type %u\n", type);
  while(cursorCol > 0)
  {
    c = szLine[cursorCol-1];
    // NedLogPrintfEx(NEDLOG_MSG, "  c %c, cursorCol %u, type %u\n", c ? c : '~', cursorCol, NedCharType(c));
    if(NedCharType(c) != type)
      break;
    --cursorCol;
  }

  return cursorCol;
}

/*-------------------------------------------------------------------------------------------------
  Helper function, next word in Message Line editor
-------------------------------------------------------------------------------------------------*/
static unsigned NMsgWordNext(char *szLine, unsigned cursorCol)
{
  nedChar_t       c;
  nedCharType_t   type;

  // NedLogPrintfEx(NEDLOG_MSG, "NMsgWordNext(szLine %p, cursorCol %u)\n", szLine, cursorCol);

  // skip all chars of same type (up to end of line)
  c = szLine[cursorCol];
  type = NedCharType(c);
  while(c)
  {
    ++cursorCol;
    c = szLine[cursorCol];
    // NedLogPrintfEx(NEDLOG_MSG, "  c %c, cursorCol %u, type %u\n", c ? c : '~', cursorCol, type);

    if(!c || (NedCharType(c) != type))
      break;
  }

  // skip all following whitespace, and stop on 1st non-whitespace
  while(c && (NedCharType(c) == IS_SPACE))
  {
    ++cursorCol;
    c = szLine[cursorCol];
    // NedLogPrintfEx(NEDLOG_MSG, "  c %c, cursorCol %u, type %u\n", c ? c : '~', cursorCol, NedCharType(c));
  }

  return cursorCol;
}

/*!------------------------------------------------------------------------------------------------
  Converts from special key NedMsgLineEdit() ret code from -2 through -n, to 0-n

  @ingroup  ned_msg
  @param    ret        return code from NedMsgLineEdit() for AltExitKeys

  @returns  0-n ret code in index form
*///-----------------------------------------------------------------------------------------------
unsigned NedMsgLineEditIndex(long ret)
{
  return (((ret) * -1) - 2);
}

/*!------------------------------------------------------------------------------------------------
  Edit a line. Return the NULL teriminated line (and length).

  The same keys that can  be used for line editing in the editor (such as arrows, home, end,
  delete, backspace, ctrl-K to kill to end of line, etc...) can be used in this line editor. Even
  if new keys are mapped to those functions.

  The alternate exit keys allow for things like tab completion, or up arrow on search to bring
  up recent searches, etc...

  Edits in-place in szLine (that is, it's destructive, even if Esc is used).

  Exits upon Esc, Enter or one of the pAltExitKeys (if any).

  @ingroup  ned_msg
  @param    szLine          initialized string. Also returned string.
  @param    maxLen          maximum length of string (may be limited by line editor)
  @param    szPrompt        prompt to display at start of Message Line
  @param    pAltExitKeys    '\0' terminated list of alternate exit keys or NULL if none
  @param    szPasteStr      paste string, NULL if none

  @returns  0-n=Enter (length of string), or -1 if Esc, -2-n = pAltExitKeys index
*///-----------------------------------------------------------------------------------------------
long NedMsgLineEdit(char *szLine, long maxLen, const char *szPrompt, const nedKey_t *pAltExitKeys, const char *szPasteStr)
{
  nedKey_t    c;
  long        len;            // current length of string
  unsigned    lenPaste = 0;
  winPos_t    row;
  winPos_t    startCol;
  unsigned    cursorCol;
  unsigned    maxCols;        // column on screen (max is cols - (startCol + 1))
  bool_t      fEdited;
  pfnNedCmd_t pfnCmd;
  nedKey_t   *pAltExit;
  bool_t      fFirstKey = TRUE;

  NedAssertDbg(szLine);
  NedAssertDbg(maxLen >= 1);
  NedAssertDbg(szPrompt);

  // get length of paste string (won't change)
  if(szPasteStr)
    lenPaste = strlen(szPasteStr);

  // start with the prompt. This will give us a row/col to start on
  maxCols = NMsgSetup(NMSG_MSGLINE, 0);
  NedWinPrintf("%s", szPrompt);
  NedWinGetCursor(&row, &startCol);

  // window too small to do string
  if(maxCols < 2 + startCol)
    return -1L;
  else
    maxCols -= (2 + startCol);
  if(maxCols < maxLen)
    maxLen = maxCols;

  // cursor starts out at end of input string
  len = cursorCol = strlen(szLine);
  fEdited = TRUE;

  NedLogPrintfEx(NEDLOG_MSG, "row %u, startCol %u, lenPaste %u, lenLine %u, maxCols %u, maxLen %u\n",
    row, startCol, lenPaste, len, maxCols, maxLen);

  while(1)
  {
    // display the edited text
    if(fEdited)
    {
      NedWinGoto(row, startCol);
      if(fFirstKey)
        NedWinSetAttr(m_sStatusBar.pTheme->textHighlight);
      else
        NedWinSetAttr(m_sStatusBar.pTheme->message);
      NedWinPrintf("%s", szLine);
      NedWinSetAttr(m_sStatusBar.pTheme->message);
      NedWinClearEol();
      NedWinFlush();
      fEdited = FALSE;
    }
    NedWinGoto(row, startCol + cursorCol);
    NedWinFlushCursor();

    c = NedKeyGetKey();
    pfnCmd = NedKeyMapGetCommand(c);
    if(pAltExitKeys)
    {
      pAltExit = memchr(pAltExitKeys, c, strlen((void *)pAltExitKeys));
      if(pAltExit)
      {
        len = -1L * ((unsigned)(pAltExit - pAltExitKeys) + 2);
        break;
      }
    }

    // can't remap ESC and ENTER
    if(c == NEDKEY_ESC)
    {
      len = -1L;
      break;
    }
    else if(c == NEDKEY_ENTER)
    {
      break;
    }
    else if(pfnCmd == NedCmdCursorLeft)
    {
      if(cursorCol > 0)
        --cursorCol;
    }
    else if(pfnCmd == NedCmdCursorRight)
    {
      if(cursorCol < len)
        ++cursorCol;
    }
    else if(pfnCmd == NedCmdCursorHome)
    {
      cursorCol = 0;
    }
    else if(pfnCmd == NedCmdCursorEnd)
    {
      cursorCol = len;
    }
    else if(pfnCmd == NedCmdCursorWordLeft)
    {
      cursorCol = NMsgWordPrev(szLine, cursorCol);
    }
    else if(pfnCmd == NedCmdCursorWordRight)
    {
      cursorCol = NMsgWordNext(szLine, cursorCol);
    }
    else if(pfnCmd == NedCmdEditBackspace)
    {
      if(fFirstKey)
      {
        cursorCol = len = 0;
        szLine[0] = '\0';
      }
      else if(cursorCol)
      {
        --cursorCol;
        memmove(&szLine[cursorCol], &szLine[cursorCol+1], (len-cursorCol)+1);
        fEdited = TRUE;
        --len;
      }
    }
    else if(pfnCmd == NedCmdEditCutEol)
    {
      szLine[cursorCol] = '\0';
      len = cursorCol;
      fEdited = TRUE;
    }
    else if(pfnCmd == NedCmdEditDelete)
    {
      if(fFirstKey)
      {
        cursorCol = len = 0;
        szLine[0] = '\0';
      }
      else if(cursorCol < len)
      {
        memmove(&szLine[cursorCol], &szLine[cursorCol+1], (len-cursorCol));
        --len;
        fEdited = TRUE;
      }
    }
    else if(pfnCmd == NedCmdEditInsertSelf)
    {
      if(fFirstKey)
      {
        cursorCol = len = 0;
        szLine[0] = '\0';
      }
      if(len < maxLen)
      {
        memmove(&szLine[cursorCol+1], &szLine[cursorCol], (len-cursorCol)+1);
        szLine[cursorCol] = c;
        ++cursorCol;
        ++len;
        fEdited = TRUE;
      }
    }
    else if(pfnCmd == NedCmdEditPaste)
    {
      if(fFirstKey)
      {
        cursorCol = len = 0;
        szLine[0] = '\0';
      }
      if(szPasteStr && ((len + lenPaste) < maxLen))
      {
        memmove(&szLine[cursorCol+lenPaste], &szLine[cursorCol], (len-cursorCol)+1);
        memcpy(&szLine[cursorCol], szPasteStr, lenPaste);
        cursorCol += lenPaste;
        len += lenPaste;
        fEdited = TRUE;
      }
    }

    // NedLogPrintfEx(NEDLOG_MSG, "c = %u, len = %u, cursorCol = %u, line=(%s)\n", c, len, cursorCol, szLine);
    if(fFirstKey)
    {
      fFirstKey = FALSE;
      fEdited = TRUE;
    }
  } // end of while loop

  return len;
}

/*!------------------------------------------------------------------------------------------------
  Get a prompt with a list of exit keys. Esc is assumed. Up to 12 exit keys. Enter will do 1st
  choice. Example:

  Replace With: world (y/n/a):
  Save: myfile.c (y/n/a): 

  @ingroup  ned_msg
  @param    szPrompt      prompt
  @param    szPrompt2     Optional 2nd prompt (may be NULL)
  @param    pExitKeys     A list of ascii characters (e.g. "yna"). 

  @returns  tolower() of exit key, or NEDKEY_ESC if quit
*///-----------------------------------------------------------------------------------------------
nedKey_t NedMsgChoice(const char *szPrompt, const char *szPrompt2, const char *szExitKeys)
{
  #define MAX_EXIT_KEYS   8
  char          szExitPrompt[MAX_EXIT_KEYS * 2 + 6];
  nedKey_t      c;
  unsigned      i;
  char         *szChar;
  unsigned      width;

  NedAssertDbg(szPrompt);
  NedAssertDbg(szExitKeys && szExitKeys[0]);

  // converts "yna" to " (y/n/a): "
  strcpy(szExitPrompt, " (");
  szChar = szExitPrompt + strlen(szExitPrompt);
  for(i=0; (i < MAX_EXIT_KEYS) && (i < strlen(szExitKeys)); ++i)
  {
    *szChar++ = tolower(szExitKeys[i]);
    if( ((i+1) < MAX_EXIT_KEYS) && (szExitKeys[i+1] != '\0') )
      *szChar++ = '/';
  }
  strcpy(szChar, "): ");

  // if middle prompt, make sure it fits on screen
  if(szPrompt2 != NULL)
  {
    width = NedMsgWidth(szPrompt, szExitPrompt);
    szPrompt2 = NedStrFitName(szPrompt2, width);
  }
  else
  {
    szPrompt2 = "";
  }

  NedMsgPrintf("%s%s%s", szPrompt, szPrompt2, szExitPrompt);
  NedWinFlushCursor();
  while(1)
  {
    c = NedKeyGetKey();
    if(c == NEDKEY_ENTER)
    {
      c = szExitKeys[0];
      break;
    }
    if(c == NEDKEY_ESC)
      break;

    szChar = NedStrIChr(szExitKeys, c);
    if(szChar)
    {
      c = *szChar;
      break;
    }
  }

  c = tolower(c);

  return c;
}


/*!------------------------------------------------------------------------------------------------
  Determine remaining width given the prompt and (optional) exit keys.

  That is, in example below, how wide can string for "myfile.c" be? Assumes windowing system has
  been initialized.

      Save: myfile.c (y/n/a): 

  @ingroup  ned_msg
  @param    szPrompt      prompt (e.g. "Save: ")
  @param    pExitKeys     A list of ascii characters (e.g. "yna"), or NULL

  @returns  remaining width. May be 0.
*///-----------------------------------------------------------------------------------------------
unsigned NedMsgWidth(const char *szPrompt, const char *szExitKeys)
{
  winPos_t  cols;
  unsigned  remainingWidth;

  NedWinGetScreenSize(NULL, &cols);
  remainingWidth = (unsigned)(cols - 1);
  if(remainingWidth >= strlen(szPrompt))
    remainingWidth -= strlen(szPrompt);
  else
    remainingWidth = 0;
  if(szExitKeys)
  {
    if(remainingWidth >= strlen(szExitKeys) + 2)
      remainingWidth -= strlen(szExitKeys) + 2;
    else 
      remainingWidth = 0;
  }
  return remainingWidth;
}

/*!------------------------------------------------------------------------------------------------
  Get a string that will fit in the message line. Common case:

  Closed .../my/long/filename.c

  @ingroup  ned_msg
  @param    szPrompt    prompt (e.g. "Save: ")
  @param    szStr       The string to potentially shorten

  @returns  remaining width. May be 0.
*///-----------------------------------------------------------------------------------------------
const char *NedMsgFit(const char *szPrompt, const char *szStr)
{
  return NedStrFitName(szStr, NedMsgWidth(szPrompt, NULL));
}

/*!------------------------------------------------------------------------------------------------
  Get a string that will fit in the choice prompt. Common case:

  Save: .../my/long/filename.c (y/n)

  @ingroup  ned_msg
  @param    szPrompt    prompt (e.g. "Save: ")
  @param    szStr       The string to potentially shorten

  @returns  remaining width. May be 0.
*///-----------------------------------------------------------------------------------------------
const char *NedMsgFitEx(const char *szPrompt, const char *szStr, const char *szExitKeys)
{
  return NedStrFitName(szStr, NedMsgWidth(szPrompt, szExitKeys));
}
