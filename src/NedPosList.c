/*!************************************************************************************************
  @file NedPointList.c

  @brief  API for a list of points

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_point

  Uses the NedList module to handle lists of nedPos_t items.

*///***********************************************************************************************
#include "NedPosList.h"
#include "NedLog.h"

/*!------------------------------------------------------------------------------------------------
  Show the nedPos_t item to the log

  @ingroup  ned_test
  @param    pPos        valid nedPos_t item

  @returns   none
*///-----------------------------------------------------------------------------------------------
void NedPosListLogShow(const void *_pPos)
{
  const sNedPos_t *pPos = _pPos;
  NedLogPrintf("pPos: pFile %p:%s, pos %ld", pPos->pFile, pPos->pFile ? NedFilePath(pPos->pFile) : "(null)", pPos->pos);
}

/*!------------------------------------------------------------------------------------------------
  Are the two nedPos_t items the same?

  @ingroup  ned_test
  @param    pPos1       valid nedPos_t item
  @param    pPos1       valid nedPos_t item

  @returns   TRUE if the same, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedPosListIsSame(const void *_pPos1, const void *_pPos2)
{
  const sNedPos_t  *pPos1 = _pPos1;
  const sNedPos_t  *pPos2 = _pPos2;
  return ((pPos1->pFile == pPos2->pFile) && (pPos1->pos == pPos2->pos)) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Inserting text, adjust points after the insert

  @param      hList     list of points, may point to multiple buffer chains
  @param      pPoint    point in some buffer chain
  @param      len       length of insert
  @returns    none
-------------------------------------------------------------------------------------------------*/
void NedPosListAdjustIns(hNedList_t hList, const sNedPoint_t *pPoint, long len)
{
  long          topPos = NedPointPos(pPoint);
  sNedPos_t    *pPos;
  unsigned      i;

  NedListIterStart(hList);
  for(i=0; i < NedListNumItems(hList); ++i)
  {
    pPos = NedListNext(hList);
    if(pPos && NedBufIsSameChain(pPos->pFile->pBufHead, pPoint->pBuf))
    {
      if(pPos->pos >= topPos)
        pPos->pos += len;
    }
  }
}

/*!------------------------------------------------------------------------------------------------
  Deleting text, any points in between pTop and pBot are deleted. Any points after pBot are
  adjusted down.

  @param      hList     list of points, may point to multiple buffer chains
  @param      pTop      top point in some buffer chain
  @param      pBot      bottom point in some buffer chain
  @returns    none
-------------------------------------------------------------------------------------------------*/
void NedPosListAdjustDel(hNedList_t hList, const sNedPoint_t *pTop, const sNedPoint_t *pBot)
{
  long          topPos = NedPointPos(pTop);
  long          botPos = NedPointPos(pBot);
  long          len;
  sNedPos_t    *pPos;
  unsigned      i;

  NedAssertDbg(NedBufIsSameChain(pTop->pBuf, pBot->pBuf));
  if(topPos > botPos)
  {
    len    = topPos;
    topPos = botPos;
    botPos = len;
  }
  len = botPos - topPos;

  NedListIterStart(hList);
  for(i=0; i < NedListNumItems(hList); ++i)
  {
    pPos = NedListNext(hList);
    if(pPos && NedBufIsSameChain(pPos->pFile->pBufHead, pTop->pBuf))
    {
      // just got deleted
      if(pPos->pos > topPos && pPos->pos < botPos)
        NedListRemove(hList, pPos);

      // adjust down
      else if(pPos->pos >= botPos)
        pPos->pos -= len;
    }
  }
}

/*!------------------------------------------------------------------------------------------------
  Remove positions that are in this file, as the file is closing.

  @ingroup  ned_editor
  @param    hList      
  @param    pFile      pointer to a file

  @return   1-n (index)
*///-----------------------------------------------------------------------------------------------
void NedPosListRemoveAll(hNedList_t hList, const sNedFile_t *pFile)
{
  sNedPos_t    *pPos;
  unsigned      i;

  NedListIterStart(hList);
  for(i=0; i < NedListNumItems(hList); ++i)
  {
    pPos = NedListNext(hList);
    if(pPos && pPos->pFile == pFile)
      NedListRemove(hList, pPos);
  }
}
