/*!************************************************************************************************
  @file NedBuffer.h

  Ned Buffer API

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_buf Ned Buffer - An API for a creating, manipulating and searching large data.

  A buffer chain is basically an efficient blob (binary large object). A single buffer is a chain
  of 1. If adding a byte to the middle of the blob, it will move at most 4K bytes. If editing to 1
  GB file, you can see how this would be more efficient.

  The buffer chain is a dual-direction linked list, so it can be navigated in any order.

  Create as many buffer chains as needed (and memory allows).

  Each buffer chain has an id (managed by higher layer) so they can easly be distinquished from
  one another with only a single buffer.

  A buffer may have a length of 0 if it's the last buffer or if all the data has been deleted from
  the buffer.

  Entire length of the buffer chain cannot exceed LONG_MAX (9.2 Exabytes on 64-bit systems).

*///***********************************************************************************************
#ifndef NED_BUFFER_H
#define NED_BUFFER_H
#include "Ned.h"


// each buffer is fixed in size. A file is made up of a linked list of buffers
#ifndef NEDBUF_SIZE
 #define NEDBUF_SIZE      (8192 - 32)
#endif

#define NEDBUF_SANCHK      777U   // sanity check


typedef struct sNedBuffer_t
{
  struct sNedBuffer_t *pPrev;     // NULL if at start of list
  struct sNedBuffer_t *pNext;     // NULL if at end of list
  unsigned             sanchk;    // sanity check
  unsigned             id;        // id for entire chain
  unsigned             len;       // length of data in the buffer (0 - (NEDBUF_SIZE-1))
  uint8_t              aData[NEDBUF_SIZE];
} sNedBuffer_t;

// buffer API
sNedBuffer_t   *NedBufNew         (unsigned id);
sNedBuffer_t   *NedBufAppend      (sNedBuffer_t *pBuf);
sNedBuffer_t   *NedBufFree        (sNedBuffer_t *pBuf);
bool_t          NedBufIsBuf       (const sNedBuffer_t *pBuf);
bool_t          NedBufIsBufChain  (const sNedBuffer_t *pBufHead);
bool_t          NedBufIsSameChain (const sNedBuffer_t *pBuf1, const sNedBuffer_t *pBuf2);
bool_t          NedBufIsFirst     (const sNedBuffer_t *pBuf);
bool_t          NedBufIsLast      (const sNedBuffer_t *pBuf);
bool_t          NedBufIsFull      (const sNedBuffer_t *pBuf);
sNedBuffer_t   *NedBufNext        (const sNedBuffer_t *pBuf);
sNedBuffer_t   *NedBufPrev        (const sNedBuffer_t *pBuf);
sNedBuffer_t   *NedBufHead        (const sNedBuffer_t *pBuf);
sNedBuffer_t   *NedBufTail        (const sNedBuffer_t *pBuf);
uint8_t        *NedBufData        (const sNedBuffer_t *pBuf);
unsigned        NedBufId          (const sNedBuffer_t *pBuf);
unsigned        NedBufLen         (const sNedBuffer_t *pBuf);
unsigned        NedBufMaxLen      (void);

// buffer basic edit functions
bool_t          NedBufSplit       (sNedBuffer_t *pBuf, unsigned offset);
bool_t          NedBufGrow        (sNedBuffer_t *pBuf, unsigned offset, long addLen);
bool_t          NedBufShrink      (sNedBuffer_t *pBuf, unsigned offset, long removeLen);
long            NedBufInsert      (sNedBuffer_t *pBuf, unsigned offset, const uint8_t *pData, long len);

// buffer chain functions
void            NedBufChainFree   (sNedBuffer_t *pBufHead);
void            NedBufChainClear  (sNedBuffer_t *pBufHead);
long            NedBufChainCopy   (sNedBuffer_t *pBufTo, unsigned toOffset,
                                   const sNedBuffer_t *pBufFrom, unsigned fromOffset, long len);
long            NedBufChainSize   (const sNedBuffer_t *pBufHead, long *pNumBytes);

// file operations
nedErr_t        NedBufFileLoad    (sNedBuffer_t *pBufHead, const char *szFilePath, long *pFileSize);
nedErr_t        NedBufFileSave    (const char *szFilePath, const sNedBuffer_t *pBufHead);
void            NedBufCondense    (sNedBuffer_t *pBufHead);
bool_t          NedBufCrLfToLf    (sNedBuffer_t *pBufHead);
bool_t          NedBufLfToCrLf    (sNedBuffer_t *pBufHead);
const sNedBuffer_t *NedBufSearch  (const sNedBuffer_t *pBufHead, uint8_t c, unsigned *pOffset);

// debug functions
void            NedBufSetMaxLen   (unsigned len);
long            NedBufLogDumpChain(const sNedBuffer_t *pBufHead, unsigned lineLen);

#endif // NED_BUFFER_H
