/*!************************************************************************************************
  @file NedPoint.h

  @brief  Ned Point API

  @copyright Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_point Ned Point - An API for searching, manipulating points in a buffer chain.

  Points can be used for things like an editor cursor, the start/end of a selected range,
  bookmarks, file search and replace, etc...

  Points treat the end of a line as a single character, regardless of whether it's `\r\n` Windows
  style line endings or `\n` Unix style line endings. Tabs are just another character and points
  count them as a single character. Any tabs to spaces is a higher-layer thing.

  Points do not act on the display in any way.

  Points are built on top of the NedBuffer system. This point API intimately understands the
  buffer chain. It's OK to have lists of points in different buffer chains.

  Points need to keep track of themselves. If data is inserted or removed into a buffer chain,
  the buffer the point pointed to may be gone. If data is added, the buffer will never be gone,
  but the offset may have changed.

  For example, if pointA points at offset 5 in buf2 below, and NedBufShrink() is called that
  removes buf2, it may not longer exist. If a pointB points to offset 19 in buf1, and the first 3
  bytes are removed from the buffer, the pointB should be adjusted to offset 16.

      buf1[  ]
      buf2[  ]
      buf3[  ] 

  Neither pointA or PointB would have to move if added/deleted buffers didn't affect them, for
  example, if buf2a is added as shown below, no points in buf1, buf2 or buf3 need change.

      buf1[  ]
      buf2[  ]
      buf2a[  ]
      buf3[  ] 

  There is no "copy" function. To copy a point, simply copy the structure.

*///***********************************************************************************************
#ifndef NED_POINT_H
#define NED_POINT_H

#include "NedBuffer.h"
#include "NedUtilStr.h"

#define NEDPOINT_SANCHK    32123
// #define NEDHISTORY_SANCHK  27272

// a point in the buffer (for example, the cursor position, or start/end of selection)
typedef struct
{
  sNedBuffer_t   *pBuf;       // pointer to buffer
  unsigned        sanchk;     // sanity check
  unsigned        offset;     // offset within the buffer
} sNedPoint_t;

typedef const void *hNedPtHistory_t;

typedef struct
{
  char    *szFile;
  char    *szMsg;
  long    line;
  long    col;
} sNedPtErrInfo_t;

typedef struct
{
  nedChar_t   c;        // ' ' or '\t'
  unsigned    nChars;   // 4 or 1
  unsigned    nCols;    // how many columns will this take up?
} sNedPtIndent_t;

typedef bool_t (*pfnIsFuncLine_t)(const sNedPoint_t *pPoint);

typedef unsigned  nedPtSearchOpts_t;  // see NedMsg.h, they must match
#define NEDPT_SEARCH_OPT_NONE              0x00
#define NEDPT_SEARCH_OPT_CASE_SENSITIVE    0x08 // C
#define NEDPT_SEARCH_OPT_WRAP              0x10 // A
#define NEDPT_SEARCH_OPT_WHOLE_WORD        0x20 // W
#define NEDPT_SEARCH_OPT_REVERSE           0x40

// basic functions
void          NedPointInit          (sNedPoint_t *pPoint, sNedBuffer_t *pBuf);
bool_t        NedPointIsPoint       (const sNedPoint_t *pPoint);
bool_t        NedPointIsSame        (const sNedPoint_t *pThis, const sNedPoint_t *pThat);

// comparison, position
bool_t        NedPointIsAtBottom    (const sNedPoint_t *pPoint);
bool_t        NedPointIsAtTop       (const sNedPoint_t *pPoint);
bool_t        NedPointIsAtBegLine   (const sNedPoint_t *pPoint);
bool_t        NedPointIsBetween     (const sNedPoint_t *pThis, const sNedPoint_t *pThat, const sNedPoint_t *pTest);
bool_t        NedPointIsSameChain   (const sNedPoint_t *pThis, const sNedPoint_t *pThat);
long          NedPointPos           (const sNedPoint_t *pPoint);
long          NedPointDifference    (const sNedPoint_t *pThis, const sNedPoint_t *pThat);
bool_t        NedPointSort2Points   (sNedPoint_t *pThis, sNedPoint_t *pThat);

// basic movement functions char/word/line/str functions
long          NedPointCharCount     (const sNedPoint_t *pPoint, nedChar_t c);
nedChar_t     NedPointCharGet       (const sNedPoint_t *pPoint);
nedChar_t     NedPointCharGetNext   (sNedPoint_t *pPoint);
nedChar_t     NedPointCharGetPrev   (sNedPoint_t *pPoint);
bool_t        NedPointCharNext      (sNedPoint_t *pPoint);
bool_t        NedPointCharPrev      (sNedPoint_t *pPoint);
bool_t        NedPointCharAdvance   (sNedPoint_t *pPoint, long amount);
bool_t        NedPointWordBeg       (sNedPoint_t *pPoint);
bool_t        NedPointWordEnd       (sNedPoint_t *pPoint);
bool_t        NedPointWordNext      (sNedPoint_t *pPoint);
bool_t        NedPointWordPrev      (sNedPoint_t *pPoint);
bool_t        NedPointLineNext      (sNedPoint_t *pPoint);
bool_t        NedPointLinePrev      (sNedPoint_t *pPoint);
bool_t        NedPointLineBeg       (sNedPoint_t *pPoint);
bool_t        NedPointLineEnd       (sNedPoint_t *pPoint);
long          NedPointLineGet       (const sNedPoint_t *pPoint, long *pCol);
long          NedPointLineGoto      (sNedPoint_t *pPoint, long line);
void          NedPointGotoLineCol   (sNedPoint_t *pPoint, long line, long col);
void          NedPointFileBottom    (sNedPoint_t *pPoint);
void          NedPointFileGotoPos   (sNedPoint_t *pPoint, long pos);
unsigned      NedPointFileIndent    (const sNedPoint_t *pPoint);
long          NedPointFileSize      (const sNedPoint_t *pPoint);
void          NedPointFileTop       (sNedPoint_t *pPoint);

// string functions
long          NedPointStrGet        (uint8_t *pTo, const sNedPoint_t *pFrom, long maxLen);
int           NedPointStrNCmp       (const sNedPoint_t *pPoint, const char *sz, long len);
int           NedPointStrCmp        (const sNedPoint_t *pPoint, const char *sz);
long          NedPointSymbolGet     (uint8_t *pTo, const sNedPoint_t *pFrom, long maxLen);
bool_t        NedPointStrChrRev     (sNedPoint_t *pPoint, nedChar_t c);
bool_t        NedPointMemChr        (sNedPoint_t *pPoint, nedChar_t c, long maxLen);
long          NedPointMemGet        (uint8_t *pTo, const sNedPoint_t *pFrom, long maxLen);

// edit functions
bool_t        NedPointInsChar       (sNedPoint_t *pPoint, nedChar_t c);
bool_t        NedPointInsBlock      (sNedPoint_t *pPoint, const uint8_t *pData, long insertLen);
bool_t        NedPointInsStr        (sNedPoint_t *pPoint, const char *sz);
bool_t        NedPointDelBackspace  (sNedPoint_t *pPoint);
bool_t        NedPointDelChar       (sNedPoint_t *pPoint);
bool_t        NedPointDelBlock      (sNedPoint_t *pThis, sNedPoint_t *pThat);

// advanced edit functions
bool_t        NedPointWrapText      (const sNedPtIndent_t *pIndent, const sNedPoint_t *pThis, sNedPoint_t *pThat, long width);
bool_t        NedPointDumpToPoint   (sNedPoint_t *pTo, long startAddr, const sNedPoint_t *pFrom, long len);

// clipboard functions
void          NedPointClipClear     (sNedPoint_t *pClip);
long          NedPointClipCopy      (sNedPoint_t *pClip, const sNedPoint_t *pThis, const sNedPoint_t *pThat);
long          NedPointClipCut       (sNedPoint_t *pClip, sNedPoint_t *pThis, sNedPoint_t *pThat);
long          NedPointClipPaste     (sNedPoint_t *pPoint, const sNedPoint_t *pClip);

// for finding functions
bool_t        NedPointIsFuncLine    (const sNedPoint_t *pPoint, pfnIsFuncLine_t pfnIsFuncLine);
bool_t        NedPointFuncNext      (sNedPoint_t *pPoint, pfnIsFuncLine_t pfnIsFuncLine);
bool_t        NedPointFuncPrev      (sNedPoint_t *pPoint, pfnIsFuncLine_t pfnIsFuncLine);
bool_t        NedPointIsFnC         (const sNedPoint_t *pPoint);
bool_t        NedPointIsFnJavascript(const sNedPoint_t *pPoint);
bool_t        NedPointIsFnPython    (const sNedPoint_t *pPoint);
bool_t        NedPointIsFnHtml      (const sNedPoint_t *pPoint);
bool_t        NedPointIsFnText      (const sNedPoint_t *pPoint);

// misc functions
int           NedPointIsBrace       (const sNedPoint_t *pPoint);
bool_t        NedPointBraceMatch    (sNedPoint_t *pPoint);
bool_t        NedPointIsErrLine     (const sNedPoint_t *pPoint, sNedPtErrInfo_t *pErrInfo);
bool_t        NedPointErrLineNext   (sNedPoint_t *pPoint, sNedPtErrInfo_t *pErrInfo);
bool_t        NedPointErrLinePrev   (sNedPoint_t *pPoint, sNedPtErrInfo_t *pErrInfo);
void          NedPointErrFree       (sNedPtErrInfo_t *pErrInfo);
bool_t        NedPointSearch        (sNedPoint_t *pPoint, const char *sz, nedPtSearchOpts_t opts);
long          NedPointSkipWhite     (sNedPoint_t *pPoint);

// debugging functions
void          NedPointLogShow       (const sNedPoint_t *pPoint);
const char *  NedPointLogStr        (const sNedPoint_t *pPoint);

#endif // NED_POINT_H
