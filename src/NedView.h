/*!************************************************************************************************
  @file NedView.h

  @brief  Ned View API - Writes the editor lines.

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_msg   Ned Message - Functions dealing with the message and status bar

*///***********************************************************************************************
#ifndef NED_VIEW_H
#define NED_VIEW_H

#include "Ned.h"
#include "NedTheme.h"
#include "NedFile.h"

#define ROW_MENU          0
#define ROW_EDIT          1
#define ROW_STATUS       -2
#define ROW_MSG          -1
#define NUM_OTHER_ROWS    3         // menu (top), status, message (bottom)

typedef  void * hNedView_t;         // handle to view

bool_t      NedViewInit               (void);
void        NedViewShutdown           (void);
hNedView_t  NedViewNew                (const nedTheme_t *pTheme);
void        NedViewFree               (hNedView_t hView);
void        NedViewPickView           (hNedView_t hView);
bool_t      NedViewIsView             (hNedView_t hView);
void        NedViewLogShow            (hNedView_t hView);
bool_t      NedViewCheckScreenSize    (hNedView_t hView);
unsigned    NedViewEditRows           (hNedView_t hView);
unsigned    NedViewScreenRows         (hNedView_t hView);
unsigned    NedViewScreenCols         (hNedView_t hView);
void        NedViewInvalidateRow      (hNedView_t hView, unsigned row);
void        NedViewInvalidateAll      (hNedView_t hView);
void        NedViewInvalidateSelected (hNedView_t hView, const sNedFile_t *pFile);
void        NedViewSetTheme           (hNedView_t hView, const nedTheme_t *pTheme);
void        NedViewCls                (hNedView_t hView);
void        NedViewDraw               (hNedView_t hView, const sNedFile_t *pFile);
void        NedViewFlushCursor        (hNedView_t hView, const sNedFile_t *pFile);
void        NedViewFlushOnly          (hNedView_t hView);
void        NedViewFlushAll           (hNedView_t hView);
void       *NedViewWinGetPicked       (hNedView_t hView);
void        NedViewWinPick            (hNedView_t hView, void *pWin);

#endif // NED_VIEW_H
