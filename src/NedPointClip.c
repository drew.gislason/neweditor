/*!************************************************************************************************
  @file NedPointClip.c

  @brief  Ned Point Clipboard Functions Implementation

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include <ctype.h>
#include "NedLog.h"
#include "NedPoint.h"

/*!------------------------------------------------------------------------------------------------
  Clear the clipboard

  @ingroup  ned_point
  @param    pPoint       pointer to valid point

  @return   TRUE if worked
*///-----------------------------------------------------------------------------------------------
void NedPointClipClear(sNedPoint_t *pClip)
{
  NedAssertDbg(NedPointIsPoint(pClip));

  NedPointFileTop(pClip);
  NedBufChainClear(pClip->pBuf);
}

/*!------------------------------------------------------------------------------------------------
  Inserts text from pThis to pThat into pClip. Doesn't matter if pThis is before or after pThat

  @ingroup  ned_point
  @param    pClip       pointer to clipboard
  @param    pThis       pointer to valid point
  @param    pThat       pointer to valid point in same change as pThis

  @return   # of bytes copied into clipboard
*///-----------------------------------------------------------------------------------------------
long NedPointClipCopy(sNedPoint_t *pClip, const sNedPoint_t *pThis, const sNedPoint_t *pThat)
{
  long            diff;
  sNedPoint_t     sThis;

  NedAssertDbg(NedPointIsPoint(pClip));
  NedAssertDbg(NedPointIsSameChain(pThis, pThat));

  // if pThat < pThis, then need to start with pThat
  diff = NedPointDifference(pThis, pThat);
  if(diff != 0L)
  {
    if(diff < 0)
    {
      diff = -1L * diff;
      sThis = *pThat;
      pThis = &sThis;
    }

    // copy the data from buffer into the clipboard
    diff = NedBufChainCopy(pClip->pBuf, pClip->offset, pThis->pBuf, pThis->offset, diff);
    NedPointCharAdvance(pClip, diff);
  }

  return diff;
}


/*!------------------------------------------------------------------------------------------------
  Appends block into clipboard, then cuts it from the original text. Both points will end up on the
  1st point after delete.

  @ingroup  ned_point
  @param    pClip       pointer to clipboard
  @param    pThis       pointer to valid point
  @param    pThat       pointer to valid point in same chain as pThis

  @return   TRUE if worked
*///-----------------------------------------------------------------------------------------------
long NedPointClipCut(sNedPoint_t *pClip, sNedPoint_t *pThis, sNedPoint_t *pThat)
{
  long    diff;

  NedAssertDbg(NedPointIsPoint(pClip));
  NedAssertDbg(NedPointIsSameChain(pThis, pThat));

  diff = NedPointClipCopy(pClip, pThis, pThat);
  NedPointDelBlock(pThis, pThat);

  return diff;
}


/*!------------------------------------------------------------------------------------------------
  Paste the entire clip buffer into the point. Does not affect clipboard. Point is advanced
  to just past the inserted data. Example: if pPoint is at the "c" in "abcdef" inserting "xyz"
  results in "abxyzcdef" with pPoint still at "c", just after "z".

  @ingroup  ned_point
  @param    pPoint       pointer to valid point

  @return   length pasted (may be 0)
*///-----------------------------------------------------------------------------------------------
long NedPointClipPaste(sNedPoint_t *pPoint, const sNedPoint_t *pClip)
{
  long          len;
  sNedPoint_t   sClip;

  NedLogPrintfEx(NEDLOG_POINT, "NedPointClipPaste(pPoint->buf %p, offset %u, pClip %p\n", pPoint->pBuf, pPoint->offset, pClip->pBuf);

  NedAssertDbg(NedPointIsPoint(pPoint));
  NedAssertDbg(NedPointIsPoint(pClip));

  sClip = *pClip;
  NedPointFileBottom(&sClip);
  len = NedPointPos(&sClip);
  NedPointFileTop(&sClip);

  // past from clip board into text file
  len = NedBufChainCopy(pPoint->pBuf, pPoint->offset, sClip.pBuf, sClip.offset, len);
  NedPointCharAdvance(pPoint, len);

  NedLogPrintfEx(NEDLOG_POINT, "  len %ld, pPoint->buf %p, offset %u\n", len, pPoint->pBuf, pPoint->offset);

  return len;
}
