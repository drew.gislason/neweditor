/*!************************************************************************************************
  @file Ned.h

  @brief  Ned.h should be included first in all files. Defines things common to all C programs

  @copyright Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#ifndef NED_H
#define NED_H

// allows source to be compiled with gcc or g++ compilers
#ifdef __cplusplus
  #define EXTERN_C extern "C" {
  #define EXTERN_C_END }
#else
  #define EXTERN_C
  #define EXTERN_C_END
#endif

EXTERN_C

// not defined in older compilers
#ifndef __STDC_VERSION__
  #ifdef __cplusplus
    #define __STDC_VERSION__ __cplusplus
  #else
    #define __STDC_VERSION__ 198901L
  #endif
#endif

// determine platform
#define NED_WINDOWS   0
#define NED_MACOS     1
#define NED_LINUX     2
#ifdef _WIN32
  #define NED_PLATFORM    NED_WINDOWS
  #define NED_OS   "Windows"
#else
  #ifdef __APPLE__
    #define NED_PLATFORM  NED_MACOS
    #define NED_OS   "macOs"
  #else
    #define NED_PLATFORM  NED_LINUX
    #define NED_OS   "Linux"
  #endif
#endif

// common includes
#include <stdio.h>    // for printf(), etc..
#include <stdint.h>   // for uint32_t, uint8_t, etc..
#include <limits.h>   // for LONG_MAX, etc...
#include <stdlib.h>   // for malloc(), etc..
#include <string.h>   // for memcpy(), etc..
#include "NedCfg.h"
#include "NedErr.h"
#include "NedAssert.h"

#define NEDVER        "Ned v2.0"
#define NEDVER_NUM    200

// boolean
typedef unsigned bool_t;
#define TRUE 1
#define FALSE 0

// number of elements in an array of any kind
#define NumElements(a) (sizeof(a)/sizeof((a)[0]))

void       NedExit(void);

EXTERN_C_END

#endif // NED_H
