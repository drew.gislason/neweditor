/*!************************************************************************************************
  @file NedKeyMap.c

  @brief  Default/Factory Key and Menu Map Implementation

  This module maps both keys and menus. It maps keys to commands. It maps menu items to keys and
  commands. It has a set of functions to set up alternate key/commmand pairs as well.

  Knows nothing about the vlue. Pure model

  @copyright Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*///***********************************************************************************************
#include "Ned.h"
#include "NedCmd.h"
#include "NedTheme.h"
#include "NedMenu.h"
#include "NedKeyMap.h"

typedef struct
{
  unsigned            nItems;
  const pfnNedCmd_t  *aCmds;
} sNedMenuCmdMap_t;

// static void     NkmSetMenuHotKeys (void);
static nedKey_t NkmFindHotkey     (pfnNedCmd_t pfnCmd);
static void     NkmSetCommands    (unsigned nItems, const sNedKeyMap_t *pKeyMap);

// default key mappings for editor window
static const sNedKeyMap_t maNedKeyMapDefault[] =
{
  { NEDKEY_CTRL_SPACE,        NedCmdMoveCenter },
  { NEDKEY_CTRL_A,            NedCmdSelectAll },
  { NEDKEY_CTRL_B,            NedCmdSearchBackward },
  { NEDKEY_CTRL_C,            NedCmdEditCopy },
  { NEDKEY_CTRL_D,            NedCmdSelectWord },
  { NEDKEY_CTRL_E,            NedCmdSearchAgain },
  { NEDKEY_CTRL_F,            NedCmdSearchForward },
  { NEDKEY_CTRL_G,            NedCmdMoveGotoLine },
  { NEDKEY_CTRL_H,            NedCmdBookmarkPrev },
  { NEDKEY_TAB,               NedCmdEditIndent },
  { NEDKEY_CTRL_J,            NedCmdBookmarkNext },
  { NEDKEY_CTRL_K,            NedCmdEditCutEol },
  { NEDKEY_CTRL_L,            NedCmdSelectLine },
  { NEDKEY_ENTER,             NedCmdEditEnter },
  { NEDKEY_CTRL_N,            NedCmdFileNew },
  { NEDKEY_CTRL_O,            NedCmdFileOpen },
  { NEDKEY_CTRL_P,            NedCmdMacroPlay },
  { NEDKEY_CTRL_Q,            NedCmdMacroRecord },
  { NEDKEY_CTRL_R,            NedCmdReplace },
  { NEDKEY_CTRL_S,            NedCmdFileSave },
  { NEDKEY_CTRL_T,            NedCmdEditToggleCase },
  { NEDKEY_CTRL_U,            NedCmdEditCutLine },
  { NEDKEY_CTRL_V,            NedCmdEditPaste },
  { NEDKEY_CTRL_W,            NedCmdFileClose },
  { NEDKEY_CTRL_X,            NedCmdEditCut },
  { NEDKEY_CTRL_Y,            NedCmdEditRedo },
  { NEDKEY_CTRL_Z,            NedCmdEditUndo },
  { NEDKEY_ESC,               NedCmdSelectOff },
  { NEDKEY_CTRL_BACKSLASH,    NedCmdSelectToggle },
  { NEDKEY_CTRL_RIGHT_BRACE,  NedCmdMoveMatchBrace },
  { NEDKEY_CTRL_CAROT,        NedCmdBookmarkToggle },
  { NEDKEY_CTRL_MINUS,        NedCmdMovePosPrev },
  { NEDKEY_BACKSPACE,         NedCmdEditBackspace },
  { NEDKEY_UP,                NedCmdCursorUp },
  { NEDKEY_DOWN,              NedCmdCursorDown },
  { NEDKEY_LEFT,              NedCmdCursorLeft },
  { NEDKEY_RIGHT,             NedCmdCursorRight },
  { NEDKEY_HOME,              NedCmdCursorHome },
  { NEDKEY_END,               NedCmdCursorEnd },
  { NEDKEY_PGUP,              NedCmdCursorPgUp },
  { NEDKEY_PGDN,              NedCmdCursorPgDn },
  { NEDKEY_CTRL_UP,           NedCmdMoveFunctionUp },
  { NEDKEY_CTRL_DOWN,         NedCmdMoveFunctionDown },
  { NEDKEY_CTRL_LEFT,         NedCmdCursorWordLeft },
  { NEDKEY_CTRL_RIGHT,        NedCmdCursorWordRight },
  { NEDKEY_CTRL_HOME,         NedCmdCursorHome },
  { NEDKEY_CTRL_END,          NedCmdCursorEnd },
  { NEDKEY_CTRL_PGUP,         NedCmdCursorTop },
  { NEDKEY_CTRL_PGDN,         NedCmdCursorBottom },
  { NEDKEY_ALT_UP,            NedCmdCursorUp },
  { NEDKEY_ALT_DOWN,          NedCmdCursorDown },
  { NEDKEY_ALT_LEFT,          NedCmdCursorHome },
  { NEDKEY_ALT_RIGHT,         NedCmdCursorEnd },
  { NEDKEY_ALT_HOME,          NedCmdCursorHome },
  { NEDKEY_ALT_END,           NedCmdCursorEnd },
  { NEDKEY_ALT_PGUP,          NedCmdCursorTop },
  { NEDKEY_ALT_PGDN,          NedCmdCursorBottom },
  { NEDKEY_SHIFT_LEFT,        NedCmdCursorLeftSelect },
  { NEDKEY_SHIFT_RIGHT,       NedCmdCursorRightSelect },
  { NEDKEY_FN1,               NedCmdHelp },
  { NEDKEY_FN2,               NedCmdFileOpenWildcard },
  { NEDKEY_FN3,               NedCmdSearchAgain },
  { NEDKEY_FN4,               NedCmdSearchProject },
  { NEDKEY_FN5,               NedCmdReplaceProject },
  { NEDKEY_FN6,               NedCmdSettingsTheme },
  { NEDKEY_FN7,               NedCmdEditSortLines },
  { NEDKEY_FN8,               NedCmdEditWrapText },
  { NEDKEY_FN9,               NedCmdMoveErrPrev },
  { NEDKEY_FN10,              NedCmdMoveErrNext },
  { NEDKEY_FN11,              NedCmdFilePrevOpen },
  { NEDKEY_FN12,              NedCmdFileNextOpen },
  { NEDKEY_ALT_A,             NedCmdFileSaveAs },
  { NEDKEY_ALT_B,             NedCmdDebug },
  { NEDKEY_ALT_C,             NedCmdEditPbCopy },
  { NEDKEY_ALT_D,             NedCmdMenuEdit },
  { NEDKEY_ALT_E,             NedCmdDoNothing },
  { NEDKEY_ALT_F,             NedCmdMenuFile },
  { NEDKEY_ALT_G,             NedCmdMenuGoto },
  { NEDKEY_ALT_H,             NedCmdMenuHelp },
  { NEDKEY_ALT_I,             NedCmdDoNothing },
  { NEDKEY_ALT_J,             NedCmdDoNothing },
  { NEDKEY_ALT_K,             NedCmdExitSaveNone },
  { NEDKEY_ALT_L,             NedCmdDoNothing },
  { NEDKEY_ALT_M,             NedCmdDoNothing },
  { NEDKEY_ALT_N,             NedCmdDoNothing },
  { NEDKEY_ALT_O,             NedCmdMenuOption },
  { NEDKEY_ALT_P,             NedCmdDoNothing },
  { NEDKEY_ALT_Q,             NedCmdExitSaveAsk },
  { NEDKEY_ALT_R,             NedCmdDoNothing },
  { NEDKEY_ALT_S,             NedCmdMenuSearch },
  { NEDKEY_ALT_T,             NedCmdHelpDateTime },
  { NEDKEY_ALT_U,             NedCmdDoNothing },
  { NEDKEY_ALT_V,             NedCmdEditPbPaste },
  { NEDKEY_ALT_W,             NedCmdMenuWindow },
  { NEDKEY_ALT_X,             NedCmdExitSaveAll },
  { NEDKEY_ALT_Y,             NedCmdDoNothing },
  { NEDKEY_ALT_Z,             NedCmdDoNothing },
  { NEDKEY_BACK_TAB,          NedCmdEditUndent },
  { NEDKEY_DELETE,            NedCmdEditDelete },
  { NEDKEY_ALT_DASH,          NedCmdCursorTop },
  { NEDKEY_ALT_EQUAL,         NedCmdCursorBottom },
  { NEDKEY_ALT_LEFT_BRACE,    NedCmdMoveFunctionUp },
  { NEDKEY_ALT_RIGHT_BRACE,   NedCmdMoveFunctionDown },
  { NEDKEY_ALT_COLON,         NedCmdCursorPgUp },
  { NEDKEY_ALT_QUOTE,         NedCmdCursorPgDn },
  { NEDKEY_ALT_COMMA,         NedCmdMoveScrollUp },
  { NEDKEY_ALT_PERIOD,        NedCmdMoveScrollDown },
  { NEDKEY_ALT_SLASH,         NedCmdEditToggleComment },
  { NEDKEY_ALT_BACKSLASH,     NedCmdMoveToDefinition },
  { NEDKEY_ALT_SPACE,         NedCmdRedrawScreen }
};

// actual editor keymappings (sparse map)
static pfnNedCmd_t maNedKeyMap[256];    // assumes keys are numbered 0-255

// see also NedKeyMap.h, as any MenuItem with -> must be mapped
static sNedMenuItem_t maNedFileMenuItems[] =
{
  { "_New",                 NEDKEY_CTRL_N },
  { "_Open->",              NEDKEY_CTRL_O },  // see also NEDFILE_INDEX_FILE_OPEN in NedKeyMap.h
  { "Open _Recent->",       NEDKEY_NONE   },  // see also NEDFILE_INDEX_FILE_OPEN_RECENT in NedKeyMap.h
  { "Open _Wildcard...",    NEDKEY_FN2    },
  { "_Save",                NEDKEY_CTRL_S },
  { "Save _As...",          NEDKEY_ALT_A  },
  { "Save Al_l",            NEDKEY_NONE   },
  { "To_ggle Read Only",    NEDKEY_NONE   },
  { "Re_vert",              NEDKEY_NONE   },
  { "_Close",               NEDKEY_CTRL_W },
  { "Close All _Files",     NEDKEY_NONE   },
  { "_Quit",                NEDKEY_ALT_Q  },
  { "E_xit Save All",       NEDKEY_ALT_X  }
};

static const pfnNedCmd_t maNedFileMenuCommands[NumElements(maNedFileMenuItems)] =
{
  NedCmdFileNew, NedCmdFileOpen, NedCmdFileOpenRecent, NedCmdFileOpenWildcard, NedCmdFileSave,
  NedCmdFileSaveAs, NedCmdFileSaveAll, NedCmdFileToggleRdOnly, NedCmdFileRevert, NedCmdFileClose,
  NedCmdFileCloseAll, NedCmdExitSaveAsk, NedCmdExitSaveAll
};

static sNedMenuItem_t maNedEditMenuItems[] =
{
  { "C_ut",                 NEDKEY_CTRL_X         },
  { "_Copy",                NEDKEY_CTRL_C         },
  { "_Paste",               NEDKEY_CTRL_V         },
  { "_Undo",                NEDKEY_CTRL_Z         },
  { "_Redo",                NEDKEY_CTRL_Y         },
  { "Select Wor_d",         NEDKEY_CTRL_D         },
  { "Select _Line",         NEDKEY_CTRL_L         },
  { "Toggle _Select",       NEDKEY_CTRL_BACKSLASH },
  { "S_ort Lines",          NEDKEY_FN7            },
  { "_Wrap Text",           NEDKEY_FN8            },
  { "Co_mment Lines",       NEDKEY_ALT_SLASH      },
  { "_Toggle Case",         NEDKEY_CTRL_T         }
};

static const pfnNedCmd_t maNedEditMenuCommands[NumElements(maNedEditMenuItems)] =
{
  NedCmdEditCut, NedCmdEditCopy, NedCmdEditPaste, NedCmdEditUndo, NedCmdEditRedo,
  NedCmdSelectWord, NedCmdSelectLine, NedCmdSelectToggle, NedCmdEditSortLines,
  NedCmdEditWrapText, NedCmdEditToggleComment, NedCmdEditToggleCase, 
};

static sNedMenuItem_t maNedSearchMenuItems[] =
{
  { "_Again",                 NEDKEY_FN3           },
  { "_Backward...",           NEDKEY_CTRL_B        },
  { "_Forward...",            NEDKEY_CTRL_F        },
  { "_Replace...",            NEDKEY_CTRL_R        },
  { "_Project Search...",     NEDKEY_FN4           },
  { "Pro_ject Replace...",    NEDKEY_FN5           },
  { "Goto Definition",        NEDKEY_ALT_BACKSLASH },
  { "Set Search _Options...", NEDKEY_NONE          }
};

static const pfnNedCmd_t maNedSearchMenuCommands[NumElements(maNedSearchMenuItems)] =
{
  NedCmdSearchAgain, NedCmdSearchBackward, NedCmdSearchForward, NedCmdReplace,
  NedCmdSearchProject, NedCmdReplaceProject, NedCmdMoveToDefinition, NedCmdSearchOptions
};

static sNedMenuItem_t maNedGotoMenuItems[] =
{
  { "_Goto Line...",          NEDKEY_CTRL_G           },
  { "Go Bac_k",               NEDKEY_CTRL_MINUS       },
  { "Toggle _Bookmark",       NEDKEY_CTRL_CAROT       },
  { "_Jump To Next Bookmark", NEDKEY_CTRL_J           },
  { "Jump To _Prev Bookmark", NEDKEY_CTRL_H           },
  { "_Clear All Bookmarks",   NEDKEY_NONE             },
  { "Pre_vious Error",        NEDKEY_FN9              },
  { "Next _Error",            NEDKEY_FN10             },
  { "Previous _Open File",    NEDKEY_FN11             },
  { "_Next Open File",        NEDKEY_FN12             },
  { "_Match Brace",           NEDKEY_CTRL_RIGHT_BRACE }
};

static const pfnNedCmd_t maNedGotoMenuCommands[NumElements(maNedGotoMenuItems)] =
{
  NedCmdMoveGotoLine, NedCmdMovePosPrev, NedCmdBookmarkToggle, NedCmdBookmarkNext,
  NedCmdBookmarkPrev, NedCmdBookmarkClearAll, NedCmdMoveErrPrev, NedCmdMoveErrNext,
  NedCmdFilePrevOpen, NedCmdFileNextOpen, NedCmdMoveMatchBrace
};

// see also NedKeyMap.h, as any MenuItem with -> must be mapped
static sNedMenuItem_t maNedOptionMenuItems[] =
{
  { "Re_draw Screen",           NEDKEY_FN2    },
  { "_Record Macro",            NEDKEY_CTRL_Q },
  { "Play _Macro",              NEDKEY_CTRL_P },
  { "Map _Keys->",              NEDKEY_NONE   },  // see also NEDOPTION_INDEX_MAP_KEYS in NedKeyMap.h
  { "Color _Theme...",          NEDKEY_FN6    },
  { "Edit _Settings",           NEDKEY_NONE   },
  { "Ta_b Settings...",         NEDKEY_NONE   },
  { "Line _Endings...",         NEDKEY_NONE   },
  { "_Wrap Length...",          NEDKEY_NONE   },
  { "_Project folders->",       NEDKEY_NONE   },  // see also NEDOPTION_PROJECT_FOLDERS in NedKeyMap.h
  { "_Factory Settings",        NEDKEY_NONE   }
};

static const pfnNedCmd_t maNedOptionMenuCommands[NumElements(maNedOptionMenuItems)] =
{
  NedCmdRedrawScreen, NedCmdMacroRecord, NedCmdMacroPlay, NedCmdSettingsMapKeys,
  NedCmdSettingsTheme, NedCmdSettingsEdit, NedCmdSettingsTab,
  NedCmdSettingsCrLf, NedCmdSettingsWrap, NedCmdProjectFolders, NedCmdSettingsFactory
};

// Set at run time. See NedMenuMapSetWindowItems()
static sNedMenuItem_t maNedWindowMenuItems[] =
{
  { "untitled",             NEDKEY_NONE }
};

static const pfnNedCmd_t maNedWindowMenuCommands[NumElements(maNedWindowMenuItems)] =
{
  NedCmdDoNothing
};

static sNedMenuItem_t maNedHelpMenuItems[] =
{
  { "_About Ned",           NEDKEY_NONE  },
  { "Ned _Manual",          NEDKEY_FN1   },
  { "_View Key Bindings",   NEDKEY_NONE  },
  { "Date and _Time",       NEDKEY_ALT_T }
};

static const pfnNedCmd_t maNedHelpMenuCommands[NumElements(maNedHelpMenuItems)] =
{
  NedCmdHelpAbout, NedCmdHelp, NedCmdViewKeyBindings, NedCmdHelpDateTime
};

// if changing any menus or menu items, make sure to adjust mpaNedMenuCommands[]
// exception is the Window menu, which is built-in the fly from the file list
static sNedMenu_t maNedMenus[] =
{
  { "_File",   NumElements(maNedFileMenuItems),     maNedFileMenuItems },
  { "E_dit",   NumElements(maNedEditMenuItems),     maNedEditMenuItems    },
  { "_Search", NumElements(maNedSearchMenuItems),   maNedSearchMenuItems  },
  { "_Goto",   NumElements(maNedGotoMenuItems),     maNedGotoMenuItems    },
  { "_Option", NumElements(maNedOptionMenuItems),   maNedOptionMenuItems  },
  { "_Window", NumElements(maNedWindowMenuItems),   maNedWindowMenuItems  },  // see NedMenuMapSetWindowItems()
  { "_Help",   NumElements(maNedHelpMenuItems),     maNedHelpMenuItems    }
};

static sNedMenuBar_t msMenuBar =
{
  NumElements(maNedMenus),
  maNedMenus
};

// map menu bar items to commands. MUST agree with enum sNedMenuIndex_t
static sNedMenuCmdMap_t maNedMenuCmdMap[NumElements(maNedMenus)] =
{
  { NumElements(maNedFileMenuCommands),   maNedFileMenuCommands },
  { NumElements(maNedEditMenuCommands),   maNedEditMenuCommands },
  { NumElements(maNedSearchMenuCommands), maNedSearchMenuCommands },
  { NumElements(maNedGotoMenuCommands),   maNedGotoMenuCommands },
  { NumElements(maNedOptionMenuCommands), maNedOptionMenuCommands },
  { NumElements(maNedWindowMenuCommands), maNedWindowMenuCommands },
  { NumElements(maNedHelpMenuCommands),   maNedHelpMenuCommands }
};


/*!------------------------------------------------------------------------------------------------
  Set keymap and menu items to factory defaults. Done on init before loading any overriding
  settings. Can also happen via menu `Option/Factory Defaults...`.

  @ingroup  ned_keymap

  @return   Pointer to command or NedCmdDoNothing() if not found.
*///-----------------------------------------------------------------------------------------------
void NedKeyMapFactory(void)
{
  // make sure two menu arrays match
  NedAssertDbg(NumElements(maNedFileMenuCommands)   == NumElements(maNedFileMenuItems));
  NedAssertDbg(NumElements(maNedEditMenuCommands)   == NumElements(maNedEditMenuItems));
  NedAssertDbg(NumElements(maNedSearchMenuCommands) == NumElements(maNedSearchMenuItems));
  NedAssertDbg(NumElements(maNedGotoMenuCommands)   == NumElements(maNedGotoMenuItems));
  NedAssertDbg(NumElements(maNedOptionMenuCommands) == NumElements(maNedOptionMenuItems));
  NedAssertDbg(NumElements(maNedHelpMenuCommands)   == NumElements(maNedHelpMenuItems));

  memset(maNedKeyMap, 0, sizeof(maNedKeyMap));
  NkmSetCommands(NumElements(maNedKeyMapDefault), maNedKeyMapDefault);

}

/*!------------------------------------------------------------------------------------------------
  Translate common cursor keys so that things like menus and MsgLineEdit() don't need to know
  about commands. For example, if Ctrl-A was remapped from NedCmdSelectAll, to NedCmdCursorHome,
  this would translate NEDKEY_CTRL_A to NEDKEY_HOME. Only translates basic Cursor and editing keys.

  @ingroup  ned_keymap

  @return   key to use
*///-----------------------------------------------------------------------------------------------
nedKey_t NedKeyMapTranslateKey(nedKey_t key)
{
  nedKey_t      newKey = key;
  pfnNedCmd_t   pfnCmd;

  pfnCmd = NedKeyMapGetCommand(key);
  if(pfnCmd == NedCmdCursorUp)
    newKey = NEDKEY_UP;
  else if(pfnCmd == NedCmdCursorDown)
    newKey = NEDKEY_DOWN;
  else if(pfnCmd == NedCmdCursorLeft)
    newKey = NEDKEY_LEFT;
  else if(pfnCmd == NedCmdCursorRight)
    newKey = NEDKEY_RIGHT;
  else if(pfnCmd == NedCmdCursorHome)
    newKey = NEDKEY_HOME;
  else if(pfnCmd == NedCmdCursorEnd)
    newKey = NEDKEY_END;
  else if(pfnCmd == NedCmdCursorPgUp)
    newKey = NEDKEY_PGUP;
  else if(pfnCmd == NedCmdCursorPgDn)
    newKey = NEDKEY_PGDN;
  else if(pfnCmd == NedCmdCursorTop)
    newKey = NEDKEY_CTRL_PGUP;
  else if(pfnCmd == NedCmdCursorBottom)
    newKey = NEDKEY_CTRL_PGDN;
  else if(pfnCmd == NedCmdCursorWordLeft)
    newKey = NEDKEY_CTRL_LEFT;
  else if(pfnCmd == NedCmdCursorWordRight)
    newKey = NEDKEY_CTRL_RIGHT;
  else if(pfnCmd == NedCmdEditBackspace)
    newKey = NEDKEY_BACKSPACE;
  else if(pfnCmd == NedCmdEditDelete)
    newKey = NEDKEY_DELETE;

  return newKey;
}

/*!------------------------------------------------------------------------------------------------
  Based on a key, get a pointer to the mapped command for this key. Note, printable ASCII keys are
  not remappable.

  @ingroup  ned_keymap

  @return   Pointer to command or NULL if no command mapped to that key
*///-----------------------------------------------------------------------------------------------
pfnNedCmd_t NedKeyMapGetCommand(nedKey_t key)
{
  pfnNedCmd_t   pfnCmd;
  if( (key >= ' ') && (key <= '~') )
    pfnCmd = NedCmdEditInsertSelf;
  else
    pfnCmd = maNedKeyMap[key];
  return pfnCmd;
}

/*!------------------------------------------------------------------------------------------------
  Attach this command to this key. Will also update menus.

  @ingroup  ned_keymap

  @return   TRUE if successful
*///-----------------------------------------------------------------------------------------------
bool_t NedKeyMapSetCommand(const char *szKeyName, const char *szCmdName)
{
  nedKey_t    key;
  pfnNedCmd_t pfnCmd;
  bool_t      fWorked = FALSE;

  NedLogPrintfEx(NEDLOG_KEY, "NedKeyMapSetCommand(%s, %s)\n", szKeyName, szCmdName);

  key = NedKeyFromName(szKeyName);
  pfnCmd = NedCmdGetCommand(szCmdName);
  if((key != NEDKEY_NONE) && (pfnCmd != NULL))
  {
    maNedKeyMap[key] = pfnCmd;
    NedMenuMapAdjustMenuKeys();
    fWorked = TRUE;
  }
  return fWorked;
}

/*-------------------------------------------------------------------------------------------------
  Return TRUE if this key is mapped to the default commmand (which may be NULL aka no command)
-------------------------------------------------------------------------------------------------*/
static bool_t NkmIsDefault(nedKey_t key)
{
  unsigned    i;
  bool_t      fIsDefault = TRUE;

  for(i=0; i<NumElements(maNedKeyMapDefault); ++i)
  {
    if(maNedKeyMapDefault[i].key == key)
    {
      if(maNedKeyMap[key] != maNedKeyMapDefault[i].pfnCmd)
        fIsDefault = FALSE;
      break;
    }
  }

  // if not in maNedKeyMapDefault[], should be NULL by default
  if((i == NumElements(maNedKeyMapDefault)) && (maNedKeyMap[key] != NULL))
    fIsDefault = FALSE;

  if(!fIsDefault)
    NedLogPrintfEx(NEDLOG_KEY, "NkmIsDefault() key %u is not default CMD\n", key);

  return fIsDefault;
}

/*!------------------------------------------------------------------------------------------------
  Get command/key pairs that are mapped different than factory. Use index 0-n.

  @ingroup  ned_keymap

  @param    idx     index 0-n
  @param    pKey    pointer to returned key if non-NULL return of ::pfnNedCmt_t

  @return   Pointer to Nth mapped command, or NULL if no more mapped commands (index too high).
*///-----------------------------------------------------------------------------------------------
pfnNedCmd_t NedKeyMapGetMapped(unsigned idx, nedKey_t *pKey)
{
  nedKey_t    key;

  NedLogPrintfEx(NEDLOG_KEY, "NedKeyMapGetMapped(%u)\n", idx);

  for(key=0; key<NEDKEY_NONE; ++key)
  {
    if(!NkmIsDefault(key))
    {
      NedLogPrintfEx(NEDLOG_KEY, "  Not default key %u, idx %u\n", key, idx);
      if(idx == 0)
      {
        *pKey = key;
        return maNedKeyMap[key];
      }
      --idx;
    }
  }

  NedLogPrintfEx(NEDLOG_KEY, "  No more mapped keys\n");
  return NULL;
}

/*!------------------------------------------------------------------------------------------------
  Returns the 1st (numerically) key mapped to this command

  @param    pfnCmd
  @return   Return the key, or NEDKEY_NONE if not mapped
*///-----------------------------------------------------------------------------------------------
nedKey_t NedKeyMapGetKeyFromCmd(pfnNedCmd_t pfnCmd)
{
  unsigned    i;

  for(i=0; i<NumElements(maNedKeyMap); ++i)
  {
    if(maNedKeyMap[i] == pfnCmd)
      return (nedKey_t)i;
  }

  return NEDKEY_NONE;
}

/*!------------------------------------------------------------------------------------------------
  Intialize the menu subsystem for ned

  @ingroup  ned_keymap

  @return   Pointer to a menu handle
*///-----------------------------------------------------------------------------------------------
void * NedMenuMapInit(nedThemeIndex_t themeIndex)
{
  return NedMenuNew(&msMenuBar, themeIndex);
}

/*!------------------------------------------------------------------------------------------------
  Get the command from a menu and item within that menu. Helper to menuing system, as the generic
  menuing system knows nothing about ned commands.

  @ingroup  ned_keymap
  @param    menu    0-n, menu bar index (e.g. File or Edit)
  @param    item    0-n, item within menu index (e.g. New File or Open File)

  @return   Pointer to command or NedCmdDoNothing() if bad parameters.
*///-----------------------------------------------------------------------------------------------
pfnNedCmd_t NedMenuMapGetCommand(unsigned menu, unsigned item)
{
  pfnNedCmd_t   pfnCmd = NedCmdDoNothing;

  NedAssertDbg(NumElements(maNedMenus) == NumElements(maNedMenuCmdMap));
  NedAssertDbg(menu < NumElements(maNedMenuCmdMap));

  if(menu != NEDMENUINDEX_WINDOW && (menu < NumElements(maNedMenuCmdMap)))
  {
    NedAssertDbg(item < maNedMenuCmdMap[menu].nItems);
    if( item < maNedMenuCmdMap[menu].nItems )
      pfnCmd = maNedMenuCmdMap[menu].aCmds[item];
  }
  return pfnCmd;
}

/*!------------------------------------------------------------------------------------------------
  Specialy function to put filenames in the Window menu. Sets up the menu model. Note that the
  filenames themselves (but not the array of pointers), must stay in memory function is called
  again. This function just points the menu to those names.

  Does not redraw the menu.

  @ingroup  ned_keymap
  @param    nItems        number of items for this menu
  @param    ppFilenames   an array of pointers to preallocated filenames

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedMenuMapSetWindowItems(unsigned nItems, const char **ppFilenames)
{
  sNedMenuItem_t  *aWindowMenuItems;
  unsigned         i;

  // free the old memory if it was allocated
  if(maNedMenus[NEDMENUINDEX_WINDOW].aItems != maNedWindowMenuItems)
  {
    free((void *)maNedMenus[NEDMENUINDEX_WINDOW].aItems);
  }

  // get new memory
  aWindowMenuItems = malloc(sizeof(sNedMenuItem_t) * nItems);
  NedAssert(aWindowMenuItems);
  memset(aWindowMenuItems, 0, sizeof(sNedMenuItem_t) * nItems);

  for(i=0; i<nItems; ++i)
  {
    aWindowMenuItems[i].szName = ppFilenames[i];
    aWindowMenuItems[i].hotkey = NEDKEY_NONE;
  }
  maNedMenus[NEDMENUINDEX_WINDOW].numItems  = nItems;
  maNedMenus[NEDMENUINDEX_WINDOW].aItems    = aWindowMenuItems;
  maNedMenus[NEDMENUINDEX_WINDOW].fNoHotKey = TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Set up submenu handlers. This is for dealing with pick lists like File/Recently Opened->

  @ingroup  ned_keymap
  @param    nItems        number of items for this menu
  @param    aSubMenus     an array of which submenus to attach the functions to

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedMenuMapSetSubMenus(void *hMenu, unsigned nItems, sNedKeyMapSubMenu_t *aSubMenus)
{
  sNedMenuItem_t *pMenuItem;
  unsigned        i;

  for(i=0; i<nItems; ++i)
  {
    pMenuItem = (sNedMenuItem_t *)NedMenuItemPtr(hMenu, aSubMenus[i].nMenu, aSubMenus[i].nItem);
    if(pMenuItem)
    {
      NedLogPrintfEx(NEDLOG_KEY, "NedMenuMapSetSubMenus(%u, %u)\n", aSubMenus[i].nMenu, aSubMenus[i].nItem);
      pMenuItem->pfnSubMenu = aSubMenus[i].pfnSubMenu;
    }
  }
}

/*-------------------------------------------------------------------------------------------------
  Goes through menus and updates hotkeys. Used after setting up new hotkeys.
-------------------------------------------------------------------------------------------------*/
void NedMenuMapAdjustMenuKeys(void)
{
  unsigned      menu;
  unsigned      item;
  pfnNedCmd_t   pfnCmd;
  nedKey_t      key;
  sNedMenuItem_t   *pMenuItem;

  for(menu=0; menu<NumElements(maNedMenuCmdMap); ++menu)
  {
    for(item = 0; item<maNedMenuCmdMap[menu].nItems; ++item)
    {
      // Window menu doesn't have hotkeys
      if(menu == NEDMENUINDEX_WINDOW)
        pfnCmd = NULL;

      // might have a hotkey for the menu item
      else
        pfnCmd = NedMenuMapGetCommand(menu, item);

      // see if the menu has a hotkey
      if( pfnCmd && (pfnCmd != NedCmdDoNothing) )
      {
        key =  NkmFindHotkey(pfnCmd);
        pMenuItem = (sNedMenuItem_t *)(&maNedMenus[menu].aItems[item]);
        pMenuItem->hotkey = key;
      }
    }
  }
}

/*-------------------------------------------------------------------------------------------------
  Find the first key that is bound to this command, or NEDKEY_NONE if no match.
-------------------------------------------------------------------------------------------------*/
static nedKey_t NkmFindHotkey(pfnNedCmd_t pfnCmd)
{
  nedKey_t  key = NEDKEY_NONE;
  unsigned  i;

  if(pfnCmd)
  {
    for(i=0; i<NumElements(maNedKeyMap); ++i)
    {
      if(maNedKeyMap[i] == pfnCmd)
      {
        key = (nedKey_t)i;
        break;
      }
    }
  }
  return key;
}

/*-------------------------------------------------------------------------------------------------
  Set commands from a packed table to a sparse table
-------------------------------------------------------------------------------------------------*/
static void NkmSetCommands(unsigned nItems, const sNedKeyMap_t *pKeyMap)
{
  unsigned  i;

  for(i = 0; i < nItems; ++i)
  {
    maNedKeyMap[pKeyMap[i].key] = pKeyMap[i].pfnCmd;
  }
}
