/*!************************************************************************************************
  @file NedCmdHelp.c

  @brief  Ned Command Implementation for Help functions.

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


*///***********************************************************************************************
#include "NedEditor.h"
#include "NedSettings.h"
#include "NedSha.h"

/*!------------------------------------------------------------------------------------------------
  Open the NedManual.md document as a file. The file is hard-coded into NED, not loaded from the
  file system.

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdHelp(void)
{
  sNedFile_t   *pFile;

  pFile = NedEditorFileNew(szNedManualPath, 0);
  if(pFile)
  {
    NedEditorSetFileCur(pFile);
    NedEditorGotoPoint(NedEditorCursor());
    NedEditorBuildAroundCursor();
  }
}

/*!------------------------------------------------------------------------------------------------
  Display info about the Ned Editor

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdHelpAbout(void)
{
  NedMsgPrintf(szNedCmd_HelpAbout, szNedMachineName, NED_OS, __STDC_VERSION__, gszNedSha);
}

/*!------------------------------------------------------------------------------------------------
  Show the current Date/Time. Also fills the clipboard with the date/time for pasting.

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdHelpDateTime(void)
{
  const char   *szDateTime  = NedStrDateTime(time(NULL));
  sNedFile_t   *pClipboard  = NedEditorClipboard(TRUE);

  NedPointInsBlock(&pClipboard->sCursor, (void *)szDateTime, strlen(szDateTime));
  NedMsgPrintf("Current time: %s", szDateTime);
}
