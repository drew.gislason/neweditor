/*!************************************************************************************************
  @file NedSignal.c

  @brief  Ned Command Implementation

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  All commands are prototyped in NedCmd.h.

  To keep it organized (and # of lines per module down), the various command groups have their own
  source files. For example, bookmark type things can be found in NedCmdBookmark.c.

  The few odd-ball commands are including in this file.

*///***********************************************************************************************
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <assert.h>
#include <err.h>
#include <execinfo.h>
#include "NedSignal.h"

static pfnNedSigOnExit_t    m_pfnNedSigOnExit;
static const char          *m_szProgName = NULL;
#if 0
static void                *m_stackTraces[NEDSIG_MAX_STACK_FRAMES];

/*-------------------------------------------------------------------------------------------------
  Resolve symbol name and source location given the path to the executable and an address

  @return 0 if worked
-------------------------------------------------------------------------------------------------*/
int NsAddr2Line(char const * const szProgName, void const * const addr)
{
  char szAdd2LineCmd[512] = {0};
 
  /* have addr2line map the address to the relent line in the code */
  #ifdef __APPLE__
    /* apple does things differently... */
    snprintf(szAdd2LineCmd, sizeof(szAdd2LineCmd) - 1, "atos -o %.256s %p", szProgName, addr); 
  #else
    snprintf(szAdd2LineCmd, sizeof(szAdd2LineCmd) - 1, "addr2line -f -p -e %.256s %p", szProgName, addr); 
  #endif
  szAdd2LineCmd[sizeof(szAdd2LineCmd) - 1] = '\0';
 
  /* This will print a nicely formatted string specifying the function and source line of the address */
  return system(szAdd2LineCmd);
}
#endif

/*!------------------------------------------------------------------------------------------------
  Display stack trace to stdout. Resolves symbol name and source location given the path to the
  executable and an address. Only works if NedSigSetExit() has been called 1st.

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedSigStackTrace(void)
{
  void* callstack[128];
  int i, frames = backtrace(callstack, 128);
  char** strs = backtrace_symbols(callstack, frames);
  for (i = 0; i < frames; ++i) {
    printf("%s\n", strs[i]);
  }
  free(strs);
}

/*-------------------------------------------------------------------------------------------------
  Generic signal handler
-------------------------------------------------------------------------------------------------*/
void NsSignalHandler(int sig)
{
  int exitCode = 1;

  fputs("\033[0m \n", stdout);
  switch(sig)
  {
    case SIGABRT:
      fputs("Caught SIGABRT: usually caused by an abort() or assert()\n", stdout);
    break;
    case SIGFPE:
      fputs("Caught SIGFPE: arithmetic exception, such as divide by zero\n", stdout);
    break;
    case SIGILL:
      fputs("Caught SIGILL: illegal instruction\n", stdout);
    break;
    case SIGINT:
      fputs("Caught SIGINT: interactive attention signal, probably a ctrl+c\n", stdout);
    break;
    case SIGSEGV:
      fputs("Caught SIGSEGV: segfault\n", stdout);
    break;
    case SIGTERM:
    default:
      fputs("Caught SIGTERM: a termination request was sent to the program\n", stdout);
    break;
  }

  NedSigStackTrace();

  if(m_pfnNedSigOnExit)
    exitCode = (*m_pfnNedSigOnExit)(sig);

  _Exit(exitCode);
}
 
/*!------------------------------------------------------------------------------------------------
  Set signal handlers we care about

  @return   none
-------------------------------------------------------------------------------------------------*/
void NsSetSignalHandlers(void)
{
  signal(SIGABRT, NsSignalHandler);
  signal(SIGFPE,  NsSignalHandler);
  signal(SIGILL,  NsSignalHandler);
  signal(SIGINT,  NsSignalHandler);
  signal(SIGSEGV, NsSignalHandler);
  signal(SIGTERM, NsSignalHandler);
}

/*!------------------------------------------------------------------------------------------------
  This causes signals to be cause and display a stack track on signal. Call with program name and
  an optional exit function

  @ingroup  ned_utils

  @param    szProgName        argv[0]
  @param    pfnNedSigOnExit   function to call on exit, or NULL   

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedSigSetExit(const char *szProgName, pfnNedSigOnExit_t pfnNedSigOnExit)
{
  m_pfnNedSigOnExit = pfnNedSigOnExit;
  m_szProgName      = szProgName;
  NsSetSignalHandlers();
}

void NedSigSegFault(void *p)
{
  *(unsigned *)p = 0;
}
