/*!************************************************************************************************
  @file NedUndo.c

  @brief  Private functions

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_undo Ned Undo - An API for Undo and Redo. Operates on files.

  This API uses a roll-back method, where each insert or delete is applied at the correct position
  in a file. A redo is a roll-back in the other direction. A single undo is defined by a series of
  edits (inserts and deletes), followed by a non-edit command.

*///***********************************************************************************************
#ifndef NED_UNDO_H
#define NED_UNDO_H

#include "Ned.h"
#include "NedLog.h"
#include "NedPoint.h"
#include "NedFile.h"

typedef void * hUndoList_t;
typedef sNedFile_t * (*pfnGetFileByPoint_t)(const sNedPoint_t *pPoint);

hUndoList_t   NedUndoListNew        (pfnGetFileByPoint_t pfnGetFileByPoint);
void          NedUndoListFree       (hUndoList_t hUndoList);
void          NedUndoListClear      (hUndoList_t hUndoList);
bool_t        NedUndoIsUndoList     (hUndoList_t hUndoList);
void          NedUndoLogShow        (hUndoList_t hUndoList, bool_t fDetails);
long          NedUndoCount          (hUndoList_t hUndoList);
void          NedUndoFileRemove     (hUndoList_t hUndoList, const sNedFile_t *pFile);
void          NedUndoEditDone       (hUndoList_t hUndoList);
sNedFile_t   *NedUndoRedo           (hUndoList_t hUndoList);
sNedFile_t   *NedUndoUndo           (hUndoList_t hUndoList);
sNedFile_t   *NedUndoRedoLen        (hUndoList_t hUndoList, long *pPos, long *pLen);
sNedFile_t   *NedUndoUndoLen        (hUndoList_t hUndoList, long *pPos, long *pLen);

// mimics point functions of the same name, but with undo capability
bool_t        NedUndoInsChar        (hUndoList_t hUndoList, sNedPoint_t *pPoint, nedChar_t c);
bool_t        NedUndoInsBlock       (hUndoList_t hUndoList, sNedPoint_t *pPoint, const uint8_t *pData, long insertLen);
bool_t        NedUndoInsStr         (hUndoList_t hUndoList, sNedPoint_t *pPoint, const char *sz);
bool_t        NedUndoDelBackspace   (hUndoList_t hUndoList, sNedPoint_t *pPoint);
bool_t        NedUndoDelChar        (hUndoList_t hUndoList, sNedPoint_t *pPoint);
bool_t        NedUndoDelBlock       (hUndoList_t hUndoList, sNedPoint_t *pThis, sNedPoint_t *pThat);
long          NedUndoWrapText       (long indent, const sNedPoint_t *pThis, sNedPoint_t *pThat, long width);

// useful if "manually" adding/deleting text
void UndoIns(hUndoList_t hUndoList, sNedPoint_t *pPoint, long len, const uint8_t *pData);
void UndoDel(hUndoList_t hUndoList, sNedPoint_t *pPoint, long len);

#endif // NED_UNDO_H
