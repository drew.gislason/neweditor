/*!************************************************************************************************
  @file NedSettings.h

  @brief  Ned Settings API, including everything about project files.

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_settings    Ned Settings - APIs for loading and using settings.

  Helper to the Editor, the settings module understands all Ned objects that can be initialized or
  saved via settings.

*///***********************************************************************************************
#ifndef NED_SETTINGS_H
#define NED_SETTINGS_H

#include "Ned.h"
#include "NedEditor.h"

typedef enum
{
  NEDSET_ERR_NONE = 0,    // worked
  NEDSET_ERR_BADPARM,     // bad sNedSettings_t or sJ2cObj_t structure
  NEDSET_ERR_FAILED,      // couldn't load/save settings file or other error
  NEDSET_ERR_MEM          // couldn't allocate memory
} nedSetErr_t;

extern const char szNedManualPath[];

// API
void            NedSettingsInitEditor       (sNedEditor_t *pEditor);
void            NedSettingsFreeEditor       (sNedEditor_t *pEditor);
nedSetErr_t     NedSettingsSave             (const char *szProjName, const sNedEditor_t *pEditor);
nedSetErr_t     NedSettingsLoad             (sNedEditor_t *pEditor, const char *szProjName, bool_t fLoadFiles);
void            NedSettingsLoadIfNedManual  (sNedFile_t *pFile);
void            NedSettingsSetFileCrLf      (sNedFile_t *pFile);

// list helper functions
bool_t          NedSettingsStrIsSame        (const void *sz1, const void *sz2);
void            NedSettingsStrShow          (const void *ptr);

#endif // NED_SETTINGS_H
