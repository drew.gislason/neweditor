/*!************************************************************************************************
  @file NedList.c

  @brief  Ned List API - simple API for a lists of dynamic strings.

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include "Ned.h"
#include "NedList.h"
#include "NedLog.h"


#define NEDLIST_SANCHK    1157U   // sanity check
#define NL_NOT_FOUND      UINT_MAX

typedef struct
{
  unsigned          id;
  bool_t            fFirstIter;   // flag for iterating via NedListNext()/Prev()
  unsigned          iterHead;     // for iterating through list
  unsigned          numItems;     // 0-n
  unsigned          maxItems;     // 1-n, circular list, old will be pushed out
  unsigned          size;         // 1-n
  pfnListIsSame_t   pfnIsSame;
  pfnListShow_t     pfnShow;
  void             *pItems;       // pointer to array of objects
} sNedList_t;


/*-------------------------------------------------------------------------------------------------
  Get previous index. Keeps index in range 0 - (numItems-1).

  Returns ptr to item or NULL if invalid input.
-------------------------------------------------------------------------------------------------*/
static unsigned NlPrevIndex(const sNedList_t *pList, unsigned index)
{
  if(index == 0 || index >= pList->numItems)
  {
    if(pList->numItems == 0)
      index = 0;
    else
      index = pList->numItems - 1;
  }
  else
      --index;
  return index;
}

/*-------------------------------------------------------------------------------------------------
  Internal function get a pointer to the item by index. If not a valid list or index >= numItems
  in list, returns NULL

  index = 0-n

  Returns ptr to item or NULL if invalid input.
-------------------------------------------------------------------------------------------------*/
static void * NlGetItem(const sNedList_t *pList, unsigned index)
{
  void  *pItem = NULL;

  if(NedListIsList(pList) && (index < pList->maxItems))
    pItem = (uint8_t *)pList->pItems + (index * pList->size);

  return pItem;
}

/*-------------------------------------------------------------------------------------------------
  Internal function shows an item

  index = 0-n

  Returns ptr to item or NULL if invalid input.
-------------------------------------------------------------------------------------------------*/
static void NlShowItem(const sNedList_t *pList, const void *pItem)
{
  if(pList != NULL && pList->pfnShow != NULL && pItem != NULL)
    pList->pfnShow(pItem);
  else
    NedLogPrintf("%p", pItem);
}

/*-------------------------------------------------------------------------------------------------
  Internal function to find this entry by its contents.

  Uses the comparison function pfnIsSame_t passed in on NedListNew().

  Returns index if found, or UINT_MAX if not.
-------------------------------------------------------------------------------------------------*/
static unsigned NlFindItem(const sNedList_t *pList, const void *ptr)
{
  unsigned            i;
  unsigned            index   = UINT_MAX;
  bool_t              fIsSame = FALSE;

  // if not valid list or empty list, not found
  if(NedListIsList(pList) && (pList->numItems > 0))
  {
    for(i=0; i<pList->numItems; ++i)
    {
      if(pList->pfnIsSame)
        fIsSame = pList->pfnIsSame(ptr, NlGetItem(pList, i));
      else
        fIsSame = ((memcmp(ptr, NlGetItem(pList, i), pList->size) == 0) ? TRUE : FALSE);
      if(fIsSame)
      {
        index = i;
        break;
      }
    }
  }

  return index;
}

/*-------------------------------------------------------------------------------------------------
  Internal function to find and remove this entry by its contents

  Example, removing 'b':
  [a,b,c,d, ]
  [a,c,d, ]


  Returns FALSE if not found
-------------------------------------------------------------------------------------------------*/
static bool_t NlRemoveItem(hNedList_t hList, const void *pItem)
{
  sNedList_t   *pList = (sNedList_t *)hList;
  uint8_t      *pThisItem;
  unsigned      index;
  unsigned      count;

  // validate parameters
  if(!NedListIsList(hList) || (pItem == NULL))
    return FALSE;

  // if not found, nothing to do, just return FALSE
  index = NlFindItem(hList, pItem);
  if(index == NL_NOT_FOUND)
    return FALSE;

  // shift down other items
  pThisItem = NlGetItem(pList, index);
  count     = (pList->numItems - index) - 1;
  if(count)
    memmove(pThisItem, pThisItem + pList->size, count * pList->size);

  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Create a new circular list of objects. Returns handle to the list.

  Used in Ned for point history, bookmarks, recent files, recent projects, etc...

  @ingroup    ned_list
  @param      maxItems    maximum items in list
  @param      size        size of each item
  @param      pfnIsSame   function to compare two items for sameness, NULL to use memcmp()
  @param      pfnShow     function to show an item in log, can be NULL
  @return     handle for list, or NULL if no memory
*///-----------------------------------------------------------------------------------------------
hNedList_t NedListNew(unsigned maxItems, unsigned size, pfnListIsSame_t pfnIsSame, pfnListShow_t pfnShow)
{
  sNedList_t  *pList;
  void        *pItems;

  NedLogPrintfEx(NEDLOG_LIST, "NedListNew(maxItems %u, size %u, isSame %p, show %p\n", maxItems, size, pfnIsSame, pfnShow);

  // validate parameters
  NedAssertDbg(maxItems);
  NedAssertDbg(size);

  // allocate list
  pItems = malloc(maxItems * size);
  if(!pItems)
    return NULL;
  pList = malloc(sizeof(sNedList_t));
  if(!pList)
  {
    free(pItems);
    return NULL;
  }

  pList->id         = NEDLIST_SANCHK;
  pList->maxItems   = maxItems;
  pList->size       = size;
  pList->pfnIsSame  = pfnIsSame;
  pList->pfnShow    = pfnShow;
  pList->pItems     = pItems;
  NedListClear(pList);

  return (hNedList_t)pList;
}

/*!------------------------------------------------------------------------------------------------
  Return TRUE if this is a list object

  @ingroup    ned_list
  @param      hList   handle for valid list
  @return     TRUE if this is a list object
*///-----------------------------------------------------------------------------------------------
bool_t NedListIsList(hNedList_t hList)
{
  const sNedList_t *pList = hList;
  return (pList && (pList->id == NEDLIST_SANCHK)) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Show the list summary or with details on each item. Logs individual items from least recent to
  most recent.

  @ingroup    ned_list
  @param      hList       handle for valid list
  @param      fDetailed   show items as well as summary
  @return     none
*///-----------------------------------------------------------------------------------------------
void NedListLogShow(hNedList_t hList, bool_t fDetailed)
{
  const sNedList_t   *pList = hList;
  unsigned            i;

  if(!NedListIsList(hList))
    return;

  // display list structure
  NedLogPrintf("NedListLogShow(hList=%p, numItems=%u, maxitems=%u, size=%u, iterHead %u, firstIter %u)\n",
    hList, pList->numItems, pList->maxItems, pList->size, pList->iterHead, pList->fFirstIter);

  for(i=0; i<pList->numItems; ++i)
  {
    NedLogPrintf("  %2u: ", i);
    NlShowItem(pList, NlGetItem(pList, i));
    NedLogPrintf("\n");
  }
}

/*!------------------------------------------------------------------------------------------------
  Clear the list

  @ingroup    ned_list
  @param      hList   handle for valid list
  @return     none
*///-----------------------------------------------------------------------------------------------
void NedListClear(hNedList_t hList)
{
  sNedList_t *pList = (sNedList_t *)hList;

  if(!NedListIsList(hList))
    return;

  pList->numItems   = 0;
  NedListIterStart(hList);
  memset(pList->pItems, 0, pList->size * pList->maxItems);
}

/*!------------------------------------------------------------------------------------------------
  Free the list and all its dynamic strings.

  @ingroup    ned_list
  @param      hList   handle for valid list
  @return     none
*///-----------------------------------------------------------------------------------------------
void NedListFree(hNedList_t hList)
{
  sNedList_t *pList = (sNedList_t *)hList;

  if(NedListIsList(hList))
  {
    if(pList->pItems)
      free(pList->pItems);
    free(pList);
  }
}

/*!------------------------------------------------------------------------------------------------
  Return maximum items in this list.

  @ingroup    ned_list
  @param      hList   handle for valid list
  @return     maximum items in this list
*///-----------------------------------------------------------------------------------------------
unsigned NedListMaxItems(hNedList_t hList)
{
  const sNedList_t *pList = hList;
  unsigned          maxItems = 0;

  if(NedListIsList(hList))
    maxItems = pList->maxItems;
  return maxItems;
}

/*!------------------------------------------------------------------------------------------------
  Return number items currently in this list.

  @ingroup    ned_list
  @param      hList   handle for valid list
  @return     maximum items in this list
*///-----------------------------------------------------------------------------------------------
unsigned NedListNumItems(hNedList_t hList)
{
  const sNedList_t *pList = hList;
  unsigned          numItems = 0;

  if(NedListIsList(hList))
    numItems = pList->numItems;
  return numItems;
}

/*!------------------------------------------------------------------------------------------------
  Add this item into the circular list at the head. Note the head can be moved with the functions
  NedListNext/NedListPrev().

  Makes a copy, so higher layer can use locals, etc...

  @ingroup    ned_list
  @param      hList     handle for valid list
  @param      pItem     ptr to item to be copied into list
  @param      size      size to copy, or 0 for default set size by NedListNew()
  @return     TRUE if worked, FALSE if bad parameter
*///-----------------------------------------------------------------------------------------------
bool_t NedListAdd(hNedList_t hList, const void *pItem, unsigned size)
{
  sNedList_t   *pList = (sNedList_t *)hList;
  bool_t        fRemoved;

  // check for bad parameters
  if(!NedListIsList(hList) || (pItem == NULL))
    return FALSE;

  // remove the item if already in the list
  fRemoved = NlRemoveItem(hList, pItem);

  // If list is full, make room by removing oldest
  if(!fRemoved && pList->numItems >= pList->maxItems)
  {
    NlRemoveItem(hList, NlGetItem(pList, 0));
    fRemoved = TRUE;
  }

  // either way, we now have a place to put it
  if(fRemoved)
    --pList->numItems;

  // add item at newest (last) part of list
  if(size == NEDLIST_DEF_SIZE)
    size = pList->size;
  memcpy(NlGetItem(pList, pList->numItems), pItem, size);
  if(pList->numItems < pList->maxItems)
    ++pList->numItems;

  // adding restarts iteration
  NedListIterStart(hList);

  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Remove a item from the list. Keeps list packed and head in proper place.

  Does NOT adjust iterhead, so we can delete while iterating the list.

  @ingroup    ned_list
  @param      hList     handle for valid list
  @param      pItem     ptr to item to remove
  @return     TRUE if removed, FALSE if not in list
*///-----------------------------------------------------------------------------------------------
bool_t NedListRemove(hNedList_t hList, const void *pItem)
{
  sNedList_t   *pList     = (sNedList_t *)hList;
  bool_t        fRemoved  = FALSE;

  if(!NedListIsList(hList) || (pItem == NULL))
    return FALSE;

  fRemoved = NlRemoveItem(hList, pItem);
  if(fRemoved)
  {
    if(pList->numItems <= 1)
      NedListClear(hList);
    else
      --pList->numItems;
  }

  return fRemoved;
}

/*!------------------------------------------------------------------------------------------------
  Return a pointer to the item in the list, or NULL if not found.

  @ingroup    ned_list
  @param      hList     handle for valid list
  @param      pItem     Pointer to some data. 
  @return     NULL if not found, pointer to item in list if found
*///-----------------------------------------------------------------------------------------------
void * NedListFind(hNedList_t hList, const void *pItem)
{
  const sNedList_t   *pList = hList;
  unsigned            index;

  index = NlFindItem(pList, pItem);
  return (index != NL_NOT_FOUND) ? NlGetItem(pList, index) : NULL;
}

/*!------------------------------------------------------------------------------------------------
  Return previous list item in round robin fashion. Newest to oldest direction.

  @ingroup    ned_list
  @param      hList   handle for valid list
  @return     Pointer to previous item.
*///-----------------------------------------------------------------------------------------------
void *NedListPrev(hNedList_t hList)
{
  sNedList_t   *pList = (sNedList_t *)hList;
  void         *pItem = NULL;

  if(NedListIsList(hList) && pList->numItems)
  {
    pList->fFirstIter = FALSE;
    pList->iterHead   = NlPrevIndex(pList, pList->iterHead);
    pItem             = NlGetItem(pList, pList->iterHead);
  }

  return pItem;
}

/*!------------------------------------------------------------------------------------------------
  Return next list item in round robin fashion. Oldest to newest direction.

  @ingroup    ned_list
  @param      hList   handle for valid list
  @return     ptr to next data or NULL if no data in list
*///-----------------------------------------------------------------------------------------------
void *NedListNext(hNedList_t hList)
{
  sNedList_t   *pList = (sNedList_t *)hList;
  void         *pItem = NULL;

  // if not a list or not items, just return NULL
  if(NedListIsList(hList) && pList->numItems)
  {
    if(pList->fFirstIter)
      pList->fFirstIter = FALSE;
    else
      ++pList->iterHead;
    if(pList->iterHead >= pList->numItems)
      pList->iterHead = 0;
    pItem = NlGetItem(pList, pList->iterHead);
  }

  return pItem;
}

/*!------------------------------------------------------------------------------------------------
  Start at the head (pointing to oldest or empty slot). See NedListPrev() and NedListNext().

  @ingroup    ned_list
  @param      hList   handle for valid list
  @return     none
*///-----------------------------------------------------------------------------------------------
void NedListIterStart(hNedList_t hList)
{
  sNedList_t   *pList = (sNedList_t *)hList;

  if(NedListIsList(hList))
  {
    pList->iterHead   = pList->numItems;
    pList->fFirstIter = TRUE;
  }
}
