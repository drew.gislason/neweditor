/*!------------------------------------------------------------------------------------------------
  @file NedPointMisc.c

  @brief  Ned Point Miscellaneous contains a variety of higher-level point functions

  For example, modifiying wrapping text to a new indent and/or column, filling a buffer with a
  Canonical hex dump or matching a brace.

  @copyright Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @ingroup  ned_point
*///-----------------------------------------------------------------------------------------------
#include <ctype.h>
#include "NedLog.h"
#include "NedPoint.h"
#include "NedUtilStr.h"

// arrays must match indexes
static char gszNpOpenBrace[]   = "({<[";
static char gszNpCloseBrace[]  = ")}>]";

/*!------------------------------------------------------------------------------------------------
  Is this character a brace? One of ({<[]>}). Returns index into "({<[".

  @param    pPoint       pointer to preallocated point
  @return   >0 if open brace, <0 if ending brace, 0 if not brace.
*///-----------------------------------------------------------------------------------------------
int NedPointIsBrace(const sNedPoint_t *pPoint)
{
  nedChar_t c;
  char     *p;

  // not on a brace, at EOF
  if(pPoint->offset < NedBufLen(pPoint->pBuf))
  {
    c = NedPointCharGet(pPoint);
    p = memchr(gszNpOpenBrace, c, sizeof(gszNpOpenBrace)-1);
    if(p)
      return (int)(p - gszNpOpenBrace) + 1;
    p = memchr(gszNpCloseBrace, c, sizeof(gszNpCloseBrace)-1);
    if(p)
      return -1 - (int)(p - gszNpCloseBrace);
  }
  return 0;
}

/*!------------------------------------------------------------------------------------------------
  Find a matching brace. If not on a brace, go forward in file until brace or EOF.

        {  {   }  }
        ^         ^
      This       Finds This

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point

  @return   TRUE if matching brace was found, FALSE if not.
*///-----------------------------------------------------------------------------------------------
bool_t NedPointBraceMatch(sNedPoint_t *pPoint)
{
  int         isBrace   = 0;
  bool_t      fFound    = FALSE;
  unsigned    count     = 0;
  nedChar_t   c;
  nedChar_t   cBrace    = '-';
  nedChar_t   cMatch    = '-';
  sNedPoint_t sPoint    = *pPoint;
  unsigned    i;

  NedAssertDbg(NedPointIsPoint(pPoint));

  // check for char under cursor, then char BEFORE cursor, then char AFTER cursor
  for(i=0; i<3; ++i)
  {
    isBrace = NedPointIsBrace(&sPoint);

    // if open brace, match closing brace
    if(isBrace > 0)
    {
      NedAssertDbg(isBrace < sizeof(gszNpCloseBrace));
      cBrace = NedPointCharGet(&sPoint);
      cMatch = gszNpCloseBrace[isBrace-1];
      break;
    }

    // if closing brace, match opening brace
    else if(isBrace < 0)
    {
      NedAssertDbg((-1*isBrace) < sizeof(gszNpOpenBrace));
      cBrace = NedPointCharGet(&sPoint);
      cMatch = gszNpOpenBrace[(-1*isBrace)-1];
      break;
    }

    if(i == 0)                    // check char BEFORE
      NedPointCharPrev(&sPoint);
    else if(i == 1)               // check CHAR AFTER
    {
      NedPointCharNext(&sPoint);
      NedPointCharNext(&sPoint);
    }
  }

  NedLogPrintfEx(NEDLOG_POINT, "NedLogBraceMatch(pBuf=%p, offset=%u) isBrace %d, cBrace %c, cMatch %c, i %u\n",
               pPoint->pBuf, pPoint->offset, isBrace, cBrace, cMatch, i);

  while(1)
  {
    // if at EOF or we weren't on a brace and can't find a brace on this line... give up!
    c = NedPointCharGet(&sPoint);
    if(c == NEDCHAR_EOF || ((isBrace == 0) && (c == '\n')))
      break;

    // looking forward for brace, found it, stop
    if((isBrace == 0) && NedPointIsBrace(&sPoint))
    {
      fFound = TRUE;
      break;
    }
    if(c == cBrace)
      ++count;

    // count braces so "{ { } }" will find the 2nd closing brace
    if(c == cMatch)
    {
      if(count)
        --count;
      if(count == 0)
      {
        fFound = TRUE;
        break;
      }
    }

    // go forward if not on brace or on opening brace
    if(isBrace >= 0)
      NedPointCharNext(&sPoint);

    // else go backward. break if at file top
    else if(!NedPointCharPrev(&sPoint))
      break;
  }

  if(fFound)
    *pPoint = sPoint;

  return fFound;
}

/*!------------------------------------------------------------------------------------------------
  Is this line an error line? If so, return the error parameters. It's an error line if it has the
  following pattern:

  * begins with alnum, . or /
  * colon followed by line number
  * colon followed by col number
  * colon followed by error string

      /Users/drewg/Work/Git/ned/src/Ned.c:19:22: error: expected ';' after expression
      ../src/NedAnsi.c:48:1: error: expected identifier
      this is a test.c:16:54: error: expected ';' after expression
      makemaze.py:3:1: E302 expected 2 blank lines, found 1

  @ingroup  ned_point
  @param    pPoint      pointer to point (must be valid)
  @param    pErrInfo    returned error info if found

  @return   TRUE if error line, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedPointIsErrLine(const sNedPoint_t *pPoint, sNedPtErrInfo_t *pErrInfo)
{
  nedChar_t     c;
  sNedPoint_t   sLineEnd;
  sNedPoint_t   sLineBeg;
  sNedPoint_t   sPoint;
  sNedPoint_t   sPointPrev;
  long          diff;
  bool_t        fWorked = TRUE;
  char         *szStrs[4];
  unsigned      i;

  // NedLogPrintfEx(NEDLOG_POINT, "NedPointIsErrLine(pBuf %p, offset %u)\n", pPoint->pBuf, pPoint->offset);

  NedAssertDbg(NedPointIsPoint(pPoint));
  NedAssertDbg(pErrInfo);

  // first character must be alnum
  sLineBeg = *pPoint;
  NedPointLineBeg(&sLineBeg);
  c = NedPointCharGet(&sLineBeg);
  if(!isalnum((int)c) && (c != '.') && (c != '/'))
  {
    return FALSE;
  }

  // get an array of 4 pointers to filename, line, col, message
  sPoint   = sLineBeg;
  sLineEnd = sLineBeg;
  NedPointLineEnd(&sLineEnd);
  memset(szStrs, 0, sizeof(szStrs));
  // NedLogPrintfEx(NEDLOG_POINT, "  sPoint.pBuf %p, offset %u, sLineEnd.pBuf %p, offset %u\n",
  //              sPoint.pBuf, sPoint.offset, sLineEnd.pBuf, sLineEnd.offset);
  for(i=0; i<NumElements(szStrs); ++i)
  {
    sPointPrev = sPoint;

    // line, column must begin with number
    if(i >= 1 && i <= 2)
    {
      c = NedPointCharGet(&sPoint);
      if(!isdigit(c))
      {
        // NedLogPrintfEx(NEDLOG_POINT, "  failed not digit\n");
        fWorked = FALSE;
        break;
      }
    }

    // last one doesn't need colon (ends on line)
    if(i == (NumElements(szStrs)-1))
      sPoint = sLineEnd;
    else
    {
      diff = NedPointDifference(&sPoint, &sLineEnd);
      if(!NedPointMemChr(&sPoint, ':', diff))
      {
        // NedLogPrintfEx(NEDLOG_POINT, "  failed not colon\n");
        fWorked = FALSE;
        break;
      }
    }

    // allocate string
    if(i == (NumElements(szStrs)-1))
    {
      diff = NedPointDifference(&sLineBeg, &sLineEnd);
      sPointPrev = sLineBeg;
    }
    else
      diff = NedPointDifference(&sPointPrev, &sPoint);

    szStrs[i] = malloc(diff+1);
    if(szStrs[i])
    {
      NedPointStrGet((void *)(szStrs[i]), &sPointPrev, diff);
    }
    else
    {
      fWorked = FALSE;
      break;
    }

    // skip colon
    NedPointCharNext(&sPoint);
  }

  // if we couldn't retrieve all 4 strings, failed
  if(!fWorked)
  {
    for(i=0; i<NumElements(szStrs); ++i)
    {
      if(szStrs[i])
        free(szStrs[i]);
    }
  }
  else
  {
    pErrInfo->szFile = szStrs[0];
    pErrInfo->szMsg  = szStrs[3];
    pErrInfo->line   = atol(szStrs[1]); free(szStrs[1]);
    pErrInfo->col    = atol(szStrs[2]); free(szStrs[2]);

    // NedLogPrintfEx(NEDLOG_POINT, "  pErrInfo->szFile %s, line %ld, col %ld, msg %s\n", pErrInfo->szFile, 
    //             pErrInfo->line, pErrInfo->col, pErrInfo->szMsg);
  }

  return fWorked;
}

/*!------------------------------------------------------------------------------------------------
  Find the next error line (which might be the current line)

  @ingroup  ned_point
  @param    pPoint      pointer to point (must be valid)
  @param    pErrInfo    returned error info if found

  @return   TRUE if found another err line, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedPointErrLineNext(sNedPoint_t *pPoint, sNedPtErrInfo_t *pErrInfo)
{
  bool_t    fFound = FALSE;

  NedAssertDbg(NedPointIsPoint(pPoint));
  NedAssertDbg(pErrInfo);

  while(!NedPointIsAtBottom(pPoint))
  {
    NedPointLineBeg(pPoint);
    if(NedPointIsErrLine(pPoint, pErrInfo))
    {
      fFound = TRUE;
      break;
    }
    if(!NedPointLineNext(pPoint))
      NedPointFileBottom(pPoint);
  }
  return fFound;
}

/*!------------------------------------------------------------------------------------------------
  Find the previous error line (which might be the current line)

  @ingroup  ned_point
  @param    pPoint      pointer to point (must be valid)
  @param    pErrInfo    returned error info if found

  @return   TRUE if found another err line, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedPointErrLinePrev(sNedPoint_t *pPoint, sNedPtErrInfo_t *pErrInfo)
{
  bool_t    fFound = FALSE;

  NedAssertDbg(NedPointIsPoint(pPoint));
  NedAssertDbg(pErrInfo);

  while(1)
  {
    NedPointLineBeg(pPoint);
    if(NedPointIsErrLine(pPoint, pErrInfo))
    {
      fFound = TRUE;
      break;
    }
    if(NedPointIsAtTop(pPoint))
      break;
    NedPointLinePrev(pPoint);
  }
  return fFound;
}

/*!------------------------------------------------------------------------------------------------
  Free the error structure.

  @ingroup  ned_point
  @param    pPoint       pointer to point (must be valid)

  @return   nothing
*///-----------------------------------------------------------------------------------------------
void NedPointErrFree(sNedPtErrInfo_t *pErrInfo)
{
  if(pErrInfo)
  {
    if(pErrInfo->szFile)
      free(pErrInfo->szFile);
    if(pErrInfo->szMsg)
      free(pErrInfo->szMsg);
  }
}

/*!------------------------------------------------------------------------------------------------
  Is this line a function line in C/C++ or Rust?

  Does not move point. Assumes point is at start of line.

  Algorithm:

  1. assumes C function begins at start of line ('_' or alnum in col 1)
  2. assumes { opens a function, and is encountered before a ';'

  Will find all the following:

      class Car {
      sMyStruct_t * my_get_struct(int c) {
      unsigned Car::speed(unsigned maxSpeed) {
      fn cowbull_getline(prompt: &str) -> String {

  @ingroup  ned_point
  @param    pPoint       valid point at start of line

  @return   TRUE if is a function line, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedPointIsFnC(const sNedPoint_t *pPoint)
{
  sNedPoint_t   sPoint;
  nedChar_t     c;
  bool_t        isFnLine = FALSE;

  // if the line begins with "_" or alpha, we may have found a func
  c = NedPointCharGet(pPoint);
  if( (c == '_') || (isalpha(c)) )
  {
    sPoint = *((sNedPoint_t *)pPoint);

    // search for the next "{" or ";"
    while(NedPointCharNext(&sPoint))
    {
      c = NedPointCharGet(&sPoint);
      if(c == '{')
      {
        isFnLine = TRUE;
        break;
      }
      else if(c == ';')
        break;
    }
  }

  return isFnLine;
}

/*-------------------------------------------------------------------------------------------------
  Is this a Javascript function?

  Does not move point. Assumes point is at start of line.

  @ingroup  ned_cmd
  @returns  none
-------------------------------------------------------------------------------------------------*/
bool_t NedPointIsFnJavascript(const sNedPoint_t *pPoint)
{
  static const char   szFunction[] = "function";
  sNedPoint_t         sPt     = *pPoint;

  NedPointSkipWhite(&sPt);
  return (NedPointStrCmp(&sPt, szFunction) == 0) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Is this a python function?

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
bool_t NedPointIsFnPython(const sNedPoint_t *pPoint)
{
  static const char   szFunction[] = "def";
  sNedPoint_t         sPt     = *pPoint;

  NedPointSkipWhite(&sPt);
  return (NedPointStrCmp(&sPt, szFunction) == 0) ? TRUE : FALSE;
}

/*-------------------------------------------------------------------------------------------------
  Is this an HTML function?

  @ingroup  ned_cmd
  @returns  none
-------------------------------------------------------------------------------------------------*/
bool_t NedPointIsFnHtml(const sNedPoint_t *pPoint)
{
  static const char   szDiv[]   = "<div";
  static const char   szHead[]  = "<head";
  static const char   szBody[]  = "<body";
  sNedPoint_t         sPt       = *pPoint;
  bool_t              fIsHtmlFn = FALSE;

  NedPointSkipWhite(&sPt);
  if(NedPointIsFnJavascript(&sPt))
    fIsHtmlFn = TRUE;
  else if(NedPointStrCmp(&sPt, szDiv) == 0)
    fIsHtmlFn = TRUE;
  else if(NedPointStrCmp(&sPt, szHead) == 0)
    fIsHtmlFn = TRUE;
  else if(NedPointStrCmp(&sPt, szBody) == 0)
    fIsHtmlFn = TRUE;

  return fIsHtmlFn;
}

/*!------------------------------------------------------------------------------------------------
  Is this a markdown/text headings like #, ##, or ###, used also for shell scripts

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
bool_t NedPointIsFnText(const sNedPoint_t *pPoint)
{
  return (NedPointCharGet(pPoint) == '#') ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Is this a function line? Doesn't move point if not. Moves point to start of line if so. Also
  works with typedefs, structures and enums.

  @ingroup  ned_point
  @param    pPoint          pointer to preallocated point
  @param    pfnIsFuncLine   language specific function pointer (e.g. Python). NULL means C/C++

  @return   TRUE if this is a function line, FALSE if not.
*///-----------------------------------------------------------------------------------------------
bool_t NedPointIsFuncLine(const sNedPoint_t *pPoint, pfnIsFuncLine_t pfnIsFuncLine)
{
  sNedPoint_t   sPoint;
  bool_t        isFnLine;

  sPoint = *pPoint;
  NedPointLineBeg(&sPoint);
  if(pfnIsFuncLine == NULL)
    isFnLine = NedPointIsFnC(&sPoint);
  else
    isFnLine = (*pfnIsFuncLine)(&sPoint);
  return isFnLine;
}

/*!------------------------------------------------------------------------------------------------
  Go to next function line. Doesn't move point if not found. Moves point to start of line if
  so. Also works with typedefs, structures and enums.

  @ingroup  ned_point
  @param    pPoint          pointer to preallocated point
  @param    pfnIsFuncLine   language specific function pointer (e.g. Python). NULL means C/C++

  @return   TRUE if found a next function line, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedPointFuncNext(sNedPoint_t *pPoint, pfnIsFuncLine_t pfnIsFuncLine)
{
  bool_t        fFound = FALSE;
  sNedPoint_t   sPoint;

  // NedLogPrintfEx(NEDLOG_POINT, "NedPointFuncNext(pBuf %p, off %u)\n", pPoint->pBuf, pPoint->offset);

  sPoint = *pPoint;
  do
  {
    // find the function line
    fFound = NedPointIsFuncLine(&sPoint, pfnIsFuncLine);
    if(fFound)
      break;

    // not found, try next line (need to go to end of line to check if at bottom)
    // in case last line has text on it
    NedPointLineEnd(&sPoint);
    NedPointCharNext(&sPoint);
  } while(!NedPointIsAtBottom(&sPoint));

  if(fFound)
  {
    NedPointLineBeg(&sPoint);
    *pPoint = sPoint;
  }
  return fFound;
}

/*!------------------------------------------------------------------------------------------------
  Go to previous function line. Doesn't move point if not found. Moves point to start of line if
  so. Also works with typedefs, structures and enums.

  @ingroup  ned_point
  @param    pPoint          pointer to preallocated point
  @param    pfnIsFuncLine   language specific function pointer (e.g. Python). NULL means C/C++

  @return   TRUE if found a prev function line, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedPointFuncPrev(sNedPoint_t *pPoint, pfnIsFuncLine_t pfnIsFuncLine)
{
  bool_t        fFound = FALSE;
  sNedPoint_t   sPoint;

  // NedLogPrintfEx(NEDLOG_POINT, "NedPointFuncPrev(pBuf %p, off %u)\n", pPoint->pBuf, pPoint->offset);

  sPoint = *pPoint;
  do
  {
    // find the function line
    fFound = NedPointIsFuncLine(&sPoint, pfnIsFuncLine);
    if(fFound)
      break;

    // not found, try previous line
    NedPointLinePrev(&sPoint);
  } while(!NedPointIsAtTop(&sPoint));

  if(fFound)
  {
    NedPointLineBeg(&sPoint);
    *pPoint = sPoint;
  }
  return fFound;
}

/*!------------------------------------------------------------------------------------------------
  Search for the given string. Complicated function because of options (whole word, wrap, etc...).
  Doesn't move point unless string is found.

  Options include:
      NEDPT_SEARCH_OPT_CASE_SENSITIVE
      NEDPT_SEARCH_OPT_WHOLE_WORD
      NEDPT_SEARCH_OPT_WRAP
      NEDPT_SEARCH_OPT_REVERSE

  @ingroup  ned_point
  @param    pPoint       pointer to valid point
  @param    sz           null terminated string to search for
  @param    opts         bitmask of options

  @return   TRUE if string is found. FALSE if not.
*///-----------------------------------------------------------------------------------------------
bool_t NedPointSearch(sNedPoint_t *pPoint, const char *sz, nedPtSearchOpts_t opts)
{
  nedChar_t     c = 0;
  bool_t        fFound = FALSE;
  unsigned      szLen;
  long          lenOffset;
  long          thisLen;
  sNedPoint_t   sPoint;
  sNedPoint_t   sPtStartSearch;   // for dealing with wrap
  sNedPoint_t   sPtStartSz;
  bool_t        fWrapped = FALSE;

  NedAssertDbg(NedPointIsPoint(pPoint));
  NedAssertDbg(sz);

  NedLogPrintfEx(NEDLOG_POINT, "NedPointSearch(pBuf %p, offset %u), sz %s, opts x%02x\n",
    pPoint->pBuf, pPoint->offset, (strcmp(sz, "\r") == 0) ? "\\r" : sz, opts);

  // don't affect pPoint unless string found
  sPoint          = *pPoint;   // don't move point unless string is found
  sPtStartSearch  = *pPoint;   // for dealing with wrap

  szLen = strlen(sz);
  while(1)
  {
    // already wrapped, we're done
    if(fWrapped && NedPointIsSame(&sPoint, &sPtStartSearch))
      break;

    // looking forward or backward for string
    if(opts & NEDPT_SEARCH_OPT_REVERSE)
      c = NedPointCharGetPrev(&sPoint);
    else
      c = NedPointCharGetNext(&sPoint);

    // hit beginning or enter of file
    if(c == NEDCHAR_EOF)
    {
      if(!fWrapped && (opts & NEDPT_SEARCH_OPT_WRAP))
      {
        fWrapped = TRUE;
        if(opts & NEDPT_SEARCH_OPT_REVERSE)
          NedPointFileBottom(&sPoint);
        else
          NedPointFileTop(&sPoint);
        continue;
      }
      else
        break;
    }

    // match 1st character
    if(((opts & NEDPT_SEARCH_OPT_CASE_SENSITIVE) && (c == *sz)) || (toupper(c) == toupper(*sz)))
    {
      lenOffset = 0;
      sPtStartSz = sPoint;   // remember start of string case we found it
      while(lenOffset < szLen && sPoint.pBuf)
      {
        // determine length to compare this buffer
        thisLen = NedBufLen(sPoint.pBuf) - sPoint.offset;
        if(thisLen > (szLen - lenOffset))
          thisLen = (unsigned)(szLen - lenOffset);

        // compare data in this buffer to part of string
        if(thisLen)
        {
          if((opts & NEDPT_SEARCH_OPT_CASE_SENSITIVE))
          {
            if(memcmp(NedBufData(sPoint.pBuf) + sPoint.offset, &sz[lenOffset], thisLen) != 0)
              break;
          }
          else if(NedMemICmp(NedBufData(sPoint.pBuf) + sPoint.offset, &sz[lenOffset], thisLen) != 0)
            break;
        }

        // on to next buffer
        lenOffset    += thisLen;
        sPoint.pBuf   = NedBufNext(sPoint.pBuf);
        sPoint.offset = 0;
      }

      if(lenOffset == szLen)
      {
        // make sure character on either side of string is not part of this symbol
        // for example, don't find "_main" or "mainThing" if looking for "main"
        if(opts & NEDPT_SEARCH_OPT_WHOLE_WORD)
        {
          sPoint = sPtStartSz;
          if(NedCharType(NedPointCharGetPrev(&sPoint)) == IS_ALNUM)
          {
            sPoint = sPtStartSz;
            continue;
          }
          sPoint = sPtStartSz;
          NedPointCharAdvance(&sPoint, szLen);
          if(NedCharType(NedPointCharGet(&sPoint)) == IS_ALNUM)
          {
            sPoint = sPtStartSz;
            continue;
          }
        }

        fFound = TRUE;
        break;
      }
    }
  }

  if(fFound)
    *pPoint = sPtStartSz;

  NedLogPrintfEx(NEDLOG_POINT, "NedPointSearch => found %u\n", fFound);

  return fFound;
}

/*!------------------------------------------------------------------------------------------------
  Skip whitespace

  @ingroup  ned_point
  @param    pPoint       pointer to valid point

  @return   length skipped (0-n)
*///-----------------------------------------------------------------------------------------------
long NedPointSkipWhite(sNedPoint_t *pPoint)
{
  long        nSkipped = 0L;
  nedChar_t   c;

  while(1)
  {
    c = NedPointCharGet(pPoint);
    if((c != ' ' && c != '\t') || (c == NEDCHAR_EOF))
      break;
    NedPointCharNext(pPoint);
    ++nSkipped;
  }

  return nSkipped;
}

/*!------------------------------------------------------------------------------------------------
  Wrap text. The resulting text may be longer or shorter than the original text. Preserves double
  blank lines. pThis stays stationary. pThat may be moved. Words will not be split. Each non-blank
  line will have at least 1 word. If width is wide enough, multiple words.

  input:  "  a  string\nof things\n\n  lifts   a\n   wing  "
  output: "a string of things\nlifts a wing"

  @param    indent      indent for left column (# of tabs or spaces)
  @param    pThis       pointer to valid point
  @param    pThis       pointer to valid point which is AFTER pThis
  @param    width       column on which to wrap line(s)
  @return   TRUE if worked, FALSE if failed
*///-----------------------------------------------------------------------------------------------
bool_t NedPointWrapText(const sNedPtIndent_t *pIndent, const sNedPoint_t *pThis, sNedPoint_t *pThat, long width)
{
  long        len = 0;
  long        thisLen;
  uint8_t    *pData;      // skipped beginning whitespace
  uint8_t    *pLine;
  nedChar_t   aIndentChars[NEDCFG_MAX_INDENT];
  bool_t      fWorked = TRUE;

  NedAssert(width > 0 && pIndent->nCols >= 0 && pIndent->nChars >= 0);

  // determine length of data to wrap
  if(NedPointIsSameChain(pThis,pThat))
    len = NedPointDifference(pThis, pThat);
  if(len <= 0L)
    return FALSE;

  // setup indent characters, if any
  if(pIndent->nChars)
    memset(aIndentChars, pIndent->c, (pIndent->nChars > NEDCFG_MAX_INDENT) ? NEDCFG_MAX_INDENT : pIndent->nChars);

  // prepare for rewrapping the text
  pData = malloc(len);
  if(pData)
  {
    // convert to a single string of paragraphs
    len = NedPointMemGet(pData, pThis, len);
    len = NedMemRemoveExtraSpaces(pData, len);

    // delete the old text
    NedPointDelBlock((sNedPoint_t *)pThis, pThat);
    *pThat = *pThis;

    // insert the new text, one line at a time
    pLine = pData;
    while(fWorked && len)
    {
      // on a paragrpah, just insert the \n, no indent
      if(*pLine == '\n')
      {
        if(!NedPointInsChar(pThat, '\n'))
          fWorked = FALSE;
        ++pLine;
        --len;
      }

      // indent and put as many characters as possible to wrap the line
      else
      {
        if(pIndent->nChars && !NedPointInsBlock(pThat, aIndentChars, pIndent->nChars))
          fWorked = FALSE;

        // insert the wrapped line, always ends in \n
        thisLen = NedMemFindWrap(pLine, len, width - pIndent->nChars);
        NedLogPrintfEx(NEDLOG_POINT, "offset %zu, thisLen %ld, width %ld\n", (size_t)(pLine - pData), thisLen, width - pIndent->nChars);
        if(!NedPointInsBlock(pThat, pLine, thisLen))
          fWorked = FALSE;
        if(!NedPointInsChar(pThat, '\n'))
          fWorked = FALSE;

        // if at word, skip the space
        pLine += thisLen;
        len   -= thisLen;
        if(len >= 0L && *pLine == ' ')
        {
          NedLogPrintfEx(NEDLOG_POINT, "on space\n");
          ++pLine;
          --len;
        }
      }
    }

    free(pData);
  }

  return fWorked;
}

/*!------------------------------------------------------------------------------------------------
  Shows a hexdump -C of the pFrom point (which might contain binary data) to the pTo point.
  Advances point.

    00008050  01 02 00 00 00 5f 70 72  69 6e 74 66 00 00 00 00  |....._printf....|
    00008290  12                                                |.|

  @param    pTo         point that shall receive the hex dump text, will end up on point after dump
  @param    startAddr   Adddress for 1st line of the hex dump
  @param    pFrom       pointer to a valid point
  @param    len         Length of data at pFrom
  @return   TRUE if worked, FALSE if failed
*///-----------------------------------------------------------------------------------------------
bool_t NedPointDumpToPoint(sNedPoint_t *pTo, long startAddr, const sNedPoint_t *pFrom, long len)
{
  sNedPoint_t   sPoint;
  long          thisLen;
  long          lineLen;
  uint8_t       aLine[NED_CHAR_DUMP_COLS];
  // uint8_t       aLastLine[NED_CHAR_DUMP_COLS];
  char          szLine[NED_CHAR_DUMP_LINE_LEN];
  bool_t        fWorked = TRUE;

  sPoint = *pFrom;
  while(fWorked && len > 0L)
  {
    // determine length of this line
    thisLen = NED_CHAR_DUMP_COLS;
    if(thisLen > len)
      thisLen = len;

    // create the dump line and write it to the destination Point pTo
    thisLen = NedPointMemGet(aLine, &sPoint, thisLen);
#if 0
    if(!fFirst && thisLen == NED_CHAR_DUMP_COLS && memcmp(aLine, aLastLine, thisLen) == 0)
    {
      NedPointInsStr("*\n");
      while(memcmp(aLine, aLastLine, thisLen) == 0
    }
#endif

    lineLen = NedCharDumpLine(szLine, startAddr, aLine, thisLen);
    if(!NedPointInsBlock(pTo, (uint8_t *)szLine, lineLen))
      fWorked = FALSE;

    // on to next data
    NedPointCharAdvance(&sPoint, thisLen);
    startAddr += thisLen;
    len -= thisLen;
    // fFirst = FALSE;
  }

  return fWorked;
}
