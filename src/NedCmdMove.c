/*!************************************************************************************************
  @file NedCmdMove.c

  @brief  Ned Command Movement function implementation

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  Includes CmdBookmarkXxx, CmdCursorXxx, CmdMoveXxx

*///***********************************************************************************************
#include "NedEditor.h"

/*!------------------------------------------------------------------------------------------------
  Go up one line. Preserve column.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdCursorUp(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();

  if(NedPointLinePrev(NedEditorCursor()))
  {
    NedEditorInvalidateSelLine();
    --pFile->curLine;
    if(pFile->curLine < pFile->topLine)
    {
      --pFile->topLine;
      NedEditorNeedRedraw();
    }
    pFile->curCol = NedEditorColSet(NedEditorCursor(), pFile->prefCol);
    NedEditorInvalidateSelected();
  }
}

/*!------------------------------------------------------------------------------------------------
  Move cursor down one line. Does nothing if at bottom of file

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdCursorDown(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();

  if(NedPointLineNext(NedEditorCursor()))
  {
    NedEditorInvalidateSelLine();
    ++pFile->curLine;
    if((pFile->curLine - pFile->topLine) >= NedEditorEditRows())
    {
      ++pFile->topLine;
      NedEditorNeedRedraw();
    }
    pFile->curCol = NedEditorColSet(NedEditorCursor(), pFile->prefCol);
    NedEditorInvalidateSelected();
  }
}

/*!------------------------------------------------------------------------------------------------
  Move cursor left one column. Does nothing if at top of file

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdCursorLeft(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();
  bool_t        fCrossedLine  = FALSE;

  if(NedPointCharPrev(NedEditorCursor()))
  {
    // are we going to another line?
    if(NedPointCharGet(NedEditorCursor()) == '\n')
      fCrossedLine = TRUE;

    if(fCrossedLine)
    {
      NedEditorInvalidateSelLine();
      --pFile->curLine;
      if(pFile->curLine < pFile->topLine)
      {
        --pFile->topLine;
      NedEditorNeedRedraw();
      }        
    }
    pFile->curCol = NedEditorColGet(NedEditorCursor());
    pFile->prefCol = pFile->curCol;
    NedEditorInvalidateSelected();
  }
}

/*!------------------------------------------------------------------------------------------------
  Move cursor right one column. Does nothing if at bottom of file

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdCursorRight(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();
  bool_t        fCrossedLine  = FALSE;

  // are we going to another line?
  if(NedPointCharGet(NedEditorCursor()) == '\n')
    fCrossedLine = TRUE;

  if(NedPointCharNext(NedEditorCursor()))
  {
    if(fCrossedLine)
    {
      NedEditorInvalidateSelLine();
      ++pFile->curLine;
      if((pFile->curLine - pFile->topLine) >= NedEditorEditRows())
      {
        ++pFile->topLine;
        NedEditorNeedRedraw();
      }
    }
    NedEditorFileCur()->curCol = NedEditorColGet(NedEditorCursor());
    pFile->prefCol = pFile->curCol;
    NedEditorInvalidateSelected();
  }
}

/*!------------------------------------------------------------------------------------------------
  Move cursor left one column. Does nothing if at top of file

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdCursorLeftSelect(void)
{
  sNedFile_t  *pFile = NedEditorFileCur();

  if(!pFile->fInSelect)
  {
    pFile->fInSelect = TRUE;
    pFile->sSelect   = pFile->sCursor;
  }
  NedCmdCursorLeft();
}

/*!------------------------------------------------------------------------------------------------
  Move cursor right one column. Does nothing if at bottom of file

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdCursorRightSelect(void)
{
  sNedFile_t  *pFile = NedEditorFileCur();

  if(!pFile->fInSelect)
  {
    pFile->fInSelect = TRUE;
    pFile->sSelect = pFile->sCursor;
  }
  NedCmdCursorRight();
}

/*!------------------------------------------------------------------------------------------------
  Go to the top of the file

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdCursorTop(void)
{
  sNedFile_t    *pFile = NedEditorFileCur();

  if(!NedPointIsAtTop(NedEditorCursor()))
    NedEditorAddPosHistory();
  if(pFile->topLine != 1)
    NedEditorNeedRedraw();
  NedPointFileTop(NedEditorCursor());
  pFile->topLine = pFile->curLine = pFile->curCol = pFile->prefCol = 1;
  NedEditorInvalidateSelected();
}

/*!------------------------------------------------------------------------------------------------
  Go to the bottom of the file

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdCursorBottom(void)
{
  sNedFile_t    *pFile = NedEditorFileCur();

  if(!NedPointIsAtBottom(NedEditorCursor()))
  {
    NedEditorAddPosHistory();
    NedPointFileBottom(NedEditorCursor());
  }
  pFile->curLine = NedEditorLineGet(NedEditorCursor(), &pFile->curCol);

  pFile->prefCol = pFile->curCol;
  if(pFile->curLine > NedEditorEditRows())
  {
    pFile->topLine = 1 + (pFile->curLine - NedEditorEditRows());
    NedEditorNeedRedraw();
  }
  NedEditorInvalidateSelected();
}

/*!------------------------------------------------------------------------------------------------
  Go to start of line. Never changes lines.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdCursorHome(void)
{
  NedPointLineBeg(NedEditorCursor());
  NedEditorFileCur()->curCol = NedEditorFileCur()->prefCol = 1;
  NedEditorInvalidateSelected();
}

/*!------------------------------------------------------------------------------------------------
  Go to end of line. Never changes lines.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdCursorEnd(void)
{
  NedPointLineEnd(NedEditorCursor());
  NedEditorFileCur()->curCol = NedEditorFileCur()->prefCol = NedEditorColGet(NedEditorCursor());
  NedEditorInvalidateSelected();
}

/*!------------------------------------------------------------------------------------------------
  Go up one page. If not enough lines to go up one page, goes to top of file.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdCursorPgUp(void)
{
  sNedFile_t   *pFile     = NedEditorFileCur();
  unsigned      scrollAmt = NedEditorEditRows();

  // already at top of file, just move cursor to top of file
  if(pFile->topLine == 1)
  {
    NedPointFileTop(NedEditorCursor());
    pFile->curLine = 1;
    pFile->curCol = pFile->leftCol = 1;
  }
  else
  {
    NedEditorNeedRedraw();
    pFile->curLine -= scrollAmt;
    pFile->topLine -= scrollAmt;
    if(pFile->curLine <= 0)
      pFile->curLine = 1;
    if(pFile->topLine <= 0)
      pFile->topLine = 1;
    NedPointLineGoto(NedEditorCursor(), pFile->curLine);
    pFile->curCol = NedEditorColSet(NedEditorCursor(), pFile->prefCol);
  }
  NedEditorInvalidateSelected();
}

/*!------------------------------------------------------------------------------------------------
  Go down one page. If bottom of file already on screen, goes to end of file.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdCursorPgDn(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();
  sNedPoint_t   sNewCur;
  long          newCurLine;
  unsigned      scrollAmt;
  bool_t        fFullScroll = FALSE;

  if(!NedPointIsAtBottom(NedEditorCursor()))
  {
    // note: NedPointLineGoto() won't go past end of file, so it's safe to use
    scrollAmt = NedEditorEditRows();
    sNewCur = *NedEditorCursor();
    newCurLine = NedPointLineGoto(&sNewCur, pFile->curLine + scrollAmt);
    NedAssertDbg(newCurLine >= pFile->curLine);

    // if not enough lines for a full page, go to end of file
    if(newCurLine < pFile->curLine + scrollAmt)
    {
      NedPointFileBottom(&sNewCur);
      scrollAmt = newCurLine - pFile->curLine;
      NedLogPrintfEx(NEDLOG_CMD_EDIT, "NedCmdCursorPgDn, scrollAmt %u, prefCol %ld, topLine %ld\n",
        scrollAmt, pFile->prefCol, pFile->topLine);
    }
    else
    {
      pFile->topLine += scrollAmt;
      fFullScroll = TRUE;
    }

    pFile->curLine += scrollAmt;
    *NedEditorCursor() = sNewCur;
    if(fFullScroll)
      NedEditorFileCur()->curCol = NedEditorColSet(NedEditorCursor(), pFile->prefCol);

    NedEditorBuildAroundCursor();
    NedEditorInvalidateSelected();
  }
}

/*!------------------------------------------------------------------------------------------------
  Go one word to the left.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdCursorWordLeft(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();
  bool_t        fCrossedLine = FALSE;

  if(NedPointWordPrev(NedEditorCursor()))
  {
    // did we go to the previous line?
    if(NedPointCharGet(NedEditorCursor()) == '\n')
      fCrossedLine = TRUE;

    if(fCrossedLine)
    {
      NedEditorInvalidateSelLine();
      --pFile->curLine;
      if(pFile->curLine < pFile->topLine)
      {
        --pFile->topLine;
        NedEditorNeedRedraw();
      }
    }
    pFile->curCol  = NedEditorColGet(NedEditorCursor());
    pFile->prefCol = pFile->curCol;
  }
  NedEditorInvalidateSelected();
}

/*!------------------------------------------------------------------------------------------------
  Go one word to the right

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdCursorWordRight(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();
  bool_t        fCrossedLine = FALSE;

  // are we going to another line?
  if(NedPointCharGet(NedEditorCursor()) == '\n')
    fCrossedLine = TRUE;

  if(NedPointWordNext(NedEditorCursor()))
  {
    if(fCrossedLine)
    {
       NedEditorInvalidateSelLine();
      ++pFile->curLine;
    }
    if((pFile->curLine - pFile->topLine) >= NedEditorEditRows())
    {
      ++pFile->topLine;
      NedEditorNeedRedraw();
    }

    pFile->curCol  = NedEditorColGet(NedEditorCursor());
    pFile->prefCol = pFile->curCol;
  }
  NedEditorInvalidateSelected();
}

/*!------------------------------------------------------------------------------------------------
  Center the cursor on the screen. If cursor is near the top of the file, it may be higher.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdMoveCenter(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();
  pFile->topLine = pFile->curLine - ((NedEditorEditRows() + 1) >> 1);
  if(pFile->topLine < 1)
    pFile->topLine = 1;
  NedEditorNeedRedraw();
  NedMsgPrintf("MoveCenter");
}

/*!------------------------------------------------------------------------------------------------
  Potentially opens a new file. 

  pPoint is in the error file.
  pErrInfo defines a new flke.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
static void NcmGotoErr(sNedPoint_t *pPoint, sNedPtErrInfo_t *pErrInfo)
{
  sNedFile_t  *pFile;

  // for debugging
  if(NedLogMaskGet() & NEDLOG_CMD)
  {
    NedLogPrintfEx(NEDLOG_CMD, "NcmGotoErr(file %s line %ld col %ld\n  ===> %s\n", pErrInfo->szFile, pErrInfo->line, 
      pErrInfo->col, pErrInfo->szMsg); 
    NedEditorLogShow(NedEditor(), 2);
  }

  pFile = NedEditorFileNew(pErrInfo->szFile, 0L);
  if(pFile)
  {
    // adjust fields such as topLine and curCol in error file
    NedEditorAdjustFileFields(NedEditorFileErr());

    // goto the specified file/position by pErrInfo
    NedPointGotoLineCol(&pFile->sCursor, pErrInfo->line, pErrInfo->col);
    NedEditorGotoPoint(&pFile->sCursor);
    NedMsgPrintf("%s", pErrInfo->szMsg);
    NedEditorLogShow(NedEditor(), 2);
  }
}

/*!------------------------------------------------------------------------------------------------
  Move to the next error message in the error file. Load the file if necessary.

  If cannot load file, don't move cursor, and just indicate failed load.

  @ingroup  ned_edit
  @returns  none
*///-----------------------------------------------------------------------------------------------
static bool_t NcmErrNext(void)
{
  sNedPtErrInfo_t   sErrInfo;
  sNedPoint_t      *pCursor;
  bool_t            fFound = FALSE;

  if(NedEditorFileErr())
  {
    pCursor = &NedEditorFileErr()->sCursor;
    if(NedPointIsAtTop(pCursor) && NedPointIsErrLine(pCursor, &sErrInfo))
    {
      fFound = TRUE;
      NedPointLineNext(pCursor);
    }
    else
    {
      NedPointLineNext(pCursor);
      if(NedPointErrLineNext(pCursor, &sErrInfo))
        fFound = TRUE;
    }
    if(fFound)
    {
      NcmGotoErr(pCursor, &sErrInfo);
      NedPointErrFree(&sErrInfo);
    }
  }
  return fFound;
}

/*!------------------------------------------------------------------------------------------------
  Move to the next error message. Load file if necessary.

  If cannot load file, don't move cursor, and just indicate failed load.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdMoveErrNext(void)
{
  if(!NcmErrNext())
    NedMsgPrintf("No more errors");
}

/*!------------------------------------------------------------------------------------------------
  Move to the previous error message. Load file if necessary.

  If cannot load file, don't move cursor, and just indicate failed load.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdMoveErrPrev(void)
{
  sNedPtErrInfo_t   sErrInfo;
  sNedPoint_t      *pCursor;
  bool_t            fIsErrLine = FALSE;

  if(NedEditorFileErr())
  {
    pCursor = &NedEditorFileErr()->sCursor;
    NedPointLinePrev(pCursor);
    if(NedPointErrLinePrev(pCursor, &sErrInfo))
    {
      NcmGotoErr(pCursor, &sErrInfo);
      NedPointErrFree(&sErrInfo);
      fIsErrLine = TRUE;
    }
  }
  if(!fIsErrLine)
    NedMsgPrintf("No previous errors");
}

/*!------------------------------------------------------------------------------------------------
  Move to the next function/structure.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdMoveFunctionDown(void)
{
  sNedFile_t           *pFile         = NedEditorFileCur();
  pfnIsFuncLine_t       pfnIsFuncLine = NedEditorGetLangFuncLine(NedFilePath(pFile));
  sNedPoint_t           sPoint;

  sPoint = *NedEditorCursor();
  NedPointLineNext(&sPoint);
  if(NedPointFuncNext(&sPoint, pfnIsFuncLine))
  {
    *NedEditorCursor()  = sPoint;
    pFile->curLine      = NedPointLineGet(NedEditorCursor(), NULL);
    pFile->curCol       = pFile->prefCol = 1;
    if(NedEditorAdjustCurFileFields())
      NedEditorNeedRedraw();
    NedEditorInvalidateSelected();
  }
  else
  {
    NedPointFileBottom(NedEditorCursor());
    NedEditorAdjustCurFileFields();
  }
}

/*!------------------------------------------------------------------------------------------------
  Move to the previous function/structure.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdMoveFunctionUp(void)
{
  sNedFile_t           *pFile         = NedEditorFileCur();
  pfnIsFuncLine_t       pfnIsFuncLine = NedEditorGetLangFuncLine(NedFilePath(pFile));
  sNedPoint_t           sPoint;

  sPoint = *NedEditorCursor();
  NedPointLinePrev(&sPoint);
  if(NedPointFuncPrev(&sPoint, pfnIsFuncLine))
  {
    *NedEditorCursor()  = sPoint;
    pFile               = NedEditorFileCur();
    pFile->curLine      = NedPointLineGet(NedEditorCursor(), NULL);
    pFile->curCol       = pFile->prefCol = 1;
    if(NedEditorAdjustCurFileFields())
      NedEditorNeedRedraw();
    NedEditorInvalidateSelected();
  }
  else
  {
    NedPointFileTop(NedEditorCursor());
    NedEditorAdjustCurFileFields();
  }
}

/*!------------------------------------------------------------------------------------------------
  Go to a particular line number.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdMoveGotoLine(void)
{
  sNedFile_t   *pFile;
  char          szLine[20];  // max 9223372036854775807
  long          len;
  long          line;

  // get the line # (should be 1-n)
  szLine[0] = '\0';
  len = NedMsgLineEdit(szLine, sizeof(szLine)-1, "Goto Line: ", NULL, NULL);

  // if user pressed enter and actually entered something
  if(len > 0)
  {
    line = atol(szLine);
    if(line >= 1)
    {
      pFile = NedEditorFileCur();
      pFile->curLine = NedPointLineGoto(NedEditorCursor(), line);
      pFile->curCol  = pFile->prefCol = 1;
    }
  }
}


/*!------------------------------------------------------------------------------------------------
  Find the matching brace.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdMoveMatchBrace(void)
{
  sNedFile_t   *pFile;
  long          col;

  if(NedPointBraceMatch(NedEditorCursor()))
  {
    pFile           = NedEditorFileCur();
    pFile->curLine  = NedPointLineGet(NedEditorCursor(), &col);
    pFile->curCol   = pFile->prefCol = col;
    if(NedEditorAdjustCurFileFields())
      NedEditorNeedRedraw();
  }
}

/*!------------------------------------------------------------------------------------------------
  Move to the next position in the PosHistory. May be in another file.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdMovePosNext(void)
{
  sNedPoint_t  sPoint;
  sNedPos_t   *pPos;
  sNedPos_t    sPos;

  NedLogPrintfEx(NEDLOG_CMD, "<<< NedCmdMovePosNext >>>\n");

  pPos = NedListNext(NedEditor()->hPosHistory);
  if(pPos)
  {
    // get current position
    sPos.pFile = NedEditorFileCur();
    sPos.pos   = NedPointPos(NedEditorCursor());

    // go to next position
    NedPointInit(&sPoint, pPos->pFile->pBufHead);
    NedPointFileGotoPos(&sPoint, pPos->pos);
    NedEditorGotoPoint(&sPoint);

    // if we're moving away from this position and it's not in the list, add it
    if(NedListFind(NedEditor()->hPosHistory, &sPos) == NULL)
      NedEditorAddPosHistoryEx(&sPos);
  }

  if(NedLogMaskGet() & NEDLOG_CMD)
    NedListLogShow(NedEditor()->hPosHistory, TRUE);
}

/*!------------------------------------------------------------------------------------------------
  Move to the previous position in the PosHistory. May be in another file.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdMovePosPrev(void)
{
  sNedPoint_t  sPoint;
  sNedPos_t   *pPos;
  sNedPos_t    sPos;

  NedLogPrintfEx(NEDLOG_CMD, "<<< NedCmdMovePosPrev >>>\n");

  pPos = NedListPrev(NedEditor()->hPosHistory);
  if(pPos)
  {
    // get current position
    sPos.pFile = NedEditorFileCur();
    sPos.pos   = NedPointPos(NedEditorCursor());

    // goto previous position
    NedPointInit(&sPoint, pPos->pFile->pBufHead);
    NedPointFileGotoPos(&sPoint, pPos->pos);
    NedEditorGotoPoint(&sPoint);

    // if we're moving away from this position and it's not in the list, add it
    if(NedListFind(NedEditor()->hPosHistory, &sPos) == NULL)
      NedEditorAddPosHistoryEx(&sPos);
  }

  if(NedLogMaskGet() & NEDLOG_CMD)
    NedListLogShow(NedEditor()->hPosHistory, TRUE);
}


/*!------------------------------------------------------------------------------------------------
  Scroll up in file one line

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdMoveScrollUp(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();
  unsigned      i;

  if(pFile->topLine > 1 || (pFile->curLine > pFile->topLine))
  {
    NedEditorNeedRedraw();
    pFile->topLine -= NEDEDITOR_SCROLL_AMOUNT;
    if(pFile->topLine < 1)
      pFile->topLine = 1;
    pFile->curLine -= NEDEDITOR_SCROLL_AMOUNT;
    if(pFile->curLine < 1)
      pFile->curLine = 1;
    for(i=0; i<NEDEDITOR_SCROLL_AMOUNT; ++i)
      NedPointLinePrev(&pFile->sCursor);
    pFile->curCol = NedEditorColSet(NedEditorCursor(), pFile->prefCol);
  }
}

/*!------------------------------------------------------------------------------------------------
  Scroll down in file one line

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdMoveScrollDown(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();
  unsigned      moved = 0;
  unsigned      i;

  for(i=0; i<NEDEDITOR_SCROLL_AMOUNT; ++i)
  {
    if(NedPointLineNext(&pFile->sCursor))
      moved++;
  }

  if(moved || pFile->curLine > pFile->topLine)
  {
    pFile->topLine += moved;
    if(moved)
    {
      pFile->curLine += moved;
      pFile->curCol   = NedEditorColSet(NedEditorCursor(), pFile->prefCol);
    }
    NedEditorNeedRedraw();
  }
}

