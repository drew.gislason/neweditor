/*!************************************************************************************************
  @file NedEditor.c

  @brief  Private functions

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_editor Ned Editor - An API wrapping all subsystems together for an editor.

  The editor keeps the state of everything by initializing and including other objects.

*///***********************************************************************************************
#include "Ned.h"
#include "NedEditor.h"
#include "NedSettings.h"
#include "NedPosList.h"
#include "NedUndo.h"
#include "NedSignal.h"

/*-------------------------------------------------------------------------------------------------
  Globals and module private globals
-------------------------------------------------------------------------------------------------*/
static const char     m_szDefProjName[] = NEDEDITOR_PROJNAME;   // "ned.json"
static sNedEditor_t   m_sEditor;
static bool_t         m_fInSignal;

#if NEDDBG_LEVEL >= 1
  #define NED_SZ_DBG  " %s (debug) "
#else
  #define NED_SZ_DBG  " %s "
#endif
const char szNedCmd_HelpAbout[] = NEDVER NED_SZ_DBG "Copyright 2021 Drew Gislason, compiled for %s, C version %lu, Git sha %s";

/*-------------------------------------------------------------------------------------------------
  Return a pointer to the editor.

  @ingroup  ned_edit
  @return   Pointer to editor structure
-------------------------------------------------------------------------------------------------*/
sNedEditor_t *NedEditor(void)
{
  return &m_sEditor;
}

/*-------------------------------------------------------------------------------------------------
  Clears an initialized editor of all fields.

  @ingroup  ned_edit
  @param    pEditor   number of strings

  @return   TRUE if successful
-------------------------------------------------------------------------------------------------*/
void NedEditorClear(sNedEditor_t *pEditor)
{
  NedAssert(NedEditorIsEditor(pEditor));

  // clear all lists
  NedListClear(pEditor->hPosHistory);
  NedListClear(pEditor->hBookmarks);
  NedListClear(pEditor->hRecentFiles);
  NedListClear(pEditor->hProjectFolders);
  NedFileFreeAll(pEditor->pFileList);
  NedEditorClipboard(TRUE);

  // leaves project name, menus, theme, etc... alone. See NedSettingsInitEditor().
}

/*!-------------------------------------------------------------------------------------------------
  Initializes the editor in preparation for using. All lists are initialized to empty. If an
  editor is already initialized, clears it.

  @ingroup  ned_edit
  @param    pEditor   number of strings

  @return   TRUE if successful
*///-------------------------------------------------------------------------------------------------
void NedEditorInit(sNedEditor_t *pEditor)
{
  if(NedEditorIsEditor(pEditor))
  {
    NedEditorClear(pEditor);
    NedUndoListClear(pEditor->pUndoList);
  }
  else
  {
    NedSettingsInitEditor(pEditor);
    NedEditorClipboard(TRUE);
    pEditor->themeIndex = NEDTHEME_FACTORY;
    pEditor->pUndoList = NedUndoListNew(NedEditorGetFileByPoint);
  }
}

/*!-------------------------------------------------------------------------------------------------
  Creates the clipboard pseudofile (if not created already)

  @ingroup  ned_edit
  @param    pEditor   number of strings

  @return   TRUE if successful
*///-------------------------------------------------------------------------------------------------
sNedFile_t * NedEditorClipboard(bool_t fClear)
{
  if(NedEditor()->pClipboard == NULL)
    NedEditor()->pClipboard = NedFileNew(NULL, "NEDCLIP.TXT");
  if(fClear)
    NedFileClear(NedEditor()->pClipboard);
  return NedEditor()->pClipboard;
}

/*-------------------------------------------------------------------------------------------------
  Print the editor stucture to the log

  @ingroup  ned_edit
  @param    pEditor   pointer to initalized editor
  @param    detail    0 = minimal, 1= some, 2 = lots

  @return   none
-------------------------------------------------------------------------------------------------*/
void NedEditorLogShow(sNedEditor_t *pEditor, nedEditorDetail_t detail)
{
  sNedFile_t   *pFile;
  unsigned      i;
  long          curLine;
  long          curCol;
  long          selLine;
  long          selCol;

  NedAssert(NedEditorIsEditor(pEditor));
  NedLogPrintf("Editor %p => screenRows %u, screenCols %u, editRows %u\n",
    pEditor, NedEditorScreenRows(), NedEditorScreenCols(), NedEditorEditRows());
  NedLogPrintf("  pFileList %p, pFileCur %p, pFileErr %p, szProjName %s\n", 
    pEditor->pFileList, pEditor->pFileCur, pEditor->pFileErr, pEditor->szProjPath);
  if(detail >= NEDEDITOR_DETAIL_FILE_LIST)
  {
    // display all files
    NedLogPrintf("  Files:\n");
    pFile = NedEditor()->pFileList;
    for(i = 0; pFile; ++i)
    {
      // prevent infinite loop
      if(i >= NEDCFG_MAX_FILES)
      {
        NedLogPrintf("TOO MANY FILES, max %u\n", NEDCFG_MAX_FILES);
        break;
      }

      NedLogPrintf("%c%d) %s\n", pFile==pEditor->pFileCur ? '*' : ' ', i, NedFilePath(pFile));
      NedLogPrintf("    file %p, id %u, topLine %ld, curLine %ld, curCol %ld, leftCol %ld, prefCol %ld\n",
        pFile, NedBufId(pFile->pBufHead),
        pFile->topLine, pFile->curLine, pFile->curCol, pFile->leftCol, pFile->prefCol);

      curLine = NedPointLineGet(NedEditorCursor(), &curCol);
      selLine = NedPointLineGet(NedEditorSelect(), &selCol);
      NedLogPrintf("    inSelect %u, select(%ld:%ld), cursor(%ld:%ld), crLf %u, htab %u, tabSize %u\n",
        pFile->fInSelect, selLine, selCol, curLine, curCol, pFile->fCrLf, pFile->fHardTabs, pFile->tabSize);

      if(detail >= NEDEDITOR_DETAIL_FILE_CONTENTS)
        NedBufLogDumpChain(pFile->pBufHead, 16);

      pFile = pFile->pNext;
    }
    if(detail >= NEDEDITOR_DETAIL_ALL_LIST)
    {
      NedLogPrintf("PosHistory:\n    ");
      NedListLogShow(pEditor->hPosHistory, TRUE);
      NedLogPrintf("Bookmarks:\n    ");
      NedListLogShow(pEditor->hBookmarks, TRUE);
      NedLogPrintf("RecentFiles:\n    ");
      NedListLogShow(pEditor->hRecentFiles, TRUE);
      NedLogPrintf("ProjectFolders:\n    ");
      NedListLogShow(pEditor->hProjectFolders, TRUE);
    }
    else
    {
      NedLogPrintf("  PosHistory     %u\n", NedListNumItems(pEditor->hPosHistory));
      NedLogPrintf("  Bookmarks      %u\n", NedListNumItems(pEditor->hBookmarks));
      NedLogPrintf("  RecentFiles    %u\n", NedListNumItems(pEditor->hRecentFiles));
      NedLogPrintf("  ProjectFolders %u\n", NedListNumItems(pEditor->hProjectFolders));
    }
  }

  NedLogPrintf("\n");
}

/*!------------------------------------------------------------------------------------------------
  Set the them in the View, Menu and Msg subsystems.

  @ingroup  ned_edit

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorSetTheme(unsigned themeIndex)
{
  NedEditor()->themeIndex = themeIndex;
  NedMenuSetTheme(NedEditor()->hMenu, themeIndex);
  NedViewSetTheme(NedEditorView(), NedThemeGet(themeIndex));
  NedStatusBarSetTheme(NedThemeGet(themeIndex));
}

/*!------------------------------------------------------------------------------------------------
  Return the current theme pointer for the editor.

  @ingroup  ned_edit

  @return   none
*///-----------------------------------------------------------------------------------------------
nedTheme_t * NedEditorTheme(void)
{
  return NedThemeGet(NedEditor()->themeIndex);
}

/*!------------------------------------------------------------------------------------------------
  Indicate the editor needs to be redrawn.

  @ingroup  ned_edit

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorNeedRedraw(void)
{
  NedEditor()->fRedrawAll = TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Redraw menu, editor Text, Status bar and Message Line. Assumes all editor/file fields are up
  to date.

  @ingroup  ned_edit

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorRedraw(void)
{
  // redraw all elements on screen
  NedMenuDraw(NedEditor()->hMenu);
  NedEditorUpdateStatusBar();
  NedMsgRedraw();
  NedViewInvalidateAll(NedEditorView());
  NedViewDraw(NedEditorView(), NedEditorFileCur());
}

/*!------------------------------------------------------------------------------------------------
  Adjust file fields based on cursor position. Only affects the sNedFile_t structure for the
  current file.

  @ingroup  ned_edit

  @return   TRUE if topline was adjusted (need to redraw screen)
*///-----------------------------------------------------------------------------------------------
bool_t NedEditorAdjustFileFields(sNedFile_t *pFile)
{
  bool_t          fAdjusted = FALSE;

  // adjust topLine, curLine, curCol if needed
  pFile->curLine = NedEditorLineGet(&pFile->sCursor, &pFile->curCol);
  if((pFile->curLine < pFile->topLine) || (pFile->curLine >= pFile->topLine + NedEditorEditRows()))
  {
    // put curLine in middle of screen if possible
    pFile->topLine = pFile->curLine - ((NedEditorEditRows() + 1) >> 1);
    if(pFile->topLine < 1)
      pFile->topLine = 1;
    fAdjusted = TRUE;
  }

  // adjust leftCol if needed
  if(pFile->curCol < pFile->leftCol)
  {
    pFile->leftCol = pFile->curCol;
    fAdjusted = TRUE;
  }
  if(pFile->curCol >= (pFile->leftCol + NedEditorScreenCols() - 1))
  {
    pFile->leftCol = pFile->curCol - (NedEditorScreenCols() - 1);
    fAdjusted = TRUE;
  }

  return fAdjusted;
}


/*!------------------------------------------------------------------------------------------------
  Adjust file fields based on cursor position. Only affects the sNedFile_t structure for the
  current file.

  @ingroup  ned_edit

  @return   TRUE if topline was adjusted (need to redraw screen)
*///-----------------------------------------------------------------------------------------------
bool_t NedEditorAdjustCurFileFields(void)
{
  bool_t          fAdjusted;

  fAdjusted = NedEditorAdjustFileFields(NedEditorFileCur());
  if(fAdjusted)
  {
    NedEditorNeedRedraw();
    NedEditor()->fDirty = TRUE;
  }

  return fAdjusted;
}

/*!------------------------------------------------------------------------------------------------
  Updates all fields around cursor. Does not draw anything.

  @ingroup  ned_edit

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorBuildAroundCursor(void)
{

  NedLogPrintfEx(NEDLOG_EDITOR, "NedEditorBuildAroundCursor()\n");

  // make sure the size of window is correct (updates pEditor->screenRows, screenCols, editRows)
  NedViewCheckScreenSize(NedEditorView());
  NedEditorNeedRedraw();
  NedViewInvalidateAll(NedEditorView());
  NedEditorAdjustCurFileFields();
}

/*!------------------------------------------------------------------------------------------------
  Update screen  in the minimalist way possible. Always updates status bar and flushes cursor to
  current editor cursor position.

  @ingroup  ned_edit

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorUpdateSmart(void)
{
  sNedFile_t          *pFile    = NedEditorFileCur();
  sNedEditor_t        *pEditor  = NedEditor();

  // if cursor is off-screen, the need to redraw
  if((pFile->curLine < pFile->topLine) || (pFile->curLine >= pFile->topLine + NedEditorEditRows()))
    NedEditorBuildAroundCursor();
  if((pFile->curCol < pFile->leftCol) || (pFile->curCol >= (pFile->leftCol + NedEditorScreenCols())))
    NedEditorBuildAroundCursor();

  // draw menu, editor text, status bar and message line
  if(pEditor->fRedrawAll)
  {
    NedEditorRedraw();
  }
  // draw only invalidated lines
  else if(pEditor->fDirty)
  {
    NedViewDraw(NedEditorView(), NedEditorFileCur());
  }
  pEditor->fRedrawAll = FALSE;
  pEditor->fDirty     = FALSE;

  // make sure cursor is at cursor in editor
  NedEditorUpdateStatusBar();
  NedViewFlushCursor(pEditor->hView, pFile);
}

void NedEditorFlushCursor(void)
{
  NedViewFlushCursor(NedEditorView(), NedEditorFileCur());
}

/*!------------------------------------------------------------------------------------------------
  Load a file into the editor. Does not load or change cursor position if already loaded.

  Does not affect the display.

  Does NOT affect pFileCur unless the pFileList hasn't been created yet. Then it sets both.

  @ingroup  ned_edit

  @return   Pointer to file if worked, otherwise NULL
*///-----------------------------------------------------------------------------------------------
sNedFile_t * NedEditorFileNew(const char *szFilename, long pos)
{
  sNedFile_t             *pFile = NULL;

  // nothing to do, file already in the pFileList
  NedLogPrintfEx(NEDLOG_EDITOR, "NedEditorFileNew(%s, pos=%ld)\n", szFilename, pos);
  pFile = NedFileInList(NedEditor()->pFileList, szFilename);
  if(pFile)
    NedLogPrintfEx(NEDLOG_EDITOR, "  found %p, %s\n", pFile, NedFilePath(pFile));

  if(!pFile)
  {
    // loads the file if it exists, create an empty file if path is OK
    pFile = NedFileNew(NedEditor()->pFileList, szFilename);
    if(pFile)
    {
      NedLogPrintfEx(NEDLOG_EDITOR, "  created %s\n", NedFilePath(pFile));

      // if no lines in file, set CRLF and tabSize and hardTabs by language
      NedSettingsSetFileCrLf(pFile);

      // add file to recents
      NedEditorAddRecentFile(NedFilePath(pFile));

      // connect into editor file list
      NedEditorSetFileCur(pFile);
      if(!NedEditor()->pFileList)
        NedEditor()->pFileList = pFile;

      // SPECIAL CASE: May need to load the NedManual.md pseudo file
      NedSettingsLoadIfNedManual(pFile);

      // adjust the cursor position in this file
      NedPointInit(&pFile->sCursor, pFile->pBufHead);
      NedPointFileGotoPos(&pFile->sCursor, pos);

      if(NedLogMaskGet() & NEDLOG_EDITOR)
        NedFileLogShow(pFile, LOG_MORE);
    }
  }
  if(!pFile)
    NedLogPrintfEx(NEDLOG_EDITOR, "  bad path\n");

  if(pFile)
    NedEditorSetWindowItems();

  return pFile;
}

/*!------------------------------------------------------------------------------------------------
  Is this file already open? Looks in editor filelist

  @ingroup  ned_edit
  @oaran    szFilename

  @return   pointer to file if open, NULL if not
*///-----------------------------------------------------------------------------------------------
sNedFile_t *NedEditorFileIsOpen(const char *szFilename)
{
  sNedFile_t *pFile       = NedEditor()->pFileList;
  sNedFile_t *pFoundFile  = NULL;

  while(pFile)
  {
    if(strcmp(szFilename, NedFilePath(pFile)) == 0)
    {
      pFoundFile = pFile;
      break;
    }
    pFile = pFile->pNext;
  }

  return pFoundFile;
}

/*!------------------------------------------------------------------------------------------------
  Remove file positions in the given file. Used when closing a file or replacing it's buffer with
  other content.

  @ingroup  ned_edit
  @param    pFile       file to close

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorRemoveFilePos(sNedFile_t *pFile)
{
  sNedEditor_t *pEditor = NedEditor();
  NedPosListRemoveAll(pEditor->hPosHistory, pFile);
  NedPosListRemoveAll(pEditor->hBookmarks, pFile);
}

/*!------------------------------------------------------------------------------------------------
  Close a file. This has many duties. Doesn't update screen.

  1. Doesn't care about dirty flag. Higher layer must save if it wants to
  2. Remove any points from bookmarks or position history for this file
  3. Updates which file is the current file (no longer the closed file)
  4. Close the file and free it's memory
  5. If closing the last file, create an "untitled" file so there is always 1 file open

  @ingroup  ned_edit
  @param    pFile       file to close

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorFileClose(sNedFile_t *pFile)
{
  sNedEditor_t *pEditor     = NedEditor();
  sNedFile_t   *pFileCurNew = NULL;

  // debugging
  NedLogPrintfEx(NEDLOG_EDITOR, "\nNedEditorFileClose(%s)\n", NedFilePath(pFile)); 
  NedLogPrintfEx(NEDLOG_EDITOR, "  pFileList %p, pFile %p\n", pEditor->pFileList, pFile);
  NedAssert(NedFileIsFile(pFile));

  // if this is the error file, indicate no error file
  if(pFile == pEditor->pFileErr)
    pEditor->pFileErr = NULL;

  // if deleting the current file, pick a new current file
  // next, preferred, if not previous, if no next or previous, then new TMP filename
  if(pFile == pEditor->pFileCur)
  {
    pFileCurNew = NedFileNext(pFile);
    if(pFileCurNew == NULL)
      pFileCurNew = NedFilePrev(pEditor->pFileList, pFile);
  }

  // remove any bookmarks or undos that refer to this file, etc...
  NedEditorRemoveFilePos(pFile);
  NedUndoFileRemove(NedEditorUndoList(), pFile);

  // remove from list. If at file is at head of list, there will be a new head
  pEditor->pFileList = NedFileFree(pEditor->pFileList, pFile);

  // if last file was closed (list now empty), make a temporary file
  if(pEditor->pFileList == NULL)
  {
    pEditor->pFileList = NedFileNew(NULL, NULL);
    NedEditorSetFileCur(pEditor->pFileList);
  }

  // picked next or previous file
  else if(pFileCurNew)
  {
    NedEditorSetFileCur(pFileCurNew);
  }

  NedEditorSetWindowItems();
}



/*!------------------------------------------------------------------------------------------------
  A file has been opened or closed. The Window menu must reflect this.

  @ingroup  ned_edit
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorSetWindowItems(void)
{
  unsigned      i;
  unsigned      fileCount = 0;
  const char  **aszFiles  = NULL;
  sNedFile_t   *pFile     = NULL;

  // no files in filelist, create one called "untitled"
  fileCount = NedEditorFileCount();
  NedAssert(fileCount);

  // create array of filename pointers
  aszFiles = malloc(fileCount * sizeof(char *));
  NedAssert(aszFiles);
  pFile = NedEditorFileList();
  for(i=0; i<fileCount && pFile; ++i)
  {
    // distinguish name-only vs needing partial path
    aszFiles[i] = NedFileNameNoDup(NedEditorFileList(), pFile);
    pFile = NedFileNext(pFile);
  }

  // set Window menu items to file list
  NedMenuMapSetWindowItems(fileCount, aszFiles);

  // done with pointers to filenames (copied to menu)
  free(aszFiles);
}

/*!------------------------------------------------------------------------------------------------
  Updates the status bar with the current editor data.

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorUpdateStatusBar(void)
{
  const sNedFile_t     *pFile   = NedEditorFileCur();
  const sNedEditor_t   *pEditor = NedEditor();
  nedStatOpts_t         opts    = NedStatusBarGetOpts();

  // update options based on editor state
  opts &= (~(NEDSTAT_OPT_UNSAVED | NEDSTAT_OPT_READONLY | NEDSTAT_OPT_SEARCH_MASK));

  if(pFile->fDirty)
    opts |= NEDSTAT_OPT_UNSAVED;
  if(pFile->fRdOnly)
    opts |= NEDSTAT_OPT_READONLY;
  opts |= pEditor->sSearch.options;

  // show the current data on the status bar
  NedStatusBarSetOpts(opts);
  NedStatusBarSetRowCol(pFile->curLine, pFile->curCol);
  NedStatusBarSetTab(pFile->fHardTabs, pFile->tabSize, pFile->fCrLf);
  NedStatusBarSetFile(NedFilePath(pFile));
  NedStatusBarShow();
}

/*!------------------------------------------------------------------------------------------------
  Get index of the current file (pFileCur)in the pFileList (0-n).

  @ingroup  ned_edit

  @return   0-n (index)
*///-----------------------------------------------------------------------------------------------
unsigned NedEditorFileGetIndex(void)
{
  sNedFile_t   *pFile = NedEditor()->pFileList;
  unsigned      index = 0;

  while(pFile)
  {
    if(pFile == NedEditor()->pFileCur)
      break;
    ++index;
    pFile = NedFileNext(pFile);
    NedAssertDbg(pFile);
  }
  return index;
}

/*!------------------------------------------------------------------------------------------------
  Get total # of open files.

  @ingroup  ned_edit

  @return   0-n
*///-----------------------------------------------------------------------------------------------
unsigned NedEditorFileCount(void)
{
  sNedFile_t   *pFile = NedEditor()->pFileList;
  unsigned count = 0;

  while(pFile)
  {
    ++count;
    pFile = pFile->pNext;
  }

  return count;
}

/*!------------------------------------------------------------------------------------------------
  Set the current file. Does nothing to the display. A higher layer must do that

  @ingroup  ned_edit
  @param    pFile          Pointer to a valid file in file list

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorSetFileCur(const sNedFile_t *pFile)
{
  NedEditor()->pFileCur = (sNedFile_t *)pFile;
  NedEditor()->pLang    = NedLangGet(NedFilePath(pFile));
}


/*!------------------------------------------------------------------------------------------------
  Given a point, find the file, and move set as the current file. Does NOT affect the
  file's cursor.

  @ingroup  ned_edit
  @param    pPoint          Pointer to a valid point.

  @return   pointer to file, or NULL if point isn't in file list
*///-----------------------------------------------------------------------------------------------
sNedFile_t * NedEditorFilePickByPoint(const sNedPoint_t *pPoint)
{
  sNedFile_t *pFile;

  pFile = NedEditorGetFileByPoint(pPoint);
  if(pFile)
    NedEditorSetFileCur(pFile);

  return pFile;
}

/*!------------------------------------------------------------------------------------------------
  Given a point, find the file

  @ingroup  ned_edit
  @param    pPoint          pointer to valid point

  @return   pFile if found, NULL if point not in the editor's file list
*///-----------------------------------------------------------------------------------------------
sNedFile_t * NedEditorGetFileByPoint(const sNedPoint_t *pPoint)
{
  sNedFile_t      *pFile    = NULL;
  sNedFile_t      *pThisFile;

  pThisFile = NedEditor()->pFileList;
  while(pThisFile)
  {
    if(NedBufIsSameChain(pThisFile->pBufHead, pPoint->pBuf))
    {
      pFile = pThisFile;
      break;
    }
    pThisFile = pThisFile->pNext;
  }

  return pFile;
}

/*!------------------------------------------------------------------------------------------------
  Return the undolist for the current editor

  @ingroup  ned_edit

  @return   UndoList handle
*///-----------------------------------------------------------------------------------------------
void *NedEditorUndoList(void)
{
  return NedEditor()->pUndoList;
}


/*!------------------------------------------------------------------------------------------------
  Choose which file to open by index. Doesn't change current file if invalid index. Redraws the
  entire screen around the new file.

  @ingroup  ned_edit
  @param    n           index (0-n) into file list. Must be in bounds.

  @return   pointer to file
*///-----------------------------------------------------------------------------------------------
sNedFile_t * NedEditorFilePick(unsigned n)
{
  sNedFile_t   *pFile = NedEditor()->pFileList;

  // get the file pointer by index
  while(n && pFile)
  {
    --n;
    pFile = NedFileNext(pFile);
  }
  NedAssert(pFile); // bad index

  NtmFileOpenReset();
  NedEditorSetFileCur(pFile);

  // indicate we need to redraw things
  NedEditorNeedRedraw();
  NedEditorUpdateSmart();
  return pFile;
}


/*!------------------------------------------------------------------------------------------------
  Get the error file (if any).

  @ingroup  ned_edit

  @return   pointer to file, or NULL
*///-----------------------------------------------------------------------------------------------
sNedFile_t * NedEditorFileErr(void)
{
  return NedEditor()->pFileErr;
}


/*!------------------------------------------------------------------------------------------------
  Get the current file.

  @ingroup  ned_edit

  @return   pointer to file
*///-----------------------------------------------------------------------------------------------
sNedFile_t * NedEditorFileCur(void)
{
  return NedEditor()->pFileCur;
}

/*!------------------------------------------------------------------------------------------------
  Get the editor's file list.

  @ingroup  ned_edit

  @return   pointer to file list
*///-----------------------------------------------------------------------------------------------
sNedFile_t * NedEditorFileList(void)
{
  return NedEditor()->pFileList;
}

/*!------------------------------------------------------------------------------------------------
  Get the cursor for the current file.

  @ingroup  ned_edit

  @return   1-n (index)
*///-----------------------------------------------------------------------------------------------
sNedPoint_t *NedEditorCursor(void)
{
  return &(NedEditorFileCur()->sCursor);
}


/*!------------------------------------------------------------------------------------------------
  Get the select cursor for the current file.

  @ingroup  ned_edit

  @return   1-n (index)
*///-----------------------------------------------------------------------------------------------
sNedPoint_t *NedEditorSelect(void)
{
  return &(NedEditorFileCur()->sSelect);
}

/*!------------------------------------------------------------------------------------------------
  Get the tab-aware column for this point. For example "\tabc", if point is on "a", the column
  would be 8 if the pFile->tabSize is 8.

  @ingroup  ned_edit
  @param    pPoint      Some point in the current file

  @return   tabbed column 1-n
*///-----------------------------------------------------------------------------------------------
long NedEditorColGet(const sNedPoint_t *pPoint)
{
  return NedFileColGet(NedEditorFileCur(), pPoint);
}

/*!------------------------------------------------------------------------------------------------
  Set the point on the actual character closest to the preferred column. If the column is too long
  for the line, then it puts the column on the \n (or EOF) char of the line. Puts the cursor at the
  beginning column for a tab

  @ingroup  ned_editor
  @param    pPoint      point in a file
  @param    prefCol     preferred column on that line, 1-n

  @return   Actual column (1-n)
*///-----------------------------------------------------------------------------------------------
long NedEditorColSet(sNedPoint_t *pPoint, long prefCol)
{
  // prefCol should always be positive
  if(prefCol < 1)
    NedLogPrintf("NedEditorColSet col %ld\n", prefCol);

  return NedFileColSet(NedEditorFileCur(), pPoint, prefCol);
}

/*!------------------------------------------------------------------------------------------------
  Similar to NedPointLineGet(), but understands tabs. Works on current file

  @ingroup  ned_edit
  @param    pPoint      a point in the current file
  @param    pCol        returned column (1-n), understands tabSize

  @return   line this point is on (1-n)
*///-----------------------------------------------------------------------------------------------
long NedEditorLineGet(const sNedPoint_t *pPoint, long *pCol)
{
  return NedFileLineGet(NedEditorFileCur(), pPoint, pCol);
}

/*-------------------------------------------------------------------------------------------------
  Go to a point, which may be in another file. Adjust other fields as necessary. Prepares to
  redraw on screen

  @ingroup  ned_cmd
  @returns  none
-------------------------------------------------------------------------------------------------*/
void NedEditorGotoPoint(const sNedPoint_t *pPoint)
{
  sNedFile_t    *pFile;
  long           curCol;

  pFile = NedEditorFilePickByPoint(pPoint);
  if(pFile)
    NedLogPrintfEx(NEDLOG_EDITOR, "NedEditorGotoPoint(file = %s)\n", NedFilePath(pFile));
  NedAssertDbg(pFile);
  if(pFile)
  {
    // recalculate row/col, topLine, etc...
    NedEditorSetFileCur(pFile);
    pFile->sCursor      = *pPoint;
    pFile->curLine      = NedPointLineGet(NedEditorCursor(), &curCol);
    pFile->curCol       = curCol;
    pFile->prefCol      = curCol;
    NedEditorBuildAroundCursor();
  }
}

/*!------------------------------------------------------------------------------------------------
  Add the any file/pos pair to the PosHistory.

  @param    pPos      valid file/pos pair
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorAddPosHistoryEx(const sNedPos_t *pPos)
{
  NedListAdd(NedEditor()->hPosHistory, pPos, NEDLIST_DEF_SIZE);
}

/*!------------------------------------------------------------------------------------------------
  Add the current cursor position to the PosHistory.

  @param    pPos      NULL or a valid file/pos pair

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorAddPosHistory(void)
{
  sNedPos_t   sPos;

  sPos.pFile  = NedEditorFileCur();
  sPos.pos    = NedPointPos(NedEditorCursor());
  NedEditorAddPosHistoryEx(&sPos);
}

/*!------------------------------------------------------------------------------------------------
  Deleting text from this file. Make sure to move/delete bookmarks and point history as needed

  @ingroup  ned_editor

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorPosListAdjustDel(const sNedPoint_t *pTop, const sNedPoint_t *pBot)
{
  NedPosListAdjustDel(NedEditor()->hPosHistory, pTop, pBot);
  NedPosListAdjustDel(NedEditor()->hBookmarks, pTop, pBot);
}

/*!------------------------------------------------------------------------------------------------
  Inserting text from this file. Make sure to move bookmarks and point history as needed

  @ingroup  ned_editor

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorPosListAdjustIns(const sNedPoint_t *pPoint, long len)
{
  NedPosListAdjustIns(NedEditor()->hPosHistory, pPoint, len);
  NedPosListAdjustIns(NedEditor()->hBookmarks, pPoint, len);
}

/*!------------------------------------------------------------------------------------------------
  Add the given file to the recent files list.

  @ingroup  ned_editor

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorAddRecentFile(const char *szFilename)
{
  NedListAdd(NedEditor()->hRecentFiles, szFilename, strlen(szFilename) + 1);
}

/*!------------------------------------------------------------------------------------------------
  Looks for the project file. If path is given, uses that. If not, searches from current folder to
  root. If still not found, then uses curernt "." folder.

  Possible szProjArg: NULL, "file.json", ".", "/path/to/file.json", "~/folder"

  1. NULL: find "ned.json" in the current folder up to the root. If not found, uses "~/ned.json"
  2. "file.json": Same as NULL, but for a different filename
  3. ".": "ned.json" in current folder, e.g. "/Users/drewg/git/myproject/ned.json"
  4. "/path/to/file.json": just uses this as-is
  5. "~/folder": expands to $HOME folder: "/Users/drewg/folder/ned.json"

  @ingroup  ned_editor
  @param    szProjPath  sz[PATH_MAX] str to receive full path, e.g. "/Users/drewg/ned.json"
  @param    szProjArg   NULL, "file.json", ".", "/path/to/file.json", "~/folder"

  @returns full project path and NEDERR_NONE if found, NEDERR_NOTFOUND if path OK, but no project
  file, NEDERR_BADPATH if failed
*///-----------------------------------------------------------------------------------------------
nedErr_t NedEditorFindProjectFile(char *szProjPath, const char *szProjArg)
{
  nedErr_t          err     = NEDERR_NONE;   // bad path if FALSE
  const char       *szDir;
  sNedFileInfo_t    sInfo;
  
  // no project name specified, use default "ned.json"
  *szProjPath = '\0';
  if(szProjArg == NULL)
    szProjArg = m_szDefProjName;

  // does file/folder exist?
  if(NedFileInfoGet(&sInfo, szProjArg))
  {
    strcpy(szProjPath, sInfo.szFullPath);
    if(sInfo.fIsDir)
    {
      if(!NedStrAppendName(szProjPath, m_szDefProjName))
        err = NEDERR_BADPATH;
    }
  }

  // file/folder does NOT exist
  else
  {
    // if there was a folder component, see if that folder exists
    szDir = NedStrDirOnlyHome(szProjArg);
    if(*szDir)
    {
      if(!NedFileInfoGet(&sInfo, szDir))
        err = NEDERR_BADPATH;
      else
      {
        // name too long?
        strcpy(szProjPath, sInfo.szFullPath);
        if(!NedStrAppendName(szProjPath, NedStrNameOnly(szProjArg)))
          err = NEDERR_BADPATH;   
      }
    }

    // filename only (e.g. "myproj.json")
    // look in current folder all the way to the root, if not found, assume $HOME folder
    else
    {
      if(!NedFileFindInFolder(szProjPath, PATH_MAX-1, szProjArg, NULL))
      {
        NedLibFileFullPath(szProjPath, ".");
        if(!NedStrAppendName(szProjPath, szProjArg))
          err = NEDERR_BADPATH;
      }
    }
  }

  // if file doesn't exist, then not found
  if(err == NEDERR_NONE && !NedFileExists(szProjPath))
    err = NEDERR_NOTFOUND;

  return err;
}

/*-------------------------------------------------------------------------------------------------
  Loads the project file if it exists and is of sufficent version.

  If returns TRUE, the absolute path project file is filed into pEditor->szProjName.

  If filename only is specified, will look in current director up to root, then in $(HOME) folder
  for file. If still no file found, assumes ~/file.

  Parameters:
  szProjPath  Full path to project file, e.g. "/Users/drewg/ned.json"
  fLoadFiles  TRUE if file list should be loaded from project file, FALSE if not

  Returns nedErr code (e.g. NEDERR_BADPATH)
-------------------------------------------------------------------------------------------------*/
nedErr_t NedEditorLoadProject(const char *szProjPath, bool_t fLoadFiles)
{
  sNedEditor_t     *pEditor = NedEditor();
  nedErr_t          err     = NEDERR_NONE;   // bad path if FALSE
  bool_t            fExists = FALSE;
  
  // validate parameters
  NedAssert(NedEditorIsEditor(pEditor));

  // Get project path. Project file may not exist (yet) until save
  fExists = NedFileExists(szProjPath);
  NedLogPrintfEx(NEDLOG_EDITOR, "  NedEditorLoadProject(%s, fLoadFiles %u) => exists %u)\n", szProjPath, fLoadFiles, fExists);

  // load the project file if it exists (NEDERR_BADPARM if invalid .json or wrong version)
  if(fExists)
  {
    if(NedSettingsLoad(pEditor, szProjPath, fLoadFiles) != NEDSET_ERR_NONE)
      err = NEDERR_BADPARM;
  }

  return err;
}

/*!------------------------------------------------------------------------------------------------
  @ingroup  ned_edit

  @return   Return the editor view.
*///-----------------------------------------------------------------------------------------------
void NedEditorShowSelectedChars(void)
{
  long diff;
  diff = NedPointDifference(NedEditorSelect(), NedEditorCursor());
  if(diff < 0)
    diff = -diff;
  NedMsgPrintf("Selected %ld chars", diff);
}

/*!------------------------------------------------------------------------------------------------
  @ingroup  ned_edit

  @return   Return the editor project path
*///-----------------------------------------------------------------------------------------------
const char * NedEditorProjPath(void)
{
  return NedEditor()->szProjPath;
}

/*!------------------------------------------------------------------------------------------------
  @ingroup  ned_edit

  @return   Return the editor log path
*///-----------------------------------------------------------------------------------------------
const char * NedEditorLogPath(void)
{
  return NedEditor()->szLogPath;
}

/*!------------------------------------------------------------------------------------------------
  @ingroup  ned_edit

  @return   Return the editor view handle
*///-----------------------------------------------------------------------------------------------
hNedView_t NedEditorView(void)
{
  return NedEditor()->hView;
}

/*!------------------------------------------------------------------------------------------------
  @ingroup  ned_edit

  @return   Return the editor menu handle
*///-----------------------------------------------------------------------------------------------
void * NedEditorMenu(void)
{
  return NedEditor()->hMenu;
}

/*!------------------------------------------------------------------------------------------------
  @ingroup  ned_edit

  @return   screen columns from view
*///-----------------------------------------------------------------------------------------------
unsigned NedEditorScreenCols(void)
{
  return NedViewScreenCols(NedEditorView());
}

/*!------------------------------------------------------------------------------------------------
  @ingroup  ned_edit

  @return   screen rows from view
*///-----------------------------------------------------------------------------------------------
unsigned NedEditorScreenRows(void)
{
  return NedViewScreenRows(NedEditorView());
}

/*!------------------------------------------------------------------------------------------------ 
  @ingroup  ned_edit

  @return   none
*///-----------------------------------------------------------------------------------------------
unsigned NedEditorEditRows(void)
{
  return NedViewEditRows(NedEditorView());
}

/*!------------------------------------------------------------------------------------------------ 
  Invalidate the current line (converts to row coordinates for the view)

  @ingroup  ned_edit

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorInvalidateLine(void)
{
  sNedFile_t    *pFile = NedEditorFileCur();
  NedViewInvalidateRow(NedEditorView(), pFile->curLine - pFile->topLine);
  NedEditor()->fDirty = TRUE;
}

/*!------------------------------------------------------------------------------------------------ 
  Invalidate the lines from the given one through end of editRows (converts to row coordinates for
  the view)

  @ingroup  ned_edit

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorInvalidateLines(unsigned line)
{
  sNedFile_t    *pFile = NedEditorFileCur();
  unsigned       row;

  for(row = (unsigned)(pFile->curLine - pFile->topLine); row < NedEditorEditRows(); ++row)
    NedViewInvalidateRow(NedEditorView(), row);
  NedEditor()->fDirty = TRUE;
}

/*!------------------------------------------------------------------------------------------------ 
  Invalidate the cursor line.

  @ingroup  ned_edit
  @param

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorInvalidateSelLine(void)
{
  sNedFile_t    *pFile = NedEditorFileCur();
  if(pFile->fInSelect)
  {
    NedEditorInvalidateLine();
    NedEditorShowSelectedChars();
  }
}

/*!------------------------------------------------------------------------------------------------ 
  Invalidate (so they get redrawn) all on-screen rows between the selection point and the cursor.
  Used both for extending selection and reducing or removing selection.

  @ingroup  ned_edit
  @param

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorInvalidateSelected(void)
{
  sNedFile_t  *pFile = NedEditorFileCur();

  if(pFile->fInSelect)
  {
    NedEditorShowSelectedChars();
    NedViewInvalidateSelected(NedEditorView(), pFile);
    NedEditor()->fDirty = TRUE;
  }
}

/*!------------------------------------------------------------------------------------------------
  Print select status and points to the log

  @ingroup  ned_edit
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorLogSelect(const char *szPrompt)
{
  sNedFile_t *pFile = NedEditorFileCur();
  long        selLine;
  long        selCol;

  selLine = NedPointLineGet(&pFile->sSelect, &selCol);
  NedLogPrintfEx(NEDLOG_EDITOR, "%s: fInSelect %u, curLine %u, curCol %u, selLine %u, selCol %u\n",
    szPrompt, pFile->fInSelect, pFile->curLine, pFile->curCol, selLine, selCol);
}

/*!------------------------------------------------------------------------------------------------
  Pick the edit window

  @ingroup  ned_edit
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorPickEditWindow(void)
{
  NedViewPickView(NedEditorView());
}

/*!------------------------------------------------------------------------------------------------
  Beep

  @ingroup  ned_edit
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorBeep(void)
{
  printf("\007"); fflush(stdout);
}

/*!------------------------------------------------------------------------------------------------ 
  Does anything needed common to commands BEFORE the command has occurred.

  @ingroup  ned_edit
  @param    pfnCmd      pointer to valid command

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorPreCmdCheck(pfnNedCmd_t pfnCmd)
{
  nedCmdFlags_t   flags;

  // clear the message line so outdated messages don't hang around
  NedMsgClear();

  if(NedLogMaskGet() & NEDLOG_CMD)
    NedLogPrintf("%s()\n", NedCmdGetName(pfnCmd));

  // Edit commands automatically turn off selection
  flags = NedCmdFlags(pfnCmd);
  if(NedEditorFileCur()->fInSelect && (flags & NEDCMD_FLAG_EDIT) && !(flags & NEDCMD_FLAG_SELECT))
  {
    NedEditorLogSelect("Auto select shutoff");
    NedEditorInvalidateSelected();
    NedEditorFileCur()->fInSelect = FALSE;
  }

  // When cursoring or doing other things, no longer editing, so the Undo is complete
  if(!(flags & NEDCMD_FLAG_EDIT))
  {
    NedUndoEditDone(NedEditorUndoList());
  }

  // for cut line/word, keep appending to clipboard, otherwise don't
  if(NedEditor()->fClipAppend && !(flags & NEDCMD_FLAG_CUT))
  {
    NedEditorLogSelect("Auto no append");
    NedEditor()->fClipAppend = FALSE;
  }
}

/*!------------------------------------------------------------------------------------------------
  Does anything needed common to commands AFTER the command has occurred.

  @ingroup  ned_edit
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedEditorPostCmdCheck(pfnNedCmd_t pfnCmd)
{
  NedEditor()->pfnLastCmd = pfnCmd;
  if(!NedEditor()->fInMenu)
    NedEditorUpdateSmart();
}

/*!------------------------------------------------------------------------------------------------
  Asserting. Try to save unsaved files to not lose changes. Show stack trace if not already
  shown by signal.

  @ingroup  ned_edit
  @return   error code
*///-----------------------------------------------------------------------------------------------
int NedEditorError(const char *szExpr, const char *szFile, const char *szFunc, unsigned line)
{
  sNedEditor_t  *pEditor = NedEditor();
  sNedFile_t    *pFile = pEditor->pFileList;

  // clear screen
  AnsiSetAttr(NEDATTR_RESET);
  AnsiClearScreen();

  // NedEditorLogShow(pEditor, TRUE);
  NedLogPrintf("\n\nNedAssert: (%s), file: %s, func: %s(), line: %u\n\n", szExpr, szFile, szFunc, line);

  // show stack trace if not already shown
  if(!m_fInSignal)
    NedSigStackTrace();

  // save any unsaved files
  while(pFile)
  {
    if(pFile->fDirty)
      NedFileSave(pFile);
    pFile = pFile->pNext;
  }

  // display assert last
  printf("\nNedAssert: (%s), file: %s, func: %s(), line: %u\n", szExpr, szFile, szFunc, line);
  return 1;
}

/*!------------------------------------------------------------------------------------------------
  Asserting. Try to save unsaved files to not lose changes, and display state of editor to log

  @return   none
*///-----------------------------------------------------------------------------------------------
int NedEditorSignal(int sig)
{
  NedEditorError("signal", __FILE__, __func__, __LINE__);
  m_fInSignal = TRUE;
  return 1;
}

/*!------------------------------------------------------------------------------------------------
  Helper function for pbpaste. Gets the data from pbpaste into an allocated string or existing
  string. Note: if passing NULL for pDst, caller must free the memory with free().

  @param      destination buffer, or NULL if this function should allocate
  @param      pMaxLen
  @return     pointer to buffer or NULL if no data (or failed to alloc)
*///-----------------------------------------------------------------------------------------------
nedChar_t * NedEditorPbPaste(nedChar_t *pDst, long *pInsertLen)
{
  sNedFile_t   *pClip;
  nedChar_t    *pData         = NULL;
  long          insertLen     = 0;
  const char   m_szTmpFile[]  = "ned.tmp";
  const char   m_szSystem[]   = "pbpaste > ned.tmp";

  // set ned.tmp file to contents of pbpaste
  if(system(m_szSystem) >= 0)
  {
    pClip = NedFileNew(NULL, m_szTmpFile);
    if(pClip)
    {
      insertLen = NedPointFileSize(&pClip->sCursor);
      *pInsertLen = insertLen;
      if(insertLen > 0)
      {
        if(pDst)
          pData = pDst;
        else
          pData = malloc(insertLen);
        if(pData)
          NedPointMemGet(pData, &pClip->sCursor, insertLen);
      }
      NedFileFree(NULL, pClip);
      remove(m_szTmpFile);
    }
  }

  return pData;
}

/*!------------------------------------------------------------------------------------------------
  Return language for current file. Will never be NULL

  @return   Return language for current file
*///-----------------------------------------------------------------------------------------------
const sNedLanguage_t *NedEditorLang(void)
{
  AnsiSetAttr(NEDATTR_RESET);
  NedAssert(NedEditor()->pLang);
  return NedEditor()->pLang;
}

/*!------------------------------------------------------------------------------------------------
  Return tabsize for current file

  @return   Return tabsize for current file
*///-----------------------------------------------------------------------------------------------
unsigned NedEditorTabSize(void)
{
  return NedEditorFileCur()->tabSize;
}

/*-------------------------------------------------------------------------------------------------
  Get the "IsFuncLine" per the language of the given file

  @ingroup  ned_cmd
  @returns  none
-------------------------------------------------------------------------------------------------*/
pfnIsFuncLine_t NedEditorGetLangFuncLine(const char *szFilePath)
{
  const sNedLanguage_t *pLang = NedLangGet(szFilePath);
  pfnIsFuncLine_t       pfnIsFuncLine = NedPointIsFnC;    // default is C/C++/Rust

  if(strcmp(pLang->szLang, SZ_LANG_PYTHON) == 0)
    pfnIsFuncLine = NedPointIsFnPython;
  else if(strcmp(pLang->szLang, SZ_LANG_JAVASCRIPT) == 0)
    pfnIsFuncLine = NedPointIsFnJavascript;
  else if(strcmp(pLang->szLang, SZ_LANG_HTML) == 0)
    pfnIsFuncLine = NedPointIsFnHtml;
  else if((strcmp(pLang->szLang, SZ_LANG_TEXT) == 0) ||
          (strcmp(pLang->szLang, SZ_LANG_SHELL) == 0) ||
          (strcmp(pLang->szLang, SZ_LANG_MAKEFILE) == 0))
  {
    pfnIsFuncLine = NedPointIsFnText;
  }

  return pfnIsFuncLine;
}

