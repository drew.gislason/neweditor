/* ************************************************************************************************
  @file NedWin.h

  @brief  Ned Window framework API

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

************************************************************************************************ */
#ifndef NED_WIN_H
#define NED_WIN_H
#include "Ned.h"
#include "NedAnsi.h"
#include "NedLog.h"

typedef int winPos_t;   // window position may be negative

typedef enum {
  NEDWIN_DIR_UP,
  NEDWIN_DIR_DOWN,
} nedWinDir_t;    // for NedWinScrolXxx() functions

#define NEDWIN_MAX_WINDOWS     999    // debugging, mostly to prevent infinite loops (causes assert)
#define NEDWIN_MAX_COLS        NEDCFG_MAX_WIDTH   // affects max length of NedWinPrintf()
#define NEDWIN_MAX_ROWS        NEDCFG_MAX_WIDTH

#define NEDWIN_DUMP_LONG       0
#define NEDWIN_DUMP_SHORT      1

#define NEDWIN_MAX_POS         (INT_MAX - NEDWIN_MAX_COLS - 2)
#define NEDWIN_MIN_POS         (INT_MIN + (NEDWIN_MAX_COLS + 2))

#define NEDWIN_LINE_NOPICK     UINT_MAX

// screen (terminal) management
bool_t          NedWinInit          (void);
bool_t          NedWinInitEx        (winPos_t rows, winPos_t cols);
bool_t          NedWinAdjustScreen  (winPos_t rows, winPos_t cols);
void            NedWinGetScreenSize (winPos_t *pRows, winPos_t *pCols);
void            NedWinExit          (void);

// calls that actually write to the screen
void            NedWinClearScreen   (nedAttr_t attr);
void            NedWinFlush         (void);
void            NedWinFlushAll      (void);
void            NedWinFlushCursor   (void);

// window management
void           *NedWinNew           (winPos_t top, winPos_t left, winPos_t rows, winPos_t cols);
void           *NedWinNewEx         (winPos_t top, winPos_t left, winPos_t rows, winPos_t cols,
                                     nedAttr_t winAttr, bool_t fFramed, nedAttr_t frameAttr);
bool_t          NedWinIsWin         (const void *pWin);
void            NedWinHide          (void);
void            NedWinUnhide        (void);
void            NedWinClose         (void *pWin);
void            NedWinCloseAll      (void);
void           *NedWinPick          (void *pWin);
void           *NedWinGetPicked     (void);
void           *NedWinPickBackdrop  (void);
void           *NedWinGetBackdrop   (void);

// operates on current window
void            NedWinFront         (void);
void            NedWinMove          (winPos_t top, winPos_t left);
bool_t          NedWinResize        (winPos_t rows, winPos_t cols);
void            NedWinGetPos        (winPos_t *pTop, winPos_t *pLeft);
winPos_t        NedWinGetTop        (void);
winPos_t        NedWinGetLeft       (void);
void            NedWinGetSize       (winPos_t *pRows, winPos_t *pCols);
winPos_t        NedWinGetRows       (void);
winPos_t        NedWinGetCols       (void);
void            NedWinGoto          (winPos_t row, winPos_t col);
void            NedWinGetCursor     (winPos_t *pRow, winPos_t *pCol);

unsigned        NedWinPuts          (const char *sz);
void            NedWinPutc          (char c);
unsigned        NedWinVPrintf       (const char *szFormat, va_list arglist);
unsigned        NedWinPrintf        (const char *szFormat, ...);

void            NedWinClearAll      (void);
void            NedWinClearPart     (winPos_t top, winPos_t left, winPos_t rows, winPos_t cols);
void            NedWinClearEol      (void);
void            NedWinFillAll       (char c);
void            NedWinFillStr       (const char *sz);
void            NedWinFillPart      (winPos_t top, winPos_t left, winPos_t rows, winPos_t cols, char c);

nedAttr_t       NedWinGetAttr       (void);
void            NedWinSetAttr       (nedAttr_t attr);
void            NedWinAttrFillAll   (nedAttr_t attr);
void            NedWinAttrFillPart  (winPos_t top, winPos_t left, winPos_t rows, winPos_t cols, nedAttr_t attr);
void            NedWinScrollAll     (nedWinDir_t dir);
void            NedWinScrollPart    (winPos_t top, winPos_t left, winPos_t rows, winPos_t cols, nedWinDir_t dir);

void            NedWinFrameSet      (bool_t fFramed);
bool_t          NedWinIsFramed      (void);
void            NedWinFrameLinePick (winPos_t row);
nedAttr_t       NedWinFrameGetAttr  (void);
void            NedWinFrameSetAttr  (nedAttr_t attr);

// debugging functions
void            NedWinLogDumpWin    (const void *pWin);
void            NedWinLogDumpWinHdr (const void *pWin);
void            NedWinLogDumpAll    (void);
void            NedWinLogDumpCanvas (void);
const char *    NedWinLogName       (const void *pWin);
void            NedWinLogShowChain  (void);
winPos_t        NedWinScreenGets    (char *pszDst, winPos_t row, winPos_t left, winPos_t cols);

#endif // NED_WIN_H
