/*!************************************************************************************************
  @file NedEditor.h

  @brief  Private file for those objects which need access to Editor state.

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  This is a support file for the editor commands. Includes many common functions, getters and
  setters. See also NedEditUtils.c

*///***********************************************************************************************
#ifndef NED_EDITOR_H
#define NED_EDITOR_H

#include "Ned.h"
#include "NedBuffer.h"
#include "NedCmd.h"
#include "NedFile.h"
#include "NedKey.h"
#include "NedKeyMap.h"
#include "NedLanguage.h"
#include "NedLog.h"
#include "NedList.h"
#include "NedMenu.h"
#include "NedMsg.h"
#include "NedPoint.h"
#include "NedPosList.h"
#include "NedTheme.h"
#include "NedView.h"


#define NEDEDITOR_SANCHK    10101U            // sanity check
#define NEDEDITOR_PROJNAME  "ned.json"        // default project name
#define NEDTHEME_FACTORY    NEDTHEME_NATURAL
#define NEDEDITOR_SCROLL_AMOUNT    5          // for veritical and horizontal scrolling

typedef enum
{
  NEDEDITOR_DETAIL_NONE,
  NEDEDITOR_DETAIL_FILE_LIST,
  NEDEDITOR_DETAIL_ALL_LIST,
  NEDEDITOR_DETAIL_FILE_CONTENTS
} nedEditorDetail_t;

extern const char   szNedCmd_HelpAbout[];
extern const char  *szNedMachineName;

typedef struct
{
  unsigned      options;
  char          szFind      [NEDCFG_MAX_WIDTH];
  char          szReplace   [NEDCFG_MAX_WIDTH];
  char          szPaste     [NEDCFG_MAX_WIDTH]; // Ctrl-V
  char          szPbPaste   [NEDCFG_MAX_WIDTH]; // Alt-V
} sNedSearch_t;

typedef struct
{
  // non-settings editor fields
  unsigned          sanchk;           // sanity check
  bool_t            fSaveProj;        // save the project
  char              szProjPath[PATH_MAX]; // current project full path (e.g. /Users/drewg/ned.json)
  char              szLogPath[PATH_MAX];  // logging full path (e.g. /Users/drewg/ned.log)
  void             *hMenu;            // menu system
  void             *hView;            // view
  bool_t            fRedrawAll;       // redraw menu, editor text, status bar and message line
  bool_t            fDirty;           // draw only dirty rows in editor window
  nedKey_t          c;                // last key pressed (used for insert self)
  sNedFile_t       *pFileErr;         // ptr to open file. Cursor is used for next/prev, NULL if no ErrFile
  sNedFile_t       *pClipboard;       // the clipboard
  bool_t            fClipAppend;      // appending to clipboard
  sNedSearch_t      sSearch;          // search criteria
  bool_t            fInMenu;          // are we currently in the menu?
  void             *pUndoList;        // handle to undo
  pfnNedCmd_t       pfnLastCmd;       // previous command before this one

  // settings fields (saved in nedproj.json)
  sNedFile_t       *pFileList;        // all open files (some which may not have been saved)
  sNedFile_t       *pFileCur;         // current file (must be in pFileList above)
  hNedList_t        hPosHistory;      // (sNedPos_t) position history across files
  hNedList_t        hBookmarks;       // (sNedPos_t) bookmarks across files
  hNedList_t        hRecentFiles;     // (char *) recently opened files
  hNedList_t        hProjectFolders;  // (char *) folder for this project
  unsigned          themeIndex;       // current theme
  const sNedLanguage_t *pLang;        // current language, e.g. .c (C/C++), .py (Python)
} sNedEditor_t;

sNedEditor_t *NedEditor                   (void);
bool_t        NedEditorIsEditor           (const sNedEditor_t *pEditor);
void          NedEditorLogShow            (sNedEditor_t *pEditor, nedEditorDetail_t detail);
int           NedEditorError              (const char *szExpr, const char *szFile, const char *szFunc, unsigned line);
void          NedEditorInit               (sNedEditor_t *pEditor);
void          NedEditorClear              (sNedEditor_t *pEditor);
const char   *NedEditorProjPath           (void);
const char   *NedEditorLogPath            (void);
sNedFile_t   *NedEditorFileNew            (const char *szFilename, long pos);
sNedFile_t   *NedEditorFileIsOpen         (const char *szFilename);
sNedFile_t   *NedEditorClipboard          (bool_t fClear);
void          NedEditorListRemove         (hNedList_t hList, sNedFile_t *pFile);
void          NedEditorPosListAdjustDel   (const sNedPoint_t *pTop, const sNedPoint_t *pBot);
void          NedEditorPosListAdjustIns   (const sNedPoint_t *pPoint, long len);
void          NedEditorRemoveFilePos      (sNedFile_t *pFile);
void          NedEditorFileClose          (sNedFile_t *pFile);
void          NedEditorFileNone           (void);
unsigned      NedEditorFileCount          (void);
unsigned      NedEditorFileGetIndex       (void);
void          NedEditorSetFileCur         (const sNedFile_t *pFile);
sNedFile_t   *NedEditorFilePickByPoint    (const sNedPoint_t *pPoint);
sNedFile_t   *NedEditorFilePick           (unsigned n);
sNedFile_t   *NedEditorFileCur            (void);
sNedFile_t   *NedEditorFileList           (void);
sNedFile_t   *NedEditorFileErr            (void);

// model which helps view understand tabs
long          NedEditorLineGet            (const sNedPoint_t *pPoint, long *pCol);
long          NedEditorColGet             (const sNedPoint_t *pPoint);
long          NedEditorColSet             (sNedPoint_t *pPoint, long col);

bool_t        NedEditorAdjustFileFields   (sNedFile_t *pFile);
bool_t        NedEditorAdjustCurFileFields(void);
void *        NedEditorView               (void);
void *        NedEditorMenu               (void);

sNedPoint_t  *NedEditorCursor             (void);
sNedPoint_t  *NedEditorSelect             (void);
nedTheme_t   *NedEditorTheme              (void);
const sNedLanguage_t *NedEditorLang       (void);
unsigned      NedEditorTabSize            (void);
void          NedEditorSetTheme           (unsigned themeIndex);
void          NedEditorGotoPoint          (const sNedPoint_t *pPoint);
void          NedEditorUpdateStatusBar    (void);
void          NedEditorPickEditWindow     (void);
void          NedEditorSetWindowItems     (void);
void          NedEditorSetSubMenus        (void);
void          NedEditorAddPosHistoryEx    (const sNedPos_t *pPos);
void          NedEditorAddPosHistory      (void);
void          NedEditorAddRecentFile      (const char *szFilename);
nedErr_t      NedEditorFindProjectFile    (char *szProjPath, const char *szProjArg);
nedErr_t      NedEditorLoadProject        (const char *szProjName, bool_t fLoadFiles);
unsigned      NedEditorScreenCols         (void);
unsigned      NedEditorScreenRows         (void);
unsigned      NedEditorEditRows           (void);
void          NedEditorInvalidateLine     (void);
void          NedEditorInvalidateLines    (unsigned line);
void          NedEditorNeedRedraw         (void);
void          NedEditorRedraw             (void);
void          NedEditorRedrawScreen       (void);
void          NedEditorFlushCursor        (void);
void          NedEditorUpdateSmart        (void);
void          NedEditorBuildAroundCursor  (void);
void          NedEditorInvalidateSelLine  (void);
void          NedEditorInvalidateSelected (void);
void          NedEditorShowSelectedChars  (void);
void          NedEditorBeep               (void);
void          NtmFileOpenReset            (void);

nedChar_t *   NedEditorPbPaste            (nedChar_t *pDst, long *pMaxLen);
pfnIsFuncLine_t NedEditorGetLangFuncLine  (const char *szFilePath);

void         *NedEditorUndoList           (void);
sNedFile_t   *NedEditorGetFileByPoint     (const sNedPoint_t *pPoint);

void          NedEditorPreCmdCheck        (pfnNedCmd_t pfnCmd);
void          NedEditorPostCmdCheck       (pfnNedCmd_t pfnCmd);

// debugging functions
void          NedEditorLogSelect          (const char *szPrompt);
int           NedEditorSignal             (int sig);

#endif // NED_EDITOR_H
