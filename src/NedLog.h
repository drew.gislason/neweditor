/*!************************************************************************************************
  @file NedLog.h

  @brief  Ned Log API

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_log Ned Log - An API for logging to a file. Defaults to file "ned.log"

  The Ned logging system supports a single log file. It is not necessary to open a log file

*///***********************************************************************************************
#ifndef NED_LOG_H
#define NED_LOG_H

#include "Ned.h"

#define SZ_NED_LOG_NAME   "ned.log"
extern const char szNedLogDefaultName[];

typedef enum
{
  LOG_MIN,      // guaranteed to fit in single line
  LOG_MORE,     // may be multiple lines
  LOG_MAX       // could be many lines
} logDetail_t;

typedef uint32_t nedLogMask_t;  // bit mask contents is up to higher layer

bool_t        NedLogFileOpen  (const char *szFilePath);
bool_t        NedLogFileAppend(const char *szFilePath);
void          NedLogFileClose (void);
int           NedLogPrintf    (const char *szFormat, ...);
int           NedLogPrintfEx  (nedLogMask_t mask, const char *szFormat, ...);
size_t        NedLogDump      (const void *pData, unsigned len, unsigned linelen, unsigned indent);
size_t        NedLogDumpEx    (nedLogMask_t mask, const void *pData, unsigned len, unsigned linelen, unsigned indent);
nedLogMask_t  NedLogMaskSet   (nedLogMask_t mask);
nedLogMask_t  NedLogMaskGet   (void);
size_t        NedLogSizeGet   (void);
void          NedLogSizeReset (void);

#endif // NED_LOG_H
