/*!************************************************************************************************
  @file NedCmdBookmarks.c

  @brief  Ned Command Bookmark function implementation

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  Includes CmdBookmarkXxx

*///***********************************************************************************************
#include "NedEditor.h"


/*-------------------------------------------------------------------------------------------------
  Is this position on a bookmark?

  @ingroup  ned_cmd
  @returns  none
-------------------------------------------------------------------------------------------------*/
bool_t NcbIsOnBookmark(const sNedPos_t *pPos)
{
  return (NedListFind(NedEditor()->hBookmarks, pPos) != NULL) ? TRUE : FALSE;
}


/*!------------------------------------------------------------------------------------------------
  Go up one line. Preserve column.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdBookmarkClearAll(void)
{
  NedListClear(NedEditor()->hBookmarks);
  NedMsgPrintf("Bookmarks cleared");
}


/*!------------------------------------------------------------------------------------------------
  Go up one line. Preserve column.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdBookmarkNext(void)
{
  sNedPoint_t  sPoint;
  sNedPos_t   *pPos;

  pPos = NedListNext(NedEditor()->hBookmarks);
  if(pPos)
  {
    NedEditorAddPosHistory();
    NedPointInit(&sPoint, pPos->pFile->pBufHead);
    NedPointFileGotoPos(&sPoint, pPos->pos);
    NedEditorGotoPoint(&sPoint);
  }
}


/*!------------------------------------------------------------------------------------------------
  Go up one line. Preserve column.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdBookmarkPrev(void)
{
  sNedPoint_t  sPoint;
  sNedPos_t   *pPos;

  pPos = NedListPrev(NedEditor()->hBookmarks);
  if(pPos)
  {
    NedEditorAddPosHistory();
    NedPointInit(&sPoint, pPos->pFile->pBufHead);
    NedPointFileGotoPos(&sPoint, pPos->pos);
    NedEditorGotoPoint(&sPoint);
  }
}


/*!------------------------------------------------------------------------------------------------
  Add or remove the bookmark from the list.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdBookmarkToggle(void)
{
  sNedPos_t     sPos;

  sPos.pFile = NedEditorFileCur();
  sPos.pos   = NedPointPos(NedEditorCursor());

  if(NcbIsOnBookmark(&sPos))
  {
    NedListRemove(NedEditor()->hBookmarks, &sPos);
    NedMsgPrintf("Bookmark removed");
  }
  else
  {
    NedListAdd(NedEditor()->hBookmarks, &sPos, NEDLIST_DEF_SIZE);
    NedMsgPrintf("Bookmark added");
  }
  if(NedLogMaskGet() & NEDLOG_CMD_EDIT)
    NedListLogShow(NedEditor()->hBookmarks, TRUE);
}
