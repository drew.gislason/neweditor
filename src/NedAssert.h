/*!************************************************************************************************
  @file NedAssert.h

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @brief Assert mechansim. 

*///***********************************************************************************************
#ifndef NED_ASSERT_H
#define NED_ASSERT_H

#ifndef NEDDBG_LEVEL
 #define NEDDBG_LEVEL 0
#endif

// allow user function to be called upon assert failure
typedef int (*pfnNedAssert_t)(const char *szExpr, const char *szFile, const char *szFunc, unsigned line);
void NedAssertSetExit(pfnNedAssert_t pfnAssert);

// assert and errors
void       NedError(const char *szExpr, const char *szFile, const char *szFunc, unsigned line);
#define    NedAssert(expr)          ((expr) ? (void)0 : NedError(#expr, __FILE__, __func__, __LINE__))
#define    NedAssertFail(expr)          NedError("fail", __FILE__, __func__, __LINE__)
#if NEDDBG_LEVEL >= 1
  #define  NedAssertDbg(expr)       ((expr) ? (void)0 : NedError(#expr, __FILE__, __func__, __LINE__))
  #define    NedAssertDbgFail()       NedError("fail", __FILE__, __func__, __LINE__)
#else
  #define  NedAssertDbg(expr)
  #define  NedAssertDbgFail()
#endif

#endif // NED_ASSERT_H
