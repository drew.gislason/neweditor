/*!************************************************************************************************
  @file NedView.c

  @brief  NedView, NedMsg and NedMenu comprise the view of Ned.

  @copyright Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_view Ned View - The API for displaying a file on-screen

  Goals of the view:

  1. Encasulate all things view
  2. Works as-is on NedWin (ANSI), for text terminals
  3. Should be able to work under Windows, macOS, iOS, Android or any other
  4. Handles redrawing screen, positioning cursor
  5. Used by menu subsystem

  @ingroup  ned_view

*///***********************************************************************************************
#include "Ned.h"
#include "NedLog.h"
#include "NedWin.h"
#include "NedLanguage.h"
#include "NedView.h"

#define NEDVIEW_SANCHK    22222U            // sanity check

#define NEDVIEW_DIRTY     1
#define NEDVIEW_CLEAN     0

typedef enum
{
  SYMTYPE_COMMENT,
  SYMTYPE_KEYWORD,
  SYMTYPE_CONSTANT,
  SYMTYPE_TEXT,
} symType_t;

typedef struct
{
  unsigned            sanchk;
  unsigned            screenRows;
  unsigned            screenCols;
  unsigned            editRows;
  long                tabSize;
  uint8_t            *pDirtyRows;     // allocated, contains # of screenRows bytes
  void               *pWin;           // pointer to window where to draw things
  const nedTheme_t   *pTheme;         // pointer to color screen for colorization
} sNedView_t;


/*!------------------------------------------------------------------------------------------------
  Initialize entire view system. Must use NedViewNew() to create a view.

  @return   TRUE if initialized. FALSE if error.
*///-----------------------------------------------------------------------------------------------
bool_t NedViewInit(void)
{
  return NedWinInit();
}

/*!------------------------------------------------------------------------------------------------
  Pick the main view window

  @return   TRUE if initialized. FALSE if error.
*///-----------------------------------------------------------------------------------------------
void NedViewPickView(hNedView_t hView)
{
  sNedView_t    *pView = hView;
  NedWinPick(pView->pWin);
}

/*!------------------------------------------------------------------------------------------------
  Get the current window handle

  @param    hView   a valid view, see NedViewNew()
  @return   TRUE if initialized. FALSE if error.
*///-----------------------------------------------------------------------------------------------
void *NedViewWinGetPicked(hNedView_t hView)
{
  (void)hView;
  return NedWinGetPicked();
}

/*!------------------------------------------------------------------------------------------------
  Pick the window

  @param    hView   a valid view, see NedViewNew()
  @param    pWin    a pointer to a window within the view
  @return TRUE if initialized. FALSE if error.
*///-----------------------------------------------------------------------------------------------
void NedViewWinPick(hNedView_t hView, void *pWin)
{
  (void)hView;
  NedWinPick(pWin);
}

/*!------------------------------------------------------------------------------------------------
  Create a view.

  @param    pTheme
  @return   TRUE if changed pEditor->screenRows, pEditor->screenCols. FALSE if same as before.
*///-----------------------------------------------------------------------------------------------
hNedView_t NedViewNew(const nedTheme_t *pTheme)
{
  sNedView_t    *pView;

  pView = malloc(sizeof(sNedView_t));
  if(pView)
  {
    memset(pView, 0, sizeof(sNedView_t));
    pView->sanchk = NEDVIEW_SANCHK;
    pView->pTheme = pTheme;
    NedViewCheckScreenSize(pView);
    pView->pWin   = NedWinGetBackdrop();
    pView->pDirtyRows = malloc(pView->editRows);
    if(pView->pDirtyRows)
      NedViewInvalidateAll(pView);
    else
      NedViewFree(pView);
  }


  return pView;
}

/*!------------------------------------------------------------------------------------------------
  Free the view.

  @param    hView     view handle
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedViewFree(hNedView_t hView)
{
  sNedView_t    *pView = hView;

  if(NedViewIsView(pView))
  {
    if(pView->pDirtyRows)
    {
      free(pView->pDirtyRows);
      pView->pDirtyRows = NULL;
    }
    memset(pView, 0, sizeof(sNedView_t));
    free(pView);
  }
}

/*!------------------------------------------------------------------------------------------------
  Is this a view handle?

  @param    hView     view handle
  @return   TRUE if yes, FALSE if not a view handle.
*///-----------------------------------------------------------------------------------------------
bool_t NedViewIsView (hNedView_t hView)
{
  sNedView_t    *pView = hView;
  return (pView && (pView->sanchk == NEDVIEW_SANCHK)) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Show the view to the log

  @param    hView     valid view handle
  @return   TRUE if yes, FALSE if not a view handle.
*///-----------------------------------------------------------------------------------------------
void NedViewLogShow(hNedView_t hView)
{
  sNedView_t     *pView = hView;
  unsigned        i;

  NedLogPrintf("NedView(%p) ", pView);
  if(!NedViewIsView(pView))
    NedLogPrintf("<<< INVALID!!!\n");
  else
  {
    NedLogPrintf("screenRows %u, screenCols %u, editRows %u\n  [",
      pView->screenRows, pView->screenCols, pView->editRows);
    for(i=0; i<pView->editRows; ++i)
    {
      NedLogPrintf("%u,", pView->pDirtyRows[i]);
    }
    NedLogPrintf("]\n");
  }
}

/*!------------------------------------------------------------------------------------------------
  Checks screen size to see if it's changed. If so, adjusts the underlying windowing system and
  the view to reflect new size. Invalidates all rows if changed.

  @param    hView     valid view handle
  @return   TRUE if screen size has changed and there is a need to redraw
*///-----------------------------------------------------------------------------------------------
bool_t NedViewCheckScreenSize(hNedView_t hView)
{
  sNedView_t     *pView = hView;
  winPos_t        screenRows;
  winPos_t        screenCols;
  unsigned        newEditRows;
  uint8_t        *pNewDirtyRows;
  bool_t          fUpdated = FALSE;

  if(NedViewIsView(pView))
  {
    NedWinGetScreenSize(&screenRows, &screenCols);
    if((screenRows != pView->screenRows) || (screenCols != pView->screenCols))
    {
      // note: this works for editor window because it uses backdrop.
      // If editor window ever changes from backdrop it's own window, then that must be adjusted too.
      NedWinAdjustScreen(screenRows, screenCols);
      pView->screenRows = (unsigned)screenRows;
      pView->screenCols = (unsigned)screenCols;

      // determine edit rows
      if(pView->screenRows > NUM_OTHER_ROWS)
        newEditRows = pView->screenRows - NUM_OTHER_ROWS;
      else
        newEditRows = 0;

      // if growing # of edit rows, allocate them now. All rows will be invalid.
      if(newEditRows > pView->editRows)
      {
        pNewDirtyRows = malloc(newEditRows);
        NedAssert(pNewDirtyRows);
        free(pView->pDirtyRows);
        pView->pDirtyRows = pNewDirtyRows;
      }
      pView->editRows   = newEditRows;

      NedLogPrintfEx(NEDLOG_VIEW, "UPDATED: NedViewCheckScreenSize(screenRows %u, screenCols %u, editRows %u\n",
          pView->screenRows, pView->screenCols, pView->editRows);

      // columns or rows may have grown, redraw lines
      NedViewInvalidateAll(hView);
      fUpdated = TRUE;
    }
  }
  return fUpdated;
}

/*!------------------------------------------------------------------------------------------------
  Set the theme for the view. Will cause everthing to be redrawn.

  @param    hView     handle for view
  @param    pTheme    pointer to theme colors
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedViewSetTheme(hNedView_t hView, const nedTheme_t *pTheme)
{
  sNedView_t     *pView = hView;

  NedAssert(pTheme);
  if(NedViewIsView(pView))
    pView->pTheme = pTheme;
}

/*!------------------------------------------------------------------------------------------------
  Get # of editor text rows (not including menu row, status bar, etc...)

  @param    hView     handle for view
  @return   1-n, number of editor text rows.
*///-----------------------------------------------------------------------------------------------
unsigned NedViewEditRows(hNedView_t hView)
{
  sNedView_t     *pView = hView;
  return NedViewIsView(pView) ? pView->editRows : 0;
}

/*!------------------------------------------------------------------------------------------------
  Get # of rows on screen

  @param    hView     handle for view
  @return   1-n, number of rows in terminal screen.
*///-----------------------------------------------------------------------------------------------
unsigned NedViewScreenRows(hNedView_t hView)
{
  sNedView_t     *pView = hView;
  return NedViewIsView(pView) ? pView->screenRows : 0;
}

/*!------------------------------------------------------------------------------------------------
  Get # of columns on screen.

  @param    hView     handle for view
  @return   1-n, number of columns in terminal screen.
*///-----------------------------------------------------------------------------------------------
unsigned NedViewScreenCols(hNedView_t hView)
{
  sNedView_t     *pView = hView;
  return NedViewIsView(pView) ? pView->screenCols : 0;
}

/*!------------------------------------------------------------------------------------------------
  Invalidate an editor row so it gets redrawn.

  @param    hView     handle for view
  @param    row       0-n (editRows - 1)
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedViewInvalidateRow(hNedView_t hView, unsigned row)
{
  sNedView_t     *pView = hView;

  NedAssert(NedViewIsView(pView));
  if(row >= NedViewEditRows(hView))
    NedLogPrintfEx(NEDLOG_VIEW, "row %ld, editRows %ld\n", row, NedViewEditRows(hView));
  NedAssert(row < NedViewEditRows(hView));
  pView->pDirtyRows[row] = NEDVIEW_DIRTY;
}

/*!------------------------------------------------------------------------------------------------
  Higher layer invalidating all editor text rows so they will be redraw.

  @param    hView     handle for view
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedViewInvalidateAll(hNedView_t hView)
{
  sNedView_t     *pView = hView;
  unsigned        row;
  if(NedViewIsView(pView))
  {
    for(row=0; row<pView->editRows; ++row)
      pView->pDirtyRows[row] = NEDVIEW_DIRTY;
  }
}

/*!------------------------------------------------------------------------------------------------
  If redrawing everything, it's a good idea to clear screen to clean up artifacts.

  @param    hView     handle for view
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedViewCls(hNedView_t hView)
{
  sNedView_t     *pView = hView;
  NedViewPickView(hView);
  NedWinClearScreen(pView->pTheme->text);
}

/*!------------------------------------------------------------------------------------------------
  Flush the physical cursor to editor cursor position.

  @param    hView     handle for view
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedViewFlushCursor(hNedView_t hView, const sNedFile_t *pFile)
{
  NedViewPickView(hView);
  NedWinFlush();
  NedWinGoto(ROW_EDIT + (pFile->curLine - pFile->topLine), pFile->curCol - pFile->leftCol);
  NedWinFlushCursor();
}

/*!------------------------------------------------------------------------------------------------
  Flush all the screen data, but not the cursor

  @param    hView     handle for view
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedViewFlushOnly(hNedView_t hView)
{
  (void)hView;
  NedWinFlushAll();
}

/*!------------------------------------------------------------------------------------------------
  Flush window data and cursor to the screen

  @param    hView     handle for view
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedViewFlushAll(hNedView_t hView)
{
  (void)hView;
  NedWinFlushAll(); 
  NedWinFlushCursor();
}

/*-------------------------------------------------------------------------------------------------
  Checks to see if the word at the point is in the list of string pointers.

  Returns the length of the found string or 0 if not found. pPoint is not moved.
-------------------------------------------------------------------------------------------------*/
static long NvInWordList(const char **ppList, const sNedPoint_t *pPoint, long maxLen, bool_t fWholeWord)
{
  char          szSymbol[NEDWIN_MAX_COLS+1];
  long          len;
  sNedPoint_t   sPoint;
  nedChar_t     c;

  while(*ppList)
  {
    len = strlen(*ppList);
    if(len <= maxLen)
    {
      NedPointStrGet((void *)szSymbol, pPoint, len);
      szSymbol[len] = '\0';
      if(strcmp(szSymbol, *ppList) == 0)
      {
        sPoint = *pPoint;
        NedPointCharAdvance(&sPoint, len);
        c = NedPointCharGet(&sPoint);
        if(!fWholeWord || NedCharType(c) != NedCharType(*szSymbol))
          return len;
      }
    }

    ++ppList;
  }
  return 0L;
}

/*-------------------------------------------------------------------------------------------------
  Checks to see if the word at the point is a number.

  Returns the length of the found number string or 0 if not a number. pPoint is not moved.
-------------------------------------------------------------------------------------------------*/
static long NvNumberLen(sNedPoint_t sPoint)
{
  long        len = 0;
  nedChar_t   c;
  bool_t      fIsHex = FALSE;

  while(1)
  {
    // 12345, or 0x1e32
    c = NedPointCharGet(&sPoint);
    if(len == 1 && c == 'x')
      fIsHex = TRUE;
    else if(c < '0' || c > '9')
    {
      c = toupper(c);
      if(!fIsHex || (c < 'A' || c > 'F'))
        break;
    }
    ++len;
    NedPointCharNext(&sPoint);
  }

  return len;
}

/*-------------------------------------------------------------------------------------------------
  Get length of string If string doesn't terminate before end of line, considered not a string.

  "this \"is a string",  `so is this`  and 'so is this'. Allows double quote, single quote, back
  quote to deliminate a string.

  For text files, the single quote isn't included so words like you'll be willin' aren't a string.

  Returns the length of string, including quotes, or 0 if not a string.
-------------------------------------------------------------------------------------------------*/
static long NvStringLen(sNedPoint_t sPoint, bool_t fIsText)
{
  const char    szStringChars[] = "'\"`";   // single quote, double quote, back tick
  const char   *psz;
  nedChar_t     c;
  long          len = 0;

  // ignore single quotes in text files. Plays havoc with words like it's, or ain't.
  if(fIsText)
    psz = &szStringChars[1];
  else
    psz = szStringChars;

  c   = NedPointCharGet(&sPoint);
  psz = strchr(psz, c);
  if(psz)
  {
    len = 1;
    NedPointCharNext(&sPoint);
    while(1)
    {
      c = NedPointCharGet(&sPoint);
      if(c == '\\')
      {
        ++len;
        NedPointCharNext(&sPoint);
      }
      else if(c == *psz)
      {
        ++len;
        break;
      }

      // if we hit end of line or end of file, not a terminated string
      else if(c == NEDCHAR_EOF || NedCharType(c) == IS_LF)
      {
        len = 0;
        break;
      }

      ++len;
      NedPointCharNext(&sPoint);
    }
  }
  return len;
}

/*-------------------------------------------------------------------------------------------------
  Skip anything that's not a keyword, string or number.

  Returns the length of other text. Never 0.
-------------------------------------------------------------------------------------------------*/
static long NvSkipOther(sNedPoint_t sPoint)
{
  const char    szStringChars[] = "\"'`";   // double quote, single quote, back quote
  nedChar_t     c;
  sNedPoint_t   sThat;
  bool_t        fFirstChar = TRUE;

  sThat = sPoint;

  // special case for ("some quote")
  c = NedPointCharGet(&sThat);
  if(NedCharType(c) == IS_PUNCT)
  {

    while(NedCharType(c) == IS_PUNCT)
    {
      // allows for case of printf("hello"), since both ( and " are IS_PUNCT
      if(!fFirstChar && strchr(szStringChars, c))
        break;
      fFirstChar = FALSE;
      c = NedPointCharGetNext(&sThat);
    }
    // skip following space
    while(NedCharType(c) == IS_SPACE)
    {
      c = NedPointCharGetNext(&sThat);
    }
  }

  // skip word
  else
    NedPointWordNext(&sThat);

  return NedPointDifference(&sPoint, &sThat);
}

/*-------------------------------------------------------------------------------------------------
  Return length of next symbol. Allows for colorization per symbol. Recognizes 4 symbol types:

  COMMENT, KEYWORD, CONSTANT (includes strings) and TEXT

  Special note: symbols like keywords will return the full length, which may be longer than *pLen
  input.

  *pLen is both an input (max length) and an output (found length).
  Returns the length and type of the symbol at the point. Point is not moved.
-------------------------------------------------------------------------------------------------*/
static symType_t NvNextSymbol(const sNedLanguage_t *pLang, const sNedPoint_t sPoint, long *pLen)
{
  long                  len;

  // nothing to display, 0 length
  if(*pLen == 0)
    return SYMTYPE_TEXT;

  // if line comment, the rest of the line belongs to the comment.
  len = NvInWordList(pLang->aszLineComments, &sPoint, *pLen, FALSE);
  if(len > 0)
  {
    return SYMTYPE_COMMENT;
  }

  // check for keyword
  len = NvInWordList(pLang->aszKeywords, &sPoint, *pLen, TRUE);
  if(len > 0)
  {
    *pLen = len;
    return SYMTYPE_KEYWORD;
  }

  // check for numeric and string constants
  len = NvNumberLen(sPoint);
  if(len > 0)
  {
    *pLen = len;
    return SYMTYPE_CONSTANT;
  }
  len = NvStringLen(sPoint, NedLangIsTxt(pLang));
  if(len > 0)
  {
    *pLen = len;
    return SYMTYPE_CONSTANT;
  }

  // OK, not a comment, keyword, constant or string, skip until another possible one
  len = NvSkipOther(sPoint);
  NedAssert(len > 0);
  *pLen = len;
  return SYMTYPE_TEXT;
}

/*-------------------------------------------------------------------------------------------------
  Draw a line of text from the buffer into the window. Duties:

  1. Draw syntax colors
  2. Draw proper tab width
  3. Draw at left col of line (may be a long line)
  4. Draw selection color

  @ingroup  ned_edit
  @param    hView         handle to the view
  @param    row           0-n, the row in the editor window (not line in file)
  @param    leftCol       zero-based left column (0-n), unlike the sNedFile_t 1 based column
  @param    pLang         language, used for tabSize and keyword look-up
  @param    sPoint        start of line (which may be off-screen to the left)

  @return   none
-------------------------------------------------------------------------------------------------*/
static void NvPrintLine(hNedView_t hView, unsigned row, const sNedFile_t *pFile, const sNedLanguage_t *pLang, sNedPoint_t sPoint)
{
  const sNedView_t   *pView = hView;
  uint8_t             szSymbol[NEDWIN_MAX_COLS + NEDCFG_MAX_INDENT + 1];
  uint8_t            *pChar;
  sNedPoint_t         sLineEnd;
  long                lineLen;    // in file characters
  long                lenSymbol;  // in file characters
  long                printLen;   // in screen chars
  long                col;        // in screen chars
  long                tabCols;    // in screen chars
  long                thisCol;    // in screen chars
  long                leftCol;    // 0-n (unlike pFile->leftCol whiuch is 1-n)
  long                i;
  long                symOffset;
  symType_t           symType;
  nedAttr_t           attr;

  NedAssert(row < pView->editRows);

  // determine length of line in file characters, not including \n (may be 0 for empty lines)
  sLineEnd = sPoint;
  NedPointLineBeg(&sPoint);
  NedPointLineEnd(&sLineEnd);
  lineLen = NedPointDifference(&sPoint, &sLineEnd);
  leftCol = pFile->leftCol - 1;

  // must always process from the beginning of the line, as a symbol may be partially off left side
  // of screen
  NedWinGoto(ROW_EDIT + row, 0);
  col = 0;
  while((lineLen > 0) && (col < leftCol + pView->screenCols))
  {
    // get length of symbol (which may be longer than screen width)
    lenSymbol = lineLen;
    symType   = NvNextSymbol(pLang, sPoint, &lenSymbol);

    // get color based on symbol type
    if(symType == SYMTYPE_COMMENT)
      attr = pView->pTheme->comment;
    else if(symType == SYMTYPE_KEYWORD)
      attr = pView->pTheme->keyword;
    else if(symType == SYMTYPE_CONSTANT)
      attr = pView->pTheme->constant;
    else
      attr = pView->pTheme->text;

    printLen = lenSymbol;
    if(printLen > sizeof(szSymbol) - 1)
      printLen = sizeof(szSymbol) - 1;
    NedPointMemGet(szSymbol, &sPoint, printLen);
    szSymbol[printLen] = '\0';

    // process print line (can be larger than file chars in that tabs are expanded)
    // 1. tabs get expanded into spaces
    // 2. unprintables (such as 0x07) will appear as dots
    pChar = szSymbol;
    thisCol = col;
    for(i = 0; i < printLen && i < sizeof(szSymbol); ++i)
    {
      if(*pChar == '\t')
      {
        // 1. tabs get expanded into spaces
        if(pView->tabSize == 0 || pView->tabSize > NEDCFG_MAX_INDENT)
        {
          NedLogPrintf("pView->tabSize %ld\n", pView->tabSize);
        }
        NedAssert(pView->tabSize && pView->tabSize <= NEDCFG_MAX_INDENT);
        tabCols = pView->tabSize - (thisCol % pView->tabSize);

        if(tabCols > 1)
        {
          if(i + tabCols > (sizeof(szSymbol) - 1))
            tabCols = (sizeof(szSymbol) - 1) - i;
          memmove(pChar + tabCols, pChar + 1, printLen - i);  // includes terminating '\0'
        }
        memset(pChar, ' ', tabCols);
        pChar     += tabCols;
        thisCol   += tabCols;
        printLen  += tabCols - 1;   // tab char took 1 space, plus expanded
        i         += tabCols - 1;
      }

      // single character (not tab), possibly replace unprintables
      else
      {
        // 2. unprintables (such as 0x01) will appear as dots
        if(*pChar < ' ' || *pChar > '~')
          *pChar = '.';
        ++pChar;
        ++thisCol;
      }
    }
    szSymbol[printLen] = '\0';

    // ignore anything off screen
    if((col + printLen > leftCol) && (col < leftCol + pView->screenCols))
    {
      symOffset = 0;
      if(col < leftCol)
      {
        symOffset = leftCol - col;
        NedLogPrintfEx(NEDLOG_VIEW, "  symOffset %ld\n", symOffset);
      }

      // if line is long, last symbol may need to be shortened
      if(col + printLen > leftCol + pView->screenCols)
      {
        NedLogPrintfEx(NEDLOG_VIEW, "  shortening printLen from %ld\n", printLen);
        printLen = leftCol + pView->screenCols - col;
        szSymbol[printLen] = '\0';
      }

      NedLogPrintfEx(NEDLOG_VIEW, "col %ld, leftCol %ld, lenSymbol %ld, printLen %ld, lineLen %ld, symType %u, '%s'\n",
        col, leftCol, lenSymbol, printLen, lineLen, symType, (char *)szSymbol);
      NedWinSetAttr(attr);
      NedWinPuts((char *)(&szSymbol[symOffset]));
    }

    // advance to next symbol
    NedPointCharAdvance(&sPoint, lenSymbol);
    lineLen -= lenSymbol;
    col     += printLen;
  }

  // clear the rest of the line in normal text attribute
  NedWinSetAttr(pView->pTheme->text);
  NedWinClearEol();
}

/*!------------------------------------------------------------------------------------------------
  Get the actual column of the point. Understands tabs for the current file .ext

  @ingroup  ned_edit
  @param    pPoint           Some point in the file

  @return   1-n (index)
*///-----------------------------------------------------------------------------------------------
long NvColGet(const sNedPoint_t *pPoint, unsigned tabSize)
{
  sNedPoint_t   pt;
  long          col = 0;
  long          nChars;
  nedChar_t     c;

  // start at beginning of line, even if off screen
  pt = *pPoint;
  NedPointLineBeg(&pt);
  nChars = NedPointDifference(&pt, pPoint);

  while(nChars)
  {
    // tab expands to 1-n columns, depeding on start column
    c = NedPointCharGet(&pt);
    if(c == '\t')
      col += tabSize - (col % tabSize);
    else
      ++col;
    NedPointCharNext(&pt);
    --nChars;
  }

  return col + 1; // file columns are 1 based
}

/*-------------------------------------------------------------------------------------------------
  Highlight all selected rows on screen. Assumes all pFile parameters are correct.

  Since cursor is always on screen, at least some part of the selection is on screen.

  parameters:
  hView     handle for view
  pFile     file with potential selection in it

  returns TRUE if part of this line has a visible selection, FALSE if not.
-------------------------------------------------------------------------------------------------*/
static void NvHighlightSelected(hNedView_t hView, const sNedFile_t *pFile)
{
  sNedView_t   *pView = hView;
  long          selLine;
  long          selCol;
  long          topLine;
  long          botLine;
  long          topCol;
  long          botCol;
  long          left;
  long          right;
  long          screenRight;
  long          line;
  bool_t        fTopOffScreen;
  bool_t        fBotOffScreen;

  if(pFile->fInSelect)
  {
    // cursor may be before or after select point
    selLine = NedPointLineGet(&pFile->sSelect, NULL);
    selCol  = NvColGet(&pFile->sSelect, pFile->tabSize);  // make selCol tab aware
    NedLogPrintfEx(NEDLOG_VIEW, "  selLine %ld, selCol %ld, curLine %ld, curCol %ld\n",
      selLine, selCol, pFile->curLine, pFile->curCol);

    // of cursor is same as select point, do nothing
    if((selLine != pFile->curLine) || (selCol != pFile->curCol))
    {
      // if select point is after cursor, then top is cursor, bottom is select point
      if(selLine > pFile->curLine || ((selLine == pFile->curLine) && (selCol > pFile->curCol)))
      {
        topLine = pFile->curLine;
        topCol  = pFile->curCol;
        botLine = selLine;
        botCol  = selCol;
      }

      // top is select point, bottom is cursor
      else
      {
        topLine = selLine;
        topCol  = selCol;
        botLine = pFile->curLine;
        botCol  = pFile->curCol;
      }

      // display file coordinates for selection
      NedLogPrintfEx(NEDLOG_VIEW, "  topLine %ld, topCol %ld, botLine %ld, botCol %ld\n", topLine, topCol, botLine, botCol);

      // limit to lines/cols on screen
      screenRight = pFile->leftCol + NedViewScreenCols(hView);
      fTopOffScreen = FALSE;
      if(topLine < pFile->topLine)
      {
        topLine = pFile->topLine;
        fTopOffScreen = TRUE;
      }
      fBotOffScreen = FALSE;
      if(botLine > pFile->topLine + NedViewEditRows(hView) - 1)
      {
        botLine = pFile->topLine + NedViewEditRows(hView) - 1;
        fBotOffScreen = TRUE;
      }

      // highlight each line
      for(line = topLine; line <= botLine; ++line)
      {
        // left could be off screen to the left or right
        left = pFile->leftCol;
        if(!fTopOffScreen && line == topLine)
        {
          // no highlight this line, as top is off screen to the right
          if(topCol >= screenRight)
            continue;

          // top be on-screen, someplace in the middle
          else if(topCol > pFile->leftCol)
            left = topCol;
        }

        right = screenRight - 1;
        if(!fBotOffScreen && line == botLine)
        {
          // no highlight this line, as bottom is off screen to the left
          if(botCol < pFile->leftCol)
            continue;

          else if(botCol < screenRight)
            right = botCol;
        }

        NedLogPrintfEx(NEDLOG_VIEW, "  left %ld, right %ld, line %ld\n", left, right, line);
        NedAssert(left <= right);
        NedWinAttrFillPart(ROW_EDIT + (winPos_t)(line - pFile->topLine), (winPos_t)(left - pFile->leftCol),
          1, (right + 1) - left, pView->pTheme->textHighlight);
      }
    }
  }
}

/*!------------------------------------------------------------------------------------------------
  Invalidate all rows that are part of the selection. Assumes all pFile parameters are correct.

  @ingroup  ned_view
  @param    hView     handle for view
  @param    pFile     editor file

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedViewInvalidateSelected(hNedView_t hView, const sNedFile_t *pFile)
{
  long          selLine;
  long          topLine;
  long          botLine;

  if(pFile->fInSelect)
  {
    selLine = NedPointLineGet(&pFile->sSelect, NULL);
    NedLogPrintfEx(NEDLOG_VIEW, "NedViewInvalidateSelected(curLine %ld, selLine %ld, topLine %ld, editRows %ld)\n",
      pFile->curLine, selLine, pFile->topLine, NedViewEditRows(hView));

    if(selLine > pFile->curLine)
    {
      topLine = pFile->curLine;
      botLine = selLine;
    }
    else
    {
      topLine = selLine;
      botLine = pFile->curLine;
    }
    // NedLogPrintfEx(NEDLOG_VIEW, "B4: topLine %ld, botLine %ld, ", topLine, botLine);
    if(topLine < pFile->topLine)
      topLine = pFile->topLine;
    if(botLine > pFile->topLine + (NedViewEditRows(hView) - 1))
      botLine = pFile->topLine + NedViewEditRows(hView) - 1;
    // NedLogPrintfEx(NEDLOG_VIEW, "AFTER: topLine %ld, botLine %ld\n", topLine, botLine);

    while(topLine <= botLine)
    {
      NedViewInvalidateRow(hView, topLine - pFile->topLine);
      ++topLine;
    }
  }
}

/*!------------------------------------------------------------------------------------------------
  If redrawing everything, it's a good idea to clear screen to clean up artifacts.

  @ingroup  ned_view
  @param    hView     handle for view

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedViewDraw(hNedView_t hView, const sNedFile_t *pFile)
{
  const sNedLanguage_t   *pLang;
  sNedView_t             *pView = hView;
  sNedPoint_t             sLine;
  unsigned                row;

  NedLogPrintfEx(NEDLOG_VIEW, "NedViewDraw(%s)  pFile->topLine %ld, curLine %ld, curCol %ld, leftCol %ld, fInSelect %u\n", 
      NedFileNameOnly(pFile), pFile->topLine, pFile->curLine, pFile->curCol, pFile->leftCol, pFile->fInSelect);

  NedViewPickView(hView);

  // start at topline
  NedPointInit(&sLine, pFile->pBufHead);
  NedPointLineGoto(&sLine, pFile->topLine);
  NedPointLineBeg(&sLine);

  row = 0;
  pLang = NedLangGet(NedFilePath(pFile));
  pView->tabSize = pFile->tabSize;
  while(row < pView->editRows)
  {
    // print the line and on to next
    if(pView->pDirtyRows[row] == NEDVIEW_DIRTY)
    {
      NvPrintLine(hView, row, pFile, pLang, sLine);
      pView->pDirtyRows[row] = NEDVIEW_CLEAN;
    }
    ++row;
    if(!NedPointLineNext(&sLine))   // if we hit end of file, then move on to blank llines
      break;
  }

  // clear any lines past end of file
  while(row < pView->editRows)
  {
    if(pView->pDirtyRows[row] == NEDVIEW_DIRTY)
    {
      NedWinGoto(ROW_EDIT + row, 0);
      NedWinClearEol();
      pView->pDirtyRows[row] = NEDVIEW_CLEAN;
    }
    ++row;
  }

  // highlight any lines that are selected
  NvHighlightSelected(hView, pFile);
}

/*!------------------------------------------------------------------------------------------------
  Shut down windowing system

  @ingroup  ned_view
  @param    hView     handle for view

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedViewShutdown(void)
{
  NedWinClearScreen(NEDATTR_RESET);
}
