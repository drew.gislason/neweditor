/*!************************************************************************************************
  @file NedJson2.h

  Parses and outputs JSON strings. Verify Python json handling, but in C style.

  @copyright Copyright (c) 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

**************************************************************************************************/

#ifndef NEDJSON_H
#define NEDJSON_H
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include "Ned.h"

EXTERN_C

// floating point libraries can be large. Optional.
#ifndef NEDJSON_CFG_FLOAT
  #define NEDJSON_CFG_FLOAT    0
#endif

#ifndef NEDJSON_MAX_LEVEL
  #define NEDJSON_MAX_LEVEL   12
#endif

typedef enum
{
  NEDJSON_ARRAY,
  NEDJSON_BOOL,
  NEDJSON_FLOAT,
  NEDJSON_NULL,
  NEDJSON_NUMBER,
  NEDJSON_OBJ,
  NEDJSON_STRING,
  NEDJSON_INVALID
} nedJsonType_t;

typedef void * hNedJson_t;

// JSON input
bool_t          NedJsonIsJson       (const char *szJson);
const char     *NedJsonGetObj       (const char *sz);
const char     *NedJsonGetKey       (const char *szObj, size_t index);
const char     *NedJsonGetValuePtr  (const char *szKey, nedJsonType_t *pType);
size_t          NedJsonGetCount     (const char *szObj);
const char     *NedJsonGetScalar    (const char *szArray, size_t index, nedJsonType_t *pType);
// const char     *NedJsonGet          (const char *szKeyPath, nedJsonType_t *pType);
bool_t          NedJsonGetBool      (const char *szBool);
#if NEDJSON_CFG_FLOAT
double          NedJsonGetFloat     (const char *szFloat);
#endif
long            NedJsonGetNumber    (const char *szNumber);
size_t          NedJsonStrLen       (const char *szJsonStr);
int             NedJsonStrCmp       (const char *sz, const char *szJsonStr);
size_t          NedJsonStrNCpy      (char *szDst, const char *szJsonStr, size_t n);

// JSON output
hNedJson_t      NedJsonNew          (char *szDst, size_t maxSize, bool_t fPretty);
bool_t          NedJsonIsHandle     (hNedJson_t hJson);
void            NedJsonLogShow      (hNedJson_t hJson);
void            NedJsonFree         (hNedJson_t hJson);
size_t          NedJsonPut          (hNedJson_t hJson, const char *szKey, nedJsonType_t type, const void *pValue);
size_t          NedJsonPutScalar    (hNedJson_t hJson, nedJsonType_t type, const void *pValue);
size_t          NedJsonPutBegin     (hNedJson_t hJson, nedJsonType_t type);
size_t          NedJsonPutEnd       (hNedJson_t hJson, nedJsonType_t type);

EXTERN_C_END

#endif // NEDJSON_H
