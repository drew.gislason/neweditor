/*!************************************************************************************************
  @file NedCmdEdit.c

  @brief  Ned Command Implementation for editing type commands

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include <time.h>
#include "Ned.h"
#include "NedEditor.h"
#include "NedUndo.h"

typedef struct
{
  long            topLine;
  long            botLine;
  sNedPoint_t    *pPtTop;
  sNedPoint_t    *pPtBot;
  bool_t          fCursorIsTop;
} sNceTopBot_t;

static const char szWrapAtWidth[] = "Wrap at width: ";
static const char szRdOnly[]      = "File is read only";
static const char szTmpFile[]     = "ned.tmp";


/*-------------------------------------------------------------------------------------------------
  Returns sNceTopBot_t: pointers to top/bot point, and topline and bottom line.
  Returns # of lines selected.
-------------------------------------------------------------------------------------------------*/
static long NceGetSelectRows(sNceTopBot_t *pTopBot)
{
  sNedFile_t   *pFile = NedEditorFileCur();
  long          selLine;

  NedAssert(pFile->fInSelect);
  selLine = NedPointLineGet(&pFile->sSelect, NULL);
  if(selLine > pFile->curLine)
  {
    pTopBot->fCursorIsTop = TRUE;
    pTopBot->pPtTop  = &pFile->sCursor;
    pTopBot->pPtBot  = &pFile->sSelect;
    pTopBot->topLine = pFile->curLine;
    pTopBot->botLine = selLine;
  }
  else
  {
    pTopBot->fCursorIsTop = FALSE;
    pTopBot->pPtTop  = &pFile->sSelect;
    pTopBot->pPtBot  = &pFile->sCursor;
    pTopBot->topLine = selLine;
    pTopBot->botLine = pFile->curLine;
  }

  return (pTopBot->botLine - pTopBot->topLine) + 1;
}

/*-------------------------------------------------------------------------------------------------
  Extends the selection to full line(s).
  Returns the # of lines.
-------------------------------------------------------------------------------------------------*/
static long NceExtendSelect(sNceTopBot_t *pTopBot)
{
  long  lines;

  lines = NceGetSelectRows(pTopBot);
  NedPointLineBeg(pTopBot->pPtTop);
  if(!NedPointIsAtBegLine(pTopBot->pPtBot))
  {
    NedPointLineEnd(pTopBot->pPtBot);
    NedPointCharNext(pTopBot->pPtBot);
  }
  return lines;
}

/*-------------------------------------------------------------------------------------------------
  Cut between two points to the clipboard. If fClear is FALSE, then appends to clipboard.
-------------------------------------------------------------------------------------------------*/
static long NceCut(bool_t fClear, sNedPoint_t *pPt1, sNedPoint_t *pPt2)
{
  sNedFile_t   *pFile      = NedEditorFileCur();
  sNedFile_t   *pClipboard = NedEditorClipboard(fClear);
  long          diff;
  long          curLineOld = pFile->curLine;

  NedAssert(NedPointIsSameChain(pPt1, pPt2));

  // invalidate all the selected lines, they are going away
  NedEditorInvalidateLine();
  if(pFile->fInSelect)
    NedEditorInvalidateSelected();

  // append to, not clear the clipboard
  if(!fClear)
    NedPointFileBottom(&pClipboard->sCursor);

  // adjust PosHistory and Bookmarks
  NedEditorPosListAdjustDel(pPt1, pPt2);

  diff = NedPointClipCopy(&pClipboard->sCursor, pPt1, pPt2);
  NedUndoDelBlock(NedEditorUndoList(), pPt1, pPt2);
  NedLogPrintfEx(NEDLOG_CMD_EDIT, "NceCut diff %ld, curLineOld %ld\n", diff, curLineOld);

  // tell user
  if(diff < 0)
    diff = -1L * diff;

  // adjust cursor (and possibley screen)
  NedEditorAdjustCurFileFields();

  // may have deleted multiple rows. if so, invalidate them
  if(curLineOld != pFile->curLine)
    NedEditorBuildAroundCursor();
  pFile->fInSelect = FALSE;
  if(diff != 0L)
    pFile->fDirty = TRUE;

  return diff;
}

/*-------------------------------------------------------------------------------------------------
  Adjust bookmarks and pos history based on undo/redo length, len is negative for deletes
-------------------------------------------------------------------------------------------------*/
static void NcePosListAdjustUndo(sNedFile_t *pFile, long pos, long len)
{
  sNedPoint_t sTop;
  sNedPoint_t sBot;

  NedLogPrintfEx(NEDLOG_CMD_EDIT, "NcePosListAdjustUndo(pos %ld, len %ld\n", pos, len);
  if(pFile && len)
  {
    sTop = pFile->sCursor;
    NedPointFileGotoPos(&sTop, pos);
    if(len > 0)
    {
      NedPosListAdjustIns(NedEditor()->hPosHistory, &sTop, len);
      NedPosListAdjustIns(NedEditor()->hBookmarks, &sTop, len);
    }
    else if(len < 0)
    {
      sBot = sTop;
      len = -1L * len;
      NedPointFileGotoPos(&sBot, pos + len);
      NedPosListAdjustDel(NedEditor()->hPosHistory, &sTop, &sBot);
      NedPosListAdjustDel(NedEditor()->hBookmarks, &sTop, &sBot);
    }
  }
}

/*-------------------------------------------------------------------------------------------------
  Cut whatever is selected to the clipboard. If fClear is FALSE, then appends to clipboard
-------------------------------------------------------------------------------------------------*/
static long NceCutSelected(bool_t fClear)
{
  sNedFile_t  *pFile = NedEditorFileCur();
  return NceCut(fClear, &pFile->sSelect, &pFile->sCursor);
}

/*-------------------------------------------------------------------------------------------------
  Given the current language, get the indent character and count (e.g 1 tab or 4 spaces)
-------------------------------------------------------------------------------------------------*/
static nedChar_t NceLangIndent(sNedFile_t *pFile, unsigned *pCount, bool_t fForceSpaces)
{
  nedChar_t              c;

  if(!fForceSpaces && pFile->fHardTabs)
  {
    c = '\t';
    *pCount = 1;
  }
  else
  {
    c = ' ';
    *pCount = pFile->tabSize;
  }

  return c;
}

/*-------------------------------------------------------------------------------------------------
  Get the indent # of chars at this point, and whether the point is tabs or spaces.
  Assumes point is at beginning of line. Does not move point
-------------------------------------------------------------------------------------------------*/
static long NceIndent(const sNedPoint_t *pPoint, nedChar_t *pTabChar)
{
  nedChar_t   c;
  long        indent = 0L;
  sNedPoint_t sPoint;

  sPoint = *pPoint;
  c = NedPointCharGet(&sPoint);
  if(NedCharType(c) == IS_SPACE)
  {
    *pTabChar = c;    
    while(NedCharType(c) == IS_SPACE)
    {
      NedPointCharNext(&sPoint);
      c = NedPointCharGet(&sPoint);
    }
    indent = NedPointDifference(pPoint, &sPoint);
    NedLogPrintfEx(NEDLOG_CMD_EDIT, "NceIndent => %ld, c %02x\n", indent, (unsigned)c);
  }

  return indent;
}

/*-------------------------------------------------------------------------------------------------
  Get the indent # of chars at this point, and whether the point is tabs or spaces.
  Assumes point is at beginning of line.
-------------------------------------------------------------------------------------------------*/
static void NceInsCharRepeated(sNedPoint_t *pPoint, long nChars, nedChar_t c)
{
  nedChar_t  *pChars;

  if(nChars > 0L)
  {
    pChars = malloc(nChars);
    if(pChars)
    {
      NedEditorPosListAdjustIns(pPoint, nChars);
      memset(pChars, c, nChars);
      NedUndoInsBlock(NedEditorUndoList(), pPoint, pChars, nChars);
      free(pChars);
    }
  }
}

/*-------------------------------------------------------------------------------------------------
  Return the minimum indent in the selected lines (may be 0). pfAdding returns TRUE if one more
  more lines do not have the line comment. Uses line comment from language (usually // or #).

  Do not call if language doesn't have a line comment (e.g. HTML)

  @ingroup  ned_cmd
  @returns  0-n, minimum indent
-------------------------------------------------------------------------------------------------*/
static long NceGetMinIndent(bool_t *pfAdding, long lines, const sNedPoint_t *pPoint)
{
  sNedFile_t             *pFile     = NedEditorFileCur();
  const char             *szComment =  NedLangGet(NedFilePath(pFile))->aszLineComments[0];
  bool_t                  fAdding   = FALSE;
  sNedPoint_t             sPt;
  long                    indent;
  long                    nChars;

  NedAssert(lines >= 1);
  NedAssert(szComment);

  indent = LONG_MAX;
  sPt    = *pPoint;
  NedPointLineBeg(&sPt);
  while(lines)
  {
    // skip whitespace, if any
    nChars = 0;
    while(NedCharType(NedPointCharGet(&sPt)) == IS_SPACE)
    {
      NedPointCharNext(&sPt);
      ++nChars;
    }
    if(nChars < indent)
      indent = nChars;
    if(NedPointStrCmp(&sPt, szComment) != 0)
      fAdding = TRUE;

    NedPointLineNext(&sPt);
    --lines;
  }

  // return values
  *pfAdding = fAdding;
  return indent;
}

/*-------------------------------------------------------------------------------------------------
  Insert a number of characters. Advances point to after the insert.
-------------------------------------------------------------------------------------------------*/
void NceFill(sNedPoint_t *pPoint, long nChars, const char c)
{
  NedEditorPosListAdjustIns(pPoint, nChars);
  while(nChars > 0)
  {
    NedUndoInsChar(NedEditorUndoList(), pPoint, c);
    --nChars;
  }
}

/*-------------------------------------------------------------------------------------------------
  Find the nearest word that does not exceed the desired line width. Returns actual width found.
-------------------------------------------------------------------------------------------------*/
long NceLineWordWidth(sNedPoint_t *pPoint, long width)
{
  sNedPoint_t   sOrg;
  sNedPoint_t   sThis;

  // will at least do 1 word
  sOrg  = *pPoint;
  NedPointWordNext(pPoint);
  sThis = *pPoint;
  while(1)
  {
    if(NedPointDifference(&sOrg, &sThis) > width)
      break;
    *pPoint = sThis;
    if(!NedPointWordNext(&sThis))
      break;
  }
  return NedPointDifference(&sOrg, pPoint);
}

/*-------------------------------------------------------------------------------------------------
  Returns TRUE (and prints msg) if file is read-only
-------------------------------------------------------------------------------------------------*/
bool_t NceRdOnly(const sNedFile_t *pFile)
{
  if(pFile->fRdOnly)
  {
    NedMsgPrintf(szRdOnly);
    NedEditorBeep();
  }
  return pFile->fRdOnly;
}

/*!------------------------------------------------------------------------------------------------
  Delete one character back, or cut the selected text to the clipboard

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditBackspace(void)
{
  sNedFile_t     *pFile       = NedEditorFileCur();
  sNedPoint_t     sPoint;
  bool_t          fNeedAdjust = FALSE;
  nedChar_t       c;

  if(NceRdOnly(pFile))
    return;

  if(pFile->fInSelect)
    NceCutSelected(TRUE);

  else
  {
    sPoint = *NedEditorCursor();
    c = NedPointCharGetPrev(&sPoint);
    if(c != NEDCHAR_EOF)
    {
      // deleting 1 char, adjust points
      NedEditorPosListAdjustDel(&sPoint, NedEditorCursor());

      // deleting a line, redraw screen
      if(c == '\n' || c =='\t')
        fNeedAdjust = TRUE;

      NedUndoDelBackspace(NedEditorUndoList(), NedEditorCursor());
      pFile->fDirty = TRUE;

      // deleted a line. If this was the topline, need to redraw whole screen
      if(fNeedAdjust)
      {
        if(!NedEditorAdjustCurFileFields())
          NedEditorInvalidateLines(pFile->curLine);
        else
          NedEditorGotoPoint(NedEditorCursor());
      }
      else
      {
        pFile->curCol = NedEditorColSet(&pFile->sCursor, pFile->curCol - 1);
        NedEditorInvalidateLine();
      }
    }
  }
  pFile->prefCol = pFile->curCol;
}

/*!------------------------------------------------------------------------------------------------
  Copy the selected text to the clipboard

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditCopy(void)
{
  sNedFile_t   *pFile      = NedEditorFileCur();
  sNedFile_t   *pClipboard = NedEditorClipboard(TRUE);
  long          diff;

  // if in select mode, copy the selected text to clipboard. Does not remove select
  if(pFile->fInSelect)
  {
    diff = NedPointClipCopy(&pClipboard->sCursor, &pFile->sSelect, &pFile->sCursor);

    NedEditorInvalidateSelected();
    pFile->fInSelect = FALSE;

    // if cursor is < select point, will be negative, so convert to positive
    if(diff < 0)
      diff = -1L * diff;
    NedMsgPrintf("Copied %ld bytes", diff);
  }
}

/*!------------------------------------------------------------------------------------------------
  Delete the selected text to the clipboard

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditCut(void)
{
  sNedFile_t     *pFile    = NedEditorFileCur();

  if(NceRdOnly(pFile))
    return;

  if(pFile->fInSelect)
    NedMsgPrintf("Cut %ld bytes", NceCutSelected(TRUE));
}

/*!------------------------------------------------------------------------------------------------
  Cut to end of line. Keeps appending to clipboard

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditCutEol(void)
{
  sNedFile_t     *pFile    = NedEditorFileCur();
  sNedPoint_t    *pCursor  = &pFile->sCursor;
  sNedPoint_t     ptEnd;

  if(NceRdOnly(pFile))
    return;

  NedEditorInvalidateLine();
  ptEnd = *pCursor;
  NedPointLineEnd(&ptEnd);
  NceCut(!NedEditor()->fClipAppend, pCursor, &ptEnd);
  NedEditor()->fClipAppend = TRUE;
  pFile->fDirty = TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Cut the line to the clipboard. Appends to clipboard

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditCutLine(void)
{
  sNedFile_t     *pFile    = NedEditorFileCur();
  sNedPoint_t    *pCursor  = &pFile->sCursor;
  sNedPoint_t     ptEnd;

  if(NceRdOnly(pFile))
    return;

  NedEditorInvalidateLines(pFile->curLine);
  ptEnd = *pCursor;
  NedPointLineBeg(pCursor);
  NedPointLineNext(&ptEnd);

  {
    long    line;
    long    colCur;
    long    colEnd;
    line = NedPointLineGet(pCursor, &colCur);
    line = NedPointLineGet(&ptEnd, &colEnd);
    NedLogPrintfEx(NEDLOG_CMD_EDIT, "NedCmdEditCutLine append %u, line %ld, curLine %ld, colCur %ld, colEnd %ld\n",
      NedEditor()->fClipAppend, line, pFile->curLine, colCur, colEnd);
  }

  NceCut(!NedEditor()->fClipAppend, pCursor, &ptEnd);
  NedEditor()->fClipAppend = TRUE;
  pFile->fDirty = TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Cut the word under the cursor. Appends to clipboard

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditCutWord(void)
{
  sNedFile_t     *pFile    = NedEditorFileCur();
  sNedPoint_t    *pCursor  = &pFile->sCursor;
  sNedPoint_t     ptEnd;

  if(NceRdOnly(pFile))
    return;

  NedEditorInvalidateLines(pFile->curLine);
  ptEnd = *pCursor;
  NedPointWordBeg(pCursor);
  NedPointWordNext(&ptEnd);

  {
    long    line;
    long    colCur;
    long    colEnd;
    line = NedPointLineGet(pCursor, &colCur);
    line = NedPointLineGet(&ptEnd, &colEnd);
    NedLogPrintfEx(NEDLOG_CMD_EDIT, "NedCmdEditCutWord append %u, line %ld, curLine %ld, colCur %ld, colEnd %ld\n",
      NedEditor()->fClipAppend, line, pFile->curLine, colCur, colEnd);
  }

  NceCut(!NedEditor()->fClipAppend, pCursor, &ptEnd);
  NedEditor()->fClipAppend = TRUE;
  pFile->fDirty = TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Delete the selected text to the clipboard, or delete the single character under cursor

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditDelete(void)
{
  sNedFile_t     *pFile = NedEditorFileCur();
  nedChar_t       c;

  if(NceRdOnly(pFile))
    return;

  if(pFile->fInSelect)
    NceCutSelected(TRUE);

  else
  {
    c = NedPointCharGet(NedEditorCursor());
    if(c == '\n')
      NedEditorInvalidateLines(pFile->curLine);
    if(NedUndoDelChar(NedEditorUndoList(), NedEditorCursor()))
    {
      pFile->fDirty = TRUE;
      NedEditorInvalidateLine();
    }
  }
}

/*!------------------------------------------------------------------------------------------------
  Enter and auto-indent to save level as current line.

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditEnter(void)
{
  sNedFile_t     *pFile   = NedEditorFileCur();
  sNedPoint_t     sLineBeg;
  long            indent  = 0;
  nedChar_t       c       = ' ';

  if(NceRdOnly(pFile))
    return;

  // if entering these character, cut the text and exit select
  if(pFile->fInSelect)
    NceCutSelected(TRUE);

  sLineBeg = *NedEditorCursor();
  if(NedUndoInsChar(NedEditorUndoList(), NedEditorCursor(), '\n'))
  {
    // determine indent based on previous line
    NedPointLineBeg(&sLineBeg);
    indent = NceIndent(&sLineBeg, &c);

    // insert indent
    NceInsCharRepeated(NedEditorCursor(), indent, c);

    // invalidate lines from here forward, they've all changed
    NedEditorInvalidateLines(pFile->curLine);

    // adjust for curLine, curCol and prefCol
    NedEditorAdjustCurFileFields();
    pFile->prefCol  = pFile->curCol;
    pFile->fDirty   = TRUE;
  }
}

/*!------------------------------------------------------------------------------------------------
  Tab key has different duties depending on select mode and where in the line the cursor is.
  Understands hardtabs `\t` vs spaces

  1. If in select mode, indent selected lines by 1 tab stop (a tab or # of spaces)
  2. If not in select mode, insert a single tab stop (a tab or # of spaces)
  2a. If not on the "indent" part of the line, always use spaces

  Tabs or spaces, depending on settings, used for indentation. Spaces are always used for alignment

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditIndent(void)
{
  sNedFile_t     *pFile = NedEditorFileCur();
  sNedPoint_t     sPtTop;
  sNceTopBot_t    sTop;
  long            lines;
  long            totalChars = 0L;
  unsigned        nChars = 0;
  nedChar_t       c;
  bool_t          fForceSpaces = FALSE;

  if(NceRdOnly(pFile))
    return;

  if(NedLogMaskGet() & NEDLOG_CMD_EDIT)
    NedEditorLogShow(NedEditor(), NEDEDITOR_DETAIL_FILE_LIST);

  // if NOT on indent part of the line, then always uses spaces
  if(!pFile->fInSelect)
  {
    sPtTop  = *NedEditorCursor();
    c = NedPointCharGetPrev(&sPtTop);
    if(c != NEDCHAR_EOF && c != '\t' && c!= '\n')
      fForceSpaces = TRUE;
    NedLogPrintfEx(NEDLOG_CMD_EDIT, "fForceSpaces %u\n", fForceSpaces);
  }

  // determine # and type of characters to use for indent
  c = NceLangIndent(pFile, &nChars, fForceSpaces);

  // indent entire selection
  pFile->fDirty = TRUE;
  if(pFile->fInSelect)
  {

    // when selecting, the bottom is at the start of the next line, so don't count it
    lines = NceGetSelectRows(&sTop);
    if(lines > 1 && NedPointIsAtBegLine(sTop.pPtBot))
      --lines;

    // make sure top starts at beginning of line
    sPtTop = *sTop.pPtTop;
    NedPointLineBeg(&sPtTop);
    NedLogPrintfEx(NEDLOG_CMD_EDIT, "NedCmdEditIndent topLine %ld, botLine %ld, fCursorIsTop %u\n",
      sTop.topLine, sTop.botLine, sTop.fCursorIsTop);
  }

  // NOT selecting, add spaces/tabs at current position
  else
  {
    sPtTop  = *NedEditorCursor();
    sTop.pPtBot = NedEditorCursor();
    lines   = 1;
    if(nChars)
      nChars -= ((pFile->curCol - 1) % nChars);

    NedEditorInvalidateLine();
  }
  NedLogPrintfEx(NEDLOG_CMD_EDIT, "NedCmdEditIndent lines %ld, nChars %u, curCol %ld\n",
    lines, nChars, pFile->curCol);

  while(lines)
  {
    NceInsCharRepeated(&sPtTop, nChars, c);
    NedPointLineNext(&sPtTop);
    totalChars += nChars;
    --lines;
  }

  // Advance the cursors
  if(pFile->fInSelect && !NedPointIsAtBegLine(sTop.pPtTop))
    NedPointCharAdvance(sTop.pPtTop, nChars);
  NedPointCharAdvance(sTop.pPtBot, totalChars);
  NedEditorAdjustCurFileFields();
  pFile->prefCol = pFile->curCol;
  NedEditorInvalidateSelected();

  if(NedLogMaskGet() & NEDLOG_CMD_EDIT)
    NedEditorLogShow(NedEditor(), NEDEDITOR_DETAIL_FILE_LIST);
}

/*!------------------------------------------------------------------------------------------------
  Insert the ASCII character. Tabs and Enter are handled separately.

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditInsertSelf(void)
{
  sNedFile_t     *pFile = NedEditorFileCur();

  if(NceRdOnly(pFile))
    return;

  if(NedEditorFileCur()->fRdOnly)
  {
    NedMsgPrintf("File is read-only");
    return;
  }

  // replace selected text with the character
  if(pFile->fInSelect)
    NceCutSelected(TRUE);

  // insert the character
  NedEditorPosListAdjustIns(NedEditorCursor(), 1);
  if(NedUndoInsChar(NedEditorUndoList(), NedEditorCursor(), NedEditor()->c))
  {
    pFile->fDirty = TRUE;
    if(NedEditor()->c == '\t')
      pFile->curCol = NedEditorColGet(NedEditorCursor());
    else
      ++pFile->curCol;
    if(pFile->curCol >= (pFile->leftCol + NedEditorScreenCols() - 1))
      NedEditorAdjustCurFileFields();
    NedEditorInvalidateLine();
  }
}

/*!------------------------------------------------------------------------------------------------
  Paste the clipboard to the current cursor position. Cursor is advanced to end of paste.

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditPaste(void)
{
  sNedPoint_t     sPoint;
  uint8_t        *pData;
  long            insertLen;

  if(NceRdOnly(NedEditorFileCur()))
    return;

  sPoint = NedEditor()->pClipboard->sCursor;
  NedPointFileTop(&sPoint);
  insertLen = NedPointFileSize(&sPoint);
  if(insertLen)
  {
    pData = malloc(insertLen);
    if(pData)
    {
      NedPointMemGet(pData, &sPoint, insertLen);
      NedEditorPosListAdjustIns(NedEditorCursor(), insertLen);
      NedUndoInsBlock(NedEditorUndoList(), NedEditorCursor(), pData, insertLen);
      NedEditorFileCur()->fDirty = TRUE;
      free(pData);
    }

    NedEditorBuildAroundCursor();
  }
  NedMsgPrintf("Pasted %ld chars", insertLen);
}

/*!------------------------------------------------------------------------------------------------
  Copy current selected text to the system Clipboard "cat ned.tmp | pbcopy"

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditPbCopy(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();
  sNedFile_t   *pClipboard = NedEditorClipboard(TRUE);
  sNedFile_t   *pFileTmp;
  long          diff;

    // if in select mode, copy the selected text to clipboard
  if(pFile->fInSelect)
  {
    diff = NedPointClipCopy(&pClipboard->sCursor, &pFile->sSelect, &pFile->sCursor);
    if(diff < 0)
      diff = -1L * diff;

    // copy entire clipboard to temporary file
    remove(szTmpFile);
    pFileTmp = NedFileNew(NULL, szTmpFile);
    if(!pFileTmp)
      NedMsgPrintf("internal error");
    else
    {
      NedFileClear(pFileTmp);
      NedPointClipCopy(&pFileTmp->sCursor, &pFile->sSelect, &pFile->sCursor);
      if(!NedFileSave(pFileTmp))
        NedMsgPrintf("couldn't save %s", NedFilePath(pFileTmp));
      else if(system("cat ned.tmp | pbcopy") < 0)
        NedMsgPrintf("pbcopy not supported");
      else
        NedMsgPrintf("Copied %ld bytes to system paste buffer", diff);
    }
    remove(szTmpFile);
  }
}

/*!------------------------------------------------------------------------------------------------
  Paste from the system clipboard into this file at the cursor position. "pbpaste > ned.tmp"
  Allow user to set tab size. Default keymap is Alt-V.

  @ingroup  ned_cmd
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditPbPaste(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();
  nedChar_t    *pData;
  long          insertLen = 0;

  // don't allow modifying a read-only file
  if(NceRdOnly(pFile))
    return;

  // get the pasted data (may be 0 length)
  insertLen = LONG_MAX;
  pData = NedEditorPbPaste(NULL, &insertLen);
  if(pData == NULL)
    NedMsgPrintf("pbpaste not supported");
  else if(insertLen)
  {
    NedEditorPosListAdjustIns(NedEditorCursor(), insertLen);
    NedUndoInsBlock(NedEditorUndoList(), NedEditorCursor(), pData, insertLen);
    NedEditorFileCur()->fDirty = TRUE;
    free(pData);

    NedEditorBuildAroundCursor();
    NedMsgPrintf("Pasted %ld chars from system", insertLen);
  }
}

/*!------------------------------------------------------------------------------------------------
  Redo last undo. Does nothihng if Undo never called.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditRedo(void)
{
  sNedFile_t   *pFile;
  long          pos;
  long          len;

  // adjust bookmarks, posHistory
  pFile = NedUndoRedoLen(NedEditorUndoList(), &pos, &len);
  if(pFile)
    NcePosListAdjustUndo(pFile, pos, len);

  // may switch files
  pFile = NedUndoRedo(NedEditorUndoList());
  if(pFile)
    NedEditorGotoPoint(&pFile->sCursor);
}

/*!------------------------------------------------------------------------------------------------
  Sort selected lines. First sort is in alphabetically. Second sort is reverse.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditSortLines(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();
  char          szLine[NEDCFG_MAX_WIDTH];
  sNceTopBot_t  sTopBot;
  sNedPoint_t   ptI;
  sNedPoint_t   ptJ;
  sNedPoint_t   ptJNext;
  long          diff;
  long          lineLen;
  long          i;
  long          j;
  long          lines;
  bool_t        fSwap;
  static bool_t fReverse = FALSE;
  int           ret;
  bool_t        fBotIsAtBeg = FALSE;

  if(NceRdOnly(pFile))
    return;

  if(!pFile->fInSelect)
  {
    NedMsgPrintf("Nothing selected to sort");
    return;
  }

  // allow reverse sorting by multiple sorts
  if(NedEditor()->pfnLastCmd == NedCmdEditSortLines)
    fReverse = fReverse ? FALSE : TRUE;

  pFile->fDirty = TRUE;
  NedEditorInvalidateSelected();
  lines = NceGetSelectRows(&sTopBot);

  // when pressing Ctrl-L to select lines, buttom cursor ends up on beg of following line
  // do NOT include that line in the sort
  if(lines > 1 && NedPointIsAtBegLine(sTopBot.pPtBot))
  {
    fBotIsAtBeg = TRUE;
    --lines;
  }

  // sort lines (bubble short greatest to bottom)
  for(i = 0; i < lines - 1; ++i)
  {
    NedPointInit(&ptI, pFile->pBufHead);
    NedPointLineGoto(&ptI, sTopBot.topLine + i);
    for(j = 0; j < lines - i - 1; ++j)
    {
      // swap if this line is greater than the next
      NedPointInit(&ptJ, pFile->pBufHead);
      NedPointInit(&ptJNext, pFile->pBufHead);
      NedPointLineGoto(&ptJ, sTopBot.topLine + j);
      NedPointLineGoto(&ptJNext, sTopBot.topLine + j + 1);
      diff = NedPointDifference(&ptJ, &ptJNext);
      NedAssert(diff >= 1);
      NedAssert(diff < sizeof(szLine));
      lineLen = NedPointStrGet((uint8_t *)szLine, &ptJ, diff);
      NedLogPrintfEx(NEDLOG_CMD_EDIT, "NedCmdEditSortLines()  i %ld, j %ld, lines %ld, lineLen %ld, diff %ld\n",
        i, j, lines, lineLen, diff);

      // if next line is less than this, swap
      fSwap = FALSE;
      ret = NedPointStrNCmp(&ptJNext, szLine, lineLen);
      if(fReverse && ret > 0)
        fSwap = TRUE;
      else if(!fReverse && ret < 0)
        fSwap = TRUE;

      if(fSwap)
      {
        NedLogPrintfEx(NEDLOG_CMD_EDIT, "swapped %s\n", szLine);
        NedUndoDelBlock(NedEditorUndoList(), &ptJ, &ptJNext);
        NedPointLineGoto(&ptJNext, sTopBot.topLine + j + 1);
        NedUndoInsBlock(NedEditorUndoList(), &ptJNext, (uint8_t *)szLine, lineLen);
      }
    }
  }

  // move cursor and select to top/bottom of sorted lines
  // remains in select so we can sort reverse
  NedPointInit(sTopBot.pPtTop, pFile->pBufHead);
  NedPointInit(sTopBot.pPtBot, pFile->pBufHead);
  NedPointLineGoto(sTopBot.pPtTop, sTopBot.topLine);
  NedPointLineGoto(sTopBot.pPtBot, sTopBot.botLine);
  if(!fBotIsAtBeg)
    NedPointLineEnd(sTopBot.pPtBot);

  NedMsgPrintf("Sorted %ld lines %s", lines, fReverse ? "(reverse)" : "");
}

/*!------------------------------------------------------------------------------------------------
  Toggle comments on/off for selected lines. Line comment depends on language.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditToggleCase(void)
{
  sNedFile_t     *pFile = NedEditorFileCur();

  char          szNew[NEDCFG_MAX_WIDTH];
  char          szOld[NEDCFG_MAX_WIDTH];
  sNedPoint_t   ptBeg;
  sNedPoint_t   ptEnd;
  long          len;
  nedStrCase_t  strCase;

  if(NceRdOnly(pFile))
    return;

  // determine length of sumbol
  ptBeg = *NedEditorCursor();
  ptEnd = ptBeg;
  NedPointWordBeg(&ptBeg);
  NedPointWordEnd(&ptEnd);
  len = NedPointDifference(&ptBeg, &ptEnd);
  NedAssert(len >= 0);
  if(len >= sizeof(szOld))
    NedMsgPrintf("Symbol too large");
  else
  {
    // copy and delete the old symbol
    pFile->fDirty = TRUE;
    NedPointStrGet((void *)szOld, &ptBeg, len);
    NedUndoDelBlock(NedEditorUndoList(), &ptBeg, &ptEnd);

    // toggle between lower and UPPER
    strCase = NedStrIsCase(szOld);
    if(strCase == IS_LOWER_CASE)
      strCase = IS_UPPER_CASE;
    else if(strCase == IS_UPPER_CASE)
      strCase = IS_LOWER_CASE;

    // toggle between camelCase, MixedCase, snake_case and CONSTANT_CASE
    else
    {
      if(strCase == IS_CONSTANT_CASE)
        strCase = IS_CAMEL_CASE;
      else
        ++strCase;
    }

    // convert to new case
    len = NedStrToCase(szNew, szOld, sizeof(szNew) - 1, strCase);
    NedLogPrintfEx(NEDLOG_CMD_EDIT, "NedCmdEditToggleCase() got %s, convert to %s, len %ld\n", szOld, szNew, len);
    if(len > 0)
      NedUndoInsBlock(NedEditorUndoList(), &ptBeg, (void *)szNew, len);
    NedEditorInvalidateLine();
  }
}


/*!------------------------------------------------------------------------------------------------
  Toggle comments on/off for selected lines. Line comment depends on language.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditToggleComment(void)
{
  sNedFile_t             *pFile       = NedEditorFileCur();
  const char             *szComment   = NedLangGet(NedFilePath(pFile))->aszLineComments[0];
  sNedPoint_t             sPt;
  sNedPoint_t             sPt2;
  sNceTopBot_t            sTopBot;
  long                    lines;
  long                    totalChars  = 0L;
  long                    indent;
  long                    posTop;
  long                    posBot;
  long                    nChars;
  bool_t                  fAdding;   // adding or removing comments

  if(NceRdOnly(pFile))
    return;

  // nothing to do, no line-comments for this language
  if(szComment == NULL)
    return;

  // in select, determine # of lines and 1st point pos
  pFile->fDirty = TRUE;
  if(pFile->fInSelect)
  {
    lines = NceExtendSelect(&sTopBot);
    NedEditorInvalidateSelected();
    sPt     = *sTopBot.pPtTop;
    if(lines > 1 && NedPointIsAtBegLine(sTopBot.pPtBot))
      --lines;
  }

  // not in select, single line
  else
  {
    NedEditorInvalidateLine();
    lines           = 1;
    sPt             = *NedEditorCursor();
    sTopBot.pPtTop  = NedEditorCursor();
    sTopBot.pPtBot  = NedEditorCursor();
  }
  posTop = NedPointPos(sTopBot.pPtTop);
  posBot = NedPointPos(sTopBot.pPtBot);

  // least indent is the indent, determine if adding or removing comments
  indent = NceGetMinIndent(&fAdding, lines, &sPt);

  NedPointLineBeg(&sPt);
  while(lines)
  {
    nChars = strlen(szComment) + 1;
    NedPointCharAdvance(&sPt, indent);
    if(fAdding)
    {
      NedUndoInsBlock(NedEditorUndoList(), &sPt, (void *)szComment, nChars - 1);
      NedUndoInsChar(NedEditorUndoList(), &sPt, ' ');
    }
    else
    {
      sPt2 = sPt;
      NedPointCharAdvance(&sPt2, nChars - 1);
      NedUndoDelBlock(NedEditorUndoList(), &sPt, &sPt2);
      if(NedCharType(NedPointCharGet(&sPt)) == IS_SPACE) 
        NedUndoDelChar(NedEditorUndoList(), &sPt);
      else
        --nChars;
    }

    totalChars += nChars;
    NedPointLineNext(&sPt);
    --lines;
  }

  // adding adds to position, removing subtracts
  if(fAdding == FALSE)
  {
    nChars      = -1L * nChars;
    totalChars  = -1L * totalChars;
  }
  NedLogPrintfEx(NEDLOG_CMD_EDIT, "NedCmdEditToggleComment posTop %ld, posBot %ld, nChars %ld, totalChars %ld\n",
    posTop, posBot, nChars, totalChars);
  if(!pFile->fInSelect)
    NedPointFileGotoPos(sTopBot.pPtTop, posTop + nChars);
  NedPointFileGotoPos(sTopBot.pPtBot, posBot + totalChars);

  NedEditorAdjustCurFileFields();
  pFile->prefCol = pFile->curCol;
}

/*!------------------------------------------------------------------------------------------------
  Undent the line or lines in selection

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditUndent(void)
{
  sNedFile_t     *pFile = NedEditorFileCur();
  sNedPoint_t     sPtTop;
  sNedPoint_t     sPt2;
  sNceTopBot_t    sTop;
  long            lines;
  long            diff;
  long            totalChars = 0L;
  unsigned        firstChars;
  bool_t          fFirst = TRUE;
  unsigned        nChars;
  unsigned        thisChars;
  unsigned        i;
  nedChar_t       c;

  if(NceRdOnly(pFile))
    return;

  // determine characters to use to indent, and how many
  c = NceLangIndent(pFile, &nChars, FALSE);

  // indent entire selection
  pFile->fDirty = TRUE;
  if(pFile->fInSelect)
  {
    lines = NceGetSelectRows(&sTop);
    if(lines > 1 && NedPointIsAtBegLine(sTop.pPtBot))
      --lines;
    sPtTop = *sTop.pPtTop;

    NedLogPrintfEx(NEDLOG_CMD_EDIT, "NedCmdEditUndent topLine %ld, botLine %ld, fCursorIsTop %u\n",
      sTop.topLine, sTop.botLine, sTop.fCursorIsTop);
  }

  // add chars to current line at cursor position
  else
  {
    NedEditorInvalidateLine();
    sPtTop  = *NedEditorCursor();
    sTop.pPtBot = NedEditorCursor();
    lines   = 1;
  }
  NedPointLineBeg(&sPtTop);

  NedLogPrintfEx(NEDLOG_CMD_EDIT, "NedCmdEditUndent lines %ld, nChars %u, curCol %ld\n", lines, nChars, pFile->curCol);

  while(lines)
  {
    c = NedPointCharGet(&sPtTop);
    if(c == ' ' || c == '\t')
    {
      sPt2 = sPtTop;
      NedPointWordNext(&sPt2);
      diff = NedPointDifference(&sPtTop, &sPt2);
      NedAssert(diff > 0L);
      thisChars = nChars;
      if(thisChars > diff)
        thisChars = (unsigned)diff;
      for(i=0; i<thisChars; ++i)
        NedUndoDelChar(NedEditorUndoList(), &sPtTop);
      if(fFirst)
      {
        fFirst = FALSE;
        firstChars = thisChars;
      }
      totalChars += thisChars;
    }

    if(!NedPointLineNext(&sPtTop))
      break;
    --lines;
  }

  // adjust cursors
  if(pFile->fInSelect && !NedPointIsAtBegLine(sTop.pPtTop))
    NedPointCharAdvance(sTop.pPtTop, -1L * firstChars);
  NedPointCharAdvance(sTop.pPtBot, -1L * totalChars);
  NedEditorAdjustCurFileFields();
  NedEditorInvalidateSelected();
  pFile->prefCol = pFile->curCol;
}

/*!------------------------------------------------------------------------------------------------
  Undo last edit operation.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditUndo(void)
{
  sNedFile_t *pFile = NedEditorFileCur();
  long        pos;
  long        len;

  // no longer in select when undoing
  pFile->fInSelect = FALSE;

  // adjust bookmarks, posHistory
  pFile = NedUndoUndoLen(NedEditorUndoList(), &pos, &len);
  if(pFile)
    NcePosListAdjustUndo(pFile, pos, len);

  // may switch files
  pFile = NedUndoUndo(NedEditorUndoList());
  if(pFile)
    NedEditorGotoPoint(&pFile->sCursor);
}

/*!------------------------------------------------------------------------------------------------
  Wrap selected text. User can chose wrap length.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdEditWrapText(void)
{
  char                  szWidth[32];
  sNedFile_t           *pFile = NedEditorFileCur();
  sNceTopBot_t          sTopBot;
  long                  wrapLen;
  long                  len;
  sNedPtIndent_t        sIndent;
  nedChar_t            *pData;

  if(NceRdOnly(pFile))
    return;

  // nothing to do if no lines selected
  if(!pFile->fInSelect)
  {
    NedMsgPrintf("Nothing selected");
    return;
  }

  // determine wrap length. Also give user chance to ESC
  do
  {
    wrapLen = pFile->wrapLen;
    len = snprintf(szWidth, sizeof(szWidth) - 1, "%ld", wrapLen);
    szWidth[len] = '\0';
    len = NedMsgLineEdit(szWidth, sizeof(szWidth) - 1, szWrapAtWidth, NULL, NULL);
    if(len < 0)
    {
      NedMsgClear();
      return;
    }
    wrapLen = atol(szWidth);
  } while(wrapLen <= 0L);

  // expand the selection to full lines
  NceExtendSelect(&sTopBot);
  NedLogPrintfEx(NEDLOG_CMD_EDIT, "topLine %ld, botLine %ld\n", sTopBot.topLine, sTopBot.botLine);

  // get indent from 1st line
  sIndent.nChars = NceIndent(sTopBot.pPtTop, &sIndent.c);
  sIndent.nCols  = sIndent.nChars;
  if(sIndent.c == '\t')
    sIndent.nCols *= pFile->tabSize;
  NedLogPrintfEx(NEDLOG_CMD_EDIT, "sIndent.c = %02x, nChars %u, nCols %u\n", (unsigned)(sIndent.c), sIndent.nChars, sIndent.nCols);

  // get current lines into undo
  NedUndoEditDone(NedEditorUndoList());
  UndoDel(NedEditorUndoList(), sTopBot.pPtTop, NedPointDifference(sTopBot.pPtTop, sTopBot.pPtBot));

  // actually replace the text and add new text to undo
  pFile->fDirty = TRUE;
  pFile->wrapLen = wrapLen;
  NedPointWrapText(&sIndent, sTopBot.pPtTop, sTopBot.pPtBot, wrapLen);
  len = NedPointDifference(sTopBot.pPtTop, sTopBot.pPtBot);
  if(len > 0L)
  {
    pData = malloc(len);
    if(pData)
    {
      len = NedPointMemGet(pData, sTopBot.pPtTop, len);
      UndoIns(NedEditorUndoList(), sTopBot.pPtTop, len, pData);
      free(pData);
    }
  }

  // show the updated selected lines. Doesn't turn off select
  NedEditorInvalidateSelected();
  pFile->fInSelect = FALSE;
  NedMsgPrintf("Wrapped text at %ld, indent %ld", wrapLen, sIndent.nCols);
  NedEditorBuildAroundCursor();
}
