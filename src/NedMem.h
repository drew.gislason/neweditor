/*!************************************************************************************************
  @file NedMem.h

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#ifndef NED_MEM_H
#define NED_MEM_H

#include "Ned.h"


// some processors like ARM require alignment for structures, unsigned, etc...
#ifndef NEDMEM_ALIGN
  typedef size_t nedMemSize_t;
  #define NEDMEM_ALIGN  sizeof(nedMemSize_t)
#endif

// can point to other functions that allocate/free/realloc
#ifndef NEDMEM_ALTDEF
 #define NEDMEM_MALLOC    malloc
 #define NEDMEM_FREE      free
 #define NEDMEM_REALLOC   realloc
#endif

// for filling buffers to detect alloc/free problems
#define NEDDBG_ALLOC  '@'
#define NEDDBG_FREE   '&'

void         *NedMemAlloc      (nedMemSize_t size);
void         *NedMemCalloc     (nedMemSize_t size);
void          NedMemFree       (void *p);
void         *NedMemRealloc    (void *p, nedMemSize_t size);
void          NedMemCountInc   (void *p);
void          NedMemCountFree  (void *p);
unsigned      NedMemCountGet   (void *p);
nedMemSize_t  NedMemAllocTotal (void);
void          NedMemTotalReset (void);
bool_t        NedMemIsMem      (const void *p);

#endif // NED_MEM_H
