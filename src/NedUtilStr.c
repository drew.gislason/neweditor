/*!************************************************************************************************
  @file NedStrUtil.c

  @brief  Ned String Utilities implementation - various things to manipulate strings

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @ingroup  ned_utils

*///***********************************************************************************************
#include "NedUtilStr.h"
#include "NedLog.h"

char szNedUtilPath[PATH_MAX];

/*!------------------------------------------------------------------------------------------------
  Returns a string buffer of size PATH_MAX
  @return   ptr to temporary string of size PATH_MAX
*///-----------------------------------------------------------------------------------------------
char * NedStrTmpBuf(void)
{
  return szNedUtilPath;
}

/*!------------------------------------------------------------------------------------------------
  Returns size of the temporary buffer
  @return size of temporary buffer
*///-----------------------------------------------------------------------------------------------
unsigned NedStrTmpBufMaxLen(void)
{
  return (unsigned)(sizeof(szNedUtilPath) - 1);
}

/*!------------------------------------------------------------------------------------------------
  Skips preceding and trailing whitespace on szSrc. Fills szDst with up to len characters. always
  NULL terminates szDst (assume len i.

  @param    szDst     destination for argument
  @param    szSrc     source string
  @param    size      max length of destination string (sizeof() -1), must be at least 1
  @return  pointer to szSrc after processing.
*///-----------------------------------------------------------------------------------------------
const char * NedStrArgCpy(char *szDst, const char *szSrc, size_t size)
{
  if(size)
  {
    --size;

    // skip preceding whitespace
    while(*szSrc && isspace(*szSrc))
      ++szSrc;

    // copy as much of arg as we can (up to size-1)
    while(*szSrc && !isspace(*szSrc))
    {
      if(size)
      {
        *szDst++ = *szSrc;
        --size;
      }
      ++szSrc;
    }

    // skip post whitespace
    while(*szSrc && isspace(*szSrc))
      ++szSrc;
  }
  *szDst = '\0';

  return szSrc;
}

/*!------------------------------------------------------------------------------------------------
  Return the character type, useful in finding end of word, and other things.

  @param    c      character (0x00-0xff)

  @return   One of IS_ALNUM, IS_SPACE, IS_LF, IS_EOF, IS_PUNCT
*///-----------------------------------------------------------------------------------------------
nedCharType_t NedCharType(nedChar_t c)
{
  nedCharType_t type;
  if( isalnum((int)c) || (c == '_') )
    type = IS_ALNUM;
  else if( (c == ' ')  || (c == '\t') )
    type = IS_SPACE;
  else if( (c == '\n') || (c == '\r') )
    type = IS_LF;
  else if(c == NEDCHAR_EOF)
    type = IS_EOF;
  else
    type = IS_PUNCT;
  return type;
}

/*!------------------------------------------------------------------------------------------------
  Return the case of the string. Examples are:

  lowercase, UPPERCASE, camelCase, MixedCase, snake_case, CONSTANT_CASE

  @param    pThis      pointer to data
  @param    pThat      pointer to data
  @param    len        length of data

  @return   0 if strings compare the same, -1 if pThis < pThat, 1 if pThis > pThat
*///-----------------------------------------------------------------------------------------------
nedStrCase_t NedStrIsCase(const char *sz)
{
  nedStrCase_t  strCase     = IS_LOWER_CASE;
  bool_t        fFirstLower = FALSE;    // lowercase, chamelCase
  bool_t        fHasLower   = FALSE;    // lowercase, camelCase, MixedCase, snake_case
  bool_t        fHasUpper   = FALSE;    // UPPERCASE, camelCase, MixedCase, CONSTANT_CASE
  bool_t        fHasSnake   = FALSE;    // snake_case, CONSTANT_CASE

  if(islower(*sz))
    fFirstLower = TRUE;

  while(*sz)
  {
    if(islower(*sz))
      fHasLower = TRUE;
    else if(isupper(*sz))
      fHasUpper = TRUE;
    else if(*sz == '_')
      fHasSnake = TRUE;
    ++sz;
  }

  if(fHasSnake)
  {
    if(!fHasLower)
      strCase = IS_CONSTANT_CASE;
    else
      strCase = IS_SNAKE_CASE;
  }
  else
  {
    if(fHasLower && fHasUpper)
    {
      if(fFirstLower)
        strCase = IS_CAMEL_CASE;
      else
        strCase = IS_MIXED_CASE;
    }
    else if(fHasUpper)
      strCase = IS_UPPER_CASE;
    else
      strCase = IS_LOWER_CASE;
  }

  return strCase;
}

/*!------------------------------------------------------------------------------------------------
  Convert string to the new case. Note that some cases don't convert well. For example, lowercase
  can't convert to camelCase, but can to UPPERCASE. Converting from MixedCase to lowercase is a
  one way street (can't convert back), as the string no longer indicates words within the string.

  case are: lower, UPPER, camelCase, MixedCase, snake_case, CONSTANT_CASE

  @param    szNew       pointer to asciiz string to convert to
  @param    szOld       pointer to asciiz string to convert from
  @param    maxLen      maxLen of dst string (may grow from old if snake_case)
  @param    strCase     new case desired (e.g. IS_CAMEL_CASE)

  @return   length of new string
*///-----------------------------------------------------------------------------------------------
size_t NedStrToCase(char *szNew, const char *szOld, size_t maxLen, nedStrCase_t strCase)
{
  size_t        len         = 0;
  bool_t        fFirstChar  = TRUE;  // first alpha character
  nedStrCase_t  oldCase     = NedStrIsCase(szOld);

  // nothing to do
  if(oldCase == strCase)
    return strlen(szOld);

  // handle lower and UPPER case as they are easy, so is CONSTANT_CASE to CONSTANT_CASE
  if(strCase == IS_LOWER_CASE || strCase == IS_UPPER_CASE)
  {
    while(*szOld && len < maxLen)
    {
      // remove underscores for lower and UPPER cases
      if(*szOld != '_')
      {
        *szNew = (strCase == IS_LOWER_CASE) ? tolower(*szOld) : toupper(*szOld);
        ++szNew;
        ++len;
      }
      ++szOld;
    }
  }

  // must be camelCase, MixedCase, snake_case, CONSTANT_CASE
  else
  {
    while(*szOld && len < maxLen)
    {
      // first alpha char is special on camelCase
      if(fFirstChar && isalpha(*szOld))
      {
        if(strCase == IS_CAMEL_CASE || strCase == IS_SNAKE_CASE)
          *szNew = tolower(*szOld);
        else
          *szNew = toupper(*szOld);
        fFirstChar = FALSE;
      }

      else
      {
        // on snake_case or CONSTANT_CASE word separator
        if(*szOld == '_')
        {
          if(strCase == IS_SNAKE_CASE || strCase == IS_CONSTANT_CASE)
            *szNew = *szOld;

          // remove _ and make 1st char of Word uppercase for cameCase and MixedCase
          else 
          {
            ++szOld;
            *szNew = toupper(*szOld);
          }
        }

        // on a camelCase or MixedCase word separator
        else if(isupper(*szOld) && (oldCase != IS_CONSTANT_CASE))
        {
          if(strCase == IS_SNAKE_CASE || strCase == IS_CONSTANT_CASE)
          {
            *szNew++ = '_';
            ++len;
            if(len < maxLen)
              *szNew = (strCase == IS_SNAKE_CASE) ? tolower(*szOld) : *szOld;
          }
          else
            *szNew = *szOld;
        }

        // normal (not 1st) character in word, always lower unless CONSTANT_CASE
        else
        {
          if(strCase == IS_CONSTANT_CASE)
            *szNew = toupper(*szOld);
          else
            *szNew = tolower(*szOld);
        }
      }
      ++szNew;
      ++szOld;
      ++len;
    }
  }

  // add terminating NUL if room
  if(len < maxLen)
    *szNew = '\0';

  return len;
}

/*!------------------------------------------------------------------------------------------------
  From the time parameter, get date/time in ISO 8601 string form. Example: "2019-12-18T14:58:01".
  Local time only. Knows nothing about time zones. Returns separate static string from
  NedStrDateTimeCur()

  @param    time      current date/time in time_t format (see <time.h>)
  @return   pointer to static date/time string built from time_t
*///-----------------------------------------------------------------------------------------------
const char * NedStrDateTime(time_t time)
{
  struct tm  *pInfo;
  static char szDateTime[22];

  pInfo = localtime(&time);
  snprintf(szDateTime, sizeof(szDateTime), "%04i-%02i-%02iT%02i:%02i:%02i",
          1900+pInfo->tm_year, pInfo->tm_mon+1, pInfo->tm_mday, 
          pInfo->tm_hour,pInfo->tm_min,pInfo->tm_sec);
  return (const char *)szDateTime;
}

/*!------------------------------------------------------------------------------------------------
  Get the current date/time in ISO 8601 form "2019-10-23T08:15:30". Local time only. Knows
  nothing about time zones. Returns separate static string from NedStrDateTime()

  @return   pointer to static date/time string
*///-----------------------------------------------------------------------------------------------
const char * NedStrDateTimeCur(void)
{
  time_t      t;
  struct tm  *pInfo;
  static char szDateTime[22];

  time(&t);
  pInfo = localtime(&t);
  snprintf(szDateTime, sizeof(szDateTime), "%04i-%02i-%02iT%02i:%02i:%02i",
          1900+pInfo->tm_year, pInfo->tm_mon+1, pInfo->tm_mday, 
          pInfo->tm_hour,pInfo->tm_min,pInfo->tm_sec);
  return (const char *)szDateTime;
}

/*!------------------------------------------------------------------------------------------------
  Returns the user home folder name (from environment $HOME). For use with '~' in filenames.

  @return   Returns name. 
*///-----------------------------------------------------------------------------------------------
const char * NedStrHomeFolder(void)
{
  return getenv("HOME");
}

/*!------------------------------------------------------------------------------------------------
  Finds the last slash in a full path (e.g. /file/path/name, find "/name"). Works with slashes
  and backslashes.

  @param       szFullPath    pointer to a file or dir path (relative or absolute).

  @return   Returns last slash/backslash
*///-----------------------------------------------------------------------------------------------
const char * NedStrLastSlash(const char *szFullPath)
{
  const char *sz          = szFullPath;
  const char *szLastSlash = NULL;

  sz = szFullPath;
  while(sz && *sz)
  {
    if((*sz == '/') || (*sz == '\\'))
      szLastSlash = sz;
    ++sz;
  }

  return szLastSlash;
}

/*!------------------------------------------------------------------------------------------------
  Finds the previous slash in a full path. Example:

  if full path is "/Users/drewg/Work/Git/filename.c", and psz is on "filename.c", would return
  "Git/filename.c", one slash back.

  Behavior is undefined if psz does not point into szFullPath string.

  @param       szFullPath    pointer to a file or dir path (relative or absolute).
  @param       psz           pointer to any part of szFullPath.

  @return   Returns last slash/backslash
*///-----------------------------------------------------------------------------------------------
const char *NedStrPrevSlash(const char *szFullPath, const char *psz)
{
  bool_t    fFoundSlash = FALSE;

  while(TRUE)
  {
    if((*psz == '/') || (*psz == '\\'))
    {
      if(fFoundSlash)
      {
        psz = psz + 1;
        break;
      }
      else
        fFoundSlash = TRUE;
    }
    if(psz <= szFullPath)
      break;
    --psz;;
  }

  return psz;
}

/*!------------------------------------------------------------------------------------------------
  Get the very last character of this string

  @param      szStr       Pointer to string

  @return   Return NULL if character not found, or ptr to character if found.
*///-----------------------------------------------------------------------------------------------
char NedStrLastChar(const char *szStr)
{
  char    c = '\0';
  size_t  len;

  len = strlen(szStr);
  if(len)
    c = szStr[len - 1];

  return c;
}

/*!------------------------------------------------------------------------------------------------
  Finds the last occurence of the character.

  @param      szStr       Pointer to string
  @param      c           character to search for

  @return   Return NULL if character not found, or ptr to character if found.
*///-----------------------------------------------------------------------------------------------
const char *NedStrLastCharOf(const char *szStr, char c)
{
  const char *psz = NULL;

  if(szStr && c)
  {
    while(*szStr)
    {
      if(*szStr == c)
        psz = szStr;
      ++szStr;
    }
  }
  return psz;
}


/*!------------------------------------------------------------------------------------------------
  Finds the name part of a full path (e.g. /file/path/name, finds "name"). This is a string only
  manipulation.

  Examples:

      This finds                                 | That
      ------------------------------------------ | ------------
      ~/Work/myfile.c                            | myfile.c
      C:\work\things\hello.py                    | hello.py
      myfile.c                                   | myfile.c
      /dir/only/                                 | "" (empty string)
      .                                          | "" (empty string)
      ..                                         | "" (empty string)

  @param       szFullPath    pointer to a file or dir path (relative or absolute).

  @return   Returns name. 
*///-----------------------------------------------------------------------------------------------
const char * NedStrNameOnly(const char *szFullPath)
{
  const char *pszName;
  const char *pszLastSlash;

  // finds the last slash, if no slash return name only
  pszLastSlash = NedStrLastSlash(szFullPath);
  if(strcmp(szFullPath, ".") == 0 || strcmp(szFullPath, "..") == 0)
    pszName = &szFullPath[strlen(szFullPath)];
  else if(pszLastSlash == NULL)
    pszName = szFullPath;
  else
    pszName = (pszLastSlash + 1);
  return pszName;
}

/*!------------------------------------------------------------------------------------------------
  Similar to NedStrNameOnly(), but finds the name of the file or directory, without the path part

  Examples:

      This becomes                               | That
      ------------------------------------------ | ------------
      ~/Work/myfile.c                            | myfile.c
      C:\work\things\hello.py                    | hello.py
      myfile.c                                   | myfile.c
      /dir/only/                                 | "only/"
      .                                          | "."
      ..                                         | ".."

  @param       szFullPath    pointer to a file or dir path (relative or absolute).

  @return   Returns name or dir part of string
*///-----------------------------------------------------------------------------------------------
const char *NedStrNameDirOk(const char *szFullPath)
{
  const char *pszName;
  const char *pszPrevSlash  = NULL;
  const char *pszSlash      = NULL;
  const char *psz;

  if(strcmp(szFullPath, ".") == 0 || strcmp(szFullPath, "..") == 0)
    pszName = szFullPath;
  else
  {
    psz = szFullPath;
    while(psz && *psz)
    {
      if(*psz == '/' || *psz == '\\')
      {
        if(pszSlash)
          pszPrevSlash = pszSlash;
        pszSlash = psz;
      }
      ++psz;
    }

    if(pszSlash == NULL)
      pszName = szFullPath;
    else if(pszSlash[1] == '\0')
    {
      if(pszPrevSlash)
        pszName = pszPrevSlash + 1;
      else
        pszName = szFullPath;
    }
    else
      pszName = pszSlash + 1;
  }

  return pszName;
}

/*!------------------------------------------------------------------------------------------------
  Assumes it's a folder if it ends in a slash or is "." or ".."

  Examples:

      This finds                                 | That
      ------------------------------------------ | ------------
      ~/Work/myfile.c                            | NULL
      /dir/only/                                 | "only/"
      .                                          | "."
      ..                                         | ".."

  @param       szPath    pointer to a file or dir path (relative or absolute).

  @return   TRUE if this is a directory
*///-----------------------------------------------------------------------------------------------
bool_t NedStrIsFolder(const char *szPath)
{
  const char *psz;
  if(strcmp(szPath, ".") == 0 || strcmp(szPath, "..") == 0)
    return TRUE;
  else
  {
    psz = NedStrLastSlash(szPath);
    if(psz && psz[1] == '\0')
      return TRUE;
  }
  return FALSE;
}


/*!------------------------------------------------------------------------------------------------
  Finds the file extension given a asciiz filename.

  Examples:

      This finds        | That
      ----------------- | ------------
      myfile.c          | .c
      hello.py          | .py
      file.what.hpp     | .hpp
      makefile          | NULL

  @param       szFilename    pointer to filename string

  @return   Returns pointer to the file extension, which may be NULL if no extension
*///-----------------------------------------------------------------------------------------------
const char *NedStrExt(const char *szFilename)
{
  return NedStrLastCharOf(NedStrNameOnly(szFilename), '.');
}

/*!------------------------------------------------------------------------------------------------
  Gets a dir out of a full path (e.g. /file/path/name, gets "/file/path/"). Not thread or reuse
  safe (uses static buffer). Does not check for actual existence path. This is a string
  manipulation only. Limited to strs < PATH_MAX.

  Examples:

      This becomes                               | That
      ------------------------------------------ | ------------
      ~/Work/myfile.c                            | ~/Work/
      /Users/drewgislason/Git/ned/src/NedFile.c" | /Users/drewgislason/Git/ned/src/
      C:\work\things\hello.py                    | C:/work/things/
      myfile.c                                   | "" (empty)
      .../too long path/...                      | "" (empty)

  @param       szFullPath    pointer to a file or dir path (relative or absolute).

  @return   Returns path without filename.
*///-----------------------------------------------------------------------------------------------
const char * NedStrDirOnly(const char *szFullPath)
{
  static char   szDir[PATH_MAX];
  const char   *sz;
  unsigned      len = 0;

  NedAssertDbg(szFullPath);

  // no dir part
  sz = NedStrNameOnly(szFullPath);
  if(sz != szFullPath)
  {
    len = (unsigned)(sz - szFullPath);
    if(len < PATH_MAX)
      memcpy(szDir, szFullPath, len);
    else
      len = 0;
  }
  szDir[len] = '\0';

  return szDir;
}

/*!------------------------------------------------------------------------------------------------
  Converts from the ~/ home character to the actual home folder (from environment $HOME). This is
  a string manipulation only. Limited to strs < PATH_MAX. As '~' is a legal filename character,
  only converts anything if string starts wtih "~/".

  Examples:

      This becomes        | That
      ------------------- | ------------
      ~/Work/myfile.c     | /Users/drewgislason/Work/myfile.c
      ~myfile.c           | ~myfile.c

  @param       szFullPath    pointer to a file or dir path, relative or absolute.

  @return   Returns path without filename.
*///-----------------------------------------------------------------------------------------------
const char *NedStrHomePath(const char *szFullPath)
{
  const char   *sz;

  NedAssertDbg(szFullPath);

  if(szFullPath && (*szFullPath == '~') && (szFullPath[1] == '/'))
  {
    sz = NedStrHomeFolder();
    strcpy(szNedUtilPath, sz ? sz : "");
    strcat(szNedUtilPath, szFullPath + 1);
    szFullPath = szNedUtilPath;
  }

  return szFullPath;
}

/*!------------------------------------------------------------------------------------------------
  Gets a dir out of a full path (e.g. /file/path/name, gets "/file/path/"), but also convert the 
  home folder '~' if present to actual folder. Not thread or reuse safe (uses static buffer). Does
  not check for actual existence path. This is a string manipulation only. Limited to
  strs < PATH_MAX.

  Examples:

      This becomes                               | That
      ------------------------------------------ | ------------
      ~                                          | /Users/drewg/
      ~/Work/myfile.c                            | /Users/drewg/Work/
      /Users/drewgislason/Git/ned/src/NedFile.c  | /Users/drewg/Git/ned/src/
      C:\work\things\hello.py                    | C:/work/things/
      myfile.c                                   | "" (empty)
      .                                          | .
      ..                                         | ..

  @param       szFullPath    pointer to a file or dir path (relative or absolute).

  @return   Returns path without filename.
*///-----------------------------------------------------------------------------------------------
const char *NedStrDirOnlyHome(const char *szFullPath)
{
  static char   szDir[PATH_MAX];
  const char   *sz;
  unsigned      len       = 0;
  bool_t        lenHome   = 0;

  NedAssertDbg(szFullPath);

  // if no dir part, return empty dir string
  sz = NedStrNameOnly(szFullPath);
  if(sz != szFullPath)
  {
    // if home folder, we'll be adding that in
    if(*szFullPath == '~')
    {
      lenHome = strlen(NedStrHomeFolder());
      ++szFullPath;
    }

    len = (unsigned)(sz - szFullPath);
    if(len + lenHome < PATH_MAX)
    {
      if(lenHome)
        memcpy(szDir, NedStrHomeFolder(), lenHome);
      memcpy(&szDir[lenHome], szFullPath, len);
      len += lenHome;
    }
    else
      len = 0;
  }
  szDir[len] = '\0';

  return szDir;
}

/*!------------------------------------------------------------------------------------------------
  Gets a name that will fit into the width. Only includes (partial) path if the name is not unique
  in the list of files. Width must be < PATH_MAX.

  Not thread safe.

  Examples with /users/drewg/folder/file.c:

      /Users/drewgislason/folder/file.c      width 50
      ...er/file.c                           width 12

  @param    szFullPath    pointer to a file or dir path (relative or absolute).
  @param    width         visible width of string (needs +1 char for '\0')

  @return   A pointer to a static string that fits in the width
*///-----------------------------------------------------------------------------------------------
const char * NedStrFitName(const char *szFullPath, unsigned width)
{
  static char   szTmpName[PATH_MAX];
  const char   *szFitName;
  unsigned      len;

  NedAssertDbg(szFullPath);

  // don't allow too large of a width
  if(width > PATH_MAX-1)
    width = PATH_MAX-1;

  len = strlen(szFullPath);
  if(len <= width)
  {
    strcpy(szTmpName, szFullPath);
  }
  else
  {
    if( width > sizeof(szTmpName) - 1 )
      width = sizeof(szTmpName) - 1;
    if(width > 3)
      len = width - 3;
    else
      len = width;
    szFitName = &szFullPath[strlen(szFullPath) - len];
    if(width < 4)
    {
      memcpy(szTmpName, "...", width);
      szTmpName[width] = '\0';
    }
    else
    {
      strcpy(szTmpName, "...");
      memcpy(&szTmpName[3], szFitName, len);
      szTmpName[3+len] = '\0';
    }
    szFitName = szTmpName;
  }

  return szTmpName;
}


/*!------------------------------------------------------------------------------------------------
  Append the filename to the path. String manipulation only. If returns FALSE (too long), then
  szPath is unchanged. Uses PATH_MAX as maximum size.

  Examples: "~","file.c" becomes "~/file.c"

  @param    szPath    pointer to path string to have name appended
  @param    szName    pointer to filename string
  @return   FALSE if string(s) too long for a path
*///-----------------------------------------------------------------------------------------------
bool_t NedStrAppendName(char *szPath, const char *szName)
{
  unsigned    len;

  if(strlen(szPath) + strlen(szName) + 1 > PATH_MAX)
    return FALSE;

  len = strlen(szPath);
  if(len > 0 && ((szPath[len-1] != '/') && (szPath[len-1] != '\\')))
    strcat(szPath, "/");
  strcat(szPath, szName);

  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  is the memory filled with the byte?

  @param    s         pointer to array
  @param    c         byte (0-255)
  @param    n         length of array

  @return   TRUE if filled (or n==0), FALSE if not.
*///-----------------------------------------------------------------------------------------------
bool_t memisfilled(const void *s, int c, size_t n)
{
  const uint8_t  *p  = s;
  uint8_t         fc = c;   // fill char

  while(s != NULL && n)
  {
    --n;
    if(*p != fc)
      return FALSE;
    ++p;
  }
  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Search backward in array for the character as some C environments don't have memrchr().

  @param    s         pointer to array
  @param    c         byte (0-255)
  @param    n         length of array

  @return   ptr to c if found, NULL if not
*///-----------------------------------------------------------------------------------------------
void * NedMemRChr(const void *s, int c, size_t n)
{
  const uint8_t  *p;
  uint8_t         byte = (uint8_t)c;
  bool_t          fFound = FALSE;

  // no length, no string, not found
  if((n < 1) || (s==NULL))
    return NULL;

  p = (uint8_t *)s + (n-1);
  while(TRUE)
  {
    if(*p == byte)
    {
      fFound = TRUE;
      break;
    }
    if(p == s)
      break;
    --p;
  }

  return (fFound ? (void *)p : NULL);
}

/*!------------------------------------------------------------------------------------------------
  Compare 2 strings, case insensitive, as some C environments don't have memicmp().

  @param    pThis      pointer to data
  @param    pThat      pointer to data
  @param    len        length of data

  @return   0 if strings compare the same, -1 if pThis < pThat, 1 if pThis > pThat
*///-----------------------------------------------------------------------------------------------
int NedMemICmp(const void *pThis, const void *pThat, size_t len)
{
  const uint8_t   *pStr1 = pThis;
  const uint8_t   *pStr2 = pThat;

  while(len)
  {
    if(toupper(*pStr1) < toupper(*pStr2))
      return -1;
    else if(toupper(*pStr1) > toupper(*pStr2))
      return 1;
    ++pStr1;
    ++pStr2;
    --len;
  }
  return 0;
}

/*!------------------------------------------------------------------------------------------------
  Search string for character, case insensitive.

  @param    pThis      pointer to data
  @param    pThat      pointer to data
  @param    len        length of data

  @return   0 if strings compare the same, -1 if pThis < pThat, 1 if pThis > pThat
*///-----------------------------------------------------------------------------------------------
char * NedStrIChr(const char *sz, int c)
{
  char * szFound = NULL;
  while(sz && *sz)
  {
    if(toupper(c) == toupper(*sz))
    {
      szFound = (char *)sz;
      break;
    }
    ++sz;
  }
  return szFound;
}

/*!------------------------------------------------------------------------------------------------
  Like strncpy(), but resulting string is always '\0' terminated.

  @param    szDst     pointer to destination string
  @param    szSrc     pointer to source string
  @param    sizeDst   size of destination string

  @return   length of szDst (may be 0)
*///-----------------------------------------------------------------------------------------------
size_t NedStrZCpy(char *szDst, const char *szSrc, size_t sizeDst)
{
  strncpy(szDst, szSrc, sizeDst);
  szDst[sizeDst - 1] = '\0';
  return strlen(szDst);
}

/*!------------------------------------------------------------------------------------------------
  Insert a substring. Resulting string is always '\0' terminated. May truncate right size of
  szDst string. May copy only some of szSrc string, if too long.

  Example: szDst="ac", offset=1, sizeDst=5, szSrc="b", reesults in szDst="abc", return 1

  @param    szDst     pointer to destination string (szDst MUST be n+1 in size)
  @param    offset    0-n
  @param    sizeDst   size of destination string
  @param    szSrc     pointer to source string

  @return   length pasted
*///-----------------------------------------------------------------------------------------------
size_t NedStrIns(char *szDst, size_t offset, size_t sizeDst, const char *szSrc)
{
  size_t  lenDst;
  size_t  lenPaste = 0;
  size_t  lenMove;

  // nothing to do if bad input parameters
  if(sizeDst > 0 && offset < sizeDst)
  {
    // keep lenDst in valid range
    lenDst = strlen(szDst);
    if(lenDst >= sizeDst)
    {
      lenDst = sizeDst - 1;
      szDst[lenDst] = '\0';
    }

    // keep offset in valid range
    if(offset > lenDst)
      offset = lenDst;

    // keep paste len in valid range
    lenPaste = strlen(szSrc);
    if(offset + lenPaste >= sizeDst)
      lenPaste = (sizeDst - offset) - 1;

    if(lenPaste)
    {
      // make room for paste if offset in middle of string
      if(offset + lenPaste < sizeDst - 1)
      {
        lenMove = lenDst - offset;
        if(offset + lenPaste + lenMove >= sizeDst)
          lenMove = sizeDst - (offset + lenPaste);
        memmove(&szDst[offset], &szDst[offset + lenPaste], lenMove + 1);
      }

      // paste is large enough, right-hand side is truncated
      else
        szDst[sizeDst - 1] = '\0';

      memcpy(&szDst[offset], szSrc, lenPaste);
    }
  }

  return lenPaste;
}

/*!------------------------------------------------------------------------------------------------
  Converts wide character to UTF-8 string, for example, 0x2020 (dagger) is "/xE2/x80/xA0". Note the
  string is global, so use immediately, or copy to something else. Guaranteed to be 5 bytes or
  less. Includes NUL at end of string.

  00000000 -- 0000007F:   0xxxxxxx
  00000080 -- 000007FF:   110xxxxx 10xxxxxx
  00000800 -- 0000FFFF:   1110xxxx 10xxxxxx 10xxxxxx
  00010000 -- 001FFFFF:   11110xxx 10xxxxxx 10xxxxxx 10xxxxxx

  @param    pThis      pointer to data

  @return   pointer to utf asciiz string for wide-character (html entity) c.
*///-----------------------------------------------------------------------------------------------
const char * NedWCharToUtf8(uint16_t c)
{
  static uint8_t sz[5];
  if(c <= 0x7f)
  {
    sz[0] = (uint8_t)c;
    sz[1] = '\0';
  }
  else if(c <= 0x7ff)
  {
    sz[0] = (uint8_t)(0xc0 | ((c >> 6) & 0x1f));
    sz[1] = (uint8_t)(0x80 | (c & 0x3f));
    sz[2] = '\0';
  }
  else if(c <= 0xffff)
  {
    sz[0] = (uint8_t)(0xe0 | ((c >> 12) & 0x0f));
    sz[1] = (uint8_t)(0x80 | ((c >> 6)  & 0x3f));
    sz[2] = (uint8_t)(0x80 | (c & 0x3f));
    sz[3] = '\0';
  }
  else
  {
    sz[0] = (uint8_t)(0xf0 | ((c >> 18) & 0x07));
    sz[1] = (uint8_t)(0x80 | ((c >> 12) & 0x3f));
    sz[2] = (uint8_t)(0x80 | ((c >> 6)  & 0x3f));
    sz[3] = (uint8_t)(0x80 | (c & 0x3f));
    sz[4] = '\0';
  }
  return (const char *)sz;
}

/*!------------------------------------------------------------------------------------------------
  Transforms string segment to single line paragraphs. Removes any extra spaces.

  input:  " a  string\nof things\n\n  lifts   a\n   wing "
  output: "a string of things\nlifts a wing"

  input:  "string here  \n\n\n   "
  output: "string here\n"

  @param    pData   an array of nedChars that will be modified (may be shorter, never longer)
  @param    len     length to string segment

  @return   new length (same or shorter, could be 0)
*///-----------------------------------------------------------------------------------------------
size_t NedMemRemoveExtraSpaces(nedChar_t *pData, size_t len)
{
  // nedChar_t  *pDataOrg = pData;
  nedChar_t  *p;
  size_t      nSpaces;
  size_t      nLfs;             // linendings, \n or \r\n
  size_t      i;
  bool_t      fEdge = TRUE;

  // NedLogDump(pDataOrg, len, 16, 2);

  i = 0;
  while(i < len)
  {
    // count spaces and linefeeds
    nSpaces = 0;
    nLfs    = 0;
    if(NedCharType(*pData) == IS_SPACE || *pData == '\n')
    {
      // count spaces and line feeds (we may remove some or all)
      p = pData;
      while((i + nSpaces < len) && (NedCharType(*p) == IS_SPACE || *p == '\n'))
      {
        if(*p == '\n')
          ++nLfs;
        ++nSpaces;    // '\n', '\t' and ' ' are all considered spaces
        ++p;
      }

      // for end of string, same as start of string, delete all spaces
      if(i + nSpaces >= len)
        fEdge = TRUE;

      // NedLogPrintf("i %zu, len %zu, nSpaces %zu, nLfs %zu, fEdge %u\n", i, len, nSpaces, nLfs, fEdge);
    }

    // transform the string
    if(nSpaces > 0)
    {
      // for multiple '\n', always leave one '\n'
      // at start/end of sting, delete all spaces, but in the middle leave 1
      if(nLfs > 1)
        fEdge = FALSE;
      if(!fEdge)
        --nSpaces;

      // remove all extra spaces and fill in space
      if(nSpaces)
      {
        memmove(pData, pData + nSpaces, (len - i) - nSpaces);
        len -= nSpaces;

      }

      if(!fEdge)
      {
        *pData = (nLfs > 1) ? '\n' : ' ';
        ++pData;
        ++i;
      }

      // NedLogDump(pDataOrg, len, 16, 2);

    }

    else
    {
      ++i;
      ++pData;
    }

    fEdge = FALSE;
  }

  return len;
}

/*!------------------------------------------------------------------------------------------------
  Determine where to wrap. Assumes pLine stirng is at least width in length

  1. Always at least 1 word
  2. Stops at \n
  3. Stops at space following word
  4. Stops at len

  Examples:

  @param pLine       pointer to start of line
  @param len         length of string (1-n)
  @param wrapWidth   length to wrap (1-n)
  @param pThisLen    pointer to receive actual line length
  @return   position at where to wrap
*///-----------------------------------------------------------------------------------------------
size_t NedMemFindWrap(const nedChar_t *pLine, size_t len, size_t wrapWidth)
{
  const nedChar_t  *p;
  const nedChar_t  *pLastWord = NULL;
  size_t            i;

  p = pLine;
  for(i=0; i < len; ++i)
  {
    if(NedCharType(*p) == IS_SPACE || NedCharType(*p) == IS_LF)
    {
      if(i > wrapWidth)
      {
        if(pLastWord)
          i = (size_t)(pLastWord - pLine);
        else
          i = (size_t)(p - pLine);
        break;
      }
      else if(NedCharType(*p) == IS_LF)
      {
        i = (size_t)(p - pLine);
        break;
      }

      pLastWord = p;
    }
    ++p;
  }

  return i;
}

/*!------------------------------------------------------------------------------------------------
  Find difference in 2 strings. Points out position where they differ

  @param  szThis    pointer to string
  @param  szThat    pointer to 2nd string
  @param  n         length to check
  @return position where there is difference, or n if no difference
*///-----------------------------------------------------------------------------------------------
size_t NedStrWhereDiff(char *szThis, char *szThat, size_t n)
{
  size_t    i;

  for(i=0; i < n; ++i)
  {
    if(*szThis != *szThat)
      break;
    ++szThis;
    ++szThat;
  }
  return i;
}

/*!------------------------------------------------------------------------------------------------
  Dump a single line (up to 16 bytes). The string must be NED_CHAR_DUMNP_LINE_LEN characters to
  hold the whole dump line

    00008050  01 02 00 00 00 5f 70 72  69 6e 74 66 00 00 00 00  |....._printf....|
    00008290  12                                                |.|

  @param  szDst     ptr to string that will contain dump chars
  @param  addr      address to display for that line
  @param  pData     pointer to uint8_t data
  @param  len       length of pData
  @return total strlen of output string in szDst
*///-----------------------------------------------------------------------------------------------
unsigned NedCharDumpLine(char *szDst, long addr, uint8_t *pData, unsigned len)
{
  unsigned    i;
  int         offset;

  offset = sprintf(szDst, "%08lx. ", addr);
  for(i=0; i < NED_CHAR_DUMP_COLS; ++i)
  {
    if(i < len)
      offset += sprintf(&szDst[offset], "%s%02x ", (i == 8) ? " " : "", pData[i]);
    else
      offset += sprintf(&szDst[offset], "%s   ", (i == 8) ? " " : "");
  }
  offset += sprintf(&szDst[offset], " |");
  for(i=0; i < NED_CHAR_DUMP_COLS; ++i)
  {
    if(i < len)
      szDst[offset++] = isprint(pData[i]) ? (char)(pData[i]) : '.';
  }
  offset += sprintf(&szDst[offset], "|\n");

  return offset;
}

/*!------------------------------------------------------------------------------------------------
  Allocate a copy of the array of strings on the heap. Input may be a hard-coded array of strings,
  local strings, etc.... Note: the array of strings can be "empty" by having the 1st element be
  NULL.

  @param  asz       a valid, NULL terminated array of strings
  @return an allocated array of strings, which may have no strings, just the terminating NULL
*///-----------------------------------------------------------------------------------------------
char **NedUtilStrArrayCopy(const char **asz)
{
  size_t      i;
  size_t      numElem;
  size_t      sizeOf;
  char      **ppChars = NULL;
  char       *psz;

  // calculate memory needed
  NedAssertDbg(asz);
  numElem = NedUtilStrArrayNumElem(asz);
  sizeOf  = NedUtilStrArraySizeOf(asz);

  // allocate the memory of asz was valid
  if(numElem)
    ppChars = NedUtilStrArrayMalloc(numElem, sizeOf);

  // fill in the newly allocated memory with a copy of text from asz
  if(ppChars)
  {
    psz = NedUtilStrArrayText((const char **)ppChars);
    for(i = 0; asz[i]; ++i)
    {
      ppChars[i] = psz;
      strcpy(psz, asz[i]);
      psz += strlen(asz[i]) + 1;
    }
  }

  return ppChars;
}

/*!------------------------------------------------------------------------------------------------
  Free the strings created by NedUtilStrArrayMalloc()

  @param  asz      a valid, NULL terminated array of strings
  @return a memory location 
*///-----------------------------------------------------------------------------------------------
void NedUtilStrArrayFree(const char **asz)
{
  if(asz)
    free(asz);
}

/*!------------------------------------------------------------------------------------------------
  Compare two string arrays. Doesn't care if they are hard-coded or allocated on heap. Stops of
  1st string that is different.

  @param  aszThis      a valid, NULL terminated array of strings
  @param  aszThat      a valid, NULL terminated array of strings
  @return -1 if this less than that, 0 if equal, 1 if greater than
*///-----------------------------------------------------------------------------------------------
int NedUtilStrArrayCmp(const char **aszThis, const char **aszThat)
{
  unsigned    i;
  int         ret = 0;

  for(i=0; (ret == 0) && aszThis[i] && aszThat[i]; ++i)
  {
    ret = strcmp(aszThis[i], aszThat[i]);
  }

  if(ret == 0)
  {
    if(aszThis[i] != NULL)
      ret = 1;
    else if(aszThat[i] != NULL)
      ret = -1;
  }
  if(ret > 0)
    ret = 1;
  if(ret < 0)
    ret = -1;

  return ret;
}

/*!------------------------------------------------------------------------------------------------
  Allocate space for an array of strings with enough room for all the text, as specified by sizeOf.
  Example:

  char    **asz;
  char     *psz;
  size_t    i;
  const    *aStrings[] = { "alpha", "beta", "gamma", NULL };

  asz = NedUtilStrArrayMalloc(NedUtilStrArrayNumElem(aStrings), NedUtilStrArraySizeOf(aStrings));
  if(asz)
  {
    psz = NedUtilStrArrayText(asz);
    for(i = 0; i < NumElements(aStrings) - 1; ++i)
    {
      asz[i] = psz;
      strcpy(asz[i], aStrings[i]);
      psz += strlen(aStrings[i]) + 1;
    }
    NedUtilStrArrayPrint(asz);
  }

  @param  numElem     number of string pointers that can be set by the higher layer
  @param  sizeOf      total size of text, including '\0', that will be filled in
  @return pointer to a NULL terminated array of strings
*///-----------------------------------------------------------------------------------------------
char **NedUtilStrArrayMalloc(size_t numElem, size_t sizeOf)
{
  char    **asz         = NULL;
  char     *psz;
  size_t    i;
  size_t    sizePtrs;

  sizePtrs = sizeof(char *) * (numElem + 1);
  asz      = malloc(sizePtrs + sizeOf);

  // if allocated OK, then initialize all strings to point to an empty string
  if(asz)
  {
    memset(asz, 0, sizePtrs + sizeOf);
    psz = ((char *)asz) + sizePtrs;
    for(i = 0; i < numElem; ++i)
    {
      asz[i] = psz;
    }
    asz[i] = NULL;
  }

  return asz;
}

/*!------------------------------------------------------------------------------------------------
  Get the text of the 1st element, aka asz[0]). If NedUtilStrArrayMalloc() was used, this is the
  area where to start filling in strings. Do not exceed sizeOf.

  @param  asz     a valid, NULL terminated array of strings
  @return ptr to text of 1st item. 
*///-----------------------------------------------------------------------------------------------
char *NedUtilStrArrayText(const char **asz)
{
  return (asz && *asz) ? (char *)(*asz) : NULL;
}

/*!------------------------------------------------------------------------------------------------
  Determine number of elemens in a NULL terminated string array. For example:

  const char *asz[] = { "alpha", "beta", "gamma", NULL };
  NedUtilStrArrayNumElem(asz) == 3

  @param  asz     a valid, NULL terminated array of strings
  @return number of strings in the NULL terminated string array.
*///-----------------------------------------------------------------------------------------------
size_t NedUtilStrArrayNumElem(const char **asz)
{
  size_t    numElem;

  for(numElem = 0; asz && asz[numElem]; ++numElem)
    /* just counting */;

  return numElem;
}

/*!------------------------------------------------------------------------------------------------
  Determine total sizeof(text) of all strings in a NULL terminated string array. For example:

  const char *asz[] = { "alpha", "beta", "gamma", NULL };
  NedUtilStrArraySizeOf(asz) == 17

  @param  asz     a valid, NULL terminated array of strings
  @return total text size of all strings, including '\0's
*///-----------------------------------------------------------------------------------------------
size_t NedUtilStrArraySizeOf(const char **asz)
{
  size_t    i;
  size_t    sizeOf = 0;

  for(i = 0; asz && asz[i]; ++i)
  {
    sizeOf += strlen(asz[i]) + 1;
  }

  return sizeOf;
}

/*!------------------------------------------------------------------------------------------------
  Print the NULL terminated array of strings in the form of:

  0: string1
  1: string2
  2: string3

  @param  asz     a valid, NULL terminated array of strings
  @return none
*///-----------------------------------------------------------------------------------------------
void NedUtilStrArrayPrint(const char **asz)
{
  size_t    i;

  for(i = 0; asz && asz[i]; ++i)
    printf("%zu: %s\n", i, asz[i]);
}
