/*!************************************************************************************************
  @file NedTheme.h

  @brief  Ned Theme - Color themes (classic, dark mode, natural, etc...)

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************

#ifndef NED_THEME_H
#define NED_THEME_H
#include "Ned.h"
#include "NedAnsi.h"

#ifndef NEDCFG_THEME_MAX
 #define NEDCFG_THEME_MAX      12
#endif
#define NEDTHEME_NUM_FIELDS    10
#define NEDTHEME_MAX_NAMESIZE  16
#define NEDTHEME_NOT_FOUND     UINT_MAX

typedef struct
{
  nedAttr_t text;           // normal text
  nedAttr_t textHighlight;  // highlighted text
  nedAttr_t comment;        // language comments both block and line
  nedAttr_t keyword;        // language keywords
  nedAttr_t constant;       // constants like 0 or "hello"
  nedAttr_t menuText;       // color of menu text/frame
  nedAttr_t menuHotkey;     // color of hot key in menu text
  nedAttr_t menuDropText;   // color of selected menu item text
  nedAttr_t menuDropHotkey; // hot key color of selected menu item
  nedAttr_t message;        // message line text
} nedTheme_t;

typedef enum
{
  NEDTHEME_CLASSIC = 0,
  NEDTHEME_DARKMODE,        // 
  NEDTHEME_NATURAL,         // uses terminal color for text
  NEDTHEME_MONOCHROME,
  NEDTHEME_1980s,           // green like a 1980s monitor
  NEDTHEME_ZERO            // no color
} nedThemeIndex_t;

void              NedThemeInit      (void);
nedTheme_t       *NedThemeGet       (unsigned index);
bool_t            NedThemeSet       (const char *szName, const nedTheme_t *pTheme);
unsigned          NedThemeGetIndex  (const char *szThemeName);
const char       *NedThemeGetName   (unsigned index);
bool_t            NedThemeIsDefault (const nedTheme_t *pTheme);

#endif // NED_THEME_H
