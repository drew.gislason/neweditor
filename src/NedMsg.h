/*!************************************************************************************************
  @file NedMsg.h

  @brief  Ned Message API - Includes functions for status bar and message bar

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_msg   Ned Message - Functions dealing with the message and status bar

*///***********************************************************************************************
#ifndef NED_MSG_H
#define NED_MSG_H

#include "Ned.h"
#include "NedKey.h"
#include "NedTheme.h"
#include "NedUtilStr.h"

// bitmap of options
typedef unsigned  nedStatOpts_t;    // see NedPoint.h, they must match for SEARCH_MASK options
#define NEDSTAT_OPT_NONE            0
#define NEDSTAT_OPT_UNSAVED         0x01
#define NEDSTAT_OPT_READONLY        0x02
#define NEDSTAT_OPT_IN_SEARCH       0x04  // searching...
#define NEDSTAT_OPT_CASE_SENSITIVE  0x08  // C - search case sensitive
#define NEDSTAT_OPT_WRAP            0x10  // A - search wrap when finding
#define NEDSTAT_OPT_WHOLE_WORD      0x20  // W - search whole words only
#define NEDSTAT_OPT_REVERSE         0x40  // search in reverse direction
#define NEDSTAT_OPT_SEARCH_MASK     0x7C
#define NEDSTAT_OPT_MACRO           0x80  // recording macro

// helpers to NedMsgLineEdit()
#define NedMsgIsSpecial(ret)          ((ret) < 0)
#define NedMsgIsEsc(ret)              ((ret) == -1)

void          NedStatusBarShow        (void);
void          NedStatusBarSetOpts     (nedStatOpts_t opts);
nedStatOpts_t NedStatusBarGetOpts     (void);
void          NedStatusBarSetFile     (const char *szFilePath);
void          NedStatusBarSetRowCol   (long row, long col);
void          NedStatusBarSetTab      (bool_t fHardTabs, unsigned tabSize, bool_t fCrLf);
void          NedStatusBarSetTheme    (const nedTheme_t *pTheme);

void          NedMsgClear             (void);
unsigned      NedMsgPrintf            (const char *szFormat, ...);
void          NedMsgRedraw            (void);
long          NedMsgLineEdit          (char *szLine, long maxLen, const char *szPrompt, const nedKey_t *pAltExitKeys, const char *szPasteStr);
unsigned      NedMsgLineEditIndex     (long ret);
nedKey_t      NedMsgChoice            (const char *szPrompt, const char *szPrompt2, const char *szExitKeys);
unsigned      NedMsgWidth             (const char *szPrompt, const char *szExitKeys);
const char   *NedMsgFit               (const char *szPrompt, const char *szStr);
const char   *NedMsgFitEx             (const char *szPrompt, const char *szStr, const char *szExitKeys);

#endif // NED_MSG_H
