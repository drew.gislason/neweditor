/*!************************************************************************************************
  @file NedCfg.h

  NED_CFG - Ned Configuration file

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#ifndef NED_CFG_H
#define NED_CFG_H

// 0=minimal like NedAssert(), 1=some like NedAssertDbg(), 2=lots
#define NEDDBG_LEVEL_OFF    0
#define NEDDBG_LEVEL_MIN    1
#define NEDDBG_LEVEL_MAX    2
#ifndef NEDDBG_LEVEL
  #define NEDDBG_LEVEL     0
#endif

// bitmask for debugging subsystems for NedLogPrintfEx(), see logMaskt_t
// use single letters on cmdline to pick mask to log, e.g. ,cfp logs cmds, file, point
#define NEDLOG_NONE     0x00000000    // -
#define NEDLOG_BUF      0x00000001    // b
#define NEDLOG_CMD      0x00000002    // c basic detail on commands
#define NEDLOG_CMD_EDIT 0x00000004    // d details on edit commands
#define NEDLOG_EDITOR   0x00000008    // e
#define NEDLOG_FILE     0x00000010    // f
#define NEDLOG_JSON     0x00000020    // j
#define NEDLOG_KEY      0x00000040    // k
#define NEDLOG_LIST     0x00000080    // l
#define NEDLOG_MENU     0x00000100    // m
#define NEDLOG_MSG      0x00000200    // g
#define NEDLOG_CMD_OPTS 0x00000400    // o, optional detail on non-edit commands
#define NEDLOG_POINT    0x00000800    // p
#define NEDLOG_SETTINGS 0x00001000    // s
#define NEDLOG_UNDO     0x00002000    // u
#define NEDLOG_UTILS    0x00004000    // t
#define NEDLOG_VIEW     0x00008000    // v
#define NEDLOG_WIN      0x00010000    // w
#define NEDLOG_DEFAULT  NEDLOG_CMD    // default ",c" if no mask specified
#define NEDLOG_ALL      0xFFFFFFFF    // x

// minimum screen rows/cols for Ned window (otherwise, too small to get anything done)
#define NEDMIN_ROWS                6    // mininum window rows to be useful
#define NEDMENU_PICK_MAX          16
#define NEDMIN_COLS               48    // minimul window columns to be useful
#define NEDCFG_MAX_WIDTH        1024    // used for maximum window width, and string/symbol

#define NEDCFG_VER_MAXSIZE        16    // "Ned v2.0"

// maximum items in various Editor structures
#define NEDCFG_MAX_HISTORY        12    // for posHistory and bookmarks
#define NEDCFG_MAX_RECENT         12    // for recent files and projects
#define NEDCFG_MAX_FILES          64    // maximum open files
#define NEDCFG_MAX_KEYMAP         64    // maximum changes from default keymap
#define NEDCFG_MAX_LANG           16    // maximum number of languages supported
#define NEDCFG_MAX_LANG_CHARS     32    // maximum # of characters for szLang or szExt fields
#define NEDCFG_MAX_UNDOS          32    // maximum # of undos
#define NEDCFG_MAX_INDENT          8    // 1-8 chars max for a single tab indent

// per language defaults
#define NEDCFG_TAB_DEFAULT         2    // tab size
#define NEDCFG_WRAP_DEFAULT      100    // typically for wrappping comment text
#define NEDCFG_TAB_PYTHON          4    // complies with PEP8
#define NEDCFG_WRAP_PYTHON        79    // complies with PEP8
#if NED_PLATFORM == NED_WINDOWS
 #define NEDCFG_CRLF            TRUE
#else
 #define NEDCFG_CRLF           FALSE
#endif

#define NEDCFG_THEME_MAX           12

// helpers for NedSettings
#define NEDCFG_LANG_MAX            12   // maximum # of languages ned understands
#define NEDCFG_LANG_COMMENTS_MAX    6   // maximum # of comment types (6 line and 3 block)
#define NEDCFG_LANG_KEYWORDS_MAX   32   // maximum # of keywords in each language
#define NEDCFG_LANG_NAME_MAXSIZE   16   // "c/c++"
#define NEDCFG_LANG_EXT_MAXSIZE    32   // ".c.h.cpp.hpp"
#define NEDCFG_LANG_CMT_MAXSIZE     5   // ["//",null], ["/*","*/","#if","#endif",null]
#define NEDCFG_LANG_KWD_MAXSIZE   128   // ["float","while","...",null]



#endif  // NED_CFG_H
