/*!************************************************************************************************
  @file NedUndo.c

  @brief  Undo and Redo API implementation.

  @copyright Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @groupdef NedUndo - API for undo and redo

  Undo/redo text edits in files. Uses a roll-back method, recording each item that was inserted or
  deleted. Redo is just a roll-back in the opposite direction. A new undo is defined by a series
  of edits (inserts and deletes). 

  The Undo API provides API compatible Point edit functions that keep track of data to undo each
  edit. The higher layer is responsible for calling NedUndoDoneEdit() to terminate each Undo.

  Rules
  1.  pList is the head (oldest) Undo
  2.  pCur points to the newest Undo when editing, and the current Undo for Undo/Redo
  2a. A new undo is only created when editing, so it will always have at least 1 edit in it
  2b. When the UndoList is new, both pList and pCur are NULL. Not created until 1st edit
  3.  For both Undo/Redo, the rule is Do-And-Move, that is, Undo/Redo on current, then move
  3a. fTop indicates already undid top entry (can't move pCur to pPrev)
  4.  An Undo contains one or more edits (position/text)  
  4a. If higher layer calls NedUndoEditDone(), then next edit will create new Undo
  4b. If pFile is different for edit point then creates a new Undo
  4c. If edits in the Undo, and NOT editing, then creates a new Undo
  4d. A single Undo is always within a single file (perhaps multiple edits)
  4e. UndoEditPrep() does the work of creating a new undo if needed
  5.  Undo applies edits in reverse order, Redo, applies edits in forward order
  6.  Higher layer chooses when Undo is made
  6a. NedUndoInsXxx() are same as NedPointInsXxx(), with the addition of adding to Undos
  6b. Higher layer decides when to call NedUndoInsXxx() or NedPointInsXxx() functions
  6c. Higher layer decies when to call NedUndoEditDone()

*///***********************************************************************************************
#include "Ned.h"
#include "NedUndo.h"

#define NEDUNDO_SANCHK    8490

typedef struct
{
  long                  pos;      // position in file
  long                  len;      // if negative, text was deleted
  uint8_t              *pData;    // data that was inserted or deleted
} sNedUndoEdit_t;

// a single undo record might involve multiple edits (inserts and deletes)
typedef struct sNedUndo_tag
{
  sNedFile_t           *pFile;
  long                  nEdits;
  sNedUndoEdit_t       *pEdits;
  struct sNedUndo_tag  *pPrev;
  struct sNedUndo_tag  *pNext;
} sNedUndo_t;

typedef struct
{
  unsigned              sanchk;
  pfnGetFileByPoint_t   pfnGetFileByPoint;
  long                  nUndos;
  sNedUndo_t           *pList;
  sNedUndo_t           *pCur;
  bool_t                fEditing;   // continue to add to this Undo?
  bool_t                fTop;       // already did 1st Undo
} sNedUndoList_t;

typedef enum
{
  FLAG_UNDO,
  FLAG_REDO
} undoFlag_t;

static sNedUndo_t      *UndoNew         (void);
static void             UndoFree        (sNedUndo_t *pUndo);
static long             UndoFreeList    (sNedUndo_t *pUndo);
static void             UndoFreeEdits   (long nEdits, sNedUndoEdit_t *pEdits);
static sNedUndoEdit_t  *UndoGetLastEdit (sNedUndo_t *pUndo);
static void             UndoApplyEdit   (sNedFile_t *pFile, sNedUndoEdit_t *pEdit, undoFlag_t flag);
static sNedUndo_t      *UndoEditPrep    (sNedUndoList_t *pUndoList, sNedPoint_t *pPoint);
static sNedUndoEdit_t * UndoGetNewEdit  (sNedUndo_t *pUndo);

/*!------------------------------------------------------------------------------------------------
  Create a new UndoList

  @ingroup  ned_undo
  @param    pfnGetFile    function to get a file from a point
  @return   handle for UndoList, or NULL if memory issue
*///-----------------------------------------------------------------------------------------------
hUndoList_t NedUndoListNew(pfnGetFileByPoint_t pfnGetFileByPoint)
{
  sNedUndoList_t  *pUndoList;

  NedAssert(pfnGetFileByPoint);

  pUndoList = malloc(sizeof(sNedUndoList_t));
  if(pUndoList)
  {
    memset(pUndoList, 0, sizeof(*pUndoList));
    pUndoList->sanchk = NEDUNDO_SANCHK;
    pUndoList->pfnGetFileByPoint = pfnGetFileByPoint;
  }

  return pUndoList;
}

/*!------------------------------------------------------------------------------------------------
  Free the UndoList

  @ingroup  ned_edit
  @param    hUndoList     handle to valid UndoList
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedUndoListFree(hUndoList_t hUndoList)
{
  sNedUndoList_t     *pUndoList = hUndoList;

  if(NedUndoIsUndoList(hUndoList))
  {
    NedUndoListClear(hUndoList);
    free(pUndoList);
  }
}

/*!------------------------------------------------------------------------------------------------
  Clear the UndoList

  @ingroup  ned_edit
  @param    hUndoList     handle to valid UndoList
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedUndoListClear(hUndoList_t hUndoList)
{
  sNedUndoList_t     *pUndoList = hUndoList;

  if(NedUndoIsUndoList(hUndoList))
  {
    if(pUndoList->pList)
      UndoFreeList(pUndoList->pList);
    pUndoList->pList    = NULL;
    pUndoList->pCur     = NULL;
    pUndoList->nUndos   = 0;
    pUndoList->fEditing = FALSE;
    pUndoList->fTop     = FALSE;
  }
}

/*!------------------------------------------------------------------------------------------------
  Is this an undo list?

  @ingroup  ned_undo
  @param    hUndoList     handle to valid UndoList
  @return   TRUE if is a valid UndoList, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedUndoIsUndoList(hUndoList_t hUndoList)
{
  sNedUndoList_t  *pUndoList = hUndoList;
  return (pUndoList && pUndoList->sanchk == NEDUNDO_SANCHK) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Return # of undos in the list

  @ingroup  ned_edit
  @param    hUndoList     handle to valid UndoList
  @return   number of undos in the list
*///-----------------------------------------------------------------------------------------------
long NedUndoCount(hUndoList_t hUndoList)
{
  sNedUndoList_t *pUndoList = hUndoList;
  long            count = 0;

  if(NedUndoIsUndoList(hUndoList))
    count = pUndoList->nUndos;

  return count;
}

/*!------------------------------------------------------------------------------------------------
  Prints UndoList to log

  @ingroup  ned_undo
  @param    hUndoList     handle to valid UndoList
  @param    fDetails      TRUE to display Undos and Edits
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedUndoLogShow(hUndoList_t hUndoList, bool_t fDetails)
{
  sNedUndoList_t   *pUndoList = hUndoList;
  sNedUndo_t       *pUndo;
  sNedUndo_t       *pPrev;
  sNedUndoEdit_t   *pEdit;
  long              i;
  long              nUndos;
  long              len;

  if(!NedUndoIsUndoList(hUndoList))
  {
    NedLogPrintf("!!ERR sNedUndoList_t %p\n", pUndoList);
    return;
  }

  NedLogPrintf("NedUndoLogShow(pUndoList %p, nUndos %ld, pList %p, pCur %p, fEditing %u, fTop %u)\n",
    pUndoList, pUndoList->nUndos, pUndoList->pList, pUndoList->pCur, pUndoList->fEditing, pUndoList->fTop);
  if(fDetails)
  {
    pUndo = pUndoList->pList;
    while(pUndo)
    {
      NedLogPrintf("  pUndo %p, pFile %p, nEdits, %ld, pEdits %p, pPrev %p, pNext %p\n",
        pUndo, pUndo->pFile, pUndo->nEdits, pUndo->pEdits, pUndo->pPrev, pUndo->pNext);

      pEdit = pUndo->pEdits;
      for(i = 0; i < pUndo->nEdits; ++i)
      {
        len = pEdit->len;
        if(len < 0L)
          len = -1L * len;
        NedLogPrintf("    %u: pos %ld, %s len %ld, pData %.*s\n", i, pEdit->pos,
          pEdit->len < 0 ? "Del" : "Ins", len, len, pEdit->pData);
        ++pEdit;
      }
      pUndo = pUndo->pNext;
    }
  }

  // check the list
  nUndos = 0;
  pPrev  = NULL;
  pUndo  = pUndoList->pList;
  while(pUndo)
  {
    if(pUndo->pPrev != pPrev)
    {
      NedLogPrintf("!!ERR pPrev incorrect\n");
      break;
    }
    if(pUndo->nEdits <= 0L || pUndo->pFile == NULL || pUndo->pEdits == NULL)
    {
      NedLogPrintf("!!ERR bad Undo %ld\n", nUndos);
      break;
    }
    ++nUndos;
    if(nUndos > NEDCFG_MAX_UNDOS)
    {
      NedLogPrintf("!!WARNING too many Undos %ld!\n", nUndos);
      break;
    }
    pPrev = pUndo;
    pUndo = pUndo->pNext;
  }
}

/*!------------------------------------------------------------------------------------------------
  This concludes the edits to this Undo

  @ingroup  ned_undo
  @param    hUndoList     handle to valid UndoList
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedUndoEditDone(hUndoList_t hUndoList)
{
  sNedUndoList_t  *pUndoList = hUndoList;
  if(NedUndoIsUndoList(pUndoList))
    pUndoList->fEditing = FALSE;
}

/*-------------------------------------------------------------------------------------------------
  Get the Redo sNedUndo_t, or NULL if no redos possible
-------------------------------------------------------------------------------------------------*/
static sNedUndo_t * _GetRedo(sNedUndoList_t *pUndoList)
{
  sNedUndo_t  *pUndo = NULL;

  if(NedUndoIsUndoList(pUndoList) && pUndoList->pList != NULL)
    pUndo = pUndoList->pCur;

  if(pUndo)
  {
    if(pUndoList->fTop)
      pUndoList->fTop = FALSE;
    else
    {
      pUndo = pUndo->pNext;
      if(pUndo)
        pUndoList->pCur = pUndo;
    }
  }

  return pUndo;
}

/*-------------------------------------------------------------------------------------------------
  Get the Undo sNedUndo_t, or NULL if no redos possible
-------------------------------------------------------------------------------------------------*/
static sNedUndo_t * _GetUndo(sNedUndoList_t *pUndoList)
{
  sNedUndo_t  *pUndo = NULL;

  if(NedUndoIsUndoList(pUndoList) && !pUndoList->fTop && pUndoList->pList != NULL)
    pUndo = pUndoList->pCur;
  return pUndo;
}

/*-------------------------------------------------------------------------------------------------
  Get the length and position as a sum of the edits
-------------------------------------------------------------------------------------------------*/
static long _GetLenPos(sNedUndo_t *pUndo, long *pPos)
{
  sNedUndoEdit_t   *pEdit;
  long              pos = LONG_MAX;
  long              len = 0;
  long              i;

  // calculate sum of all edits (pos/len)
  pEdit = pUndo->pEdits;
  for(i=0; i < pUndo->nEdits; ++i)
  {
    if(pEdit->pos < pos)
      pos = pEdit->pos;
    len += pEdit->len;
    ++pEdit;
  }

  NedLogPrintfEx(NEDLOG_UNDO, "_GetLenPos, nEdits %ld, len %ld, pos %ld\n", pUndo->nEdits, len, pos);

  *pPos = pos;
  return len;
}

/*!------------------------------------------------------------------------------------------------
  Redo previous undo. Does nothing if already at UndoList bottom

  @ingroup  ned_undo
  @param    hUndoList     handle to valid UndoList
  @return   pFile for the Redo, or NULL if no redo
*///-----------------------------------------------------------------------------------------------
sNedFile_t *  NedUndoRedo(hUndoList_t hUndoList)
{
  sNedUndoList_t   *pUndoList = hUndoList;
  sNedUndo_t       *pRedo     = NULL;
  sNedFile_t       *pFile     = NULL;
  sNedUndoEdit_t   *pEdit;
  long              i;

  // apply edits for Redo in forward order
  pRedo = _GetRedo(pUndoList);
  if(pRedo && pRedo->nEdits)
  {
    // modifying file, it's now dirty
    pFile = pRedo->pFile;
    pFile->fDirty = TRUE;

    // apply edits
    pEdit = pRedo->pEdits;
    for(i=0; i < pRedo->nEdits; ++i)
    {
      UndoApplyEdit(pFile, pEdit, FLAG_REDO);
      ++pEdit;
    }
  }

  return pFile;
}

/*!------------------------------------------------------------------------------------------------ 
  Undo an edit. Does nothing if already at UndoList top

  @ingroup  ned_undo
  @param    hUndoList     handle to valid UndoList
  @return   pFile for the Undo, or NULL if no more Undos
*///-----------------------------------------------------------------------------------------------
sNedFile_t * NedUndoUndo(hUndoList_t hUndoList)
{
  sNedUndoList_t   *pUndoList = hUndoList;
  sNedUndo_t       *pUndo     = NULL;
  sNedFile_t       *pFile     = NULL;
  long              i;

  // get the undo
  pUndo = _GetUndo(pUndoList);;
  if(pUndo)
  {
    // undo the ediets
    pFile = pUndo->pFile;
    if(pUndo->nEdits)
    {
      pFile = pUndo->pFile;
      pFile->fDirty = TRUE;

      i = pUndo->nEdits;
      while(i > 0)
      {
        --i;
        UndoApplyEdit(pFile, &pUndo->pEdits[i], FLAG_UNDO);
      }
    }

    // advance pCur after undoing the edits
    if(pUndo->pPrev)
      pUndoList->pCur = pUndo->pPrev;
    else
      pUndoList->fTop = TRUE;
  }

  return pFile;
}


/*!------------------------------------------------------------------------------------------------
  Return length/position of the redo

  @ingroup  ned_edit
  @param    hUndoList     handle to valid UndoList
  @param    pPos          po
  @return   length of redo at the 
*///-----------------------------------------------------------------------------------------------
sNedFile_t * NedUndoRedoLen(hUndoList_t hUndoList, long *pPos, long *pLen)
{
  sNedUndoList_t   *pUndoList = hUndoList;
  sNedUndo_t       *pRedo     = NULL;
  sNedFile_t       *pFile     = NULL;
  long              len       = 0;

  if(NedUndoIsUndoList(pUndoList) && pUndoList->pList != NULL)
    pRedo = pUndoList->pCur;

  if(pRedo)
  {
    pFile = pRedo->pFile;
    len   = _GetLenPos(pRedo, pPos);
  }

  // return values
  *pLen = len;
  return pFile;
}

/*!------------------------------------------------------------------------------------------------
  Remove any Undos that refer to this file. Used when a file is closing

  @ingroup  ned_edit
  @param    hUndoList     handle to valid UndoList
  @param    pFile         pointer to valid file
  @return   none
*///-----------------------------------------------------------------------------------------------
sNedFile_t * NedUndoUndoLen(hUndoList_t hUndoList, long *pPos, long *pLen)
{
  sNedUndoList_t   *pUndoList = hUndoList;
  sNedUndo_t       *pUndo;
  sNedFile_t       *pFile     = NULL;
  long              len       = 0;

  pUndo = _GetUndo(pUndoList);
  if(pUndo)
  {
    pFile = pUndo->pFile;
    len = -1L * _GetLenPos(pUndo, pPos);
  }

  // return values
  *pLen = len;
  return pFile;
}

/*!------------------------------------------------------------------------------------------------
  Remove any Undos that refer to this file. Used when a file is closing

  @ingroup  ned_edit
  @param    hUndoList     handle to valid UndoList
  @param    pFile         pointer to valid file
  @return   none
*///-----------------------------------------------------------------------------------------------
void NedUndoFileRemove(hUndoList_t hUndoList, const sNedFile_t *pFile)
{
  sNedUndoList_t     *pUndoList = hUndoList;
  sNedUndo_t         *pUndo;
  sNedUndo_t         *pUndoNext;

  NedAssert(pFile);

  if(NedUndoIsUndoList(hUndoList) && pFile)
  {
    pUndo = pUndoList->pList;
    while(pUndo)
    {
      pUndoNext = pUndo->pNext;
      if(pUndo->pFile == pFile)
      {
        // remove from dual linked list
        if(pUndo->pPrev)
          pUndo->pPrev->pNext = pUndoNext;
        if(pUndo->pNext)
          pUndo->pNext->pPrev = pUndo->pPrev;

        // keep list up to date
        if(pUndoList->pList == pUndo)
          pUndoList->pList = pUndoNext;
        if(pUndoList->pCur == pUndo)
        {
          if(pUndoNext)
            pUndoList->pCur = pUndoNext;
          else
            pUndoList->pCur = pUndo->pPrev;
        }

        UndoFree(pUndo);
        if(pUndoList->nUndos)
          --pUndoList->nUndos;
      }

      pUndo = pUndoNext;
    }
  }
}

/*!------------------------------------------------------------------------------------------------
  Same as NedPointInsChar(), but adds to current undo

  @ingroup  ned_edit
  @param    pPoint       pointer to valid point
  @param    c            character to insert

  @return   TRUE if worked, FALSE if can't allocate another buffer
*///-----------------------------------------------------------------------------------------------
bool_t NedUndoInsChar(hUndoList_t hUndoList, sNedPoint_t *pPoint, nedChar_t c)
{
  UndoIns(hUndoList, pPoint, 1, &c);
  return NedPointInsChar(pPoint, c);
}

/*!------------------------------------------------------------------------------------------------
  Same as NedUndoInsBlock(), but adds to current undo

  @ingroup  ned_edit
  @param    pPoint      pointer to valid point
  @param    pData       pointer to data to insert
  @param    insertLen   length of data (up to LONG_MAX)

  @return   TRUE if worked, FALSE if can't allocate another buffer
*///-----------------------------------------------------------------------------------------------
bool_t NedUndoInsBlock(hUndoList_t hUndoList, sNedPoint_t *pPoint, const uint8_t *pData, long insertLen)
{
  UndoIns(hUndoList, pPoint, insertLen, pData);
  return NedPointInsBlock(pPoint, pData, insertLen);
}

/*!------------------------------------------------------------------------------------------------
  Same as NedUndoInsStr() but adds to current undo

  @ingroup  ned_edit
  @param    pPoint      pointer to valid point
  @param    pData       pointer to data to insert
  @return   TRUE if worked, FALSE if can't allocate another buffer
*///-----------------------------------------------------------------------------------------------
bool_t NedUndoInsStr(hUndoList_t hUndoList, sNedPoint_t *pPoint, const char *sz)
{
  long  insertLen = strlen(sz);
  return NedUndoInsBlock(hUndoList, pPoint, (void *)sz, insertLen);
}

/*!------------------------------------------------------------------------------------------------
  Same as  NedPointDelBackspace(), but removes from current undo

  @ingroup  ned_edit
  @param    pPoint      pointer to valid point
  @return   TRUE if worked, FALSE if at start of file
*///-----------------------------------------------------------------------------------------------
bool_t NedUndoDelBackspace(hUndoList_t hUndoList, sNedPoint_t *pPoint)
{
  if(!NedPointCharPrev(pPoint))
    return FALSE;
  return NedUndoDelChar(hUndoList, pPoint);
}

/*!------------------------------------------------------------------------------------------------
  Same as NedPointDelChar(), but removes from current undo

  @ingroup  ned_edit
  @param    pFile       pointer to valid file
  @return   TRUE if worked, FALSE if at EOF
*///-----------------------------------------------------------------------------------------------
bool_t NedUndoDelChar(hUndoList_t hUndoList, sNedPoint_t *pPoint)
{
  UndoDel(hUndoList, pPoint, 1);
  return NedPointDelChar(pPoint);
}

/*!------------------------------------------------------------------------------------------------ 
  Same as NedPointDelBlock(), but removes from current undo

  @ingroup  ned_edit
  @param    pThis       pointer to valid file
  @param    pThis       pointer to valid file
  @return   TRUE if worked, FALSE if points aren't in same buffer chain
*///-----------------------------------------------------------------------------------------------
bool_t NedUndoDelBlock(hUndoList_t hUndoList, sNedPoint_t *pThis, sNedPoint_t *pThat)
{
  long    delLen;

  if(!NedPointSort2Points(pThis, pThat))
    return FALSE;

  delLen = NedPointDifference(pThis, pThat);
  UndoDel(hUndoList, pThis, delLen);
  return NedPointDelBlock(pThis, pThat);
}

/*------------------------------------------------------------------------------------------------- 
  Create a new undo item. Does NOT add to the list

  @return   none
-------------------------------------------------------------------------------------------------*/
static sNedUndo_t * UndoNew(void)
{
  sNedUndo_t *pUndo;

  pUndo = malloc(sizeof(*pUndo));
  if(pUndo)
    memset(pUndo, 0, sizeof(*pUndo));

  return pUndo;
}

/*------------------------------------------------------------------------------------------------- 
  Free this undo item. Does NOT remove from the list

  @return   none
-------------------------------------------------------------------------------------------------*/
static void UndoFree(sNedUndo_t *pUndo)
{
  if(pUndo->nEdits)
    UndoFreeEdits(pUndo->nEdits, pUndo->pEdits);
  free(pUndo);
}

/*------------------------------------------------------------------------------------------------- 
  Apply the item, used by Undo and Redo.

  @return   none
-------------------------------------------------------------------------------------------------*/
static void UndoFreeEdits(long nEdits, sNedUndoEdit_t *pEdits)
{
  sNedUndoEdit_t *pEdit;
  long            i;

  pEdit = pEdits;
  for(i=0; i < nEdits; ++i)
  {
    if(pEdit->pData)
      free(pEdit->pData);
    ++pEdit;
  }

  memset(pEdits, 0, nEdits * sizeof(sNedUndoEdit_t));
  free(pEdits);
}

/*------------------------------------------------------------------------------------------------- 
  Used for pruning some of or the entire UndoList

  @return   # of Undos pruned
-------------------------------------------------------------------------------------------------*/
static long UndoFreeList(sNedUndo_t *pUndo)
{
  sNedUndo_t  *pUndoNext;
  long         nUndos = 0;

  while(pUndo)
  {
    pUndoNext = pUndo->pNext;
    UndoFree(pUndo);
    ++nUndos;
    pUndo = pUndoNext;
  }
  return nUndos;  // # pruned
}

/*------------------------------------------------------------------------------------------------- 
  About to add an edit to the current Undo. Has many duties.

  1. Creates an Undo entry if list is empty
  2. If pCur not at end of list, then prunes the list so no branches
  3. If this file is different than the current Undo, then create a new Undo
  4. If this Undo has edits, but is not fEditing, then create a new Undo

  @return   pointer to Undo in which to add this edit
-------------------------------------------------------------------------------------------------*/
static sNedUndo_t *UndoEditPrep(sNedUndoList_t *pUndoList, sNedPoint_t *pPoint)
{
  sNedUndo_t   *pUndo = NULL;
  sNedUndo_t   *pUndoNew;
  sNedFile_t   *pFile;
  bool_t        fNeedNew = FALSE;

  // 1. Creates an Undo entry if list is empty
  if(pUndoList->pList == NULL)
  {
    pUndo = pUndoList->pList = pUndoList->pCur = UndoNew();
    ++pUndoList->nUndos;
  }
  else
  {
    pUndo = pUndoList->pCur;
  }
  NedAssert(pUndoList->pCur != NULL);

  // 2. If pCur not at end of list, then prunes the list so no branches
  if(pUndoList->fTop)
  {
    NedUndoListClear(pUndoList);
    pUndo = pUndoList->pList = pUndoList->pCur = UndoNew();
    ++pUndoList->nUndos;
  }
  else if(pUndo->pNext != NULL)
  {
    pUndoList->nUndos -= UndoFreeList(pUndo->pNext);
    NedAssert(pUndoList->nUndos > 0);
    pUndo->pNext = NULL;
  }

  // 3. If this file is different than the current Undo, then create a new Undo
  pFile = (pUndoList->pfnGetFileByPoint)(pPoint);
  NedAssert(pFile);
  if(pUndo->pFile != NULL && pUndo->pFile != pFile)
    fNeedNew = TRUE;

  // 4. If this Undo has edits, but is not fEditing, then create a new Undo
  if(pUndo->nEdits && !pUndoList->fEditing)
    fNeedNew = TRUE;

  // append a new Undo to the list. pUndo on last Undo
  if(fNeedNew)
  {
    pUndoNew        = UndoNew();
    pUndo->pNext    = pUndoNew;
    pUndoNew->pPrev = pUndo;
    pUndoList->pCur = pUndoNew;
    pUndoNew->pFile = pFile;
    pUndo           = pUndoNew;
    ++pUndoList->nUndos;
  }

  // now editing this current undo
  pUndo->pFile = pFile;
  pUndoList->fEditing = TRUE;

  // NedLogPrintfEx(NEDLOG_UNDO, "UndoEditPrep(pList %p, pCur %p, nUndos %ld, fEditing %u, fTop %u, pUndo %p nEdits %ld)\n",
  //     pUndoList->pList, pUndoList->pCur, pUndoList->nUndos, pUndoList->fEditing, pUndoList->fTop, pUndo, pUndo->nEdits);

  return pUndo;
}

/*-------------------------------------------------------------------------------------------------
  Get last edit in Undo. May return NULL if no edits so far in this Undo entry

  @return   pointer to last edit or NULL if not edits yet
-------------------------------------------------------------------------------------------------*/
static sNedUndoEdit_t * UndoGetLastEdit(sNedUndo_t *pUndo)
{
  sNedUndoEdit_t * pEdit = NULL;
  if(pUndo->nEdits > 0)
    pEdit = &pUndo->pEdits[pUndo->nEdits - 1];
  return pEdit;
}

/*-------------------------------------------------------------------------------------------------
  Create a new edit in this Undo. Higher layer must fill in whether edit is an insert or delete

  @return   Returns ptr to new edit
-------------------------------------------------------------------------------------------------*/
static sNedUndoEdit_t * UndoGetNewEdit(sNedUndo_t *pUndo)
{
  sNedUndoEdit_t *pEdit = NULL;

  ++pUndo->nEdits;
  if(pUndo->nEdits == 1)
    pEdit = pUndo->pEdits = malloc(sizeof(sNedUndoEdit_t));
  else
  {
    pUndo->pEdits = realloc(pUndo->pEdits, pUndo->nEdits * sizeof(sNedUndoEdit_t));
    NedAssert(pUndo->pEdits);
    pEdit = UndoGetLastEdit(pUndo);
  }
  if(pEdit)
    memset(pEdit, 0, sizeof(*pEdit));
  return pEdit;
}

/*-------------------------------------------------------------------------------------------------
  Inserting text, add to current Undo edit if possible

  @return   none
-------------------------------------------------------------------------------------------------*/
void UndoIns(hUndoList_t hUndoList, sNedPoint_t *pPoint, long len, const uint8_t *pData)
{
  sNedUndoList_t   *pUndoList = hUndoList;
  sNedUndo_t       *pUndo;
  sNedUndoEdit_t   *pEdit;
  long              pos;

  NedAssert(NedUndoIsUndoList(hUndoList));

  pUndo = UndoEditPrep(pUndoList, pPoint);
  pEdit = UndoGetLastEdit(pUndo);
  pos   = NedPointPos(pPoint);

  // if(pEdit)
  //   NedLogPrintfEx(NEDLOG_UNDO, "UndoIns(pEdit->pos %ld, pEdit->len %ld, pos %ld, len %ld)\n", pEdit->pos, pEdit->len, pos, len);

  // user is entering text, treat as one insert (append)
  if(pEdit && pEdit->len > 0 && pEdit->pos + pEdit->len == pos)
  {
    pEdit->pData = realloc(pEdit->pData, pEdit->len + len);
    memcpy(&pEdit->pData[pEdit->len], pData, len);
    pEdit->len += len;
  }

  // user is doing other insert operations (like toggling comments)
  else
  {
    pEdit = UndoGetNewEdit(pUndo);
    pEdit->pData = malloc(len);
    memcpy(pEdit->pData, pData, len);
    pEdit->len = len;
    pEdit->pos = pos;
  }
}

/*------------------------------------------------------------------------------------------------- 
  Deleting text, add to current Undo edit if possible

  @return   none
-------------------------------------------------------------------------------------------------*/
void UndoDel(hUndoList_t hUndoList, sNedPoint_t *pPoint, long len)
{
  sNedUndoList_t   *pUndoList = hUndoList;
  sNedUndo_t       *pUndo;
  sNedUndoEdit_t   *pEdit;
  long              pos;
  long              editLen;
  bool_t            fDeleted = FALSE;

  NedAssert(NedUndoIsUndoList(hUndoList));

  pUndo = UndoEditPrep(pUndoList, pPoint);
  pEdit = UndoGetLastEdit(pUndo);
  pos   = NedPointPos(pPoint);

  // if(pEdit)
  //   NedLogPrintfEx(NEDLOG_UNDO, "UndoDel(pEdit->pos %ld, pEdit->len %ld, pos %ld, len %ld)\n", pEdit->pos, pEdit->len, pos, len);

  // user is deleting text, might be able to treat as a single delete
  if(pEdit && pEdit->len < 0)
  {
    // prepend delete?
    editLen = -1L * pEdit->len;
    if(pos + len == pEdit->pos)
    {
      pEdit->pData = realloc(pEdit->pData, editLen + len);
      memmove(&pEdit->pData[len], pEdit->pData, editLen);
      NedPointMemGet(pEdit->pData, pPoint, len);
      pEdit->pos -= len;   // position is earlier
      pEdit->len -= len;   // deleted more...

      fDeleted = TRUE;
    }

    // append delete?
    else if(pEdit->pos == pos)
    {
      pEdit->pData = realloc(pEdit->pData, editLen + len);
      NedPointMemGet(&pEdit->pData[editLen], pPoint, len);
      pEdit->len -= len;   // deleted more...
      fDeleted = TRUE;
    }
  }

  // user is doing other insert operations (like toggling comments)
  if(!fDeleted)
  {
    pEdit = UndoGetNewEdit(pUndo);
    pEdit->pData = malloc(len);
    NedPointMemGet(pEdit->pData, pPoint, len);
    pEdit->len = -1L * len;
    pEdit->pos = pos;
  }
}

/*------------------------------------------------------------------------------------------------- 
  Apply the item, used by Undo and Redo.

  @return   none
-------------------------------------------------------------------------------------------------*/
static void UndoApplyEdit(sNedFile_t *pFile, sNedUndoEdit_t *pEdit, undoFlag_t flag)
{
  sNedPoint_t   sPoint2;
  bool_t        fIns;
  long          editLen;

  NedPointFileGotoPos(&pFile->sCursor, pEdit->pos);
  NedAssert(pEdit->len != 0);

  editLen = pEdit->len < 0 ? -1L * pEdit->len : pEdit->len;
  if(flag == FLAG_UNDO)
  {
    fIns = pEdit->len < 0 ? TRUE : FALSE;
  }
  else
  {
    fIns = pEdit->len < 0 ? FALSE : TRUE;
  }

  NedLogPrintfEx(NEDLOG_UNDO, "UndoApplyEdit(%s, pos %ld, len %ld, pData %c)\n", fIns ? "Ins" : "Del",
      pEdit->pos, pEdit->len, *(char *)pEdit->pData);

  // was deleted, so now insert
  if(fIns)
  {
    NedPointInsBlock(&pFile->sCursor, pEdit->pData, editLen);
  }

  // was inserted, so now delete
  else
  {
    NedPointInit(&sPoint2, pFile->pBufHead);
    NedPointFileGotoPos(&sPoint2, pEdit->pos + editLen);
    NedPointDelBlock(&pFile->sCursor, &sPoint2);
  }
}

