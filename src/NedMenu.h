/*!************************************************************************************************
  @file NedMenu.h

  @brief  Ned Menu API provides those functions needed for a menuing system

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_menu Ned Menu - An API for a menuing system.

  The Menuing API is a generic text menuing system. Each main menu (aka menu bar) is attached to a
  window, which may be the backdrop for an all-program main menu.

  Any window of sufficient size (>=5 rows, enough columns for menu items) can host a menu system.

  The concepts are as follows:

  Main Menu - the menu bar (a list of menus)  
  Menu      - A single menu, with 1 or more menu items, aka submenu.  
  Menu Item - An line item in the menu (e.g. Forward... below)  

  See NedTheme.h for menu colors.

  Example of what's displayed when entering the main menu at the Search Menu:

      File  Edit  Search  Goto  Options  Window  Help
                  +-------------------------+
                  |Again                 Fn3|
                  |Backward...        Ctrl-B|
                  |Forward...         Ctrl-F|
                  |Replace...         Ctrl-R|
                  |Project Search...   Alt-P|
                  +-------------------------+

  When specifying names of menus or menu items, use an underscore to indicate which letter should
  be highlighted for quick-select. In the example above, pressing 's' would quick-select the
  "_Search" menu. If pressing 'd' when on the menu bar would quick-select the "E_dit" menu.

*///***********************************************************************************************
#ifndef NED_MENU_H
#define NED_MENU_H

#include "NedKey.h"
#include "NedLog.h"
#include "NedTheme.h"

#define NEDMENU_PICK_NONE   UINT_MAX

#define MENU_HOTCHAR     '_'    // defines which char is highlighted in menubar and menus

typedef bool_t (*pfnSubMenu_t)(nedKey_t key, unsigned *pSubItem);

typedef struct
{
  const char           *szName;         // name of menu item
  nedKey_t              hotkey;         // hotkey to display, or NEDKEY_NONE
  pfnSubMenu_t          pfnSubMenu;     // not NULL if pick window, e.g. "File/Open Recent->"
} sNedMenuItem_t;

typedef struct
{
  const char           *szName;         // name of menu, e.g. File, Edit
  unsigned              numItems;       // # items in menu
  const sNedMenuItem_t *aItems;         // array of items
  bool_t                fNoHotKey;      // don't use hot keys for these items
} sNedMenu_t;

typedef struct
{
  unsigned              numMenus;       // # of menus in menu bar
  const sNedMenu_t     *aMenus;         // array of menus
} sNedMenuBar_t;

typedef struct
{
  bool_t                fPicked;        // TRUE if something was picked
  unsigned              pickedMenu;     // 0-n, menu bar item picked (File) or NEDMENU_PICK_NONE
  unsigned              pickedItem;     // 0-n, item within that menu (File/Open) or NEDMENU_PICK_NONE
  unsigned              pickedSubItem;  // 0-n, item within sub (File/Open/files...) menu or NEDMENU_PICK_NONE
} sMenuPicked_t;

typedef void * hPick_t;

void       *NedMenuNew          (sNedMenuBar_t *pMenuBar, nedThemeIndex_t themeIndex);
bool_t      NedMenuSetParent    (void *hMenu, void *hWinParent);
bool_t      NedMenuResize       (void *hMenu);
void        NedMenuSetTheme     (void *hMenu, nedThemeIndex_t theme);
void        NedMenuFree         (void *hMenu);
bool_t      NedMenuIsMenu       (void *hMenu);
void        NedMenuLogShow      (void *hMenu);
bool_t      NedMenuDraw         (void *hMenu);
unsigned    NedMenuSubMenuCol   (void *hMenu, unsigned nMenu);
const char *NedMenuItemName     (void *hMenu, unsigned nMenu, unsigned nItem);
const sNedMenuItem_t *NedMenuItemPtr(void *hMenu, unsigned nMenu, unsigned nItem);
void        NedMenuEnter        (void *hMenu, unsigned pickMenu);
bool_t      NedMenuInMenu       (void *hMenu);
bool_t      NedMenuFeedKey      (void *hMenu, nedKey_t key, sMenuPicked_t *pPicked);

// pop-up menu that lets you pick from a list of string
hPick_t     NedPickNew        (unsigned top, unsigned left, unsigned numItems, const char **ppszItems, unsigned curItem);
void        NedPickFree       (hPick_t hPick);
bool_t      NedPickIsPick     (hPick_t hPick);
void        NedPickLogShow    (hPick_t hPick);
void        NedPickDraw       (hPick_t hPick);
void        NedPickSetItems   (hPick_t hPick, unsigned numItems, const char **ppszItems, unsigned curItem);
const char *NedPickGetItem    (hPick_t hPick, unsigned nItem);
void        NedPickAdjust     (hPick_t hPick);
void        NedPickSetColors  (hPick_t hPick, nedAttr_t text, nedAttr_t highlight);
bool_t      NedPickFeedKey    (hPick_t hPick, nedKey_t key, unsigned *pPickedItem);

#endif // NED_MENU_H
