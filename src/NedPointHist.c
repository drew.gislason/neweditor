/*!************************************************************************************************
  @file NedPointHist.c

  @brief  Ned Point History Functions

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  Keeps track of the history of major movement in the point system. Useful for "go back" type
  functions like bookmarks and position history. History points are across any set of files.

*///***********************************************************************************************
#include <ctype.h>
#include "Ned.h"
#include "NedLog.h"
#include "NedPoint.h"

/*!------------------------------------------------------------------------------------------------
  Create a history object

  @ingroup  ned_point
  @param    maxPoints     maximum number of points in the history

  @return   pointer to history structure, or NULL if failed.
*///-----------------------------------------------------------------------------------------------
hNedPtHistory_t NedPtHistoryNew(unsigned maxPoints)
{
  hNedPtHistory_t *pHistory;

  pHistory = (hNedList_t)    NedListNew      (unsigned maxItems, unsigned size, pfnListIsSame_t pfnIsSame, pfnListShow_t pfnShow);

  if(pHistory)
  {
    pHistory->sanchk    = NEDHISTORY_SANCHK;
    pHistory->maxPoints = maxPoints;
    NedPtHistoryClear(pHistory);
  }
  return pHistory;
}

/*!------------------------------------------------------------------------------------------------
  Is this an actual history object?

  @ingroup  ned_point
  @param    pHistory     history object

  @return   nothing
*///-----------------------------------------------------------------------------------------------
bool_t NedPointIsHistory(const hNedPtHistory_t *pHistory)
{
  return (pHistory && (pHistory->sanchk == NEDHISTORY_SANCHK)) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Get maximum points in this history.

  @ingroup  ned_point
  @param    pHistory     history object

  @return   maximum points in this history
*///-----------------------------------------------------------------------------------------------
unsigned NedPtHistoryGetMax(hNedPtHistory_t *pHistory)
{
  return NedPointIsHistory(pHistory) ? pHistory->maxPoints : 0;
}

/*!------------------------------------------------------------------------------------------------
  Get a point from the history and index. History may be sparse.

  @ingroup  ned_point
  @param    pHistory     history object
  @param    i            index into history
  @param    pPoint       copy of point

  @return   TRUE if index exists, FALSE if no more history entries
*///-----------------------------------------------------------------------------------------------
bool_t NedPtHistoryGetPt(hNedPtHistory_t *pHistory, sNedPoint_t *pPoint, unsigned idx)
{
  unsigned  i;
  unsigned  thisIdx = 0;
  bool_t    fFound = FALSE;

  // search forward through sparse list (which may be empty)
  for(i=0; i<pHistory->maxPoints; ++i)
  {
    if(NedPointIsPoint(&pHistory->aPoints[i]))
    {
      if(thisIdx == idx)
      {
        *pPoint = pHistory->aPoints[i];
        fFound  = TRUE;
        break;
      }
      else
        ++thisIdx;
    }
  }

  return fFound ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Clear the history object to empty

  @ingroup  ned_point
  @param    pHistory     history object

  @return   nothing
*///-----------------------------------------------------------------------------------------------
void NedPtHistoryClear(hNedPtHistory_t *pHistory)
{
  if(NedPointIsHistory(pHistory))
  {
    pHistory->pos = 0;
    memset(pHistory->aPoints, 0, pHistory->maxPoints * sizeof(sNedPoint_t));
  }
}

/*!------------------------------------------------------------------------------------------------
  Free the history object allocated with NedPtHistoryNew().

  @ingroup  ned_point
  @param    pHistory     history object

  @return   nothing
*///-----------------------------------------------------------------------------------------------
void NedPtHistoryFree(hNedPtHistory_t *pHistory)
{
  if(NedPointIsHistory(pHistory))
  {
    NedPtHistoryClear(pHistory);
    memset(pHistory, 0, sizeof(hNedPtHistory_t));
    free(pHistory);
  }
}

/*!------------------------------------------------------------------------------------------------
  Add to point history. Inserts just prior to current position so "go back" will go to that point.
  Points in history don't have to be in the same buffer. Will keep adding points up to
  pHistory->maxPoints, then will replace oldest point.

  @ingroup  ned_point
  @param    pPoint       pointer to preallocated point
  @param    pHistory     history of points to recall

  @return   nothing
*///-----------------------------------------------------------------------------------------------
void NedPtHistoryAdd(hNedPtHistory_t *pHistory, const sNedPoint_t *pPoint)
{
  NedAssertDbg(NedPointIsHistory(pHistory));

  pHistory->aPoints[pHistory->pos] = *pPoint;

  // on to next so add will 
  ++pHistory->pos;
  if(pHistory->pos >= pHistory->maxPoints)
    pHistory->pos = 0;
}

/*!------------------------------------------------------------------------------------------------
  Closing a file. Remove all items in the history that point to that file.

  @ingroup  ned_point
  @param    pBufHead      pointer to preallocated point
  @param    pHistory      history of points to recall

  @return   nothing
*///-----------------------------------------------------------------------------------------------
void NedPtHistoryCloseFile(hNedPtHistory_t *pHistory, const sNedBuffer_t *pBufHead)
{
  unsigned      i;
  sNedPoint_t  *pPoint;

  pPoint = &pHistory->aPoints[0];
  for(i=0; i<pHistory->maxPoints; ++i)
  {
    // clear item from list if in the closing buffer
    if(NedBufId(pPoint->pBuf) == NedBufId(pBufHead))
      memset(pPoint, 0, sizeof(*pPoint));
    ++pPoint;
  }
}

/*!------------------------------------------------------------------------------------------------
  Recall next point in history (which may be none). This is go forward.

  @ingroup  ned_point
  @param    pHistory     history of points to recall
  @param    pPoint       pointer to point (may be uninitialized)

  @return   TRUE if recalled, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedPtHistoryNext(hNedPtHistory_t *pHistory, sNedPoint_t *pPoint)
{
  unsigned  i;
  bool_t    fFound = FALSE;

  // search forward through sparse list (which may be empty)
  for(i=0; !fFound && i<pHistory->maxPoints; ++i)
  {
    if(NedPointIsPoint(&pHistory->aPoints[pHistory->pos]))
    {
      *pPoint = pHistory->aPoints[pHistory->pos];
      fFound  = TRUE;
    }

    // advance point and check again
    ++pHistory->pos;
    if(pHistory->pos >= pHistory->maxPoints)
      pHistory->pos = 0;
  }

  return fFound ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Recall previous point in history (which may be none if list is empty). This is go back.

  @ingroup  ned_point
  @param    pHistory     history of points to recall
  @param    pPoint       pointer to point (may be uninitialized)

  @return   TRUE if recalled, FALSE if not.
*///-----------------------------------------------------------------------------------------------
bool_t NedPtHistoryPrev(hNedPtHistory_t *pHistory, sNedPoint_t *pPoint)
{
  unsigned  i;
  bool_t    fFound = FALSE;

  // search backward through sparse list, which may be empty
  for(i=0; !fFound && i<pHistory->maxPoints; ++i)
  {
    if(pHistory->pos == 0)
      pHistory->pos = pHistory->maxPoints - 1;
    else
      --pHistory->pos;

    if(NedPointIsPoint(&pHistory->aPoints[pHistory->pos]))
    {
      *pPoint = pHistory->aPoints[pHistory->pos];
      fFound  = TRUE;
    }
  }

  return fFound ? TRUE : FALSE;
}
