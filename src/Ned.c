/*!************************************************************************************************
  @file Ned.c

  @brief Ned Main Entry implementation

  Copyright (c) 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************

#include <sys/utsname.h>
#include "Ned.h"
#include "NedController.h"

static bool_t           m_fNedExit    = FALSE;
static struct utsname   m_uname;
const char             *szNedMachineName = m_uname.machine;

/*-------------------------------------------------------------------------------------------------
  Causes Ned to exit
-------------------------------------------------------------------------------------------------*/
void NedExit(void)
{
  m_fNedExit = TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Main entry point to Ned.

      Examples:
      $ ned
      $ ned *.c *.h "~/Documents/My Files/Temp/t*.txt"
      $ ned -p /path/to/nedproj.json
      $ ned -e /path/to/errfile
      $ ned --version
      $ ned --help

  @ingroup  ned_editor
  @param    argc    # of cmdline arguments. File lists will be expanded by shell.
  @param    argv    cmdline arguments, in string form

  @returns  error code (usually 0). Only returns an error code if Assert().
*///-----------------------------------------------------------------------------------------------
int main(int argc, const char *argv[])
{
  nedKey_t    key;

  // get info about the CPU, machine
  uname(&m_uname);

  // failed to load editor, printf() error message already displayed
  if(!NedControllerStartup(argc, argv))
    return 1;

  while(!m_fNedExit)
  {
    key = NedControllerGetKey();
    NedControllerFeedKey(key);
  }

  NedControllerShutdown();

  return 0;
}
