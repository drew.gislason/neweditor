/*!************************************************************************************************
  @file NedCmdMenu.c

  @brief  Ned Command Implementation for Menu commands

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  This includes any commands that don't fit into other categories.

*///***********************************************************************************************
#include "NedEditor.h"

void NedCmdMenuFile             (void) { NedMenuEnter(NedEditor()->hMenu, NEDMENUINDEX_FILE); }
void NedCmdMenuEdit             (void) { NedMenuEnter(NedEditor()->hMenu, NEDMENUINDEX_EDIT); }
void NedCmdMenuSearch           (void) { NedMenuEnter(NedEditor()->hMenu, NEDMENUINDEX_SEARCH); }
void NedCmdMenuGoto             (void) { NedMenuEnter(NedEditor()->hMenu, NEDMENUINDEX_GOTO); }
void NedCmdMenuOption           (void) { NedMenuEnter(NedEditor()->hMenu, NEDMENUINDEX_OPTION); }
void NedCmdMenuWindow           (void) { NedMenuEnter(NedEditor()->hMenu, NEDMENUINDEX_WINDOW); }
void NedCmdMenuHelp             (void) { NedMenuEnter(NedEditor()->hMenu, NEDMENUINDEX_HELP); }
