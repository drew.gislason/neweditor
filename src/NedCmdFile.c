/*!************************************************************************************************
  @file NedCmdFile.c

  @brief  Ned Command Implementation for File commands

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @ingroup ned_cmd

*///***********************************************************************************************
#include "NedEditor.h"
#include "NedEditSubmenu.h"
#include "NedSettings.h"

// common prompts for MsgChoice() and MsgPrintf()
static const char szSave[]                  = "Save ";
static const char szSaveAs[]                = "Save As: ";
static const char szNotSaved[]              = "Not saved";
static const char szReadOnly[]              = "File is Read Only";
static const char szClosed[]                = "Closed ";
static const char szCannotSave[]            = "Cannot save ";
static const char szSaved[]                 = "Saved ";
static const char szOpened[]                = "Opened ";
static const char szCannotOpen[]            = "Cannot open ";
static const char szExitNoSave[]            = "Exit No Save?";
static const char szReverted[]              = "Reverted ";
static const char szPathTooLong[]           = "Path too long";
static const char szOpen[]                  = "Open: ";

// prompts for NcfMsgChoice
static const char szNcfSaveBeforeClosing[]  = "%s modified. Save before closing? ";
static const char szNcfCloseCannotSave[]    = "Cannot save %s. Close anyway? ";
static const char szNcfSaveAnotherName[]    = "%s not saved. Try another name? ";
static const char szNfcOverwrite[]          = "%s exists. Overwrite?";
static const char szNfOpenedFiles[]         = "Opened %u files";
static const char szNfCannotOpen[]          = "Cannot Open %s";

// common exit keys
static const char szYesNo[]                 = "yn";
static const char szNoYes[]                 = "ny";
static const char szYesNoAll[]              = "yna";


/*-------------------------------------------------------------------------------------------------
  Helper function to get a choice with special prompt. Similar to NedMsgChoice, but allows
  filename to be anywhere in string, not just after prompt.
-------------------------------------------------------------------------------------------------*/
static nedKey_t NcfMsgChoice(const char *szMsg, const char *szPath, const char *szExitKeys)
{
  char  szPrompt[NEDCFG_MAX_WIDTH];

  sprintf(szPrompt, szMsg, NedMsgFitEx(szMsg, szPath, szExitKeys));
  return NedMsgChoice(szPrompt, NULL, szExitKeys);
}

/*-------------------------------------------------------------------------------------------------
  Save the current file. Helper function to NedCmdSave and NedCmdSaveAll, NedCmdSaveAs. This
  does many things.

  1. Save silently to same name (force or fDirty)
  2. File exists, save anyway?
  3. Cannot save %s. Try another name?
  4. User ESCed, do nothing

  Returns 'y' if saved, NEDKEY_ESC if escapped, 'n' if not saved
-------------------------------------------------------------------------------------------------*/
static nedKey_t NcfSave(sNedFile_t *pFile, bool_t fForceSave)
{
  char          szNewPath[PATH_MAX];
  char          szOldPath[PATH_MAX];
  const char   *szPrompt;
  bool_t        fSaved = TRUE;
  nedKey_t      c = 'y';
  long          len;

  // cannot save if read-only
  if(pFile->fRdOnly)
  {
    NedMsgPrintf(szReadOnly);
    return 'n';
  }

  if(fForceSave || pFile->fDirty)
  {
    strcpy(szNewPath, NedFilePath(pFile));
    while(1)
    {
      if(!pFile->fSaveAs)
        fSaved = NedFileSave(pFile);

      else
      {
        // do nothing if user pressed ESC
        len = NedMsgLineEdit(szNewPath, sizeof(szNewPath) - 1, szSaveAs, NULL, NedFilePath(pFile));
        if(len <= 0)
        {
          c = NEDKEY_ESC;
          break;
        }

        // file exists, save anyway?
        if(NedFileExists(szNewPath))
        {
          c = NcfMsgChoice(szNfcOverwrite, szNewPath, szNoYes);
          if(c == 'n')
          {
            NedMsgPrintf(szNotSaved);
            return c;
          }
          else if(c == NEDKEY_ESC)
            break;
        }

        // try saving with the new name
        strcpy(szOldPath, NedFilePath(pFile));
        NedFilePathRename(pFile, szNewPath);
        if(NedFileSave(pFile))
        {
          fSaved = TRUE;
        }
        else
        {
          NedFilePathRename(pFile, szOldPath);
          fSaved = FALSE;
        }
      }

      // saved, we're done
      if(fSaved)
        break;

      if(!fSaved)
      {
        // file not saved. Try another name?
        c = NcfMsgChoice(szNcfSaveAnotherName, szNewPath, szYesNo);
        if(c == 'n')
        {
          fSaved = FALSE;
          break;
        }
        else if(c == NEDKEY_ESC)
          break;
        pFile->fSaveAs = TRUE;
      }
    }
  }

  // no message needed if user aborted
  if(c == NEDKEY_ESC)
  {
    NedMsgClear();
  }

  // display whether file was saved or not
  else
  {
    if(fSaved)
    {
      szPrompt = szSaved;
      pFile->fDirty = pFile->fSaveAs = FALSE;
    }
    else
      szPrompt = szCannotSave;
    NedMsgPrintf("%s%s", szPrompt, NedMsgFit(szPrompt, NedFilePath(pFile)));
  }

  return c;
}

/*-------------------------------------------------------------------------------------------------
  Save all files silently (if possible). Only asks user for input if SaveAs or CannotSave.
-------------------------------------------------------------------------------------------------*/
static nedKey_t NcfSaveAll(void)
{
  sNedEditor_t   *pEditor = NedEditor();
  sNedFile_t     *pFile;
  nedKey_t        c = 'y';

  pFile = pEditor->pFileList;
  while(pFile)
  {
    c = NcfSave(pFile, FALSE);
    if(c == NEDKEY_ESC)
      break;
    pFile = pFile->pNext;
  }
  return c;
}


/*-------------------------------------------------------------------------------------------------
  Helper function to close a file. This has many duties.

  1. File modified. Save before closing?
  2. Cannot save file. Close anyway?
  3. User ESCed, do nothing

  Returns key: y,n,[a],ESC
-------------------------------------------------------------------------------------------------*/
static nedKey_t NcfClose(sNedFile_t *pFile, const char *szExitKeys)
{
  nedKey_t  c       = 'y';
  bool_t    fClose  = TRUE;

  // only ask if file is dirty
  if(pFile->fDirty)
  {
    // assume 'y' to save if no exit keys
    if(szExitKeys == NULL)
      c = 'y';
    else
      c = NcfMsgChoice(szNcfSaveBeforeClosing, NedFilePath(pFile), szExitKeys);

    // yes (or save all) save before close
    if(c == 'y' || c =='a')
    {
      // cannot save file. Close anyway?
      if(!NedFileSave(pFile))
      {
        c = NcfMsgChoice(szNcfCloseCannotSave, NedFilePath(pFile), szNoYes);
        if(c == 'n')
          fClose = FALSE;
      }
    }

    // user is giving up, quit
    if(c == NEDKEY_ESC)
      fClose = FALSE;
  }

  // close the file
  if(fClose)
  {
    NedMsgPrintf("%s%s", szClosed, NedMsgFit(szClosed, NedFilePath(pFile)));
    NedEditorFileClose(pFile);
    NedEditorBuildAroundCursor();
  }
  else
    NedMsgClear();

  return c;   // return the key: y,n,[a],ESC
}

/*!------------------------------------------------------------------------------------------------
  Close the current file

  1. Closed path/to/file
  2. path/to/file modified. Save before closing? (y/n)

  Will ask to save if dirty. Points pFileCur to next file if possible, otherwise previous one. If
  last file is closed, will create a temporary file so there is always a file list and current
  file (pFileList, pFileCur).

  @ingroup  ned_cmd

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdFileClose(void)
{
  NcfClose(NedEditorFileCur(), szYesNo);
}

/*!------------------------------------------------------------------------------------------------
  Close all files but the current one. Will ask on any files that are not saved.

  @ingroup  ned_cmd

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdFileCloseAll(void)
{
  const char   *szExitKeys = szYesNoAll;
  nedKey_t      c;
  sNedFile_t   *pFile;
  sNedFile_t   *pFileNext;
  sNedFile_t   *pFileCur = NedEditorFileCur();

  // validate inputs
  NedAssertDbg(NedEditor()->pFileList);

  pFile = NedEditor()->pFileList;
  while(pFile)
  {
    // we're closing this file, get the next before we do that
    pFileNext = pFile->pNext;

    // don't close current file
    if(pFile != pFileCur)
    {
      c = NcfClose(pFile, szExitKeys);
      if(c == 'a')
        szExitKeys = NULL;
      if(c == NEDKEY_ESC)
        break;
    }

    pFile = pFileNext;
  }
  NedMsgPrintf("All files closed except this one");
}

/*!------------------------------------------------------------------------------------------------
  Create a new untitled file in the same folder as the current file.

  @ingroup  ned_cmd

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdFileNew(void)
{
  sNedFile_t *pFile = NedEditorFileCur();
  char        szNewPath[PATH_MAX];
  const char *szExt;

  szExt = NedStrExt(NedFilePath(pFile));
  strcpy(szNewPath, NedStrDirOnly(NedFilePath(pFile)));
  if(!NedStrAppendName(szNewPath, NedFileUniqueName(NedEditorFileList(), szExt)))
    NedMsgPrintf(szPathTooLong);
  else
  {
    pFile = NedFileNew(NedEditor()->pFileList, szNewPath);

    NedLogPrintfEx(NEDLOG_CMD, "NedFileNew(%s)\n", NedFilePath(pFile));

    if(NedEditor()->pFileList == NULL)
      NedEditor()->pFileList = pFile;
    NedEditorSetFileCur(pFile);
    NedEditorSetWindowItems();
    NedMsgClear();
    NedEditorBuildAroundCursor();
  }
}

/*!------------------------------------------------------------------------------------------------
  Go to next file tab. Does nothing if no file list. Refreshes entire screen. Fn12.

  @ingroup  ned_cmd

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdFileNextOpen(void)
{
  unsigned  count = NedFileCount(NedEditor()->pFileList);
  unsigned  n;

  if(count > 1)
  {
    n = NedEditorFileGetIndex();
    ++n;
    if(n >= count)
      NedEditorFilePick(0);
    else
      NedEditorFilePick(n);
  }
}

/*!------------------------------------------------------------------------------------------------
  Go to next file tab. Does nothing if no file list. Refreshes entire screen. Fn11.

  @ingroup  ned_cmd

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdFilePrevOpen(void)
{
  unsigned  count = NedFileCount(NedEditor()->pFileList);
  unsigned  n;

  if(count > 1)
  {
    n = NedEditorFileGetIndex();
    if(n == 0)
      NedEditorFilePick(count-1);
    else
      NedEditorFilePick(n-1);
  }
}

/*!------------------------------------------------------------------------------------------------
  Open a file from a pick window. Allows user to navigate folders.

  @ingroup  ned_cmd

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdFileOpen(void)
{
  sNedFile_t   *pFile       = NULL;
  bool_t        fDone       = FALSE;
  const char   *szPrompt    = szOpened;
  const char   *szPath;
  char         *psz;

  // in 
  NedLogPrintfEx(NEDLOG_CMD, "NedCmdFileOpen\n");

  // user may have chosen a key bound to the command (not through menu)
  while(!fDone)
  {
    NtmCommonSubMenuFeedKeys(NtmFileOpenSubMenu, &m_FileOpen);

    // user ESCed out
    if(m_FileOpen.pickedSubItem == NEDMENU_PICK_NONE)
    {
      fDone = TRUE;
    }

    // user hit ENTER. It's either a file or folder
    else
    {
      // if it's a folder, go to parent or child folder
      szPath = NedStrNameDirOk(m_FileOpen.ppStrings[m_FileOpen.pickedSubItem]);
      if(NedStrIsFolder(szPath))
      {
        if(strcmp(szPath, "..") == 0)
        {
          NedLibParentPath(m_FileOpen.pData);
        }
        else
        {
          psz = (char *)NedStrLastSlash(m_FileOpen.pData);
          *psz = '\0';
          NedStrAppendName(m_FileOpen.pData, szPath);
          NedStrAppendName(m_FileOpen.pData, "*");
        }
        NedMsgPrintf("Open path %s", m_FileOpen.pData);
      }

      // it's a file, try to open it
      else
      {
        // create full path with filename
        psz = (char *)NedStrLastSlash(m_FileOpen.pData);
        *psz = '\0';
        NedStrAppendName(m_FileOpen.pData, szPath);
        pFile  = NedEditorFileNew(m_FileOpen.pData, 0L);
        NedLogPrintfEx(NEDLOG_CMD, "  opening file %s, pFile %p\n", m_FileOpen.pData, pFile);
        *psz = '\0';
        NedStrAppendName(m_FileOpen.pData, "*");
        if(pFile)
        {
          NedEditorAddPosHistory();
          NedEditor()->pFileCur = pFile;
          NedEditorBuildAroundCursor();
          fDone = TRUE;
        }
        else
          szPrompt = szCannotOpen;
        NedMsgPrintf("%s%s", szPrompt, NedMsgFit(szPrompt, NedFilePath(pFile)));
      }
    }

    // done with list and strings
    if(m_FileOpen.ppStrings)
    {
      free(m_FileOpen.ppStrings);
      m_FileOpen.ppStrings = NULL;
    }
    if(m_FileOpen.hList)
    {
      NedFileListFree(m_FileOpen.hList);
      m_FileOpen.hList = NULL;
    }
  }
}

/*!------------------------------------------------------------------------------------------------
  Bring up a pick list of recent files to open.

  @ingroup  ned_cmd

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdFileOpenRecent(void)
{
  sNedFile_t     *pFile;
  const char     *szPath;
  const char     *szPrompt = szOpened;

  // user may have chosen a key bound to the command (not through menu)
  NtmCommonSubMenuFeedKeys(NtmFileOpenRecentSubMenu, &m_FileOpenRecent);

  if(m_FileOpenRecent.pickedSubItem != NEDMENU_PICK_NONE)
  {
    NedLogPrintfEx(NEDLOG_CMD, "Opening file %s, pickedItem %u\n",
      m_FileOpenRecent.ppStrings[m_FileOpenRecent.pickedSubItem], m_FileOpenRecent.pickedSubItem);
    szPath = m_FileOpenRecent.ppStrings[m_FileOpenRecent.pickedSubItem];
    pFile = NedEditorFileNew(szPath, 0L);
    if(pFile)
    {
      NedEditorAddPosHistory();
      NedEditor()->pFileCur = pFile;
      NedEditorBuildAroundCursor();
    }
    else
      szPrompt = szCannotOpen;
    NedMsgPrintf("%s%s", szPrompt, NedMsgFit(szPrompt, szPath));
  }

  if(m_FileOpenRecent.ppStrings)
  {
    free(m_FileOpenRecent.ppStrings);
    m_FileOpenRecent.ppStrings = NULL;
  }
}

/*!------------------------------------------------------------------------------------------------
  Allow opening multiple files using a wildcard. Similar to on the command-line. Also allows for
  tab completion. For example, if there are folders MyDoc1, MyDoc2, MyDoc3, typing My and pressing
  tab will first add MyDoc1/ to the sting. Press tab again and it will add MyDoc2/ to the string
  instead. Just like at the cmdline.

  Exampls: test*.c, ~/Documents/myfile.py

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdFileOpenWildcard(void)
{
  char        szFileMask[PATH_MAX];
  void       *hList;
  long        len;
  sNedFile_t *pFirstFile     = NULL;
  sNedFile_t *pOldFileCur    = NedEditorFileCur();
  sNedFile_t *pFile;
  void       *hTabComplete    = NULL;
  unsigned    numOpenedFiles  = 0;
  unsigned    numFiles;
  unsigned    i;
  nedKey_t    aSpecialKeys[] = { '\t', '\0' };

  // get the filemask string
  *szFileMask = '\0';
  while(1)
  {
    len = NedMsgLineEdit(szFileMask, sizeof(szFileMask) - 1, szOpen, aSpecialKeys, NedStrDirOnly(NedEditorProjPath()));
    if(!NedMsgIsSpecial(len))
      break;

    if(NedMsgIsEsc(len))
    {
      NedMsgClear();
      return;
    }

    if(NedMsgLineEditIndex(len) == 0)
    {
      if(hTabComplete == NULL)
        hTabComplete = NedTabCompleteNew(sizeof(szFileMask));
      if(hTabComplete)
      {
        if(!NedTabComplete(szFileMask, hTabComplete))
        {
          if(!NedTabCompleteRewind(hTabComplete))
            NedEditorBeep();
          else
            NedTabComplete(szFileMask, hTabComplete);
        }
      }
    }
  }
  NedFileExpandHome(szFileMask, sizeof(szFileMask));

  // try to open the filelist
  hList = NedFileListNew(szFileMask);
  if(!NedFileIsList(hList))
  {
    NedMsgPrintf(szNfCannotOpen, szFileMask);
    return;
  }

  // open all the files found with the wildcard
  numFiles = NedFileListLen(hList);
  for(i=0; i<numFiles; ++i)
  {
    pFile = NedEditorFileNew(NedFileListGetName(hList, i), 0L);
    if(pFile)
    {
      ++numOpenedFiles;
      if(!pFirstFile)
        pFirstFile = pFile;
    }
  }
  NedFileListFree(hList);

  // go to the new file if wasn't already loaded before we started
  if(pFirstFile && pFirstFile != pOldFileCur)
  {
    NedEditorAddPosHistory();
    NedEditorGotoPoint(&pFirstFile->sCursor);
  }
  NedMsgPrintf(szNfOpenedFiles, numOpenedFiles);
}

/*!------------------------------------------------------------------------------------------------
  Revert file to what's saved on disk. Will remove all positions and bookmarks, as the buffer and
  file has changed. Cursor attempts to move to same line/col.

  @ingroup  ned_cmd

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdFileRevert(void)
{
  sNedFile_t     *pFile = NedEditorFileCur();
  sNedBuffer_t   *pBufHead;
  long            fileSize = 0L;
  long            curLine;
  long            curCol;

  pBufHead = NedBufNew(NedFileUniqueId(NedEditorFileList()));
  if(pBufHead)
  {
    // load the file into a new buffer
    if(NedBufFileLoad(pBufHead, NedFilePath(pFile), &fileSize) != NEDERR_NONE)
    {
      NedMsgPrintf("%s%s", szCannotOpen, NedMsgFit(szCannotOpen, NedFilePath(pFile)));
      NedBufChainFree(pBufHead);
    }
    else
    {
      // remember/remove things from old buffer chain
      curLine = NedPointLineGet(&pFile->sCursor, &curCol);

      // autodetect CR/LF
      pFile->pBufHead = pBufHead;
      if(NedBufSearch(pFile->pBufHead, '\r', NULL))
      {
        pFile->fCrLf = TRUE;
        NedBufCrLfToLf(pFile->pBufHead);
      }
      else
      {
        pFile->fCrLf = FALSE;
      }

      NedPointInit(&pFile->sCursor, pBufHead);
      NedPointGotoLineCol(&pFile->sCursor, curLine, curCol);
      pFile->sSelect = pFile->sCursor;
      pFile->fDirty = pFile->fSaveAs = pFile->fInSelect = FALSE;
      NedEditorBuildAroundCursor();
      NedMsgPrintf("%s%s", szReverted, NedMsgFit(szReverted, NedFilePath(pFile)));
    }
  }
}

/*!------------------------------------------------------------------------------------------------
  Save the current file, even if not dirty (force save)

  @ingroup  ned_cmd

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdFileSave(void)
{
  NcfSave(NedEditorFileCur(), TRUE);
}

/*!------------------------------------------------------------------------------------------------
  Save the file under another name/folder

  @ingroup  ned_cmd

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdFileSaveAs(void)
{
  sNedFile_t *pFile = NedEditorFileCur();
  bool_t    fOldValue = pFile->fSaveAs;

  pFile->fSaveAs = TRUE;
  if(NcfSave(pFile, TRUE) == NEDKEY_ESC)
    pFile->fSaveAs = fOldValue;
}

/*!------------------------------------------------------------------------------------------------
  Quietly save all files that are dirty. Only asks if a file can't be saved due to bad path or
  other problems.

  @ingroup  ned_cmd

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdFileSaveAll(void)
{
  NcfSaveAll();
}

/*!------------------------------------------------------------------------------------------------
  Toggle read-only. Allows no further edits. Does NOT change the fact the file may have been
  changed already in memory but not saved to disk.

  @ingroup  ned_cmd

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedCmdFileToggleRdOnly(void)
{
  sNedFile_t   *pFile = NedEditorFileCur();

  if(pFile->fIsBinary)
    NedMsgPrintf("Binary files are always read only");
  else
    pFile->fRdOnly = pFile->fRdOnly ? FALSE : TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Usually bound to key Alt-X. Only asks if cannot save or SaveAs files.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdExitSaveAll(void)
{
  if(NcfSaveAll() != NEDKEY_ESC)
  {
    if(NedEditor()->fSaveProj)
      NedSettingsSave(NedEditorProjPath(), NedEditor());
    NedExit();
  }
}

/*!------------------------------------------------------------------------------------------------
  Usually bound to key Alt-Q. Ask on each unsaved file.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdExitSaveAsk(void)
{
  sNedEditor_t *pEditor = NedEditor();
  sNedFile_t   *pFile;
  nedKey_t      c       = 'y';
  bool_t        fAll    = FALSE;

  pFile = pEditor->pFileList;
  while(pFile)
  {
    if(pFile->fDirty)
    {
      if(!fAll)
        c = NedMsgChoice(szSave, NedFilePath(pFile), szYesNoAll);
      if(c == 'a')
      {
        fAll = TRUE;
        c = 'y';
      }
      if(c == 'y')
        NedFileSave(pFile);
      if(c == NEDKEY_ESC)
        return;
    }
    pFile = pFile->pNext;
  }
  if(pEditor->fSaveProj)
    NedSettingsSave(NedEditorProjPath(), NedEditor());
  NedExit();
}

/*!------------------------------------------------------------------------------------------------
  Usually bound to key Alt-K (kill). Doesn't save anything, including project file. Asks before
  exiting.

  @ingroup  ned_cmd
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedCmdExitSaveNone(void)
{
  if(NedMsgChoice(szExitNoSave, NULL, szNoYes) == 'y')
    NedExit();
}
