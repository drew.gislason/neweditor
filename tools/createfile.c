#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../src/Ned.h"

int main(int argc, char *argv[])
{
  FILE   *fp;
  long    maxLen;
  long    len     = 0;
  int     thisLen;
  long    line    = 0;

  if(argc < 3)
  {
    printf("createfile filename size\n");
    return 1;
  }

  fp = fopen(argv[1], "w");
  if(!fp)
  {
    printf("Failed to create %s\n", argv[1]);
    return 1;
  }
  maxLen = atol(argv[2]);

  while(len < maxLen)
  {
    ++line;
    thisLen = fprintf(fp, "Line %4ld, len %4ld\n", line, len);
    if(thisLen <= 0)
      break;
    len += thisLen;
  }

  printf("Create file %s, %ld lines, len %ld\n", argv[1], line, len);
  fclose(fp);
  return 0;
}
