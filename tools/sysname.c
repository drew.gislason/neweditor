#include <stdio.h>
#include <sys/utsname.h>

int main(void)
{
  struct utsname sUname;

  uname(&sUname);
  printf("sysname  %s\n", sUname.sysname);
  printf("nodename %s\n", sUname.nodename);
  printf("release  %s\n", sUname.release);
  printf("version  %s\n", sUname.version);
  printf("machine  %s\n", sUname.machine);

  return 0;
}
