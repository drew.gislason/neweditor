#!/bin/zsh python3
import json
import sys

if len(sys.argv) < 2:
    print("jpretty [file] | 'json' [indent]")
    sys.exit(1)

if not sys.argv[1][0] == '{':
    f = open(sys.argv[1])
    json_str = f.read()
else:
    json_str = sys.argv[1]

_indent = 2
if len(sys.argv) > 2:
    _indent = int(sys.argv[2])
    if _indent == 0:
        _indent = None

j = json.loads(json_str)
print(json.dumps(j, indent=_indent))
