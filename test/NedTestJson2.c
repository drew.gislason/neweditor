/*!************************************************************************************************
  @file NedTestJson2.c

  @brief  Ned Json2 Test Cases

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include "Ned.h"
#include "NedLog.h"
#include "NedTest2.h"
#include "NedJson2.h"
#include "NedSignal.h"

typedef struct
{
  long      number;
  bool_t    fBool;
  char     *sz;
} sTjcObj_t;

const char szJson2Simple[] = "{\"one\":1}";
const char szJson2SimplePretty[] = "{\n    \"one\": 1\n}";

const char szJson2Complex[] = {
  "{\"bool\":true,"
  "\"number\":5,"
  "\"string\":\"string\","
  "\"boolArray\":["
  "true,"
  "false,"
  "true"
  "],"
  "\"numberArray\":["
  "-1,"
  "2,"
  "3,"
  "2147483647"
  "],"
  "\"stringArray\":["
  "\"the quick brown fox jumped over the lazy dog's back\","
  "\"every good boy does fine\""
  "],"
  "\"emptyArray\":[],"
  "\"obj\":{"
  "\"one\":1,"
  "\"two\":true,"
  "\"three\":\"three\""
  "},"
  "\"objArray\":["
  "{"
  "\"a\":1,"
  "\"b\":true,"
  "\"c\":\"three\""
  "},"
  "{"
  "\"a\":4,"
  "\"b\":false,"
  "\"c\":\"six\""
  "}"
  "]"
  "}"
};

const char szJson2ComplexPretty[] = {
  "{\n"
  "    \"bool\": true,\n"
  "    \"number\": 5,\n"
  "    \"string\": \"string\",\n"
  "    \"boolArray\": [\n"
  "        true,\n"
  "        false,\n"
  "        true\n"
  "    ],\n"
  "    \"numberArray\": [\n"
  "        -1,\n"
  "        2,\n"
  "        3,\n"
  "        2147483647\n"
  "    ],\n"
  "    \"stringArray\": [\n"
  "        \"the quick brown fox jumped over the lazy dog's back\",\n"
  "        \"every good boy does fine\"\n"
  "    ],\n"
  "    \"emptyArray\": [],\n"
  "    \"obj\": {\n"
  "        \"one\": 1,\n"
  "        \"two\": true,\n"
  "        \"three\": \"three\"\n"
  "    },\n"
  "    \"objArray\": [\n"
  "        {\n"
  "            \"a\": 1,\n"
  "            \"b\": true,\n"
  "            \"c\": \"three\"\n"
  "        },\n"
  "        {\n"
  "            \"a\": 4,\n"
  "            \"b\": false,\n"
  "            \"c\": \"six\"\n"
  "        }\n"
  "    ]\n"
  "}"
};

/*-------------------------------------------------------------------------------------------------
  Helper to TestJsonGetComplex()
-------------------------------------------------------------------------------------------------*/
bool_t TjObjCmp(const char *szObj, const sTjcObj_t *pObj)
{
  bool_t          fWorked = TRUE;
  size_t          count;
  size_t          i;
  nedJsonType_t   type;
  const char     *szKey;
  const char     *szValue;

  count = NedJsonGetCount(szObj);
  if(count != 3)
    fWorked = FALSE;
  for(i = 0; fWorked && i < count; ++i)
  {
    szKey = NedJsonGetKey(szObj, i);
    if(!szKey)
      fWorked = FALSE;
    else
    {
      szValue = NedJsonGetValuePtr(szKey, &type);

      if(NedJsonStrCmp("a", szKey) == 0)
      {
        if(type != NEDJSON_NUMBER || NedJsonGetNumber(szValue) != pObj->number)
          fWorked = FALSE;
      }

      else if(NedJsonStrCmp("b", szKey) == 0)
      {
        if(type != NEDJSON_BOOL || NedJsonGetBool(szValue) != pObj->fBool)
          fWorked = FALSE;
      }

      else if(NedJsonStrCmp("c", szKey) == 0)
      {
        if(type != NEDJSON_STRING || NedJsonStrCmp(pObj->sz, szValue) != 0)
          fWorked = FALSE;
      }

      else
        fWorked = FALSE;
    }
  }

  return fWorked;
}

/*-------------------------------------------------------------------------------------------------
  Print out failure with diagnostics
-------------------------------------------------------------------------------------------------*/
void TjLogFailed(size_t len, const char *sz, size_t expLen, const char *szExp)
{
  NedLogPrintf("Failed, got len %zu:\n%s\n, expected len %zu:\n%s\n", len, sz, expLen, szExp);
}

/*-------------------------------------------------------------------------------------------------
  Helper to TestJsonPutComplex()
-------------------------------------------------------------------------------------------------*/
size_t TjPutComplex(char *sz, size_t maxSize, bool_t fPretty)
{
  hNedJson_t  hJson         = NULL;
  size_t      len           = 0;
  unsigned    i;
  bool_t      fBool         = TRUE;
  long        number        = 5;
  char        szString[]    = "string";
  bool_t      boolArray[]   = { TRUE, FALSE, TRUE };
  long        numberArray[] = { -1, 2, 3, INT32_MAX };
  char       *stringArray[] = {
                "the quick brown fox jumped over the lazy dog's back",
                "every good boy does fine" };
  long        one           = 1;
  bool_t      fTwo          = TRUE;
  char        szThree[]     = "three";
  sTjcObj_t   aObj[]        = {{1, TRUE, "three"}, {4, FALSE, "six"}};

  hJson = NedJsonNew(sz, maxSize, fPretty);
  if(NedJsonIsHandle(hJson))
  {
    len += NedJsonPutBegin(hJson, NEDJSON_OBJ);
    len += NedJsonPut(hJson, "bool", NEDJSON_BOOL, &fBool);
    len += NedJsonPut(hJson, "number", NEDJSON_NUMBER, &number);
    len += NedJsonPut(hJson, "string", NEDJSON_STRING, szString);
    len += NedJsonPut(hJson, "boolArray", NEDJSON_ARRAY, NULL);
    for(i=0; i < NumElements(boolArray); ++i)
      len += NedJsonPutScalar(hJson, NEDJSON_BOOL, &boolArray[i]);
    len += NedJsonPutEnd(hJson, NEDJSON_ARRAY);
    len += NedJsonPut(hJson, "numberArray", NEDJSON_ARRAY, NULL);
    for(i=0; i < NumElements(numberArray); ++i)
      len += NedJsonPutScalar(hJson, NEDJSON_NUMBER, &numberArray[i]);
    len += NedJsonPutEnd(hJson, NEDJSON_ARRAY);
    len += NedJsonPut(hJson, "stringArray", NEDJSON_ARRAY, NULL);
    for(i=0; i < NumElements(stringArray); ++i)
      len += NedJsonPutScalar(hJson, NEDJSON_STRING, stringArray[i]);
    len += NedJsonPutEnd(hJson, NEDJSON_ARRAY);
    len += NedJsonPut(hJson, "emptyArray", NEDJSON_ARRAY, NULL);
    len += NedJsonPutEnd(hJson, NEDJSON_ARRAY);
    len += NedJsonPut(hJson, "obj",   NEDJSON_OBJ,    NULL);
    len += NedJsonPut(hJson, "one",   NEDJSON_NUMBER, &one);
    len += NedJsonPut(hJson, "two",   NEDJSON_BOOL,   &fTwo);
    len += NedJsonPut(hJson, "three", NEDJSON_STRING, szThree);
    len += NedJsonPutEnd(hJson, NEDJSON_OBJ);
    len += NedJsonPut(hJson, "objArray", NEDJSON_ARRAY, NULL);
    for(i=0; i < NumElements(aObj); ++i)
    {
      len += NedJsonPutBegin(hJson, NEDJSON_OBJ);
      len += NedJsonPut(hJson, "a", NEDJSON_NUMBER, &aObj[i].number);
      len += NedJsonPut(hJson, "b", NEDJSON_BOOL,   &aObj[i].fBool);
      len += NedJsonPut(hJson, "c", NEDJSON_STRING,  aObj[i].sz);
      len += NedJsonPutEnd(hJson, NEDJSON_OBJ);
    }
    len += NedJsonPutEnd(hJson, NEDJSON_ARRAY);
    len += NedJsonPutEnd(hJson, NEDJSON_OBJ);

    NedJsonFree(hJson);
  }

  return len;
}

/*!------------------------------------------------------------------------------------------------
  Tests NedJsonPut() function simply

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcJsonPutSimple(void)
{
  hNedJson_t  hJson   = NULL;
  size_t      len     = 0;
  long        number  = 1;
  char        szJson[sizeof(szJson2SimplePretty)];

  NedTestBegin();

  // test compact version
  hJson = NedJsonNew(szJson, sizeof(szJson), FALSE);
  if(!NedJsonIsHandle(hJson))
    NedTestFailed();
  len = 0;
  len += NedJsonPutBegin(hJson, NEDJSON_OBJ);
  len += NedJsonPut(hJson, "one", NEDJSON_NUMBER, &number);
  len += NedJsonPutEnd(hJson, NEDJSON_OBJ);
  if(len != strlen(szJson2Simple))
  {
    TjLogFailed(len, szJson, strlen(szJson2Simple), szJson2Simple);
    NedTestFailed();
  }
  if(strcmp(szJson, szJson2Simple) != 0)
  {
    TjLogFailed(len, szJson, strlen(szJson2Simple), szJson2Simple);
    NedTestFailed();
  }
  NedJsonFree(hJson);

  // test pretty version
  hJson = NedJsonNew(szJson, sizeof(szJson), TRUE);
  if(!NedJsonIsHandle(hJson))
    NedTestFailed();
  len = 0;
  len += NedJsonPutBegin(hJson, NEDJSON_OBJ);
  len += NedJsonPut(hJson, "one", NEDJSON_NUMBER, &number);
  len += NedJsonPutEnd(hJson, NEDJSON_OBJ);
  if(len != strlen(szJson2SimplePretty))
  {
    TjLogFailed(len, szJson, strlen(szJson2SimplePretty), szJson2SimplePretty);
    NedTestFailed();
  }
  if(strcmp(szJson, szJson2SimplePretty) != 0)
  {
    TjLogFailed(len, szJson, strlen(szJson2SimplePretty), szJson2SimplePretty);
    NedTestFailed();
  }
  NedJsonFree(hJson);

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Tests NedJsonPut() function with example of each type and nesting

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcJsonPutComplex(void)
{
  size_t      len     = 0;
  size_t      len2    = 0;
  char       *szJson  = NULL;

  NedTestBegin();

  // test compact version
  len = TjPutComplex(NULL, sizeof(szJson), FALSE);
  if(len != strlen(szJson2Complex))
  {
    TjLogFailed(len, szJson, strlen(szJson2Complex), szJson2Complex);
    NedTestFailed();
  }
  szJson = malloc(len + 1);
  if(!szJson)
    NedTestFailed();
  len2 = TjPutComplex(szJson, sizeof(szJson), FALSE);
  if(len2 != len || strcmp(szJson, szJson2Complex) != 0)
  {
    TjLogFailed(len, szJson, strlen(szJson2Complex), szJson2Complex);
    NedTestFailed();
  }
  free(szJson);

  // test pretty version
  len = TjPutComplex(NULL, sizeof(szJson), TRUE);
  if(len != strlen(szJson2ComplexPretty))
  {
    TjLogFailed(len, szJson, strlen(szJson2ComplexPretty), szJson2ComplexPretty);
    NedTestFailed();
  }
  szJson = malloc(len + 1);
  if(!szJson)
    NedTestFailed();
  len2 = TjPutComplex(szJson, sizeof(szJson), TRUE);
  if(len2 != len || strcmp(szJson, szJson2ComplexPretty) != 0)
  {
    TjLogFailed(len, szJson, strlen(szJson2ComplexPretty), szJson2ComplexPretty);
    NedTestFailed();
  }
  free(szJson);

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Get a simple JSON file

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcJsonGetSimple(void)
{
  const char         *szValue;
  const char         *szKey;
  const char         *szObj;
  nedJsonType_t       type      = NEDJSON_INVALID;
  const char          szOne[]   = "one";

  NedTestBegin();

  // verify we can get the number
  szObj = NedJsonGetObj(szJson2SimplePretty);
  if(!szObj || NedJsonGetCount(szObj) != 1)
    NedTestFailed();
  szKey = NedJsonGetKey(szObj, 0);
  if(!szKey || NedJsonStrCmp(szOne, szKey) != 0)
    NedTestFailed();
  szValue = NedJsonGetValuePtr(szKey, &type);
  NedLogPrintf("szJson2SimplePretty %p, szValue %p, type %u\n", szJson2SimplePretty, szValue, type);
  if(!szValue)
    NedTestFailed();
  if(type != NEDJSON_NUMBER)
    NedTestFailed();
  if(NedJsonGetNumber(szValue) != 1)
    NedTestFailed();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Get a complex JSON file

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcJsonGetComplex(void)
{
  bool_t          fBool         = TRUE;
  long            number        = 5;
  char            szString[]    = "string";
  bool_t          boolArray[]   = { TRUE, FALSE, TRUE };
  long            numberArray[] = { -1, 2, 3, INT32_MAX };
  char           *stringArray[] = {
                    "the quick brown fox jumped over the lazy dog's back",
                    "every good boy does fine" };
  long            one           = 1;
  bool_t          fTwo          = TRUE;
  char            szThree[]     = "three";
  sTjcObj_t       aObj[]        = {{1, TRUE, "three"}, {4, FALSE, "six"}};
  size_t          i, j;
  size_t          count;
  size_t          count2;
  const char     *szKey;
  const char     *szObj;
  const char     *szValue;
  const char     *szArray;
  nedJsonType_t   type;
  const char     *aszKeys[] = { "bool", "number", "string", "boolArray", "numberArray",
                                "stringArray", "emptyArray", "obj", "objArray" };
  char            szTmp[20];

  NedTestBegin();

  szObj = NedJsonGetObj(szJson2ComplexPretty);
  if(!szObj)
    NedTestFailed();
  count = NedJsonGetCount(szObj);
  NedLogPrintf("count = %zu\n", count);
  if(count != NumElements(aszKeys))
    NedTestFailed();
  for(i = 0; i < count; ++i)
  {
    szKey = NedJsonGetKey(szObj, i);
    if(!szKey)
      NedTestFailed();
    szValue = NedJsonGetValuePtr(szKey, &type);

    // verify key
    NedLogPrintf("  [%zu] key=%.16s value=%.8s, type %u\n", i, szKey, szValue, type);
    if(NedJsonStrCmp(aszKeys[i], szKey) != 0)
    {
      NedJsonStrNCpy(szTmp, szKey, sizeof(szTmp) - 1);
      szTmp[sizeof(szTmp) - 1] = '\0';
      NedLogPrintf("Bad key \"%s\", expected \"%s\"\n", szTmp, aszKeys[i]);
      NedTestFailed();
    }

    if(NedJsonStrCmp("bool", szKey) == 0)
    {
      if(!szValue || type != NEDJSON_BOOL)
        NedTestFailed();
      if(NedJsonGetBool(szValue) != fBool)
        NedTestFailed();
    }

    else if(NedJsonStrCmp("number", szKey) == 0)
    {
      if(!szValue || type != NEDJSON_NUMBER)
        NedTestFailed();
      if(NedJsonGetNumber(szValue) != number)
        NedTestFailed();
    }

    else if(NedJsonStrCmp("string", szKey) == 0)
    {
      if(!szValue || type != NEDJSON_STRING)
        NedTestFailed();
      if(NedJsonStrCmp(szString, szValue) != 0)
        NedTestFailed();
    }

    else if(NedJsonStrCmp("boolArray", szKey) == 0)
    {
      if(!szValue || type != NEDJSON_ARRAY)
        NedTestFailed();
      szArray = szValue;
      count2 = NedJsonGetCount(szArray);
      for(j = 0; j < count2; ++j)
      {
        szValue = NedJsonGetScalar(szArray, j, &type);
        if(type != NEDJSON_BOOL || !szValue || NedJsonGetBool(szValue) != boolArray[j])
        {
          NedLogPrintf("  [%zu], szValue=%.6s, got %u, expected %u, got type %u, expected %u\n", 
            j, szValue, NedJsonGetBool(szValue), boolArray[j], type, NEDJSON_BOOL);
          NedTestFailed();
        }
      }
    }

    else if(NedJsonStrCmp("numberArray", szKey) == 0)
    {
      if(!szValue || type != NEDJSON_ARRAY)
        NedTestFailed();
      szArray = szValue;
      count2 = NedJsonGetCount(szArray);
      for(j = 0; j < count2; ++j)
      {
        szValue = NedJsonGetScalar(szArray, j, &type);
        if(type != NEDJSON_NUMBER || !szValue || NedJsonGetNumber(szValue) != numberArray[j])
          NedTestFailed();
      }
    }

    else if(NedJsonStrCmp("stringArray", szKey) == 0)
    {
      if(!szValue || type != NEDJSON_ARRAY)
        NedTestFailed();
      szArray = szValue;
      for(j = 0; j < NedJsonGetCount(szArray); ++j)
      {
        szValue = NedJsonGetScalar(szArray, j, &type);
        if(type != NEDJSON_STRING || !szValue || NedJsonStrCmp(stringArray[j], szValue) != 0)
          NedTestFailed();
      }
    }

    else if(NedJsonStrCmp("emptyArray", szKey) == 0)
    {
      szArray = szValue;
      if(NedJsonGetCount(szArray) != 0)
        NedTestFailed();
    }

    else if(NedJsonStrCmp("obj", szKey) == 0)
    {
      if(!szValue || type != NEDJSON_OBJ)
        NedTestFailed();
      szArray = szValue;
      if(NedJsonGetCount(szArray) != 3)
        NedTestFailed();
      szKey = NedJsonGetKey(szArray, 0);
      szValue = NedJsonGetValuePtr(szKey, &type);
      if(type != NEDJSON_NUMBER || !szValue || NedJsonGetNumber(szValue) != one)
        NedTestFailed();
      szKey = NedJsonGetKey(szArray, 1);
      szValue = NedJsonGetValuePtr(szKey, &type);
      if(type != NEDJSON_BOOL || !szValue || NedJsonGetBool(szValue) != fTwo)
        NedTestFailed();
      szKey = NedJsonGetKey(szArray, 2);
      szValue = NedJsonGetValuePtr(szKey, &type);
      if(type != NEDJSON_STRING || !szValue || NedJsonStrCmp(szThree, szValue) != 0)
        NedTestFailed();
    }

    else if(NedJsonStrCmp("objArray", szKey) == 0)
    {
      if(!szValue || type != NEDJSON_ARRAY)
        NedTestFailed();
      szArray = szValue;
      count2 = NedJsonGetCount(szArray);
      if(count2 != 2)
        NedTestFailed();
      for(j = 0; j < count2; ++j)
      {
        szValue = NedJsonGetScalar(szArray, j, &type);
        if(type != NEDJSON_OBJ || !szValue)
          NedTestFailed();
        if(!TjObjCmp(szValue, &aObj[j]))
          NedTestFailed();
      }
    }

    else
    {
      NedTestFailed();
    }
  }

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test function TcJsonIsJson()

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcJsonIsJson(void)
{
  const char szNotJson1[] = "{}";
  const char szNotJson2[] = " \"key without\" : \"array or obj\" ";
  const char szIsJson[]   = "[ \"key with\" : \"array\" ]";

  NedTestBegin();

  if(!NedJsonIsJson(szJson2Simple))
    NedTestFailed();

  if(!NedJsonIsJson(szIsJson))
    NedTestFailed();

  if(!NedJsonIsJson(szJson2Complex))
    NedTestFailed();

  if(NedJsonIsJson(szNotJson1))
    NedTestFailed();

  if(NedJsonIsJson(szNotJson2))
    NedTestFailed();

  NedTestEnd();
}

/*-------------------------------------------------------------------------------------------------
  Test each function
*///-----------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  {
    { "TcJsonPutSimple",  TcJsonPutSimple },
    { "TcJsonPutComplex", TcJsonPutComplex },
    { "TcJsonGetSimple",  TcJsonGetSimple },
    { "TcJsonGetComplex", TcJsonGetComplex },
    { "TcJsonIsJson",     TcJsonIsJson },
  };
  hTestSuite_t        hSuite;
  int                 ret;

  // set up signal handling and logging
  NedSigSetExit(argv[0], NULL);
  NedLogMaskSet(NEDLOG_JSON);

  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestJson2", NumElements(aTestCases), aTestCases);
  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
