/*!************************************************************************************************
  @file NedTestUndo.c

  @brief  Test Ned Undo functions

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @ingroup  ned_test

  This file tests the NedUndo subsystem.

*///***********************************************************************************************
#include <ctype.h>
#include "NedLog.h"
#include "NedTest2.h"
#include "NedUndo.h"

typedef struct {
  sNedFile_t    *pFile;
  const char    *szFile;
} sTestUndoChange_t;

static sNedFile_t *m_pFileList;

static const char m_szUndo1a[] = {
  "#include <stdio.h>\n"
  "\n"
  "int main(void)\n"
  "{\n"
  "  printf(\"hello world\\n\");\n"
  "  return 0;\n"
  "}"
};

static const char m_szUndo1b[] = {
  "#include <>\n"
  "\n"
  "int main(void)\n"
  "{\n"
  "  printf(\"hello world\\n\");\n"
  "  return 0;\n"
  "}"
};

static const char m_szUndo1c[] = {
  "#include <babbity.h>\n"
  "\n"
  "int main(void)\n"
  "{\n"
  "  printf(\"hello world\\n\");\n"
  "  return 0;\n"
  "}"
};

static const char m_szUndo1d[] = {
  "#include <babbity.h>\n"
  "\n"
  "int main(void)\n"
  "{\n"
  "  printf(\"Goodbye\"\n"
  "      \"World\\n\");\n"
  "  return 0;\n"
  "}"
};

static const char m_szUndo2a[] = {
  "/* tcdata_undo2.c */\n"
  "#include <stdio.h>\n"
  "\n"
  "int main(int argc, char *argv[])\n"
  "{\n"
  "  int   i;\n"
  "\n"
  "  for(i=0; i<argc; ++i)\n"
  "    printf(\"%s \", argv[i]);\n"
  "  printf(\"\\n\");\n"
  "\n"
  "  return 0;\n"
  "}\n"
};

static const char m_szUndo2b[] = {
  "/* tcdata_undo2.c */\n"
  "#include <stdio.h>\n"
  "\n"
  "int main(int argc, char *argv[])\n"
  "{\n"
  "  int   i;\n"
  "\n"
  "  // for(i=0; i<argc; ++i)\n"
  "  //   printf(\"%s \", argv[i]);\n"
  "  // printf(\"\\n\");\n"
  "\n"
  "  return 0;\n"
  "}\n"
};

static const char m_szUndo2c[] = {
  "/* tcdata_undo2.c */\n"
  "#include <stdio.h>\n"
  "\n"
  "int main(int argc, char *argv[])\n"
  "{\n"
  "  int   i;\n"
  "\n"
  "  for(i=0; i<argc; ++i)\n"
  "    printf(\"%s \", argv[i]);\n"
  "  printf(\"\\n\");\n"
  "\n"
  "  return 0;\n"
  "}\n"
};

/*-------------------------------------------------------------------------------------------------
  Show a file
  @ingroup    ned_test
  @param      pFile       the file
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TestUndoShowFile(const sNedFile_t *pFile)
{
  sNedPoint_t   pt;
  long          fileSize;
  char         *pszFileCopy = NULL;

  pt = pFile->sCursor;
  NedPointFileBottom(&pt);
  fileSize = NedPointPos(&pt);
  pszFileCopy = malloc(fileSize + 1);
  if(pszFileCopy)
  {
    NedPointFileTop(&pt);
    NedPointMemGet((void *)pszFileCopy, &pt, fileSize);
    pszFileCopy[fileSize] = '\0';
    NedLogPrintf("File:\n%s\n\n", pszFileCopy);
  }
}

/*-------------------------------------------------------------------------------------------------
  Compare a file with a hard-coded string of the file
  @ingroup    ned_test
  @param      pFile       the file
  @param      szFile      the file in string form
  @returns    none
-------------------------------------------------------------------------------------------------*/
bool_t TestUndoCompareFiles(const sNedFile_t *pFile, const char *szFile)
{
  sNedPoint_t   pt;
  long          fileSize;
  char         *pszFileCopy = NULL;
  bool_t        fIsSame     = TRUE;

  pt = pFile->sCursor;
  NedPointFileBottom(&pt);
  fileSize = NedPointPos(&pt);
  NedLogPrintf("TestUndoCompareFiles(fileSize %ld, strlen %ld)\n", fileSize, strlen(szFile));
  NedPointFileTop(&pt);
  pszFileCopy = malloc(fileSize + 1);
  if(!pszFileCopy)
    fIsSame = FALSE;
  else if(NedPointMemGet((void *)pszFileCopy, &pt, fileSize) != fileSize)
  {
    NedLogPrintf("badsize\n");
    fIsSame = FALSE;
  }
  else if(strncmp(pszFileCopy, szFile, fileSize) != 0)
  {
    NedLogPrintf("!!FILE COMPARE FAILED!!\n");
    NedLogPrintf("Expected:\n%s\n\n", szFile);
    pszFileCopy[fileSize] = '\0';
    NedLogPrintf("Got:\n%s\n\n", pszFileCopy);
    fIsSame = FALSE;
  }

  if(pszFileCopy)
    free(pszFileCopy);

  return fIsSame;
}

/*-------------------------------------------------------------------------------------------------
  Insert characters 1 char at a time.
  @ingroup    ned_test
  @returns    TRUE if entire thing worked
-------------------------------------------------------------------------------------------------*/
bool_t NedTestInsChars(hUndoList_t hUndoList, sNedFile_t *pFile, const nedChar_t *pData, long len)
{
  long    i;
  bool_t  fWorked = TRUE;

  for(i=0; fWorked && i < len; ++i)
  {
    if(!NedUndoInsChar(hUndoList, &pFile->sCursor, pData[i]))
      fWorked = FALSE;
  }

  return fWorked;
}

/*-------------------------------------------------------------------------------------------------
  Delete characters 1 char at a time
  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
bool_t NedTestDelChars(hUndoList_t hUndoList, sNedFile_t *pFile, long len)
{
  long    i;
  bool_t  fWorked = TRUE;

  for(i=0; fWorked && i < len; ++i)
    fWorked &= NedUndoDelChar(hUndoList, &pFile->sCursor);    

  return fWorked;
}

/*-------------------------------------------------------------------------------------------------
  Get a file by the point
  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
sNedFile_t * TestUndoGetFileByPoint(const sNedPoint_t *pPoint)
{
  sNedPoint_t   sPoint  = *pPoint;
  sNedFile_t   *pFile   = m_pFileList;

  NedPointFileTop(&sPoint);
  while(pFile)
  {
    if(pPoint->pBuf == pFile->pBufHead)
      break;
    pFile = pFile->pNext;
  }

  return pFile;
}

/*-------------------------------------------------------------------------------------------------
  Free all the files in the m_pFileList
  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TestUndoFreeAllFiles(void)
{
  NedFileFreeAll(m_pFileList);
  m_pFileList = NULL;
}

/*-------------------------------------------------------------------------------------------------
  Assumes file is a single buffer...
  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TestUndoLogShowFile(const sNedFile_t *pFile)
{
  NedLogPrintf("%s\n", (char *)(pFile->pBufHead->aData));
}

/*!------------------------------------------------------------------------------------------------
  Test NedUndoListLogShow() function

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcUndoShow(void)
{
  sNedFile_t     *pFile     = NedFileNew(NULL, "tcdata_undo1.c");
  hUndoList_t     hUndoList = NedUndoListNew(TestUndoGetFileByPoint);
  sNedPoint_t     sPoint;

  NedTestBegin();

  m_pFileList = pFile;
  if(!pFile)
    NedTestFailed();

  NedUndoLogShow(hUndoList, TRUE);
  if(NedUndoCount(hUndoList) != 0)
    NedTestFailed();

  // create 2 undo entries
  NedPointFileGotoPos(&pFile->sCursor, 10);
  sPoint = pFile->sCursor;
  NedPointFileGotoPos(&sPoint, 17);
  NedUndoDelBlock(hUndoList, &pFile->sCursor, &sPoint);
  NedUndoInsBlock(hUndoList, &pFile->sCursor, (void *)"babbity.h", 9);
  NedUndoEditDone(hUndoList);

  NedUndoLogShow(hUndoList, TRUE);
  if(NedUndoCount(hUndoList) != 1)
    NedTestFailed();

  NedTestPassed();
  NedTestEnd();

  NedUndoListFree(hUndoList);
  TestUndoFreeAllFiles();
}

/*!------------------------------------------------------------------------------------------------
  Tests a basic undo. Does undo 3 times (insert, delete, inserts and deletes)

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcUndoBasic(void)
{
  sNedFile_t     *pFile     = NedFileNew(NULL, "tcdata_undo1.c");
  sNedFile_t     *pFile2;
  hUndoList_t     hUndoList = NedUndoListNew(TestUndoGetFileByPoint);
  sNedPoint_t     sPoint;
  bool_t          fWorked;
  const char     *aszFiles[] = { m_szUndo1c, m_szUndo1b, m_szUndo1a };
  unsigned        i;

  NedTestBegin();

  m_pFileList = pFile;
  if(!pFile)
    NedTestFailed();

  NedUndoLogShow(hUndoList, TRUE);
  if(NedUndoCount(hUndoList) != 0)
    NedTestFailed();

  // create 3 Undos (delete, insert, set of inserts/deletes)

  // org file:
  // #include <stdio.h>
  //
  // int main(void)
  // {
  //   printf("hello world\n");
  //   return 0;
  // }
  if(!TestUndoCompareFiles(pFile, m_szUndo1a))
    NedTestFailed();

  // 1. delete: stdio.h
  // #include <>
  //
  // int main(void)
  // {
  //   printf("hello world\n");
  //   return 0;
  // }
  NedPointFileGotoPos(&pFile->sCursor, 10);
  sPoint = pFile->sCursor;
  NedPointFileGotoPos(&sPoint, 17);
  NedUndoDelBlock(hUndoList, &pFile->sCursor, &sPoint);
  NedUndoEditDone(hUndoList);
  if(!TestUndoCompareFiles(pFile, m_szUndo1b))
    NedTestFailed();

  // 2. insert: babbity.h
  // #include <babbity.h>
  //
  // int main(void)
  // {
  //   printf("hello world\n");
  //   return 0;
  // }
  NedUndoInsBlock(hUndoList, &pFile->sCursor, (void *)"babbity.h", 9);
  NedUndoEditDone(hUndoList);
  if(!TestUndoCompareFiles(pFile, m_szUndo1c))
    NedTestFailed();

  // 3. insert, deletes to make : babbity.h
  // this:                                becomes:
  // #include <babbity.h>                 #include <babbity.h>
  // 
  // int main(void)                       int main(void)
  // {                                    {
  //   printf("hello world\n");             printf("Goodbye"
  //   return 0;                                "World\n");
  // }                                      return 0;
  //                                      }
  NedPointGotoLineCol(&pFile->sCursor, 5, 11);
  fWorked = TRUE;
  fWorked &= NedTestInsChars(hUndoList, pFile, (void *)"g", 1);
  fWorked &= NedUndoDelBackspace(hUndoList, &pFile->sCursor);
  fWorked &= NedTestInsChars(hUndoList, pFile, (void *)"Goodbye\"\n", 9);
  fWorked &= NedTestDelChars(hUndoList, pFile, 11);         // delete "hello world"
  fWorked &= NedUndoInsBlock(hUndoList, &pFile->sCursor, (uint8_t *)"      \"World", 12);
  if(!TestUndoCompareFiles(pFile, m_szUndo1d))
    NedTestFailed();
  if(!fWorked)
    NedTestFailed();
  NedUndoLogShow(hUndoList, TRUE);
  if(NedUndoCount(hUndoList) != 3)
    NedTestFailed();

  for(i=0; i < NumElements(aszFiles); ++i)
  {
    pFile2 = NedUndoUndo(hUndoList);
    if(pFile2 != pFile || !TestUndoCompareFiles(pFile, aszFiles[i]))
      NedTestFailed();
  }

  NedTestPassed();
  NedTestEnd();

  NedUndoListFree(hUndoList);
  TestUndoFreeAllFiles();
}

/*!------------------------------------------------------------------------------------------------
  Same as TcUndoBasic(), but applies redo

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcUndoRedo(void)
{
  sNedFile_t     *pFile       = NedFileNew(NULL, "tcdata_undo1.c");
  sNedFile_t     *pFile2;
  hUndoList_t     hUndoList   = NedUndoListNew(TestUndoGetFileByPoint);
  sNedPoint_t     sPoint;
  bool_t          fWorked;
  const char     *aszFiles[]  = { m_szUndo1c, m_szUndo1b, m_szUndo1a };
  const char     *aszFiles2[] = { m_szUndo1b, m_szUndo1c, m_szUndo1d };
  unsigned        i;

  NedTestBegin();

  m_pFileList = pFile;
  if(!pFile)
    NedTestFailed();

  NedUndoLogShow(hUndoList, TRUE);
  if(NedUndoCount(hUndoList) != 0)
    NedTestFailed();

  // Nothing to undo
  pFile2 = NedUndoUndo(hUndoList);
  if(pFile2 != NULL)
    NedTestFailed();

  // Nothing to redo
  pFile2 = NedUndoRedo(hUndoList);
  if(pFile2 != NULL)
    NedTestFailed();

  // create 3 Undos (delete, insert, set of inserts/deletes)

  // org file:
  // #include <stdio.h>
  //
  // int main(void)
  // {
  //   printf("hello world\n");
  //   return 0;
  // }
  if(!TestUndoCompareFiles(pFile, m_szUndo1a))
    NedTestFailed();

  // 1. delete: stdio.h
  // #include <>
  //
  // int main(void)
  // {
  //   printf("hello world\n");
  //   return 0;
  // }
  NedPointFileGotoPos(&pFile->sCursor, 10);
  sPoint = pFile->sCursor;
  NedPointFileGotoPos(&sPoint, 17);
  NedUndoDelBlock(hUndoList, &pFile->sCursor, &sPoint);
  NedUndoEditDone(hUndoList);
  if(!TestUndoCompareFiles(pFile, m_szUndo1b))
    NedTestFailed();

  // 2. insert: babbity.h
  // #include <babbity.h>
  //
  // int main(void)
  // {
  //   printf("hello world\n");
  //   return 0;
  // }
  NedUndoInsBlock(hUndoList, &pFile->sCursor, (void *)"babbity.h", 9);
  NedUndoEditDone(hUndoList);
  if(!TestUndoCompareFiles(pFile, m_szUndo1c))
    NedTestFailed();

  // 3. insert and deletes
  // this:                                becomes:
  // #include <babbity.h>                 #include <babbity.h>
  // 
  // int main(void)                       int main(void)
  // {                                    {
  //   printf("hello world\n");             printf("Goodbye"
  //   return 0;                                "World\n");
  // }                                      return 0;
  //                                      }
  NedPointGotoLineCol(&pFile->sCursor, 5, 11);
  fWorked = TRUE;
  fWorked &= NedTestInsChars(hUndoList, pFile, (void *)"g", 1);
  fWorked &= NedUndoDelBackspace(hUndoList, &pFile->sCursor);
  fWorked &= NedTestInsChars(hUndoList, pFile, (void *)"Goodbye\"\n", 9);
  fWorked &= NedTestDelChars(hUndoList, pFile, 11);         // delete "hello world"
  fWorked &= NedUndoInsBlock(hUndoList, &pFile->sCursor, (uint8_t *)"      \"World", 12);
  if(!TestUndoCompareFiles(pFile, m_szUndo1d))
    NedTestFailed();
  if(!fWorked)
    NedTestFailed();
  NedUndoLogShow(hUndoList, TRUE);
  if(NedUndoCount(hUndoList) != 3)
    NedTestFailed();

  // redo should not be possible at this point
  pFile2 = NedUndoRedo(hUndoList);
  if(pFile2 != NULL)
    NedTestFailed();

  // undo all the changes in the file
  for(i=0; i < NumElements(aszFiles); ++i)
  {
    pFile2 = NedUndoUndo(hUndoList);
    if(pFile2 != pFile || !TestUndoCompareFiles(pFile, aszFiles[i]))
      NedTestFailed();
  }

  NedUndoLogShow(hUndoList, TRUE);
  TestUndoShowFile(pFile);

  // redo once
  pFile2 = NedUndoRedo(hUndoList);
  if(pFile2 != pFile || !TestUndoCompareFiles(pFile, m_szUndo1b))
    NedTestFailed();

  // undo after redo
  pFile2 = NedUndoUndo(hUndoList);
  if(pFile2 != pFile || !TestUndoCompareFiles(pFile, m_szUndo1a))
    NedTestFailed();

  // make sure can't undo past beginning of changes
  pFile2 = NedUndoUndo(hUndoList);
  if(pFile2 != NULL)
    NedTestFailed();

  // redo until done
  for(i=0; i < NumElements(aszFiles2); ++i)
  {
    pFile2 = NedUndoRedo(hUndoList);
    if(pFile2 != pFile || !TestUndoCompareFiles(pFile, aszFiles2[i]))
      NedTestFailed();
  }

  // should be no more redos
  pFile2 = NedUndoRedo(hUndoList);
  if(pFile2 != NULL)
    NedTestFailed();

  NedTestPassed();
  NedTestEnd();

  NedUndoListFree(hUndoList);
  TestUndoFreeAllFiles();
}

/*!------------------------------------------------------------------------------------------------
  Test complex undo/redos (like reformatting a paragraph or inserting comment lines)

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcUndoAdvanced(void)
{
  NedTestBegin();
  NedTestSkipped();
  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test multi-file undo, redos

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcUndoMultiFile(void)
{
  sNedFile_t         *pFile1;
  sNedFile_t         *pFile2;
  sNedFile_t         *pFileCheck;
  hUndoList_t         hUndoList   = NedUndoListNew(TestUndoGetFileByPoint);
  sNedPoint_t         sPoint;
  bool_t              fWorked;
  unsigned            i;
  sTestUndoChange_t   aUndoExpected[] = {
    { .szFile = m_szUndo1c },
    { .szFile = m_szUndo2b },
    { .szFile = m_szUndo1b },
    { .szFile = m_szUndo2a },
    { .szFile = m_szUndo1a }
  };
  sTestUndoChange_t   aRedoExpected[] = {
    { .szFile = m_szUndo1b },
    { .szFile = m_szUndo2b },
    { .szFile = m_szUndo1c },
    { .szFile = m_szUndo2c },
    { .szFile = m_szUndo1d }
  };
  sTestUndoChange_t   aUndoFileRemoved[] = {
    { .szFile = m_szUndo1c },
    { .szFile = m_szUndo1b },
    { .szFile = m_szUndo1a }
  };
  sTestUndoChange_t   aRedoFileRemoved[] = {
    { .szFile = m_szUndo1b },
    { .szFile = m_szUndo1c },
    { .szFile = m_szUndo1d }
  };
#if 0
  sTestUndoChange_t   aUndoExpected2[] = {
    { .szFile = m_szUndo1c },
    { .szFile = m_szUndo2b },
    { .szFile = m_szUndo1b },
    { .szFile = m_szUndo2a },
    { .szFile = m_szUndo1a }
  };
  sTestUndoChange_t   aRedoExpected2[] = {
    { .szFile = m_szUndo1b },
    { .szFile = m_szUndo2b },
    { .szFile = m_szUndo1c },
    { .szFile = m_szUndo2c },
    { .szFile = m_szUndo1d }
  };
  sTestUndoChange_t   aUndoFileRemoved2[] = {
    { .szFile = m_szUndo1c },
    { .szFile = m_szUndo1b },
    { .szFile = m_szUndo1a }
  };
  sTestUndoChange_t   aRedoFileRemoved2[] = {
    { .szFile = m_szUndo1b },
    { .szFile = m_szUndo1c },
    { .szFile = m_szUndo1d }
  };
#endif

  NedTestBegin();

  // load to files
  pFile1 = NedFileNew(NULL, "tcdata_undo1.c");
  m_pFileList = pFile1;
  if(!pFile1)
    NedTestFailed();
  pFile2 = NedFileNew(m_pFileList, "tcdata_undo2.c");
  if(!pFile2)
    NedTestFailed();
  if(!TestUndoCompareFiles(pFile1, m_szUndo1a))
    NedTestFailed();
  if(!TestUndoCompareFiles(pFile2, m_szUndo2a))
    NedTestFailed();

  aUndoExpected[0].pFile = pFile1;
  aUndoExpected[1].pFile = pFile2;
  aUndoExpected[2].pFile = pFile1;
  aUndoExpected[3].pFile = pFile2;
  aUndoExpected[4].pFile = pFile1;

  aRedoExpected[0].pFile = pFile1;
  aRedoExpected[1].pFile = pFile2;
  aRedoExpected[2].pFile = pFile1;
  aRedoExpected[3].pFile = pFile2;
  aRedoExpected[4].pFile = pFile1;

  aUndoFileRemoved[0].pFile = pFile1;
  aUndoFileRemoved[1].pFile = pFile1;
  aUndoFileRemoved[2].pFile = pFile1;

  aRedoFileRemoved[0].pFile = pFile1;
  aRedoFileRemoved[1].pFile = pFile1;
  aRedoFileRemoved[2].pFile = pFile1;

  // Modify file 1 with 3 Undos, file2 with 2 Undos, intermixed

  // 1. delete stdio.h File1
  // #include <>
  //
  // int main(void)
  // {
  //   printf("hello world\n");
  //   return 0;
  // }
  NedPointFileGotoPos(&pFile1->sCursor, 10);
  sPoint = pFile1->sCursor;
  NedPointFileGotoPos(&sPoint, 17);
  NedUndoDelBlock(hUndoList, &pFile1->sCursor, &sPoint);
  NedUndoEditDone(hUndoList);
  if(!TestUndoCompareFiles(pFile1, m_szUndo1b))
    NedTestFailed();

  // 2. insert comments File2
  // /* tcdata_undo2.c */
  // #include <stdio.h>
  //
  // int main(int argc, char *argv[])
  // {
  //   int   i;
  //
  //   // for(i=0; i<argc; ++i)
  //   //   printf("%s ", argv[i]);
  //   // printf("\n");
  //
  //   return 0;
  // }
  for(i=0; i<3; ++i)
  {
    NedPointGotoLineCol(&pFile2->sCursor, 8 + i, 3);
    NedUndoInsStr(hUndoList, &pFile2->sCursor, "// ");
  }
  NedUndoEditDone(hUndoList);
  if(!TestUndoCompareFiles(pFile2, m_szUndo2b))
    NedTestFailed();

  // 3. insert babbity.h File1
  // #include <babbity.h>
  //
  // int main(void)
  // {
  //   printf("hello world\n");
  //   return 0;
  // }
  NedUndoInsBlock(hUndoList, &pFile1->sCursor, (void *)"babbity.h", 9);
  NedUndoEditDone(hUndoList);
  if(!TestUndoCompareFiles(pFile1, m_szUndo1c))
    NedTestFailed();

  // 4. remove comments File2
  // /* tcdata_undo2.c */
  // #include <stdio.h>
  //
  // int main(int argc, char *argv[])
  // {
  //   int   i;
  //
  //   for(i=0; i<argc; ++i)
  //     printf("%s ", argv[i]);
  //   printf("\n");
  //
  //   return 0;
  // }
  for(i=0; i<3; ++i)
  {
    NedPointGotoLineCol(&pFile2->sCursor, 8 + i, 3);
    sPoint = pFile2->sCursor;
    NedPointCharAdvance(&sPoint, 3);
    NedUndoDelBlock(hUndoList, &pFile2->sCursor, &sPoint);
  }
  // NedUndoEditDone(hUndoList);  // should automatically created differnt undo, since different file
  if(!TestUndoCompareFiles(pFile2, m_szUndo2c))
    NedTestFailed();

  // 5. insert and deletes
  // this:                                becomes:
  // #include <babbity.h>                 #include <babbity.h>
  // 
  // int main(void)                       int main(void)
  // {                                    {
  //   printf("hello world\n");             printf("Goodbye"
  //   return 0;                                "World\n");
  // }                                      return 0;
  //                                      }
  NedPointGotoLineCol(&pFile1->sCursor, 5, 11);
  fWorked = TRUE;
  fWorked &= NedTestInsChars(hUndoList, pFile1, (void *)"Goodbye\"\n", 9);
  fWorked &= NedTestDelChars(hUndoList, pFile1, 11);         // delete "hello world"
  fWorked &= NedUndoInsBlock(hUndoList, &pFile1->sCursor, (uint8_t *)"      \"World", 12);
  if(!TestUndoCompareFiles(pFile1, m_szUndo1d))
    NedTestFailed();
  if(!fWorked)
    NedTestFailed();

  // verify there are 5 undos
  NedUndoLogShow(hUndoList, TRUE);
  if(NedUndoCount(hUndoList) != 5)
    NedTestFailed();

  // undo all the changes in the file
  NedLogPrintf("!!HERE 1\n");
  for(i=0; i < NumElements(aUndoExpected); ++i)
  {
    pFileCheck = NedUndoUndo(hUndoList);
    if(pFileCheck != aUndoExpected[i].pFile || !TestUndoCompareFiles(aUndoExpected[i].pFile, aUndoExpected[i].szFile))
      NedTestFailed();
  }
  NedLogPrintf("!!HERE 2\n");

  for(i=0; i < NumElements(aRedoExpected); ++i)
  {
    pFileCheck = NedUndoRedo(hUndoList);
    if(pFileCheck != aRedoExpected[i].pFile || !TestUndoCompareFiles(aRedoExpected[i].pFile, aRedoExpected[i].szFile))
      NedTestFailed();
  }
  NedLogPrintf("!!HERE 3\n");

  // remove file2
  NedLogPrintf("!!REMOVING FILE %p\n", pFile2);
  NedUndoFileRemove(hUndoList, pFile2);
  NedLogPrintf("!!DONE\n");

  // undo all the changes in the file
  for(i=0; i < NumElements(aUndoFileRemoved); ++i)
  {
    pFileCheck = NedUndoUndo(hUndoList);
    if(pFileCheck != aUndoFileRemoved[i].pFile || !TestUndoCompareFiles(aUndoFileRemoved[i].pFile, aUndoFileRemoved[i].szFile))
      NedTestFailed();
  }

  for(i=0; i < NumElements(aRedoFileRemoved); ++i)
  {
    pFileCheck = NedUndoRedo(hUndoList);
    if(pFileCheck != aRedoFileRemoved[i].pFile || !TestUndoCompareFiles(aRedoFileRemoved[i].pFile, aRedoFileRemoved[i].szFile))
      NedTestFailed();
  }  

  NedTestPassed();
  NedTestEnd();

  NedUndoListFree(hUndoList);
  TestUndoFreeAllFiles();
}

/*!------------------------------------------------------------------------------------------------
  Test Cut and Paste operations with Undo.

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcUndoCutPaste(void)
{
  NedTestBegin();
  NedTestSkipped();
  NedTestEnd();
}

/*-------------------------------------------------------------------------------------------------
  Run each test case
-------------------------------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
  { "TcUndoShow",       TcUndoShow },
  { "TcUndoBasic",      TcUndoBasic },
  { "TcUndoRedo",       TcUndoRedo },
  { "TcUndoAdvanced",   TcUndoAdvanced },
  { "TcUndoMultiFile",  TcUndoMultiFile },
  { "TcUndoCutPaste",   TcUndoCutPaste }
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestUndo", NumElements(aTestCases), aTestCases);
  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
