/*!************************************************************************************************
  @file NedTestLang.c

  @brief  Ned Log Test Cases

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include <unistd.h>
#include "Ned.h"
#include "NedLog.h"
#include "NedTest2.h"
#include "NedUtilFile.h"
#include "NedUtilStr.h"
#include "NedLanguage.h"

/*!------------------------------------------------------------------------------------------------
  Get an answer from the user

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
bool_t TestLogOk(const char *szPrompt)
{
  char sz[64];
  printf("\n%s (y/n) ", szPrompt);
  fflush(stdout);
  memset(sz, 0, sizeof(sz));
  fgets(sz, sizeof(sz)-1, stdin);
  return (*sz == 'n' || *sz == 'N') ? FALSE : TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Test NedLogDump()

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcLogDump(void)
{
  char szString[] = "Hello World is the classic phrase. How does this look in the dump world?\n";
  char szStringDbg[] = "Dbg: Hello World is the classic phrase. How does this look in the dump world?\n";

  NedTestBegin();

  NedLogDump(szString, sizeof(szString), 16, 2);
  if(!TestLogOk("in the log file, is dump 16 characters, indented by 2?"))
    NedTestFailed();

  NedLogDump(szString, sizeof(szString), 24, 4);
  if(!TestLogOk("is dump 24 characters, indented by 4?"))
    NedTestFailed();

  NedLogDump(szStringDbg, sizeof(szStringDbg), 8, 0);
  if(!TestLogOk("is Dbg: dump 8 characters, not indented?"))
    NedTestFailed();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test NedLogPrintf()

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcLogPrintf(void)
{
  NedTestBegin();

  if(NedLogPrintf("Hello %d %s\n", 3, "Worlds") != 15)
    NedTestFailed();

  if(!TestLogOk("Did it print Hello 3 Worlds?"))
    NedTestFailed();

  NedLogPrintf("Date/time: %s\n", NedStrDateTimeCur());
  if(!TestLogOk("Did it print the current date/time?"))
    NedTestFailed();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test NedLogPrintfEx()

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcLogPrintfEx(void)
{
  nedLogMask_t  oldMask;
  nedLogMask_t  newMask;

  NedTestBegin();

  oldMask = NedLogMaskSet(NEDLOG_CMD | NEDLOG_VIEW);
  if(oldMask != 0)
    NedTestFailed();
  if(NedLogMaskGet() != (NEDLOG_CMD | NEDLOG_VIEW))
    NedTestFailed();

  NedLogPrintfEx(NEDLOG_CMD, "A: should print NEDLOG_CMD\n");
  NedLogPrintfEx(NEDLOG_WIN, "B: should NOT print NEDLOG_WIN\n");
  NedLogPrintfEx(NEDLOG_VIEW, "C: should print NEDLOG_VIEW\n");
  NedLogPrintfEx(NEDLOG_EDITOR | NEDLOG_VIEW, "D: should print NEDLOG_EDITOR | NEDLOG_VIEW\n");
  NedLogPrintfEx(NEDLOG_ALL, "E: should print %s\n", "NEDLOG_ALL");
  if(!TestLogOk("Did it print A,C,D,E but not B?"))
    NedTestFailed();

  newMask = NedLogMaskSet(oldMask);
  if(newMask != (NEDLOG_CMD | NEDLOG_VIEW))
    NedTestFailed();

  NedTestEnd();  
}

/*!------------------------------------------------------------------------------------------------
  Test NedLogSize()

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcLogSize(void)
{
  const char szHello[] = "Hello";
  const char szWorld[] = "World\n";
  size_t    len1;
  size_t    len2;

  NedTestBegin();

  NedLogSizeReset();
  NedLogPrintf("Hello");
  NedLogMaskSet(NEDLOG_MSG);
  NedLogPrintfEx(NEDLOG_MSG, "World\n");
  len1 = NedLogSizeGet();
  len2 = strlen(szHello) + strlen(szWorld);
  NedLogPrintf("NedLogSizeGet() %zu, strlen strings %zu\n", len1, len2);
  if(len1 != len2)
    NedTestFailed();

  NedTestEnd();  
}

/*-------------------------------------------------------------------------------------------------
  Test each function
-------------------------------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcLogDump",      TcLogDump,      TC_USER_INPUT },
    { "TcLogPrintf",    TcLogPrintf,    TC_USER_INPUT },
    { "TcLogPrintfEx",  TcLogPrintfEx,  TC_USER_INPUT },
    { "TcLogSize",      TcLogSize }
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestLog", NumElements(aTestCases), aTestCases);
  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
