/*!************************************************************************************************
  @file NedTestMenu.c

  @brief  NedTestMenu - Tests the menuing system and the NedKeyMap functionality.

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  NedTestMenu requires user input to test and verify. For example, manually checking the menu
  and keymappings against `NedManual.md`.

*///***********************************************************************************************
#include "Ned.h"
#include "NedFile.h"
#include "NedKeyMap.h"
#include "NedWin.h"
#include "NedMenu.h"
#include "NedTest2.h"

#define OTHER_ROWS 3

typedef struct
{
  unsigned      nMenu;
  unsigned      nItem;
  bool_t        fEnteredViaMenu;
  void         *hList;            // see hList NedFileListNew(), NedUtilFile.h
  hPick_t      *hPick;            // see hPick NedPickNew(), NedMenu.h
  unsigned      pickedSubItem;
  unsigned      numItems;
  const char  **ppStrings;
} sNtmCsm_t;

static const char * maUtWindowMenuItems[] =
{
  "file.c"
};

static const char * maUtWindowMenuItems3[] =
{
  "file1.c", 
  "file2.c",
  "/user/drewg/.../file3.c"
};

// globals subsitute for Editor variables
static unsigned     m_themeIndex  = NEDTHEME_CLASSIC;
static nedTheme_t  *m_pTheme;
static void        *m_hMenu       = NULL;
static unsigned     m_editRows    = 0;
static bool_t       m_exit        = FALSE;
static int          m_exitCode    = TEST_EXIT_FAIL;
static winPos_t     m_oldRows     = 0;
static winPos_t     m_oldCols     = 0;
static sNtmCsm_t    m_FileOpen        = { NEDMENUINDEX_FILE,   NEDFILE_INDEX_FILE_OPEN };
static sNtmCsm_t    m_FileOpenRecent  = { NEDMENUINDEX_FILE,   NEDFILE_INDEX_FILE_OPEN_RECENT };
static sNtmCsm_t    m_OptionMapKeys   = { NEDMENUINDEX_OPTION, NEDOPTION_INDEX_MAP_KEYS };

/*-------------------------------------------------------------------------------------------------
  Change the theme of the menu.
-------------------------------------------------------------------------------------------------*/
void NtmRedrawScreen(bool_t fCls)
{
  winPos_t            rows;
  winPos_t            cols;
  winPos_t            col;
  void               *pOldWin;
  static const char   aStatus1[] = "[*][Sel]  ";
  static const char   aStatus2[] = "  Ned v1.00";

  pOldWin = NedWinGetPicked();
  NedLogPrintf("pOldWin %p\n", pOldWin);

  // in case we changed screen size, adjust possibley larger or smaller
  NedWinGetScreenSize(&rows, &cols);
  NedWinAdjustScreen(rows, cols);
  m_editRows = (unsigned)(rows - OTHER_ROWS);

  NedLogPrintf("NtmRedrawScreen(mEditRows %u, picked %p)\n", m_editRows, NedWinGetPicked());
  NedWinLogShowChain();

  // clear the backdrop
  NedWinPick(NedWinGetBackdrop());
  if(fCls)
    NedWinClearScreen(NedThemeGet(m_themeIndex)->text);

  NedMenuSetTheme(m_hMenu, m_themeIndex);
  NedMenuDraw(m_hMenu);

  // display status line (same color as menubar)
  NedWinGoto(rows-2, 0);
  NedWinSetAttr(NedThemeGet(m_themeIndex)->menuText);
  col = NedWinPrintf("file.c");
  NedWinPrintf("%*s%3u:%-3u%s", cols - (col + 7 + strlen(aStatus2)), aStatus1, rows, cols, aStatus2);
  NedWinClearEol();

  // display message line
  NedWinGoto(rows-1, 0);
  NedWinSetAttr(NedThemeGet(m_themeIndex)->message);
  NedWinPrintf("Press Alt-X to pass, ALt-Q to fail. Press Fn2 to redraw screen");
  NedWinClearEol();
  NedWinSetAttr(NedThemeGet(m_themeIndex)->text);

  NedWinPick(pOldWin);

  NedWinFlushAll();
  NedWinFlushCursor();
}

/*-------------------------------------------------------------------------------------------------
  Change the theme of the menu.
-------------------------------------------------------------------------------------------------*/
void NtmSetTheme(void)
{
  // go to next theme
  ++m_themeIndex;
  if(m_themeIndex > NEDTHEME_ZERO)
    m_themeIndex = NEDTHEME_CLASSIC;
  m_pTheme = NedThemeGet(m_themeIndex);
  NedLogPrintf("New theme %s\n", NedThemeGetName(m_themeIndex));

  // adjust window menu items depending on theme
  if(m_themeIndex == NEDTHEME_CLASSIC)
    NedMenuMapSetWindowItems(NumElements(maUtWindowMenuItems), maUtWindowMenuItems );
  else
    NedMenuMapSetWindowItems(NumElements(maUtWindowMenuItems3), maUtWindowMenuItems3 );

  // redraw in new color theme
  NtmRedrawScreen(TRUE);
}

/*-------------------------------------------------------------------------------------------------
  Map or 
-------------------------------------------------------------------------------------------------*/
void NtmCmdSettingsMapKeys(void)
{
  static bool_t fSet = FALSE;   // set or remove special key

  if(fSet == FALSE)
  {
    // should remove Fn2 from File/Open and put into Help/About
    NedKeyMapSetCommand(NedKeyName(NEDKEY_FN2), NedCmdGetName(NedCmdHelpAbout));
    fSet = TRUE;
  }
  else
  {
    // should remove Fn2 from Help/About and add to File/Open
    NedKeyMapSetCommand(NedKeyName(NEDKEY_FN2), NedCmdGetName(NedCmdFileOpen));
    fSet = FALSE;
  }

  NedMenuMapAdjustMenuKeys();
}

/*-------------------------------------------------------------------------------------------------
  Test of the pick window outside of the menu system
-------------------------------------------------------------------------------------------------*/
void NtmCmdPickWindow(void)
{
  static const char  *aszStrings[] = 
  {
    "Definition",
    "Hello",
    "World",
    "A longer string",
    "Testing one",
    "two, three"
  };
  nedKey_t      key;
  hPick_t       hPick;
  nedTheme_t   *pTheme = NedThemeGet(m_themeIndex);
  bool_t        fInPick;
  unsigned      pickedItem = NEDMENU_PICK_NONE;

  hPick = NedPickNew(10, 16, NumElements(aszStrings), aszStrings, 0);
  NedPickSetColors(hPick, pTheme->menuText, pTheme->menuDropText);
  NedPickDraw(hPick);
  do
  {
    key = NedKeyGetKey();
    fInPick = NedPickFeedKey(hPick, key, &pickedItem);
    if(fInPick)
      NedPickDraw(hPick);
    else
    {
      NedWinFlush();
    }
  } while(fInPick);
  NedPickFree(hPick);

  NedWinGoto(6, 2);
  NedWinSetAttr(pTheme->text);
  NedWinPrintf("Picked %u\n", pickedItem);
  NedWinClearEol();
  NedWinFlush();
}

/*-------------------------------------------------------------------------------------------------
  Does all the common submenu stuff. Assumes strings already initialized.
  Returns TRUE if still in submenu, FALSE if not.
-------------------------------------------------------------------------------------------------*/
bool_t NtmCommonSubMenu(sNtmCsm_t *pCsm, nedKey_t key, unsigned *pSubItem)
{
  bool_t        fInSubMenu;
  unsigned      left;

  pCsm->fEnteredViaMenu = TRUE;
  if(pCsm->hPick == NULL)
  {
    left = NedMenuSubMenuCol(m_hMenu, pCsm->nMenu);
    pCsm->hPick = NedPickNew(2, left, pCsm->numItems, pCsm->ppStrings, 0);
    NedAssert(pCsm->hPick);
    NedPickSetColors(pCsm->hPick, m_pTheme->menuText, m_pTheme->menuDropText);
    NedPickLogShow(pCsm->hPick);
  }

  // on screen rows may have changed, update them if needed
  NedPickAdjust(pCsm->hPick);

  // feed the key into the pick window state machine. Returns FALSE if out of state machine
  fInSubMenu = NedPickFeedKey(pCsm->hPick, key, pSubItem);

  // keep hList around as we'll need it in NtmFileOpen()
  if(fInSubMenu)
    NedPickDraw(pCsm->hPick);
  else
  {
    pCsm->pickedSubItem = *pSubItem;
    NedPickFree(pCsm->hPick);
    pCsm->hPick = NULL;
  }

  return fInSubMenu;
}

/*-------------------------------------------------------------------------------------------------
  If the command happened without the menu, get keys until either ENTER or ESC.
  Pick menu has been exited and freed by this point.
-------------------------------------------------------------------------------------------------*/
void NtmCommonSubMenuFeedKeys(pfnSubMenu_t pfnSubMenu, sNtmCsm_t *pCsm)
{
  nedKey_t    key;

  if(!pCsm->fEnteredViaMenu)
  {
    (pfnSubMenu)(NEDKEY_NONE, &pCsm->pickedSubItem);
    while(1)
    {
      key = NedKeyGetKey();
      if(!(pfnSubMenu)(key, &pCsm->pickedSubItem))
        break;
    }
  }

  if(pCsm->hPick)
    NedPickFree(pCsm->hPick);
  pCsm->fEnteredViaMenu = FALSE;
}

/*-------------------------------------------------------------------------------------------------
  Common print routine for all
-------------------------------------------------------------------------------------------------*/
void NtmCommonPrint(const char *szPrompt, sNtmCsm_t *pCsm)
{
  NedWinPickBackdrop();
  NedWinGoto(6, 2);
  NedWinSetAttr(m_pTheme->text);
  if(pCsm->pickedSubItem != NEDMENU_PICK_NONE)
  {
    NedWinPrintf("%s idx %u, ", szPrompt, pCsm->pickedSubItem);
    NedWinPrintf("%s", pCsm->ppStrings[pCsm->pickedSubItem]);
  }
  else
    NedWinPrintf("Picked none");
  NedWinClearEol();
  NedWinFlush();  
}

/*-------------------------------------------------------------------------------------------------
  Helps to open a file. Gets fed keys from menu system
  Returns TRUE if still in submenu, FALSE if not
-------------------------------------------------------------------------------------------------*/
bool_t NtmFileOpenSubMenu(nedKey_t key, unsigned *pSubItem)
{
  unsigned      i;

  // intializing picker
  if(m_FileOpen.hList == NULL)
  {
    m_FileOpen.hList      = NedFileListNew("*");
    m_FileOpen.numItems   = NedFileListLen(m_FileOpen.hList);
    m_FileOpen.ppStrings  = malloc(m_FileOpen.numItems * sizeof(char *));
    NedAssert(m_FileOpen.ppStrings);
    for(i=0; i<m_FileOpen.numItems; ++i)
      m_FileOpen.ppStrings[i] = NedFileListGetName(m_FileOpen.hList, i);
  }

  return NtmCommonSubMenu(&m_FileOpen, key, pSubItem);
}

/*-------------------------------------------------------------------------------------------------
  When keyboard is idle, check if screen has changed, or for changed files
-------------------------------------------------------------------------------------------------*/
void NtmFileOpen(void)
{
  // user may have chosen a key bound to the command (not through menu)
  NtmCommonSubMenuFeedKeys(NtmFileOpenSubMenu, &m_FileOpen);
  NtmCommonPrint("FileOpen", &m_FileOpen);

  if(m_FileOpen.hList)
  {
    NedFileListFree(m_FileOpen.hList);
    m_FileOpen.hList = NULL;
  }
}

/*-------------------------------------------------------------------------------------------------
  Shows recent files
  Returns TRUE if still in submenu, FALSE if not
-------------------------------------------------------------------------------------------------*/
bool_t NtmFileOpenRecentSubMenu(nedKey_t key, unsigned *pSubItem)
{
  const char   *aszRecentFiles[] = { "Ned.h", "../docs/NedManual.md", "../test/NedTestMenu.c" };
  unsigned      i;

  if(m_FileOpenRecent.ppStrings == NULL)
  {
    m_FileOpenRecent.numItems   = NumElements(aszRecentFiles);
    m_FileOpenRecent.ppStrings  = malloc(m_FileOpenRecent.numItems * sizeof(char *));
    for(i=0; i<m_FileOpenRecent.numItems; ++i)
      m_FileOpenRecent.ppStrings[i] = aszRecentFiles[i];
  }

  return NtmCommonSubMenu(&m_FileOpenRecent, key, pSubItem);
}

/*-------------------------------------------------------------------------------------------------
  When keyboard is idle, check if screen has changed, or for changed files
-------------------------------------------------------------------------------------------------*/
void NtmFileOpenRecent(void)
{
  // user chose a key bound to the command (not through menu)
  NtmCommonSubMenuFeedKeys(NtmFileOpenRecentSubMenu, &m_FileOpenRecent);
  NtmCommonPrint("FileOpenRecent", &m_FileOpenRecent);
}

/*-------------------------------------------------------------------------------------------------
  Handle the MapKeys Submenu pick window
-------------------------------------------------------------------------------------------------*/
bool_t NtmOptionMapKeysSubMenu(nedKey_t key, unsigned *pSubItem)
{
  const char   *aszStrings[] = { "ExitSaveAll", "ExitSaveAsk", "ExitSaveNone", "FileClose", "FileCloseAll" };
  unsigned      i;

  if(m_OptionMapKeys.ppStrings == NULL)
  {
    m_OptionMapKeys.numItems   = NumElements(aszStrings);
    m_OptionMapKeys.ppStrings  = malloc(m_OptionMapKeys.numItems * sizeof(char *));
    for(i=0; i<m_OptionMapKeys.numItems; ++i)
      m_OptionMapKeys.ppStrings[i] = aszStrings[i];
  }

  return NtmCommonSubMenu(&m_OptionMapKeys, key, pSubItem);
}

/*-------------------------------------------------------------------------------------------------
  Handle the MapKeys command
-------------------------------------------------------------------------------------------------*/
void NtmOptionMapKeys(void)
{
  // user chose a key bound to the command (not through menu)
  NtmCommonSubMenuFeedKeys(NtmOptionMapKeysSubMenu, &m_OptionMapKeys);
  NtmCommonPrint("OptionMapKeys", &m_OptionMapKeys);
}

/*-------------------------------------------------------------------------------------------------
  When keyboard is idle, check if screen size has changed
-------------------------------------------------------------------------------------------------*/
void NtmIdle(void)
{
  winPos_t          rows;
  winPos_t          cols;

  NedWinGetScreenSize(&rows, &cols);
  if((rows != m_oldRows) || (cols != m_oldCols))
  {
    NedMenuResize(m_hMenu);
    NtmRedrawScreen(TRUE);
    m_oldRows = rows;
    m_oldCols = cols;
  }
}

/*-------------------------------------------------------------------------------------------------
  Print the log banner
-------------------------------------------------------------------------------------------------*/
void NtmLogPrintBanner(void)
{
  static const char aszBanner[] =
  {
    "\n\n"
    "=======  -----  -----   ---  -----  =======\n"
    "_______    |    |__    /       |    _______\n"
    "           |    |      ----    |\n"
    "=======    |    |____  ___/    |    =======\n"
    "\n\n"
  };

  NedLogPrintf("%s",aszBanner);
}

/*
  Maps commands to menu test functions
*/
void NtmExec(pfnNedCmd_t pfnCmd)
{
  // Alt-X to pass
  if(pfnCmd == NedCmdExitSaveAll)
  {
    m_exitCode = TEST_EXIT_PASS;
    m_exit = TRUE;
  }
  // Alt-Q to fail
  else if(pfnCmd == NedCmdExitSaveAsk)
  {
    m_exitCode = TEST_EXIT_FAIL;
    m_exit = TRUE;
  }
  else if(pfnCmd == NedCmdMenuFile)
    NedMenuEnter(m_hMenu, NEDMENUINDEX_FILE);
  else if(pfnCmd == NedCmdMenuEdit)
    NedMenuEnter(m_hMenu, NEDMENUINDEX_EDIT);
  else if(pfnCmd == NedCmdMenuSearch)
    NedMenuEnter(m_hMenu, NEDMENUINDEX_SEARCH);
  else if(pfnCmd == NedCmdMenuGoto)
    NedMenuEnter(m_hMenu, NEDMENUINDEX_GOTO);
  else if(pfnCmd == NedCmdMenuOption)
    NedMenuEnter(m_hMenu, NEDMENUINDEX_OPTION);
  else if(pfnCmd == NedCmdMenuWindow)
    NedMenuEnter(m_hMenu, NEDMENUINDEX_WINDOW);
  else if(pfnCmd == NedCmdMenuHelp)
    NedMenuEnter(m_hMenu, NEDMENUINDEX_HELP);
  else if(pfnCmd == NedCmdSettingsTheme)
    NtmSetTheme();  
  else if(pfnCmd == NedCmdRedrawScreen)
    NtmRedrawScreen(TRUE);
  else if(pfnCmd == NedCmdDebug) // Alt-B
    NtmCmdPickWindow();
  else if(pfnCmd == NedCmdFileOpen)
    NtmFileOpen();
  else if(pfnCmd == NedCmdFileOpenRecent)
    NtmFileOpenRecent();
  else if(pfnCmd == NedCmdSettingsMapKeys)
    NtmCmdSettingsMapKeys();
}

/*-------------------------------------------------------------------------------------------------
  Unit tests the menu system
-------------------------------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
  nedKey_t          key;
  sMenuPicked_t     sPicked;
  pfnNedCmd_t       pfnCmd;
  sNedKeyMapSubMenu_t aSubMenus[] =
  {
    { NEDMENUINDEX_FILE,   NEDFILE_INDEX_FILE_OPEN,        NtmFileOpenSubMenu },
    { NEDMENUINDEX_FILE,   NEDFILE_INDEX_FILE_OPEN_RECENT, NtmFileOpenRecentSubMenu },
    { NEDMENUINDEX_OPTION, NEDOPTION_INDEX_MAP_KEYS,       NtmOptionMapKeysSubMenu }
  };
  unsigned          i;
  bool_t            fAutomatedOnly   = FALSE;
  bool_t            fMachineReadable = FALSE;
  const char       *pszResult;
  char              szRetCode[6];
  FILE             *fp;

  // process command-line
  for(i = 1; i < argc; ++i)
  {
    if(strcmp(argv[i], "-a") == 0)
    {
      fAutomatedOnly = TRUE;
      m_exitCode = TEST_EXIT_SKIP;
    }
    if(strcmp(argv[i], "-m") == 0)
      fMachineReadable = TRUE;
  }

  // print the test banner/start of test
  if(!fMachineReadable)
    NtmLogPrintBanner();

  m_exit = FALSE;

  // setup the backdrop and menu
  if(!fAutomatedOnly)
  {
    NedThemeInit();
    NedKeyMapFactory();

    m_themeIndex = NEDTHEME_CLASSIC;
    m_pTheme     = NedThemeGet(m_themeIndex);
    NedWinInit();
    NedWinPickBackdrop();

    m_hMenu = NedMenuMapInit(m_themeIndex);
    NedMenuMapSetSubMenus(m_hMenu, NumElements(aSubMenus), aSubMenus);
    NedAssert(m_hMenu);
    NedMenuMapSetWindowItems(NumElements(maUtWindowMenuItems), maUtWindowMenuItems);
    NtmRedrawScreen(TRUE);
    NedWinGoto(2,2);
    NedWinPrintf("Hello World");
    NedWinFlush();
    NedWinFlushCursor();

    NedKeySetIdle(NtmIdle);

    while(!m_exit)
    {
      key = NedKeyGetKey();
      NedLogPrintf("key %u\n", (unsigned)key);

      // if in menu subsystem, menu absorbs the key
      if(NedMenuInMenu(m_hMenu))
      {
        NedLogPrintf("inMenu, curWin %p, menu %p\n", NedWinGetPicked(), m_hMenu);
        if(NedMenuFeedKey(m_hMenu, key, &sPicked))
        {
          NedLogPrintf("picked=%u,menu=%u,item=%u\n", sPicked.fPicked, sPicked.pickedMenu,
                                                      sPicked.pickedItem);
          NedWinGoto(4,2);
          if(sPicked.fPicked)
          {
            NedWinPrintf("Picked (%s) menu=%u,item=%u, cmd = %s, inMenu %u",
              NedMenuItemName(m_hMenu, sPicked.pickedMenu, sPicked.pickedItem),
              sPicked.pickedMenu, sPicked.pickedItem, NedCmdGetName(pfnCmd), NedMenuInMenu(m_hMenu));
            pfnCmd = NedMenuMapGetCommand(sPicked.pickedMenu, sPicked.pickedItem);
            NtmExec(pfnCmd);
          }
          else
          {
            NedWinPrintf("nothing picked...");
          }
          NedWinClearEol();
          // display a text string
          NedWinGoto(2,2);
          NedWinPrintf("Hello World");
          NedWinFlush();
          NedWinFlushCursor();
        }
      }

      // in editor, do the command mapped to the key
      else
      {
        NedLogPrintf("NOT in menu, curWin %p, menu %p\n", NedWinGetPicked(), m_hMenu);

        // get and do the command
        pfnCmd = NedKeyMapGetCommand(key);
        NtmExec(pfnCmd);

        NedWinGoto(5,2);
        if(pfnCmd)
          NedWinPrintf("key %s = cmd %s", NedKeyName(key), NedCmdGetName(pfnCmd));
        else
          NedWinPrintf("key %s not mapped", NedKeyName(key));
        NedWinClearEol();

        // display a text string
        NedWinGoto(2,2);
        NedWinPrintf("Hello World");
        NedWinFlush();
        NedWinFlushCursor();
      }
    }

    // exit
    NedMenuFree(m_hMenu);
    AnsiSetAttr(NEDATTR_RESET);
    AnsiClearScreen();
  }

  if(m_exitCode == TEST_EXIT_PASS)
    pszResult = m_szMTestPassed;
  else if(m_exitCode == TEST_EXIT_FAIL)
    pszResult = m_szMTestFailed;
  else
    pszResult = m_szMTestSkipped;
  NedLogPrintf("----- TcMenuUser -----\n----- TcMenuUser(%s) -----\n", pszResult);

  // return the code via file to NedTestAll.c
  if(fMachineReadable)
  {
    remove(m_szRetCodeFile);
    fp = fopen(m_szRetCodeFile, "w");
    if(fp)
    {
      sprintf(szRetCode, "%d\n", m_exitCode);
      fwrite(szRetCode, 1, strlen(szRetCode), fp);
      fclose(fp);
    }
  }

  return m_exitCode;
}
