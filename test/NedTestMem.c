/*!************************************************************************************************
  @file NedTestMem.c

  @brief  Ned Memory Test Cases

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include "Ned.h"
#include "NedMem.h"
#include "NedUtilStr.h"
#include "NedTest2.h"

/*!------------------------------------------------------------------------------------------------
  Test MemAlloc

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcMemAlloc(void)
{
  char    *p1;
  char    *p2;

  NedTestBegin();
  NedMemTotalReset();

  // verify allocation
  p1 = NedMemAlloc(9);
  if(!NedMemIsMem(p1))
    NedTestFailed();
  if(!memisfilled(p1, NEDDBG_ALLOC, 9))
    NedTestFailed();
  memset(p1, 'G', 9);
  p2 = NedMemAlloc(1024);
  if(!NedMemIsMem(p2))
    NedTestFailed();
  if(!memisfilled(p2, NEDDBG_ALLOC, 1024))
    NedTestFailed();
  NedLogPrintf("1: alloc total = %zu\n", NedMemAllocTotal());
  if(NedMemAllocTotal() != 9 + 1024)
    NedTestFailed();

  // free, and verify was freed
  NedMemFree(p1);
  NedMemFree(p2);
  NedLogPrintf("2: alloc total = %zu\n", NedMemAllocTotal());
  if(NedMemAllocTotal() != 0)
    NedTestFailed();

  // test calloc()
  p1 = NedMemCalloc(99);
  if(!NedMemIsMem(p1))
    NedTestFailed();
  if(!memisfilled(p1, 0, 99))
    NedTestFailed();
  NedLogPrintf("3: alloc total = %zu\n", NedMemAllocTotal());
  NedMemFree(p1);
  if(NedMemAllocTotal() != 0)
    NedTestFailed();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test MemFree with buffer overrun

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcMemFree(void)
{
  char    *p1;

  NedTestBegin();
  NedMemTotalReset();

  // verify overrun
  p1 = NedMemAlloc(16);
  if(!NedMemIsMem(p1))
    NedTestFailed();
  memset(p1, 'D', 17);
  if(NedMemIsMem(p1))
    NedTestFailed();
  NedLogPrintf("4: alloc total = %zu\n", NedMemAllocTotal());
  NedMemFree(p1);
  NedMemTotalReset();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test MemRealloc

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcMemRealloc(void)
{
  char    *p1;
  char    *p2;
  char    *p3;

  NedTestBegin();
  NedMemTotalReset();

  p1 = NedMemAlloc(2);
  p2 = NedMemAlloc(4);
  p3 = NedMemAlloc(8);
  if(!p1 || !p2 || !p3)
    NedTestFailed();
  memset(p1, 'D', 2);
  memset(p2, 'A', 4);
  memset(p3, 'G', 8);
  if(NedMemAllocTotal() != 2 + 4 + 8)
    NedTestFailed();

  NedLogPrintf("1: p2 %p\n", p2);
  p2 = NedMemRealloc(p2, 3);
  NedLogPrintf("2: p2 %p\n", p2);
  if(!memisfilled(p2, 'A', 3))
    NedTestFailed();
  p2 = NedMemRealloc(p2, 1023);
  NedLogPrintf("3: p2 %p\n", p2);
  if(!p2)
    NedTestFailed();
  if(!memisfilled(p2, 'A', 3))
    NedTestFailed();
  memset(p2, 'A', 1023);
  if(!NedMemIsMem(p1))
    NedTestFailed();
  if(!NedMemIsMem(p2))
    NedTestFailed();
  if(!NedMemIsMem(p3))
    NedTestFailed();
  if(!memisfilled(p2, 'A', 1023))
    NedTestFailed();
  if(NedMemAllocTotal() != 2 + 1023 + 8)
    NedTestFailed();

  NedTestEnd();

  NedMemFree(p1);
  NedMemFree(p2);
  NedMemFree(p3);
}

/*!------------------------------------------------------------------------------------------------
  Test NedMemCount() functions

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcMemCount(void)
{
  char    *p1;
  char    *p2;

  NedTestBegin();
  NedMemTotalReset();

  p1 = NedMemAlloc(99);
  p2 = NedMemAlloc(16);
  if(NedMemAllocTotal() != 99 + 16)
    NedTestFailed();
  memset(p1, 'D', 99);
  memset(p2, 'G', 16);

  // newly allocated should be count of 1
  if(NedMemCountGet(p1) != 1)
    NedTestFailed();
  if(NedMemCountGet(p2) != 1)
    NedTestFailed();

  // increment counts
  NedMemCountInc(p1);
  if(NedMemCountGet(p1) != 2)
    NedTestFailed();
  NedMemCountInc(p2);
  NedMemCountInc(p2);
  if(NedMemCountGet(p2) != 3)
    NedTestFailed();
  if(NedMemCountGet(p1) != 2)
    NedTestFailed();
  if(!NedMemIsMem(p1))
    NedTestFailed();
  if(!NedMemIsMem(p2))
    NedTestFailed();

  // make sure user data was not harmed
  if(!memisfilled(p1, 'D', 99))
    NedTestFailed();
  if(!memisfilled(p2, 'G', 16))
    NedTestFailed();

  // decrement count
  NedMemCountFree(p2);
  if(NedMemCountGet(p2) != 2)
    NedTestFailed();

  // make sure user data was not harmed
  if(!memisfilled(p1, 'D', 99))
    NedTestFailed();
  if(!memisfilled(p2, 'G', 16))
    NedTestFailed();

  // free both pointer using both NedMemFree() and NedMemCountFree()
  NedMemFree(p1);
  if(NedMemAllocTotal() != 16)
    NedTestFailed();
  NedMemCountFree(p2);
  NedMemCountFree(p2);
  if(NedMemAllocTotal() != 0)
    NedTestFailed();
  
  NedTestEnd();
}

/*-------------------------------------------------------------------------------------------------
  Test each function
*///-----------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcMemAlloc",     TcMemAlloc   },
    { "TcMemFree",      TcMemFree    },
    { "TcMemRealloc",   TcMemRealloc },
    { "TcMemCount",     TcMemCount   }
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestMem", NumElements(aTestCases), aTestCases);
  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
