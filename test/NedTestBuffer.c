/*!************************************************************************************************
  @file NedTestBuffer.c

  @brief  Ned Buffer Test Cases

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  This module 

*///***********************************************************************************************
#include "Ned.h"
#include "NedBuffer.h"
#include "NedLog.h"
#include "NedMem.h"
#include "NedTest2.h"

#define LINELEN  32

/*!------------------------------------------------------------------------------------------------
  Check if memory is filled with a particular byte.

  @ingroup  ned_utils

  @param    pMem      memory to check
  @param    c         byte (character) to check
  @param    len       length of memory

  @returns  TRUE if memory is filled with the byte.
*///-----------------------------------------------------------------------------------------------
static bool_t memisfilled(void *pMem, unsigned char c, unsigned len)
{
  unsigned char *piMem = pMem;
  if(!pMem)
    return FALSE;

  while(len)
  {
    if(*piMem != c)
      return FALSE;
    ++piMem;
    --len;
  }

  return TRUE;
}

/*-------------------------------------------------------------------------------------------------
  Helper function, creates a file in full lines. The pattern is 'aaa', 'bbb' where each buffer has
  a new letter. Undefined if goes past 'z'. Last buffer may be short.

  Returns pBufHead.
-------------------------------------------------------------------------------------------------*/
static sNedBuffer_t * NedTestBufCreateFile(long fileSize, unsigned bufMaxLen)
{
  char          szLine[LINELEN+1];
  sNedBuffer_t *pBuf;
  sNedBuffer_t *pBufHead  = NULL;
  long          len       = 0;
  char          c         = 'a';

  // fills each buffer with a pattern. Last buffer may be short
  NedBufSetMaxLen(bufMaxLen);
  pBufHead = pBuf = NedBufNew(1);
  for(len=0; pBuf && len < fileSize; len += LINELEN)
  {
    memset(szLine, c, LINELEN-1);
    szLine[LINELEN-1] = '\n';
    szLine[LINELEN] = '\0';
    // NedLogPrintf("thisLen %u, buflen %u, szLine[0]=%c\n", thisLen, NedBufLen(pBuf), szLine[0]);

    if(NedBufInsert(pBuf, NedBufLen(pBuf), (void *)szLine, LINELEN) != LINELEN)
    {
      // NedLogPrintf("insert failed\n");
      break;
    }

    // need new buffer
    if(len && (NedBufLen(pBuf) == NedBufMaxLen()))
    {
      pBuf = NedBufAppend(pBuf);
      // NedLogPrintf("append buf %p\n", pBuf);
      if(!pBuf)
      {
        NedBufChainFree(pBufHead);
        pBufHead = NULL;
        break;
      }
      ++c;
    }
  }

  NedBufSetMaxLen(0);
  return pBufHead;
}

/*-------------------------------------------------------------------------------------------------
  Helper function to create a buffer. strlen(sz) must be less that size
  of max buffer. Returns pBufHead.
*///-----------------------------------------------------------------------------------------------
static sNedBuffer_t * NedTestBufSimpleAlloc(char *sz)
{
  unsigned len;

  sNedBuffer_t *pBuf;
  pBuf = NedBufNew(1);
  if(pBuf)
  {
    len = strlen(sz);
    memcpy(&pBuf->aData[0], sz, len);
    pBuf->len = len;
  }
  return pBuf;
}

/*-------------------------------------------------------------------------------------------------
  Helper function, creates a buffer list of 4 buffers, one, 2, three, four.
*///-----------------------------------------------------------------------------------------------
static bool_t NedTestBufAdd4(
  sNedBuffer_t **ppBufHead,
  sNedBuffer_t **ppBuf2,
  sNedBuffer_t **ppBuf3,
  sNedBuffer_t **ppBuf4)
{
  // add a head buffer "one"
  *ppBufHead = NedBufNew(1);
  if( (*ppBufHead) == NULL )
    return FALSE;
  memcpy(&((*ppBufHead)->aData[0]), "one", 3);
  (*ppBufHead)->len = 3;

  // add "2" right after head
  *ppBuf2 = NedBufAppend(*ppBufHead);
  if( (*ppBuf2) == NULL )
    return FALSE;
  memcpy(&((*ppBuf2)->aData[0]), "2", 1);
  (*ppBuf2)->len = 1;

  // add "four" after "2"
  *ppBuf4 = NedBufAppend(*ppBuf2);
  if( (*ppBuf4) == NULL )
    return FALSE;
  memcpy(&((*ppBuf4)->aData[0]), "four", 4);
  (*ppBuf4)->len = 4;

  // add "three" after "2", before "four"
  *ppBuf3 = NedBufAppend(*ppBuf2);
  if( (*ppBuf3) == NULL )
    return FALSE;
  memcpy(&(*ppBuf3)->aData[0], "three", 5);
  (*ppBuf3)->len = 5;

  return TRUE;
}

/*-------------------------------------------------------------------------------------------------
  Helper function verifies links when creating 4 buffers
*///-----------------------------------------------------------------------------------------------
static bool_t NedTestBufVerifyLinks4(
  sNedBuffer_t *pBufHead,
  sNedBuffer_t *pBuf2,
  sNedBuffer_t *pBuf3,
  sNedBuffer_t *pBuf4)
{
  if( (pBufHead->pPrev != NULL    ) || (pBufHead->pNext != pBuf2) ||
      (pBuf2->pPrev    != pBufHead) || (pBuf2->pNext    != pBuf3) ||
      (pBuf3->pPrev    != pBuf2   ) || (pBuf3->pNext    != pBuf4) ||
      (pBuf4->pPrev    != pBuf3   ) || (pBuf4->pNext    != NULL ) )
  {
    return FALSE;
  }
  return TRUE;
}

/*--------------------------------------------------------------------------------------------------
  Helper function, creates a line of the given characters, 64 bytes.
  Newlines only for lines.
  Returns length of line.
*///-----------------------------------------------------------------------------------------------
static unsigned NedTestBufWriteLine(FILE *fp, char c)
{
  unsigned i;
  for(i = 0; i < LINELEN - 1; ++i)
    fwrite(&c, 1, 1, fp);
  fwrite("\n", 1, 1, fp);   // 63 + '\n' = 64 bytes
  return LINELEN;
}

/*-------------------------------------------------------------------------------------------------
  Helper function, creates a 4K portion of a file.
  Returns length of file created
*///-----------------------------------------------------------------------------------------------
unsigned NedTestBufWrite4K(FILE *fp, char c)
{
  unsigned  i;
  unsigned  len = 0;
  for(i = 0; i < NedBufMaxLen() / LINELEN; ++i)
    len += NedTestBufWriteLine(fp, c);
  return len;
}

/*-------------------------------------------------------------------------------------------------
  Helper function, Creates a file of 16K + 1 line.
  Returns length of file created
*///-----------------------------------------------------------------------------------------------
static unsigned NedTestBufWriteFile(char *szFilename)
{
  FILE       *fp;
  unsigned    len = 0;

  fp = fopen(szFilename,"wb");
  if(fp)
  {
    len += NedTestBufWrite4K(fp, 'a');
    len += NedTestBufWrite4K(fp, 'b');
    len += NedTestBufWrite4K(fp, 'c');
    len += NedTestBufWrite4K(fp, 'd');
    len += NedTestBufWriteLine(fp, 'e');
    fclose(fp);
  }
  return len;
}

/*-------------------------------------------------------------------------------------------------
  Helper function, tests a file that should have buffers filled with aaa, bbb, ccc, ddd, ee.
  The last buffer may be short. Verifies the file contents.

  Returns TRUE if file is correct, FALSE if not.
*///-----------------------------------------------------------------------------------------------
static bool_t NedTestBufCheckFile(const sNedBuffer_t *pBufHead, long fileSize)
{
  sNedBuffer_t  *pBuf      = NULL;
  unsigned       numLinks;

  numLinks = (fileSize / NEDBUF_SIZE) + 1;

  // should be 5 buffers (4 full, plus spare)
  if(NedBufChainSize(pBufHead, NULL) != numLinks)
  {
    NedLogPrintf("ERR: too few links\n");
    return FALSE;
  }

  // file should start with an 'a'
  if(pBufHead->aData[0] != 'a')
  {
    NedLogPrintf("ERR: doesn't start with 'a'\n");
    return FALSE;
  }

  // file should end with a "e\n"
  pBuf = NedBufTail(pBufHead);
  if(NedBufLen(pBuf) < 2 || (memcmp(&pBuf->aData[pBuf->len-2], "e\n", 2) != 0))
  {
    NedLogPrintf("ERR: doesn't end with e\n");
    return FALSE;
  }

  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Test Case for NedBufNew() and NedBufFree()

  @ingroup    ned_testcase
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcBufNewFree(void)
{
  sNedBuffer_t *pBuf = NULL;

  NedTestBegin();

  pBuf = NedBufNew(1);  // allocates a single buffer
  if(!pBuf)
    NedTestFailed();

  if(pBuf->pNext || pBuf->pPrev || pBuf->len)
    NedTestFailed();

  if(!NedBufIsBuf(pBuf))
    NedTestFailed();

#if NEDDBG_LEVEL
  if(!memisfilled(&pBuf->aData[0], NEDDBG_ALLOC, NedBufMaxLen()))
    NedTestFailed();
#endif

  NedTestEnd();

  if(pBuf)
    NedBufFree(pBuf);
}

/*!------------------------------------------------------------------------------------------------
  Test Case

  - NedBufAppend()
  - NedBufFree()
*///-----------------------------------------------------------------------------------------------
void TcBufAppendFree(void)
{
  sNedBuffer_t *pBufHead = NULL;
  sNedBuffer_t *pBuf2 = NULL;
  sNedBuffer_t *pBuf3 = NULL;
  sNedBuffer_t *pBuf4 = NULL;
  long          len = 0;

  NedTestBegin();

  // creates a list of 4 buffers with contents "one", "2", "three", "four"
  if( !NedTestBufAdd4(&pBufHead, &pBuf2, &pBuf3, &pBuf4) )
    NedTestFailed();

  // verify 4 buffers
  if( (NedBufChainSize(pBufHead, &len) != 4) || (len != 13) )
    NedTestFailed();

  // verify bi-directional links between "one", "2", "three", "four"
  if(!NedTestBufVerifyLinks4(pBufHead, pBuf2, pBuf3, pBuf4))
    NedTestFailed();

  // free buffer with "2", verify it's gone
  if( NedBufFree(pBuf2) != pBuf3 )
    NedTestFailed();

  if( (NedBufChainSize(pBufHead, &len) != 3) || (len != 12) )
    NedTestFailed();

  // verify bidirectional links between "one", "three", "four"
  if(pBufHead->pPrev != NULL || pBufHead->pNext != pBuf3 || pBufHead->len != 3)
    NedTestFailed();

  if(pBuf3->pPrev != pBufHead || pBuf3->pNext != pBuf4 || pBuf3->len != 5)
    NedTestFailed();

  if(pBuf4->pPrev != pBuf3 || pBuf4->pNext != NULL || pBuf4->len != 4)
    NedTestFailed();

  // free buffer "four", verify it's gone
  if( NedBufFree(pBuf4) != NULL )
    NedTestFailed();

  if( NedBufChainSize(pBufHead, NULL) != 2 )
    NedTestFailed();

  // verify bidirectional links between "one", "three"
  if(pBufHead->pPrev != NULL || pBufHead->pNext != pBuf3 || pBufHead->len != 3)
    NedTestFailed();

  if(pBuf3->pPrev != pBufHead || pBuf3->pNext != NULL || pBuf3->len != 5)
    NedTestFailed();

  // free head, verify it's gone
  if( NedBufFree(pBufHead) != pBuf3 )
    NedTestFailed();

  if( NedBufChainSize(pBuf3, NULL) != 1 )
    NedTestFailed();

  // verify "three" is alone
  if(pBuf3->pPrev != NULL || pBuf3->pNext != NULL || pBuf3->len != 5)
    NedTestFailed();

  // remove "three", all gone
  if( NedBufFree(pBuf3) != NULL )
    NedTestFailed();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test Case for BufLen

    unsigned        NedBufLen         (const sNedBuffer_t *pBuf);

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcBufLen(void)
{
  sNedBuffer_t *pBufHead = NULL;
  sNedBuffer_t *pBuf2 = NULL;
  sNedBuffer_t *pBuf3 = NULL;
  sNedBuffer_t *pBuf4 = NULL;

  NedTestBegin();

  if( !NedTestBufAdd4(&pBufHead, &pBuf2, &pBuf3, &pBuf4) )
    NedTestFailed();

  if(NedBufLen(pBufHead) != 3) // "one"
    NedTestFailed();

  if(NedBufLen(pBuf2) != 1)    // "2"
    NedTestFailed();

  if(NedBufLen(pBuf3) != 5)    // "three"
    NedTestFailed();

  if(NedBufLen(pBuf4) != 4)    // "four"
    NedTestFailed();

  NedTestEnd();

  if(pBufHead)
    NedBufChainFree(pBufHead);
}


/*!------------------------------------------------------------------------------------------------
  Test case for

  - TcBufChainFree()

  @ingroup  ned_testcase
  @return   none
*///-----------------------------------------------------------------------------------------------
void TcBufChainFree(void)
{
  sNedBuffer_t *pBufHead = NULL;
  sNedBuffer_t *pBuf2 = NULL;
  sNedBuffer_t *pBuf3 = NULL;
  sNedBuffer_t *pBuf4 = NULL;

  NedTestBegin();

  // creates a list of 4 buffers with contents "one", "2", "three", "four"
  if( !NedTestBufAdd4(&pBufHead, &pBuf2, &pBuf3, &pBuf4) )
    NedTestFailed();

  // verify links
  if(!NedTestBufVerifyLinks4(pBufHead, pBuf2, pBuf3, pBuf4))
    NedTestFailed();

  // verify links have been severed
  NedBufChainFree(pBufHead);
  if( NedBufIsBuf(pBufHead) || NedBufIsBuf(pBuf2) || NedBufIsBuf(pBuf3) || NedBufIsBuf(pBuf4) )
    NedTestFailed();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test case for

  - NedBufChainClear()

  @ingroup  ned_testcase
  @return   none
*///-----------------------------------------------------------------------------------------------
void TcBufChainClear(void)
{
  sNedBuffer_t *pBufHead = NULL;
  sNedBuffer_t *pBuf2 = NULL;
  sNedBuffer_t *pBuf3 = NULL;
  sNedBuffer_t *pBuf4 = NULL;

  NedTestBegin();

  // creates a list of 4 buffers with contents "one", "2", "three", "four"
  if( !NedTestBufAdd4(&pBufHead, &pBuf2, &pBuf3, &pBuf4) )
    NedTestFailed();

  if(!pBufHead || pBufHead->pPrev != NULL || pBufHead->pNext != pBuf2 || pBufHead->len != 3)
    NedTestFailed();

  // clear should remove all but head. Head should be empty.
  NedBufChainClear(pBufHead);
  if(pBufHead->pPrev != NULL || pBufHead->pNext != NULL || pBufHead->len != 0)
    NedTestFailed();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test Case for

  - NedBufSplit()

  @ingroup  ned_testcase
  @returns  none
*///-----------------------------------------------------------------------------------------------
void TcBufSplit(void)
{
  sNedBuffer_t *pBufHead = NULL;
  sNedBuffer_t *pBuf2 = NULL;

  NedTestBegin();

  pBufHead = NedTestBufSimpleAlloc("Hi Bye");
  if(!pBufHead || pBufHead->len != 6)
    NedTestFailed();

  if( !NedBufSplit(pBufHead, 3) )
    NedTestFailed();

  pBuf2 = pBufHead->pNext;
  if(!pBuf2 || pBuf2->len != 3 || pBuf2->pNext || memcmp(&pBuf2->aData[0], "Bye", 3) != 0)
    NedTestFailed();

  if(pBufHead->len != 3 || pBufHead->pNext != pBuf2 || pBuf2->pPrev != pBufHead)
    NedTestFailed();

  NedTestEnd();

  // done, free the entire buffer chain
  if(pBufHead)
    NedBufChainFree(pBufHead);
}

/*-------------------------------------------------------------------------------------------------
  Test case for NedBufGrow().

  - NedBufGrow()

  @ingroup  ned_testcase
  @returns  none
*///-----------------------------------------------------------------------------------------------
void TcBufGrow(void)
{
  sNedBuffer_t  *pOrg   = NULL;
  sNedBuffer_t  *pNew   = NULL;
  sNedBuffer_t  *pNew2  = NULL;
  bool_t         fWorked;

  NedTestBegin();

  /*
    Example 1 (all in same buffer):
    step   org       new       comment
    1      abc.....            NedBufGrow(pBuf,0,3)
    2      abc---..            Space added (no new buffer(s) needed)
    3      ab---c..            Moved split data
  */
  pOrg    = NedBufNew(1);
  fWorked = NedBufGrow(pOrg, 0, 3);
  if(!fWorked || NedBufLen(pOrg) != 3 || NedBufNext(pOrg))
  {
    NedTestFailed();
  }

  memcpy(NedBufData(pOrg), "abc", 3);
  fWorked = NedBufGrow(pOrg, 2, 3);
  if(!fWorked || NedBufLen(pOrg) != 6 || pOrg->aData[5] != 'c' || NedBufNext(pOrg))
  {
    NedTestFailed();
  }
  NedBufFree(pOrg);

  /*
    Example 2, all of the data moves to end of new buffer:
    step   org       new       comment
    1      abcdef..            NedBufGrow(pBuf,2,9)
    2      abcdef--  -------.  Space added (new buffer len=7)
    3      ab------  ---cdef.  Copied part2 of split data after offset (cdef)
  */
  pOrg = NedBufNew(2);
  fWorked = NedBufGrow(pOrg, 0, 6);
  if(!fWorked || NedBufLen(pOrg) != 6 || NedBufNext(pOrg))
  {
    NedTestFailed();
  }

  memcpy(NedBufData(pOrg), "abcdef", 6);
  fWorked = NedBufGrow(pOrg, 2, (NEDBUF_SIZE*2) - 7);
  pNew = NedBufNext(pOrg);
  if(!fWorked || NedBufLen(pOrg) != NEDBUF_SIZE || !pNew || NedBufLen(pNew) != (NEDBUF_SIZE-1) ||
    strncmp( ((char *)NedBufData(pNew))+(NEDBUF_SIZE-5), "cdef", 4 ) != 0 || NedBufNext(pNew))
  {
    NedTestFailed();
  }
  NedBufFree(pOrg);
  NedBufFree(pNew);

  /*
    Example 3, some of the data stays in original buffer:
    step   org       new       comment
    1      abcdef..            NedBufGrow(pBuf,2,5)
    2      abcdef--  ---.....  Space added (new buffer len=3)
    3      abcdef--  ---.....  Copied splitPart2 to new buffer
    4      ab-----c  def.....  Copied splitPart1 to end of org buffer
  */
  pOrg = NedBufNew(3);
  fWorked = NedBufGrow(pOrg, 0, 6);
  if(!fWorked || NedBufLen(pOrg) != 6 || NedBufNext(pOrg))
  {
    NedTestFailed();
  }

  memcpy(NedBufData(pOrg), "abcdef", 6);
  fWorked = NedBufGrow(pOrg, 2, NEDBUF_SIZE-3);
  pNew = NedBufNext(pOrg);
  if(!fWorked || NedBufLen(pOrg) != NEDBUF_SIZE || !pNew || NedBufLen(pNew) != 3 ||
    memcmp(NedBufData(pNew), "def", 3) != 0 || NedBufNext(pNew) ||
    ((char *)NedBufData(pOrg))[NEDBUF_SIZE-1] != 'c')
  {
    NedTestFailed();
  }
  NedBufFree(pOrg);
  NedBufFree(pNew);

  /*
    Example 4, variant on 3
    step   org       new1      new2      comment
    1      abcdef..                      NedBufGrow(pBuf,2,13)
    2      abcdef--  --------  ---.....  Space added (new buffer len=2)
    3      abcdef--  --------  def.....  Copied splitPart2 to new2 buffer
    4      ab------  -------c  def.....  Copied splitPart1 to end of new1
  */
  pOrg = NedBufNew(4);
  fWorked = NedBufGrow(pOrg, 0, 6);
  if(!fWorked || NedBufLen(pOrg) != 6 || NedBufNext(pOrg))
  {
    NedTestFailed();
  }

  memcpy(NedBufData(pOrg), "abcdef", 6);
  fWorked = NedBufGrow(pOrg, 2, (NEDBUF_SIZE*2)-3);
  pNew    = NedBufNext(pOrg);
  pNew2   = NedBufNext(pNew);
  if(!fWorked || !pNew || !pNew2 ||
    NedBufLen(pOrg) != NEDBUF_SIZE ||
    NedBufLen(pNew) != NEDBUF_SIZE ||
    NedBufLen(pNew2) != 3 ||
    memcmp(NedBufData(pNew2), "def", 3) != 0  ||
    ((char *)NedBufData(pNew))[NEDBUF_SIZE-1] != 'c')
  {
    NedTestFailed();
  }

  NedTestEnd();

  NedBufFree(pOrg);
  NedBufFree(pNew);
  NedBufFree(pNew2);
}

/*!------------------------------------------------------------------------------------------------
  Test Case for

  - NedBufShrink()

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcBufShrink(void)
{
  sNedBuffer_t *pOrg   = NULL;
  sNedBuffer_t *pNew   = NULL;
  sNedBuffer_t *pNew2  = NULL;
  bool_t        fWorked;

  NedTestBegin();

  /*
    Example 1 (all in same buffer):
    step   org       new       comment
    1      ab---c..            original
    2      abc.....            NedBufShrink(pBuf,2,3)
  */
  pOrg    = NedBufNew(5);
  fWorked = NedBufGrow(pOrg, 0, 6);
  if(!fWorked || NedBufLen(pOrg) != 6 || NedBufNext(pOrg))
    NedTestFailed();

  memcpy(NedBufData(pOrg), "ab...c", 6);
  fWorked = NedBufShrink(pOrg, 2, 3);
  if(!fWorked || NedBufLen(pOrg) != 3 || memcmp(NedBufData(pOrg),"abc",3) || NedBufNext(pOrg))
  {
    NedTestFailed();
  }
  NedBufFree(pOrg);

  /*
    Example 2, all of the data moves to end of new buffer:
    step   org       new       comment
    1      ab------  ---cdef.  original
    2      ab......  cdef....  NedBufShrink(pBuf,2,9)
  */
  pOrg    = NedBufNew(6);
  fWorked = NedBufGrow(pOrg, 0, NEDBUF_SIZE*2-1);
  pNew    = NedBufNext(pOrg);
  if(!fWorked || NedBufLen(pOrg) != NEDBUF_SIZE || NedBufLen(pNew) != NEDBUF_SIZE-1)
  {
    NedTestFailed();
  }
  memcpy(NedBufData(pOrg), "ab", 2);
  memcpy(((char *)NedBufData(pNew)) + NEDBUF_SIZE-5, "cdef", 4);
  fWorked = NedBufShrink(pOrg, 2, (NEDBUF_SIZE*2) - 7);
  pNew    = NedBufNext(pOrg);
  if( !fWorked || NedBufLen(pOrg) != 2 || NedBufLen(pNew) != 4 ||
      memcmp(NedBufData(pNew), "cdef", 4) != 0 )
  {
    NedTestFailed();
  }
  NedBufFree(pOrg);
  NedBufFree(pNew);

  /*
    Example 3, some of the data stays in original buffer:
    step   org       new       comment
    1      ab-----c  def.....  original
    2      abc       def.....  NedBufShrink(pBuf,2,NEDBUF_SIZE-3)
  */
  pOrg = NedBufNew(7);
  fWorked = NedBufGrow(pOrg, 0, NEDBUF_SIZE+3);
  pNew = NedBufNext(pOrg);
  if(!fWorked || NedBufLen(pOrg) != NEDBUF_SIZE || NedBufLen(pNew) != 3)
  {
    NedTestFailed();
  }
  memcpy(NedBufData(pOrg), "ab", 2);
  memcpy((char *)NedBufData(pOrg)+(NEDBUF_SIZE-1), "c", 1);
  memcpy(NedBufData(pNew), "def", 3);
  fWorked = NedBufShrink(pOrg, 2, NEDBUF_SIZE-3);
  pNew = NedBufNext(pOrg);
  if( !fWorked || NedBufLen(pOrg) != 3 || NedBufLen(pNew) != 3 ||
    memcmp(NedBufData(pOrg), "abc", 3) != 0 )
  {
    NedTestFailed();
  }
  NedBufFree(pOrg);
  NedBufFree(pNew);

  /*
    Example 4, variant on 3
    step   org       new1      new2      comment
    1      ab------  -------c  def.....
    2      ab......  c.......  def.....  NedBufShrink(pBuf,2,13)
  */
  pOrg    = NedBufNew(8);
  fWorked = NedBufGrow(pOrg, 0, (NEDBUF_SIZE*2)+3);
  pNew    = NedBufNext(pOrg);
  pNew2   = NedBufNext(pNew);
  if(!fWorked || NedBufLen(pOrg) != NEDBUF_SIZE ||
             NedBufLen(pNew) != NEDBUF_SIZE || NedBufLen(pNew2) != 3)
  {
    NedTestFailed();
  }
  memcpy(NedBufData(pOrg), "ab", 2);
  memcpy((char *)NedBufData(pNew)+(NEDBUF_SIZE-1), "c", 1);
  memcpy(NedBufData(pNew2), "def", 3);
  fWorked = NedBufShrink(pOrg, 2, (NEDBUF_SIZE*2)-3);
  pNew    = NedBufNext(pOrg);
  pNew2   = NedBufNext(pNew);
  if(!fWorked || NedBufLen(pOrg) != 2 || NedBufLen(pNew) != 1 || NedBufLen(pNew2) != 3 ||
    memcmp(NedBufData(pOrg), "ab", 2) != 0 || *(char *)NedBufData(pNew) != 'c' ||
    memcmp(NedBufData(pNew2), "def", 3) != 0)
  {
    NedTestFailed();
  }

  NedTestEnd();

  NedBufFree(pOrg);
  NedBufFree(pNew);
  NedBufFree(pNew2);
}

/*!------------------------------------------------------------------------------------------------
  Test Case for NedBufInsert().

      long NedBufInsert(sNedBuffer_t *pBuf, unsigned offset, const uint8_t *pData, long len);

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcBufInsert(void)
{
  sNedBuffer_t *pBufHead = NULL;
  sNedBuffer_t *pBuf;
  uint8_t      *pData = NULL;

  NedTestBegin();

  // allocate a buffer for testing
  NedBufSetMaxLen(16);
  pBufHead = pBuf = NedBufNew(1);
  if(!pBufHead)
    NedTestFailed();

  // insert some stuff in the buffer
  if(NedBufInsert(pBuf, 0, (void *)"abc", 3) != 3)
    NedTestFailed();

  if(NedBufInsert(pBuf, 0, (void *)"wxyz", 4) != 4)
    NedTestFailed();

  if(NedTestVerbose())
    NedBufLogDumpChain(pBuf, 16);
  if((memcmp(&pBuf->aData[0], "wxyzabc", 7) != 0) || NedBufLen(pBuf) != 7)
    NedTestFailed();

  // create some data
  pData = malloc(NedBufMaxLen());
  if(!pData)
    NedTestFailed();

  memset(pData, '@', NedBufMaxLen());
  if(NedBufInsert(pBuf, 4, pData, NedBufMaxLen() - 4) != (NedBufMaxLen() - 4))
    NedTestFailed();

  if(NedTestVerbose())
    NedBufLogDumpChain(pBuf, 16);

  // check the pBuf is OK
  if((memcmp(&pBuf->aData[0], "wxyz@", 5) != 0) || (NedBufLen(pBuf) != NedBufMaxLen()) || (!pBuf->pNext))
    NedTestFailed();

  // check that pBuf->pNext is OK
  if((NedBufLen(pBuf->pNext) != 3) || (memcmp(&pBuf->pNext->aData[0], "abc", 3) != 0))
    NedTestFailed();

  // insert a whole buffer
  free(pData);
  pData = malloc(NedBufMaxLen() * 2);
  if(!pData)
    NedTestFailed();

  memset(pData, '^', NedBufMaxLen() * 2);
  if(NedBufInsert(pBuf, 5, pData, NedBufMaxLen() * 2) != (NedBufMaxLen() * 2))
    NedTestFailed();

  if(!pBuf->pNext || (NedBufLen(pBuf->pNext) != NedBufMaxLen()) || 
                 (memcmp(&pBuf->pNext->aData[0], pData, NedBufMaxLen()) != 0))
  {
    NedTestFailed();
  }

  // insert a at the end
  uint8_t c = '?';
  pBuf = NedBufTail(pBuf);
  if(NedBufInsert(pBuf, NedBufLen(pBuf), &c, 1) != 1 || (pBuf->aData[pBuf->len - 1] != c))
    NedTestFailed();

  if(NedTestVerbose())
    NedBufLogDumpChain(pBuf, 16);

  // reset everything
  if(pData)
    free(pData);
  if(pBufHead)
    NedBufChainFree(pBufHead);
  NedBufSetMaxLen(0);

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test Case for NedBufChainCopy()

    long NedBufChainCopy(sNedBuffer_t *pBufTo, unsigned toOffset,
                         const sNedBuffer_t *pBufFrom, unsigned fromOffset, long len);

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcBufChainCopy(void)
{
  sNedBuffer_t *pBufHead  = NULL;
  sNedBuffer_t *pClip     = NULL;
  sNedBuffer_t *pBuf;
  uint8_t      *pData     = NULL;
  unsigned      i;

  NedTestBegin();

  NedBufSetMaxLen(32);

  // create a buffer with some characters
  pBufHead = NedBufNew(1);
  if(!pBufHead || NedBufInsert(pBufHead, 0, (void *)"abcdef", 6) != 6)
    NedTestFailed();

  NedLogPrintf("Buf b4:\n");
  NedBufLogDumpChain(pBufHead, 16);

  // create a small clipboard
  pClip    = NedBufNew(2);
  if(!pClip || NedBufInsert(pClip, 0, (void *)"xyz", 3) != 3)
    NedTestFailed();

  // copy small clip into the buffer and verify
  if(NedBufChainCopy(pBufHead, 2, pClip, 0, NedBufLen(pClip)) != NedBufLen(pClip))
    NedTestFailed();

  NedLogPrintf("Clip:\n");
  NedBufLogDumpChain(pClip, 16);
  NedLogPrintf("Buf:\n");
  NedBufLogDumpChain(pBufHead, 16);
  if((pBufHead->len != 9) || (memcmp(&pBufHead->aData[0], "abxyzcdef", 9) != 0))
    NedTestFailed();

  // create a larger clip (use 3 x 32-byte buffers to keep dataset managable in log)
  // creates 3 buffers filled with 'A'-'C'
  pData = malloc(NedBufMaxLen());
  if(!pData)
    NedTestFailed();

  pBuf = pClip;
  NedBufChainClear(pClip);
  for(i=0; i<3; ++i)
  {
    memset(pData, 'A'+i, NedBufMaxLen());
    if(NedBufInsert(pBuf, 0, pData, NedBufMaxLen()) != NedBufMaxLen())
      NedTestFailed();

    pBuf = NedBufAppend(pBuf);
    if(!pBuf)
      NedTestFailed();
  }

  NedLogPrintf("Large pClip\n");
  NedBufLogDumpChain(pClip, 16);

  // verify it worked
  if(NedBufChainCopy(pBufHead, 4, pClip, 0, NedBufMaxLen() * 3) != (NedBufMaxLen() * 3))
    NedTestFailed();

  NedLogPrintf("Large pBufHead\n");
  NedBufLogDumpChain(pBufHead, 16);

  // pBufHead should have split
  if((NedBufLen(pBufHead) != 4) || (memcmp(NedBufData(pBufHead), "abxy", 4) != 0))
    NedTestFailed();

  // other data should be appended as-is (buffers filled with A-C)
  pBuf = NedBufNext(pBufHead);
  for(i=0; i<3; ++i)
  {
    if(!pBuf || !pData)
      NedTestFailed();

    if(!memisfilled(NedBufData(pBuf), 'A'+i, NedBufMaxLen()))
      NedTestFailed();

    pBuf = NedBufNext(pBuf);
  }

  // verify data
  pBuf = NedBufTail(pBufHead);
  if((NedBufLen(pBuf) != 5) || (memcmp(NedBufData(pBuf), "zcdef", 5) != 0))
    NedTestFailed();

  NedTestEnd();

  // clean up
  NedBufSetMaxLen(0);
  if(pBufHead)
    NedBufChainFree(pBufHead);
  if(pClip)
    NedBufChainFree(pClip);
  if(pData)
    free(pData);
}

/*!------------------------------------------------------------------------------------------------
  Test case for

  - NedBufLoadFile()

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcBufLoadFile(void)
{
  sNedBuffer_t *pBufHead  = NULL;
  nedErr_t      err       = NEDERR_FAILED;
  long          fileSize;
  long          testFileSize;

  NedTestBegin();

  // test file
  // 4K each of 'a', 'b', 'c', 'd', followed by 64 bytes of 'e'
  testFileSize = NedTestBufWriteFile("tcdata_load.txt");

  // read the file
  pBufHead = NedBufNew(10);
  if(pBufHead)
    err = NedBufFileLoad(pBufHead, "tcdata_load.txt", &fileSize);

  if(!pBufHead || (err != NEDERR_NONE) || fileSize != testFileSize)
  {
    NedTestFailed();
  }

  // verify file is correct
  if(!NedTestBufCheckFile(pBufHead, fileSize))
  {
    NedBufChainFree(pBufHead);
    NedTestFailed();
  }

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test case for NedBufSaveFile()

  @ingroup  ned_testcase
  @returns  none
*///-----------------------------------------------------------------------------------------------
void TcBufSaveFile(void)
{

  sNedBuffer_t *pBufHead    = NULL;
  sNedBuffer_t *pBufHead2   = NULL;
  long          fileSize    = 4 * NedBufMaxLen() + LINELEN;
  long          fileSize2   = 0;
  nedErr_t      err         = NEDERR_FAILED;

  NedTestBegin();

  // create and check the file buffer
  pBufHead = NedTestBufCreateFile(fileSize, NedBufMaxLen());

  if(NedTestVerbose())
    NedBufLogDumpChain(pBufHead, 16);
  if(!pBufHead || !NedTestBufCheckFile(pBufHead, fileSize))
    NedTestFailed();

  // save it to a file
  if(NedBufFileSave("tcdata_save.txt", pBufHead) != NEDERR_NONE)
    NedTestFailed();

  // read it back in and verify
  pBufHead2 = NedBufNew(10);
  if(pBufHead2)
    err = NedBufFileLoad(pBufHead2, "tcdata_save.txt", &fileSize2);

  // NedBufLogDumpChain(pBufHead, 16);
  NedLogPrintf("pBufHead2 %p, fileSize2 %ld, fileSize = %ld, err %u\n", pBufHead2, fileSize2, fileSize, err);  
  if(!pBufHead2 || (err != NEDERR_NONE) || fileSize2 != fileSize)
    NedTestFailed();

  if(!NedTestBufCheckFile(pBufHead2, fileSize2))
  {
    NedBufChainFree(pBufHead2);
    NedTestFailed();
  }

  NedTestEnd();
}

/*-------------------------------------------------------------------------------------------------
  Test each function
*///-----------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcBufLen",         TcBufLen  },
    { "TcBufNewFree",     TcBufNewFree },
    { "TcBufAppendFree",  TcBufAppendFree },
    { "TcBufChainFree",   TcBufChainFree  },
    { "TcBufChainClear",  TcBufChainClear  },
    { "TcBufSplit",       TcBufSplit  },
    { "TcBufGrow",        TcBufGrow  },
    { "TcBufShrink",      TcBufShrink  },
    { "TcBufInsert",      TcBufInsert  },
    { "TcBufChainCopy",   TcBufChainCopy },
    { "TcBufLoadFile",    TcBufLoadFile  },
    { "TcBufSaveFile",    TcBufSaveFile  }
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestBuffer", NumElements(aTestCases), aTestCases);
  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
