/*!************************************************************************************************
  @file NedTestCmd.c

  @brief  Ned Command Test Cases

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include "Ned.h"
#include "NedLog.h"
#include "NedTest2.h"
#include "NedCmd.h"


pfnNedCmd_t aAllCmds[] = 
{
  NedCmdBookmarkClearAll,
  NedCmdBookmarkNext,
  NedCmdBookmarkPrev,
  NedCmdBookmarkToggle,

  NedCmdCursorUp,
  NedCmdCursorDown,
  NedCmdCursorLeft,
  NedCmdCursorLeftSelect,
  NedCmdCursorRight,
  NedCmdCursorRightSelect,
  NedCmdCursorTop,
  NedCmdCursorBottom,
  NedCmdCursorHome,
  NedCmdCursorEnd,
  NedCmdCursorPgUp,
  NedCmdCursorPgDn,
  NedCmdCursorWordLeft,
  NedCmdCursorWordRight,

  NedCmdDebug,
  NedCmdDoNothing,

  NedCmdEditBackspace,
  NedCmdEditCopy,
  NedCmdEditCut,
  NedCmdEditCutEol,
  NedCmdEditCutLine,
  NedCmdEditCutWord,
  NedCmdEditDelete,
  NedCmdEditEnter,
  NedCmdEditIndent,
  NedCmdEditInsertSelf,
  NedCmdEditPaste,
  NedCmdEditPbCopy,
  NedCmdEditPbPaste,
  NedCmdEditRedo,
  NedCmdEditSortLines,
  NedCmdEditToggleCase,
  NedCmdEditToggleComment,
  NedCmdEditUndent,
  NedCmdEditUndo,
  NedCmdEditWrapText,

  NedCmdExitSaveAll,
  NedCmdExitSaveAsk,
  NedCmdExitSaveNone,

  NedCmdFileClose,
  NedCmdFileCloseAll,
  NedCmdFileNew,
  NedCmdFileNextOpen,
  NedCmdFilePrevOpen,
  NedCmdFileOpen,
  NedCmdFileOpenRecent,
  NedCmdFileOpenWildcard,
  NedCmdFileRevert,
  NedCmdFileSave,
  NedCmdFileSaveAs,
  NedCmdFileSaveAll,
  NedCmdFileToggleRdOnly,

  NedCmdHelp,
  NedCmdHelpAbout,
  NedCmdHelpDateTime,

  NedCmdMacroRecord,
  NedCmdMacroPlay,

  NedCmdMenuFile,
  NedCmdMenuEdit,
  NedCmdMenuSearch,
  NedCmdMenuGoto,
  NedCmdMenuOption,
  NedCmdMenuWindow,
  NedCmdMenuHelp,

  NedCmdMoveCenter,
  NedCmdMoveErrNext,
  NedCmdMoveErrPrev,
  NedCmdMoveFunctionDown,
  NedCmdMoveFunctionUp,
  NedCmdMoveGotoLine,
  NedCmdMoveMatchBrace,
  NedCmdMovePosNext,
  NedCmdMovePosPrev,
  NedCmdMoveScrollUp,
  NedCmdMoveScrollDown,
  NedCmdMoveToDefinition,

  NedCmdProjectFolders,
  NedCmdRedrawScreen,

  NedCmdReplace,
  NedCmdReplaceProject,
  NedCmdSearchAgain,
  NedCmdSearchBackward,
  NedCmdSearchForward,
  NedCmdSearchOptions,
  NedCmdSearchProject,

  NedCmdSelectAll,
  NedCmdSelectLine,
  NedCmdSelectOff,
  NedCmdSelectToggle,
  NedCmdSelectWord,

  NedCmdSettingsCrLf,
  NedCmdSettingsEdit,
  NedCmdSettingsFactory,
  NedCmdSettingsMapKeys,
  NedCmdSettingsTab,
  NedCmdSettingsTheme,
  NedCmdSettingsWrap,
  NedCmdViewKeyBindings
};

/* Check for duplicates as recorded in string array */
bool_t DuplicatesPresent(const char *sz, unsigned count, const char **szArray)
{
  unsigned  i;
  for(i=0; i<count; ++i)
  {
    if( sz && szArray[i] && (strcmp(sz, szArray[i]) == 0) )
      return TRUE;
  }
  return FALSE;
}

/*!------------------------------------------------------------------------------------------------
  Test Case for NedCmdGetName()

      const char *  NedCmdGetName     (pfnNedCmd_t pfnCmd);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcCmdGetName(void)
{
  #define COLS  5
  const char   *aszNames[NumElements(aAllCmds)];
  unsigned      i;
  unsigned      failures = 0;
  const char   *szCmdName;
  pfnNedCmd_t   pfnCmd;
  unsigned      numCmds = 0;
  unsigned      colWidth[COLS];
  char          szLine[80];
  const char   *szSep = " | ";

  NedTestBegin();

  // calculate column width for all commands
  memset(colWidth, 0, sizeof(colWidth));
  for(i=0; i<NumElements(aAllCmds); ++i)
  {
    szCmdName = NedCmdGetName(aAllCmds[i]);
    if(szCmdName && strlen(szCmdName) > (colWidth[i % COLS]))
      colWidth[i % COLS] = strlen(szCmdName);
  }

  // print all commmands in columns. Useful to put into NedManual.md. Verify no duplicates.
  memset(szLine, '-', sizeof(szLine) - 1);
  szLine[sizeof(szLine) - 1] = '\0';
  printf("\n");
  for(i=0; i < COLS; ++i)
    printf("%-*s%s", colWidth[i % COLS], "Command", ((i + 1) % COLS) != 0 ? szSep : "");
  printf("\n");
  for(i=0; i < COLS; ++i)
    printf(":%.*s%s", colWidth[i % COLS] - 1, szLine, ((i + 1) % COLS) != 0 ? szSep : "");
  printf("\n");
  for(i=0; i<NumElements(aAllCmds); ++i)
  {
    szCmdName = NedCmdGetName(aAllCmds[i]);
    aszNames[i] = szCmdName;

    if( szCmdName == NULL )
    {
      printf("Command Name %u not found\n", i);
      ++failures;
    }
    else if( DuplicatesPresent(szCmdName, i, aszNames) )
    {
      printf("Duplicate on entry %d\n", i);
      ++failures;
    }

    if( szCmdName != NULL)
      printf("%-*s%s", colWidth[i % COLS], szCmdName, ((i + 1) % COLS) != 0 ? szSep : "");
    if(((i+1) % COLS) == 0)
      printf("\n");
  }
  if((i % COLS) != 0)
    printf("\n");

  // verify converson from command to name
  for(i=0; i<NumElements(aAllCmds); ++i)
  {
    pfnCmd = NedCmdGetCommand(aszNames[i]);
    if( pfnCmd != aAllCmds[i] )
    {
      printf("Bad Command Name %s, index %u\n", aszNames[i] ? aszNames[i] : "NULL", i);
      ++failures;
    }
  }

  NedCmdGetAllCommands(&numCmds);
  if(numCmds != NumElements(aAllCmds))
  {
    printf("Expected %lu commands, got %u\n", NumElements(aAllCmds), numCmds);
    ++failures;
  }

  if(failures)
  {
    printf("%u failures!\n", failures);
    NedTestFailed();
  }

  printf("total commands %u\n", numCmds);

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test Case for NedCmdIs()

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcCmdIs(void)
{
  NedTestBegin();

  if(!NedCmdIs(NedCmdGetCommand("EditInsertSelf"), NEDCMD_FLAG_EDIT))
    NedTestFailed();
  if(!NedCmdIs(NedCmdGetCommand("EditCutLine"), NEDCMD_FLAG_CUT))
    NedTestFailed();
  if(NedCmdIs(NedCmdGetCommand("CursorUp"), NEDCMD_FLAG_EDIT))
    NedTestFailed();

  NedTestEnd();
}

int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcCmdGetName", TcCmdGetName },
    { "TcCmdIs",      TcCmdIs      }
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestCmd", NumElements(aTestCases), aTestCases);
  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
