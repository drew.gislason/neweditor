/*!************************************************************************************************
  @file NedTestMsg.c

  @brief  NedTestMenu - Tests the message and status bar functionality

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include "Ned.h"
#include "NedTest2.h"
#include "NedKeyMap.h"
#include "NedWin.h"
#include "NedMsg.h"

nedTheme_t *pTheme;

const sNedMenuItem_t *NedMenuItemPtr(void *hMenu, unsigned nMenu, unsigned nItem)
{
  (void)hMenu;
  (void)nMenu;
  (void)nItem;
  return NULL;
}

void NtmPrepScreen(void)
{
  NedWinInit();
  NedWinClearScreen(pTheme->text);
  NedWinFlushAll();
  NedWinPickBackdrop();
}

void NtmPrompt(const char *szPrompt)
{
  NedWinPickBackdrop();
  NedWinGoto(2, 0);
  NedWinSetAttr(pTheme->text);
  NedWinPrintf("%s", szPrompt);
  NedWinClearEol();
  NedWinFlush();
}

bool_t NtmPassFail(const char *szPrompt)
{
  nedKey_t  c;
  bool_t    fPassed = TRUE;
  winPos_t  row;
  winPos_t  col;

  NedWinPickBackdrop();
  NedWinGoto(2, 0);
  NedWinSetAttr(pTheme->text);
  NedWinPrintf("%s Press 'f' for fail, '<Enter> for pass: ", szPrompt);
  NedWinGetCursor(&row, &col);
  NedWinClearEol();
  NedWinFlush();
  NedWinGoto(row, col);
  NedWinFlushCursor();

  while(1)
  {
    c = NedKeyGetKey();
    if(c == NEDKEY_ENTER)
      break;
    if(c == 'f' || c == 'F')
    {
      fPassed = FALSE;
      break;
    }
  }
  return fPassed;
}


/*-------------------------------------------------------------------------------------------------
  Stubbed functions
-------------------------------------------------------------------------------------------------*/
void * NedMenuNew(sNedMenuBar_t *pMenuBar, nedThemeIndex_t themeIndex)
{
  (void)pMenuBar;
  (void)themeIndex;
  return NULL;
}
pfnNedCmd_t NedCmdGetCommand (const char * szName) { (void)szName; return NULL; }


/*!------------------------------------------------------------------------------------------------
  Test case for status bar functionality

  @ingroup  ned_testcase
  @returns  none
*///-----------------------------------------------------------------------------------------------
void TcStatusBar(void)
{
  static nedStatOpts_t aOpts[] =
  {
    NEDSTAT_OPT_NONE,
    NEDSTAT_OPT_READONLY, 
    NEDSTAT_OPT_UNSAVED | NEDSTAT_OPT_IN_SEARCH,
    NEDSTAT_OPT_IN_SEARCH | NEDSTAT_OPT_CASE_SENSITIVE | NEDSTAT_OPT_WRAP | NEDSTAT_OPT_WHOLE_WORD | NEDSTAT_OPT_REVERSE
  };
  static long          aRowCol[] =
  {
         1L, 1,
      1234L, 123,
    123456L, 12345L,
        99L, 3L,
  };
  static const char   *aszFilePath[] =
  {
    "file.c",
    "/Users/drewgislason/Documents/Work/Git/ned/test/NedTestMsg.c",
    "/Users/drewgislason/Documents/Work/Git/ned/test/untitled",
    "/Users/drewg/NedManual.md",
  };
  unsigned    i;
  char        szRowCol[42];

  NedTestBegin();

  for(i=0; i<NumElements(aOpts); ++i)
  {
    NedStatusBarSetOpts(aOpts[i]);
    NedStatusBarSetFile(aszFilePath[i]);
    NedStatusBarSetRowCol(aRowCol[i*2], aRowCol[i*2+1]);
    NedStatusBarShow();

    snprintf(szRowCol, sizeof(szRowCol), "Is it row:col %ld:%ld?", aRowCol[i*2], aRowCol[i*2+1]);
    if(!NtmPassFail(szRowCol))
      NedTestFailed();
  }

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test case for most Message Line functions

  @ingroup  ned_testcase
  @returns  none
*///-----------------------------------------------------------------------------------------------
void TcMsgLine(void)
{
  nedKey_t    c;
  unsigned    i;
  const char  szYna[] = "yna";
  const char *aszFiles[] = { "file1.c", "untitled", "~/Work/a.txt" };

  NedTestBegin();

  // test NedMsgPrintf()
  NedMsgPrintf("Hello World");
  if(!NtmPassFail("Does it say Hello World on message line?"))
    NedTestFailed();

  NedMsgPrintf("Yo");
  if(!NtmPassFail("Does it say Yo on message line?"))
    NedTestFailed();

  // test NedMsgClear()
  NedMsgClear();
  if(!NtmPassFail("Did msgline clear?"))
    NedTestFailed();

  // test NedMsgChoice()
  for(i=0; i<sizeof(szYna)-1; ++i)
  {
    char szPrompt[10];
    snprintf(szPrompt, sizeof(szPrompt), "Choose %c", szYna[i]);
    NtmPrompt(szPrompt);
    c = NedMsgChoice("Save: ", aszFiles[i], szYna);
    if(c != szYna[i])    
      NedTestFailed();
  }

  NtmPrompt("Choose Esc");
  c = NedMsgChoice("Save: ", aszFiles[0], szYna);
  if(c != NEDKEY_ESC)
    NedTestFailed();

  NtmPrompt("Choose Enter");
  c = NedMsgChoice("Save: ", aszFiles[1], szYna);
  if(c != szYna[0])
    NedTestFailed();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test case for most Message Line Edit functions

  @ingroup  ned_testcase
  @returns  none
*///-----------------------------------------------------------------------------------------------
void TcMsgLineEdit(void)
{
  char        szLine[80]      = "line";    /// make line artifically small
  const char  szHelloWorld[]  = "Hello World";
  long        len;

  NedTestBegin();

  // NedMsgLineEdit 
  NtmPrompt("Enter \"Hello World\" exactly in the line editor.\n"
            "Try ^V to paste, ^K to delEol, BS, arrows, etc...\n");
  strcpy(szLine, szHelloWorld);
  len = NedMsgLineEdit(szLine, 20, "Write: ", NULL, szHelloWorld);
  if((len != strlen(szHelloWorld)) || (strcmp(szLine, szHelloWorld) != 0))
    NedTestFailed();

  NtmPrompt("Enter stuff, then press Esc\n");
  strcpy(szLine, "or not to save");
  len = NedMsgLineEdit(szLine, sizeof(szLine)-1, "Save: ", NULL, "nada");
  if(len >= 0)
    NedTestFailed();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test MsgWidth

  @ingroup  ned_testcase
  @returns  none
*///-----------------------------------------------------------------------------------------------
void TcMsgWidth(void)
{
  const char *szPrompt      = "Did prompt shorten? ";
  const char *szExitKeys    = "yna";
  const char *szLongPrompt2 = "The quick brown fox jumped over the lazy dog's back "
                              "The quick brown fox jumped over the lazy dog's back "
                              "The quick brown fox jumped over the lazy dog's back "
                              "The quick brown fox jumped over the lazy dog's back "
                              "The quick brown fox jumped over the lazy dog's back ";
  nedKey_t    key;

  NedTestBegin();

  key = NedMsgChoice(szPrompt, szLongPrompt2, szExitKeys);
  if(key != 'y')
    NedTestFailed();

  NedTestEnd();
  NedMsgClear();
}

/*!------------------------------------------------------------------------------------------------
  Test getting a line. Make sure line is selected at the start

  @ingroup  ned_testcase
  @returns  none
*///-----------------------------------------------------------------------------------------------
void TcMsgLineLoop(void)
{
  char        szLine[100];
  long        len;

  NedTestBegin();

  NtmPrepScreen();
  while(1)
  {
    strcpy(szLine, "Hello World");
    len = NedMsgLineEdit(szLine, sizeof(szLine)-1, "Try things. When done, enter p for pass or f for fail: ", NULL, "bubba");
    NedLogPrintf("len %ld, %s\n", len, szLine);
    if(strcmp(szLine, "p") == 0)
      break;
    if(strcmp(szLine, "f") == 0)
      NedTestFailed();
  }

  NedTestEnd();
}

/*-------------------------------------------------------------------------------------------------
  Unit tests the msg system
-------------------------------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcMsgWidth",     TcMsgWidth,     TC_SCREEN | TC_USER_INPUT },
    { "TcStatusBar",    TcStatusBar,    TC_SCREEN | TC_USER_INPUT },
    { "TcMsgLine",      TcMsgLine,      TC_SCREEN | TC_USER_INPUT },
    { "TcMsgLineEdit",  TcMsgLineEdit,  TC_SCREEN | TC_USER_INPUT },
    { "TcMsgLineLoop",  TcMsgLineLoop,  TC_SCREEN | TC_USER_INPUT }
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestMsg", NumElements(aTestCases), aTestCases);

  // setup theme (as status bar and message line need it)
  NedThemeInit();
  pTheme = NedThemeGet(NEDTHEME_CLASSIC);
  NedStatusBarSetTheme(pTheme);
  NedKeyMapFactory();
  NtmPrepScreen();

  NedTestRun(hSuite);

  NedWinClearScreen(NEDATTR_RESET);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
