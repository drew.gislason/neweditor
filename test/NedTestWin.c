/*!************************************************************************************************
  @file NedTestWin.c

  @brief  Test Ned Window subsystem

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include "Ned.h"
#include "NedWin.h"
#include "NedLog.h"
#include "NedKey.h"

#define UT_NEDWIN_ROWS    16
#define UT_NEDWIN_COLS    20

// row 2, left 7, cols 9
typedef struct
{
  winPos_t  row;
  winPos_t  left;
  winPos_t  cols;
} sTestDirty_t;

static char m_szPassFail[] = "Press Enter if passed, 'f' if failed";

static nedKey_t NtwGetKey(const char *szName, const char *szParm);

/*-------------------------------------------------------------------------------------------------
  Intializes windowing system for basic tests. Called at start of each other test.
  @ingroup  ned_test
  @return   TRUE if passed, FALSE if failed
-------------------------------------------------------------------------------------------------*/
bool_t TestNedWinInit(void)
{
  bool_t  fInitialized;

  // intialize windowing system
  fInitialized = NedWinInitEx(UT_NEDWIN_ROWS, UT_NEDWIN_COLS);
  if(fInitialized)
  {
    NedWinClearScreen(NEDATTR_RESET);
    NedWinFlush();
  }
  else
    NedLogPrintf("  Failed to init!");
  return fInitialized;
}

/*-------------------------------------------------------------------------------------------------
  Basic test creates a window
  @ingroup  ned_test
  @return   TRUE if passed, FALSE if failed
-------------------------------------------------------------------------------------------------*/
bool_t TestNedWinBasic(void)
{
  void             *pWin;
  winPos_t          rows;
  winPos_t          cols;
  static const char szHello[] = "Hello";

  // initialize system
  if(!TestNedWinInit())
  {
    return FALSE;
  }
  pWin = NedWinNew(2, 2, UT_NEDWIN_ROWS-4, UT_NEDWIN_COLS-4);
  if(pWin == NULL)
  {
    NedLogPrintf("  Failed... Can't create window");
    return FALSE;
  }
  if(NedWinGetPicked() != pWin)
  {
    NedLogPrintf("  Failed... Picked %p, pWin %p\n",NedWinGetPicked(), pWin);
    return FALSE;
  }

  // fill with blue, then print in green with yellow, centered
  NedWinSetAttr(NEDBACK_NIGHT | NEDATTR_WHITE);
  NedWinClearAll();
  NedWinGetSize(&rows, &cols);
  NedWinGoto(rows/2, (cols-sizeof(szHello))/2);
  NedWinSetAttr(NEDBACK_GREEN | NEDATTR_YELLOW | NEDATTR_BOLD);
  NedWinPuts(szHello);
  NedWinFlush();
  NedWinLogDumpAll();
  NedWinClose(pWin);
  NedWinLogDumpAll();
  if(NedWinGetPicked() != NedWinGetBackdrop())
  {
    NedLogPrintf("  Failed... backdrop should be picked\n");
    return FALSE;
  }

  return TRUE;
}

/*-------------------------------------------------------------------------------------------------
  Tests NedWinPuts() and NedWinPutc() functions.

  @ingroup  ned_test
  @return   TRUE if passed, FALSE if failed
-------------------------------------------------------------------------------------------------*/
bool_t TestNedWinPuts(void)
{
  void             *pWin;
  winPos_t          rows;
  winPos_t          cols;
  static const char szHello[] = "Hello\nWorld";

  // initialize system
  if(!TestNedWinInit())
  {
    return FALSE;
  }

  // put hello world on background
  NedWinGoto(1, 1);
  NedWinPuts(szHello);

  pWin = NedWinNew(2, 2, UT_NEDWIN_ROWS-4, UT_NEDWIN_COLS-4);
  if(pWin == NULL)
  {
    NedLogPrintf("  Failed... alloc window\n");
    return FALSE;
  }
  if(NedWinGetPicked() != pWin)
  {
    NedLogPrintf("  Failed... Picked %p, pWin %p\n",NedWinGetPicked(), pWin);
    return FALSE;
  }

  // fill with blue, then print in green with yellow, centered
  NedWinSetAttr(NEDBACK_NIGHT | NEDATTR_WHITE);
  NedWinFillAll('.');
  NedWinFlush();
  NedWinGetSize(&rows, &cols);
  NedWinGoto(rows/2, cols/2);
  NedWinSetAttr(NEDBACK_WHITE | NEDATTR_RED | NEDATTR_BOLD);
  NedWinPuts(szHello);
  NedWinFlush();
  NedWinPutc('!');
  NedWinFlush();

  return TRUE;
}

/*-------------------------------------------------------------------------------------------------
  Test NedWinClose() of a single window. Does the background get redrawn on Flush?

  @ingroup  ned_test
  @return   TRUE if passed, FALSE if failed
-------------------------------------------------------------------------------------------------*/
bool_t TestNedWinClose(void)
{
  void       *pWin;
  winPos_t    rows;
  winPos_t    cols;
  char        szChristmas[] = "Christmas";

  NedWinInitEx(UT_NEDWIN_ROWS, UT_NEDWIN_COLS);
  NedWinClearScreen(NEDBACK_FOREST);

  pWin = NedWinNew(2, 2, UT_NEDWIN_ROWS-4, UT_NEDWIN_COLS-4);
  if(pWin == NULL)
  {
    NedLogPrintf("  Failed... alloc window\n");
    return FALSE;
  }
  NedWinSetAttr(NEDBACK_RED | NEDATTR_WHITE | NEDATTR_BOLD);
  NedWinClearAll();
  NedWinGetSize(&rows, &cols);
  NedWinGoto(rows/2, (cols - strlen(szChristmas))/2);
  NedWinPuts(szChristmas);
  NedWinFlush();
  NedWinLogDumpAll();

  if(NtwGetKey("See Christmas Window in Red?", m_szPassFail) != NEDKEY_ENTER)
    return FALSE;

  NedWinClose(pWin);
  NedWinFlush();
  NedWinLogDumpAll();

  return TRUE;
}

/*-------------------------------------------------------------------------------------------------
  Test NedWinFillStr().

    BackdropBackdropBackdrop
    Backd   No    opBackdrop
    Backd  Frame  opBackdrop
    Backd Window1 opBackdrop
    Backd     +---------+rop
    BackdropBa| Framed  |rop
    BackdropBa| Window2 |rop
    BackdropBa+---------+rop
    BackdropBackdropBackdrop

  @ingroup  ned_test
  @return   TRUE if passed, FALSE if failed
-------------------------------------------------------------------------------------------------*/
bool_t TestNedWinFillStr(void)
{
  void *pWin1;
  void *pWin2;
  NedWinInitEx(9,24);
  NedWinClearScreen(NEDATTR_RESET);
  NedWinFillStr("Backdrop");
  pWin1 = NedWinNew(1, 5, 4, 9);
  NedWinGoto(0, 3);
  NedWinPuts("No");
  NedWinGoto(1, 2);
  NedWinPuts("Frame");
  NedWinGoto(2, 1);
  NedWinPuts("Window1");
  pWin2 = NedWinNew(4, 11, 2, 9);
  NedWinPuts(" Framed\n Window2");
  NedWinFrameSet(TRUE);
  NedWinFlush();
  return (pWin1 && pWin2) ? TRUE : FALSE;
}

/*-------------------------------------------------------------------------------------------------
  Test overlapping windows.
  @ingroup  ned_test
  @return   TRUE if passed, FALSE if failed
-------------------------------------------------------------------------------------------------*/
bool_t TestNedWinOverlap(void)
{
  unsigned     i;
  nedAttr_t    aAttr[] = { NEDBACK_RED | NEDATTR_WHITE | NEDATTR_BOLD ,
                           NEDBACK_GREEN,
                           NEDBACK_BLUE | NEDATTR_WHITE, 
                           NEDBACK_PURPLE,
                           NEDBACK_CHARCOAL | NEDATTR_WHITE | NEDATTR_BOLD };
  void        *apWin[NumElements(aAttr)];

  if(!TestNedWinInit())
  {
    return FALSE;
  }
  NedLogPrintf("After Initial\n\n");

  for(i=0; i<NumElements(aAttr); ++i)
  {
    apWin[i] = NedWinNew(i*3, i*2, 8, 8);
    if(apWin[i] == NULL)
    {
      NedLogPrintf("  Failed... alloc window\n");
      return FALSE;
    }

    NedWinSetAttr(aAttr[i]);
    NedWinFillAll('1'+i);
  }
  NedWinFlush();

  if(NtwGetKey("Do all 5 windows appear, with 5 cut-off?", m_szPassFail) != NEDKEY_ENTER)
    return FALSE;

  NedWinPick(apWin[3]);
  NedWinFrameSetAttr(NEDBACK_BLACK | NEDATTR_WHITE | NEDATTR_BOLD);
  NedLogPrintf("\nOverlap: After Frame\n");
  NedWinFlush();

  if(NtwGetKey("Do you see the frame on 4?", m_szPassFail) != NEDKEY_ENTER)
    return FALSE;

  NedWinFrameLinePick(2);
  NedWinFlush();

  if(NtwGetKey("Do you see line 2 picked on frame of 4?", m_szPassFail) != NEDKEY_ENTER)
    return FALSE;

  NedWinFrameLinePick(4);
  NedWinFlush();

  if(NtwGetKey("Do you see line 4 picked on frame of 4?", m_szPassFail) != NEDKEY_ENTER)
    return FALSE;

  NedWinFrameLinePick(NEDWIN_LINE_NOPICK);
  NedWinFlush();

  if(NtwGetKey("Do you see line nothing picked on frame of 4?", m_szPassFail) != NEDKEY_ENTER)
    return FALSE;

  NedWinFrameSet(FALSE);
  NedLogPrintf("\nOverlap: Frame Removed\n");
  NedWinFlush();

  if(NtwGetKey("Do you see the frame removed on 4?", m_szPassFail) != NEDKEY_ENTER)
    return FALSE;

  return TRUE;
}

/*-------------------------------------------------------------------------------------------------
  Test Clear screen
  @ingroup  ned_test
  @return   TRUE if passed, FALSE if failed
-------------------------------------------------------------------------------------------------*/
bool_t TestNedWinClearScreen(void)
{
  bool_t  fPassed = TRUE;

  NedWinInitEx(UT_NEDWIN_ROWS, UT_NEDWIN_COLS);
  NedWinClearScreen(NEDBACK_BLUE);
  if(NtwGetKey("Is screen blue?", m_szPassFail) != NEDKEY_ENTER)
    fPassed = FALSE;

  NedWinClearScreen(NEDBACK_GREEN);
  if(NtwGetKey("Is screen green?", m_szPassFail) != NEDKEY_ENTER)
    fPassed = FALSE;

  NedWinClearScreen(NEDBACK_BLACK);
  if(NtwGetKey("Is screen black?", m_szPassFail) != NEDKEY_ENTER)
    fPassed = FALSE;

  NedWinClearScreen(NEDATTR_RESET);

  return fPassed;
}

/*-------------------------------------------------------------------------------------------------
  Make various windows dirty. Verify ONLY the dirty parts get sent to screen, and colors work.
  For purposes of this test, canvas is 4 x 8, window is only 1x2

  @ingroup  ned_test
  @return   TRUE if passed, FALSE if failed.
-------------------------------------------------------------------------------------------------*/
bool_t TestNedWinDirty(void)
{
  bool_t        fPassed   = TRUE;
  static char   szDirty1[] = "dd";
  static char   szDirty2[] = "DD";
  nedKey_t      c;
  void         *pWin;

  // Create a small 1x2 window
  NedWinInitEx(UT_NEDWIN_ROWS, UT_NEDWIN_COLS);
  NedWinClearScreen(NEDATTR_NONE);

  pWin = NedWinNewEx(1, 2, 1, 2, NEDATTR_EDIT_TXT, FALSE, NEDATTR_EDIT_FRAME);
  if(!pWin)
    fPassed = FALSE;
  else
  {
    NedWinPuts(szDirty1);
    NedWinFlush();
    NedWinLogDumpCanvas();
    c = NtwGetKey("Do you see a small blue window with dd?", "Press Enter for pass, 'f' for fail");
    if(c == 'f')
      fPassed = FALSE;

    if(fPassed)
    {
      NedWinGoto(0, 0);
      NedWinPuts(szDirty2);
      NedWinLogDumpCanvas();
      NedWinFlush();
    }
  }

  return fPassed;
}

/*-------------------------------------------------------------------------------------------------
  Adjust window size. Verify results.

  @ingroup  ned_test
  @return   TRUE if passed, FALSE if failed.
-------------------------------------------------------------------------------------------------*/
bool_t TestNedWinAdjust(void)
{
  winPos_t     rows     = 0;
  winPos_t     cols     = 0;
  static char  szRowsCols[12];  // xxxx,xxxx

  if(!TestNedWinInit())
    return FALSE;
  NedWinGetSize(&rows, &cols);
  if( (rows != UT_NEDWIN_ROWS) || (cols != UT_NEDWIN_COLS) )
  {
    NedLogPrintf("  Failed... screenRows %u, screenCols %u\n");
    return FALSE;
  }

  // adjust larger
  if(!NedWinAdjustScreen(UT_NEDWIN_ROWS + 2, UT_NEDWIN_COLS + 2))
  {
    NedLogPrintf("  Failed... adjusting\n");
    return FALSE;
  }
  else
  {
    rows = cols = 0;
    NedWinPickBackdrop();
    NedWinGetSize(&rows, &cols);
    if((rows != (UT_NEDWIN_ROWS + 2)) || (cols != (UT_NEDWIN_COLS + 2)))
    {
      NedLogPrintf("  Failed... rows %u, cols %u\n", rows, cols);
      return FALSE;
    }
  }
  NedWinLogDumpAll();

  // adjust smaller
  if(!NedWinAdjustScreen(UT_NEDWIN_ROWS - 2, UT_NEDWIN_COLS - 2))
  {
    NedLogPrintf("  Failed... adjusting\n");
    return FALSE;
  }
  else
  {
    rows = cols = 0;
    NedWinPickBackdrop();
    NedWinGetSize(&rows, &cols);
    if((rows != (UT_NEDWIN_ROWS - 2)) || (cols != (UT_NEDWIN_COLS - 2)))
    {
      NedLogPrintf("  Failed... rows %d, cols %d\n", rows, cols);
      return FALSE;
    }
    else
    {
      snprintf(szRowsCols, sizeof(szRowsCols), "%d,%d ",rows, cols);
      NedWinGoto(rows - 1, cols - strlen(szRowsCols));
      NedWinPuts(szRowsCols);
      NedWinFlush();
    }
  }
  NedWinLogDumpAll();

  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Allow moving windows around, including frames, etc...

  @ingroup  ned_test
  @return   TRUE if passed, FALSE if failed.
*///-----------------------------------------------------------------------------------------------
bool_t TestNedWinInteractive(void)
{
  void              *pWin;
  unsigned           i;
  winPos_t           top;
  winPos_t           left;
  winPos_t           rows;
  winPos_t           cols;
  nedKey_t           c;
  void              *apWin[5];
  static unsigned    strIndex = 0;
  static char       *aszString[] = { "Hello", "World", " Ned " };
  static nedAttr_t   aStrAttr[]  = { NEDBACK_AQUA | NEDATTR_WHITE, 
                                     NEDBACK_YELLOW,
                                     NEDBACK_NIGHT | NEDATTR_YELLOW | NEDATTR_BOLD };
  static nedAttr_t   aAttr[]     = { NEDBACK_RED | NEDATTR_WHITE | NEDATTR_BOLD ,
                           NEDBACK_GREEN,
                           NEDBACK_BLUE | NEDATTR_WHITE, 
                           NEDBACK_PURPLE,
                           NEDBACK_CHARCOAL | NEDATTR_WHITE | NEDATTR_BOLD };
  static nedAttr_t   aBackAttr[] = { NEDBACK_AQUA | NEDATTR_WHITE, 
                                     NEDBACK_YELLOW,
                                     NEDATTR_RESET };  
  static unsigned    backIndex = 0;

  // create some windows
  NedLogPrintf("<here>");
  TestNedWinInit();
  NedLogPrintf("<there>");
  for(i=0; i<NumElements(apWin); ++i)
  {
    apWin[i] = NedWinNew(i*2, i*2, 8, 8);
    NedLogPrintf("<here %u>", i);
    if(apWin[i] == NULL)
    {
      NedLogPrintf("  Failed... alloc window");
      return FALSE;
    }
    NedWinSetAttr(aAttr[i]);
    NedWinFillAll('1'+i);
    if(i==3)
      NedWinFrameSetAttr(NEDBACK_BLACK | NEDATTR_WHITE | NEDATTR_BOLD);
  }
  NedWinLogDumpAll();

  do
  {
    NedLogPrintf("Canvas B4 flush: ");
    NedWinLogDumpCanvas();
    NedWinFlush();
    AnsiSetAttr(NEDATTR_RESET);
    AnsiGoto(UT_NEDWIN_ROWS + 1, 0);
    printf("arrows=move,1-5=select,t=top,s=str,c=close,b=background,r=frame");
    fflush(stdout);
    c = NtwGetKey("Interactive", m_szPassFail);
    NedLogPrintf("...got c=%u\n", c);

    if(c >= '1' && c <= '5')
      NedWinPick(apWin[c-'1']);
    else if(c == NEDKEY_UP)
    {
      NedWinGetPos(&top,&left);
      --top;
      NedWinMove(top,left);
    }
    else if(c == NEDKEY_DOWN)
    {
      NedWinGetPos(&top, &left);
      ++top;
      NedWinMove(top,left);
    }
    else if(c == NEDKEY_LEFT)
    {
      NedWinGetPos(&top,&left);
      --left;
      NedWinMove(top,left);
    }
    else if(c == NEDKEY_RIGHT)
    {
      NedWinGetPos(&top,&left);
      ++left;
      NedWinMove(top,left);
    }
    else if(c == 't')
      NedWinFront();
    else if(c == 's')
    {
      NedWinGetSize(&rows,&cols);
      if(cols > strlen(aszString[strIndex]))
        cols = cols - strlen(aszString[strIndex]);
      else
        cols = 0;
      NedWinGoto(rows/2, cols/2);
      NedWinSetAttr(aStrAttr[strIndex]);
      NedWinPuts(aszString[strIndex]);
      ++strIndex;
      if(strIndex >= NumElements(aszString))
        strIndex = 0;
    }
    else if(c == 'd')
    {
      NedWinLogDumpAll();
    }
    else if(c == 'w')
    {
      NedWinLogDumpWin(NedWinGetPicked());
    }
    else if(c == 'c')
    {
      NedWinClose(NedWinGetPicked());
    }
    else if(c == 'b')
    {
      pWin = NedWinGetPicked();
      NedWinPick(NedWinGetBackdrop());
      NedWinSetAttr(aBackAttr[backIndex]);
      NedWinClearAll();
      ++backIndex;
      if(backIndex >= NumElements(aBackAttr))
        backIndex = 0;
      NedWinPick(pWin);
    }
    else if(c == 'r')
    {
      if(NedWinIsFramed())
        NedWinFrameSet(FALSE);
      else
        NedWinFrameSet(TRUE);
    }

  } while( (c != NEDKEY_ENTER) && (c != 'f'));


  // done, close the windows
  for(i=0; i<NumElements(apWin); ++i)
    NedWinClose(apWin[i]);

  return (c == NEDKEY_ENTER ? TRUE : FALSE);
}

typedef struct
{
  const char *szName;
  bool_t    (*pfnTest)(void);
  const char *szExpectedResults;
} sUtNedWinTest_t;

sUtNedWinTest_t aUtNedWinTests[] =
{
  { "TestNedWinInit",        TestNedWinInit , "Window should be all clear to background" },
  { "TestNedWinBasic",       TestNedWinBasic, "Window should have green/yellow hello centered in field of blue" },
  { "TestNedWinPuts",        TestNedWinPuts,  "'.' filled window. hello in white/red cleared to eol, world! below at col 0 in red/white" },
  { "TestNedClearScreen",    TestNedWinClearScreen, "Is screen back to default?" },
  { "TestNedWinClose",       TestNedWinClose,  "Cheristmas Window should be closed, green background only." },
  { "TestNedWinFillStr",     TestNedWinFillStr,  "Should see background with 2 windows, 1 framed." },
  { "TestNedWinOverlap",     TestNedWinOverlap, "5 Overlapping windows" },
  { "TestNedWinDirty",       TestNedWinDirty, "Only 2 chars should be dirty in Canvas in log" },
  { "TestNedWinAdjust",      TestNedWinAdjust, "" },
  { "TestNedWinInteractive", TestNedWinInteractive, "" }
};

/*
  Gets a key while not using NedWin() system (ANSI only).
*/
static nedKey_t NtwGetKey(const char *szName, const char *szParm)
{
  nedKey_t  c;

  AnsiSetAttr(NEDATTR_RESET);
  AnsiGoto(UT_NEDWIN_ROWS + 2, 0);
  printf("%s %s: ",szName, szParm);
  AnsiClearEol();

  c = NedKeyGetKey();

  return c;
}

int main(int argc, const char *argv[])
{
  static const char aszBanner[] =
  {
    "\n\n"
    "=======  -----  -----   ---  -----  =======\n"
    "_______    |    |__    /       |    _______\n"
    "           |    |      ----    |\n"
    "=======    |    |____  ___/    |    =======\n"
  };

  unsigned    i;
  const char *pszTest = NULL;
  unsigned    passed  = 0;
  unsigned    failed  = 0;
  bool_t      fPassed;
  unsigned    c;
  int         ret;
  FILE       *fp;
  char        szRetCode[6];
  const char  m_szRetCodeFile[] = "testall.tmp";
  bool_t      m_fMachineReadable = FALSE;

  for(i = 1; i < argc; ++i)
  {
    if(strcmp(argv[i],"--help") == 0)
    {
      printf("test_win [-l] [TestFilter]\n");
      return 1;
    }
    else if(strcmp(argv[i],"-l") == 0)
    {
      printf("test_win list of tests:\n");
      for(i=0; i<NumElements(aUtNedWinTests); ++i)
        printf("  %s\n", aUtNedWinTests[i].szName);
      return 0;
    }
    else if(strcmp(argv[i], "-m") == 0)
      m_fMachineReadable = TRUE;
    else
      pszTest = argv[i];
  }

  // run through test suite. Press Esc twice to quit
  NedLogPrintf(aszBanner);
  NedLogPrintf("\n\n\n========== Starting Test Run ===========\n\n\n");
  for(i=0; i<NumElements(aUtNedWinTests); ++i)
  {
    // skip test if doesn't match user test filter
    if( pszTest && (strstr(aUtNedWinTests[i].szName, pszTest) == NULL) )
      continue;

    NedLogPrintf("\n------ %s Begin -------\n",aUtNedWinTests[i].szName);
    fPassed = (aUtNedWinTests[i].pfnTest)();
    NedLogPrintf("------ %s %s -------\n", aUtNedWinTests[i].szName,
      fPassed ? "Passed" : "Failed");

    if(aUtNedWinTests[i].szExpectedResults)
    {
      AnsiSetAttr(NEDATTR_RESET);
      AnsiGoto(UT_NEDWIN_ROWS + 1, 0);
      printf("%s",aUtNedWinTests[i].szExpectedResults);
      AnsiClearEol();
    }

    if(fPassed)
    {
      c = NtwGetKey(aUtNedWinTests[i].szName, " - Press <Enter> if it passed, 'f' if failed, '.' to quit");
      if(c == '.')
      {
        NedLogPrintf("Used quit after test %s...\n", aUtNedWinTests[i].szName);
        break;
      }
      if(c != 'f')
      {
        ++passed;
        NedLogPrintf("Tester agreed test passed...\n");
      }
      else
        fPassed = FALSE;
    }

    if(!fPassed)
      ++failed;
  }

  NedLogPrintf("\n\n============== Summary ==============\n");
  NedLogPrintf("    %3u Tests Passed\n",passed);
  NedLogPrintf("    %3u Tests Failed\n",failed);
  NedLogPrintf(    "========== Ended Test Run ===========\n\n\n");

  // NedKeyExit();

  AnsiGoto(0,0);
  AnsiSetAttr(NEDATTR_RESET);
  AnsiClearScreen();
  printf("%3u Tests Passed\n",passed);
  printf("%3u Tests Failed\n",failed);
  if(failed)
    ret = 1;
  else if(failed == 0 && passed == 0)
    ret = 2;
  else
    ret = 0;

  remove(m_szRetCodeFile);
  if(m_fMachineReadable)
  {
    fp = fopen(m_szRetCodeFile, "w");
    if(fp)
    {
      sprintf(szRetCode, "%d\n", ret);
      fwrite(szRetCode, 1, strlen(szRetCode), fp);
      fclose(fp);
    }
  }

  return ret;
}
