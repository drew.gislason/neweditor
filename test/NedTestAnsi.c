/*!************************************************************************************************
  @file NedUtAnsi.c

  @brief  Ned ANSI Unit Test

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include "Ned.h"
#include "NedAnsi.h"
#include "NedTest2.h"

void NedTestAnsiShowLocation(void)
{
  char      s[40];
  unsigned  len;
  unsigned  rows;
  unsigned  cols;
  unsigned  i;
  unsigned  val;

  NedTestBegin();

  AnsiSetAttr(NEDBACK_NIGHT | NEDATTR_WHITE | NEDATTR_BOLD);
  AnsiGetRowsCols(&rows, &cols);
  AnsiClearScreen();

  AnsiGoto(0, 0);
  printf("0,0");
  AnsiGoto(1, 0);
  printf("NedTestAnsiShowLocation");
  AnsiGoto(2, 3);
  printf("2, 3");
  len = snprintf(s, sizeof(s), "%u,%u", rows, cols);
  AnsiGoto(rows-2, (cols-10) - len);
  printf("%s",s);
  AnsiGoto(rows-1, (cols-1) - len);
  printf("%s",s);

  AnsiGoto(rows/2, 0);
  val = 0;
  for(i=0; i<cols; ++i)
  {
    printf("%u",val);
    ++val;
    if(val >= 10)
      val = 0;
  }

  AnsiGoto(rows-1, 0);

  printf("Rows %d, Cols %d. ", rows, cols);
  if(!NedTestPassFail())
    NedTestFailed();

  NedTestEnd();
}

void NedTestAnsiAllColors(void)
{
  unsigned  rows;
  unsigned  cols;

  NedTestBegin();

  AnsiSetAttr(NEDATTR_RESET);
  AnsiClearScreen();
  AnsiShowAllColors();
  AnsiGetRowsCols(&rows, &cols);
  AnsiSetAttr(NEDATTR_RESET);

  printf("Rows %d, Cols %d. Total chars %zu\n", rows, cols, AnsiCharCount());
  if(!NedTestPassFail())
    NedTestFailed();

  NedTestEnd();
}

int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "NedTestAnsiShowLocation",  NedTestAnsiShowLocation, TC_SCREEN | TC_USER_INPUT },
    { "NedTestAnsiAllColors", NedTestAnsiAllColors, TC_SCREEN | TC_USER_INPUT }
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("SimpleSuite", NumElements(aTestCases), aTestCases);
  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}

