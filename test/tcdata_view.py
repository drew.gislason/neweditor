import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation

fig = plt.figure()
fig.set_dpi(100)
fig.set_size_inches(7, 7)
ax = plt.axes(xlim=(0, 20), ylim=(0, 20))
ax.axes.set_aspect('equal')

patch = plt.Circle((5, -5), 0.75, facecolor='orange')

def _init():
    patch.center = (5, 5)
    ax.add_patch(patch)
    return patch,

def _animate(i):
    x, y = patch.center
    x = 5 + 3 * np.sin(np.radians(i))
    y = 5 + 3 * np.cos(np.radians(i))
    patch.center = (x, y)
    return patch,

anim = animation.FuncAnimation(fig, _animate, 
                               init_func=_init, 
                               frames=360, 
                               interval=20,
                               blit=True)

#anim.save('animation.mp4', fps=30, 
#          extra_args=['-vcodec', 'h264', 
#                      '-pix_fmt', 'yuv420p'])

plt.show()
