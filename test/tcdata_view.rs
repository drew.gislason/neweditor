/// Cows and Bulls v1.0
use std::io;
use rand::Rng;
use std::io::Write;

const MASTER_PATTERN: &str = "ROYGBIV";
const MASTER_ITEMS: i32 = 5;     // # of randomly generated colors
const MASTER_GUESSES: i32 = 12;
const MASTER_HELP: &str = r#"
    Moo! Cows and Bulls v1.0

    Guess a random pattern of characters. With each guess, the number of items
    in the correct column (bulls) and the number of items that are in the pattern
    but the wrong column (cows) are returned.

    Example:

    Pattern: BBIRO  (blue, blue, indigo, red, orange)
    Guess 1: BOYYR
        => Bulls: 1, Cows: 2 (blue in right column, red and orange in wrong columns)
    Guess 2: OOORR
        => Bulls: 1, Cows: 1 (red in right column, orange in wrong column)
    Guess 3: GGGGG
        => Bulls: 0, Cows: 0 (no green in pattern)

"#;

fn main() {

    println!("{}", MASTER_HELP);

    loop {
        cowbull_play(MASTER_PATTERN, MASTER_ITEMS, MASTER_GUESSES);

        let again = cowbull_getline("\nPlay Again? ");
        if again.len() > 0 {
            let again = &again[0..1];
            if again == "Y" || again == "y" {
                println!("");
                continue;
            }
        }
        break;
    }
}

// get a line with a prompt
fn cowbull_getline(prompt: &str) -> String {
    print!("{}", prompt);
    let _ = io::stdout().flush();
    let mut ret_str = String::new();
    io::stdin().read_line(&mut ret_str).expect("Failed to read line");

    ret_str.trim().to_string()
}

/// play a masterrust pattern guessing game
fn cowbull_play(master_pattern: &str, master_items: i32, master_guesses: i32) {

    assert!(master_pattern.chars().count() >= 1);
    assert!(master_items >= 1);
    assert!(master_guesses >= 1);

    // show help at start of each game
    println!("Guess a set of {} colors (randomly selected from {}) in {} guesses or less", 
        master_items, master_pattern, master_guesses);

    // generate a random pattern
    let pattern = cowbull_generate(master_pattern, master_items);

    let hidden = String::from_utf8(vec![b'*'; master_items as usize]).unwrap();
    println!("Pattern: {}\n", hidden);

    let mut indent = String::from_utf8(vec![b' '; 10]).unwrap();
    let mut num_guesses = 1;
    loop {

        // get the guess
        let prompt = format!("Guess {}: ", num_guesses);
        let guess = cowbull_getline(&prompt);

        // check for help
        if guess == "?" {
            println!("{}", MASTER_HELP);
            println!("Pattern: {}", pattern);   // DEBUG!
            continue;
        }

        // check # of characters
        let len = guess.chars().count() as i32;
        if len != master_items {
            println!("{}Enter {} characters, you entered {}", indent, master_items, len);
            continue;
        }
        let guess = guess.to_uppercase();

        println!("{}Your guess is: {}", indent, guess);    // DEBUG!

        // check guess against pattern to determine right guesses and wrong columns guesses
        let (bulls, cows) = cowbull_check(&pattern, &guess);
        println!("{}=> bulls: {}, cows: {}", indent, bulls, cows);
        if bulls == master_items {
            println!("{}Moo! Let the bells ring out and the banners fly, you have won!", indent);
            break;
        }
        if num_guesses >= master_guesses {
            println!("{}Moo who who! You lost. The pattern was: {}", indent, master_pattern);
            break;
        }

        num_guesses += 1;
        if num_guesses == 10 {
            indent.push(' ');
        }
    }
}

/// generate a random pattern for cows and bulls
fn cowbull_generate(master_pattern: &str, master_items: i32,  ) -> String {
    let mut pattern = String::new();

    for _ in 0..master_items {
        let color_index = rand::thread_rng().gen_range(0, master_pattern.len());
        pattern.push_str(&master_pattern[color_index..color_index+1]);
    }

    pattern
}

/// check pattern against guess for cows and bulls
fn cowbull_check(pattern: &str, guess: &str) -> (i32, i32) {

    let mut bulls = 0;
    let mut cows = 0;
    let mut pat_leftover = String::new();       // items not found in pattern this guess
    let mut guess_leftover = String::new();     // items not used in this guess

    assert!(guess.chars().count() == pattern.chars().count());

    // check for bulls (right character, right column)
    for (i, pat_char) in pattern.chars().enumerate() {

        // is it a bull?
        let guess_char = guess.chars().nth(i).unwrap();
        if pat_char ==  guess_char {
            bulls += 1;
        }

        // not a bull; might be a cow
        else {
            pat_leftover.push(pat_char);
            guess_leftover.push(guess_char);
        }
    }

    // println!("AFTER BULLS: pat_leftover {}, guess_leftover {}", pat_leftover, guess_leftover);

    // check for cows (right character, wrong column)
    for guess_char in guess_leftover.chars() {
        let result = pat_leftover.find(guess_char);
        match result {
            Some(found_idx) => {
                let mut new_pat_leftover = String::new();
                for (i, c) in pat_leftover.chars().enumerate() {
                    if i != found_idx {
                        new_pat_leftover.push(c);
                    }
                }

                pat_leftover = new_pat_leftover;
                cows += 1;
                // println!("IN COWS {}: guess_char {}, pat_leftover {}", cows, guess_char, pat_leftover);
            },
            None => {},
        }
    }

    (bulls, cows)
}
