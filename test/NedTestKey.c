/*!************************************************************************************************
  @file NedUtKey.c

  @brief  Ned Key Unit Test

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "Ned.h"
#include "NedKey.h"
#include "NedTest2.h"

/*!------------------------------------------------------------------------------------------------
  show raw key sequences

  @ingroup  ned_test
  @returns  none
*///-----------------------------------------------------------------------------------------------
static void NedTestKeyRaw(void)
{
  nedKey_t    c = 0;
  bool_t      fInNone = 0;

  // get a key, no echo
  printf("Press '.' to exit return for newline, Otherwise, will display raw key sequences\n");
  while(c != '.')
  {
    c = NedKeyRawGetKey();
    if(c != NEDKEY_NONE)
    {
      if(c == NEDKEY_ENTER)
        printf("\r\n");
      printf("%d ",c);
      fflush(stdout);
      fInNone = 0;
    }
    else if(!fInNone)
    {
      fInNone = 1;
      printf("(None) ");
      fflush(stdout);
    }
  }
  printf("\r\n");
  NedKeyRawDisable();
}

/*-------------------------------------------------------------------------
  Print all the key value/names pairs to stdout in JSON form
-------------------------------------------------------------------------*/
static void NedTestKeyPrintNames(void)
{
  static nedKey_t aKeys[] =
  {
    NEDKEY_CTRL_SPACE,
    NEDKEY_CTRL_A,
    NEDKEY_CTRL_B,
    NEDKEY_CTRL_C,
    NEDKEY_CTRL_D,
    NEDKEY_CTRL_E,
    NEDKEY_CTRL_F,
    NEDKEY_CTRL_G,
    NEDKEY_CTRL_H,
    NEDKEY_TAB,
    NEDKEY_CTRL_J,
    NEDKEY_CTRL_K,
    NEDKEY_CTRL_L,
    NEDKEY_ENTER,
    NEDKEY_CTRL_N,
    NEDKEY_CTRL_O,
    NEDKEY_CTRL_P,
    NEDKEY_CTRL_Q,
    NEDKEY_CTRL_R,
    NEDKEY_CTRL_S,
    NEDKEY_CTRL_T,
    NEDKEY_CTRL_U,
    NEDKEY_CTRL_V,
    NEDKEY_CTRL_W,
    NEDKEY_CTRL_X,
    NEDKEY_CTRL_Y,
    NEDKEY_CTRL_Z,
    NEDKEY_ESC,
    NEDKEY_CTRL_BACKSLASH,
    NEDKEY_CTRL_RIGHT_BRACE,
    NEDKEY_CTRL_CAROT,
    NEDKEY_CTRL_MINUS,
    NEDKEY_BACKSPACE,
    NEDKEY_UP,
    NEDKEY_DOWN,
    NEDKEY_LEFT,
    NEDKEY_RIGHT,
    NEDKEY_HOME,
    NEDKEY_END,
    NEDKEY_PGUP,
    NEDKEY_PGDN,
    NEDKEY_CTRL_UP,
    NEDKEY_CTRL_DOWN,
    NEDKEY_CTRL_LEFT,
    NEDKEY_CTRL_RIGHT,
    NEDKEY_CTRL_HOME,
    NEDKEY_CTRL_END,
    NEDKEY_CTRL_PGUP,
    NEDKEY_CTRL_PGDN,
    NEDKEY_ALT_UP,
    NEDKEY_ALT_DOWN,
    NEDKEY_ALT_LEFT,
    NEDKEY_ALT_RIGHT,
    NEDKEY_ALT_HOME,
    NEDKEY_ALT_END,
    NEDKEY_ALT_PGUP,
    NEDKEY_ALT_PGDN,
    NEDKEY_FN1,
    NEDKEY_FN2,
    NEDKEY_FN3,
    NEDKEY_FN4,
    NEDKEY_FN5,
    NEDKEY_FN6,
    NEDKEY_FN7,
    NEDKEY_FN8,
    NEDKEY_FN9,
    NEDKEY_FN10,
    NEDKEY_FN11,
    NEDKEY_FN12,
    NEDKEY_ALT_A,
    NEDKEY_ALT_B,
    NEDKEY_ALT_C,
    NEDKEY_ALT_D,
    NEDKEY_ALT_E,
    NEDKEY_ALT_F,
    NEDKEY_ALT_G,
    NEDKEY_ALT_H,
    NEDKEY_ALT_I,
    NEDKEY_ALT_J,
    NEDKEY_ALT_K,
    NEDKEY_ALT_L,
    NEDKEY_ALT_M,
    NEDKEY_ALT_N,
    NEDKEY_ALT_O,
    NEDKEY_ALT_P,
    NEDKEY_ALT_Q,
    NEDKEY_ALT_R,
    NEDKEY_ALT_S,
    NEDKEY_ALT_T,
    NEDKEY_ALT_U,
    NEDKEY_ALT_V,
    NEDKEY_ALT_W,
    NEDKEY_ALT_X,
    NEDKEY_ALT_Y,
    NEDKEY_ALT_Z,
    NEDKEY_BACK_TAB,
    NEDKEY_DELETE,
    NEDKEY_ALT_DASH,
    NEDKEY_ALT_EQUAL,
    NEDKEY_ALT_LEFT_BRACE,
    NEDKEY_ALT_RIGHT_BRACE,
    NEDKEY_ALT_BACKSLASH,
    NEDKEY_ALT_SPACE,
    NEDKEY_ALT_COLON,
    NEDKEY_ALT_QUOTE,
    NEDKEY_ALT_COMMA,
    NEDKEY_ALT_PERIOD,
    NEDKEY_ALT_SLASH,
    NEDKEY_NONE
  };
  const char *szName;

  printf("{\r\n  \"nedKeys\": {\r\n");
  for(unsigned i=0; i<NumElements(aKeys); ++i)
  {
    szName = NedKeyName(aKeys[i]);
    if( strcmp(szName,"Ctrl-\\") == 0 )
      szName = "Ctrl-\\\\";
    if( strcmp(szName,"Alt-\\") == 0 )
      szName = "Alt-\\\\";
    if( strcmp(szName,"Alt-\"") == 0 )
      szName = "Alt-\\\"";

    printf("    \"%s\": %d", szName, aKeys[i]);
    if(i + 1 != NumElements(aKeys))
      printf(",");
    printf("\r\n");
  }
  printf("  }\r\n}\r\n");
}

/*!------------------------------------------------------------------------------------------------
  Test Case testing key names

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcCheckKeyNames(void)
{
  static char * aszNames[] =
  {
    "Ctrl-Space",
    "Ctrl-A",
    "Ctrl-B",
    "Ctrl-C",
    "Ctrl-D",
    "Ctrl-E",
    "Ctrl-F",
    "Ctrl-G",
    "Ctrl-H",
    "Tab",
    "Ctrl-J",
    "Ctrl-K",
    "Ctrl-L",
    "Enter",
    "Ctrl-N",
    "Ctrl-O",
    "Ctrl-P",
    "Ctrl-Q",
    "Ctrl-R",
    "Ctrl-S",
    "Ctrl-T",
    "Ctrl-U",
    "Ctrl-V",
    "Ctrl-W",
    "Ctrl-X",
    "Ctrl-Y",
    "Ctrl-Z",
    "Esc",
    "Ctrl-\\",
    "Ctrl-]",
    "Ctrl-^",
    "Ctrl--",
    " ",
    "a",
    "~",
    "Backspace",
    "Up",
    "Down",
    "Left",
    "Right",
    "Home",
    "End",
    "PgUp",
    "PgDn",
    "Ctrl-Up",
    "Ctrl-Down",
    "Ctrl-Left",
    "Ctrl-Right",
    "Ctrl-Home",
    "Ctrl-End",
    "Ctrl-PgUp",
    "Ctrl-PgDn",
    "Alt-Up",
    "Alt-Down",
    "Alt-Left",
    "Alt-Right",
    "Alt-Home",
    "Alt-End",
    "Alt-PgUp",
    "Alt-PgDn",
    "Shift-Left",
    "Shift-Right",
    "Ctrl-Backspace",
    "Fn1",
    "Fn2",
    "Fn3",
    "Fn4",
    "Fn5",
    "Fn6",
    "Fn7",
    "Fn8",
    "Fn9",
    "Fn10",
    "Fn11",
    "Fn12",
    "Alt-A",
    "Alt-B",
    "Alt-C",
    "Alt-D",
    "Alt-E",
    "Alt-F",
    "Alt-G",
    "Alt-H",
    "Alt-I",
    "Alt-J",
    "Alt-K",
    "Alt-L",
    "Alt-M",
    "Alt-N",
    "Alt-O",
    "Alt-P",
    "Alt-Q",
    "Alt-R",
    "Alt-S",
    "Alt-T",
    "Alt-U",
    "Alt-V",
    "Alt-W",
    "Alt-X",
    "Alt-Y",
    "Alt-Z",
    "Backtab",
    "Delete",
    "Alt--",
    "Alt-=",
    "Alt-[",
    "Alt-]",
    "Alt-\\",
    "Alt-Space",
    "Alt-;",
    "Alt-'",
    "Alt-,",
    "Alt-.",
    "Alt-/",
    "None",
    "Blah"
  };
  unsigned    i;
  nedKey_t    key;
  unsigned    cols = 4;

  NedTestBegin();

  printf("\n");
  for(i=0; i<NumElements(aszNames); ++i)
  {
    if(i && ((i % cols) == 0))
      printf("\n");
    printf("%*s = key %3u  ", 14, aszNames[i], NedKeyFromName(aszNames[i]));
  }
  printf("\n");

  printf("\n%lu Keys. Examine names and keys above. Press 'p' for pass, 'f' for fail: ", NumElements(aszNames)-1);
  fflush(stdout);

  while(1)
  {
    key = NedKeyGetKey();
    if(key == 'p')
      break;
    else if(key == 'f')
      NedTestFailed();
  }

  NedTestEnd();

  NedKeyRawDisable();
  printf("%c\r\n", key);
}

static bool_t fInIdle = FALSE;
void NtkIdle(void)
{
  if(!fInIdle)
  {
    fInIdle = TRUE;
    printf("idle\n");
  }
}

/*!------------------------------------------------------------------------------------------------
  Test Case testing keyboard input

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcKeys(void)
{
  nedKey_t    key;

  NedTestBegin();

  NedKeySetIdle(NtkIdle);
  printf("\n\nTry various keys. When done, press 'p' for pass, 'f' for fail\n");
  while(1)
  {
    key = NedKeyGetKey();
    fInIdle = FALSE;
    printf("%d - %s\n", key, NedKeyName(key));
    if(key == 'p')
      break;
    else if(key == 'f')
      NedTestFailed();
  }

  NedTestEnd();
  NedKeyRawDisable();
}

int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcKeys",           TcKeys,           TC_USER_INPUT },
    { "TcCheckKeyNames",  TcCheckKeyNames,  TC_USER_INPUT }
  };
  hTestSuite_t        hSuite;
  int                 ret;
  int                 i;
  bool_t              fRaw   = FALSE;
  bool_t              fNames = FALSE;

  for(i=1; i<argc; ++i)
  {
    if(strcmp(argv[i], "--help") == 0)
    {
      printf("test_key [-raw] [-names]\n");
      exit(1);
    }
    if(strcmp(argv[i], "-raw") == 0)
      fRaw = TRUE;
    else if(strcmp(argv[i], "-names") == 0)
      fNames = TRUE;
  }
  NedTestInit(argc, argv);

  if(fRaw)
  {
    NedTestKeyRaw();
    exit(0);
  }
  else if(fNames)
  {
    NedTestKeyPrintNames();
    exit(0);
  }

  hSuite = NedTestNew("NedTestKey", NumElements(aTestCases), aTestCases);

  NedTestRun(hSuite);

  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
