/*!************************************************************************************************
  @file NedTestCmdsEmpty.c

  @brief  All the commands, empty (do nothing)

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @ingroup  ned_test

  Tests for Ned Settings. Loads the file nedproj.json

*///***********************************************************************************************

// list of all commands
void NedCmdBookmarkClearAll     (void){}
void NedCmdBookmarkNext         (void){}
void NedCmdBookmarkPrev         (void){}
void NedCmdBookmarkToggle       (void){}

void NedCmdDebug                (void){}
void NedCmdDoNothing            (void){}

void NedCmdCursorUp             (void){}
void NedCmdCursorDown           (void){}
void NedCmdCursorLeft           (void){}
void NedCmdCursorLeftSelect     (void){}
void NedCmdCursorRight          (void){}
void NedCmdCursorRightSelect    (void){}
void NedCmdCursorTop            (void){}
void NedCmdCursorBottom         (void){}
void NedCmdCursorHome           (void){}
void NedCmdCursorEnd            (void){}
void NedCmdCursorPgUp           (void){}
void NedCmdCursorPgDn           (void){}
void NedCmdCursorWordLeft       (void){}
void NedCmdCursorWordRight      (void){}

void NedCmdEditAutoComplete     (void){}
void NedCmdEditBackspace        (void){}
void NedCmdEditCopy             (void){}
void NedCmdEditCut              (void){}
void NedCmdEditCutEol           (void){}
void NedCmdEditCutLine          (void){}
void NedCmdEditCutWord          (void){}
void NedCmdEditDelete           (void){}
void NedCmdEditEnter            (void){}
void NedCmdEditIndent           (void){}
void NedCmdEditInsDateTime      (void){}
void NedCmdEditInsertSelf       (void){}
void NedCmdEditInsertSpecial    (void){}
void NedCmdEditPaste            (void){}
void NedCmdEditPbCopy           (void){}
void NedCmdEditPbPaste          (void){}
void NedCmdEditRedo             (void){}
void NedCmdEditSortLines        (void){}
void NedCmdEditToggleCase       (void){}
void NedCmdEditToggleComment    (void){}
void NedCmdEditUndent           (void){}
void NedCmdEditUndo             (void){}
void NedCmdEditWrapText         (void){}

void NedCmdExitSaveAll          (void){}
void NedCmdExitSaveAsk          (void){}
void NedCmdExitSaveNone         (void){}

void NedCmdFileBrowse           (void){}
void NedCmdFileClose            (void){}
void NedCmdFileCloseAll         (void){}
void NedCmdFileGotoChosen       (void){}
void NedCmdFileList             (void){}
void NedCmdFileNew              (void){}
void NedCmdFileNextOpen         (void){}
void NedCmdFilePrevOpen         (void){}
void NedCmdFileOpen             (void){}
void NedCmdFileOpenRecent       (void){}
void NedCmdFileOpenWildcard     (void){}
void NedCmdFileRevert           (void){}
void NedCmdFileSave             (void){}
void NedCmdFileSaveAs           (void){}
void NedCmdFileSaveAll          (void){}
void NedCmdFileToggleRdOnly     (void){}

void NedCmdHelp                 (void){}
void NedCmdHelpAbout            (void){}
void NedCmdHelpDateTime         (void){}

void NedCmdMacroRecord          (void){}
void NedCmdMacroPlay            (void){}

void NedCmdMenuFile             (void){}
void NedCmdMenuEdit             (void){}
void NedCmdMenuSearch           (void){}
void NedCmdMenuGoto             (void){}
void NedCmdMenuOption           (void){}
void NedCmdMenuWindow           (void){}
void NedCmdMenuHelp             (void){}

void NedCmdMoveCenter           (void){}
void NedCmdMoveErrNext          (void){}
void NedCmdMoveErrPrev          (void){}
void NedCmdMoveFunctionDown     (void){}
void NedCmdMoveFunctionList     (void){}
void NedCmdMoveFunctionUp       (void){}
void NedCmdMoveGotoLine         (void){}
void NedCmdMoveMatchBrace       (void){}
void NedCmdMovePosNext          (void){}
void NedCmdMovePosPrev          (void){}
void NedCmdMoveScrollUp         (void){}
void NedCmdMoveScrollDown       (void){}
void NedCmdMoveToDefinition     (void){}

void NedCmdProjectFolders       (void){}

void NedCmdRedrawScreen         (void){}

void NedCmdReplace              (void){}
void NedCmdReplaceProject       (void){}
void NedCmdSearchAgain          (void){}
void NedCmdSearchBackward       (void){}
void NedCmdSearchForward        (void){}
void NedCmdSearchOptions        (void){}
void NedCmdSearchProject        (void){}

void NedCmdSelectAll            (void){}
void NedCmdSelectLine           (void){}
void NedCmdSelectOff            (void){}
void NedCmdSelectToggle         (void){}
void NedCmdSelectWord           (void){}

void NedCmdSettingsCrLf         (void){}
void NedCmdSettingsEdit         (void){}
void NedCmdSettingsFactory      (void){}
void NedCmdSettingsMapKeys      (void){}
void NedCmdSettingsTab          (void){}
void NedCmdSettingsTheme        (void){}
void NedCmdSettingsWrap         (void){}
void NedCmdViewKeyBindings      (void){}

void NedEditorError(const char *szExpr, const char *szFile, const char *szFunc, unsigned line)
{
  (void)szExpr;
  (void)szFile;
  (void)szFunc;
  (void)line;
}
