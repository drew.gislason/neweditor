/*!************************************************************************************************
  @file NedTestFile.c

  @brief  Ned File Test Cases

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include <unistd.h>
#include "Ned.h"
#include "NedLog.h"
#include "NedTest2.h"
#include "NedBuffer.h"
#include "NedFile.h"

#define TCDATA_FILE_SIZE 32576    // size of tcdata_file.txt

/*---------------------------------------------------------------------------
  Helper function, creates a line of the given characters, 64 bytes.
  Newlines only for lines.
  Returns length of line.
---------------------------------------------------------------------------*/
static unsigned NtfWriteLine(FILE *fp, char c)
{
  unsigned i;
  for(i=0;i<63;++i)
    fwrite(&c, 1, 1, fp);
  fwrite("\n", 1, 1, fp);   // 63 + '\n' = 64 bytes
  return 64;
}

/*---------------------------------------------------------------------------
  Helper function, creates a 4K portion of a file.
  Returns length of 4K
---------------------------------------------------------------------------*/
static unsigned NtfWrite4K(FILE *fp, char c)
{
  unsigned  i;
  unsigned  len = 0;
  for(i=0;i<64;++i)
    len += NtfWriteLine(fp, c);
  return len;
}

/*-------------------------------------------------------------------------------------------------
  Helper function, Creates a file of 16K + 1 line. Returns length of file created
-------------------------------------------------------------------------------------------------*/
static unsigned NedFileTestWriteFile(const char *szFilename)
{
  FILE       *fp;
  unsigned    len = 0;

  fp = fopen(szFilename,"wb");
  if(fp)
  {
    len += NtfWrite4K(fp, 'a');
    len += NtfWrite4K(fp, 'b');
    len += NtfWrite4K(fp, 'c');
    len += NtfWrite4K(fp, 'd');
    len += NtfWriteLine(fp, 'e');
    fclose(fp);
  }
  return len;
}

/*-------------------------------------------------------------------------------------------------
  Compare two files.

  Returns 0 if same, -1 if something failed to open or file1 less, 1 if file1 greater than file2.
-------------------------------------------------------------------------------------------------*/
static int NtfCompareFiles(const char *szFileName1, const char *szFileName2)
{
  FILE   *fp;
  size_t  len;
  char   *szFile1;
  char   *szFile2;
  int     cmp;

  // load file1 as string
  fp = fopen(szFileName1, "r");
  if(!fp)
    return -1;
  fseek(fp, 0L, SEEK_END);
  len = ftell(fp);
  fseek(fp, 0L, SEEK_SET);
  szFile1 = malloc(len + 1);
  if(fread(szFile1, 1, len, fp) != len)
  {
    free(szFile1);
    fclose(fp);
    return -1;
  }
  szFile1[len] = '\0';
  fclose(fp);

  // load file2 as string
  fp = fopen(szFileName2, "r");
  if(!fp)
    return -1;
  fseek(fp, 0L, SEEK_END);
  len = ftell(fp);
  fseek(fp, 0L, SEEK_SET);
  szFile2 = malloc(len + 1);
  if(fread(szFile2, 1, len, fp) != len)
  {
    free(szFile1);
    free(szFile2);
    fclose(fp);
    return -1;
  }
  szFile2[len] = '\0';
  fclose(fp);

  // compare files
  cmp = strcmp(szFile1, szFile2);

  // free files
  free(szFile1);
  free(szFile2);

  // return comparison (-1 = less or failed, 0=same, 1=file1 > file2)
  return cmp;
}

/*!------------------------------------------------------------------------------------------------
  Test Case for NedFileUserFolder()

      const char   *NedFileUserFolder   (void);

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcFileHomeFolder(void)
{
  NedTestBegin();

  printf("\nDoes '%s' look like the home folder?\n", NedStrHomeFolder());
  if(!NedTestPassFail())
    NedTestFailed();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test Case for NedFileInfoGet and NedStrDateTime:

      bool_t        NedFileInfoGet      (sNedFileInfo_t *pInfo, const char *szPath);
      const char   *NedStrDateTime     (time_t time);

  @ingroup    ned_testcase
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcFileInfo(void)
{
  sNedFileInfo_t    sInfo;
  const char        szFilename[PATH_MAX]  = "tmpdata_file1.txt";
  const char        szCwd[PATH_MAX];

  NedTestBegin();

  getcwd((char *)szCwd, sizeof(szCwd));

  memset(&sInfo, 0, sizeof(sInfo));
  if(NedFileInfoGet(&sInfo, "nothere"))
    NedTestFailed();

  memset(&sInfo, 0, sizeof(sInfo));
  if(!NedFileInfoGet(&sInfo, ".") || (strcmp(sInfo.szFullPath, szCwd) != 0))
    NedTestFailed();

  memset(&sInfo, 0, sizeof(sInfo));
  if(!NedFileInfoGet(&sInfo, ".") || !sInfo.fIsDir)
    NedTestFailed();

  // create a file
  if(!NedFileTestWriteFile(szFilename))
    NedTestFailed();

  // verify it's the file
  memset(&sInfo, 0, sizeof(sInfo));
  if(!NedFileInfoGet(&sInfo, szFilename) || sInfo.fIsDir || sInfo.fIsModified)
    NedTestFailed();

  // verify filename is the same
  if((strcmp(NedStrNameOnly(sInfo.szFullPath), szFilename) != 0))
    NedTestFailed();

  // veriful full path is different
  if(strcmp(sInfo.szFullPath, szFilename) == 0)
    NedTestFailed();

  // write it again and verify it's modified
  printf("\nWait 2 seconds or so then press <enter> to test file date/time: ");
  getchar();
  if(!NedFileTestWriteFile(szFilename))
    NedTestFailed();

  // verify the file is modified
  if(!NedFileInfoGet(&sInfo, szFilename) || sInfo.fIsDir || !sInfo.fIsModified)
    NedTestFailed();

  // check date/time
  printf("Does this look like the current date/time (%s)?\n", NedStrDateTime(sInfo.modTime));
  if(!NedTestPassFail())
    NedTestFailed();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test Case for NedStrNameOnly(), NedStrDirOnly(), NedStrNameFit()

    const char   LastSlash    (const char *szFullPath);
    const char   *NedStrNameOnly     (const char *szFullPath);
    const char   *NedStrDirOnly      (const char *szFullPath);
    const char   *NedStrDirOnlyHome  (const char *szFullPath);
    const char   *NedStrNameFit      (const char *szFullPath, unsigned width);

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcFileNames(void)
{
  static char * aszPaths[] =
  {
    "/Users/drewg/Git/ned/test/NedTestFile.c",
    "/Users/drewg/Git/ned/src/",
    "~/Work/myfile.c",
    "C:\\work\\things\\hello world.py",
    "~tmpfile",
    "myfile.c"
  };
  static char * aszNameOnly[] =
  {
    "NedTestFile.c",
    "",
    "myfile.c",
    "hello world.py",
    "~tmpfile",
    "myfile.c"
  };
  static char * aszDirOnly[] =
  {
    "/Users/drewg/Git/ned/test/",
    "/Users/drewg/Git/ned/src/",
    "~/Work/",
    "C:\\work\\things\\",
    "",
    ""
  };
  static char * aszDirOnlyHome[] =
  {
    "/Users/drewg/Git/ned/test/",
    "/Users/drewg/Git/ned/src/",
    "/Users/drewg/Work/",
    "C:\\work\\things\\",
    "",
    ""
  };
  static int aFit[]      = { 21, 11, 14, 28, 7, 12 };
  static char * aszFit[] =
  {
    "...test/NedTestFile.c",
    "...ned/src/",
    "...rk/myfile.c",
    "...ork\\things\\hello world.py",
    "...file",
    "myfile.c"
  };

  unsigned      i;
  const char   *sz;
  char          szCwd[PATH_MAX];

  // setup filenames
  getcwd(szCwd, sizeof(szCwd));

  NedTestBegin();
  
  // test NedStrLastSlash()
  sz = NedStrLastSlash(aszPaths[0]);
  if(!sz || *sz != '/' || sz[1] != 'N')
    NedTestFailed();

  // test NedStrNameOnly()
  for(i=0; i<NumElements(aszPaths); ++i)
  {
    sz = NedStrNameOnly(aszPaths[i]);
    if(strcmp(sz, aszNameOnly[i]) != 0)
    {
      printf("Got '%s', should be '%s'\n", sz, aszNameOnly[i]);
      NedTestFailed();
    }
  }

  // test NedStrDirOnly()
  for(i=0; i<NumElements(aszPaths); ++i)
  {
    sz = NedStrDirOnly(aszPaths[i]);
    if(strcmp(sz, aszDirOnly[i]) != 0)
    {
      printf("Got '%s', should be '%s'\n", sz, aszDirOnly[i]);
      NedTestFailed();
    }
  }

  // test NedStrDirOnlyHome()
  for(i=0; i<NumElements(aszPaths); ++i)
  {
    sz = NedStrDirOnlyHome(aszPaths[i]);
    if(strcmp(sz, aszDirOnlyHome[i]) != 0)
    {
      printf("Got '%s', should be '%s'\n", sz, aszDirOnlyHome[i]);
      NedTestFailed();
    }
  }

  // test NedStrFitName()
  for(i=0; i<NumElements(aszPaths); ++i)
  {
    sz = NedStrFitName(aszPaths[i], aFit[i]);
    if(strcmp(sz, aszFit[i]) != 0)
    {
      printf("Got '%s', should be '%s'\n", sz, aszFit[i]);
      NedTestFailed();
    }
  }

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test Case for NedFileNew()

      sNedFile_t   *NedFileNew          (sNedFile_t *pFileList, const char *szPath);
      bool_t        NedFileIsFile       (const sNedFile_t *pFile);
      sNedFile_t    NedFileNext         (sNedFile_t *pFile);
      char         *NedFilePath         (sNedFile_t *pFile);

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcFileNew(void)
{
  static const char *aszPaths[] =
  {
    ".",
    "/folder/not/there",
    "/Users/drewg/Work/Git/ned/test",
    NULL,
    "~/Work/Git/ned/test/tcdata_file.txt",
    "newfile.c",
    NULL
  };
  static const char *aszExpectedPaths[] =
  {
    NULL,
    NULL,
    NULL,
    "/Users/drewg/Work/Git/ned/test/untitled.txt",
    "/Users/drewg/Work/Git/ned/test/tcdata_file.txt",
    "/Users/drewg/Work/Git/ned/test/newfile.c",
    "/Users/drewg/Work/Git/ned/test/untitled2.txt"
  };
  static long      aFileSizes[] = { 0, 0, 0, 0, TCDATA_FILE_SIZE, 0, 0 };
  sNedFile_t       *pFile;
  sNedFile_t       *pFileList = NULL;
  unsigned          i;
  long              fileSize;
  unsigned          expectedBufId;

  NedTestBegin();

  for(i=0; i<NumElements(aszPaths); ++i)
  {
    pFile = NedFileNew(pFileList, aszPaths[i]);
    if(pFile && !pFileList)
      pFileList = pFile;
    NedLogPrintf("  i %u, pFile %p, pFileList %p\n", i, pFile, pFileList);

    // did succeed or fail as expected?
    if((pFile && !aszExpectedPaths[i]) || (!pFile && aszExpectedPaths[i]))
    {
      NedLogPrintf("  Failure mismatch pFile %s ExpPath %s\n",
             pFile ? "Y" : "N", aszExpectedPaths[i] ? "Y" : "N");
      NedTestFailed();
    }

    if(pFile && !NedFileIsFile(pFile))
      NedTestFailed();

    // does name match expected?
    if(pFile && (strcmp(NedFilePath(pFile), aszExpectedPaths[i]) != 0))
    {
      NedLogPrintf("  Expected %s, got %s\n", aszExpectedPaths[i], NedFilePath(pFile));
      NedTestFailed();
    }

    // does size match expected?
    if(pFile)
    {
      fileSize = 99;
      NedBufChainSize(NedFileBuf(pFile), &fileSize);
      if(fileSize != aFileSizes[i])
      {
        NedLogPrintf("  file: %s, Expected size %ld, got %ld\n", NedFilePath(pFile), aFileSizes[i], fileSize);
        NedTestFailed();
      }
    }
  }

NedLogPrintf(" --- got here ---\n");

  // verify the expected files are linked
  if(!pFileList)
  {
    NedTestFailed();
  }
  else
  {
    pFile = pFileList;
    expectedBufId = 1;
    for(i=0; i<NumElements(aszPaths); ++i)
    {
      if(aszExpectedPaths[i] == NULL)
        continue;

      if(pFile)
      {
        NedLogPrintf("  i %u, buf id %u, fullpath %s, expected %s\n",
          i, NedBufId(NedFileBuf(pFile)), NedFilePath(pFile),
          aszExpectedPaths[i] ? aszExpectedPaths[i] : "NULL");
      }
      else
      {
        NedLogPrintf("  i %u, pFile is NULL!\n", i);
      }

      if(!pFile || (strcmp(NedFilePath(pFile), aszExpectedPaths[i]) != 0) || 
         (NedBufId(NedFileBuf(pFile)) != expectedBufId))
      {
        NedTestFailed();
      }
      else
      {
        pFile = NedFileNext(pFile);
        ++expectedBufId;
      }
    }
  }

  NedTestEnd();

  // done, close all the files
  NedFileFreeAll(pFileList);
}

/*!------------------------------------------------------------------------------------------------
  Test Case for NedFileExists()

      bool_t        NedFileExists       (const char *szPath);

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcFileExists(void)
{
  NedTestBegin();

  if(NedFileExists("."))
    NedTestFailed();

  if(NedFileExists("nothere"))
    NedTestFailed();

  if(!NedFileExists("tcdata_load.txt"))
    NedTestFailed();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test Case for NedFileExists()

      sNedFile_t   *NedFileFree        (sNedFile_t *pFileList, sNedFile_t *pFile);
      void          NedFileFreeAll     (sNedFile_t *pFileList);

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcFileClose(void)
{
  static char    *aszFiles[]  = { "file1", "tcdata_load.txt", "file2" };
  sNedFile_t     *pFile;
  sNedFile_t     *pFileList   = NULL;
  unsigned        i;

  NedTestBegin();

  // open three files
  for(i=0; i<NumElements(aszFiles); ++i)
  {
    pFile = NedFileNew(pFileList, aszFiles[i]);
    if(!pFile)
    {
      NedTestFailed();
    }

    else if(pFileList == NULL)
      pFileList = pFile;
  }

  // close the middle one, and verify the other 2 still exist
  if(!NedFileNext(pFileList) || !NedFileFree(pFileList, NedFileNext(pFileList)))
    NedTestFailed();

  // verify there are only two files
  if(!NedFileIsFile(pFileList) || !NedFileNext(pFileList) || NedFileNext(NedFileNext(pFileList)))
    NedTestFailed();

  // verify filenames
  pFile = NedFileNext(pFileList);
  if((strcmp(NedStrNameOnly(NedFilePath(pFileList)), aszFiles[0]) != 0) ||
     (strcmp(NedStrNameOnly(NedFilePath(pFile)), aszFiles[2]) != 0))
  {
    NedTestFailed();
  }


  NedTestEnd();

  // done, close all the files
  NedFileFreeAll(pFileList);
}

/*!------------------------------------------------------------------------------------------------
  Test Case for NedFileLoad()

    bool_t        NedFileLoad         (sNedFile_t *pFile);
    void          NedFileClear        (sNedFile_t *pFile);

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcFileLoad(void)
{
  sNedFile_t     *pFile;
  long            fileSize = 99;    // something not 0 or 16448

  NedTestBegin();

  pFile = NedFileNew(NULL, "tcdata_file.txt");
  if(!pFile)
    NedTestFailed();

  if(!NedBufChainSize(NedFileBuf(pFile), &fileSize) || (fileSize != TCDATA_FILE_SIZE))
    NedTestFailed();

  NedFileClear(pFile);
  if(!NedBufChainSize(NedFileBuf(pFile), &fileSize) || (fileSize != 0))
    NedTestFailed();

  if(NedFileFree(NULL, pFile) != NULL)
    NedTestFailed();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test Case for NedFileLoad(), verifies extra fields

    bool_t        NedFileLoad         (sNedFile_t *pFile);
    void          NedFileClear        (sNedFile_t *pFile);

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcFileLoadEx(void)
{
  sNedFile_t     *pFile;

  NedTestBegin();

  pFile = NedFileNew(NULL, "files/test.c");
  if(!NedFileIsFile(pFile))
    NedTestFailed();
  if(pFile->fHardTabs != FALSE || pFile->tabSize != 2 || pFile->fCrLf != FALSE || pFile->fRdOnly != FALSE)
    NedTestFailed();
  if(NedFileFree(NULL, pFile) != NULL)
    NedTestFailed();

  pFile = NedFileNew(NULL, "files/test_space_3.c");
  if(!NedFileIsFile(pFile))
    NedTestFailed();
  if(pFile->fHardTabs != FALSE || pFile->tabSize != 3 || pFile->fCrLf != FALSE || pFile->fRdOnly != FALSE)
    NedTestFailed();
  if(NedFileFree(NULL, pFile) != NULL)
    NedTestFailed();

  pFile = NedFileNew(NULL, "files/test_space_4.c");
  if(!NedFileIsFile(pFile))
    NedTestFailed();
  if(pFile->fHardTabs != FALSE || pFile->tabSize != 4 || pFile->fCrLf != FALSE || pFile->fRdOnly != FALSE)
    NedTestFailed();
  if(NedFileFree(NULL, pFile) != NULL)
    NedTestFailed();

  pFile = NedFileNew(NULL, "files/test_space_4_crlf.c");
  if(!NedFileIsFile(pFile))
    NedTestFailed();
  if(pFile->fHardTabs != FALSE || pFile->tabSize != 4 || pFile->fCrLf != TRUE || pFile->fRdOnly != FALSE)
    NedTestFailed();
  if(NedFileFree(NULL, pFile) != NULL)
    NedTestFailed();

  pFile = NedFileNew(NULL, "files/test_tab.c");
  if(!NedFileIsFile(pFile))
    NedTestFailed();
  if(pFile->fHardTabs != TRUE || pFile->tabSize != 0 || pFile->fCrLf != FALSE || pFile->fRdOnly != FALSE)
  {
    NedLogPrintf("fHardTabs %u, tabSize %u, fCrLf %u, fRdOnly %u\n", pFile->fHardTabs, pFile->tabSize, pFile->fCrLf, pFile->fRdOnly);
    NedTestFailed();
  }
  if(NedFileFree(NULL, pFile) != NULL)
    NedTestFailed();

  pFile = NedFileNew(NULL, "files/test");
  if(!NedFileIsFile(pFile))
    NedTestFailed();
  if(pFile->fHardTabs != FALSE || pFile->tabSize != 0 || pFile->fCrLf != FALSE || pFile->fIsBinary != TRUE || pFile->fRdOnly != TRUE)
  {
    NedLogPrintf("fHardTabs %u, tabSize %u, fCrLf %u, fRdOnly %u, fIsBinary\n", pFile->fHardTabs, pFile->tabSize, pFile->fCrLf, pFile->fRdOnly,
      pFile->fIsBinary);
    NedTestFailed();
  }
  if(NedFileFree(NULL, pFile) != NULL)
    NedTestFailed();

  pFile = NedFileNew(NULL, "files/rdonly.c");
  if(!NedFileIsFile(pFile))
    NedTestFailed();
  if(pFile->fHardTabs != FALSE || pFile->tabSize != 0 || pFile->fCrLf != FALSE || pFile->fRdOnly != TRUE)
  {
    NedLogPrintf("fHardTabs %u, tabSize %u, fCrLf %u, fRdOnly %u\n", pFile->fHardTabs, pFile->tabSize, pFile->fCrLf, pFile->fRdOnly);
    NedTestFailed();
  }
  if(NedFileFree(NULL, pFile) != NULL)
    NedTestFailed();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test Case for NedFileListNew(), NedFileIsList(), NedFileListFree(), NedFileListLen(),
  NedFileListGetName(), NedFileListIsDir()

      void         *NedFileListNew      (const char *pszWildPath);
      bool_t        NedFileIsList       (void *hList);
      void          NedFileListFree     (void *hList);
      void          NedFileListLen      (void *hList);
      const char   *NedFileListGetName  (void *hList, unsigned i);

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcFileList(void)
{
  void     *hList;
  unsigned  i;
  static char    *aszNames[] =
  {
    "tcdata_file.txt",
    "tcdata_hello.txt",
    "tcdata_load.txt",
    "tcdata_save.txt"
  };

  NedTestBegin();

  // test on a wildcard
  hList = NedFileListNew("tcdata_*.txt");
  if(!NedFileIsList(hList))
    NedTestFailed();

  // verify list is correct
  NedLogPrintf("  tcdata_*.txt, len %u\n", NedFileListLen(hList));
  if(NedFileListLen(hList) != NumElements(aszNames))
    NedTestFailed();

  for(i=0; i<NedFileListLen(hList); ++i)
  {
    NedLogPrintf("  name %s\n", NedFileListGetName(hList, i));
    if(strcmp(NedFileListGetName(hList, i), aszNames[i]) != 0)
      NedTestFailed();
  }
  NedFileListFree(hList);

  // test on a directory
  hList = NedFileListNew("*");
  if(!NedFileIsList(hList))
    NedTestFailed();

  NedLogPrintf("  *, len %u\n", NedFileListLen(hList));
  if(NedFileListLen(hList) < 5)
    NedTestFailed();

  for(i=0; i<NedFileListLen(hList); ++i)
  {
    NedLogPrintf("  name %s\n", NedFileListGetName(hList, i));
    if(NedFileListGetName(hList, i) == NULL)
      NedTestFailed();
  }
  
  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test Case for NedFileLoad() and NedFileSave() with regards to CRLF and LF endings.

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcFileCrLf(void)
{
  sNedFile_t     *pFile = NULL;

  NedTestBegin();

  // test loading a file with LF only
  pFile = NedFileNew(NULL, "tcdata_file_lf.html");
  if(pFile == NULL)
    NedTestFailed();
  if(pFile->fCrLf != FALSE)
    NedTestFailed();
  NedFileFree(NULL, pFile);

  // test loading a file with CRLF
  pFile = NedFileNew(NULL, "tcdata_file.html");
  if(pFile == NULL || pFile->pBufHead == NULL)
    NedTestFailed();
  if(pFile->fCrLf != TRUE)
    NedTestFailed();

  // verify no CR in the in-memory file (should be LF only, but with fCrLf set)
  if(pFile->pBufHead == NULL)
    NedTestFailed();
  if(NedBufSearch(pFile->pBufHead, '\r', NULL))
    NedTestFailed();

  // test saving load file as LF
  unlink("tmp.txt");
  NedFilePathRename(pFile, "tmp.txt");
  pFile->fCrLf = FALSE;
  if(!NedFileSave(pFile))
    NedTestFailed();
  if(NtfCompareFiles("tmp.txt", "tcdata_file_lf.html") != 0)
    NedTestFailed();

  // test saving load file as CRLF
  pFile->fCrLf = TRUE;
  if(!NedFileSave(pFile))
    NedTestFailed();
  if(NtfCompareFiles("tmp.txt", "tcdata_file.html") != 0)
    NedTestFailed();
  
  NedTestEnd();

  if(pFile)
    NedFileFree(NULL, pFile);
}

void TcFileNoDup(void)
{
  const char *aszFullPath[] = 
  {
    "/Users/drewg/test.c",
    "/Users/drewg/Work/Git/ned/src/test.c",
    "/Users/drewg/Work/Git/ned/src/test.h",
    "/test.c",
    "/Users/drewg/Work/Git/ned/test/test.c"
  };
  const char *aszDupPath[] =
  {
    "drewg/test.c",
    "src/test.c",
    "test.h",
    "/test.c",
    "test/test.c"
  };
  const char szDupFullPath[] = "/Users/drewg/Work/Git/ned/src/test.c";
  const char *aszDupLevel[] =
  {
    "test.c",
    "src/test.c",
    "ned/src/test.c",
    "Git/ned/src/test.c",
    "Work/Git/ned/src/test.c",
    "drewg/Work/Git/ned/src/test.c",
    "Users/drewg/Work/Git/ned/src/test.c",
    "/Users/drewg/Work/Git/ned/src/test.c"
  };
  const char   *psz;
  const char   *pszExpected;
  unsigned      i;
  sNedFile_t   *pFileList = NULL;
  sNedFile_t   *pFile;

  NedTestBegin();

  if(NumElements(aszFullPath) != NumElements(aszDupPath))
    NedTestFailed();

  // test NedStrPrevSlash()
  psz = NedStrNameOnly(szDupFullPath);
  for(i = 0; i < NumElements(aszDupLevel); ++i)
  {
    pszExpected = aszDupLevel[i];
    if(strcmp(pszExpected, psz) != 0)
    {
      NedLogPrintf("Dup %u: expected %s, got %s\n", i, pszExpected, psz);
      NedTestFailed();
    }
    psz = NedStrPrevSlash(szDupFullPath, psz);
  }

  // create the files with the special names
  for(i = 0; i < NumElements(aszFullPath); ++i)
  {
    pFile = NedFileNew(pFileList, NULL);
    if(!pFileList)
      pFileList = pFile;
    if(!pFile)
      NedTestFailed();
    strcpy(pFile->sInfo.szFullPath, aszFullPath[i]);
  }

  // verify they work out as the duplicate names
  pFile = pFileList;
  for(i = 0; i < NumElements(aszFullPath); ++i)
  {
    if(strcmp(NedFileNameNoDup(pFileList, pFile), aszDupPath[i]) != 0)
    {
      NedLogPrintf("%u: got %s, expected %s\n", i, NedFileNameNoDup(pFileList, pFile), aszDupPath[i]);
      NedTestFailed();
    }
    pFile = pFile->pNext;
  }

  NedTestEnd();

  NedFileFreeAll(pFileList);
}

/*-------------------------------------------------------------------------------------------------
  Test each function
*///-----------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcFileHomeFolder", TcFileHomeFolder, TC_USER_INPUT },
    { "TcFileInfo",       TcFileInfo, TC_USER_INPUT },
    { "TcFileNames",      TcFileNames  },
    { "TcFileNew",        TcFileNew    },
    { "TcFileExists",     TcFileExists },
    { "TcFileClose",      TcFileClose  },
    { "TcFileLoad",       TcFileLoad   },
    { "TcFileLoadEx",     TcFileLoadEx },
    { "TcFileList",       TcFileList   },
    { "TcFileCrLf",       TcFileCrLf   },
    { "TcFileNoDup ",     TcFileNoDup  }
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestFile", NumElements(aTestCases), aTestCases);
  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
