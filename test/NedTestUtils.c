/*!************************************************************************************************
  @file NedTestUtils.c

  @brief  Ned File Test Cases for Utility Functions

  @copyright Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @ingroup    ned_test
*///***********************************************************************************************
#include <unistd.h>
#include "Ned.h"
#include "NedLog.h"
#include "NedTest2.h"
#include "NedUtilFile.h"
#include "NedUtilStr.h"

/*!------------------------------------------------------------------------------------------------
  Test whether a file can be found in the path or not.

  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcUtilFindInPath(void)
{
  char      szPath[PATH_MAX];

  NedTestBegin();

  if(NedFileFindInPath(szPath, sizeof(szPath), "nothere", TRUE))
    NedTestFailed();

  if(!NedFileFindInPath(szPath, sizeof(szPath), "test_utils", TRUE))
    NedTestFailed();

  if(NedFileFindInPath(szPath, sizeof(szPath), "test_utils", FALSE))
    NedTestFailed();

  if(!NedFileFindInPath(szPath, sizeof(szPath), "gcc", FALSE))
    NedTestFailed();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test whether a file can be found in the folder tree or not.

  @ingroup    ned_test
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcUtilFindInFolder(void)
{
  char      szPath[PATH_MAX];

  NedTestBegin();

  if(NedFileFindInFolder(szPath, sizeof(szPath), "nothere", NULL))
    NedTestFailed();

  if(!NedFileFindInFolder(szPath, sizeof(szPath), "tcdata_hello.txt", NULL))
    NedTestFailed();

  if(!NedFileFindInFolder(szPath, sizeof(szPath), "test_utils", NULL))
    NedTestFailed();

  if(NedFileFindInFolder(szPath, sizeof(szPath), "test_utils", "/usr/bin"))
    NedTestFailed();

  if(!NedFileFindInFolder(szPath, sizeof(szPath), "diff", "/usr/bin"))
    NedTestFailed();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test NedFileInfoGet() function

  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcUtilInfoGet(void)
{
  sNedFileInfo_t  sInfo;

  NedTestBegin();

  if(!NedFileInfoGet(&sInfo, "files"))
    NedTestFailed();
  if(sInfo.fIsDir == FALSE || sInfo.fRdOnly == FALSE)
    NedTestFailed();

  if(!NedFileInfoGet(&sInfo, "files/test.c"))
    NedTestFailed();
  if(sInfo.fIsDir == TRUE || sInfo.fRdOnly == TRUE)
    NedTestFailed();

  if(!NedFileInfoGet(&sInfo, "files/rdonly.c"))
    NedTestFailed();
  if(sInfo.fIsDir == TRUE || sInfo.fRdOnly == FALSE)
    NedTestFailed();

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test the StrCase functions

  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcUtilStrCase(void)
{
  const char   *asz[]   = { "lowercase", "UPPERCASE", "camelCase", "MixedCase", "snake_case", "CONSTANT_CASE" };
  nedStrCase_t  aCase[] = { IS_LOWER_CASE, IS_UPPER_CASE, IS_CAMEL_CASE, IS_MIXED_CASE, IS_SNAKE_CASE, IS_CONSTANT_CASE };
  const char   *aszCase1[] = { "testone1", "TESTONE1" };
  nedStrCase_t  aCase1[]   = { IS_LOWER_CASE, IS_UPPER_CASE };
  const char   *aszCase2[] = { "testTwoOk", "TestTwoOk", "test_two_ok",  "TEST_TWO_OK",};
  nedStrCase_t  aCase2[]   = { IS_CAMEL_CASE, IS_MIXED_CASE, IS_SNAKE_CASE, IS_CONSTANT_CASE };
  unsigned      n;
  unsigned      next;
  unsigned      i;
  unsigned      len;
  char          szDst[16];

  NedTestBegin();

  // verioy case detection
  for(i=0; i<NumElements(asz); ++i)
  {
    if(NedStrIsCase(asz[i]) != aCase[i])
    {
      NedLogPrintf("%u: got case %u, expected case %u\n", i, NedStrIsCase(asz[i]), aCase[i]);
      NedTestFailed();
    }
  }

  // convert between lower and UPPPER and back
  n = NumElements(aszCase1);
  for(i = 0; i <= n; ++i)
  {
    next = (i + 1) % n;
    *szDst = '\0';
    len = NedStrToCase(szDst, aszCase1[i % n], sizeof(szDst)-1, aCase1[next]);
    NedLogPrintf("i = %u, next = %u, szDst %s\n", i, next, szDst);
    if(strcmp(szDst, aszCase1[next]) != 0 || len != strlen(szDst))
      NedTestFailed();
  }

  // convert between chamelCase, MixedCase, snake_case, CONSTANT_CASE
  n = NumElements(aszCase2);
  for(i = 0; i <= n; ++i)
  {
    next = (i + 1) % n;
    *szDst = '\0';
    len = NedStrToCase(szDst, aszCase2[i % n], sizeof(szDst)-1, aCase2[next]);
    NedLogPrintf("i = %u, next = %u, from %s to %s\n", i, next, aszCase2[i % n], szDst);
    if(strcmp(szDst, aszCase2[next]) != 0 || len != strlen(szDst))
      NedTestFailed();
  }

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test NedWCharToUtf8()
  See https://www.w3schools.com/charsets/ref_html_entities_d.asp

  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcUtilUtf8(void)
{
  const uint16_t  dagger = 0x2020; // HTML entity dagger, 
  const char      szExpected[] = { "\xE2\x80\xA0" };
  const char     *sz;

  NedTestBegin();

  sz = NedWCharToUtf8(dagger);
  if(memcmp(sz, szExpected, sizeof(szExpected)) != 0)
  {
    NedLogPrintf("FAIL TcUtilUtf8: expected %s, got %s\n", szExpected, sz);
    NedLogDump(szExpected, strlen(szExpected), 16, 0);
    NedLogDump(sz, strlen(sz), 16, 0);
    NedTestFailed();
  }

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Tests both NedMemFindWrap() and NedMemRemoveSpaces()

  Convert the string to single line paragraphs without extra spaces. If there is more than 1
  newline, then that is a new paragraph. A single newline is treated like a space

  input:  "  a  string\nof things\n\n  lifts   a\n   wing "
  output: "a string of things\nlifts a wing"

  input:  "string here  \n   \n   \n   "
  output: "string here\n\n"

  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcUtilWrap(void)
{
  const char    szExtraSpaces[] = " a  string\nof things\n\n  lifts   a\n   wing ";
  const char    szFewSpaces[]   = "a string of things\nlifts a wing";
  const char    szStringHere1[] = "here\t\ttoday  \n   \n   \n   ";
  const char    szStringHere2[] = "here today\n";
  char          sz[sizeof(szExtraSpaces)];
  size_t        len;

  NedTestBegin();

  strcpy(sz, szExtraSpaces);
  len = NedMemRemoveExtraSpaces((nedChar_t *)sz, strlen(sz));
  if(len != strlen(szFewSpaces) || strncmp(sz, szFewSpaces, len) != 0)
  {
    NedLogPrintf("\n!!ERR got len %zu, expected %zu, got sz \"%.*s\", expected %s\n",
      len, strlen(szFewSpaces), len, sz, szFewSpaces);
    NedLogDump(sz, len, 16, 2);
    NedTestFailed();
  }

  // try a different string
  strcpy(sz, szStringHere1);
  len = NedMemRemoveExtraSpaces((nedChar_t *)sz, strlen(sz));
  if(len != strlen(szStringHere2) || strncmp(sz, szStringHere2, len) != 0)
  {
    NedLogPrintf("\n!!ERR got len %zu, expected %zu, got sz \"%.*s\", expected \"%s\"\n",
      len, strlen(szStringHere2), len, sz, szStringHere2);
    NedLogDump(sz, len, 16, 2);
    NedTestFailed();
  }

  // wrap at 1 should be a word "string"
  len = NedMemFindWrap((const nedChar_t *)&szFewSpaces[2], strlen(szFewSpaces) - 2, 1);
  if(len != 6)
  {
    NedLogPrintf("!!ERR got len %zu, expected %zu\n", len, 6);
    NedTestFailed();
  }

  // wrap at "a string of things\nlifts a wing" should be 11 "a string of"
  len = NedMemFindWrap((const nedChar_t *)szFewSpaces, strlen(szFewSpaces), 14);
  if(len != 11)
  {
    NedLogPrintf("!!ERR got len %zu, expected %zu\n", len, 11);
    NedTestFailed();
  }

  // wrap at 14 "things\nlifts a wing" should be 11 "things"
  len = NedMemFindWrap((const nedChar_t *)&szFewSpaces[12], strlen(szFewSpaces) - 12, 14);
  if(len != 6)
  {
    NedLogPrintf("!!ERR got len %zu, expected %zu\n", len, 6);
    NedTestFailed();
  }

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Tests string array functions, NedUtilStrArrayMalloc(), etc...
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcUtilStrArray(void)
{
  const char   *asz[]  = { "a", "bc", "def", "ghij", "klmno", "pqrstuvwxyz", NULL };
  const char   *asz2[] = { "a", "bc", "def", "ghij", "klmno", NULL };
  const char   *asz3[] = { "a", "bc", "foo", "ghij", "klmno", "pqrstuvwxyz", NULL };
  const char  **ppsz   = NULL;
  int           ret;
  size_t        sizeOf;

  NedTestBegin();

  if(NedUtilStrArrayNumElem(asz) != 6)
    NedTestFailed();

  sizeOf = NedUtilStrArraySizeOf(asz);
  if(sizeOf != 32)
  {
    NedLogPrintf("got %zu, expected 32\n", sizeOf);
    NedTestFailed();
  }

  ppsz = (const char **)NedUtilStrArrayCopy(asz);
  if(ppsz == NULL || (void *)ppsz == (void *)asz)
    NedTestFailed();

  printf("\nasz\n"); NedUtilStrArrayPrint(asz);
  printf("\nppsz\n"); NedUtilStrArrayPrint(ppsz);

  if(strcmp(*ppsz, *asz) != 0)
    NedTestFailed();

  if(NedUtilStrArrayCmp(ppsz, asz) != 0)
    NedTestFailed();

  ret = NedUtilStrArrayCmp(asz2, ppsz);
  if(ret != -1)
  {
    printf("got ret %d, expected -1\n", ret);
    NedTestFailed();
  }

  ret = NedUtilStrArrayCmp(asz3, ppsz);
  if(ret != 1)
  {
    printf("got ret %d, expected 1\n", ret);
    NedTestFailed();
  }

  NedTestEnd();

  if(ppsz)
    NedUtilStrArrayFree(ppsz);
}

/*!------------------------------------------------------------------------------------------------
  Tests the home folder functions
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcUtilHome(void)
{
  char  szPath1[]         = "~/Doc*";
  char  szPath2[]         = "~nochange.txt";
  char  szHome[PATH_MAX]  = "";
  char  szPath[PATH_MAX]  = "";

  NedTestBegin();

  strcpy(szPath, szPath1);
  NedFileExpandHome(szPath, PATH_MAX);
  printf("szPath %s\n", szPath);
  NedFileGetHomeFolder(szHome, sizeof(szHome));
  if(!NedStrAppendName(szHome, "Doc*"))
    NedTestFailed();
  if(strcmp(szHome, szPath) != 0)
  {
    NedLogPrintf("szPath %s, szHome %s\n", szPath, szHome);
    NedTestFailed();
  }

  strcpy(szPath, szPath2);
  NedFileExpandHome(szPath, PATH_MAX);
  NedFileGetHomeFolder(szHome, sizeof(szHome));
  if(strncmp(szPath, szHome, strlen(szHome)) == 0)
  {
    NedLogPrintf("szPath %s, szHome %s\n", szPath, szHome);
    NedTestFailed();
  }

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Similar to cmdline tab which autocompletes file/folder names.

  For example, if the home folder contains the file Dodo.txt, and folders Desktop, Documents,
  Downloads, then tab will produce the following:

      ~/Desktop/
      ~/Downloads/

  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcUtilTabComplete(void)
{
  const char   *aszTcData[] = { "tcdata_util1.txt", "tcdata_util2.txt", "tcdata_util3.py" }; 
  unsigned      i;
  char          szCwd[PATH_MAX];
  char          szPath[PATH_MAX];
  void         *hTabComplete;

  NedTestBegin();

  hTabComplete = NedTabCompleteNew(sizeof(szPath));
  if(!hTabComplete)
    NedTestFailed();

  NedFileGetCwd(szPath, sizeof(szPath));
  strcat(szPath, "/tcdata_util");

  for(i = 0; i < NumElements(aszTcData); ++i)
  {
    if(!NedTabComplete(szPath, hTabComplete))
    {
      NedLogPrintf("i %u, expected success\n", i);
      NedTestFailed();
    }

    NedFileGetCwd(szCwd, sizeof(szCwd));
    strcat(szCwd, "/");
    strcat(szCwd, aszTcData[i]);

    if(strcmp(szPath, szCwd) != 0)
    {
      NedLogPrintf("szCwd %s\nszPath %s\n", szCwd, szPath);
      NedTestFailed();
    }
  }

  if(NedTabComplete(szPath, hTabComplete))
  {
    NedLogPrintf("expected only %u files, got more\n", NumElements(aszTcData));
    NedTestFailed();
  }

  NedTestEnd();

  // cleanup
  if(hTabComplete)
    NedTabCompleteFree(hTabComplete);
}

/*-------------------------------------------------------------------------------------------------
  Test each function
*///-----------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcUtilFindInPath",   TcUtilFindInPath  },
    { "TcUtilFindInFolder", TcUtilFindInFolder },
    { "TcUtilInfoGet",      TcUtilInfoGet },
    { "TcUtilUtf8",         TcUtilUtf8 },
    { "TcUtilStrCase",      TcUtilStrCase },
    { "TcUtilWrap",         TcUtilWrap },
    { "TcUtilStrArray",     TcUtilStrArray },
    { "TcUtilHome",         TcUtilHome },
    { "TcUtilTabComplete",  TcUtilTabComplete },
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestUtils", NumElements(aTestCases), aTestCases);
  NedLogMaskSet(NEDLOG_UTILS);
  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
