/********************************************************************************//**
  @file NedUtTheme.c

  @brief  Ned Theme Unit Test

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @ingroup  ned_theme   API for getting and setting color themes

  @ingroup  ned_theme

************************************************************************************/
#include "Ned.h"
#include "NedTheme.h"
#include "NedTest2.h"

#define cText           0
#define cTextHighlight  1
#define cComment        2
#define cKeyword        3
#define cConstant       4
#define cMenuText       5
#define cMenuHotkey     6
#define cMenuDropText   7
#define cMenuDropHotkey 8
#define cMessage        9

static unsigned   m_themeIndex = NEDTHEME_CLASSIC;
static nedTheme_t m_sTheme;

// set color to current theme, index
static void NttSetColor(unsigned colorIndex)
{
  nedAttr_t     color;

  if(m_themeIndex != NEDTHEME_ZERO)
  {
    switch(colorIndex)
    {
      case cText:
        color = m_sTheme.text;
      break;
      case cTextHighlight:
        color = m_sTheme.textHighlight;
      break;
      case cComment:
        color = m_sTheme.comment;
      break;
      case cKeyword:
        color = m_sTheme.keyword;
      break;
      case cConstant:
        color = m_sTheme.constant;
      break;
      case cMenuText:
        color = m_sTheme.menuText;
      break;
      case cMenuHotkey:
        color = m_sTheme.menuHotkey;
      break;
      case cMenuDropText:
        color = m_sTheme.menuDropText;
      break;
      case cMenuDropHotkey:
        color = m_sTheme.menuDropHotkey;
      break;
      case cMessage:
        color = m_sTheme.message;
      break;
      default:
        color = NEDATTR_RESET;
      break;
    }

    AnsiSetAttr(color);
  }
}

typedef struct
{
  unsigned        iAttr;
  const char *    psz;
} utText_t;

typedef struct
{
  unsigned        row;
  unsigned        col;
  unsigned        iAttr;
  const char *    psz;
} utMenuText_t;

void UtShowNedScreen(void)
{
  // File  Edit  Search  Goto  Optoins  Window  Help
  static utText_t aMenuLine[] =
  {
                              { cMenuHotkey, "F" },
    { cMenuText, "ile  E" },  { cMenuHotkey, "d" },
    { cMenuText, "it  " },    { cMenuHotkey, "S" },
    { cMenuText, "earch  " }, { cMenuHotkey, "G" },
    { cMenuText, "oto  " },   { cMenuHotkey, "O" },
    { cMenuText, "ptions  " },{ cMenuHotkey, "W" },
    { cMenuText, "indow  " }, { cMenuHotkey, "H" },
    { cMenuText, "elp" },
  };
  // About Ned
  // Ned Manual
  static utMenuText_t aMenu[] =
  {
    { 1, 43, cMenuText,         "+--------------------+" },
    { 2, 43, cMenuText,         "|"                      },
    { 2, 43+1, cMenuHotkey,      "A"                     },
    { 2, 43+2, cMenuText,         "bout Ned           |" },
    { 3, 43,   cMenuText,       "["                      },
    { 3, 43+1, cMenuDropText,    "Ned "                  },
    { 3, 43+5, cMenuDropHotkey,      "M"                 },
    { 3, 43+6, cMenuDropText,         "anual       Fn1"  },
    { 3, 43+21, cMenuText,                           "]" },
    { 4, 43, cMenuText,         "+--------------------+" }
  };

  static utText_t aMainScreen[] =
  {
    { cComment,  "/*\n  Ned File Test\n  Copyright (c) 2019\n*/\n\n" },
    { cKeyword,  "void" },
    { cText,     " PrintMessage(" },
    { cKeyword,  "char" },
    { cText,     " *szmsg)\n{\n  " },
    { cKeyword,  "if" },
    { cText,     "(!szMsg)\n    szMsg = " },
    { cConstant, "\"hello world\"" },
    { cText,     ";    " },
    { cComment,  "// default message\n" },
    { cText,     "  " },
    { cKeyword,  "printf" },
    { cText,     "(" },
    { cConstant, "\"%s\\n\"" },
    { cText,     ",szMsg);\n}\n\n" },
    { cTextHighlight,"void DoNothing(void)\n{\n  // nothing to do\n}\n\n" },
    { cKeyword,  "int" },
    { cText,     " main(void)\n{\n  PrintMessage(" },
    { cKeyword,  "NULL" },
    { cText,      ");\n  " },
    { cKeyword,  "return" },
    { cText,     " " },
    { cConstant, "0" },
    { cText,     ";\n}" }
  };
  static char aLineStatus[] = "[*][Sel]  12345:1234  Ned v1.00";

  unsigned rows;
  unsigned cols;
  unsigned col;
  unsigned i;

  // clear the screen
  AnsiGetRowsCols(&rows, &cols);
  NttSetColor(cText);
  AnsiClearScreen();

  // menu line
  for(i=0; i<sizeof(aMenuLine)/sizeof(aMenuLine[0]); ++i)
  {
    NttSetColor(aMenuLine[i].iAttr);
    printf("%s",aMenuLine[i].psz);
  }
  NttSetColor(cMenuText);
  AnsiClearEol();

  // some text
  AnsiGoto(2, 0);
  for(i=0; i<sizeof(aMainScreen)/sizeof(aMainScreen[0]); ++i)
  {
    NttSetColor(aMainScreen[i].iAttr);
    printf("%s",aMainScreen[i].psz);
  }

  // a menu
  for(i=0; i<sizeof(aMenu)/sizeof(aMenu[0]); ++i)
  {
    AnsiGoto(aMenu[i].row, aMenu[i].col);
    NttSetColor(aMenu[i].iAttr);
    printf("%s",aMenu[i].psz);
  }

  // status line (same color as menubar)
  AnsiGoto(rows-2, 0);
  NttSetColor(cMenuText);
  col = printf("file.c");
  printf("%*s",cols - col, aLineStatus);

  // message line
  AnsiGoto(rows-1, 0);
  NttSetColor(cMessage);
  printf("Theme %s. Press <Enter> if OK, or 'f<Enter>' if theme looks wrong: ", NedThemeGetName(m_themeIndex));
}

/*!------------------------------------------------------------------------------------------------
  Test all themes. User must visually check and determine if they are correct

  @return   return theme pointer or NULL if index out of range
*///-----------------------------------------------------------------------------------------------
void TcThemeView(void)
{
  int           c;
  unsigned      rows;
  unsigned      cols;
  nedTheme_t   *pTheme;

  NedTestBegin();

  AnsiGetRowsCols(&rows, &cols);
  if((rows < NEDMIN_ROWS) || (cols < NEDMIN_COLS))
  {
    printf("Need at least %u rows and %u columns for this test\n", NEDMIN_ROWS, NEDMIN_COLS);
    NedTestFailed();
  }

  // user visual inspection of all themes
  else
  {
    NedThemeInit();

    m_themeIndex = 0;
    while(TRUE)
    {
      pTheme = NedThemeGet(m_themeIndex);
      if(pTheme == NULL)
        break;
      m_sTheme = *pTheme;
      if(m_themeIndex == NEDTHEME_ZERO)
        AnsiSetAttr(NEDATTR_RESET);
      UtShowNedScreen();
      c = getchar();
      if(c == 'f')
      {
        AnsiSetAttr(NEDATTR_RESET);
        AnsiClearScreen();
        NedTestFailed();
      }
      ++m_themeIndex;
    }

    AnsiSetAttr(NEDATTR_RESET);
    AnsiClearScreen();
  }

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Tests set and get function.

  @return   return theme pointer or NULL if index out of range
*///-----------------------------------------------------------------------------------------------
void TcThemeSet(void)
{
  nedTheme_t   *pTheme;
  nedTheme_t    sTheme;
  unsigned      i;
  const char   *szName = "Hello";
  const char   *psz;

  NedTestBegin();

  NedThemeInit();

  // verify theme exists and is a default theme
  pTheme = NedThemeGet(NEDTHEME_CLASSIC);
  if(pTheme == NULL)
    NedTestFailed();
  pTheme = NedThemeGet(NEDTHEME_ZERO);
  if(pTheme == NULL)
    NedTestFailed();
  if(!NedThemeIsDefault(pTheme))
    NedTestFailed();

  // verify we can get names
  psz = NedThemeGetName(NEDTHEME_CLASSIC);
  if(psz == NULL || strcmp(psz, "Classic") != 0)
    NedTestFailed();
  psz = NedThemeGetName(NEDTHEME_ZERO);
  if(psz == NULL || strcmp(psz, "Zero") != 0)
    NedTestFailed();

  // verify theme doesn't exist (yet)
  pTheme = NedThemeGet(NEDTHEME_ZERO + 1);
  if(pTheme != NULL)
    NedTestFailed();
  i = NedThemeGetIndex(szName);
  if(i != NEDTHEME_NOT_FOUND)
    NedTestFailed();

  // set a new theme and verify it exists
  memset(&sTheme, 63, sizeof(sTheme));
  if(!NedThemeSet(szName, &sTheme))
    NedTestFailed();
  i = NedThemeGetIndex(szName);
  if(i == NEDTHEME_NOT_FOUND)
    NedTestFailed();
  pTheme = NedThemeGet(i);
  if(pTheme == NULL)
    NedTestFailed();
  if(NedThemeIsDefault(pTheme))
    NedTestFailed();
  psz = NedThemeGetName(i);
  if(psz == NULL || strcmp(psz, szName) != 0)
    NedTestFailed();

  // change an existing theme and verify it's no longer the default
  pTheme = NedThemeGet(NEDTHEME_ZERO);
  if(pTheme == NULL)
    NedTestFailed();
  if(!NedThemeIsDefault(pTheme))
    NedTestFailed();
  sTheme = *pTheme;
  sTheme.constant = 47;
  if(!NedThemeSet("Zero", &sTheme))
    NedTestFailed();
  pTheme = NedThemeGet(NEDTHEME_ZERO);
  if(NedThemeIsDefault(pTheme))
    NedTestFailed();

  NedTestEnd();
}

/*---------------------------------------------------------------------------
  Test each function
---------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcThemeView",  TcThemeView, TC_SCREEN | TC_USER_INPUT },
    { "TcThemeSet",   TcThemeSet }
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestTheme", NumElements(aTestCases), aTestCases);
  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}

