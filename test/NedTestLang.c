/*!************************************************************************************************
  @file NedTestLang.c

  @brief  Ned Language Test Cases

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include "Ned.h"
#include "NedLog.h"
#include "NedTest2.h"
#include "NedLanguage.h"
#include "NedUtilStr.h"

/*!------------------------------------------------------------------------------------------------
  Tests language get

      void                    NedLangInit(void);
      unsigned                NedLangMaxLanguages(void);
      sNedLanguage_t const *  NedLangGet(const char *szFilename);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcLangGet(void)
{
  typedef struct
  {
    char *szFilename;
    char *szLang;
  } testLangGet_t;
  const testLangGet_t aTests[] =
  {
    { "test.c",             "C" },
    { "test.cpp",           "C++" },
    { "~/Work/home.htm",    "Html" },
    { "homepage.html",      "Html" },
    { "thing.java",         "Java" },
    { "webpage.js",         "JavaScript" },
    { "webpage_style.css",  "JavaScript" },
    { "Makefile",           "Makefile" },
    { "numpy.py",           "Python" },
    { "myscript.rb",        "Ruby" },
    { "rustica.rs",         "Rust" },
    { "myscript.sh",        "Shell" },
    { "myscript.swift",     "Swift" },
    { "mydoc.md",           "Text" },
    { "songs.txt",          "Text" },
    { "unknown.no",         "Text" }
  };
  const sNedLanguage_t  *pLang;
  unsigned      i;

  NedTestBegin();

  // check max languages
  NedLangInit();

  // should match name always
  for(i=0; i<NumElements(aTests); ++i)
  {
    pLang = NedLangGet(aTests[i].szFilename);
    if(pLang == NULL)
    {
      NedLogPrintf("Not found, should always be a default language\n");
      NedTestFailed();
    }
    if(strcmp(pLang->szLang, aTests[i].szLang) != 0)
    {
      NedLogPrintf("%u) %s did not match %s for file %s\n", i, pLang->szLang, aTests[i].szLang, aTests[i].szFilename);
      NedTestFailed();
    }
  }

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test Language set function.

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcLangGetIndexed(void)
{
  const char * szLangs[] = 
  {
    SZ_LANG_C, SZ_LANG_CPP, SZ_LANG_HTML, SZ_LANG_JAVA, SZ_LANG_JAVASCRIPT, SZ_LANG_JSON,
    SZ_LANG_MAKEFILE, SZ_LANG_PYTHON, SZ_LANG_RUBY, SZ_LANG_RUST, SZ_LANG_SHELL, SZ_LANG_SWIFT,
    SZ_LANG_TEXT
  };
  const sNedLanguage_t   *pLang;
  unsigned                i;

  NedTestBegin();

  NedLangInit();

  for(i=0; i<NEDCFG_MAX_LANG; ++i)
  {
    pLang = NedLangGetIndexed(i);
    if(pLang == NULL)
      break;
    NedLogPrintf("  found lang %s\n", pLang->szLang);
    if(strcmp(pLang->szLang, szLangs[i]) != 0)
    {
      NedLogPrintf("  lang didn't match. Expected %s, got %s\n", szLangs[i], pLang->szLang);
      NedTestFailed();
    }
  }

  if(i != NumElements(szLangs))
  {
    NedLogPrintf("  Expected %lu langs, got %u\n", NumElements(szLangs), i);
    NedTestFailed();
  }

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test Language set function.
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcLangSet(void)
{
  const char *aszJavaScriptLineComments[]   = { NULL };
  const char *aszJavaScriptBlockComments[]  = { NULL };
  const char *aszJavaScriptKeyWords[]       = { NULL };
  const sNedLanguage_t sLangJavaScript =
  {
    .szLang           = "JavaScript",
    .szExt            = ".js .css .jv .jjj",
    .tabSize          = 3,
    .fHardTabs        = FALSE,
    .fCrLf            = FALSE,
    .wrapLen          = 99,
    .aszLineComments  = aszJavaScriptLineComments,
    .aszBlockComments = aszJavaScriptBlockComments,
    .aszKeywords      = aszJavaScriptKeyWords
  };
  const char *aszFooLineComments[]   = { "@", NULL };
  const char *aszFooBlockComments[]  = { "<!", "!>", NULL };
  const char *aszFooKeyWords[]       = {"foo", "bar", "baz", NULL };
  const sNedLanguage_t sLangFoo =
  {
    .szLang           = "Foo",
    .szExt            = ".foo .bar",
    .tabSize          = 5,
    .fHardTabs        = FALSE,
    .fCrLf            = FALSE,
    .wrapLen          = 100,
    .aszLineComments  = aszFooLineComments,
    .aszBlockComments = aszFooBlockComments,
    .aszKeywords      = aszFooKeyWords
  };
  const sNedLanguage_t   *pLang;
  const sNedLanguage_t   *pLang2;

  NedTestBegin();

  NedLangInit();

  // look for non-existing language, should default to C/C++
  pLang  = NedLangGet("a.not");
  if(strcmp(pLang->szLang, "Text") != 0)
  {
    NedLogPrintf("  Found unexpected .not\n");
    NedTestFailed();
  }

  // change Javascript to our version and see if it worked
  pLang2 = &sLangJavaScript;
  if(!NedLangSet(pLang2))
  {
    NedLogPrintf("  Failed to set JavaScript\n");
    NedTestFailed();
  }
  pLang = NedLangGet("a.jjj");

  if(strcmp(pLang->szLang, pLang2->szLang) != 0)
  {
    NedLogPrintf("  Failed to find extention .jjj\n");
    NedTestFailed();
  }

  if((pLang->tabSize != pLang2->tabSize) || (pLang->wrapLen != pLang2->wrapLen))
  {
    NedLogPrintf("  Bad table data for JavaScript\n");
    NedTestFailed();
  }

  // add foo and see if it worked
  pLang2 = &sLangFoo;
  if(!NedLangSet(pLang2))
  {
    NedLogPrintf("  Failed to set foo\n");
    NedTestFailed();
  }
  pLang  = NedLangGet("foo.bar");

  if(strcmp(pLang->szLang, pLang2->szLang) != 0)
  {
    NedLogPrintf("  Failed to find foo\n");
    NedTestFailed();
  }

  if((pLang->tabSize != pLang2->tabSize) || (pLang->wrapLen != pLang2->wrapLen))
  {
    NedLogPrintf("  Bad table data for foo\n");
    NedTestFailed();
  }

  if((NedUtilStrArrayCmp(pLang->aszLineComments, aszFooLineComments) != 0) ||
     (NedUtilStrArrayCmp(pLang->aszBlockComments, aszFooBlockComments) != 0) ||
     (NedUtilStrArrayCmp(pLang->aszKeywords, aszFooKeyWords) != 0))
  {
    NedTestFailed();
  }

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test Language isSame function.
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcLangSame(void)
{
  const sNedLanguage_t  *pLang1;
  const sNedLanguage_t  *pLang2;
  sNedLanguage_t         sLang;
  size_t                 numElem;

  NedTestBegin();
  NedLangInit();

  pLang1 = NedLangGet("test.c");
  if(!pLang1)
    NedTestFailed();
  pLang2 = NedLangGet("test.c");
  if(!pLang2)
    NedTestFailed();

  if(!NedLangIsSame(pLang1, pLang2))
    NedTestFailed();

  sLang = *pLang1;
  if(!NedLangIsSame(&sLang, pLang1))
    NedTestFailed();

  sLang.fHardTabs = TRUE;
  if(NedLangIsSame(&sLang, pLang1))
    NedTestFailed();

  // remove last keyword, should not be the same
  sLang.fHardTabs = pLang1->fHardTabs;
  sLang.aszKeywords = (const char **)NedUtilStrArrayCopy(pLang1->aszKeywords);
  numElem = NedUtilStrArrayNumElem(sLang.aszKeywords);
  if(numElem != NedUtilStrArrayNumElem(pLang1->aszKeywords))
    NedTestFailed();
  sLang.aszKeywords[numElem - 1] = NULL;
  if(NedLangIsSame(&sLang, pLang1))
    NedTestFailed();

  NedTestEnd();
}

/*-------------------------------------------------------------------------------------------------
  Test each function
-------------------------------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcLangGet",          TcLangGet  },
    { "TcLangGetIndexed",   TcLangGetIndexed },
    { "TcLangSet",          TcLangSet },
    { "TcLangSame",         TcLangSame }
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestLang", NumElements(aTestCases), aTestCases);
  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
