/*!************************************************************************************************
  @file NedTestAssert.c

  @brief NedTestAssert is an assert to use that doesn't rely on NedWin() functions.

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include <stdio.h>
#include <stdlib.h>
#include "Ned.h"

/*!------------------------------------------------------------------------------------------------
  An Assert or Error has occurred. Display it to logfile if open and to screen and put keyboard
  back to normal and exit.

  @ingroup  ned_utils

  @param    rows   
  @param    cols   

  @return   none
*///-----------------------------------------------------------------------------------------------
void NedError(const char *szExpr, const char *szFile, const char *szFunc, unsigned line)
{
  printf("NedAssert: (%s), file: %s, func: %s(), line: %u\n",szExpr, szFile, szFunc, line);
  exit(1);
}
