/*!************************************************************************************************
  @file NedTestSig.c

  @brief  Ned Signal Test Cases

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include <signal.h>
#include "Ned.h"
#include "NedTest2.h"
#include "NedSignal.h"

static const char * m_szProgName;
static int          m_gotSig;
static hTestSuite_t m_hSuite;

int TestSigCallback(int sig)
{
  int ret = 1;
  m_gotSig = sig;
  if(sig == SIGSEGV)
  {
    NT_Passed();
    ret = NedTestSummary(m_hSuite);
    _Exit(ret);
  }
  return 0;
}

void TestSigFunc2(char *p)
{
  NedSigSegFault(p);  
}

void TestSigFunc1(char *p)
{
  TestSigFunc2(p);
}

/*!------------------------------------------------------------------------------------------------
  Test MemAlloc

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcSignal(void)
{
  char    *p = NULL;

  NedTestBegin();

  // cause a segment fault (NULL ptr assignment)
  m_gotSig = 0;
  NedSigSetExit(m_szProgName, TestSigCallback);
  TestSigFunc1(p);
  if(m_gotSig != SIGSEGV)
    NedTestFailed();

  NedTestEnd();
}

/*-------------------------------------------------------------------------------------------------
  Test each function
*///-----------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcSignal",     TcSignal   }
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestSignal", NumElements(aTestCases), aTestCases);

  m_szProgName = argv[0];
  m_hSuite     = hSuite;

  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
