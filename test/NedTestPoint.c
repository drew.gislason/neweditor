/*!************************************************************************************************
  @file NedTestPoint.c

  @brief  Test Ned Point functions

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @ingroup  ned_test

  This file tests the NedPoint subsystem. For most test cases the buffer size is 64 bytes and line
  sizes are 8 bytes. Internal functions use NptXxxx(), Test cases use TcPointXxxx().

*///***********************************************************************************************
#include <ctype.h>
#include "NedPoint.h"
#include "NedTest2.h"
#include "NedLog.h"

// Ned Point Test options for NptFileCreate()
typedef uint8_t nptNedPtOpts_t;
#define NPT_OPTS_NONE      0x00
#define NPT_OPTS_ENDLF     0x01    // Final LF or not
#define NPT_OPTS_CRLF      0x02    // CR/LF or just LF line endings
#define NPT_OPTS_MIDSHORT  0x04    // mid buffers are short (or not)
#define NPT_OPTS_RANDLINE  0x08    // random line lengths from 0-linelen

#define NPT_BUFSIZE          16    // 16 byte buffers (most tests)
#define NPT_LINELEN           8    // 8 byte line lengths (most tests)

#define NPT_BEG "beg\n"
#define NPT_END "end\n"


static void NptFileDestroy(sNedBuffer_t *pBufHead);


static char szNptFile[] =
  "#include <stdio.h>  \r\n"
  "\n"
  "int main(void) {\n"
  "\t  printf(\"hello world\");\n"
  "  return 0;\n"
  "}\n";

static char szNptErrFile[] =
  "File.c:10:3: error: use of undeclared identifier 'ptrdiff_t'\n"
  "  ptrdiff_t   diff1 = (void *)(&iValue) - (void *)(&uValue);\n"
  "  ^\n"
  "File.c:11:5: error: use of undeclared identifier 'ptrdiff_t'\n"
  "  ptrdiff_t   diff2 = (void *)(&uValue) - (void *)(&iValue);\n"
  "  ^\n"
  "File.c:103:43: error: use of undeclared identifier 's'\n"
  "  printf(\"ssize_t  len %zu, value %zd\\n\", s izeof(iValue), iValue);\n"
  "                                          ^\n"
  "File2.c:15:36: error: use of undeclared identifier 'diff1'\n"
  "  printf(\"diff1 %ld, diff2 %td\\n\", diff1, diff2);\n"
  "                                   ^\n"
  "File2.c:15:43: error: use of undeclared identifier 'diff2'\n"
  "  printf(\"diff1 %ld, diff2 %td\\n\", diff1, diff2);\n"
  "                                          ^\n"
  "5 errors generated.\n";


/*
unsigned prototypeA(void);
unsigned prototypeB(void);
  
class Car {
  public:
    string          brand;
    string          model;
    unsigned long   year;
    Car(string _model, _string brand, _year);
};

Car::::Car(string _model, _string brand, _year)
{
  brand = _brand;
  model = _model;
  year  = _year;
}

int main(void) {
  Car carObj1("Honda", "Fit", 2015);
  Car carObj2("Ford", "Mustang", 1969);
  return 0;
}
*/
static char szNptFuncFile[] =
  "unsigned prototypeA(void);\n"
  "unsigned prototypeB(void);\n"
  "\n"
  "class Car {\n"
  "  public:\n"
  "    string          brand;\n"
  "    string          model;\n"
  "    unsigned long   year;\n"
  "    Car(string _model, _string brand, _year);\n"
  "};\n"
  "\n"
  "Car::Car(string _model, _string brand, _year)\n"
  "{\n"
  "  brand = _brand;\n"
  "  model = _model;\n"
  "  year  = _year;\n"
  "}\n"
  "\n"
  "int main(void) {\n"
  "  Car carObj1(\"Honda\", \"Fit\", 2015);\n"
  "  Car carObj2(\"Ford\", \"Mustang\", 1969);\n"
  "  return 0;\n"
  "}\n";

/*
  Note: SublimeText wraps things wrong!!! Had to fix in test suite!

  Every good boy does fine. Every good boy does fine. Every good boy does fine. Every good boy does
  fine. Every good boy does fine. Every good boy does fine. Every good boy does fine. Every good boy
  does fine. Every good boy does fine. Every good boy does fine. Every good boy does fine. Every
  good boy does fine.

  The quick brown fox jumped over the lazy dog. The quick brown fox jumped over the lazy dog. The
  quick brown fox jumped over the lazy dog. The quick brown fox jumped over the lazy dog. The quick
  brown fox jumped over the lazy dog. The quick brown fox jumped over the lazy dog. The quick brown
  fox jumped over the lazy dog.

    Every good boy does fine. Every good boy does fine. Every good boy
    does fine. Every good boy does fine. Every good boy does fine.
    Every good boy does fine. Every good boy does fine. Every good boy
    does fine. Every good boy does fine. Every good boy does fine.
    Every good boy does fine. Every good boy does fine.

    The quick brown fox jumped over the lazy dog. The quick brown fox
    jumped over the lazy dog. The quick brown fox jumped over the lazy
    dog. The quick brown fox jumped over the lazy dog. The quick brown
    fox jumped over the lazy dog. The quick brown fox jumped over the
    lazy dog. The quick brown fox jumped over the lazy dog.
*/
static char szNptWrappedFile1[] = 
  NPT_BEG
  "  Every good boy does fine. Every good boy does fine. Every good boy does fine. Every good boy does\n"
  "  fine. Every good boy does fine. Every good boy does fine. Every good boy does fine. Every good boy\n"
  "  does fine. Every good boy does fine. Every good boy does fine. Every good boy does fine. Every\n"
  "  good boy does fine.\n"
  "\n"
  "  The quick brown fox jumped over the lazy dog. The quick brown fox jumped over the lazy dog. The\n"
  "  quick brown fox jumped over the lazy dog. The quick brown fox jumped over the lazy dog. The quick\n"
  "  brown fox jumped over the lazy dog. The quick brown fox jumped over the lazy dog. The quick brown\n"
  "  fox jumped over the lazy dog.\n"
  NPT_END;

static char szNptWrappedFile2[] = 
  NPT_BEG
  "    Every good boy does fine. Every good boy does fine. Every good boy\n"
  "    does fine. Every good boy does fine. Every good boy does fine.\n"
  "    Every good boy does fine. Every good boy does fine. Every good boy\n"
  "    does fine. Every good boy does fine. Every good boy does fine.\n"
  "    Every good boy does fine. Every good boy does fine.\n"
  "\n"
  "    The quick brown fox jumped over the lazy dog. The quick brown fox\n"
  "    jumped over the lazy dog. The quick brown fox jumped over the lazy\n"
  "    dog. The quick brown fox jumped over the lazy dog. The quick brown\n"
  "    fox jumped over the lazy dog. The quick brown fox jumped over the\n"
  "    lazy dog. The quick brown fox jumped over the lazy dog.\n"
  NPT_END;


/*-------------------------------------------------------------------------------------------------
  Creates a chain of buffers, no content. Each buffer will be of bufSize.
  Returns NULL if failed, pBufHead if worked.
-------------------------------------------------------------------------------------------------*/
void NptPrintPoint(const char *szName, sNedPoint_t *pPoint)
{
  printf("%s: pBuf=%p, offset=%u\n",szName, pPoint->pBuf, pPoint->offset);
}

/*-------------------------------------------------------------------------------------------------
  Creates a chain of buffers, no content. Each buffer will be of bufSize.
  Returns NULL if failed, pBufHead if worked.
-------------------------------------------------------------------------------------------------*/
static sNedBuffer_t * NptBufCreate(unsigned id, unsigned fileSize, unsigned bufSize)
{
  sNedBuffer_t   *pBufHead    = NULL;
  NedBufSetMaxLen(bufSize);
  pBufHead = NedBufNew(id);
  if(pBufHead && fileSize)
  {
    if( !NedBufGrow(pBufHead, 0, fileSize) )
    {
      NedBufChainFree(pBufHead);
      pBufHead = NULL;
    }
  }
  return pBufHead;
}

/*-------------------------------------------------------------------------------------------------
  Get the entire buffer chain into a asciiz string. Returns NULL if something went wrong
-------------------------------------------------------------------------------------------------*/
static char * NtpGetAllText(const sNedPoint_t *pPoint)
{
  sNedPoint_t   sPointTop;
  char         *psz = NULL;
  long          len;

  sPointTop = *pPoint;
  NedPointFileTop(&sPointTop);
  len = NedPointFileSize(&sPointTop);
  if(len)
  {
    psz = malloc(len + 1);
    if(psz)
    {
      NedPointMemGet((uint8_t *)psz, &sPointTop, len);
      psz[len] = '\0';
    }
  }

  return psz;
}

/*!------------------------------------------------------------------------------------------------
  Loads a "file" from a NULL terminated string into a buffer chain.

  @ingroup  ned_test
  @param    id          Identifier for buffer chain
  @param    bufSize     size of each buffer in the chain
  @param    szFile      NULL terminated string

  @returns   pointer to bufHead of buffer chain, or NULL if failed.
*///-----------------------------------------------------------------------------------------------
static sNedBuffer_t * NtpFileLoads(unsigned id, unsigned bufSize, char *szFile)
{
  sNedBuffer_t   *pBufHead    = NULL;

  NedBufSetMaxLen(bufSize);
  pBufHead = NedBufNew(id);
  if( pBufHead && !NedBufInsert(pBufHead, 0, (void *)szFile, strlen(szFile)) )
  {
    NptFileDestroy(pBufHead);
    pBufHead = NULL;
  }

  return pBufHead;
}

/*!------------------------------------------------------------------------------------------------
  Creates a variety of buffer chains for testing the point functions.

      No options (fileSize=40, bufSize= 6, lineLen=8, opts=0)
      1   1111111n     Lines end in \n only
          2222222n
      2   3333333n
          4444444n
      3   55555555

      last line has /n (fileSize=47, bufSize=16, lineLen=8, opts=NPT_OPTS_ENDLF)
      1   1111111n     Lines end in \n only
          2222222n
      2   3333333n
          4444444n
      3   5555555n
          666666n

      Windows-style (fileSize=42, bufSize=16, lineLen=8, opts=NPT_OPTS_ENDLF | NPT_OPTS_CRLF)
      1   111111rn     Lines end in \r\n only
          222222rn
      2   333333rn
          444444rn
      3   555555rn
          rn

  @ingroup  ned_test
  @param    id          Identifier for buffer chain
  @param    fileSize    total size of file in bytes
  @param    bufSize     size of each buffer in the chain
  @param    lineLen     length of each line (may be up to entire filesize)
  @param    opts        options for line lengths (random, \r or \r\n, etc...)

  @returns   pointer to bufHead of buffer chain, or NULL if failed.
*///-----------------------------------------------------------------------------------------------
static sNedBuffer_t * NptFileCreate(
  unsigned        id,
  unsigned        fileSize,
  unsigned        bufSize,
  unsigned        lineLen,
  nptNedPtOpts_t  opts)
{
  unsigned        thisLen;
  unsigned        lineEndSize = 1;
  unsigned        offset;
  char            cLine       = '1';
  sNedBuffer_t   *pBufHead    = NULL;
  sNedBuffer_t   *pBuf;

  NedAssertDbg(fileSize);
  NedAssertDbg(bufSize >= 2);
  NedAssertDbg(lineLen);

  pBufHead = NptBufCreate(id, fileSize, bufSize);
  if(!pBufHead)
    return NULL;

  // UTCF_OPTS_CRLF option
  if(opts & NPT_OPTS_CRLF)
    lineEndSize = 2;

  // UTCF_OPTS_MIDSHORT option
  if(opts & NPT_OPTS_MIDSHORT)
  {
    pBuf = NedBufNext(pBufHead);
    while(pBuf && NedBufNext(pBuf))
    {
      if(NedBufSplit(pBuf, bufSize/2))
      {
        pBuf = NedBufNext(pBuf);
        if(pBuf)
          pBuf = NedBufNext(pBuf);
      }
    }
  }

  // add line data into file
  pBuf = pBufHead;
  while(pBuf)
  {
    offset  = 0;
    while(offset < NedBufLen(pBuf))
    {
      thisLen = lineLen;
      if(thisLen > NedBufLen(pBuf) - offset)
        thisLen = NedBufLen(pBuf) - offset;

      // last line may not have a line ending
      if( (thisLen < 2) ||
          (!(opts & NPT_OPTS_ENDLF) && NedBufIsLast(pBuf) && (offset + thisLen >= NedBufLen(pBuf))) )
      {
        memset(NedBufData(pBuf) + offset, cLine, thisLen);
      }

      // create line with line ending
      else
      {
        memset(NedBufData(pBuf) + offset, cLine, thisLen - lineEndSize);
        if(opts & NPT_OPTS_CRLF)
          NedBufData(pBuf)[offset+thisLen-2] = '\r';
        NedBufData(pBuf)[offset+thisLen-1] = '\n';
      }

      ++cLine;
      if(cLine > '9')
        cLine = '0';
      offset += thisLen;
    }

    pBuf = NedBufNext(pBuf);
  }

  return pBufHead;
}

/*-------------------------------------------------------------------------------------------------
  Free up buffers and restore buffer size to default.
-------------------------------------------------------------------------------------------------*/
static void NptFileDestroy(sNedBuffer_t *pBufHead)
{
  // restore buffer size and free up buffer chain
  NedBufSetMaxLen(0);
  if(pBufHead)
    NedBufChainFree(pBufHead);
}

/*!------------------------------------------------------------------------------------------------
  Test basic point functions

      void          NedPointInit        (sNedPoint_t *pPoint, sNedBuffer_t *pBuf);
      bool_t        NedPointIsPoint     (const sNedPoint_t *pPoint);
      void          NedPointTop         (sNedPoint_t *pPoint);
      void          NedPointBottom      (sNedPoint_t *pPoint);

  Test buffer for testing basic point functions

      Buf 1
      31 31 31 31 31 31 31 0a  |1111111.|
      32 32 32 32 32 32 32 0a  |2222222.|
      Buf 2
      33 33 33 33 33 33 33 0a  |3333333.|
      34 34 34 34 34 34 34 0a  |4444444.|
      Buf 3
      35 35 35 35 35 35 35 35  |55555555|

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointBasic(void)
{
  sNedPoint_t     sPoint;
  sNedBuffer_t   *pBufHead;
  static char     szAbc[] = "abc";

  NedTestBegin();

  // create a buffer with abc in it
  NedBufSetMaxLen(NPT_BUFSIZE);
  pBufHead = NedBufNew(1);
  if(pBufHead)
  {
    if(!NedBufGrow(pBufHead, 0, 3))
      NedTestFailed();
    memcpy(NedBufData(pBufHead), szAbc, sizeof(szAbc)-1);
  }
  else
  {
    NedTestFailed();
  }

  // verify NedPointInit()
  NedPointInit(&sPoint, pBufHead);
  if( !NedPointIsPoint(&sPoint) || (sPoint.pBuf != pBufHead) )
    NedTestFailed();

  // test NedPointFileBottom();
  NedPointFileBottom(&sPoint);
  if( (NedPointCharGet(&sPoint) != NEDCHAR_EOF) || (sPoint.offset != (sizeof(szAbc)-1)) )
    NedTestFailed();

  if( !NedPointCharPrev(&sPoint) || (NedPointCharGet(&sPoint) != 'c') )
    NedTestFailed();

  // test NedPointFileTop()
  NedPointFileTop(&sPoint);    
  if(NedPointCharGet(&sPoint) != 'a')
    NedTestFailed();

  NedTestEnd();

  if(pBufHead)
    NptFileDestroy(pBufHead);
}

/*!------------------------------------------------------------------------------------------------
  Test point position functions:

      long   NedPointPos        (const sNedPoint_t *pPoint);
      long   NedPointDifference (const sNedPoint_t *pThis, const sNedPoint_t *pThat);
      bool_t NedPointSort2Points(sNedPoint_t *pThis, sNedPoint_t *pThat);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointPos(void)
{
  sNedPoint_t     sPtThis;
  sNedPoint_t     sPtThat;
  sNedBuffer_t   *pBufHead  = NULL;
  unsigned        linelen   = 8;
  unsigned        filesize  = 29;

  NedTestBegin();

  pBufHead = NptFileCreate(1, filesize, NPT_BUFSIZE, linelen, NPT_OPTS_NONE);
  if(!pBufHead)
  {
    NedTestFailed();
  }
  else
  {
    NedPointInit(&sPtThis, pBufHead);
    NedPointInit(&sPtThat, pBufHead);
  }

  // test NedPointPos()
  if(NedPointPos(&sPtThis) != 0)
    NedTestFailed();

  NedPointFileBottom(&sPtThat);
  if(NedPointPos(&sPtThat) != filesize)
    NedTestFailed();

  // test NedPointDifference()
  if(!NedPointCharPrev(&sPtThat))
    NedTestFailed();

  if(NedPointDifference(&sPtThis, &sPtThat) != (filesize-1))
    NedTestFailed();

  if(NedPointDifference(&sPtThat, &sPtThis) != (-1L * (filesize-1)))
    NedTestFailed();

  // test sorting 2 points
  if(!NedPointSort2Points(&sPtThat, &sPtThis))
    NedTestFailed();

  if(NedPointDifference(&sPtThat, &sPtThis) != (filesize-1))
    NedTestFailed();

  NedTestEnd();

  if(pBufHead)
    NptFileDestroy(pBufHead);
}

/*!------------------------------------------------------------------------------------------------
  Test point position functions:

      long   NedPointPos        (const sNedPoint_t *pPoint);
      long   NedPointDifference (const sNedPoint_t *pThis, const sNedPoint_t *pThat);
      bool_t NedPointSort2Points(sNedPoint_t *pThis, sNedPoint_t *pThat);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointPosFile(void)
{
  static char    *szFilename    = "tcdata_1.c";
  static uint8_t  szLine[50];   // temporary line for printing
  sNedPoint_t     sLineBeg;
  sNedPoint_t     sLineEnd;
  sNedBuffer_t   *pBufHead;
  unsigned        i;
  unsigned        aLineLen[] = { 25, 18, 0, 32, 1, 26, 53, 12, 1 };
  long            len;
  long            expectedSize  = 175;
  long            fileSize      = 0;

  NedTestBegin();

  // try it with small buffer
  NedBufSetMaxLen(32);
  pBufHead = NedBufNew(1);
  if(!pBufHead || (NedBufFileLoad(pBufHead, szFilename, &fileSize) != NEDERR_NONE))
    NedTestFailed();

  // verify file size and line lengths
  if(fileSize != expectedSize)
    NedTestFailed();

  NedPointInit(&sLineBeg, pBufHead);
  for(i=0; i<NumElements(aLineLen); ++i)
  {
    sLineEnd = sLineBeg;
    NedPointLineEnd(&sLineEnd);
    len = NedPointDifference(&sLineBeg, &sLineEnd);
    NedLogPrintf("  len %ld, sLineBeg.pBuf %p, offset %u, sLineEnd.pBuf %p, offset %u\n",
      len, sLineBeg.pBuf, sLineBeg.offset, sLineEnd.pBuf, sLineEnd.offset);
    if(len >= sizeof(szLine))
      NedTestFailed();

    if(NedPointStrGet(szLine, &sLineBeg, len) != len)
      NedTestFailed();

    NedLogPrintf("  szLine = '%s'\n", szLine);
  }

  NedTestEnd();

  if(pBufHead)
    NptFileDestroy(pBufHead);
}

/*!------------------------------------------------------------------------------------------------
  Test point "is" functions

      bool_t        NedPointIsPoint       (const sNedPoint_t *pPoint);
      bool_t        NedPointIsAtBottom    (const sNedPoint_t *pPoint);
      bool_t        NedPointIsAtTop       (const sNedPoint_t *pPoint);
      bool_t        NedPointIsBetween     (const sNedPoint_t *pThis, const sNedPoint_t *pThat,
                                                                        const sNedPoint_t *pTest);
      bool_t        NedPointIsSameChain   (const sNedPoint_t *pThis, const sNedPoint_t *pThat);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointIs(void)
{
  sNedPoint_t     sPoint;
  sNedPoint_t     sPoint2;
  sNedPoint_t     sPointTest;
  sNedBuffer_t   *pBufHead;
  sNedBuffer_t   *pBufHead2;
  unsigned        linelen = 9;

  NedTestBegin();

  // create a test file (id=1, size=42, bufsize=16, lineline=8, opts=0)
  pBufHead  = NptFileCreate(1, 42, NPT_BUFSIZE, linelen, NPT_OPTS_ENDLF | NPT_OPTS_CRLF);
  pBufHead2 = NptFileCreate(2, 64, NPT_BUFSIZE, linelen, NPT_OPTS_ENDLF | NPT_OPTS_CRLF);

  if(!pBufHead || !pBufHead2)
    NedTestFailed();

  // test NedPointIsAtTop()
  NedPointInit(&sPoint, pBufHead);
  if(!NedPointIsAtTop(&sPoint))
    NedTestFailed();

  // test NedPointIsAtBottom()
  NedPointInit(&sPoint2, pBufHead);
  NedPointFileBottom(&sPoint2);
  if(!NedPointIsAtBottom(&sPoint2))
    NedTestFailed();

  // test NedPointIsBetween()
  NedPointCharNext(&sPoint);
  NedPointInit(&sPointTest, pBufHead);
  if(NedPointIsBetween(&sPoint, &sPoint2, &sPointTest))
    NedTestFailed();

  NedPointLineNext(&sPointTest);
  if(!NedPointIsBetween(&sPoint, &sPoint2, &sPointTest))
    NedTestFailed();

  // test NedPointIsSameChain()
  NedPointInit(&sPoint2, pBufHead2);
  NedPointFileBottom(&sPoint);
  if(NedPointIsSameChain(&sPoint, &sPoint2))
    NedTestFailed();

  if(!NedPointIsSameChain(&sPoint, &sPointTest))
    NedTestFailed();


  NedTestEnd();

  if(pBufHead)
    NptFileDestroy(pBufHead);
  if(pBufHead2)
    NptFileDestroy(pBufHead2);
}

/*!------------------------------------------------------------------------------------------------
  Test point character functions

    nedChar_t     NedPointCharGet       (const sNedPoint_t *pPoint);
    nedChar_t     NedPointCharGetNext   (sNedPoint_t *pPoint);
    nedChar_t     NedPointCharGetPrev   (sNedPoint_t *pPoint);
    bool_t        NedPointCharNext      (sNedPoint_t *pPoint);
    bool_t        NedPointCharPrev      (sNedPoint_t *pPoint);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointChar(void)
{
  sNedPoint_t     sPoint;
  sNedBuffer_t   *pBufHead;
  unsigned        linelen = 8;

  NedTestBegin();

  // create a test file (id=1, size=40, bufsize=16, lineline=8, opts=0)
  pBufHead = NptFileCreate(1, 40, NPT_BUFSIZE, linelen, NPT_OPTS_NONE);
  if(!pBufHead)
    NedTestFailed();

  // test init
  NedPointInit(&sPoint, pBufHead);
  if(!NedPointIsPoint(&sPoint) || (sPoint.pBuf != pBufHead))
    NedTestFailed();

  // test NedPointCharGet(), NedPointCHarPrev() and NedPointCharGetNext()
  NedPointFileBottom(&sPoint);
  if(NedPointCharGet(&sPoint) != NEDCHAR_EOF)
    NedTestFailed();

  if(!NedPointCharPrev(&sPoint) || (NedPointCharGet(&sPoint) != '5'))
    NedTestFailed();

  if(NedPointCharGetNext(&sPoint) != NEDCHAR_EOF)
    NedTestFailed();

  // test top
  NedPointFileTop(&sPoint);    
  if(NedPointCharGet(&sPoint) != '1')
    NedTestFailed();

  // test home/end
  NedPointLineEnd(&sPoint);
  if(NedPointCharGet(&sPoint) != '\n')
    NedTestFailed();

  if(NedPointCharGetNext(&sPoint) != '2')
    NedTestFailed();

  NedPointLineEnd(&sPoint);
  if(NedPointCharGet(&sPoint) != '\n')
    NedTestFailed();

  NedPointLineBeg(&sPoint);
  if(NedPointCharGet(&sPoint) != '2')
    NedTestFailed();

  NedTestEnd();

  if(pBufHead)
    NptFileDestroy(pBufHead);
}

/*!------------------------------------------------------------------------------------------------
  Test word movement functions

      bool_t        NedPointNextWord    (sNedPoint_t *pPoint);
      bool_t        NedPointEndWord     (sNedPoint_t *pPoint);
      bool_t        NedPointPrevWord    (sNedPoint_t *pPoint);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointWord(void)
{
  static char szFirstCharOfWord[] =  // must be in sync with szNptFile[]
  { '#','i','<','s','.','h','>','\n',
    '\n',
    'i','m','(','v',')','{','\n',
    '\t','p','(','h','w','"','\n',
    ' ','r','0',';','\n',
    '}','\n'
  };

  sNedBuffer_t   *pBufHead;
  sNedPoint_t     sPoint;
  unsigned        i;
  nedChar_t       c;

  NedTestBegin();

  pBufHead = NtpFileLoads(1, NPT_BUFSIZE, szNptFile);
  if(pBufHead == NULL)
    NedTestFailed();

  // walk forwards through file by words
  NedPointInit(&sPoint, pBufHead);
  i = 0;
  while(1)
  {
    // get the character
    c = NedPointCharGet(&sPoint);
    if(c == NEDCHAR_EOF)
    {
      if(i != NumElements(szFirstCharOfWord))
        NedTestFailed();

      break;
    }
  
    // is it the right character?
    if(c != szFirstCharOfWord[i])
      NedTestFailed();

    // go on to next work
    ++i;
    if(i > NumElements(szFirstCharOfWord))
      NedTestFailed();

    if(!NedPointWordNext(&sPoint))
      NedTestFailed();
  }

  // must start at end of buffer
  if(!NedPointIsAtBottom(&sPoint))
    NedTestFailed();

  // walk backwards through file by words
  i = NumElements(szFirstCharOfWord) - 1;
  while(NedPointWordPrev(&sPoint))
  {
    c = NedPointCharGet(&sPoint);

    // // check the word
    if(c != szFirstCharOfWord[i])
      NedTestFailed();

    // should be at top of file
    if(i == 0)
    {
      // NedPointWordPrev() should return FALSE at top of file
      if(NedPointWordPrev(&sPoint))
        NedTestFailed();
      break;
    }

    // on to previous index
    --i;
  }

  // too few words, failed
  if(i != 0)
    NedTestFailed();

  NedTestEnd();

  if(pBufHead)
    NptFileDestroy(pBufHead);
}

/*!------------------------------------------------------------------------------------------------
  Test line functions:

      bool_t        NedPointLineNext      (sNedPoint_t *pPoint);
      bool_t        NedPointLinePrev      (sNedPoint_t *pPoint);
      bool_t        NedPointLineBeg       (sNedPoint_t *pPoint);
      bool_t        NedPointLineEnd       (sNedPoint_t *pPoint);
      long          NedPointLineGet       (const sNedPoint_t *pPoint, long *pCol);
      long          NedPointLineGoto      (sNedPoint_t *pPoint, long line);

  Test buffer for testing line point functions.

      Buf 1
      31 31 31 31 31 31 31 0a  |1111111.|
      32 32 32 32 32 32 32 0a  |2222222.|
      Buf 2
      33 33 33 33 33 33 33 0a  |3333333.|
      34 34 34 34 34 34 34 0a  |4444444.|
      Buf 3
      35 35 35 35 35 35 35 35  |55555555|

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointLine(void)
{
  unsigned        i;
  sNedPoint_t     sPoint;
  sNedBuffer_t   *pBufHead;
  unsigned        lineLen = 8;
  bool_t          fMoved;
  long            line;
  long            col;

  NedTestBegin();

  // build test harness (id=1, filesize=40, bufsize=16, linelen=8, opts=0)
  pBufHead = NptFileCreate(1, 40, 16, lineLen, 0);
  if(!pBufHead)
    NedTestFailed();

  NedPointInit(&sPoint, pBufHead);
  if(!NedPointIsPoint(&sPoint) || (sPoint.pBuf != pBufHead))
    NedTestFailed();

  // test NedPointNextLine()
  for(i=1; i<=5; ++i)
  {
    if (NedPointCharGet(&sPoint) != ('0'+i) )
      NedTestFailed();

    fMoved = NedPointLineNext(&sPoint);
    if( ((i<5) && !fMoved) || (i==5 && fMoved) )
      NedTestFailed();
  }

  // test NedPointPrevLine()
  for(i=5; i>=1; --i)
  {
    if(NedPointCharGet(&sPoint) != ('0'+i) )
      NedTestFailed();

    fMoved = NedPointLinePrev(&sPoint);
    if( ((i>1) && !fMoved) || (i==1 && fMoved) )
      NedTestFailed();
  }

  // test NedPointGotoLine(), 
  for(i=1; i<5; ++i)
  {
    // test NedPointLineGoto()
    NedPointLineGoto(&sPoint, i);
    if(NedPointCharGet(&sPoint) != ('0'+i))
      NedTestFailed();

    // test NedPointLineGet()
    // for odd lines, advance point, for even, don't
    if(i & 1)
      NedPointCharNext(&sPoint);

    line = col = 0;
    line = NedPointLineGet(&sPoint, &col);
    if((line != i) || (col != ((i&1)+1)))
      NedTestFailed();
  }

  NedTestEnd();

  if(pBufHead)
    NptFileDestroy(pBufHead);
}

/*!------------------------------------------------------------------------------------------------
  Test line functions:

      long          NedPointLineGet       (const sNedPoint_t *pPoint, long *pCol);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointLineGet(void)
{
  sNedPoint_t     sPoint;
  sNedBuffer_t   *pBufHead  = NULL;
  long            line;
  long            col;

  NedTestBegin();

  // initialize the buffer
  pBufHead = NedBufNew(1);
  NedPointInit(&sPoint, pBufHead);
  if(!pBufHead)
    NedTestFailed();

  // verify empty buffer get
  line = col = 0;
  line = NedPointLineGet(&sPoint, &col);
  if((line != 1) || (col != 1))
    NedTestFailed();

  NptFileDestroy(pBufHead);

  pBufHead = NptFileCreate(1, 19, 16, 16, NPT_OPTS_NONE);
  NedPointInit(&sPoint, pBufHead);
  if(!pBufHead)
    NedTestFailed();

  NedPointFileBottom(&sPoint);
  line = NedPointLineGet(&sPoint, &col);
  if((line != 2) || (col != 4))
    NedTestFailed();

  NptFileDestroy(pBufHead);

  // verify buffer with 4 lines, last line has \n
  pBufHead = NptFileCreate(1, 42, 16, 16, NPT_OPTS_ENDLF);
  NedPointInit(&sPoint, pBufHead);
  if(!pBufHead)
    NedTestFailed();

  NedPointFileBottom(&sPoint);
  line = NedPointLineGet(&sPoint, &col);
  if((line != 4) || (col != 1))
    NedTestFailed();

  NedPointFileTop(&sPoint);
  line = NedPointLineGet(&sPoint, &col);
  if((line != 1) || (col != 1))
    NedTestFailed();

  NedPointLineNext(&sPoint);
  NedPointCharNext(&sPoint);
  NedPointCharNext(&sPoint);
  line = NedPointLineGet(&sPoint, &col);
  if((line != 2) || (col != 3))
    NedTestFailed();

  NedTestEnd();

  if(pBufHead)
    NptFileDestroy(pBufHead);
}

/*!------------------------------------------------------------------------------------------------
  Test character search functions

    bool_t        NedPointMemChr        (sNedPoint_t *pPoint, nedChar_t c, long maxLen);
    bool_t        NedPointStrChrRev     (sNedPoint_t *pPoint, nedChar_t c);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointStrChr(void)
{
  sNedBuffer_t   *pBufHead;
  bool_t          fFound;
  sNedPoint_t     sPoint;
  unsigned        i;
  long            line;
  long            col;

  // lines and columns are 1 based
  static char aSearchChars[] = { '#', '>', '{', '\t', '~' };
  static long aLineCols[] =   // must be in line/col pairs and in sync with szNptFile[]
  {   1, 1,     // # 
      1, 18,    // >
      3, 16,    // {
      4, 1,     // \t
      0, 0      // ~ must be not found
  };

  NedTestBegin();
  NedAssert(NumElements(aSearchChars) == (NumElements(aLineCols) / 2));

  pBufHead = NtpFileLoads(1, NPT_BUFSIZE, szNptFile);
  if(pBufHead == NULL)
    NedTestFailed();

  // test NedPointStrChr
  NedPointInit(&sPoint, pBufHead);
  for(i=0; i<NumElements(aSearchChars); ++i)
  {
    fFound = NedPointMemChr(&sPoint, aSearchChars[i], LONG_MAX);

    // last char in array should NOT be found
    if(i == (NumElements(aSearchChars)-1))
    {
      if(fFound == TRUE)
        NedTestFailed();
    }

    else
    {
      // char should be found
      if(NedPointCharGet(&sPoint) != aSearchChars[i])
        NedTestFailed();

      // should be on correct line/col
      line = col = 0;
      line = NedPointLineGet(&sPoint, &col);
      if((line != aLineCols[i*2]) || (col != aLineCols[i*2+1]))
        NedTestFailed();
    }
  }

  NedTestEnd();

  NptFileDestroy(pBufHead);
}

/*!------------------------------------------------------------------------------------------------
  Test point edit insert functions

      bool_t        NedPointInsChar       (sNedPoint_t *pPoint, nedChar_t c);
      bool_t        NedPointInsBlock      (sNedPoint_t *pPoint, uint8_t *pBlock, unsigned size);
      bool_t        NedPointInsStr        (sNedPoint_t *pPoint, const char *sz);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointIns(void)
{
  sNedBuffer_t   *pBufHead;
  sNedPoint_t     sPoint;

  NedTestBegin();

  pBufHead = NptFileCreate(1, 40, 16, 8, NPT_OPTS_NONE);
  if(!pBufHead)
    NedTestFailed();

  // test NedPointInsChar(). Advances cursor
  NedPointInit(&sPoint, pBufHead);
  if((NedPointCharGet(&sPoint) != '1') || !NedPointInsChar(&sPoint, 'a') || (NedPointCharGet(&sPoint) != '1') ||
    !NedPointDelChar(&sPoint) || !NedPointCharPrev(&sPoint) || (NedPointCharGet(&sPoint) != 'a'))
  {
    NedTestFailed();
  }

  // test NedPointInsBlock. Advances cursor
  NedPointLineNext(&sPoint);

  if((NedPointCharGet(&sPoint) != '2') || !NedPointInsBlock(&sPoint, (void *)"hjk", 3) ||
     (NedPointCharGet(&sPoint) != '2'))
  {
    NedTestFailed();
  }

  // test NedPointInsString
  NedPointFileBottom(&sPoint);
  NedBufLogDumpChain(pBufHead, 16);
  NedPointLogShow(&sPoint);
  if((NedPointCharGet(&sPoint) != NEDCHAR_EOF) || !NedPointInsStr(&sPoint, "wxyz") ||
     (NedPointCharGet(&sPoint) != NEDCHAR_EOF))
  {
    NedBufLogDumpChain(pBufHead, 16);
    NedPointLogShow(&sPoint);
    NedTestFailed();
  }

  // verify all points are still correct
  NedPointFileTop(&sPoint);
  if(NedPointCharGet(&sPoint) != 'a')
    NedTestFailed();

  NedPointLineNext(&sPoint);
  NedPointCharNext(&sPoint);
  if(NedPointCharGet(&sPoint) != 'j')
    NedTestFailed();

  NedPointFileBottom(&sPoint);
  NedPointCharPrev(&sPoint);
  if(NedPointCharGet(&sPoint) != 'z')
    NedTestFailed();

  // test NedPointInsBlock with exactly 1 buffer
  {
    uint8_t *pBlock;

    // free the chain without resetting NedBufMaxLen()
    if(pBufHead)
      NedBufChainFree(pBufHead);

    pBufHead = NedBufNew(1);
    if(!pBufHead)
      NedTestFailed();

    NedPointInit(&sPoint, pBufHead);
    pBlock = malloc(NedBufMaxLen());
    if(!pBlock)
    {
      NedTestFailed();
    }
    else
    {
      memset(pBlock, 'A', NedBufMaxLen());
    }

    if(!NedPointInsBlock(&sPoint, pBlock, NedBufMaxLen()))
      NedTestFailed();

    if((NedBufPrev(sPoint.pBuf) != pBufHead) || (NedBufLen(sPoint.pBuf) != 0))
    {
      NedBufLogDumpChain(pBufHead, 16);
      NedPointLogShow(&sPoint);
      NedTestFailed();
    }
  }

  NedTestEnd();

  NptFileDestroy(pBufHead);
}

/*!------------------------------------------------------------------------------------------------
  Test point edit delete functions

      bool_t        NedPointDelBackspace  (sNedPoint_t *pPoint);
      bool_t        NedPointDelChar       (sNedPoint_t *pPoint);
      bool_t        NedPointDelBlock      (sNedPoint_t *pThis, sNedPoint_t *pThat);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointDel(void)
{
  sNedBuffer_t   *pBufHead;
  sNedPoint_t     sThis;
  sNedPoint_t     sThat;
  long            diff;

  NedTestBegin();

  // create the test file
  pBufHead = NptFileCreate(1, 40, 16, 8, NPT_OPTS_NONE);
  if(!pBufHead)
  {
    NedTestFailed();
  }
  else
  {
    NedPointInit(&sThis, pBufHead);
    NedPointInit(&sThat, pBufHead);
  }

  // test NedPointDelChar()
    // delete a character
  NedPointLineNext(&sThat);
  diff = NedPointDifference(&sThis, &sThat);
  if(!NedPointDelChar(&sThis))
    NedTestFailed();

  // verify the character was deleted by using line length (should be 1 less)
  NedPointFileTop(&sThat);
  NedPointLineNext(&sThat);
  if(NedPointDifference(&sThis, &sThat) != (diff - 1))
    NedTestFailed();

  // try to delete character at end of file, should fail
  NedPointFileBottom(&sThat);
  if(NedPointDelChar(&sThat))
    NedTestFailed();

  // test NedPointDelBackspace()
  // delete the \n (joins lines 1 and 2)
  NedPointFileTop(&sThis);
  NedPointLineNext(&sThis);

  if(!NedPointDelBackspace(&sThis))
    NedTestFailed();
  if((NedPointCharGet(&sThis) != '2') || (NedPointCharGetPrev(&sThis) != '1'))
    NedTestFailed();

  // test NedPointDelBlock()
  // put this on line 3, that on line 4, then delete the whole line
  NedPointFileTop(&sThis);
  NedPointLineNext(&sThis);
  NedPointFileTop(&sThat);
  NedPointLineNext(&sThat);
  NedPointLineNext(&sThat);
  if((NedPointCharGet(&sThis) != '3') || !NedPointDelBlock(&sThis, &sThat))
    NedTestFailed();

  // verify line was deleted (line after 1,2 should be 4)
  if(!NedPointIsSame(&sThis, &sThat) || NedPointCharGet(&sThis) != '4')
    NedTestFailed();

  // delete whole file.
  NedPointFileTop(&sThis);
  NedPointFileBottom(&sThat);
  if(!NedPointDelBlock(&sThis, &sThat))
    NedTestFailed();

  // verify file is empty
  if(!NedPointIsAtTop(&sThis) || !NedPointIsAtBottom(&sThis))
    NedTestFailed();

  NedTestEnd();

  NptFileDestroy(pBufHead);
}

/*!------------------------------------------------------------------------------------------------
  Test NedPointClipClear().

      void NedPointClipClear(sNedPoint_t *pClip);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointClipClear(void)
{
  sNedBuffer_t   *pBufHead;
  sNedPoint_t     sClip;
  #define         TPCC_FILE_SIZE  33

  NedTestBegin();

  // create the test file
  pBufHead = NptFileCreate(1, TPCC_FILE_SIZE, 16, 8, NPT_OPTS_NONE);
  if(!pBufHead)
  {
    NedTestFailed();
  }
  else
  {
    NedPointInit(&sClip, pBufHead);
    NedPointFileBottom(&sClip);
  }

  if(NedPointPos(&sClip) != TPCC_FILE_SIZE)
    NedTestFailed();

  NedPointClipClear(&sClip);
  if(NedPointPos(&sClip) != 0)
    NedTestFailed();

  NedTestEnd();

  NptFileDestroy(pBufHead);
}

/*!------------------------------------------------------------------------------------------------
  Test NedPointClipCopy(). 

      bool_t NedPointClipCopy(sNedPoint_t *pClip, const sNedPoint_t *pThis, const sNedPoint_t *pThat);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointClipCopy(void)
{
  sNedBuffer_t   *pBufHead;
  sNedBuffer_t   *pClipHead;
  sNedPoint_t     sClip;
  sNedPoint_t     sThis;
  sNedPoint_t     sThat;

  NedTestBegin();

  // create a file and clip to work on (not likely to fail).
  // Sets buffer sizes to 16 bytes.
  pBufHead  = NtpFileLoads(1, 16, szNptFile);
  pClipHead = NedBufNew(2);
  NedPointInit(&sClip, pClipHead);
  if(!pBufHead || !pClipHead)
    NedTestFailed();

  // copy line 3 "int main(void) {\n"
  NedPointInit(&sThis, pBufHead);
  NedPointLineNext(&sThis);
  NedPointLineNext(&sThis);
  sThat = sThis;
  NedPointLineNext(&sThat);
  if(!NedPointClipCopy(&sClip, &sThis, &sThat))
    NedTestFailed();

  // NedPointClipCopy() should have created 1 buffer in the clip in the clip
  {
    static char szClip1[] = "int main(";
    static char szClip2[] = "void) {\n";

    if((sClip.offset != NedBufLen(sClip.pBuf)) ||
      (memcmp(NedBufData(sClip.pBuf), szClip2, sizeof(szClip2)-1) != 0))
      NedTestFailed();

    NedPointFileTop(&sClip);
    if((NedBufLen(sClip.pBuf) != (sizeof(szClip1)-1)) ||
      (memcmp(NedBufData(sClip.pBuf), szClip1, sizeof(szClip1)-1) != 0))
    {
      NedTestFailed();
    }
  }

  NedTestEnd();

  NptFileDestroy(pBufHead);
  NedBufChainFree(sClip.pBuf);
}

/*!------------------------------------------------------------------------------------------------
  Test NedPointClipCut().

      bool_t NedPointClipCut(sNedPoint_t *pClip, sNedPoint_t *pThis, sNedPoint_t *pThat);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointClipCut(void)
{
  sNedBuffer_t   *pBufHead;
  sNedBuffer_t   *pClipHead;
  sNedPoint_t     sClip;
  sNedPoint_t     sThis;
  sNedPoint_t     sThat;

  NedTestBegin();

  // create a file and clip to work on (not likely to fail).
  // Sets buffer sizes to 16 bytes.
  pBufHead  = NtpFileLoads(1, 16, szNptFile);
  pClipHead = NedBufNew(2);
  NedPointInit(&sClip, pClipHead);
  if(!pBufHead || !pClipHead)
    NedTestFailed();

  // select the "include " in "#include <stdio.h>  \r\n"
  NedPointInit(&sThis, pBufHead);
  NedPointWordNext(&sThis);
  sThat = sThis;
  NedPointWordNext(&sThat);

  if(!NedPointClipCut(&sClip, &sThis, &sThat))
    NedTestFailed();

  // clip should be filled with "include "
  {
    static char szClip[] = "include ";

    if((sClip.offset != NedBufLen(sClip.pBuf)) ||
      (memcmp(NedBufData(sClip.pBuf), szClip, sizeof(szClip)-1) != 0))
    {
      NedTestFailed();
    }
  }

  // sThis and sThat should be equal and the data should be cut
  {
    static char szBuffer[] = "#<stdio.";

    if(!NedPointIsSame(&sThis, &sThat) || 
      (memcmp(NedBufData(sThis.pBuf), szBuffer, sizeof(szBuffer)-1) != 0))
    {
      NedTestFailed();
    }
  }

  NedTestEnd();

  NptFileDestroy(pBufHead);
  NedBufChainFree(sClip.pBuf);
}

/*!------------------------------------------------------------------------------------------------
  Test NedPointClipPaste().

      long NedPointClipPaste(sNedPoint_t *pPoint, const sNedPoint_t *pClip);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointClipPaste(void)
{
  sNedBuffer_t   *pBufHead;
  sNedBuffer_t   *pClipHead;
  sNedPoint_t     sPoint;
  sNedPoint_t     sPoint2;
  sNedPoint_t     sClip;
  static char     szHello[]  = "Hello";
  static char     szWorld[]  = "World!";
  static char     szWorld3[] = "World!World!World!";
  unsigned        i;

  NedTestBegin();

  // create a file and clip to work on (not likely to fail).
  // Sets buffer sizes to 16 bytes.
  pBufHead  = NtpFileLoads(1, 16, szNptFile);
  pClipHead = NedBufNew(2);
  NedPointInit(&sPoint, pBufHead);
  NedPointInit(&sClip, pClipHead);
  if(!pBufHead || !pClipHead)
  {
    NedTestFailed();
  }
  else
  {
    NedBufInsert(sClip.pBuf, 0, (void *)szHello, sizeof(szHello)-1);
  }

  // insert "Hello" at top of file
  if(NedPointClipPaste(&sPoint, &sClip) != (sizeof(szHello)-1))
    NedTestFailed();

  pBufHead = NedBufHead(sPoint.pBuf);
  NedLogPrintf("  pBufHead %p\n", pBufHead);
  NedBufLogDumpChain(pBufHead, 16);

  if(memcmp(NedBufData(pBufHead), szHello, sizeof(szHello)-1) != 0)
    NedTestFailed();

  if(NedPointPos(&sPoint) != 5)
    NedTestFailed();

  // Paste "World!" 3 times on next line
  NedPointClipClear(&sClip);
  NedBufInsert(sClip.pBuf, 0, (void *)szWorld, sizeof(szWorld)-1);
  NedPointLineNext(&sPoint);
  sPoint2 = sPoint;

  for(i=0; i<3; ++i)
  {
    if(NedPointClipPaste(&sPoint, &sClip) != (sizeof(szWorld)-1))
      NedTestFailed();
  }

  // verify it got pasted correctly
  {
    char aData[sizeof(szWorld3)];

    memset(aData, 'A', sizeof(aData));
    if((NedPointStrGet((void *)aData, &sPoint2, sizeof(aData)-1) != (sizeof(aData)-1)) ||
       (memcmp(aData, szWorld3, sizeof(aData)) != 0))
    {
      NedTestFailed();
    }
  }

  NedTestEnd();

  // free the chains
  NptFileDestroy(pBufHead);
  NedBufChainFree(sClip.pBuf);
}

/*!------------------------------------------------------------------------------------------------
  Test point brace functions

    int    NedPointIsBrace(const sNedPoint_t *pPoint);
    bool_t NedPointBraceMatch(sNedPoint_t *pPoint);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointBrace(void)
{
  sNedBuffer_t  *pBufHead;
  sNedPoint_t   sPoint;

  NedTestBegin();

  // create a file and clip to work on (not likely to fail).
  // Sets buffer sizes to 16 bytes.
  pBufHead  = NtpFileLoads(1, 16, szNptFile);
  NedPointInit(&sPoint, pBufHead);
  if(!pBufHead)
    NedTestFailed();

  // verify '#' is not a brace
  if(NedPointIsBrace(&sPoint))
    NedTestFailed();

  // verify we find the opening '<'
  if(!NedPointBraceMatch(&sPoint) || (NedPointCharGet(&sPoint) != '<'))
    NedTestFailed();

  // verify we find the close '>'
  if(!NedPointBraceMatch(&sPoint) || (NedPointCharGet(&sPoint) != '>'))
    NedTestFailed();

  // verify we find the opening '>' again
  if(!NedPointBraceMatch(&sPoint) || (NedPointCharGet(&sPoint) != '<'))
    NedTestFailed();

  NedTestEnd();

  NptFileDestroy(pBufHead);
}

/*!------------------------------------------------------------------------------------------------
  Test point error line functions

    bool_t        NedPointIsErrLine     (const sNedPoint_t *pPoint, sNedPtErrInfo_t *pErrInfo);
    bool_t        NedPointErrFree       (sNedPtErrInfo_t *pErrInfo);
    void          NedPointErrNextLine   (sNedPoint_t *pPoint, sNedPtErrInfo_t *pErrInfo);
    void          NedPointErrPrevLine   (sNedPoint_t *pPoint, sNedPtErrInfo_t *pErrInfo);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointErrLine(void)
{
  sNedBuffer_t     *pBufHead;
  sNedPoint_t       sPoint;
  sNedPtErrInfo_t   sErrInfo;
  static char      *aszName[] = { "File.c", "File.c", "File.c", "File2.c", "File2.c" };
  static long       aLine[]   = { 10,       11,       103,      15,        15 };
  static long       aCol[]    = {  3,        5,        43,      36,        43 };
  unsigned          i;

  NedTestBegin();

  // create a file and clip to work on (not likely to fail).
  // Sets buffer sizes to 16 bytes.
  pBufHead  = NtpFileLoads(1, 256, szNptErrFile);
  NedPointInit(&sPoint, pBufHead);
  if(!pBufHead)
    NedTestFailed();

  // 1st line should be an error line
  memset(&sErrInfo, 0, sizeof(sErrInfo));
  if(!NedPointIsErrLine(&sPoint, &sErrInfo))
    NedTestFailed();
  NedPointErrFree(&sErrInfo);

  // look forward for errors
  for(i=0; i<NumElements(aszName); ++i)
  {
    // should be another err line
    memset(&sErrInfo, 0, sizeof(sErrInfo));
    if(!NedPointErrLineNext(&sPoint, &sErrInfo) ||
       (strcmp(sErrInfo.szFile, aszName[i]) != 0) ||
       (sErrInfo.line != aLine[i]) || (sErrInfo.col != aCol[i]))
    {
      NedTestFailed();
    }
    NedPointErrFree(&sErrInfo);
    NedPointLineNext(&sPoint);
  }

  // should be no more errors
  memset(&sErrInfo, 0, sizeof(sErrInfo));
  if(NedPointErrLineNext(&sPoint, &sErrInfo))
    NedTestFailed();

  // look for previous errors
  NedTestPrintf("\n-- Reverse --\n\n");
  i = NumElements(aszName) - 1;
  while(1)
  {
    // should be another err line
    memset(&sErrInfo, 0, sizeof(sErrInfo));
    if(!NedPointErrLinePrev(&sPoint, &sErrInfo) ||
       (strcmp(sErrInfo.szFile, aszName[i]) != 0) ||
       (sErrInfo.line != aLine[i]) || (sErrInfo.col != aCol[i]))
    {
      NedTestFailed();
    }
    NedPointErrFree(&sErrInfo);
    NedPointLinePrev(&sPoint);
    if(i == 0)
      break;
    --i;
  }

  NedTestEnd();

  NptFileDestroy(pBufHead);
}

/*!------------------------------------------------------------------------------------------------
  Test point function lines

      bool_t        NedPointIsFuncLine    (const sNedPoint_t *pPoint);
      void          NedPointFuncNext      (sNedPoint_t *pPoint, pfnIsFuncLine_t pfnIsFuncLine);
      void          NedPointFuncPrev      (sNedPoint_t *pPoint, pfnIsFuncLine_t pfnIsFuncLine);

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointFuncLine(void)
{
  sNedBuffer_t     *pBufHead;
  sNedBuffer_t     *pBuf;
  sNedPoint_t       sPoint;
  bool_t            fFound;
  static char       aLines[] = {'c', 'C', 'i'};
  unsigned          i;

  NedTestBegin();

  // create a file and clip to work on (not likely to fail).
  // Sets buffer sizes to 16 bytes.
  pBufHead = pBuf = NtpFileLoads(1, 256, szNptFuncFile);
  NedPointInit(&sPoint, pBuf);
  if(!pBufHead)
    NedTestFailed();

  // first line is a prototype, not a function
  if(NedPointIsFuncLine(&sPoint, NULL))
    NedTestFailed();

  // look forward for function lines. Should be three.
  NedPointFileTop(&sPoint);
  for(i=0; i<NumElements(aLines); ++i)
  {
    fFound = NedPointFuncNext(&sPoint, NULL);
    if(!fFound || (NedPointCharGet(&sPoint) != aLines[i]))
      NedTestFailed();

    if(!NedPointLineNext(&sPoint))
      break;
  }

  // should NOT find another line
  if(NedPointFuncNext(&sPoint, NULL))
    NedTestFailed();

  // look up for functions
  NedTestPrintf("-- Reverse --\n");
  NedPointFileBottom(&sPoint);
  for(i=0; i<NumElements(aLines); ++i)
  {
    fFound = NedPointFuncPrev(&sPoint, NULL);
    if(!fFound || (NedPointCharGet(&sPoint) != aLines[NumElements(aLines) - (i+1)]))
      NedTestFailed();

    if(!NedPointLinePrev(&sPoint))
      break;
  }

  // should NOT find another line
  if(NedPointFuncPrev(&sPoint, NULL))
    NedTestFailed();

  NedTestEnd();

  NptFileDestroy(pBufHead);
}

/*!------------------------------------------------------------------------------------------------
  Test point search function

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointSearch(void)
{
  sNedBuffer_t     *pBufHead;
  sNedBuffer_t     *pBuf;
  sNedPoint_t       sPoint;
  sNedPoint_t       sPointOld;
  long              col = 0;

  NedTestBegin();

  // create a file and clip to work on (not likely to fail).
  // Sets buffer sizes to 16 bytes.
  pBufHead = pBuf = NtpFileLoads(1, 256, szNptFuncFile);
  NedPointInit(&sPoint, pBuf);
  if(!pBufHead)
    NedTestFailed();

  // should find Car on line 4, column 7
  if(!NedPointSearch(&sPoint, "Car", NEDPT_SEARCH_OPT_NONE))
  {
    NedTestFailed();
  }
  else if(NedPointLineGet(&sPoint, &col) != 4 || (col !=7))
  {
    NedTestFailed();
  }

  // find next should find Car on line 9, column 5
  if(!NedPointSearch(&sPoint, "Car", NEDPT_SEARCH_OPT_NONE))
  {
    NedTestFailed();
  }
  else if(NedPointLineGet(&sPoint, &col) != 9 || (col !=5))
  {
    NedTestFailed();
  }

  // Verify reverse search finds Car at line 4, col 7
  sPointOld = sPoint;
  if(!NedPointSearch(&sPoint, "Car", NEDPT_SEARCH_OPT_REVERSE))
  {
    NedTestFailed();
  }
  else if(NedPointLineGet(&sPoint, &col) != 4 || (col !=7))
  {
    NedTestFailed();
  }

  // Verify point doesn't move if not found.
  sPointOld = sPoint;
  if(NedPointSearch(&sPoint, "nothere", NEDPT_SEARCH_OPT_CASE_SENSITIVE))
  {
    NedTestFailed();
  }
  else if(!NedPointIsSame(&sPoint, &sPointOld))
  {
    NedTestFailed();
  }

  // Verify case sensitive skips "Car", and only finds "car"
  sPointOld = sPoint;
  if(!NedPointSearch(&sPoint, "car", NEDPT_SEARCH_OPT_CASE_SENSITIVE))
  {
    NedTestFailed();
  }
  else if(NedPointLineGet(&sPoint, &col) != 20 || (col !=7))
  {
    NedTestFailed();
  }

  // find data that crosses buffer boundaries
  NedPointFileTop(&sPoint);
  if(!NedPointSearch(&sPoint, "\n  brand", NEDPT_SEARCH_OPT_NONE))
  {
    NedTestFailed();
  }
  else if(NedPointLineGet(&sPoint, &col) != 13 || (col !=2))
  {
    NedTestFailed();
  }

  NedTestEnd();

  NptFileDestroy(pBufHead);
}

/*!------------------------------------------------------------------------------------------------
  Test advanced search functions WholeWord, Wrap

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPointSearchAdv(void)
{
  sNedBuffer_t     *pBufHead;
  sNedBuffer_t     *pBuf;
  sNedPoint_t       sPoint;
  sNedPoint_t       sPointOld;
  long              col;

  NedTestBegin();

  // create a file and clip to work on (not likely to fail).
  // Sets buffer sizes to 16 bytes.
  pBufHead = pBuf = NtpFileLoads(1, 256, szNptFuncFile);
  if(!pBuf)
    NedTestFailed();
  NedPointInit(&sPoint, pBuf);

  // check wrap in forward direction, should not find "Car" without wrap
  col = 0;
  NedPointLineGoto(&sPoint, 22);
  sPointOld = sPoint;
  if(NedPointSearch(&sPoint, "Car", NEDPT_SEARCH_OPT_NONE))
    NedTestFailed();
  if(!NedPointIsSame(&sPoint, &sPointOld))
    NedTestFailed();
  if(!NedPointSearch(&sPoint, "Car", NEDPT_SEARCH_OPT_WRAP))
    NedTestFailed();
  if(NedPointLineGet(&sPoint, &col) != 4 || (col != 7))
    NedTestFailed();

  // should NOT find Car1
  col = 0;
  NedPointLineGoto(&sPoint, 22);
  sPointOld = sPoint;
  if(NedPointSearch(&sPoint, "Car1", NEDPT_SEARCH_OPT_WRAP))
    NedTestFailed();
  if(!NedPointIsSame(&sPoint, &sPointOld))
    NedTestFailed();

  // check whole word
  col = 0;
  NedPointLineGoto(&sPoint, 8);
  if(!NedPointSearch(&sPoint, "model", NEDPT_SEARCH_OPT_WHOLE_WORD))
    NedTestFailed();
  if(NedPointLineGet(&sPoint, &col) != 15 || (col != 3))
    NedTestFailed();

  NedTestEnd();

  NptFileDestroy(pBufHead);
}

void TcPointWrapText(void)
{
  sNedBuffer_t     *pBufHead;
  sNedPoint_t       sPointTop;
  sNedPoint_t       sPointBot;
  sNedPoint_t       sPointTopBefore;
  char             *psz = NULL;
  const char        szBeg[] = NPT_BEG;
  const char        szEnd[] = NPT_END;
  sNedPtIndent_t    sIndent;

  NedTestBegin();

  pBufHead = NtpFileLoads(1, 256, szNptWrappedFile1);
  if(!pBufHead)
    NedTestFailed();
  if(NedTestVerbose())
    NedBufLogDumpChain(pBufHead, 16);

  NedPointInit(&sPointTop, pBufHead);
  NedPointCharAdvance(&sPointTop, strlen(szBeg));
  sPointTopBefore = sPointTop;
  NedPointInit(&sPointBot, pBufHead);
  if(!NedPointSearch(&sPointBot, szEnd, 0))
  {
    NedTestFailed();
  }

  // wrap at 70 should convert from file1 to file2
  // verify returned lines, returned points, and contents
  sIndent.c       = ' ';
  sIndent.nChars  = 4; 
  sIndent.nCols   = 4;
  if(!NedPointWrapText(&sIndent, &sPointTop, &sPointBot, 70))
    NedTestFailed();
  if(NedPointPos(&sPointTop) != NedPointPos(&sPointTopBefore))
    NedTestFailed();
  if(NedPointDifference(&sPointTop, &sPointBot) != strlen(szNptWrappedFile2) - strlen(szBeg) - strlen(szEnd))
  {
    if(NedTestVerbose())
    {
      NedLogPrintf("dif %ld, expected dif %ld\n", NedPointDifference(&sPointTop, &sPointBot),
        sizeof(szNptWrappedFile2) - strlen(szBeg) - strlen(szEnd));
      NedBufLogDumpChain(pBufHead, 16);
    }
    NedTestFailed();
  }
  psz = NtpGetAllText(&sPointTop);
  if(!psz)
    NedTestFailed();
  if(strcmp(psz, szNptWrappedFile2) != 0)
  {
    NedLogPrintf("string didn't compare, len %zu, len File2 %zu, diff %ld, whereDiff 0x%02x\n",
      strlen(psz), strlen(szNptWrappedFile2), NedPointFileSize(&sPointTop),
      (unsigned)NedStrWhereDiff(psz, szNptWrappedFile2, strlen(psz)));
    NedLogPrintf("szNptWrappedFile2:\n%s\n", szNptWrappedFile2);
    NedLogPrintf("psz:\n%s\n", psz);
    NedTestFailed();
  }
  free(psz);
  psz = NULL;

  // wrap at 100 should convert from file2 to file1
  // verify returned lines, returned points, and contents
  sIndent.c       = ' ';
  sIndent.nChars  = 2;
  sIndent.nCols   = 2;
  if(!NedPointWrapText(&sIndent, &sPointTop, &sPointBot, 100))
    NedTestFailed();
  if(NedPointPos(&sPointTop) != NedPointPos(&sPointTopBefore))
    NedTestFailed();
  if(NedPointDifference(&sPointTop, &sPointBot) != strlen(szNptWrappedFile1) - strlen(szBeg) - strlen(szEnd))
    NedTestFailed();
  psz = NtpGetAllText(&sPointTop);
  if(!psz)
    NedTestFailed();
  if(strcmp(psz, szNptWrappedFile1) != 0)
  {
    NedLogPrintf("string didn't compare, len %zu, len File1 %zu, diff %ld, whereDiff 0x%02x\n",
      strlen(psz), strlen(szNptWrappedFile1), NedPointFileSize(&sPointTop),
      (unsigned)NedStrWhereDiff(psz, szNptWrappedFile1, strlen(psz)));
    NedLogPrintf("szNptWrappedFile1:\n%s\n", szNptWrappedFile1);
    NedLogPrintf("psz:\n%s\n", psz);
    NedTestFailed();
  }
  free(psz);
  psz = NULL;

  NedTestEnd();
  if(psz)
    free(psz);
}

/*-------------------------------------------------------------------------------------------------
  Run each test case
-------------------------------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcPointBasic",     TcPointBasic },
    { "TcPointPos",       TcPointPos },
    { "TcPointPosFile",   TcPointPosFile },
    { "TcPointIs",        TcPointIs },
    { "TcPointChar",      TcPointChar },
    { "TcPointWord",      TcPointWord },
    { "TcPointLine",      TcPointLine },
    { "TcPointLineGet",   TcPointLineGet },
    { "TcPointStrChr",    TcPointStrChr },
    { "TcPointIns",       TcPointIns },
    { "TcPointDel",       TcPointDel },
    { "TcPointClipClear", TcPointClipClear },
    { "TcPointClipCopy",  TcPointClipCopy },
    { "TcPointClipCut",   TcPointClipCut },
    { "TcPointClipPaste", TcPointClipPaste },
    { "TcPointBrace",     TcPointBrace },
    { "TcPointErrLine",   TcPointErrLine },
    { "TcPointFuncLine",  TcPointFuncLine },
    { "TcPointSearch",    TcPointSearch },
    { "TcPointSearchAdv", TcPointSearchAdv },
    { "TcPointWrapText",  TcPointWrapText }
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestPoint", NumElements(aTestCases), aTestCases);
  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
