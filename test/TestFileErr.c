#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>

int main( int argc, char *argv[] ) 
{
  ssize_t     iValue = -1 * (SSIZE_MAX-1);
  unsigned    uValue = UINT_MAX;
  ptrdiff_t   diff1 = (void *)(&iValue) - (void *)(&uValue);
  ptrdiff_t   diff2 = (void *)(&uValue) - (void *)(&iValue);

  printf("ssize_t  len %zu, value %zd\n", s izeof(iValue), iValue);
  printf("unsigned len %zu, value %u\n", sizeof(uValue), uValue);
  printf("diff1 %ld, diff2 %td\n", diff1, diff2);
  return 0;
}
