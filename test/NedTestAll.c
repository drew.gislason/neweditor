/*!************************************************************************************************
  @file NedTestAll.c

  @brief  Ned File Test Cases

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  Runs all the tests, automated or not

*///***********************************************************************************************
#include <unistd.h>
#include <errno.h>
#include "Ned.h"
#include "NedLog.h"
#include "NedTest2.h"
#include "NedBuffer.h"
#include "NedFile.h"

typedef enum
{
  RESULT_PASSED,
  RESULT_FAILED,
  RESULT_SKIPPED
} testResult_t;

/*--------------------------------------------------------------------------------------------------
  Set color to one of passed, failed, skipped
--------------------------------------------------------------------------------------------------*/
void NtaColor(testResult_t result)
{
  if(result == RESULT_PASSED)
    AnsiSetAttr((nedAttr_t)TESTATTR_PASSED);
  else if(result == RESULT_FAILED)
    AnsiSetAttr((nedAttr_t)TESTATTR_FAILED);
  else
    AnsiSetAttr((nedAttr_t)TESTATTR_SKIPPED);
}

/*--------------------------------------------------------------------------------------------------
  Set color to none
--------------------------------------------------------------------------------------------------*/
void NtaNoColor(void)
{
  AnsiSetAttr((nedAttr_t)TESTATTR_NORMAL);
}

/*--------------------------------------------------------------------------------------------------
  Summarize results of running all test tests, both on-screen and in log
--------------------------------------------------------------------------------------------------*/
void NtaSummary(unsigned count, const char *szTestPrograms[], const testResult_t aResults[])
{
  unsigned      i;
  const char    szTitle[]       = "\nTEST SUITES:\n";
  const char    szTotalTitle[]  = "\nTOTAL RESULTS:\n";
  const char    szNoTotals[]    = "Log File not found or contains no test cases\n";
  const char   *aszResults[]    = { "Passed", "Failed", "Skipped" };
  unsigned      aResultTotals[3];
  const char   *pszResult;
  testResult_t  result;

  // sanity check on arrays that must be same size
  NedAssert(NumElements(aszResults) == NumElements(aResultTotals));

  // display and log the list of test suites plus results
  NtaNoColor();
  printf(szTitle);
  NedLogPrintf(szTitle);
  for(i = 0 ; i < count; ++i)
  {
    // determine result in string form
    result = aResults[i];
    if(result > NumElements(aszResults))
      result = RESULT_FAILED;
    pszResult = aszResults[result];

    // summarize to screen
    printf(m_szColumn, szTestPrograms[i]);
    NtaColor(result);
    printf("%s", pszResult);
    NtaNoColor();
    printf("\n");

    // summarize to log file
    NedLogPrintf(m_szColumn, szTestPrograms[i]);
    NedLogPrintf("%s\n", pszResult);
  }

  // calculate total test cases from log file
  printf(szTotalTitle);
  NedLogPrintf(szTotalTitle);
  if(!NedTestCalcLogTotals(&aResultTotals[RESULT_PASSED], &aResultTotals[RESULT_FAILED], &aResultTotals[RESULT_SKIPPED]))
  {
    printf(szNoTotals);
    NedLogPrintf(szNoTotals);
  }
  else
  {
    // reopen log file closed when calculating totals
    NedLogFileAppend(szNedLogDefaultName);

    // display and log totals
    for(i = 0 ; i < NumElements(aszResults); ++i)
    {
      // colorize for screen
      NtaColor(i);
      printf(m_szColumn, aszResults[i]);
      printf("%u", aResultTotals[i]);
      NtaNoColor();
      printf("\n");

      // no color for log file
      NedLogPrintf(m_szColumn, aszResults[i]);
      NedLogPrintf("%u\n", aResultTotals[i]);
    }
  }
}

#if 0
/*-------------------------------------------------------------------------------------------------
  Run a test suite, pass through parameters -a and -v
  Returns the result of the test suite (cmdline return code)
-------------------------------------------------------------------------------------------------*/
int NtaSuiteRun(const char *szTest, bool_t fAutomated, unsigned verbose)
{
  FILE         *fp;
  char          szCmdLine[PATH_MAX];
  int           exitCode;
  char          szVerbose[6];

  // set up verbose string
  if(verbose == 0)
    *szVerbose = '\0';
  else
  {
    if(verbose > 9)
      verbose = 9;
    sprintf(szVerbose, " -v%u", verbose);
  }

  /* run the automated program, pass automated and verbose through */
  szCmdLine[sizeof(szCmdLine) - 1] = '\0';
  snprintf(szCmdLine, sizeof(szCmdLine) - 1, "./%s%s%s -m", szTest, fAutomated ? " -a" : "", szVerbose);

  /* Open the command for reading */
  fp = popen(szCmdLine, "r");
  if(fp == NULL)
  {
    printf("Failed to run %s\n", szCmdLine);
    NedLogPrintf("Failed to run %s\n", szCmdLine);
    return TEST_EXIT_SKIP;
  }

  /* show the output, then get exit code */
  while(fgets(szCmdLine, sizeof(szCmdLine) - 1, fp) != NULL)
    printf("%s", szCmdLine);
  exitCode = pclose(fp);

  return exitCode;
}
#endif

/*-------------------------------------------------------------------------------------------------
  Run a test suite, pass through parameters -a and -v
  Returns the result of the test suite (cmdline return code)
-------------------------------------------------------------------------------------------------*/
int NtaSuiteRun(const char *szTest, bool_t fAutomated, unsigned verbose)
{
  char          szCmdLine[PATH_MAX];
  int           ret;
  char          szVerbose[6];
  char          szRetCode[6];
  FILE         *fp;
  size_t        len;

  // set up verbose string
  if(verbose == 0)
    *szVerbose = '\0';
  else
  {
    if(verbose > 9)
      verbose = 9;
    sprintf(szVerbose, " -v%u", verbose);
  }

  /* run the automated program, pass automated and verbose through */
  remove(m_szRetCodeFile);
  szCmdLine[sizeof(szCmdLine) - 1] = '\0';
  snprintf(szCmdLine, sizeof(szCmdLine) - 1, "./%s%s%s -m", szTest, fAutomated ? " -a" : "", szVerbose);
  ret = system(szCmdLine);
  printf("cmdline %s, system ret %d, errno %d\n", szCmdLine, ret, errno);

  // get the return code from the tmp file
  *szRetCode = '\0';
  fp = fopen(m_szRetCodeFile, "r");
  if(fp)
  {
    len = fread(szRetCode, 1, sizeof(szRetCode)-1, fp);
    fclose(fp);
    szRetCode[len] = '\0';
  }
  remove(m_szRetCodeFile);

  // if we got a return code, retun it. If not, choose fail
  if(isdigit(*szRetCode))
    ret = atoi(szRetCode);
  else
    ret = TEST_EXIT_FAIL;

  return ret;
}

/*-------------------------------------------------------------------------------------------------
  Returns TRUE if this test should be filtered
-------------------------------------------------------------------------------------------------*/
bool_t NtaFilterThis(const char *szTest, const char *szFilter)
{
  bool_t    fFilter = FALSE;

  if(szFilter && strstr(szTest, szFilter) == NULL)
    fFilter = TRUE;

  return fFilter;
}

/*-------------------------------------------------------------------------------------------------
  Run each test suite in the list.

  The list is created manually to allow more control of order and which tests are included
-------------------------------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
#if 1
  const char *szTestPrograms[] =
  {
    "test_ansi", "test_buffer", "test_cmd", "test_file", "test_json2", "test_key", "test_keymap",
    "test_lang", "test_list", "test_log", "test_mem", "test_menu", "test_msg", "test_point",
    "test_poslist", "test_settings", "test_sig", "test_theme", "test_undo", "test_utils",
    "test_view", "test_win"
  };
#else
  const char *szTestPrograms[] =
  {
    "test_buffer", "test_menu", "test_point", "test_simple"
  };
#endif
  testResult_t  aResults[NumElements(szTestPrograms)];
  const char    szHelp[]    = "test_all [-a] [-f filter] [-v[#]]\n";
  const char   *szFilter    = NULL;
  bool_t        fAutomated  = FALSE;
  unsigned      verbose     = 0;
  int           retCode     = TEST_EXIT_PASS;
  unsigned      count;
  int           ret;
  unsigned      i;
  hTestSuite_t  hSuite;

  // process command-line args
  for(i = 1; i < argc; ++i)
  {
    if(strcmp(argv[1], "--help") == 0)
    {
      printf(szHelp);
      exit(1);
    }
    else if(strcmp(argv[i], "-a") == 0)
      fAutomated = TRUE;
    else if((strcmp(argv[i], "-f") == 0) && (i + 1 < argc))
    {
      szFilter = argv[i + 1];
      ++i;
    }
    else if(strncmp(argv[i], "-v", 2) == 0)
    {
      verbose = 1;
      if(isdigit(argv[i][2]))
        verbose = atoi(&argv[i][2]);
    }
  }

  // just to get test header into log file
  NedTestInit(argc, argv);
  hSuite = NedTestNew("TestAllSuite", 0, NULL);
  NedTestFree(hSuite);

  // run each test program
  count = NumElements(szTestPrograms);
  for(i = 0; i < count; ++i)
  {
    if(NtaFilterThis(szTestPrograms[i], szFilter))
      aResults[i] = RESULT_SKIPPED;
    else
    {
      ret = NtaSuiteRun(szTestPrograms[i], fAutomated, verbose);
      if(ret == TEST_EXIT_PASS)
        aResults[i] = RESULT_PASSED;
      else if(ret == TEST_EXIT_FAIL)
      {
        retCode = TEST_EXIT_FAIL;
        aResults[i] = RESULT_FAILED;
      }
      else
      {
        aResults[i] = RESULT_SKIPPED;
      }
    }
  }

  NtaSummary(count, szTestPrograms, aResults);

  // returns first test program failure result
  return retCode;
}
