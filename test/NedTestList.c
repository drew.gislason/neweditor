/*!************************************************************************************************
  @file NedTestList.c

  @brief  Ned List Test Cases

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include "Ned.h"
#include "NedLog.h"
#include "NedTest2.h"
#include "NedList.h"

typedef struct
{
  unsigned    month;
  unsigned    day;
  unsigned    year;
} tlDate_t;

/*!------------------------------------------------------------------------------------------------
  String Compare function for tests.
-------------------------------------------------------------------------------------------------*/
bool_t TlStrIsSame(const void *sz1, const void *sz2)
{
  return (strcmp(sz1, sz2) == 0) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  String Show function for tests.
-------------------------------------------------------------------------------------------------*/
void TlStrShow(const void *ptr)
{
  NedLogPrintf("%s", (const char *)ptr);
}

/*!------------------------------------------------------------------------------------------------
  Date Compare function for tests.
-------------------------------------------------------------------------------------------------*/
bool_t TlDateIsSame(const void *pDate1, const void *pDate2)
{
  const tlDate_t   *pD1 = pDate1;
  const tlDate_t   *pD2 = pDate2;
  bool_t            fIsSame = TRUE;

  if(pD1->month != pD2->month)
    fIsSame = FALSE;
  if(pD1->day != pD2->day)
    fIsSame = FALSE;
  if(pD1->year != pD2->year)
    fIsSame = FALSE;
  return fIsSame;
}

/*!------------------------------------------------------------------------------------------------
  Date Show function for tests.
-------------------------------------------------------------------------------------------------*/
void TlDateShow(const void *ptr)
{
  const tlDate_t    *pDate = ptr;

  const char *aszMonth[] = { "???", "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec" };
  NedLogPrintf("%s-%u-%u", (pDate->month < 1 || pDate->month > 12) ? aszMonth[0] : aszMonth[pDate->month],
    pDate->day, pDate->year);
}

/*!------------------------------------------------------------------------------------------------
  Create a list of strings. 1st string must be longest. Example: ["azul", "bye", "chi", "diet"]
-------------------------------------------------------------------------------------------------*/
bool_t TlCreateStrList(const char **asz, unsigned numItems, unsigned maxItems, hNedList_t *pHList)
{
  hNedList_t  hList = NULL;
  unsigned    i;
  char       *pItem;

  // create list
  hList = NedListNew(maxItems, strlen(asz[0]) + 1, TlStrIsSame, TlStrShow);
  if(NedListMaxItems(hList) != maxItems)
    return FALSE;

  for(i=0; i<numItems; ++i)
  {
    if(!NedListAdd(hList, asz[i], strlen(asz[i]) + 1))
    {
      NedLogPrintf("here 1\n");
      return FALSE;
    }
    if(NedListNumItems(hList) != (i + 1))
    {
      NedLogPrintf("here 2\n");
      return FALSE;
    }
  }

  // should be proper items in list
  if(NedListNumItems(hList) != numItems)
  {
    NedLogPrintf("here 3\n");
    return FALSE;
  }

  // verify contents
  NedListIterStart(hList);
  NedListLogShow(hList, TRUE);
  for(i=0; i<numItems; ++i)
  {
    pItem = NedListNext(hList);
    if(pItem == NULL || !TlStrIsSame(pItem, asz[i]))
    {
      NedLogPrintf("here 4 numItems %u, maxItems %u, got %s, expected %s\n", numItems, maxItems, pItem, asz[i]);
      return FALSE;
    }
  }

  if(!NedListIsList(hList))
  {
    NedLogPrintf("here 5\n");
    return FALSE;
  }

  *pHList = hList;
  return TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Test NedListNew() function

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcListNew(void)
{
  hNedList_t    hList;
  hNedList_t    hList2;
  const char *  asz[]   = { "a", "b", "c", "d"};

  NedTestBegin();

  hList = NedListNew(NumElements(asz) - 1, strlen(asz[0]) + 1, TlStrIsSame, TlStrShow);
  if(!NedListIsList(hList))
    NedTestFailed();

  if(NedListMaxItems(hList) != NumElements(asz) - 1)
    NedTestFailed();

  if(NedListNumItems(hList) != 0)
    NedTestFailed();

  if(!NedListAdd(hList, asz[0], 0))
    NedTestFailed();

  if(NedListNumItems(hList) != 1)
    NedTestFailed();

  hList2 = NedListNew(NumElements(asz), strlen(asz[0]) + 1, TlStrIsSame, TlStrShow);
  if(((NedListMaxItems(hList)  != NumElements(asz) - 1) ||
                  (NedListMaxItems(hList2) != NumElements(asz) )))
  {
    NedTestFailed();
  }

  NedTestEnd();

  NedListFree(hList);
  NedListFree(hList2);
}

/*!------------------------------------------------------------------------------------------------
  Test NedListAdd() function

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcListAdd(void)
{
  hNedList_t    hList;
  hNedList_t    hList2;
  unsigned      i;
  void         *pItem;
  const char *  asz[]   = { "azul", "by", "chi", "diet"};
  const tlDate_t aDate[] =
  {
    { 12, 31, 1999 },
    { 2, 25, 2021 },
    { 3, 14, 2020 }
  };

  NedTestBegin();

  // create two lists
  hList  = NedListNew(NumElements(asz), strlen(asz[0]) + 1, TlStrIsSame, TlStrShow);
  hList2 = NedListNew(NumElements(aDate), sizeof(aDate), TlDateIsSame, TlDateShow);
  if((NedListMaxItems(hList) != NumElements(asz)) || (NedListMaxItems(hList2) != NumElements(aDate)))
    NedTestFailed();

  // add until list is filled
  for(i=0; i<NumElements(asz); ++i)
  {
    if(!NedListAdd(hList, asz[i], strlen(asz[i]) + 1))
      NedTestFailed();
    if((NedListNumItems(hList) != (i + 1)))
      NedTestFailed();
  }

  // list should be full
  if(NedListNumItems(hList) != NedListMaxItems(hList))
    NedTestFailed();

  NedListLogShow(hList, TRUE);

  // verify list
  NedListIterStart(hList);
  for(i=0; i<NumElements(asz); ++i)
  {
    pItem = NedListNext(hList);
    if(pItem == NULL || !TlStrIsSame(pItem, asz[i]))
      NedTestFailed();
  }

  NedListLogShow(hList, TRUE);

  // verify empty list has nothing in it
  if(NedListNext(hList2) != NULL)
    NedTestFailed();

  // add to list of a different type
  if(!NedListAdd(hList2, &aDate[0], 0))
    NedTestFailed();

  // verify added to list
  if((NedListNumItems(hList2) != 1 || NedListMaxItems(hList2) != NumElements(aDate)))
    NedTestFailed();

  NedListLogShow(hList2, TRUE);

  // verify item is the same in the list
  pItem = NedListPrev(hList2);
  if(pItem == NULL || !TlDateIsSame(pItem, &aDate[0]))
    NedTestFailed();

  // verify list made a copy (not same pointer)
  if(pItem == &aDate[0])
    NedTestFailed();

  NedTestEnd();

  NedListFree(hList);
  NedListFree(hList2);
}

/*!------------------------------------------------------------------------------------------------
  Test TcListRemove() function

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcListRemove(void)
{
  hNedList_t    hList   = NULL;
  unsigned      i;
  void         *pItem;
  const char *  asz[]   = { "azul", "by", "chi", "diet"};

  NedTestBegin();

  // create the list
  if(!TlCreateStrList(asz, NumElements(asz), NumElements(asz), &hList))
    NedTestFailed();

  // try to remove something that's not there
  if(NedListRemove(hList, "buba"))
    NedTestFailed();

  // shift down other items. Examples (^ is head, removing 'a'):
  // before: [a,b,c,d]    [c,d,a,b]    [c,d,a,b]    [b,c,d,a]
  //          ^                ^          ^                ^
  // after:  [b,c,d, ]    [c,d,b, ]    [c,d,b, ]    [b,c,d, ]
  //          ^                ^          ^                ^
  if(!NedListRemove(hList, asz[0]))
    NedTestFailed();

  if(NedListNumItems(hList) != (NumElements(asz) - 1))
    NedTestFailed();

  // show removed item in log
  NedListLogShow(hList, TRUE);

  // verfy it's b,c,d
  NedListIterStart(hList);
  for(i=0; i<NumElements(asz)-1; ++i)
  {
    pItem = NedListNext(hList);
    NedLogPrintf("  i %u, pItem %s, asz[i+1] %s\n", i, pItem, asz[i+1]);
    if(!TlStrIsSame(pItem, asz[i+1]))
      NedTestFailed();
  }

  NedTestEnd();
  NedListFree(hList);
}

/*!------------------------------------------------------------------------------------------------
  Test NedListPrev() and NedListNext() functions.

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcListIter(void)
{
  hNedList_t    hList;
  unsigned      i;
  void         *pItem;
  const char *  asz[]   = { "azul", "by", "chi", "diet" };
  const char    szEye[] = "eye";
  const char *  asz2[]  = { "eye", "diet", "chi", "by", "azul" };

  NedTestBegin();

  // create the list
  if(!TlCreateStrList(asz, NumElements(asz), NumElements(asz) + 1, &hList))
    NedTestFailed();

  NedListIterStart(hList);
  pItem = NedListNext(hList);
  if(!TlStrIsSame(pItem, "azul"))
    NedTestFailed();

  NedListIterStart(hList);
  pItem = NedListPrev(hList);
  if(!TlStrIsSame(pItem, "diet"))
    NedTestFailed();

  NedListIterStart(hList);
  for(i=0; i<7; ++i)
  {
    pItem = NedListPrev(hList);
    NedLogPrintf("  pItem Prev %s\n", pItem);
  }
  pItem = NedListNext(hList);
  NedLogPrintf("  pItem Next %s\n", pItem);

  NedListLogShow(hList, TRUE);

  if(!TlStrIsSame(pItem, "chi"))
    NedTestFailed();

  NedLogPrintf("TcListIter about to add eye:\n");
  NedListLogShow(hList, TRUE);
  if(!NedListAdd(hList, szEye, sizeof(szEye)) || (NedListNumItems(hList) != NumElements(asz2)))
    NedTestFailed();

  NedLogPrintf("Added eye:\n");
  NedListLogShow(hList, TRUE);

  NedListIterStart(hList);
  for(i=0; i<NumElements(asz2); ++i)
  {
    pItem = NedListPrev(hList);
    NedLogPrintf("  pItem Prev %s, asz2[i]=%s\n", pItem, asz2[i]);
    if(!TlStrIsSame(pItem, asz2[i]))
      NedTestFailed();
  }

  NedTestEnd();
  NedListFree(hList);
}

/*!------------------------------------------------------------------------------------------------
  String Compare function for tests.
-------------------------------------------------------------------------------------------------*/
bool_t NedSettingsStrIsSame(const void *sz1, const void *sz2)
{
  return (strcmp(sz1, sz2) == 0) ? TRUE : FALSE;
}

/*!------------------------------------------------------------------------------------------------
  String Show function for tests.
-------------------------------------------------------------------------------------------------*/
void NedSettingsStrShow(const void *ptr)
{
  NedLogPrintf("%s", (const char *)ptr);
}


/*!------------------------------------------------------------------------------------------------
  Let user try things

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcListUser(void)
{
  hNedList_t  hList;
  char        sz[100];
  const char *psz;
  unsigned    len;
  unsigned    i;
  const char  szHelp[] = "<ENTER>=show list, n=next, v=prev, -str=remove, str=add, ~=clear, f=fail, p=pass, ?=help\n\n";

  NedTestBegin();

  printf("\n\nAdd, remove things, verify always just 1 of each item in correct order\n");
  printf(szHelp);
  hList = NedListNew(4, sizeof(sz), NedSettingsStrIsSame, NedSettingsStrShow);

  while(1)
  {
    NedListLogShow(hList, TRUE);
    printf("Enter Str: ");
    fflush(stdout);
    memset(sz, 0, sizeof(sz));
    fgets(sz, sizeof(sz)-1, stdin);
    len = strlen(sz);
    if(len && sz[len - 1] == '\n')
    {
      --len;
      sz[len] = '\0';
    }
    if(*sz == '\0')
    {
      if(NedListNumItems(hList) == 0)
        printf("  <empty>\n");
      else
      {
        NedListIterStart(hList);
        for(i=0; i<NedListNumItems(hList); ++i)
        {
          psz = NedListNext(hList);
          printf("  %u: %s\n", i, psz);
        }
        NedListIterStart(hList);
      }
    }
    else if(strcmp(sz, "?") == 0)
    {
      printf(szHelp);
    }
    else if(strcmp(sz, "~") == 0)
    {
      NedListClear(hList);
      printf("List cleared\n");
    }
    else if(strcmp(sz, "n") == 0)
    {
      psz = NedListNext(hList);
      printf("Next is %s\n", psz);
    }
    else if(strcmp(sz, "v") == 0)
    {
      psz = NedListPrev(hList);
      printf("Prev is %s\n", psz);
    }
    else if(strcmp(sz, "p") == 0)
    {
      break;
    }
    else if(strcmp(sz, "f") == 0)
    {
      NedTestFailed();
    }
    else if(sz[0] == '-' && len >= 2)
      NedListRemove(hList, &sz[1]);
    else
      NedListAdd(hList, sz, len + 1);
  }

  NedTestEnd();
  NedListFree(hList);
}


int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcListUser",   TcListUser, TC_USER_INPUT },
    { "TcListNew",    TcListNew },
    { "TcListAdd",    TcListAdd },
    { "TcListRemove", TcListRemove },
    { "TcListIter",   TcListIter }
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestList", NumElements(aTestCases), aTestCases);
  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
