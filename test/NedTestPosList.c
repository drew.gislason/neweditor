#include <ctype.h>
#include "NedPosList.h"
#include "NedTest2.h"

// lines in 12s
static const char szNtPlFile1[] =
{
  "AAAAAAAAAAA\n"
  "BBBBBBBBBBB\n"
  "CCCCCCCCCCC\n"
  "DDDDDDDDDDD\n"
  "EEEEEEEEEEE\n"
};

// lines in 16s
static const char szNtPlFile2[] =
{
  "aaaaaaaaaaaaaaa\n"
  "bbbbbbbbbbbbbbb\n"
  "ccccccccccccccc\n"
  "ddddddddddddddd\n"
  "eeeeeeeeeeeeeee\n"
  "fffffffffffffff\n"
};

/*!------------------------------------------------------------------------------------------------
  Loads a "file" from a NULL terminated string into a buffer chain.

  @ingroup  ned_test
  @param    id          Identifier for buffer chain
  @param    bufSize     size of each buffer in the chain
  @param    szFile      NULL terminated string

  @returns   pointer to bufHead of buffer chain, or NULL if failed.
*///-----------------------------------------------------------------------------------------------
static sNedFile_t * NptFileLoads(sNedFile_t *pFileList, unsigned id, const char *szFile)
{
  sNedFile_t   *pFile;
  char          szName[8];

  strncpy(szName, szFile, sizeof(szName));
  szName[sizeof(szName)-1] = '\0';

  pFile = NedFileNew(pFileList, szName);
  if(pFile && !NedPointInsStr(&pFile->sCursor, (void *)szFile))
  {
    NedFileFree(pFileList, pFile);
    pFile = NULL;
  }

  return pFile;
}

/*!------------------------------------------------------------------------------------------------
  Test advanced search functions WholeWord, Wrap

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcPosList(void)
{
  sNedFile_t   *apFile[2];
  const char   *aszFile[] = { szNtPlFile1, szNtPlFile2 };
  sNedPoint_t   sTop;
  sNedPoint_t   sBot;
  sNedPos_t     sPos;
  sNedPos_t    *pPos;
  unsigned      aPos[] = { 0, 1, 7, 12, 20, 31 };
  unsigned      aPos2[] = { 0, 1, 7, 17, 25, 36 };
  unsigned      aPos3[] = { 0, 1, 13, 24 };
  hNedList_t    hList;
  unsigned      i;
  unsigned      j;

  NedTestBegin();

  // create a couple of files for adding points
  NedBufSetMaxLen(32);
  for(i=0 ; i < NumElements(apFile); ++i)
  {
    apFile[i] = NptFileLoads(NULL, i + 1, aszFile[i]);
    if(apFile[i] == NULL)
      NedTestFailed();
    if(NedTestVerbose())
      NedLogPrintf("pFile[%u].id = %u\n", i, NedBufId(apFile[i]->pBufHead));
  }

  // create a list to hold the points
  hList = NedListNew(NumElements(aPos) * 2, sizeof(sNedPos_t), NedPosListIsSame, NedPosListLogShow);
  if(!hList)
    NedTestFailed();

  // create some sNedPos_t items int 2 files in the list
  for(i = 0; i < NumElements(aPos) * NumElements(apFile); ++i)
  {
    sPos.pFile  = apFile[i % NumElements(apFile)];
    sPos.pos    = aPos[i / NumElements(apFile)];
    if(!NedListAdd(hList, &sPos, 0))
      NedTestFailed();
  }
  if(NedTestVerbose())
  {
    NedLogPrintf("Created:\n");
    NedListLogShow(hList, TRUE);
  }
  if(NedListNumItems(hList) != NumElements(aPos) * NumElements(apFile))
    NedTestFailed();

  // add some text to file1
  NedPointInit(&sTop, apFile[1]->pBufHead);
  NedPointFileGotoPos(&sTop, 12);
  NedPosListAdjustIns(hList, &sTop, 5);
  if(NedTestVerbose())
  {
    NedLogPrintf("Inserted 5:\n");
    NedListLogShow(hList, TRUE);
  }

  // should not have added or removed any sNedPos_t items
  if(NedListNumItems(hList) != NumElements(aPos) * NumElements(apFile))
    NedTestFailed();

  // verify points have moved (or not) by proper amount
  NedListIterStart(hList);
  for(i=0; i < NedListNumItems(hList); ++i)
  {
    pPos = NedListNext(hList);

    // verify points after pos 12 moved
    if(pPos->pFile == apFile[1])
    {
      if(pPos->pos != aPos2[i / NumElements(apFile)])
      {
        NedLogPrintf("got %ld, expected %ld\n", pPos->pos, aPos2[i / NumElements(apFile)]);
        NedTestFailed();
      }
    }

    // verify points in file0 did not move
    else
    {
      if(pPos->pos != aPos[i / NumElements(apFile)])
      {
        NedLogPrintf("got %ld, expected %ld\n", pPos->pos, aPos[i / NumElements(apFile)]);
        NedTestFailed();
      }
    }
  }

  // delete some text from file2. Should have also deleted 2 points
  NedPointInit(&sTop, apFile[0]->pBufHead);
  NedPointFileGotoPos(&sTop, 6);
  NedPointInit(&sBot, apFile[0]->pBufHead);
  NedPointFileGotoPos(&sBot, 13);
  NedPosListAdjustDel(hList, &sTop, &sBot);
  if(NedTestVerbose())
  {
    NedLogPrintf("Deleted 6:\n");
    NedListLogShow(hList, TRUE);
  }

  // should removed 2 points (inside the deleted area) of apFile[0]
  if(NedListNumItems(hList) != (NumElements(aPos) * NumElements(apFile)) - 2)
  {
    NedLogPrintf("Got %u items, expected %u\n", NedListNumItems(hList), (NumElements(aPos) * NumElements(apFile)) - 2);
    NedTestFailed();
  }

  j = 0;
  NedListIterStart(hList);
  for(i=0; i < NedListNumItems(hList); ++i)
  {
    pPos = NedListNext(hList);
    if(pPos->pFile == apFile[0])
    {
      if(pPos->pos != aPos3[j])
      {
        NedLogPrintf("%u: got %ld, expected %ld\n", i, pPos->pos, aPos3[j]);
        NedTestFailed();
      }
      ++j;
    }
  }

  NedPosListRemoveAll(hList, apFile[0]);
  if(NedTestVerbose())
  {
    NedLogPrintf("RemoveAll:\n");
    NedListLogShow(hList, TRUE);
  }
  if(NedListNumItems(hList) != NumElements(aPos) * (NumElements(apFile) - 1))
    NedTestFailed();

  NedTestEnd();
}

/*-------------------------------------------------------------------------------------------------
  Run each test case
-------------------------------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcPosList",        TcPosList }
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestPosList", NumElements(aTestCases), aTestCases);
  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
