#include <stdio.h>
#include "NedJson.h"
#include "NedLog.h"

typedef struct
{
  unsigned    valHello;
  char        szWorld[9];
} sHelloWorld_t;

// handle asserts and logging
void NedError(const char *szExpr, const char *szFile, const char *szFunc, unsigned line)
{
  printf("NedAssert: (%s), file: %s, func: %s(), line: %u\n",szExpr, szFile, szFunc, line);
  exit(1);
}
int NedLogPrintf(const char *szFormat, ...) { (void)szFormat; return 0; }
int NedLogPrintfEx(nedLogMask_t mask, const char *szFormat, ...) { (void)mask; (void)szFormat; return 0; }

int main(void)
{
  // {"hello":1,"world":"earth"}
  const char          szJsonIn []                 = "{\"hello\":1,\"world\":\"earth\"}";
  char                szJsonOut[sizeof(szJsonIn)];
  sHelloWorld_t       sHello = { 0 };
  const sJ2cKeyVal_t  aKeyVals[] =
  {
    {"hello", &sHello.valHello, J2C_UNSIGNED},
    {"world", &sHello.szWorld,  J2C_STRING, 0, sizeof(sHello.szWorld)}
  };
  uint8_t           aIncluded[J2cBits2Bytes(NumElements(aKeyVals))];
  const sJ2cObj_t   sObj = { NumElements(aKeyVals), aKeyVals, aIncluded };

  printf("\nNedJson Hello World Example\n\n");
  printf("Sizes   : sJ2cKeyVal_t=%zu, sJ2cObj_t=%zu\n\n", sizeof(sJ2cKeyVal_t), sizeof(sJ2cObj_t));

  printf("Decoding: %s\n", szJsonIn);
  if(J2cDecode(&sObj, szJsonIn))
  {
    printf("Decoded : hello=%u, world=%s\n", sHello.valHello, sHello.szWorld);
  }

  if(J2cEncode(szJsonOut, sizeof(szJsonOut), &sObj, FALSE))
  {
    printf("Encoded : %s\n", szJsonOut);
    if(strcmp(szJsonIn, szJsonOut) != 0)
      printf("\nError! Something went wrong. Not the same!\n");
    else
      printf("\nSuccess!\n");
  }

  return 0;
}
