#!/bin/bash
for file in ./test_*
do
  $file
  if [ "$?" != "0" ]; then
  echo "$file failed"
    exit 1
  fi
done

echo ""
echo '--------------------------------'
echo "     Ned Tests All Passed"
echo '--------------------------------'
