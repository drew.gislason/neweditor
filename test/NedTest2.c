/*!************************************************************************************************
  NedTest.c

  @brief  Ned Test Framework

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_test Ned test framework and test cases

  The Ned test framework has the following features

  Usage = TestSuite [test_suite_args] [-a] [-f filter] [-m] [-v]

  @ingroup  ned_test

*///***********************************************************************************************
#include <time.h>
#include <stdarg.h>
#include <ctype.h>
#include "NedTest2.h"
#include "NedUtilStr.h"
#include "NedUtilFile.h"

typedef enum
{
  RESULT_SKIPPED = 0,
  RESULT_PASSED,
  RESULT_FAILED
} testResult_t;

typedef struct
{
  const char         *szSuiteName;
  unsigned            numCases;
  unsigned            curCase;
  const sTestCase_t  *pTestCases;
  testResult_t       *aResult;
  unsigned            passCount;
  unsigned            failCount;
  unsigned            skipCount;
  bool_t              fStopped;
} sTestSuite_t;

static void               NT_Color  (unsigned iColor);
static void               NT_Line   (void);
static const sTestCase_t *NT_Case   (void);

static sTestSuite_t  *m_pSuite;
static unsigned       m_verbose;          // -v[#]
static bool_t         m_fAutomatedOnly;   // -a
static bool_t         m_fMachineReadable; // -m
static const char    *m_szFilter;         // -f filter
const char            m_szColumn[]  = "%-40s";

// for machine readable parsing
const char m_szMTestStart[]    = "$TEST_START";
const char m_szMTestPassed[]   = "$PASSED";
const char m_szMTestFailed[]   = "$FAILED";
const char m_szMTestSkipped[]  = "$SKIPPED";
const char m_szRetCodeFile[]   = "testall.tmp"; // see NedTestAll.c

/*---------------------------------------------------------------------------
  Process command-line and Display header info into log file
---------------------------------------------------------------------------*/
void NedTestInit(int argc, char *argv[])
{
  const char szBanner[] =
  {
    "\n\n"
    "=======  -----  -----   ---  -----  =======\n"
    "_______    |    |__    /       |    _______\n"
    "           |    |      ----    |\n"
    "=======    |    |____  ___/    |    =======\n"
    "\n\n"
  };
  const char szHelp[] = "\nUsage = %s [test_suite_args] [-a] [-f filter] [-m] [-v[#]]\n\n";
  int       i;
  char     *psz;
  bool_t    fGotIt;

  m_pSuite            = NULL;
  m_verbose           = 0;
  m_fAutomatedOnly    = FALSE;
  m_szFilter          = NULL;
  m_fMachineReadable  = FALSE;
  for(i = 1; i < argc; ++i)
  {
    if(strcmp(argv[1], "--help") == 0)
    {
      printf(szHelp, argv[0]);
      exit(1);
    }
    else if(strcmp(argv[i], "-a") == 0)
      m_fAutomatedOnly = TRUE;
    else if((strcmp(argv[i], "-f") == 0) && (i + 1 < argc))
    {
      m_szFilter = argv[i + 1];
      ++i;
    }
    else if(strcmp(argv[i], "-m") == 0)
      m_fMachineReadable = TRUE;
    else if(strncmp(argv[i], "-v", 2) == 0)
    {
      m_verbose = 1;
      if(isdigit(argv[i][2]))
        m_verbose = atoi(&argv[i][2]);
    }
  }

  // if -m, then don't print banner, as it's part of a bigger suite
  if(!m_fMachineReadable)
  {
    // display banner and basic info
    NedLogPrintf("%sc version %lu, datetime of test run: %s\nargc %d argv { ", szBanner, __STDC_VERSION__,
                 NedStrDateTimeCur(), argc);
    for(i=1; i<argc; ++i)
      NedLogPrintf("%s ",argv[i]);
    NedLogPrintf("}\n");

    // display Git SHA version of NED
    fGotIt = FALSE;
    NedLogPrintf("\nSHA of project: ");
    if(system("git log -n 1 >nedtest2.tmp") == 0)
    {
      psz = NedFileRead("nedtest2.tmp");
      if(psz)
      {
        fGotIt = TRUE;
        NedLogPrintf("%s\n", psz);
        free(psz);
      }
    }
    if(!fGotIt)
      NedLogPrintf("SHA not found! Perhaps Git not installed?\n");

    // display gcc version
    fGotIt = FALSE;
    NedLogPrintf("\nC Version Info: ");
    if(system("gcc --version >nedtest2.tmp 2>nedtest2.tmp2") == 0)
    {
      psz = NedFileRead("nedtest2.tmp");
      if(psz)
      {
        fGotIt = TRUE;
        NedLogPrintf("%s\n\n", psz);
        free(psz);
      }
    }
    if(!fGotIt)
      NedLogPrintf("C Version not found! Perhaps gcc not installed?\n\n");
    remove("nedtest2.tmp");
    remove("nedtest2.tmp2");

    printf("verbose %u, machine %u\n", m_verbose, m_fMachineReadable);

    NedLogPrintf("%s\n\n", m_szMTestStart);
  }
}

/*!------------------------------------------------------------------------------------------------
  Create a test suite from test cases

  @param    szSuiteName     ptr to persistent string
  @param    numCases        number of test cases
  @param    pTestCases      pointer to peristent array of test cases

  @returns  length of printed string
*///-----------------------------------------------------------------------------------------------
hTestSuite_t NedTestNew(const char *szSuiteName, unsigned numCases, const sTestCase_t *pTestCases)
{
  sTestSuite_t   *pSuite;

  pSuite = malloc(sizeof(sTestSuite_t));
  if(pSuite)
  {
    memset(pSuite, 0, sizeof(sTestSuite_t));
    pSuite->szSuiteName = szSuiteName;
    pSuite->numCases    = numCases;
    pSuite->pTestCases  = pTestCases;
    if(numCases)
      pSuite->aResult = malloc(numCases * sizeof(testResult_t));
  }
  return (hTestSuite_t)pSuite;
}

/*!------------------------------------------------------------------------------------------------
  Free the test suite

  @param    hSuite          handle to test suite
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedTestFree(hTestSuite_t hSuite)
{
  if(hSuite)
  {
    if(hSuite == m_pSuite)
      m_pSuite = NULL;
    free(hSuite);
  }
}

/*!------------------------------------------------------------------------------------------------
  Free the test suite

  @ingroup  ned_msg
  @param    hSuite          handle to test suite

  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedTestRun(hTestSuite_t hSuite)
{
  sTestSuite_t   *pSuite = hSuite;
  bool_t          fSkipped;

  NedAssert(hSuite);

  NedLogPrintf("TEST SUITE: %s\n", pSuite->szSuiteName);
  m_pSuite              = pSuite;
  pSuite->passCount     = 0;
  pSuite->failCount     = 0;
  pSuite->skipCount     = 0;
  pSuite->fStopped      = FALSE;
  for(pSuite->curCase = 0; pSuite->curCase < pSuite->numCases; ++pSuite->curCase)
  {
    fSkipped = FALSE;
    if(m_szFilter && (strstr(NT_Case()->szName, m_szFilter) == 0))
      fSkipped  = TRUE;
    if(m_fAutomatedOnly && (NT_Case()->opts & TC_USER_INPUT))
      fSkipped  = TRUE;

    // run the case (or indicated skipped)
    if(fSkipped)
    {
      NT_Begin();
      NT_Skipped();
    }
    else if(NT_Case()->pfnTestCase)
      (NT_Case()->pfnTestCase)();

    if(pSuite->fStopped)
      break;
  }

  // no longer in test suite
  m_pSuite = NULL;
}

/*!------------------------------------------------------------------------------------------------
  Stop the test run. Used by test cases that know continuing would be bad

  @ingroup  ned_test
  @param    hSuite    valid test suite

  @returns  TRUE if verbose, FALSE if not
*///-----------------------------------------------------------------------------------------------
void NedTestStop(hTestSuite_t hSuite)
{
  sTestSuite_t   *pSuite = hSuite;
  pSuite->fStopped = TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Summarize the test in both log and on-screen. Assumes any full-screen stuff is done.

  @ingroup  ned_test
  @param    hSuite    valid test suite

  @returns  0 if all tests passed, 1 if any tests failed, 2 if all tests skipped
*///-----------------------------------------------------------------------------------------------
int NedTestSummary(hTestSuite_t hSuite)
{
  sTestSuite_t   *pSuite = hSuite;
  int             ret;
  char            szRetCode[6];
  FILE           *fp;

  m_pSuite = pSuite;

  NT_Color(TESTATTR_NORMAL);
  printf("\nSUITE SUMMARY: %s -- opts: %s%sfilter %s\n", pSuite->szSuiteName, m_verbose ? "verbose " : "",
    m_fAutomatedOnly ? "automated only " : "", m_szFilter ? m_szFilter : "(none)");
  NedLogPrintf("\nSUITE SUMMARY: %s -- opts: %s%sfilter %s\n", pSuite->szSuiteName, m_verbose ? "verbose " : "",
    m_fAutomatedOnly ? "automated only " : "", m_szFilter ? m_szFilter : "(none)");
  NT_Line();

  // summarize to screen
  NT_Color(TESTATTR_PASSED);
  printf(m_szColumn, "Passed");
  printf("%u\n",pSuite->passCount);
  NT_Color(TESTATTR_FAILED);
  printf(m_szColumn, "Failed");
  printf("%u\n", pSuite->failCount);
  NT_Color(TESTATTR_SKIPPED);
  printf(m_szColumn, "Skipped");
  printf("%u\n", pSuite->skipCount);
  NT_Color(TESTATTR_NORMAL);

  // summarize to log
  NedLogPrintf(m_szColumn, "Passed");
  NedLogPrintf("%u\n", pSuite->passCount);
  NedLogPrintf(m_szColumn, "Failed");
  NedLogPrintf("%u\n", pSuite->failCount);
  NedLogPrintf(m_szColumn, "Skipped");
  NedLogPrintf("%u\n", pSuite->skipCount);

  NT_Line();

  // return 0=pass, 1=fail, 2=skipped
  // if any failures, that takes precedence
  // if some skipped and some passed, that is still a pass
  // if all skipped, then skipped
  if(pSuite->failCount > 0)
    ret = TEST_EXIT_FAIL;
  else if(pSuite->passCount == 0 && pSuite->skipCount > 0)
    ret = TEST_EXIT_SKIP;
  else
    ret = TEST_EXIT_PASS;

  // for NedTestAll, to pass return this code, as system() does not
  remove(m_szRetCodeFile);
  if(m_fMachineReadable)
  {
    fp = fopen(m_szRetCodeFile, "w");
    if(fp)
    {
      sprintf(szRetCode, "%d\n", ret);
      fwrite(szRetCode, 1, strlen(szRetCode), fp);
      fclose(fp);
    }
  }

  return ret;
}

/*!------------------------------------------------------------------------------------------------
  Small banner for log, shows test case name and result

  @ingroup  ned_test
  @returns  TRUE if verbose, FALSE if not
*///-----------------------------------------------------------------------------------------------
void NT_End(unsigned attr, const char *szResult)
{
  static const char     m_szEndLine[] = "----- %s(%s) -----\n";

  // Passed, Failed, Skipped
  if(!(NT_Case()->opts & TC_SCREEN))
  {
    NT_Color(attr);
    printf("%s\n", szResult);
  }

  if(strcmp(szResult, "Passed") == 0)
    szResult = m_szMTestPassed;
  if(strcmp(szResult, "Skipped") == 0)
    szResult = m_szMTestSkipped;

  NedLogPrintf(m_szEndLine, NT_Case()->szName, szResult);
}

/*-------------------------------------------------------------------------------------------------
  Internal function to indicate failed with small end banner
-------------------------------------------------------------------------------------------------*/
void NT_Failed(const char *szExpr, const char *szFile, unsigned line)
{
  static const char     m_szEndLine[] = "----- %s(%s) -----\n";

  ++m_pSuite->failCount;
  m_pSuite->aResult[m_pSuite->curCase] = RESULT_FAILED;
  NedLogPrintf("%s:%u:1: failed %s: %s\n", szFile, line, NT_Case()->szName, szExpr);

  // display to screen in color
  if(!(NT_Case()->opts & TC_SCREEN))
  {
    NT_Color(TESTATTR_FAILED);
    printf("%s, File: %s, line %u\n", "Failed", szFile, line);
  }

  NedLogPrintf(m_szEndLine, NT_Case()->szName, m_szMTestFailed);
}

/*-------------------------------------------------------------------------------------------------
  Internal function to indicate skipped with small end banner
-------------------------------------------------------------------------------------------------*/
void NT_Skipped(void)
{
  ++m_pSuite->skipCount;
  m_pSuite->aResult[m_pSuite->curCase] = RESULT_SKIPPED;
  NT_End(TESTATTR_SKIPPED, "Skipped");
}

/*-------------------------------------------------------------------------------------------------
  Internal function to indicate passed with small end banner
-------------------------------------------------------------------------------------------------*/
void NT_Passed(void)
{
  ++m_pSuite->passCount;
  m_pSuite->aResult[m_pSuite->curCase] = RESULT_PASSED;
  NT_End(TESTATTR_PASSED, "Passed");
}

/*!------------------------------------------------------------------------------------------------
  Indicates the name of the test with small banner

  @ingroup  ned_test
  @returns  TRUE if verbose, FALSE if not
*///-----------------------------------------------------------------------------------------------
void NT_Begin(void)
{
  // to screen
  if(!(NT_Case()->opts & TC_SCREEN))
  {
    NT_Color(TESTATTR_NORMAL);
    printf(m_szColumn, NT_Case()->szName);
  }

  // log to debug.log
  NedLogPrintf("----- %s -----\n", NT_Case()->szName);
}

/*!------------------------------------------------------------------------------------------------
  Print to (optionally screen) and to log

  @ingroup  ned_msg
  @param    szFormat    constant format string
  @param    ...         optional variable number of parameters

  @returns  length of printed string
*///-----------------------------------------------------------------------------------------------
int NedTestPrintf(const char *szFormat, ...)
{
  char        szLine[1024];
  va_list     arglist;
  int         len;

  // print, and clear to end of line
  va_start(arglist, szFormat);
  len = vsnprintf(szLine, sizeof(szLine) - 1, szFormat, arglist);
  va_end(arglist);

  if(m_verbose || (NT_Case()->opts & TC_VERBOSE))
  {
    NedLogPrintf("%s", szLine);
    if(!(NT_Case()->opts & TC_SCREEN))
      printf("%s", szLine);
  }

  return len;
}

/*!------------------------------------------------------------------------------------------------
  Run tests in automated mode only?

  @ingroup  ned_test
  @returns  TRUE if automated tests only, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedTestAutomated(void)
{
  return m_fAutomatedOnly;
}

/*!------------------------------------------------------------------------------------------------
  What level is verbose on for this test case?

  @returns  verbose level (0-n)
*///-----------------------------------------------------------------------------------------------
unsigned NedTestVerbose(void)
{
  unsigned    verboseLevel = m_verbose;

  if(m_pSuite && (NT_Case()->opts & TC_VERBOSE))
    ++verboseLevel;

  return verboseLevel;
}

/*!------------------------------------------------------------------------------------------------
  Turn verbose on or off

  @param    verboseLevel
  @returns  none
*///-----------------------------------------------------------------------------------------------
void NedTestVerboseSet(unsigned verboseLevel)
{
  m_verbose = verboseLevel;
}

/*-------------------------------------------------------------------------------------------------
  Gets keyboard input using getchar() with 'p' for pass, 'f' for fail, Returns TRUE if passed,
  FALSE if not.

  @returns  TRUE if passed, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedTestPassFail(void)
{
  bool_t    fPassed = TRUE;
  char      sz[5];

  printf("Press <enter> for pass, 'f'<enter> for fail: ");
  *sz = '\0';
  fgets(sz, sizeof(sz)-1, stdin);
  if(*sz == 'f' || *sz == 'F')
    fPassed = FALSE;

  return fPassed;
}

/*!------------------------------------------------------------------------------------------------
  Get command-line arguments (argv and argc). 

  @ingroup    ned_test
  @param      pArgc   pointer to recieve argc

  @returns    argv (return char**), and argc (parm int *pArgc)
*///-----------------------------------------------------------------------------------------------
unsigned NedTestRandRange(unsigned low, unsigned high)
{
  static bool_t fInit = TRUE;

  if(fInit)
  {
    srand(time(0));
    fInit = FALSE;
  }
  return low + (unsigned)(random()%(high-low));
}

/*---------------------------------------------------------------------------
  Print a line
---------------------------------------------------------------------------*/
static void NT_Line(void)
{
  char      szBuffer[51];

  memset(szBuffer, '-', sizeof(szBuffer));
  szBuffer[sizeof(szBuffer)-1] = '\0';

  printf("%s\n", szBuffer);
  NedLogPrintf("%s\n", szBuffer);
}

/*---------------------------------------------------------------------------
  Set the color
---------------------------------------------------------------------------*/
static void NT_Color(unsigned color)
{
  AnsiSetAttr((nedAttr_t)color);
}

/*-------------------------------------------------------------------------------------------------
  Internal function: returns ptr to current suite suite case
-------------------------------------------------------------------------------------------------*/
static const sTestCase_t * NT_Case(void)
{
  const sTestCase_t *pTestCase = NULL;

  NedAssert(m_pSuite);
  pTestCase = &m_pSuite->pTestCases[m_pSuite->curCase];

  return pTestCase;
}


/*--------------------------------------------------------------------------------------------------
  Return count of substring needles found in large string haystack

  pszHaystack   large string to search
  pszNeedle     substring to count within haystack
  returns       count of needles in haystack (0-n)
--------------------------------------------------------------------------------------------------*/
unsigned Nt2Count(const char *pszHaystack, const char *pszNeedle)
{
  const char   *psz;
  unsigned      count = 0;

  psz = pszHaystack;
  while(psz)
  {
    psz = strstr(psz, pszNeedle);
    if(psz)
    {
      ++count;
      psz += strlen(pszNeedle);
    }
  }

  return count;
}

/*!------------------------------------------------------------------------------------------------
  Calculate totals from log file

  @ingroup  ned_test
  @param    pPassed    pointer to receive passed count
  @param    pFailed    pointer to receive failed count
  @param    pSkipped   pointer to receive skipped count

  @returns  TRUE if log file contains test results, FALSE if not
*///-----------------------------------------------------------------------------------------------
bool_t NedTestCalcLogTotals(unsigned *pPassed, unsigned *pFailed, unsigned *pSkipped)
{
  const char       *pszFile;
  const char       *pszStart;
  const char       *psz;
  unsigned          passed  = 0;
  unsigned          failed  = 0;
  unsigned          skipped = 0;
  bool_t            fWorked = TRUE;

  // read the log file
  NedLogFileClose();
  pszFile = NedFileRead(szNedLogDefaultName);
  if(!pszFile)
    fWorked = FALSE;

  if(fWorked)
  {
    // find last test suite run (last $TEST_START)
    psz = pszFile;
    pszStart = NULL;
    while(psz)
    {
      psz = strstr(psz, m_szMTestStart);
      if(psz != NULL)
      {
        pszStart = psz;
        psz += strlen(m_szMTestStart);
      }
    }

    if(pszStart == NULL)
      fWorked = FALSE;
    else
    {
      passed  = Nt2Count(pszStart, m_szMTestPassed);
      failed  = Nt2Count(pszStart, m_szMTestFailed);
      skipped = Nt2Count(pszStart, m_szMTestSkipped);
    }

    free((void *)pszFile);
  }

  *pPassed  = passed;
  *pFailed  = failed;
  *pSkipped = skipped;
  return fWorked;
}
