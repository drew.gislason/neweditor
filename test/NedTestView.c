/*!************************************************************************************************
  @file NedTest View.c

  @brief  Test Ned  View functions

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @ingroup  ned_test

  This file tests the NedView subsystem.

*///***********************************************************************************************
#include <ctype.h>
#include "NedTest2.h"
#include "NedLog.h"
#include "NedLanguage.h"
#include "NedFile.h"
#include "NedWin.h"
#include "NedMsg.h"
#include "NedMenu.h"
#include "NedView.h"

typedef struct
{
  hNedView_t    hView;
  sNedFile_t   *pFile;
  long          topLine;
  long          leftCol;
  long          selectOffset; // offset from cursor, may be negative
  const char   *szPrompt;
} sSelectInfo_t;

static const char szTabFile[] = {
  "# for test_view test case TcViewTabs()\n"
  "\n"
  "def myfunc():\n"
  "\t# line containing....\ttabs\n"
  "    # line containing....   spaces\n"
  "\n"
  "\ttabline   = \"this is a line\tstarting with tabs\"\t\n"
  "\tif True:\n"
  "\t\t# has tabs\n"
  "    spaceline = \"this is a line starting with spaces\"\n"
  "    if True:\n"
  "        # has spaces\n"
  "\n"
  "# non-printables\x01\xff\x00:\n"
};

static const char szTabFileLong[] = {
  "The quick brown fox jumped over the lazy dog's `back1`.\tThe quick brown fox jumped over the lazy dog's back2.\tThe quick brown fox jumped over the lazy dog's back3.\tThe quick brown fox jumped over the lazy dog's back4.\tThe quick brown fox jumped over the lazy dog's back5.\n"
  "\t\tEvery good boy does fine1. Every good boy does `fine2`. Every good boy does fine3. Every good boy does fine4. Every good boy does fine5. Every good boy does fine6. Every good boy does fine7. Every good boy does fine8. Every good boy does fine9. Every good boy does fine10. Every good boy does fine11.\n"
  "\tdone\n"
};
static unsigned tabFileLines = 3; // must match above

/*-------------------------------------------------------------------------------------------------
  Stub
-------------------------------------------------------------------------------------------------*/
const sNedMenuItem_t *NedMenuItemPtr(void *hMenu, unsigned nMenu, unsigned nItem)
{
  (void)hMenu;
  (void)nMenu;
  (void)nItem;
  return NULL;
}

/*-------------------------------------------------------------------------------------------------
  Stub
-------------------------------------------------------------------------------------------------*/
void * NedMenuNew(sNedMenuBar_t *pMenuBar, nedThemeIndex_t themeIndex)
{
  (void)pMenuBar;
  (void)themeIndex;
  return NULL;
}

/*-------------------------------------------------------------------------------------------------
  Ask user to verify selected
-------------------------------------------------------------------------------------------------*/
static bool_t TnvVerifySelected(const sSelectInfo_t *pSelectInfo)
{
  sNedFile_t     *pFile;
  hNedView_t      hView;
  nedKey_t        c;

  // given inputs, set up pFile. Adusts the pFile structure
  hView = pSelectInfo->hView;
  pFile = pSelectInfo->pFile;
  pFile->curLine = NedFileLineGet(pFile, &pFile->sCursor, &pFile->curCol);
  pFile->topLine = pSelectInfo->topLine;
  pFile->leftCol = pSelectInfo->leftCol;
  pFile->sSelect = pFile->sCursor;
  if(pSelectInfo->selectOffset != 0)
    NedPointCharAdvance(&pFile->sSelect, pSelectInfo->selectOffset);
  NedStatusBarSetRowCol(pFile->curLine, pFile->curCol);

  // visualize the file
  NedViewInvalidateAll(hView);
  NedViewDraw(hView, pFile);
  NedStatusBarSetFile(NedFilePath(pFile));
  NedStatusBarSetTab(pFile->fHardTabs, pFile->tabSize, pFile->fCrLf);
  NedStatusBarShow();

  // get user verification of visual data
  c = NedMsgChoice(pSelectInfo->szPrompt, NULL, "yn");
  return (c == 'y');
}

/*-------------------------------------------------------------------------------------------------
  Load a list of files. Returns NULL if one or more file failed to load. Not expected to fail.
-------------------------------------------------------------------------------------------------*/
sNedFile_t * TnvLoadFiles(const char *aszFilenames[], unsigned count)
{
  unsigned      i;
  sNedFile_t   *pFileList = NULL;
  sNedFile_t   *pFile;

  for(i=0; i<count; ++i)
  {
    pFile = NedFileNew(pFileList, aszFilenames[i]);
    if(pFileList == NULL)
      pFileList = pFile;
    if(!pFile)
      return NULL;
  }

  return pFileList;
}

/*!------------------------------------------------------------------------------------------------
  Test view on all different file types, as per language.

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcViewFiles(void)
{
  hNedView_t      hView;
  const char     *aszFilenames[] = { "tcdata_view.c", "tcdata_view.py", "tcdata_view.rs" };
  sNedFile_t     *pFileList = NULL;
  sNedFile_t     *pFile;
  nedTheme_t     *pTheme = NedThemeGet(NEDTHEME_NATURAL);
  unsigned        i;
  nedKey_t        c;

  NedTestBegin();

  NedViewInit();
  hView = NedViewNew(pTheme);
  if(!NedViewIsView(hView))
    NedTestFailed();
  NedViewCls(hView);

  // read the files
  pFileList = TnvLoadFiles(aszFilenames, NumElements(aszFilenames));
  if(pFileList == NULL)
    NedTestFailed();

  // display files and ask use if they look OK
  NedWinPickBackdrop();
  NedWinGoto(0,0);
  NedWinSetAttr(pTheme->text);
  NedWinClearEol();

  pFile = pFileList;
  for(i=0; i<NumElements(aszFilenames); ++i)
  {
    NedViewInvalidateAll(hView);
    NedViewDraw(hView, pFile);
    NedStatusBarSetFile(NedFilePath(pFile));
    NedStatusBarSetRowCol(1L, 1L);
    NedStatusBarShow();
    c = NedMsgChoice("Does the file look colored properly?", NULL, "yn");
    if(c != 'y')
      NedTestFailed();

    pFile = pFile->pNext;
  }

  NedTestEnd();

  NedMsgClear();
  NedFileFreeAll(pFileList);
  NedViewFree(hView);
}

/*!------------------------------------------------------------------------------------------------
  Test Resizing view. Make sure it works.

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcViewResize(void)
{
  hNedView_t      hView;
  const char     *aszFilename = "tcdata_view.rs";
  const char     *aszPrompts[] =
                  {
                    "Is screen clear?",
                    "resize window and press enter",
                    "Look ok? If so, press enter and resize again",
                    "still look OK?"
                  };
  sNedFile_t     *pFileList = NULL;
  sNedFile_t     *pFile;
  nedTheme_t     *pTheme = NedThemeGet(NEDTHEME_NATURAL);
  unsigned        i;
  nedKey_t        c;

  NedTestBegin();

  NedViewInit();
  hView = NedViewNew(pTheme);
  if(!NedViewIsView(hView))
    NedTestFailed();
  NedViewCls(hView);

  // read in a file for resizing
  pFileList = pFile = NedFileNew(pFileList, aszFilename);
  if(pFileList == NULL)
    NedTestFailed();

  // ask user to resize 3 times
  // clear the top line (normally menu)
  NedWinPickBackdrop();
  NedWinGoto(0,0);
  NedWinSetAttr(pTheme->text);
  NedWinClearEol();

  pFile = pFileList;
  for(i=0; i<NumElements(aszPrompts); ++i)
  {
    // check for cls 1st time
    if(i == 0)
      NedViewCls(hView);
    else
    {
      // draw status bar 1st. NedViewDraw() should never overwrite.
      NedStatusBarSetFile(NedFilePath(pFile));
      NedStatusBarSetRowCol(1L, 1L);
      NedStatusBarShow();

      NedViewInvalidateAll(hView);
      NedViewDraw(hView, pFile);
    }
    c = NedMsgChoice(aszPrompts[i], NULL, "yn");
    if(c != 'y')
      NedTestFailed();

    // check for screen resize
    if((i > 0) && (i + 1 < NumElements(aszPrompts)) && !NedViewCheckScreenSize(hView))
      NedTestFailed();
  }


  NedTestEnd();

  NedMsgClear();
  NedFileFreeAll(pFileList);
  NedViewFree(hView);
}

/*!------------------------------------------------------------------------------------------------
  Test view on all different file types, as per language.

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcViewTheme(void)
{
  hNedView_t      hView;
  const char     *aszFilename = "tcdata_view.rs";
  const char     *aszPrompts[] =
                  {
                    "Is file shown in dark mode?",
                    "Is file shown in 1980s green?",
                    "Is file shown in classic Ned?",
                    "Is file back to Natural?"
                  };
  unsigned        aThemes[] = { NEDTHEME_DARKMODE, NEDTHEME_1980s, NEDTHEME_CLASSIC, NEDTHEME_NATURAL };
  sNedFile_t     *pFileList = NULL;
  sNedFile_t     *pFile;
  nedTheme_t     *pTheme = NedThemeGet(NEDTHEME_NATURAL);
  unsigned        i;
  nedKey_t        c;

  NedTestBegin();

  NedViewInit();
  hView = NedViewNew(pTheme);
  if(!NedViewIsView(hView))
    NedTestFailed();
  NedViewCls(hView);

  // read in a file for resizing
  pFileList = pFile = NedFileNew(pFileList, aszFilename);
  if(pFileList == NULL)
    NedTestFailed();

  // ask user to resize 3 times
  // clear the top line (normally menu)
  NedWinPickBackdrop();
  NedWinGoto(0,0);
  NedWinSetAttr(pTheme->text);
  NedWinClearEol();

  pFile = pFileList;
  for(i=0; i<NumElements(aszPrompts); ++i)
  {
    // draw status bar. NedViewDraw() should never overwrite.
    pTheme = NedThemeGet(aThemes[i]);
    NedStatusBarSetTheme(pTheme);
    NedStatusBarSetFile(NedFilePath(pFile));
    NedStatusBarSetRowCol(1L, 1L);
    NedStatusBarShow();

    // check for cls 1st time
    NedViewSetTheme(hView, pTheme);
    NedViewCheckScreenSize(hView);
    NedViewInvalidateAll(hView);
    NedViewDraw(hView, pFile);
    c = NedMsgChoice(aszPrompts[i], NULL, "yn");
    if(c != 'y')
    {
      NedTestFailed();
    }
  }

  NedTestEnd();

  NedMsgClear();
  NedFileFreeAll(pFileList);
  NedViewFree(hView);
}

/*!------------------------------------------------------------------------------------------------
  Test viewing tabs vs. spaces
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcViewTabs(void)
{
  hNedView_t        hView;
  sNedFile_t       *pFile = NULL;
  sSelectInfo_t     sSelectInfo;

  NedTestBegin();

  hView = NedViewNew(NedThemeGet(NEDTHEME_NATURAL));
  if(!NedViewIsView(hView))
    NedTestFailed();
  NedViewCls(hView);

  // create an in-memory file with more lines that screen, with long lines and tabs
  pFile = NedFileNew(NULL, "tcdata_not_there.py");
  if(pFile == NULL)
    NedTestFailed()
  if(NedFileSize(pFile) != 0)
  {
    NedLogPrintf("file tcdata_not_there.py must NOT exists!\n");
    NedTestFailed();
  }
  NedPointInsBlock(&pFile->sCursor, (const uint8_t *)szTabFile, sizeof(szTabFile) - 1);
  pFile->fHardTabs      = TRUE;
  pFile->tabSize        = 4;
  pFile->fCrLf          = FALSE;

  // verify view can select a word with cursor before select point
  NedPointGotoLineCol(&pFile->sCursor, 1, 1);
  sSelectInfo.hView         = hView;
  sSelectInfo.pFile         = pFile;
  sSelectInfo.topLine       = 1;
  sSelectInfo.leftCol       = 1;
  sSelectInfo.selectOffset  = 0;
  sSelectInfo.szPrompt      = "Are the code lines lined up?";
  if(!TnvVerifySelected(&sSelectInfo))
    NedTestFailed();

  pFile->tabSize            = 2;
  sSelectInfo.szPrompt      = "Are the code lines now NOT lined up?";
  if(!TnvVerifySelected(&sSelectInfo))
    NedTestFailed();

  NedTestEnd();

  // clean up
  NedMsgClear();
  NedFileFree(NULL, pFile);
  NedViewFree(hView);
}

/*!------------------------------------------------------------------------------------------------
  Show a long file at various left offsets.

  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcViewTabLong(void)
{
  hNedView_t            hView;
  sNedFile_t           *pFile = NULL;
  sSelectInfo_t         sSelectInfo;

  NedTestBegin();

  hView = NedViewNew(NedThemeGet(NEDTHEME_NATURAL));
  if(!NedViewIsView(hView))
    NedTestFailed();
  NedViewCls(hView);

  // create the file from szTabFileLong
  pFile = NedFileNew(NULL, "tcdata_testview.txt");
  if(pFile == NULL)
    NedTestFailed();
  NedPointInsBlock(&pFile->sCursor, (const uint8_t *)szTabFileLong, sizeof(szTabFileLong) - 1);
  pFile->tabSize = 8;
  pFile->fHardTabs = TRUE;

  // left is 1 (start of line)
  NedPointFileTop(&pFile->sCursor);
  sSelectInfo.hView         = hView;
  sSelectInfo.pFile         = pFile;
  sSelectInfo.selectOffset  = 0;
  sSelectInfo.topLine       = 1;
  sSelectInfo.leftCol       = 1;
  sSelectInfo.szPrompt = "Is left at start of long lines?";
  if(!TnvVerifySelected(&sSelectInfo))
    NedTestFailed();

  // left is 2
  NedPointCharAdvance(&pFile->sCursor, 1);
  sSelectInfo.leftCol   = 2;
  sSelectInfo.szPrompt  = "Is left at 2 of long lines?";
  if(!TnvVerifySelected(&sSelectInfo))
    NedTestFailed();

  // left is 62
  NedFileColSet(pFile, &pFile->sCursor, 260);
  sSelectInfo.leftCol   = 62;
  sSelectInfo.szPrompt  = "Is left at 62 of long lines?";
  if(!TnvVerifySelected(&sSelectInfo))
    NedTestFailed();

  // left is 260
  sSelectInfo.leftCol   = 250;
  NedFileColSet(pFile, &pFile->sCursor, 260);
  sSelectInfo.szPrompt  = "Is left at 260 of long lines?";
  if(!TnvVerifySelected(&sSelectInfo))
    NedTestFailed();

  NedTestEnd();

  // clean up
  NedMsgClear();
  NedFileFree(NULL, pFile);
  NedViewFree(hView);
}

/*!------------------------------------------------------------------------------------------------
  Test select, on long lines short, long files, tabed and spaced lines
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcViewSelect(void)
{
  hNedView_t        hView;
  sNedFile_t       *pFile = NULL;
  sSelectInfo_t     sSelectInfo;
  long              i;

  NedTestBegin();

  NedViewInit();
  hView = NedViewNew(NedThemeGet(NEDTHEME_NATURAL));
  if(!NedViewIsView(hView))
    NedTestFailed();
  NedViewCls(hView);

  // create a file with more lines that screen, with long lines and tabs
  pFile = NedFileNew(NULL, "tcdata_not_there.md");
  if(pFile == NULL)
    NedTestFailed()
  if(NedFileSize(pFile) != 0)
  {
    NedLogPrintf("file tcdata_not_there.md must NOT exists!\n");
    NedTestFailed();
  }
  i = 0;
  while(i <= NedViewScreenRows(hView) + 2)
  {
    NedPointInsBlock(&pFile->sCursor, (const uint8_t *)szTabFileLong, sizeof(szTabFileLong) - 1);
    i += tabFileLines;
  }
  pFile->tabSize        = 8;
  pFile->fCrLf          = FALSE;
  pFile->fInSelect      = TRUE;

  // verify view can select a word with cursor before select point
  NedPointGotoLineCol(&pFile->sCursor, 1, 1);
  sSelectInfo.hView         = hView;
  sSelectInfo.pFile         = pFile;
  sSelectInfo.topLine       = 1;
  sSelectInfo.leftCol       = 1;
  sSelectInfo.selectOffset  = 3;
  sSelectInfo.szPrompt      = "Is word 'The' selected?";
  if(!TnvVerifySelected(&sSelectInfo))
    NedTestFailed();

  // verify view can select a word with cursor after select point
  NedPointGotoLineCol(&pFile->sCursor, 1, 10);
  sSelectInfo.selectOffset  = -5;
  sSelectInfo.szPrompt      = "Is word 'quick' selected?";
  if(!TnvVerifySelected(&sSelectInfo))
    NedTestFailed();
  
  NedTestEnd();

  // clean up
  NedMsgClear();
  NedFileFree(NULL, pFile);
  NedViewFree(hView);
}

/*-------------------------------------------------------------------------------------------------
  Run each test case
-------------------------------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcViewFiles",    TcViewFiles,   TC_SCREEN | TC_USER_INPUT },
    { "TcViewResize",   TcViewResize,  TC_SCREEN | TC_USER_INPUT },
    { "TcViewTheme",    TcViewTheme,   TC_SCREEN | TC_USER_INPUT },
    { "TcViewTabs",     TcViewTabs,    TC_SCREEN | TC_USER_INPUT },
    { "TcViewTabLong",  TcViewTabLong, TC_SCREEN | TC_USER_INPUT },
    { "TcViewSelect",   TcViewSelect,  TC_SCREEN | TC_USER_INPUT }
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestView", NumElements(aTestCases), aTestCases);

  NedThemeInit();
  NedLangInit();
  NedViewInit();
  NedStatusBarSetTheme(NedThemeGet(NEDTHEME_NATURAL));

  NedLogMaskSet(NEDLOG_VIEW);

  NedTestRun(hSuite);

  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
