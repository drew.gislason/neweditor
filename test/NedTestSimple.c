/*!************************************************************************************************
  @file NedTestSimple.c

  @brief  Ned Key Unit Test

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*///***********************************************************************************************
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "NedTest2.h"

void TcSimpleFail()
{
  NedTestBegin();
  NedTestPrintf("Failing is easy ");
  NedTestFailed();
  NedTestEnd();
}

void TcSimplePass1()
{
  NedTestBegin();
  NedTestPrintf("Verbose level is %u ", NedTestVerbose());
  NedTestPassed();
  NedTestEnd();
}

void TcSimplePass2()
{
  NedTestBegin();
  NedTestPrintf("Verbose level is %u ", NedTestVerbose());
  NedTestPassed();
  NedTestEnd();
}

void TcSimpleSkip()
{
  NedTestBegin();
  NedTestSkipped();
  NedTestEnd();
}

void TcSimpleInput()
{
  NedTestBegin();
  NedTestPassed();
  NedTestEnd();
}

int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcSimpleFail",  TcSimpleFail  },
    { "TcSimplePass1", TcSimplePass1 },
    { "TcSimplePass2", TcSimplePass2, TC_VERBOSE },
    { "TcSimpleSkip",  TcSimpleSkip  },
    { "TcSimpleInput", TcSimpleInput, TC_USER_INPUT }
  };
  hTestSuite_t        hSuite;
  int                 ret;
  unsigned            passed;
  unsigned            failed;
  unsigned            skipped;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("SimpleSuite", NumElements(aTestCases), aTestCases);
  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  if(!NedTestCalcLogTotals(&passed, &failed, &skipped))
    printf("Log doesn't contain test results\n");
  else
    printf("passed %u, failed %u, skipped %u\n", passed, failed, skipped);

  return ret;
}
