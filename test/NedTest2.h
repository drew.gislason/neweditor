/*!************************************************************************************************
  @file NedTest.h

  @brief  Test Harness Framework API

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @defgroup ned_test Test System API allows unit tests for code subsystems.

  This framework provides the following features:

  - Common test framework for all unit tests
  - Stylized (parsable) logging system
  - Test Cases and Test Groups
  - Similar to Python Unit Test
  - Colorized or not
  - Test Summary Report

  Some general rules that make testing easier:

  - "tcdata_nnn.*"  are input files to test cases
  - "tmp_nnn.*"     are temporary files that can be deleted

*///***********************************************************************************************
#ifndef NED_TEST2_H
#define NED_TEST2_H
#include "../src/Ned.h"
#include "../src/NedAnsi.h"
#include "../src/NedLog.h"

#define TESTATTR_PASSED  NEDATTR_GREEN
#define TESTATTR_FAILED  NEDATTR_RED
#define TESTATTR_SKIPPED NEDATTR_YELLOW
#define TESTATTR_NORMAL  NEDATTR_RESET

#define TC_USER_INPUT    0x0001   // requires user input/examination
#define TC_LONG          0x0002   // takes a long time to run
#define TC_SCREEN        0x0004   // uses screen, so test framework must not (log only)
#define TC_VERBOSE       0x0008   // enables NedTestPrintf()...

#define TEST_EXIT_PASS    0
#define TEST_EXIT_FAIL    1
#define TEST_EXIT_SKIP    2

typedef void * hTestSuite_t;
typedef void (*pfnTestCase_t)(void);

typedef struct
{
  const char     *szName;       // used for filtering
  pfnTestCase_t   pfnTestCase;  // order of test cases
  unsigned        opts;         // see TC_xxx
} sTestCase_t;

// INTERNAL: DO NOT USE
void NT_Failed  (const char *szExpr, const char *szFile, unsigned line);
void NT_Skipped (void);
void NT_Passed  (void);
void NT_Begin   (void);


// API
void          NedTestInit           (int argc, char *argv[]);
hTestSuite_t  NedTestNew            (const char *szSuiteName, unsigned numCases, const sTestCase_t *pTestCases);
void          NedTestFree           (hTestSuite_t hSuite);
void          NedTestRun            (hTestSuite_t hSuite);
int           NedTestSummary        (hTestSuite_t hSuite);
void          NedTestStop           (hTestSuite_t hSuite);
#define       NedTestBegin()        bool_t fNt_fPassed = TRUE; NT_Begin();
#define       NedTestEnd()          TestEnd: if(fNt_fPassed) NT_Passed();
#define       NedTestFailed()       { NT_Failed("", __FILE__,__LINE__); fNt_fPassed = FALSE; goto TestEnd; }
#define       NedTestAssert(expr)   if(!(expr)) { NT_Failed(#expr, __FILE__, __LINE__); fNt_fPassed = FALSE; goto TestEnd; }
#define       NedTestSkipped()      { fNt_fPassed = FALSE; NT_Skipped(); goto TestEnd; }
#define       NedTestPassed()       { fNt_fPassed = TRUE; goto TestEnd; }
int           NedTestPrintf         (const char *szFormat, ...);
unsigned      NedTestVerbose        (void);
bool_t        NedTestAutomated      (void);
void          NedTestVerboseSet     (unsigned verboseLevel);
bool_t        NedTestPassFail       (void);
unsigned      NedTestRandRange      (unsigned low, unsigned high);
bool_t        NedTestCalcLogTotals  (unsigned *pPassed, unsigned *pFailed, unsigned *pSkipped);

// for machine parsing of log file
extern const char m_szColumn[];
extern const char m_szMTestStart[];     // "$TEST_START"
extern const char m_szMTestPassed[];    // "$PASSED";
extern const char m_szMTestFailed[];    // "$FAILED";
extern const char m_szMTestSkipped[];   // "$SKIPPED";
extern const char m_szRetCodeFile[];    // "testall.tmp"

#endif // NED_TEST2_H
