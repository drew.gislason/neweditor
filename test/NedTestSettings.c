/*!************************************************************************************************
  @file NedTestSettings.c

  @brief  Test Ned setting functions.

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  @ingroup  ned_test

  Tests for Ned Settings. Loads the file nedproj.json

*///***********************************************************************************************
#include <ctype.h>
#include "NedTest2.h"
#include "NedLog.h"
#include "NedUtilFile.h"
#include "NedUtilStr.h"
#include "NedSettings.h"
#include "NedLanguage.h"

#define SZ_PROJ_NAME  "tcdata_Settings.json"

typedef struct
{
  char        *szFile;
  long         pos;            // cursor position in file
  long         topLine;        // 1-n, line at top of screen
  long         leftCol;        // 1-n, if line is long, screen may have scrolled to left
  long         prefCol;        // 1-n, preferred column (e.g. up/down arrows stay on column)
  bool_t       fRdOnly;        // file is read only
} sTestFile_t;

typedef struct
{
  unsigned    idx;
  long        pos;
} sTestPosPair_t;

typedef struct
{
  const char    *szKey;
  const char    *szCmd;
} sTestKeyMap_t;

typedef struct
{
  unsigned                  curFile;
  const char              **ppszRecentFiles;
  const char              **ppszProjectFolders;
  const char               *szTheme;
  const sTestFile_t        *pOpenFiles;
  const sTestPosPair_t     *pBookmarks;
  const sTestKeyMap_t      *pKeyMap;
  const sNedLanguage_t     *pLanguages;
} sTestEditorSetup_t;

static const char          *m_szProjName    = SZ_PROJ_NAME;
static const unsigned       m_curFile       = 1;
static const char          *m_ppszRecentFiles[]     = { "test.c", "~/Work/Git/file.c", "file3.h", NULL };
static const char          *m_ppszProjectFolders[]  = { "~/Work/Git/ned", NULL };
static const char           m_szTheme[]     = "Natural";
static const sTestFile_t    m_aOpenFiles[]  =
{
  { SZ_PROJ_NAME,         99, 1, 0, 16, FALSE },
  { "NedTestSettings.c", 133, 1, 2, 24, TRUE },
  { NULL }
};
static const sTestPosPair_t  m_aBookmarks[]  = { {1, 99}, {0, 200}, {0, 0} };
static const sTestKeyMap_t   m_aKeyMap[]     = { {"Ctrl-G", "EditCutWord"}, {NULL, NULL} };
static const sTestEditorSetup_t m_sSetup = 
{
  m_curFile,
  m_ppszRecentFiles,
  m_ppszProjectFolders,
  m_szTheme,
  m_aOpenFiles,
  m_aBookmarks,
  m_aKeyMap,
};

/*-------------------------------------------------------------------------------------------------
  Stub for linking NedKeyMap.c
-------------------------------------------------------------------------------------------------*/
const sNedMenuItem_t * NedMenuItemPtr(void *hMenu, unsigned nMenu, unsigned nItem)
{
  (void)hMenu;
  (void)nMenu;
  (void)nItem;
  return NULL;
}
void * NedMenuNew(sNedMenuBar_t *pMenuBar, nedThemeIndex_t themeIndex)
{
  (void)pMenuBar;
  (void)themeIndex;
  return NULL;
}

/*-------------------------------------------------------------------------------------------------
  Compare 2 text files. If there is a difference, returns -1 or 1, like strcmp() for files.
  Returns 0 if files are the same.
-------------------------------------------------------------------------------------------------*/
static int NsTestCompareFiles(const char *szFile1, const char *szFile2)
{
  char   *pszFile1;
  char   *pszFile2;
  int     ret = 0;

  pszFile1 = NedFileRead(szFile1);
  pszFile2 = NedFileRead(szFile2);
  if(pszFile1 && pszFile2)
  {
    ret = strcmp(pszFile1, pszFile2);
  }
  if(pszFile1)
    free(pszFile1);
  if(pszFile2)
    free(pszFile2);

  return ret;
}

/*-------------------------------------------------------------------------------------------------
  Get pointer to file by index and fileList. If index is too large, returns last file in list.
-------------------------------------------------------------------------------------------------*/
static sNedFile_t * NtsGetFileByIndex(sNedFile_t *pFileList, unsigned index)
{
  unsigned        i;
  sNedFile_t     *pFile;

  pFile = pFileList;
  for(i=0; i<index; ++i)
  {
    if(pFile->pNext)
      pFile = pFile->pNext;
  }

  return pFile;
}

/*-------------------------------------------------------------------------------------------------
  Return index of file. If not found, returns UINT_MAX
-------------------------------------------------------------------------------------------------*/
static unsigned NtsGetIndexByFile(const sNedFile_t *pFileList, const sNedFile_t *pFile)
{
  unsigned index = 0;

  while(pFileList)
  {
    if(pFileList == pFile)
      break;
    pFileList = pFileList->pNext;
    ++index;
  }

  return (pFileList == NULL) ? UINT_MAX : index;
}

/*-------------------------------------------------------------------------------------------------
  Clear the editor structure
-------------------------------------------------------------------------------------------------*/
static void NtsTestEditorSetup(sNedEditor_t *pEditor, const sTestEditorSetup_t *pSetup)
{
  const sTestFile_t      *pTestFile;
  const sTestPosPair_t   *pPosPair;
  sNedFile_t             *pFile;
  const char            **ppsz;
  sNedPos_t               sPos;
  const sTestKeyMap_t    *pKeyMap;

  NedLogPrintf("NtsTestEditorSetup()\n");

  memset(pEditor, 0, sizeof(*pEditor));
  pEditor->sanchk = NEDEDITOR_SANCHK;

  // setup pFileList in edtior
  pTestFile = pSetup->pOpenFiles;
  while(pTestFile && pTestFile->szFile)
  {
    NedLogPrintf("  szFile %s\n", pTestFile->szFile);

    // create a file structure from filename
    pFile = NedFileNew(pEditor->pFileList, pTestFile->szFile);
    if(pFile == NULL)
      break;

    // head of list if 1st file
    if(pEditor->pFileList == NULL)
      pEditor->pFileList = pFile;

    // initialize file structure from test fields
    NedPointInit(&pFile->sCursor, pFile->pBufHead);
    NedPointFileGotoPos(&pFile->sCursor, pTestFile->pos);
    pFile->topLine = pTestFile->topLine;
    pFile->leftCol = pTestFile->leftCol;
    pFile->prefCol = pTestFile->prefCol;
    pFile->fRdOnly = pTestFile->fRdOnly;
    ++pTestFile;
  }

  // setup pFileCur in editor
  pEditor->pFileCur = NtsGetFileByIndex(pEditor->pFileList, pSetup->curFile);


  // setup recent files and project folders
  ppsz = pSetup->ppszRecentFiles;
  pEditor->hRecentFiles = NedListNew(NEDCFG_MAX_RECENT, PATH_MAX, NedSettingsStrIsSame, NedSettingsStrShow);
  while(ppsz && *ppsz)
  {
    NedListAdd(pEditor->hRecentFiles, *ppsz, strlen(*ppsz) + 1);
    ++ppsz;
  }
  ppsz = pSetup->ppszProjectFolders;
  pEditor->hProjectFolders = NedListNew(NEDCFG_MAX_RECENT, PATH_MAX, NedSettingsStrIsSame, NedSettingsStrShow);
  while(ppsz && *ppsz)
  {
    NedListAdd(pEditor->hProjectFolders, *ppsz, strlen(*ppsz) + 1);
    ++ppsz;
  }

  // setup theme
  NedThemeInit();
  pEditor->themeIndex = NedThemeGetIndex(pSetup->szTheme);

  // set up bookmarks
  pEditor->hBookmarks = NedListNew(NEDCFG_MAX_HISTORY, sizeof(sNedPos_t), NedPosListIsSame, NedPosListLogShow);
  pPosPair = pSetup->pBookmarks;
  while(pPosPair->idx || pPosPair->pos) // {0,0} terminates
  {
    sPos.pFile  = NtsGetFileByIndex(pEditor->pFileList, pPosPair->idx);
    sPos.pos    = pPosPair->pos;
    NedListAdd(pEditor->hBookmarks, &sPos, NEDLIST_DEF_SIZE);
    ++pPosPair;
  }

  // setup keymap
  NedKeyMapFactory();
  pKeyMap = pSetup->pKeyMap;
  while(pKeyMap && pKeyMap->szKey != NULL)
  {
    NedLogPrintf("  keymap %s->%s\n", pKeyMap->szKey, pKeyMap->szCmd);
    if(!NedKeyMapSetCommand(pKeyMap->szKey, pKeyMap->szCmd))
      NedLogPrintf("  failed!\n");
    ++pKeyMap;
  }

  // set language to current file
  NedLangInit();
  pEditor->pLang = NedLangGet(NedFilePath(pEditor->pFileCur));
}

/*!------------------------------------------------------------------------------------------------
  Test Case for keymapping
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcSettingsKeyMap(void)
{
  const sTestKeyMap_t  *pKeyMap = m_aKeyMap;
  pfnNedCmd_t           pfnCmd;
  nedKey_t              key;
  unsigned              numExpected = 0;
  unsigned              i;

  NedTestBegin();

  NedKeyMapFactory();
  while(pKeyMap->szKey)
  {
    ++numExpected;
    if(!NedKeyMapSetCommand(pKeyMap->szKey, pKeyMap->szCmd))
    {
      NedLogPrintf("  failed to set key %s cmd %s\n", pKeyMap->szKey, pKeyMap->szCmd);
      NedTestFailed();
    }
    ++pKeyMap;
  }

  for(i=0; i<4; ++i)    // NEDCFG_MAX_KEYMAP
  {
    pfnCmd = NedKeyMapGetMapped(i, &key);
    if(pfnCmd == NULL)
      break;
    NedLogPrintf("  found non-default key %s cmd %s\n", NedKeyName(key), NedCmdGetName(pfnCmd));
  }
  if(i != numExpected)
  {
    NedLogPrintf("  failed, expected %u to be mapped, found %u\n", numExpected, i);
    NedTestFailed();
  }

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test Case for TcSettingsBasic(). A very simple test setting a few fields, then comparing to a
  saved file.
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcSettingsSave(void)
{
  sNedEditor_t    sEditor;
  char           *szProjName = "tmp_proj.json";

  NedTestBegin();

  memset(&sEditor, 0, sizeof(sEditor));
  NtsTestEditorSetup(&sEditor, &m_sSetup);

  if(NedSettingsSave(szProjName, &sEditor) != NEDSET_ERR_NONE)
  {
    NedLogPrintf("  Failed to save settings\n");
    NedTestFailed();
  }

  if(NsTestCompareFiles(szProjName, m_szProjName) != 0)
  {
    NedLogPrintf("  output .json didn't compare\n");
    NedTestFailed();
  }

  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test Case for NedSettingsLoad()
  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcSettingsLoad(void)
{
  sNedEditor_t            sEditor;
  sNedPos_t              *pPos;
  unsigned                i;
  nedSetErr_t             err;
  const sTestFile_t      *pTestFile;
  const sTestPosPair_t   *pPosPair;
  const sNedFile_t       *pFile;
  const char             *psz;

  NedTestBegin();

  NedLangInit();
  memset(&sEditor, 0, sizeof(sEditor));
  err = NedSettingsLoad(&sEditor, m_szProjName, TRUE);
  if(err != NEDSET_ERR_NONE)
  {
    NedLogPrintf("  Failed to load settings\n");
    NedTestFailed();
  }

  if(!NedEditorIsEditor(&sEditor))
  {
    NedLogPrintf("  Failed Editor Check\n");
    NedTestFailed();
  }

  // verify files are correct
  pTestFile = &m_sSetup.pOpenFiles[0];
  pFile     = sEditor.pFileList;
  i = 0;
  while((pFile != NULL) && (pTestFile->szFile != NULL))
  {
    NedLogPrintf("  file[%u] %s\n", i, pFile->sInfo.szFullPath);
    if(strcmp(pTestFile->szFile, NedStrNameOnly(pFile->sInfo.szFullPath)) != 0)
    {
      NedLogPrintf("  expected file %s, got %s\n", pTestFile->szFile, pFile->sInfo.szFullPath);
      NedTestFailed();
    }

    // check metadata
    if((pFile->topLine != pTestFile->topLine) || (pFile->leftCol != pTestFile->leftCol) ||
       (pFile->prefCol != pTestFile->prefCol) || (pFile->fRdOnly != pTestFile->fRdOnly))
    {
      NedLogPrintf("  failed file metadata topline %u,%u, leftCol %u,%u, prefCol %u,%u, rdOnly %u,%u\n",
        pFile->topLine, pTestFile->topLine, pFile->leftCol, pTestFile->leftCol,
        pFile->prefCol, pTestFile->prefCol, pFile->fRdOnly, pTestFile->fRdOnly);
      NedTestFailed();
    }

    // check point position
    if(NedPointPos(&pFile->sCursor) != pTestFile->pos)
    {
      NedLogPrintf("  failed file pos\n");
      NedTestFailed();
    }

    ++pTestFile;
    ++i;
    pFile = pFile->pNext;
  }

  if((pFile != NULL) || (pTestFile->szFile != NULL))
  {
    NedLogPrintf("  to many or too few files\n");
    NedTestFailed();
  }

  // verify current file
  if(NtsGetIndexByFile(sEditor.pFileList, sEditor.pFileCur) != m_sSetup.curFile)
  {
    NedLogPrintf("  current file wrong. Expected %u, got %u\n", m_sSetup.curFile, 
        NtsGetFileByIndex(sEditor.pFileList, m_sSetup.curFile));
    NedTestFailed();
  }

  // verify recent files and project folders
  NedListIterStart(sEditor.hRecentFiles);
  for(i=0; (i < NEDCFG_MAX_RECENT) && (m_sSetup.ppszRecentFiles[i] != NULL); ++i)
  {
    if(i >= NedListNumItems(sEditor.hRecentFiles))
      break;
    psz = NedListNext(sEditor.hRecentFiles);
    if(psz == NULL)
    {
      NedLogPrintf("  failed pEditor->hRecentFiles\n");
      NedTestFailed();
    }
    if(strcmp(psz, m_sSetup.ppszRecentFiles[i]) != 0)
    {
      NedLogPrintf("  failed hRecentFiles, expected %s, got %s\n", psz, m_sSetup.ppszRecentFiles[i]);
      NedTestFailed();
    }
  }

  NedListIterStart(sEditor.hProjectFolders);
  for(i=0; (i < NEDCFG_MAX_RECENT) && (m_sSetup.ppszProjectFolders[i] != NULL); ++i)
  {
    if(i >= NedListNumItems(sEditor.hProjectFolders))
      break;
    psz = NedListNext(sEditor.hProjectFolders);
    if(psz == NULL)
    {
      NedLogPrintf("  failed pEditor->hProjectFolders\n");
      NedTestFailed();
    }
    if(strcmp(psz, m_sSetup.ppszProjectFolders[i]) != 0)
    {
      NedLogPrintf("  failed hRecentFiles, expected %s, got %s\n", psz, m_sSetup.ppszProjectFolders[i]);
      NedTestFailed();
    }
  }

  // verify theme
  if(strcmp(NedThemeGetName(sEditor.themeIndex), m_sSetup.szTheme) != 0)
  {
    NedLogPrintf("  failed them, expected %s\n", m_sSetup.szTheme);
    NedTestFailed();
  }

  // verify bookmarks
  pPosPair = &m_sSetup.pBookmarks[0];
  NedListIterStart(sEditor.hBookmarks);
  NedLogPrintf("NumBookmarks: %u\n", NedListNumItems(sEditor.hBookmarks));
  for(i=0; (i < NEDCFG_MAX_HISTORY) && (pPosPair->idx || pPosPair->pos); ++i)  // {0,0} terminates
  {
    if(i >= NedListNumItems(sEditor.hBookmarks))
    {
      NedLogPrintf("  failed Bookmarks, not enough points\n");
      NedTestFailed();
    }

    pPos = NedListNext(sEditor.hBookmarks);
    if(pPos->pos != pPosPair->pos)
    {
      NedLogPrintf("  failed Bookmarks, wrong pos, expected %ld, got %ld\n", pPosPair->pos, pPos->pos);
      NedTestFailed();
    }
    ++pPosPair;
  }

  // sSetKeyMap_t     *pKeyMap;
  NedTestEnd();
}

/*!------------------------------------------------------------------------------------------------
  Test Case to check that keywords can be saved and loaded

  Adds "Foo" language and modifies "Ruby" language.

  @returns    none
*///-----------------------------------------------------------------------------------------------
void TcSettingsSetLang(void)
{
  const char *aszKeywordsRuby[] = { "BEGIN", "END", "__LINE__", NULL };
  sNedEditor_t          sEditor;
  char                  szTmpName[]  = "tmp_proj.json";
  nedSetErr_t           err;
  const char *aszLineCommentsFoo[]  = { "~~", NULL };
  const char *aszBlockCommentsFoo[] = { "<~~~", "~~~>", NULL };
  const char *aszKeywordsFoo[]      = { "foo", "bar", "baz", NULL };
  const sNedLanguage_t  sFoo =
  {
    .szLang           = "Foo",
    .szExt            = ".foo .bar .baz",
    .tabSize          = 4,
    .fHardTabs        = FALSE,
    .fCrLf            = TRUE,
    .wrapLen          = 120,
    .aszLineComments  = aszLineCommentsFoo,
    .aszBlockComments = aszBlockCommentsFoo,
    .aszKeywords      = aszKeywordsFoo
   };
  sNedLanguage_t        sLangRuby;
  const sNedLanguage_t *pLang;

  NedTestBegin();

  NedLangInit();
  memset(&sEditor, 0, sizeof(sEditor));

  err = NedSettingsLoad(&sEditor, "tcdata_Settings.json", TRUE);
  if(err != NEDSET_ERR_NONE)
  {
    NedLogPrintf("  Failed to load settings\n");
    NedTestFailed();
  }

  if(!NedEditorIsEditor(&sEditor))
  {
    NedLogPrintf("  Failed Editor Check\n");
    NedTestFailed();
  }


  // set up the new languages
  pLang = NedLangGet("x.rb");
  if(pLang == NULL)
  {
    NedLogPrintf("  Failed to find Ruby\n");
    NedTestFailed();
  }
  memcpy(&sLangRuby, pLang, sizeof(sLangRuby));
  sLangRuby.aszKeywords = aszKeywordsRuby;
  sLangRuby.tabSize = 3;
  if(!NedLangSet(&sLangRuby))
  {
    NedLogPrintf("  Failed to set Ruby\n");
    NedTestFailed();
  }
  if(!NedLangSet(&sFoo))
  {
    NedLogPrintf("  Failed to set Foo\n");
    NedTestFailed();
  }

  // check language loaded properly
  pLang = NedLangGet("test.rb");
  if(pLang == NULL)
  {
    NedLogPrintf("  Failed to find Ruby\n");
    NedTestFailed();
  }

  if(NedUtilStrArrayCmp(pLang->aszKeywords, aszKeywordsRuby) != 0)
  {
    NedLogPrintf("  Ruby keywords didn't compare\n");
    NedTestFailed();
  }

  if(NedSettingsSave(szTmpName, &sEditor) != NEDSET_ERR_NONE)
  {
    NedLogPrintf("  Failed to save settings\n");
    NedTestFailed();
  }

  if(NsTestCompareFiles("tcdata_SettingsLoad.json", szTmpName) != 0)
  {
    NedLogPrintf("  output .json didn't compare\n");
    NedTestFailed();
  }

  NedTestEnd();
}

/*-------------------------------------------------------------------------------------------------
  Run all test cases
-------------------------------------------------------------------------------------------------*/
int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcSettingsKeyMap",   TcSettingsKeyMap },
    { "TcSettingsSave",     TcSettingsSave },
    { "TcSettingsLoad",     TcSettingsLoad },
    { "TcSettingsSetLang",  TcSettingsSetLang }
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedLogMaskSet(NEDLOG_SETTINGS | NEDLOG_JSON);
  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestSettings", NumElements(aTestCases), aTestCases);
  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
