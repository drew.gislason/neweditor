#include <stdio.h>
#include <string.h>
#include "NedUtilFile.h"
#include "NedUtilStr.h"

// # a #define symbol.field, or symbol->field 
void NedError(const char *szExpr, const char *szFile, const char *szFunc, unsigned line)
{
  printf("NedAssert: (%s), file: %s, func: %s(), line: %u\n",szExpr, szFile, szFunc, line);
  exit(1);
}

int NedLogPrintf(const char *szFormat, ...)   // just a stub
{
  (void)szFormat;
  return 0;
}

int main(int argc, char *argv[])
{
  sNedFileInfo_t  sInfo;
  bool_t          fWorked;
  const char     *szDir;
  const char     *szFile;
  char            szPath[PATH_MAX];

  if(argc == 1)
  {
    printf("test dirname\n");
    return 0x1;
  }

  *szPath = '\0';
  fWorked = NedFileFindInPath(szPath, sizeof(szPath)-1, "ned.json", TRUE);
  printf("NedFileFindInPath(nedproj.json) worked %u, fullpath '%s'\n", fWorked, szPath);

  *szPath = '\0';
  fWorked = NedFileFindInFolder(szPath, sizeof(szPath)-1, "ned.json", ".");
  printf("NedFileFindInFolder(nedproj.json) worked %u, fullpath '%s'\n", fWorked, szPath);

  *szPath = '\0';
  fWorked = NedLibFileFullPath(szPath, argv[1]);
  printf("'%s' worked %u, fullpath '%s'\n", argv[1], fWorked, szPath);

  szDir = NedStrDirOnlyHome(argv[1]);
  szFile = NedStrNameOnly(argv[1]);
  printf("dir %s, file %s\n", szDir, szFile);
  strcpy(szPath, szDir);
  NedStrAppendName(szPath, "file.c");
  printf("appended %s\n", szPath);

  sInfo.szFullPath[0] = '\0';
  fWorked = NedFileInfoGet(&sInfo, argv[1]);
  printf("'%s' worked %u, isDir %u, fullpath '%s'\n", argv[1], fWorked, sInfo.fIsDir, sInfo.szFullPath);

  return 0;
}
