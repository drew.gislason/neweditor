/*!************************************************************************************************
  @file NedTestKeyMap.c

  @brief  Ned KeyMap And MenuMap Tests

  Copyright 2021 Drew Gislason

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
  associated documentation files (the "Software"), to deal in the Software without restriction,
  including without limitation the rights to use, copy, modify, merge, publish, distribute,
  sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  This module tests how the keys and menus and mapped to command. Verifies it's correct. Helps
  with documenting in NedManual.md

*///***********************************************************************************************
#include "Ned.h"
#include "NedLog.h"
#include "NedTest2.h"
#include "NedKeyMap.h"

// key/cmd pairs, helper structure
typedef struct
{
  const char  *szKey;
  const char  *szCmd;
} keyCmd_t; 

/*!------------------------------------------------------------------------------------------------
  Get an answer from the user
-------------------------------------------------------------------------------------------------*/
bool_t TkmOk(const char *szPrompt)
{
  char sz[64];
  printf("\n%s (y/n) ", szPrompt);
  fflush(stdout);
  memset(sz, 0, sizeof(sz));
  fgets(sz, sizeof(sz)-1, stdin);
  return (*sz == 'n' || *sz == 'N') ? FALSE : TRUE;
}

/*!------------------------------------------------------------------------------------------------
  Copy a string
-------------------------------------------------------------------------------------------------*/
char * NtkCopy(const char *sz)
{
  unsigned   len = strlen(sz);
  char      *psz;

  psz = malloc(len + 1);
  if(psz)
  {
    memcpy(psz, sz, len);
    psz[len] = '\0';
  }

  return psz;
}

/*!------------------------------------------------------------------------------------------------
  Show all the key mappings in a markdown table

  @ingroup    ned_test
  @returns    none
-------------------------------------------------------------------------------------------------*/
void TcKeyMapShowKeys(void)
{
  #define       COLS  3
  keyCmd_t     *pKeyCmds;
  unsigned      cmdWidth[COLS];
  unsigned      keyWidth[COLS];
  unsigned      numKeys;
  unsigned      len;
  unsigned      i;
  pfnNedCmd_t   pfnCmd;
  nedKey_t      key;
  char          szLine[80];     // separator line ---------
  const char    szSep[] = " | ";
  const char    szKey[] = "Key";
  const char    szCmd[] = "Command";

  NedTestBegin();

  NedKeyMapFactory();

  // get and calculate column width for all key/command pairs
  for(i = 0; i < COLS; ++i)
  {
    keyWidth[i] = strlen(szKey);
    cmdWidth[i] = strlen(szCmd);
  }
  pKeyCmds = malloc(sizeof(keyCmd_t) * NEDKEY_NONE);
  if(!pKeyCmds)
    NedTestFailed();
  numKeys = 0;
  for(i = 0; i < NEDKEY_NONE; ++i)
  {
    key = (nedKey_t)i;
    pfnCmd = NedKeyMapGetCommand(key);
    if(pfnCmd && pfnCmd != NedCmdDoNothing && pfnCmd != NedCmdEditInsertSelf)
    {
      pKeyCmds[numKeys].szKey = NtkCopy(NedKeyName(key));
      pKeyCmds[numKeys].szCmd = NedCmdGetName(pfnCmd);

      // calculate maximm width for key/commmand for this column
      len = strlen(pKeyCmds[numKeys].szKey);
      if(len > keyWidth[numKeys % COLS])
        keyWidth[numKeys % COLS] = len;
      len = strlen(pKeyCmds[numKeys].szCmd);
      if(len > cmdWidth[numKeys % COLS])
        cmdWidth[numKeys % COLS] = len;

      ++numKeys;
    }
  }

  // print all commmands in columns. Useful to put into NedManual.md. Verify no duplicates.
  memset(szLine, '-', sizeof(szLine) - 1);
  szLine[sizeof(szLine) - 1] = '\0';

  // print markdown header
  // : Key     | Command 
  // :------ | :--------
  printf("\n");
  for(i = 0; i < COLS; ++i)
  {
    printf("%-*s%s%-*s%s", keyWidth[i % COLS], "Key", szSep,
           cmdWidth[i % COLS], "Command", (i == COLS - 1) ? "" : szSep);
  }
  printf("\n");
  for(i=0; i < COLS; ++i)
  {
    printf(":%.*s%s:%.*s%s", keyWidth[i % COLS] - 1, szLine, szSep,
          cmdWidth[i % COLS] - 1, szLine, (i == COLS - 1) ? "" : szSep);
  }
  printf("\n");

  // print column data 
  for(i=0; i<numKeys; ++i)
  {
    printf("%-*s%s%-*s%s", keyWidth[i % COLS], pKeyCmds[i].szKey, szSep,
           cmdWidth[i % COLS], pKeyCmds[i].szCmd, (((i + 1) % COLS) == 0) ? "" : szSep);
    if(((i+1) % COLS) == 0)
      printf("\n");
  }
  if((i % COLS) != 0)
    printf("\n");

  printf("\n%u Mapped Keys\n", numKeys);

  // expected number of default keys bound to commands
  if(NedTestAutomated())
  {
    if(numKeys != 99) 
      NedTestFailed();
  }
  // let user decide if they look right
  else
  {
    if(!TkmOk("Examine key/cmd pairs. Look OK to you?"))
      NedTestFailed();
  }

  NedTestEnd();
}


int main(int argc, char *argv[])
{
  const sTestCase_t   aTestCases[] =
  { 
    { "TcKeyMapShowKeys", TcKeyMapShowKeys }
  };
  hTestSuite_t        hSuite;
  int                 ret;

  NedTestInit(argc, argv);
  hSuite = NedTestNew("NedTestKeyMap", NumElements(aTestCases), aTestCases);
  NedTestRun(hSuite);
  ret = NedTestSummary(hSuite);
  NedTestFree(hSuite);

  return ret;
}
