#include <stdio.h>
#include <string.h>
#include "NedUtilFile.h"
#include "NedUtilStr.h"

// # a #define symbol.field, or symbol->field
void NedError(const char *szExpr, const char *szFile, const char *szFunc, unsigned line)
{
  printf("NedAssert: (%s), file: %s, func: %s(), line: %u\n",szExpr, szFile, szFunc, line);
  exit(1);  
}

int NedLogPrintf(const char *szFormat, ...)   // just a stub
{
  (void)szFormat;
  return 0;
}

int main(int argc, char *argv[])
{
  char  szPath[PATH_MAX]    = "/Users/drew/ned/*";
  char  szDirOnly[PATH_MAX] = "";
  const char szDir[]        = "dir/";
  char *psz;

  printf("szPath %s, isFolder %u\n", szPath, NedStrIsFolder(szPath));
  // thing
  printf("szDir  %s, isFolder %u, isFolder .. %u\n", szDir,  NedStrIsFolder(szDir), NedStrIsFolder(".."));
  // worked
  strcpy(szDirOnly, NedStrDirOnly(szPath));
  printf("szDirOnly '%s'\n", szDirOnly);

  if(NedLibParentPath(szPath))
    printf("parent path %s\n", szPath);
  else
    printf("no parent\n");

  psz = (char *)NedStrLastSlash(szPath);
  if(psz)
    *psz = '\0';
  NedStrAppendName(szPath, "dir/");
  NedStrAppendName(szPath, "*");
  printf("Path now %s\n", szPath);

  return 0;
}
